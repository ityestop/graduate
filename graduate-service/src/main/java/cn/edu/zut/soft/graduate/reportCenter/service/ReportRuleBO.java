package cn.edu.zut.soft.graduate.reportCenter.service;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportRule;

import java.util.Date;
import java.util.List;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportRuleBO extends BasicBO<ReportRule,Query> {

    List<ReportRule> findRuleByDate(Date date);

}
