package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.model.BaseModel;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString
public class BaseModelImpl implements BaseModel {
    private Integer id;
    private String createAuthor;
    private String updateAuthor;
    private Date createTime;
    private Date updateTime;
    private Integer del;

}
