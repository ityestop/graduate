package cn.edu.zut.soft.graduate.interceptor;

import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.userCenter.bo.StatisticsService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * @author chuchuang
 * @date 16/11/21
 */
@Component
public class RoleControllerInterceptorAdapter extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute(IKEY.USER);
        if (Pattern.compile("^/"+loginVO.getRole()+".*").matcher(uri).matches()){
            StatisticsService.loginMap.put(loginVO.getId(),loginVO);
            return super.preHandle(request, response, handler);
        }else {
            response.sendRedirect(request.getContextPath() + "/home");
            return false;
        }
    }
}
