$(function () {
    $("#chooseInfo").hide();
    var group_id = $("#group_ID").val();
    var group_leader = $("#group_Leader").val();
    var $identyId = $("#identyId").val();
    var $topic = $("#topicId").val();

    var dataResult = {};

    var create_btn = createGroup_btn();
    var noGroup_btn = noGroup_stu_btn();
    var delete_btn = delete_group_btn();
    var company_btn = company_group_btn();
    var create_company_btn = create_company_group_btn();


    if(group_id == null || group_id == undefined || group_id == "" ){
        $("#group_btnes").append(create_btn);
    }else {
        if(group_leader != null && group_leader != undefined && group_leader != ""){
            $("#group_btnes").append(noGroup_btn).append(delete_btn);
        }
    }


    // $.ajax({
    //     type:'get',
    //     url:'/stu/groupInfo',
    //     dataType: "json",
    //     success:function (result) {
    //         if(result.success){
    //             dataResult = result.data;
    //             groupInfoList();
    //         }
    //     }
    // });

    $.ajax({
        type:'post',
        url:'/group/companyRole',
        dataType:'json',
        success:function (result) {
            if(result.success){
                if(result.data == '' || result.data == null || result.data == undefined){
                    if(group_id != '' && group_id != null && group_id != undefined){
                       if($topic == "" || $topic == null || $topic == undefined){
                           $("#group_btnes").append(company_btn);
                       }
                    }
                }else{
                    if(result.data.status == 'ING'){
                        $("#company_status_cont").html('您申请的公司项目权限正在被管理员审核中!');
                    }else if(result.data.status == 'YES'){
                        $.ajax({
                            type:'post',
                            dataType:'json',
                            url:'identity/getGuideTea',
                            data:{
                                id:$identyId,
                            },
                            success:function (result) {
                                if(result.success){
                                    var $teaData = result.data[0];
                                    $("#company_status_cont").html("恭喜您获得了新建公司项目权限! 您的指导教师为"+$teaData.identityName);
                                }
                            }
                        });

                    }
                }
            }
        }
    });

    $.ajax({
        type:'get',
        url:'/stu/createCompanyRole',
        dataType: "json",
        success:function (result) {
            if(result.success){
                $("#group_btnes").append(create_company_btn);
            }
        }
    });

    $("#createGroup_btn").click(function () {
        if(group_id !== null && group_id !== undefined && group_id !== ""){
            alert("您已经存在小组!");
            return false;
        }
        $.ajax({
            type:'POST',
            dataType:'json',
            url:'./stu/creatGroup',
            success:function (result) {
                if(result.success){
                    alert("创建成功!");
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        })
    });

    $("#deleteGroup_btn").click(function () {
        $.ajax({
            type:'POST',
            dataType:'json',
            url:'./stu/deleteGroup',
            success:function (result) {
                if(result.success){
                    alert("解散小组成功!");
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        })
    });

    $("#company_reason_submit").click(function () {
       var $reason = $("#reason").val();
        if($reason == null || $reason == '' || $reason == undefined){
            alert("申请内容不可为空!");
            return false;
        }else{
            $.ajax({
               type:'post',
                url:'/group/companyAsk',
                dataType:'json',
                data:{
                    reason:$reason,
                },
                success:function (result) {
                    if(result.success){
                        alert("申请已发送管理员,请等待结果!");
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                    }
                }
            });
        }
    });

    function  createGroup_btn() {
        return '<div id=\"createGroup_stu\" class=\"col-sm-2\">'+
               '<button type=\"button\" class=\"btn btn-blue btn-block\" id=\"createGroup_btn\" >'+
               '<span> 创建小组</span>'+
               '</button>'+
               '</div>';
    }

    function  noGroup_stu_btn() {
        return '<div id=\"createGroup_stu\" class=\"col-sm-2\">'+
            '<button type=\"button\" class=\"btn btn-blue btn-block\" id=\"noGroup_btn\" onclick=\"window.location.href=\'./stu/noGroupList\'\" >'+
            '<span> 邀请成员</span>'+
            '</button>'+
            '</div>';
    }

    function  delete_group_btn() {
        return '<div id=\"createGroup_stu\" class=\"col-sm-2\">'+
            '<button type=\"button\" class=\"btn btn-blue btn-block\" id="deleteGroup_btn" >'+
            '<span> 解散小组</span>'+
            '</button>'+
            '</div>';
    }

    function  company_group_btn() {
        return '<div class=\"col-sm-2\">'+
            '<button type=\"button\" class=\"btn btn-blue btn-block\" data-toggle=\"modal\" data-target=\"#companyReason\" >'+
            '<span> 申请公司项目</span>'+
            '</button>'+
            '</div>';
    }
    function  create_company_group_btn() {
        return '<div class=\"col-sm-2\">'+
            '<a  class=\"btn btn-blue btn-block\" href="/stu/toCompanyIssue" )" >'+
            '<span> 填写公司项目申请</span>'+
            '</a>'+
            '</div>';
    }

    function groupInfoList() {
        var $tbody=$(getTbody());
        $.each(dataResult,function (key,value) {
            var tr_h = $(getTr());
            var td_stu_nid = $(getTd());
            var td_stu_nid_sm = $(getsm());
            td_stu_nid_sm.append(value.identity.noId);
            td_stu_nid.append(td_stu_nid_sm);
            tr_h.append(td_stu_nid);

            var td_stu_name = $(getTd());
            var td_stu_name_sm = $(getsm());
            td_stu_name_sm.append(value.identity.name);
            td_stu_name.append(td_stu_name_sm);
            tr_h.append(td_stu_name);

            var td_stu_sex = $(getTd());
            var td_stu_sex_sm = $(getsm());
            if(value.identity.sex == -1){
                td_stu_sex_sm.append('女');
            }else if(value.identity.sex == 1){
                td_stu_sex_sm.append('男');
            }else{
                td_stu_sex_sm.append('');
            }
            td_stu_sex.append(td_stu_sex_sm);
            tr_h.append(td_stu_sex);

            var td_stu_class = $(getTd());
            var ts_stu_class_sm = $(getsm());
            ts_stu_class_sm.append(value.infoList[0].value);
            td_stu_class.append(ts_stu_class_sm);
            tr_h.append(td_stu_class);

            var td_stu_phone = $(getTd());
            var td_stu_phone_sm = $(getsm());
            td_stu_phone_sm.append(value.identity.phone);
            td_stu_phone.append(td_stu_phone_sm);
            tr_h.append(td_stu_phone);

            var td_stu_child = $(getTd());
            var td_stu_child_sm = $(getsm());
            td_stu_child_sm.append('无');
            td_stu_child.append(td_stu_child_sm);
            tr_h.append(td_stu_child);
            $tbody.append(tr_h);
            tr_h.append('');

        })

        $("#group_stu_info").append($tbody);
        
    }

    function getTr() {
        return "<tr></tr>";
    }

    function getTd() {
        return "<td></td>";
    }
    function getsm() {
        return "<small></small>";
    }

    function getId() {
        return "<a href='#'></a>"
    }
    function getTbody() {
        return"<tbody></tbody>";
    }

    $.ajax({
        type:'post',
        dataType:'json',
        url:'/stu/stuIssueStatus',
        success:function (result) {
            if(result.success){
                $("#chooseInfo").show();
                var data = result.data;
                var $tbody=$(getTbody());
                $.each(data,function (key,value) {
                    var tr_h = $(getTr());
                    var td_title = $(getTd());
                    var td_title_sm = $(getsm());
                    var td_title_id = $(getId());
                    td_title_id.append(value.issue.title);
                    td_title_id.attr("href","/stu/issueInfo?id="+value.issue.id);

                    td_title_sm.append(td_title_id);
                    td_title.append(td_title_sm);
                    tr_h.append(td_title);

                    var td_source = $(getTd());
                    var td_source_sm = $(getsm());
                    if(value.issue.source == 'school'){
                        td_source_sm.append('校内题目');
                    }else{
                        td_source_sm.append('自拟题目');
                    }
                    td_source.append(td_source_sm);
                    tr_h.append(td_source);

                    var td_tea = $(getTd());
                    var td_tea_sm = $(getsm());
                    td_tea_sm.append(value.issue.schoolTeaName);
                    $("#TeaguName").append(value.issue.schoolTeaName);
                    td_tea.append(td_tea_sm);
                    tr_h.append(td_tea);

                    var td_time = $(getTd());
                    var td_time_sm = $(getsm());
                    td_time_sm.append(timeStamp2String(value.declare.createTime));
                    td_time.append(td_time_sm);
                    tr_h.append(td_time);

                    var td_status = $(getTd());
                    var td_status_sm = $(getsm());
                    if(value.declare.status == 'ING'){
                        td_status_sm.append("教师审核中");
                    }else if(value.declare.status == 'YES'){
                        td_status_sm.append("选报成功");
                    }else if(value.declare.status == 'NO'){
                        td_status_sm.append("选报失败");
                    }
                    td_status.append(td_status_sm);
                    tr_h.append(td_status);
                    if(value.issue.source == 'myself' && value.declare.status == 'ING'){
                        var td_edit = $(getTd());
                        var td_edit_sm = $(getsm());
                        var td_edit_id = $(getId());
                            td_edit_id.attr("href","/stu/toCompanyIssue?issueId="+value.issue.id),
                            td_edit_id.append('编辑'),
                            td_edit_sm.append(td_edit_id);
                            td_edit.append(td_edit_sm);
                            tr_h.append(td_edit);
                    }else{
                        var td_edit = $(getTd());
                        var td_edit_sm = $(getsm());
                        td_edit_sm.append('无');
                        td_edit.append(td_edit_sm);
                        tr_h.append(td_edit);
                    }

                    $tbody.append(tr_h);
                    tr_h.append('');
                })
                $("#group_issue_info").append($tbody);
            }
        }
    });

    function timeStamp2String(time) {
        var datetime = new Date();
        datetime.setTime(time);
        var year = datetime.getFullYear();
        var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
        var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
        var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
        var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
        var second = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
        return year + "-" + month + "-" + date;
    }

})