package cn.edu.zut.soft.graduate.remarkCenter.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Remark;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by chuchuang on 16/10/21.
 */
public interface RemarkDAO<T extends Remark> extends BasicDAO<Integer,T> {
    int updateByExampleWithBLOBs(@Param("record") T record, @Param("example") Example example);

    List<T> selectByExampleWithBLOBs(Example example);

    int updateByPrimaryKeyWithBLOBs(T record);
}
