package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * 专家
 * Created by chuchuang on 16/10/23.
 */
@Data
public class Witness {

    private Integer id;
    private String name;

    public Witness(){

    }
    public Witness(Integer id,String name){
        this.id = id;
        this.name = name;
    }

}
