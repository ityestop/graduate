package cn.edu.zut.soft.graduate.reportCenter.query;

import cn.edu.zut.soft.basic.core.model.dao.Page;

import java.util.List;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportDataQuery extends Page {

    List<Integer> getIds();

    List<Integer> getRuleIds();

    List<Integer> getIdentityIds();

    List<Integer> getNums();

    List<Integer> getUnRuleIds();

}
