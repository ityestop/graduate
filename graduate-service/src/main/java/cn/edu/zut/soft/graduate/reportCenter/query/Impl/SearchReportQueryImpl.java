package cn.edu.zut.soft.graduate.reportCenter.query.Impl;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.reportCenter.common.TimePeriod;
import cn.edu.zut.soft.graduate.reportCenter.query.SearchReportQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author cc
 * @Date 2017/2/13
 * @value 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SearchReportQueryImpl extends Query implements SearchReportQuery {

    private TimePeriod timePeriod;

    private List<Integer> nums;

    private Alternative reportStatus;

    private Integer teaId;

    private Integer teaGroupId;

    private List<Integer> ruleIds;

}
