package cn.edu.zut.soft.graduate.core.vo;

import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class UserDescVO {


    private Identity identity;

    private List<Info> infoList;

    /**
     * 课题 {@link Issue}
     */
    private Issue issue;

    /**
     * 任务列表 {@link Task}
     */
    private Task task;

    public UserDescVO() {
        this.infoList = new ArrayList<>();
    }
}
