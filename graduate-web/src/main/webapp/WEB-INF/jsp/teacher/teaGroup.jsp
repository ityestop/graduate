
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<html>
<head>

    <title>教师答辩小组</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">小组信息</li>
        </ol>
        <div class="page-header">
            <h3>小组信息</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
    
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>
                        <small>教师工号</small>
                    </th>
                    <th>
                        <small>姓名</small>
                    </th>
                    <th>
                        <small>性别</small>
                    </th>
                    <th>
                        <small>教师职称</small>
                    </th>
                    <th>
                        <small>联系方式</small>
                    </th>
                    <th>
                        <small>学生人数</small>
                    </th>
                    <th>
                    	<small>小组身份</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${list}" var="t">
                <tr>
                <input type="hidden" class="teaId" value="${t.teaId }">
                <input type="hidden" class="teaName" value="${t.teaName }">
                <input type="hidden" class="tgId" value="${t.teaGroupId }">
                <td><small>${t.teaTeacherId}</small></td>
                <td><small>${t.teaName }</small></td>
                <td><small>${t.teaSex ? "女" : "男"}</small></td>
                 <c:choose>
                            <c:when test="${t.teaQualifications eq 0}">
                                <td>
                                    <small>无</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 1}">
                                <td>
                                    <small>助教</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 2}">
                                <td>
                                    <small>讲师</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 3}">
                                <td>
                                    <small>副教授</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 4}">
                                <td>
                                    <small>教授</small>
                                </td>
                            </c:when>

                        </c:choose>
                <td><small>${t.teaTell }</small></td>
                   <c:choose>
                            <c:when test="${t.stuAll eq 0}">
                                <td>
                                    <small>无</small>
                                </td>
                            </c:when>
                            <c:when test="${t.stuAll > 0}">
                                <td>
                                    <small>${ t.stuAll}</small>
                                </td>
                            </c:when>
                        </c:choose>
                <c:choose>
                            <c:when test="${t.teaIdentity eq 0}">
                                <td>
                                    <small>成员</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaIdentity eq 1}">
                                <td>
                                    <small>秘书</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaIdentity eq 2}">
                                <td>
                                    <small>组长</small>
                                </td>
                            </c:when>
                        </c:choose>
                </tr>
                </c:forEach>
            </table>
        </div>
        
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
</div>
<script>
    $('#assignIndetity').on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
       	alert($p.html()); 
       	var $teaId=$p.find(".teaId");
       	var $teaName=$p.find(".teaName");
       	var $tgId=$p.find(".tgId");
       	var $modal = $(this);
        $modal.find("input[name='teaName']").val($teaName.val());
        $modal.find("input[name='teaId']").val($teaId.val());
        $modal.find("input[name='tgId']").val($teaId.val());
        alert($tgId.val());
    });
</script>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
