//package cn.edu.zut.soft.graduate.service;
//
//
//import cn.edu.zut.soft.graduate.model.HTopic;
//import cn.edu.zut.soft.graduate.model.HistoryFormatIssue;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
///**
// * <p>文件名称：HTopicService.java</p>
// * <p>文件描述：</p>
// * <p>版权所有： 版权所有(C)2011-2099</p>
// * <p>公   司： 口袋购物 </p>
// * <p>内容摘要： </p>
// * <p>其他说明： </p>
// * <p>完成日期：16/10/29 上午12:32</p>
// *
// * @author yangxiaotian@weidian.com
// * @version 1.0
// */
//public interface HTopicService {
//
//    public Integer save(HTopic hTopic);
//
//    public Integer update(HTopic hTopic);
//
//    public boolean delteTopic(Integer id);
//
//    public Integer count();
//
//    public List<HTopic> findAll();
//
//    public List<HTopic> selectByTeaName(String teaName);
//
//    public HTopic selectById(Integer id);
//
//}
