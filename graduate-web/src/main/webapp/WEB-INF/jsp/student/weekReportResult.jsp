<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>周月报详情</title>
<script type="text/javascript" src="IAssets/js/selectTopic.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a>
				</li>
				<li class="active">周月报详情</li>
			</ol>
			<div class="page-header">
				<h3>周月报详情</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<table class="table">
					<tbody>
						<tr>
							<td width="10%"><small>周月报标题:</small>
							</td>
							<td><small>${info.title}</small>
							</td>
						</tr>
						<tr>
							<td><small>类型:</small>
							</td>
							<td><c:if test="${info.type eq 1 }">
									<small>周报</small>
								</c:if> <c:if test="${info.type eq 2 }">
									<small>月报</small>
								</c:if>
							</td>
						</tr>
						<tr>
							<td><small>周/月次:</small>
							</td>
							<td><small> ${info.number}</small>
							</td>
						</tr>

						<tr>
							<td><small>本周进度:</small>
							</td>
							<td><small>${info.nowContext}</small>
							</td>
						</tr>
						<tr>
							<td><small>下周计划:</small>
							</td>
							<td><small> ${info.nextContext}</small>
							</td>
						</tr>
						<tr>
							<td><small>成绩：</small>
							</td>
							<td><small> <c:if test="${info.value eq 90 }">A</c:if>
									<c:if test="${info.value eq 80 }">B</c:if> <c:if
										test="${info.value eq 70 }">C</c:if> <c:if
										test="${info.value eq 60 }">D</c:if> <c:if
										test="${info.value eq 0 }">0</c:if> <font color="red">
										<c:if test="${info.status eq 2}">老师未审核</c:if> <c:if
											test="${info.status eq 3}">老师已审核</c:if> </font> </small>
							</td>
						</tr>
						<c:choose>
							<c:when test="${info.common ne null}">
								<tr>
									<td><small>指导教师评语：</small>
									</td>
									<td><small>${info.common }</small>
									</td>
								</tr>
							</c:when>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="form-group">
				<div class="col-sm-9"></div>
				<div class="col-sm-2">
					<button type="button" class="btn btn-blue btn-block"
						onclick="window.location.href='javascript:history.go(-1);'">
						<i class="clip-arrow-right-2 "></i> 返回
					</button>
				</div>

			</div>
		</div>
	</div>
	</div>
</body>
</html>
