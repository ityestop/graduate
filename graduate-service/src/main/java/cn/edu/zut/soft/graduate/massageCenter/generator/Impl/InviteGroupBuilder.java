package cn.edu.zut.soft.graduate.massageCenter.generator.Impl;

import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.Impl.Message;
import cn.edu.zut.soft.graduate.massageCenter.generator.Builder;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author chuchuang
 * @date 16/11/21
 */
@Component
public class InviteGroupBuilder implements Builder {

//    @Value("#{commonProp.createGroupTem}")
    private static final String createGroupTem = "亲爱的 %s,\n\t我是 %s 班的 %s,诚挚的邀请你加入我的小组。联系方式: %s。\n\t<a href='%s'>去查看</a>";

    private static final String createGroupTitle = "【组队邀请】%s 发给 %s 的组队邀请";

    private static final String Url = "/stu/toInvitnte?groupId=%s";

    @Autowired
    private InfoBO infoBO;

    @Override
    public Message assemblyMessage(Identity addressor, Identity recipients) {
        Assert.notNull(addressor, "addressor is null");
        Assert.notNull(recipients, "recipients is null");
        Message message = new Message();
        List<Info> infoList = infoBO.queryByUserInfoStrategy(addressor.getId(), Arrays.<InfoStrategy>asList(new BasicInfoStrategy(InfoType.Basic, IKEY.CLAZZ, ""), new BasicInfoStrategy(InfoType.Group, IKEY.GROUP, "")));
        Map<String, Info> map = new HashMap<>();
        for (Info i : infoList) {
            map.put(i.getType() + i.getKey(), i);
        }
        Assert.notNull(map.get(InfoType.Basic.name() + IKEY.CLAZZ), "班级不存在");
        Assert.notNull(map.get(InfoType.Group.name() + IKEY.GROUP), "邀请者小组不存在");
        message.setContent(String.format(createGroupTem, recipients.getName(), infoList.get(0).getValue(), addressor.getName(), addressor.getPhone(), String.format(Url, map.get(InfoType.Group.name() + IKEY.GROUP).getValue())));
        message.setAddressor(addressor.getId());
        message.setRecipients(recipients.getId());
        message.setTitle(String.format(createGroupTitle,addressor.getName(),recipients.getName()));
        message.setRead(0);
        message.setAddressorName(addressor.getName());
        message.setRecipientsName(recipients.getName());
        return message;
    }
}
