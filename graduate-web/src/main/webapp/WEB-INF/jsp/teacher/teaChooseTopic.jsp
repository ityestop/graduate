犆a<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<title></title>
<script type="text/javascript" src="IAssets/js/topoclist.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">专家审批</li>
			</ol>
			<div class="page-header">
				<h3>专家审批 <small>可以对管理员分配的题目进行操作</small> </h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
				<div class="panel-body">
					<table id="sample-table-1" class="table table-hover"style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all" >
						<thead>
							<tr>
								<th width="20%"><small>课题名称</small></th>
								<th><small>课题类型</small></th>
								<th><small>课题来源</small></th>
								<th><small>人数</small></th>
								<th><small>申报教师</small></th>
								<th width="16%" class="center"><small>申报时间</small></th>
								<th><small>课题状态</small></th>
								<th width="18%"><small>操作</small></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${teacherAndTopic}" var="topic">
								<tr>
									<td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">
									<a
										href="./${sessionScope.pathCode}/topicDetails.do?id=${topic.topId}" title="${topic.topTitle}"><small>${topic.topTitle}</small></a>
									</td>
									<td> <small>${topic.topType }</small></td>
									<c:choose>
										<c:when test="${topic.topTopicType eq false}">
											<td><small>社会服务</small></td>
										</c:when>
										<c:when test="${topic.topTopicType eq true}">
											<td><small>科研项目</small></td>
										</c:when>
									</c:choose>

									<td><small>  ${topic.topCount }</small></td>
									<td><small>${topic.teacher.teaName }</small></td>
									<td><small><fmt:formatDate value="${topic.topCreateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></small></td>

									<c:choose>
										<c:when test="${topic.topStatus eq 0 }">
											<td><small class="badge badge-info ">已报满</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 2 }">
											<td><small class="badge badge-success ">选报中</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 1 }">
											<td ><small class="badge badge-warning">审核中</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 3 }">
											<td><small class="badge badge-danger">未通过</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 4 }">
											<td><small class="badge badge-default ">未提交</small></td>
										</c:when>
									</c:choose>
									<td >
									<c:choose>
										<c:when test="${topic.status.index eq 1 or topic.status.index eq 2}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
										<a href="./${sessionScope.pathCode}/topicDetails.do?id=${topic.topId}">
										<i class=" clip-pencil-3 "></i><small>查看详情</small>
										</a>
										</c:when>
										</c:choose>
									  </div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

		</div>
	</div>

</body>
</html>
