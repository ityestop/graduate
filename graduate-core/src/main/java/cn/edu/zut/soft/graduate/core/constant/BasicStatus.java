package cn.edu.zut.soft.graduate.core.constant;

/**
 * 注释:
 * 基本状态
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
public enum BasicStatus {
    ING,YES,NO
}
