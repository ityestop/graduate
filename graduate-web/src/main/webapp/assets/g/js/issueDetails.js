/**
 * Created by cc on 2016/10/27.
 */
document.write("<script src='/assets/g/js/config.js'></script>");
function findPhone(id) {
    $.ajax({
        url:"/identity/id/"+id,
        dataType:"json",
        success:function (res) {
            if(res.success){
                alert("手机号: "+res.data.phone);
            }else {
                alert(res.errorMsg);
            }
        }
    });
}
$(function () {
    $("#chooseBtn").hide();
    var id = getQueryString("id");
    var $data = $(".issue-value");
    var data = {};
    $.ajax({
        url: "/issue/" + id,
        dataType: "json",
        type: "get",
        success: function (result) {
            if (result.success) {
                data = result.data;
                $("#issueTitle").html(data.issue.title);
                $("#issueTeacher").html(data.issue.schoolTeaName+"(<a href='javascript:findPhone("+data.issue.schoolTeaId+")'>查看手机</a>)");
                $("#issueType").html(data.issue.type);
                $("#issueSource").html(sourceMaps().issueSourceMap[data.issue.source]);
                $("#issueTaskSize").html(data.issue.taskSize);
                $("#issueKind").html(sourceMaps().issueKindMap[data.issue.kind]);
                var $technology = $("#issueTechnology");
                $.each(JSON.parse(data.issue.technology), function (i, node) {
                    $technology.append(node.title + "、");
                });
                $("#issueBackground").html(data.issueContent.background);
                var $issueTaskList = $("#issueTaskList");
                $.each(data.issueContent.taskList, function (i, node) {
                    $issueTaskList.append("<h5>" + node.title + "</h5><pre>" + node.content + "</pre>");
                });
            } else {
                alert(result.errorMsg);
            }
        }
    });
    $.ajax({
        type:'post',
        dataType: 'json',
        url:'/stu/stuChooseRule',
        data:{
            issueId:id,
        },
        success:function (result) {
            if(result.success){
                $("#chooseBtn").show();
            }
        }
    });
    
    $("#stu_Choose_btn").click(function () {
        $.ajax({
            type:'post',
            dataType:'json',
            url:'/stu/stuChooseIssue',
            data:{
                issueId:id,
            },
            success:function (result) {
                if(result.success){
                    window.location="/stu/stuGroup";
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    })
});
