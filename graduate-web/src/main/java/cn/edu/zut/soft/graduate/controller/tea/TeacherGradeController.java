package cn.edu.zut.soft.graduate.controller.tea;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.core.constant.GradeType;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicGradeStrategy;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Grade;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.gradesCenter.bo.GradeBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>文件名称：TeacherGradeController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/3 下午5:04</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Controller
@RequestMapping("/tea")
public class TeacherGradeController extends BaseController {

    @Resource
    private GradeBO gradeBO;

    @Resource
    private InfoBO infoBO;

    @RequestMapping("/topOpenGrade")
    public ModelAndView toOpenGrade() {
        return new ModelAndView("/tea/openGrade");
    }

    @RequestMapping("/interimGrade")
    public ModelAndView interimGrade() {
        return new ModelAndView("/tea/interimGrade");
    }

    @RequestMapping("/systemGrade")
    public ModelAndView systemGrade() {
        return new ModelAndView("/tea/systemGrade");
    }

    @RequestMapping("/guideGrade")
    public ModelAndView guideGrade() {
        return new ModelAndView("/tea/guideGrade");
    }

    @RequestMapping("/replyGrade")
    public ModelAndView replyGrade() {
        return new ModelAndView("/tea/replyGrade");
    }

}
