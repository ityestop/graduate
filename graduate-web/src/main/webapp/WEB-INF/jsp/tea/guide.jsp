<%--suppress JSDuplicatedDeclaration --%>
<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教师任务列表</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table{
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">指导学生列表</li>
        </ol>
        <div class="page-header">
            <h3>指导学生列表
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <%--<form class="form-inline" action="/tea/guide" method="get">--%>
            <%--<div class="form-group">--%>
            <%--<input class="form-control" style="width: 300px" title="学生姓名" placeholder="学生姓名" name="name"--%>
            <%--value="${param.name}"/>--%>
            <%--</div>--%>
            <%--<div class="form-group" style="text-align: right;">--%>
            <%--<button type="submit" class="btn btn-primary">检索</button>--%>
            <%--</div>--%>
            <%--</form>--%>
        </div>
        <div class="col-sm-12">
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="chooseChTopic" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="chName">子任务分配</h4>
            </div>
            <input type="hidden" id="stuId" name="id"/>
            <input type="hidden" id="topId" name="topId"/>
            <div class="modal-body" id="childTopic">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="assignChild_submit">提交
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>
        </div>
    </div>
</div>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->


<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">
    var $id = "";
    function distributeChTopic(cId, cIdentityId) {
        $id = cId;
        $identityId = cIdentityId;
        $("#topId").attr("value", $id);
        $("#stuId").attr("value", $identityId);
        distributeChTopi();
    }
    function distributeChTopi() {
        if ($id != null) {
            $.getJSON("/issue/" + $id, function (result) {
                if (result.success) {
                    succ(result, $identityId);
                } else {
                    err(result);
                }
            });
        }
    }
    function params() {
        return {
            value: ${user.id},
            infoType: 'Tea',
            key: 'guide'
        }
    }

    var columns = [
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'identity.phone',
            title: '手机'
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'topicName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            },
            sortStable: true
        }, {
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        },{
            title: '是/否分配',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "未分配";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicId') {
                        str = "已分配";
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var strid = "";
                var stridentityId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'topicId') {
                        strid += array[i].value;
                        stridentityId += array[i].identityId;
                        break;
                    }
                }
                var $result = "<a class='btn btn-primary btn-sm' data-toggle='modal' data-target='#chooseChTopic' " +
                        "onclick='distributeChTopic(" + strid + "," + stridentityId + ")'>分配子任务</a>";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicId' && array[i].value != null ) {
                        $result = '<a href="/issue/restChildTop?topId='+strid+'&&childId='+array[i].value+'&&stuId='+stridentityId+'"> 重置子标题</a>';
                        break;
                    }
                }
                return $result;
            }
        }

    ];
    customSearch({
        id: "#table",
        url: "/identity/info/select",
        dataType: "json",
        params: params(),
        columns: columns,
        pageSize: 30
    });

    function succ(result, identityId) {
        var data = result.data;
        var $issueTaskList = $("#childTopic");
        $issueTaskList.empty();
        if(result.data.issue.status == "finish"){
            $.each(data.issueContent.taskList, function (i, node) {
                var str = '';
                str += "<div><label> <input type='radio' value='" + node.nid + "' name='childId' class='grey' >" + node.title + "</label>";
                str += "<div class='col-sm-12'> <textarea id='editor_id' style='width:100%;height:150px;' disabled='disabled'>" + node.content + "</textarea></div> </div>"
                if (node.isAssign != null) {
                    str = '';
                }
                $issueTaskList.append(str);

            });
        }else{
            $issueTaskList.html("该题目状态未至-选拔成功,不能进行子标题分配!");
            $("#assignChild_submit").hide();
        }
    }

    function err(data) {
        alert(data.errorMsg);
    }
    $("#assignChild_submit").click(function () {
        var $childId = $("input[name='childId']:checked").val();
        var $topId = $("#topId").val();
        var $stuId = $("#stuId").val();

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/issue/assignChildTop',
            data: {
                stuId: $stuId,
                topId: $topId,
                childId: $childId
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }
        });


    });


</script>
</body>
</html>
