package cn.edu.zut.soft.graduate.reportCenter.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Report;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by chuchuang on 16/10/21.
 */
public interface ReportDAO<T extends Report> extends BasicDAO<Integer,Report> {

    int updateByExampleWithBLOBs(@Param("record") T record, @Param("example") Example example);

    List<T> selectByExampleWithBLOBs(Example example);

    int updateByPrimaryKeyWithBLOBs(T record);
}
