package cn.edu.zut.soft.graduate.userCenter.bo.Impl;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.bo.TeacherBO;
import cn.edu.zut.soft.graduate.userCenter.opjo.Teacher;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author chuchuang
 * @date 16/12/5
 */
@Component
@Slf4j
public class TeacherBOImpl implements TeacherBO {

    @Autowired
    private IdentityBO identityBO;

    @Autowired
    private InfoBO infoBO;

    @Override
    public CommonResult<List<Teacher>> queryTeaOfStuName(IdentityQuery identityQuery) {
        Assert.notNull(identityQuery);
        identityQuery.setRole(Role.tea);
        CommonResult<List<Identity>> result = identityBO.findByQuery(identityQuery);
        if (CollectionUtils.isEmpty(result.getData())) {
            return CommonResult.successReturn(Collections.<Teacher>emptyList());
        }
        //设定检索的长度
        identityQuery.setCount(result.getTotalCount().longValue());

        List<Identity> identityList = result.getData();
        Map<Integer, Teacher> teacherMap = new HashMap<>(identityList.size());
        Teacher teacher;
        InfoQuery infoQuery = new InfoQuery();
        infoQuery.clear();
        List<String> valueList = new ArrayList<>(result.getData().size());
        for (Identity identity : identityList) {
            valueList.add(identity.getId().toString());
            teacher = new Teacher();
            teacher.setIdentity(identity);
            teacherMap.put(identity.getId(), teacher);
        }
        infoQuery.setValueList(valueList);
        infoQuery.setInfoType(InfoType.Tea);
        infoQuery.setKey(IKEY.Guide);
        //检索所有的教师学生
        List<Info> infoList = infoBO.findByQuery(infoQuery).getData();

        infoQuery = new InfoQuery();
        infoQuery.clear();
        infoQuery.setInfoType(InfoType.Positive);
        infoQuery.setKey(IKEY.Petitioner);
        infoQuery.setValueList(valueList);
        //检索所有申报校外课题的学生
        List<Info> infoList1 = infoBO.findByQuery(infoQuery).getData();
        //合并
        infoList.addAll(infoList1);


        for (Info info : infoList) {
            teacher = teacherMap.get(Integer.parseInt(info.getValue()));
            if (InfoType.Tea.name().equals(info.getType()) && IKEY.Guide.equals(info.getKey())) {
                teacher.addAllStuNum();
                continue;
            }
            if (info.getType().equals(InfoType.Positive.name()) && IKEY.Petitioner.equals(info.getKey())) {
                teacher.addCompanyStuNum();
            }
        }

        List<Teacher> teacherList = new ArrayList<>(teacherMap.size());
        for (Map.Entry<Integer, Teacher> me : teacherMap.entrySet()) {
            me.getValue().AllSubtractCompanyNum();
            teacherList.add(me.getValue());
        }

        return CommonResult.successReturn(teacherList, result.getTotalCount());
    }

    /**
     * 全部未分组学生数不为零的教师列表，
     */
    @Override
    public CommonResult<List<Identity>> queryNoGroupAndNoStuOfTea(IdentityQuery identityQuery) {
        //检索所有的指导教师列表
        //检索教师存在小组的名单,剔除

        List<Info> teaList = infoBO.queryByInfoStrategy(Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.Tea, IKEY.Guide, null)));
        Set<Integer> teaIds = new HashSet<>();
        CollectionUtils.collect(teaList, new Transformer() {
            @Override
            public Object transform(Object o) {
                return Integer.parseInt(((Info) o).getValue());
            }
        }, teaIds);
        InfoQuery infoQuery = new InfoQuery();
        infoQuery.setInfoType(InfoType.Group);
        infoQuery.setKey(IKEY.GROUP);
        infoQuery.setRole(Role.tea);
        infoQuery.clear();
        List<Info> teaGroup = infoBO.findByQuery(infoQuery).getData();
        for (Info info : teaGroup) {
            teaIds.remove(info.getIdentityId());
        }
        //查找教师

        identityQuery.setIds(new ArrayList<>(teaIds));
        return identityBO.findByQuery(identityQuery);
    }

    @Override
    public CommonResult<Map<Integer, List<Info>>> queryExistStudentOfTea(IdentityQuery identityQuery) {
        CommonResult<List<Identity>> teaResult = identityBO.queryAllTea(identityQuery);
        if (teaResult.getTotalCount() == 0) {
            log.info("检索教师数为0");
            return CommonResult.successReturn(Collections.<Integer, List<Info>>emptyMap());
        }
        List<Info> guideAll = infoBO.queryByInfoStrategy(Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.Tea, IKEY.Guide, null)));
        List<Identity> teaAll = teaResult.getData();
        Set<Integer> existStuTeaAllSet = new HashSet<>(50);
        for (Identity tea : teaAll) {
            existStuTeaAllSet.add(tea.getId());
        }
        Map<Integer, List<Info>> teaIdAndStuMap = new HashMap<>(existStuTeaAllSet.size());
        for (Info info : guideAll) {
            Integer teaId = new Integer(info.getValue());
            if (existStuTeaAllSet.contains(teaId)) {
                if (teaIdAndStuMap.containsKey(teaId)) {
                    teaIdAndStuMap.get(teaId).add(info);
                } else {
                    //asList 返回的是ArrayList
                    List<Info> infoList = new ArrayList<>();
                    infoList.add(info);
                    teaIdAndStuMap.put(teaId, infoList);
                }
            }
        }
        //教师以及所对应的关系可以采用迭代器直接输出的
        return CommonResult.successReturn(teaIdAndStuMap);
    }



}
