<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>软件学院毕业设计管理平台</title>
</head>

<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
               <li class="active">权限服务</li>
        </ol>
        <div class="page-header">
            <h2>
                权限服务
            </h2>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<div class="row">
    <div class="col-md-12">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                      	<h4><font color="red">您好，现在已经过了申报时间，若有个别需要请联系管理员！</font></h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: GENERAL PANEL -->
    </div>
</div>
</body>
</html>
