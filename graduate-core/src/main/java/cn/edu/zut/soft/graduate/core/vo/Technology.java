package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class Technology {

    private String id;
    private String title;

    public Technology(){

    }

    public Technology(String id,String title){
        this.id = id;
        this.title = title;
    }

}
