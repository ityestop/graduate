package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InfoQuery extends Query {

    private InfoType infoType;

    private String key;

    private String value;

    private Role role;

    private Integer identityId;

    private Integer del;

    private List<String> valueList;

}
