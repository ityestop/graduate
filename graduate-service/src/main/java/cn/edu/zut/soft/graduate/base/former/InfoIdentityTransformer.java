package cn.edu.zut.soft.graduate.base.former;

import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import org.apache.commons.collections.Transformer;
import org.springframework.stereotype.Component;

/**
 * @author cc
 * @Date 2016/12/13
 * @value 1.0
 */
@Component
public class InfoIdentityTransformer implements Transformer {

    @Override
    public Object transform(Object o) {
        return ((Info)o).getIdentityId();
    }
}
