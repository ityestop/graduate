package cn.edu.zut.soft.graduate.controller.tea;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cc
 * @Date 2017/2/22
 * @value 1.0
 */
@Controller
@RequestMapping("/tea/report")
public class TeacherReportController {

    @RequestMapping({"","list"})
    private String reportList(){
        return "/tea/report/reportList";
    }

    @RequestMapping("/data")
    private String getReportData(){
        return "/tea/report/reportData";
    }

}
