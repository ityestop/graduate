package cn.edu.zut.soft.graduate.base.utils;

import com.alibaba.fastjson.JSONObject;

import java.io.*;

/**
 * 注释:
 *
 * @author chuchuang
 * @Date 2017/3/26 下午7:51
 * @value 1.0
 */
public class RandomFileUtil {

    public static void writerFile(File file, JSONObject output) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(FormatUtil.formatJson(output.toJSONString()));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void readFile(File file, StringBuilder sb) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String l;
            while ((l = reader.readLine()) != null){
                sb.append(l);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
