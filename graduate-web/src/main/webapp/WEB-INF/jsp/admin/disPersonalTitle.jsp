<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<html>
<head>
    <title>分配自拟题目</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!--   <script type="text/javascript" src="./IAssets/js/toNewTopic.js"></script> -->
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">教师分配自拟题目</li>
        </ol>
        <div class="page-header">
            <h3>教师分配自拟题目</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
      
       <div class="panel-body">
        <div class="row"></div>
        <div class="panel-body">
			 <form action="./${sessionScope.pathCode}/submitDisPersonal.do" method="post">
			 <input type="hidden" name="teaId" value="${teaId}">
            <table class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                	<th><small>
                            选项
                        </small></th>
                    <th width="20%" class="center">
                        <small>题目名称</small>
                    </th>
                    <th>
                        <small>题目来源</small>
                    </th>
                    <th>
                        <small>题目类型</small>
                    </th>
                    <th>
                        <small>题目状态</small>
                    </th>
                    <th>
                        <small>校外教师</small>
                    </th>
                    <th>
                        <small>校内教师</small>
                    </th>
                    <th>
                        <small>题目人数</small>
                    </th>
                    <th width="15%">
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${topicList}" var="topic">
                    <tbody>
                    <tr>
                    <th>
                            <small>
                                <label>
                                    <input type="checkbox" name="topId" value="${topic.topId}">
                                </label>
                            </small>
                        </th>
                        <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${topic.topTitle}">
                            <a href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topic.topId}"
                               title="${topic.topTitle}"></small>${topic.topTitle}</a>
                        </td>
                        <c:choose>
                            <c:when test="${ topic.topTopicType eq false}">
                                <td>
                                    <small>社会服务</small>
                                </td>
                            </c:when>
                            <c:when test="${ topic.topTopicType eq true}">
                                <td>
                                    <small>科研项目</small>
                                </td>
                            </c:when>

                        </c:choose>

                        <td>
                            <small>${topic.topType}</small>
                        </td>
                       <c:choose>
										<c:when test="${topic.topStatus eq 0 }">
											<td><small class="label label-danger"  style="font-size: 75% !important;">已报满</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 2 }">
											<td><small class="label label-success" style="font-size: 75% !important;">选报中</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 1 }">
											<td ><small class="label label-warning" style="font-size: 75% !important;">审核中</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 3 }">
											<td><small class="label label-inverse " style="font-size: 75% !important;">未通过</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 4 }">
											<td><small class=" 	label label-default" style="font-size: 75% !important;">未提交</small></td>
										</c:when>
									</c:choose>

                        <c:choose>
                            <c:when test="${topic.topOutTeacher eq null }">
                                <td>
                                    无
                                </td>
                            </c:when>
                            <c:when test="${topic.topOutTeacher ne null }">
                                <td>
                                    <small> ${ topic.topOutTeacher}</small>
                                </td>
                            </c:when>
                        </c:choose>
                        <td>
                            <small>${topic.teacher.teaName }</small>
                        </td>
                        <td>
                            <small>${topic.topCount }</small>
                        </td>
                        <td>
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topic.topId}">
                                    <i class="fa fa-info-circle"></i>
                                    <small>查看详情</small>
                                </a>
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="col-sm-2" style="text-align: left;">
                <button type="submit" class="btn btn-blue btn-block" >提交</button>
				</div>
            </form>
        </div>
         <%--<div class="form-group">--%>
        <%--<label class="col-sm-4 "> </label>--%>

        <%--<div class="col-sm-8" style="text-align: right; height: 40px">--%>
            <%--<page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"--%>
                              <%--curPage="${pageNum}" formId="condition"/>--%>
        <%--</div>--%>
    </div>
        	
    </div>
</div>
<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();

    });
    //    $(function(){
    //        var $eventSelect = $("#status");
    //        $eventSelect.on("change", function (e) {
    //            alert("12312412");
    //        });
    //    });
</script>


</body>
</html>

