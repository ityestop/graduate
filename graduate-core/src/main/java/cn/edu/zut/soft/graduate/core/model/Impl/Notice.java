package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Notice extends BaseModelImpl {

    private String title;

    private String owner;

    private String tag;

    private Integer readNum;

    private String content;

    private String role;

}