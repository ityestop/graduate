<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>软件学院毕业设计管理平台</title>
</head>

<body>

	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">周月报管理</li>
				<li class="active">第${weekCount}周</li>
			</ol>
			<div class="page-header">
				<h3>
					周月报管理 <small><font color="red">当前周-第${weekCount}周</font> </small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<!-- start: GENERAL PANEL -->
			<div class="panel">
				<div class="panel-body">
					<div class="col-sm-3">
						<c:choose>
							<c:when test="${weekBoolean eq false}">
								<a class="btn btn-primary"
									href="./${sessionScope.pathCode}/toWeekReport.do?type=1&num=0"><i
									class=" clip-file-plus  "></i> 添加周报</a>
							</c:when>
							<c:when test="${weekBoolean eq true }">
								<a class="btn btn-primary" href="javaScript:void(0);"
									data-toggle="modal" data-target="#WeekOrMonth"><i
									class=" clip-file-plus  "></i> 添加周报</a>
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${monthBoolean eq false}">
								<a class="btn btn-primary"
									href="./${sessionScope.pathCode}/toWeekReport.do?type=2&num=0"><i
									class="  clip-file-plus "></i> 添加月报</a>
							</c:when>
							<c:when test="${monthBoolean eq true }">
								<a class="btn btn-primary" href="javaScript:void(0);"
									data-toggle="modal" data-target="#WeekOrMonth"><i
									class="  clip-file-plus  "></i> 添加月报</a>
							</c:when>
						</c:choose>
					</div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>编号</th>
								<th>名称</th>
								<th>周/月次</th>
								<th>类型</th>
								<th>操作</th>
							</tr>
						</thead>
						<c:forEach items="${weekList}" var="week" varStatus="i">
							<c:if
								test="${week.type eq 1  and week.number > 3 and week.number < 13 or week.type eq 2 and week.number > 2 and week.number < 6}">
								<tbody>


									<tr>
										<td><small>${i.index+1}</small></td>
										<td><small><a
												href="./${sessionScope.pathCode}/showReport.do?id=${week.id}">${week.title}</a>
										</small></td>

										<td><c:if test="${week.type eq 1 }">
												<small>${week.number}周</small>
											</c:if> <c:if test="${week.type eq 2 }">
												<small>${week.number}月</small>
											</c:if></td>
										<td><c:if test="${week.type eq 1 }">
												<small>周报</small>
											</c:if> <c:if test="${week.type eq 2 }">
												<small>月报</small>
											</c:if></td>
										<td><c:if test="${week.id ne null }">
												<c:choose>
													<c:when test="${week.status eq 1 }">
														<small><a
															href="./${sessionScope.pathCode}/editReport.do?id=${week.id}">编辑</a>
															<c:if test="${week.title ne null }">
																<a
																	href="./${sessionScope.pathCode}/submitReport.do?id=${week.id}">提交</a>
															</c:if> </small>
													</c:when>
													<c:when test="${week.status eq 2 }">
														<small class="label label-success"
															style="font-size: 75% !important;">已提交</small>
													</c:when>
													<c:when test="${week.status eq 3 }">
														<small class="label label-danger"
															style="font-size: 75% !important;">已批阅</small>
													</c:when>
												</c:choose>
											</c:if> <c:if test="${week.id eq null }">
												<c:choose>
													<c:when
														test="${week.type eq 1 and sessionScope.functionMap['reportSubmit'].status eq true }">
														<small> <a
															href="./${sessionScope.pathCode}/toWeekReport.do?type=3&num=${week.number}">
																补交周报</a>
														</small>
													</c:when>

													<c:when
														test="${week.type eq 2 and sessionScope.functionMap['reportSubmit'].status eq true }">
														<small> <a
															href="./${sessionScope.pathCode}/toWeekReport.do?type=4&num=${week.number}">
																补交月报</a>
														</small>
													</c:when>
												</c:choose>
											</c:if></td>
									</tr>

								</tbody>
							</c:if>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="WeekOrMonth" tabindex="-1" role="dialog"
		aria-labelledby="CSGModalLab">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="CSGModalLab">周/月报提示</h4>
				</div>
				<div class="modal-body">您已经存在本周周报或者已经存在本月月报！</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
