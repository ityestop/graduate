<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>教师申报题目</title>
    <%--<script charset="utf-8" src="assets/plugins/kindeditor/kindeditor.js"></script>--%>
    <%--<script charset="utf-8" src="assets/plugins/kindeditor/lang/zh_CN.js"></script>--%>
    <%--<script charset="utf-8" src="IAssets/js/toNewTopic.js"></script>--%>
    <link rel="stylesheet" href="/assets/plugins/select2/select2.css">
    <%--<script src="/assets/g/js/source.js"></script>--%>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./tea"> 首页 </a></li>
            <li class="active">题目编辑</li>
        </ol>
        <div class="page-header">
            <h3>题目编辑
                <small>请填写项目基本信息</small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="row">
                    <div class="form-group" id="issueIdDiv" style="display: none;">
                        <label class="col-sm-2 control-label">
                            题目编号
                        </label>
                        <div class="col-sm-7">
                            <input name="issueId" type="number" placeholder="题目编号" class="form-control"
                                   maxlength="4" value="${param.issueId}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            题目名称 </label>
                        <div class="col-sm-7">
                            <input name="issueTitle" type="text" placeholder="课题名称" class="form-control"
                                   maxlength="25" value="">
                        </div>
                        <span class="help-inline col-sm-3">
                            <i class="fa fa-info-circle"></i>
                            <font color="red">毕业设计题目与任意子标题长度的<font color="blue">组合</font>均不能超过25字</font>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            学生人数/子标题数 </label>

                        <div class="col-sm-2">
                            <select name="taskSize" class="form-control" ${(param.op != 'new' && param.issueId != '') ? "disabled='disabled'":""}>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                        <span class="help-inline col-sm-3"> <i
                                class="fa fa-info-circle"></i><font color="red">请选择学生人数</font> </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-select-3">
                            课题来源 </label>
                        <div class="col-sm-2">
                            <select name="issueKind" id="form-field-select-3" class="form-control">
                                <option value="Social">社会服务</option>
                                <option value="Research">科研项目</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            课题类型 </label>
                        <div class="col-sm-2">
                            <select id="issueTypeSelect" class="form-control" title="课题类型">
                                <option value="设计">设计</option>
                                <option value="研究">研究</option>
                                <option value="其他">其他</option>
                            </select>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" name="issueType" class="form-control" style="display: none;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            技术领域 </label>
                        <div class="col-sm-7" id="jsly">
                        </div>
                        <span class="help-inline col-sm-3">
                            <i class="fa fa-info-circle"></i>
                            <font color="red">若此项为空将不能保存项目信息！</font>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jsly">
                            项目背景 </label>

                        <div class="col-sm-7">
                            <textarea name='issueBackground' style="height: 150px;"
                                      placeholder="此处为项目背景，包含题目背景说明、设计内容说明，学生详细任务请写入下方任务分工"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="page-header">
                        <h3>任务详情
                            <small>请填写项目子标题</small>
                        </h3>
                    </div>
                    <div id="group_rw">

                    </div>

                    <div class="from-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-4">
                            <div class="col-sm-6">
                                <submit class="btn btn-blue btn-block">
                                    <i class="clip-checkmark-2"></i> 保存
                                </submit>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-blue btn-block"
                                        onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i> 返回
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end: GENERAL PANEL -->
    </div>
</div>
<script src="/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="/assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="/assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<%--<script src="/assets/js/form-elements.js"></script><!-- 4 -->--%>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
//        FormElements.init();
    });
</script>
<script src="assets/g/js/newIssue.js"></script>

</body>
</html>
