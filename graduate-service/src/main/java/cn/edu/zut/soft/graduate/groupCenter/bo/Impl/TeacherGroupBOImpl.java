package cn.edu.zut.soft.graduate.groupCenter.bo.Impl;

import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.transform.GroupTransformTeacherGroupVO;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.view.TeacherGroupVO;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.groupCenter.bo.TeacherGroupBO;
import cn.edu.zut.soft.graduate.groupCenter.dao.GroupDAO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
@Service
public class TeacherGroupBOImpl implements TeacherGroupBO {

    @Resource
    private GroupBO groupBO;

    @Resource
    private GroupDAO<Group> groupDAO;

    @Resource
    private InfoBO infoBO;

    @Resource
    private GroupTransformTeacherGroupVO groupTransformTeacherGroupVO;

    @Override
    public List<TeacherGroupVO> findByQuery(GroupQuery groupQuery) {

        List<Group> groupList = groupBO.findByQuery(groupQuery).getData();
        if (CollectionUtils.isEmpty(groupList)){
            return Collections.emptyList();
        }
        return (List<TeacherGroupVO>) CollectionUtils.collect(groupList, new Transformer<Group, TeacherGroupVO>() {
            @Override
            public TeacherGroupVO transform(Group group) {
                return GroupToTeacherGroupVO(group);
            }
        });
    }
    private TeacherGroupVO GroupToTeacherGroupVO(Group group) {
        TeacherGroupVO teacherGroupVO = groupTransformTeacherGroupVO.transform(group);
        if (group.getParallelismId() != null){
            teacherGroupVO.setParallelism(groupTransformTeacherGroupVO.transform(groupBO.get(group.getParallelismId())));
        }
        return teacherGroupVO;
    }

    public List<TeacherGroupVO> noParallelism(){
        Example example = new Example();
        Criteria criteria_one = example.createCriteria();
        criteria_one.andIsNull("parallelism_id").andEqualTo("del",0).andEqualTo("role","tea");
        example.or().andEqualTo("parallelism_id","").andEqualTo("del",0).andEqualTo("role","tea");
        List<Group> groupList = groupDAO.selectByExample(example);
        if (CollectionUtils.isEmpty(groupList)){
            return Collections.emptyList();
        }
        return (List<TeacherGroupVO>) CollectionUtils.collect(groupList,groupTransformTeacherGroupVO);
    }

    public boolean parallelism(Integer groupId,Integer parallelismId,String operator){
        if (groupId == null || parallelismId == null || StringUtils.isBlank(operator)){
            return false;
        }
        //两边都要更新
        Group group = new Group();
        group.setId(groupId);
        group.setParallelismId(parallelismId);
        groupBO.update(group,operator);
        return true;
    }

    @Override
    public TeacherGroupVO get(Integer id) {
        Group group = groupBO.get(id);
        if (group == null || !Role.tea.equals(group.getRole())){
            return null;
        }
        return GroupToTeacherGroupVO(group);
    }

    @Override
    public TeacherGroupVO getByTeacher(Integer id) {
        List<Info> infoList = infoBO.queryByUserInfoStrategy(id, Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.Group, IKEY.GROUP,null)));
        Integer groupId = null;
        if (CollectionUtils.isNotEmpty(infoList)){
            groupId = Integer.parseInt(infoList.get(0).getValue());
        }
        if (groupId == null){
            return null;
        }
        return get(groupId);
    }

    @Override
    public boolean restartParallelism(Integer groupId, String operator) {
        if (groupId == null || StringUtils.isBlank(operator)){
            return false;
        }
        int count = groupDAO.restartParallelismId(groupId,operator);
        return count != 0;
    }
}
