<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>软件学院毕业设计管理平台</title>
</head>

<body>

	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/homePage.do"> Home </a></li>
				<li class="active">校內題目</li>
			</ol>
			<div class="page-header">
				<h2>校內題目</h2>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<!-- start: GENERAL PANEL -->
			<div class="panel panel-default">
				<div class="panel-body">
					<!--  panel-scroll" style="height:215px" -->
					<form id="condition" role="form" class="form-horizontal" action=""
						method="post">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="teacherName">
								教师姓名 </label>
							<div class="col-xs-6">
								<select id="teacherName" class="form-control input-sm">
									<option value="0">全部</option>
								<c:forEach items="${teacherAllList}" var="teaList">
									<option value="${teaList.teaId }">${teaList.teaName}</option>
								</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="teacherStatus">
								课题状态 </label>
							<div class="col-xs-6">
								<select id="teacherStatus" class="form-control input-sm">
									<option value="">全部</option>
									<option value="0">已报满</option>
									<option value="2">选报中</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="topicType">
								课题类型 </label>
							<div class="col-xs-6">
								<select id="topicType" class="form-control input-sm">
									<option value="">全部</option>
									<option value="1">社会服务</option>
									<option value="0">科研项目</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">
								技术领域 </label>

							<div class="col-sm-7">
								<label class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label> <label
									class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label> <label
									class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label> <label
									class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label> <label
									class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label> <label
									class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label> <label
									class="checkbox-inline"> <input type="checkbox"
									value="" class="grey"> Checkbox 1 </label>
							</div>
						</div>
						<div class="from-group">
							<div class="col-sm-11" style="text-align:right;">
								<button type="button" class="btn btn-primary">检索</button>
							</div>
						</div>
					</form>
				</div>
				<div class="alert alert-info" style="height:40px">A powerful
					slider for selecting value ranges, supporting dates and more.</div>
				<div class="panel-body">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>题目名称</th>
								<th>题目类型</th>
								<th>题目来源</th>
								<th>指导教师</th>
								<th>题目人数</th>
								<th>题目状态</th>
								<th>技术领域</th>
								<th>操作</th>
							</tr>
						</thead>
						<c:forEach items="${topicList}" var="topic">
							<tbody>
								<tr>
									<td>${topic.topTitle}</td>
									<c:choose>
										<c:when test="${topic.topTopicType eq false}">
											<td>社会服务</td>
										</c:when>
										<c:when test="${topic.topTopicType eq true}">
											<td>科研项目</td>
										</c:when>
									</c:choose>
									<c:choose>
										<c:when test="${topic.topSource eq 3 }">
											<td>自拟题目</td>
										</c:when>
										<c:when test="${topic.topSource eq 2 }">
											<td>校内题目</td>
										</c:when>
										<c:when test="${topic.topSource eq 1 }">
											<td>公司项目</td>
										</c:when>
									</c:choose>
									<td>${topic.teacher.teaName}</td>
									<td>${topic.topCount }</td>
									<c:choose>
										<c:when test="${topic.topStatus eq 0 }">
											<td>已报满</td>
										</c:when>
										<c:when test="${topic.topStatus eq 2 }">
											<td>选报中</td>
										</c:when>
									</c:choose>
									<td>无</td>
									<td>
										<div class="btn-group">
											<button data-toggle="dropdown"
												class="btn btn-purple dropdown-toggle">
												<i class="fa fa-wrench"></i> 操作 <span class="caret"></span>
											</button>
											<ul class="dropdown-menu" role="menu">
												<li><a href="#"> <i class="fa fa-pencil"></i> Edit
												</a></li>
												<li><a href="#"> <i class="fa fa-trash-o"></i>
														Delete </a></li>
												<li><a href="#"> <i class="fa fa-ban"></i> Ban </a></li>
												<li class="divider"></li>
												<li><a href="#"> <i class="i"></i> Make admin </a></li>
											</ul>
										</div></td>
								</tr>
							</tbody>
						</c:forEach>
					</table>
				</div>
		</div>
	</div>
</body>
</html>
