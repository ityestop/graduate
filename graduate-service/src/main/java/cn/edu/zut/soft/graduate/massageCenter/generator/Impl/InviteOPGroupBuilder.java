package cn.edu.zut.soft.graduate.massageCenter.generator.Impl;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Message;
import cn.edu.zut.soft.graduate.massageCenter.generator.Builder;
import org.springframework.util.Assert;

import static cn.edu.zut.soft.basic.core.constant.Alternative.NO;
import static cn.edu.zut.soft.basic.core.constant.Alternative.YES;

/**
 * @author chuchuang
 * @date 16/11/21
 */

/**
 * 当前方法不适合使用注解
 * 邀请 决定的 消息生成器
 */
public class InviteOPGroupBuilder implements Builder {

    private static final String refuseGroupTem="亲爱的 %s \n\t非常抱歉的拒绝了你的邀请。";
    private static final String refuseGroupTitle = "【组队拒绝】 %s 回复 %s 的组队邀请";
    private String createGroupTem;
    private String createGroupTitle;
    private static final String okGroupTem="亲爱的 %s \n \t非常感谢你的邀请,我同意你的计划,希望我们合作愉快。";
    private static final String okGroupTitle = "【组队同意】 %s 回复 %s 的组队邀请";

    private InviteOPGroupBuilder(Alternative alternative){
        Assert.notNull(alternative);
        if (YES == alternative){
            createGroupTem = okGroupTem;
            createGroupTitle = okGroupTitle;
        }
        if (NO == alternative){
            createGroupTem = refuseGroupTem;
            createGroupTitle = refuseGroupTitle;
        }
    }

    @Override
    public Message assemblyMessage(Identity addressor, Identity recipients) {
        Assert.notNull(addressor, "addressor is null");
        Assert.notNull(recipients, "recipients is null");
        Message message = new Message();
        message.setContent(String.format(createGroupTem,recipients.getName()));
        message.setTitle(String.format(createGroupTitle,addressor.getName(),recipients.getName()));
        message.setRead(0);
        message.setAddressorName(addressor.getName());
        message.setRecipientsName(recipients.getName());
        message.setAddressor(addressor.getId());
        message.setRecipients(recipients.getId());
        return message;
    }

    public static Builder createBuilder(Alternative alternative){
        return new InviteOPGroupBuilder(alternative);
    }
}
