package cn.edu.zut.soft.graduate.core.constant.config;

import cn.edu.zut.soft.graduate.core.constant.GradeTimeType;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.design.AdminInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.*;

/**
 * @author chuchuang
 * @date 16/11/20
 */
public class IKEY {

    static {
        //json 的循环调用
        JSON.DEFAULT_GENERATE_FEATURE |= SerializerFeature.DisableCircularReferenceDetect.getMask();
    }

    public static final String USER = "user";

    public static final String CLAZZ= "clazz";
    public static final String GROUP = "group";
    public static final String GroupLeader ="groupLeader";//小组长
    public static final String GroupSEC = "groupSEC";//教师小组中秘书

    public static final String Petitioner="petitioner";//申请自拟题目标记
    public static final String UnEnergetic="unenergetic";//不积极的

    public static final String GROUP_GROUP = InfoType.Group.name()+GROUP;//小组ID
    public static final String GROUP_LEADER = InfoType.Group.name()+GroupLeader;
    public static final int DefaultID = 0;

    public static final int ZERO = 0;

    public static final int GROUP_COUNT_MAX = 3;

    public static final String TopicName = "topicName";//课题名称
    public static final String TopicId = "topicId";//课题编号
    public static final String ChildTopicName = "childTopicName";//子课题名称
    public static final String ChildTopicId = "childTopicId";//子标题id


    public static final String ISSUESTART = "issueStart";//开启课题的key
    public static final String ISSUE_EDIT = "issueEdit";//开启课题编辑key

    public static final String Guide = "guide";//指导
    public static final String GuideName = "guideName";//指导教师名称

    //职称
    public static final String PROFESSIONALLEVER = "professionalLever";
    public static final String WITNESS="witness";

    public static final String BASIC = "Basic";

    public static final String ADMIN = "admin";
    public static final String BASIC_ADMIN = "Basicadmin";
    public static final String ADDRESS = "address";

    /**
     * 通用一辨/ 二辨标识
     */
    public static final Integer ONE = 1;
    public static final Integer TWO = 2;
    public static final Integer END = -1;
    public static final String ONE_REPLY = "_one";
    public static final String TWO_REPLY = "_two";
    /**
     * 成绩key
     */
    public static final String OPEN_GRADE= "openGrade";
    public static final String INTERIM_GRADE= "interimGrade";
    public static final String SYSTEM_GRADE= "systemGrade";
    //答辩成绩
    public static final String REPLY_GRADE= "replyGrade";
    //教师知道成绩
    public static final String GUIDE_GRADE = "guideGrade";
    //报告成绩
    public static final String REPORT_GRADE = "reportGrade";
    //最终成绩
    public static final String FINAL_REPLY_GRADE = "finalReplyGrade";
    /**
     * <p>功能描述：优秀成绩key   90分以上</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-06 22:39:00</p>
     *
     */
    public static final String EXCELLENCE_GRADE = "excellenceGrade";
    /**
     * <p>功能描述：良好成绩Key  80-90分</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-06 22:39:05</p>
     *
     */
    public static final String WELL_GRADE = "wellGrade";
    /**
     * <p>功能描述：及格成绩key 60-80分</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-06 22:39:14</p>
     *
     */
    public static final String PASS_GRADE = "passGrade";
    /**
     * <p>功能描述：不合格成绩key 60分一下及终止答辩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-06 22:39:17</p>
     *
     */
    public static final String NO_PASS_GRADE = "noPassGrade";


    /**
     * <p>功能描述：终止答辩key</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-03-06 01:28:29</p>
     */
    public static final String GRADE_STOP_KEY= "stop";

    public static final String GRADE_STOP_VALUE = "end";



    public static final Map<String,Object> headMap = new HashMap<String, Object>() {
        {
            //'3','手机号','2':'班级','1':'姓名','0':'学号','7':'职称','6':'指导教师','5':'校外导师','4':'答辩题目','9':'题目来源','8':'地区'
            put("0","学号");
            put("1","姓名");
            put("2","班级");
            put("3","手机号");
            put("4","答辩题目");
            put("5","子标题");
            put("6","校外导师");
            put("7","指导教师");
            put("8","职称");
            put("9","地区");
            put("10","题目来源");
        }
    };

    public static final Map<String, Object> newHeadMap = new HashMap<String, Object>() {
        {
            /**
             * 学院名称 学生班级 学号 学生姓名 指导教师姓名 指导教师职称 毕业设计(论文)课题名称          课题来源                 课题类型       备注
             *                                                                          社会服务	科研项目	其它来源	    设计	论文	其它类型
             */
            put("0", "学院名称");
            put("1", "学生班级");
            put("2", "学号");
            put("3", "学生姓名");
            put("4", "指导教师姓名");
            put("5", "指导教师职称");
            put("6", "毕业设计(论文)课题名称");

            put("7", "课题来源");   //父
            put("8", "社会服务");   //子1
            put("9", "科研项目");   //子2
            put("10", "其他来源");  //子3

            put("11", "课题类型");   //父
            put("12", "设计");      //子1
            put("13", "论文");      //子2
            put("14", "其他类型");  //子3

            put("14", "备注");






        }
    };

    public static final String downloadTeaGroupStuSheet = "指导小组学生";

    public static final List<IssuePhase> statusSet = Arrays.asList(IssuePhase.auditIng, IssuePhase.auditErrorIng, IssuePhase.auditSuccessIng, IssuePhase.chooseIng, IssuePhase.finish);

    public static final List<InfoStrategy> StuInfoAll = Arrays.<InfoStrategy>asList(
            new BasicInfoStrategy(InfoType.Basic,IKEY.CLAZZ,""),
            new BasicInfoStrategy(InfoType.TOPIC,IKEY.TopicId,""),
            new BasicInfoStrategy(InfoType.TOPIC,IKEY.TopicName,""),
            new BasicInfoStrategy(InfoType.TOPIC,IKEY.ChildTopicId,""),
            new BasicInfoStrategy(InfoType.TOPIC,IKEY.ChildTopicName,""),
            new BasicInfoStrategy(InfoType.Group,IKEY.GROUP,null),
            new BasicInfoStrategy(InfoType.Group,IKEY.GroupLeader,null),
            new BasicInfoStrategy(InfoType.Basic,IKEY.ADDRESS,null),
            new BasicInfoStrategy(InfoType.Tea,IKEY.Guide,null),
            new BasicInfoStrategy(InfoType.Tea,IKEY.GuideName,null),
            new BasicInfoStrategy(InfoType.GRADE,IKEY.OPEN_GRADE,null),
            new BasicInfoStrategy(InfoType.GRADE,IKEY.INTERIM_GRADE,null),
            new BasicInfoStrategy(InfoType.GRADE,IKEY.GRADE_STOP_KEY,null),
            new BasicInfoStrategy(InfoType.GRADE,IKEY.MID,null),
            new BasicInfoStrategy(InfoType.GRADE,IKEY.SYSTEM_GRADE,null),
            new BasicInfoStrategy(InfoType.GRADE, GradeTimeType.SYSTEM_GRADE_TWO.getKey(),null),
            new BasicInfoStrategy(InfoType.GRADE, GradeTimeType.REPLY_GRADE_ONE.getKey(),null),
            new BasicInfoStrategy(InfoType.GRADE, GradeTimeType.REPLY_GRADE_TWO.getKey(),null),
            new BasicInfoStrategy(InfoType.GRADE, IKEY.REPLY_GRADE,null),
            new BasicInfoStrategy(InfoType.GRADE, IKEY.REPORT_GRADE,null),
            new BasicInfoStrategy(InfoType.GRADE, IKEY.GUIDE_GRADE,null),
            new BasicInfoStrategy(InfoType.GRADE, IKEY.FINAL_REPLY_GRADE,null),
            AdminInfoStrategy.admin

    );

    public static final List<InfoStrategy> teaInfoStrategyList = Arrays.<InfoStrategy>asList(
            new BasicInfoStrategy(InfoType.Basic,IKEY.PROFESSIONALLEVER,null),
            new BasicInfoStrategy(InfoType.Group,IKEY.GROUP,null),
            new BasicInfoStrategy(InfoType.Group,IKEY.GroupLeader,null),
            new BasicInfoStrategy(InfoType.Group,IKEY.GroupSEC,null),
            new BasicInfoStrategy(InfoType.Audit,IKEY.WITNESS,null)
    );
    public static String getReplyTimeKey(String key, String time){
        return key + time;
    }

    public static final String MID = "MID";
}
