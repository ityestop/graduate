package cn.edu.zut.soft.graduate.base.utils;

import cn.edu.zut.soft.graduate.core.model.BaseModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cc
 * @Date 2016/12/21
 * @value 1.0
 */
public class Utils {

    public  static <T extends BaseModel> Map<Integer,T> modelListToMapOfId(List<T> modelList){
        Map<Integer,T> map = new HashMap<>(modelList.size());
        for (T t:modelList){
            map.put(t.getId(),t);
        }
        return map;
    }

}
