<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>专家分配</title>
    <script src="/assets/g/js/witenss.js"></script>

</head>
<body>

<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./admin"> 首页 </a></li>
            <li class="active">专家管理</li>
        </ol>
        <div class="page-header">
            <h4>专家管理</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-1 col-sm-offset-10">
        <button class="btn btn-primary" id = "addButton" data-toggle="modal" data-target="#expertNotTeacher">
            <span><i class=" clip-plus-circle "></i> 添加专家</span>
        </button>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>
                        <small>姓名</small>
                    </th>
                    <th>
                        <small>手机</small>
                    </th>
                    <th>
                        <small>分配题目数</small>
                    </th>
                    <th>
                        <small>分配子标题数</small>
                    </th>
                    <th>
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${witness}" var="wl" >
                    <tr>
                        <td><small>${wl.name}</small></td>
                        <td><small>${wl.phone}</small></td>
                        <td><small>${wl.allNum}</small></td>
                        <td><small>${wl.smallAllNum}</small></td>
                        <td><small><a href="./admin/toAuditIssueList?userId=${wl.id}&userName=${wl.name}">分配 </a></small> <small><a href="./admin/issue_userId?userId=${wl.id}"> 删除 </a></small></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>

<div class="modal fade" id="expertNotTeacher" tabindex="-1" role="dialog"
     aria-labelledby="CSGModalLabel">
    <div class="modal-dialog" role="document">
        <form action="/admin/updateExpertTea" method="post">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="CSGModalLabel">添加专家成员</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <table class="table table-hover">
                        <th><input type="checkbox"/></th>
                        <th>姓名</th>
                        <th>职称</th>
                        <tbody id="no-witness">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default">添加</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
        </form>
    </div>
</div>

</body>
</html>
