package cn.edu.zut.soft.graduate.topicCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import org.springframework.stereotype.Repository;

/**
 * Created by chuchuang on 16/10/21.
 */
@Repository
public interface DeclareDAO<T extends Declare> extends BasicDAO<Integer,T> {
}
