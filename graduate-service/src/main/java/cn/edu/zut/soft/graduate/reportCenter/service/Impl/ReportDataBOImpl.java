package cn.edu.zut.soft.graduate.reportCenter.service.Impl;

import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.base.bo.BasicBOImpl;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.annotation.SimpleQueryParameterHandler;
import cn.edu.zut.soft.graduate.reportCenter.dao.ReportDataDAO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportData;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportDataBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
@Service
public class ReportDataBOImpl extends BasicBOImpl<ReportData,Query> implements ReportDataBO {

    @Autowired
    private ReportDataDAO<ReportData> reportDataDAO;

    @Override
    protected BasicDAO<Integer, ReportData> getDAO() {
        return reportDataDAO;
    }

    @Override
    protected List<Criteria> Query(Query query) {
        Criteria criteria = new Criteria();
        try {
            SimpleQueryParameterHandler.DEFAULT.handler(criteria,query);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("转换检索条件错误",e);
        }
        return Collections.singletonList(criteria);
    }
}
