package cn.edu.zut.soft.graduate.reportCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.BaseModelImpl;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 报告规则
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public class ReportRule extends BaseModelImpl implements ReportRuleDTO {

    /**
     * 报告规则的类型
     */
    private Integer type;

    /**
     * 报告规则的开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startTime;

    /**
     * 报告规则的结束时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endTime;

    /**
     * 报告次数标记
     */
    private Integer num;

    /**
     * 是否可以重复
     */
    private Integer repeat;


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getRepeat() {
        return repeat;
    }

    public void setRepeat(Integer repeat) {
        this.repeat = repeat;
    }
}
