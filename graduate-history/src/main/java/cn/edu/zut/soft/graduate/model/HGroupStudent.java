package cn.edu.zut.soft.graduate.model;

/**
 * <p>文件名称：HGroupStudent.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/29 下午5:17</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HGroupStudent {

    public String stuId;

    public String name;

    public HGroupStudent(String stuId, String name) {
        this.stuId = stuId;
        this.name = name;
    }

    public String getStuId() {
        return stuId;
    }

    public String getName() {
        return name;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public void setName(String name) {
        this.name = name;
    }
}
