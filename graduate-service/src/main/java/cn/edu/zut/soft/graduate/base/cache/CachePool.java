package cn.edu.zut.soft.graduate.base.cache;

import cn.edu.zut.soft.graduate.core.utils.cache.CacheMap;
import cn.edu.zut.soft.graduate.core.vo.IssueVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportRule;

/**
 * @author cc
 * @Date 2017/2/13
 * @value 1.0
 */
public class CachePool {
    public static CacheMap<Integer,ReportRule> reportRuleCacheMap = CacheMap.getInstance(3600000);//60*60*1000 一个小时

    public static CacheMap<Integer,UserDescVO> userDescVOCacheMap = CacheMap.getInstance(3600000);

    public static CacheMap<Integer,IssueVO> issueVOCacheMap = CacheMap.getInstance(3600000);
}
