<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>历史题目列表</title>
    <script src="assets/g/js/teaHistory.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/tea"> 首页 </a></li>
            <li class="active">历史题目</li>
        </ol>
        <div class="page-header">
            <h3>历史题目
                <small>可以将历史题目转换为线上题目</small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="sample-table-1" class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                    <th width="20%">
                        <small>历史题目</small>
                    </th>
                    <th>
                        <small>任务数</small>
                    </th>
                    <th>
                        <small>题目状态</small>
                    </th>
                    <th>
                        <small>子标题</small>
                    </th>
                    <th width="20%">
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <tbody id="table">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
