<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>选报审核</title>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
</head>
<body>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/tea"> 选报审核 </a></li>
            <li class="active">选报审核</li>
        </ol>
        <div class="page-header">
            <h3>选报审核</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->

<div class="row">
    <div class="col-sm-12 messages">
        <ul class="messages-list">
            <li class="messages-search">
                <form action="#" class="form-inline">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search messages...">
                        <div class="input-group-btn">
                            <button class="btn btn-primary searchSelf" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </li>
        </ul>
        <div class="messages-content">
            <div class="message-header">
                <div class="message-time">
                    <%--11 NOV 2014, 11:46 PM--%>
                </div>
                <div class="message-from">
                    <%--Nicole Bell &lt;nicole@example.com&gt;--%>
                </div>
                <div class="message-to">
                    <%--To: Peter Clark--%>
                </div>
                <div class="message-status">
                    <%--To: Peter Clark--%>
                </div>
                <div class="message-subject">
                    <%--New frontend layout--%>
                </div>
                <div class="message-actions">
                </div>
            </div>
            <div class="message-content">
                <div class="col-sm-12">

                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script src='/assets/g/js/config.js'></script>
<script type="application/javascript">
    function issueAll(data) {
        $.ajax({
            url: "/issue/querySelf",
            data: data,
            dataType: "json",
            type: "post",
            success: function (res) {
                if (res.success) {
                    issueList(res.data);
                } else {
                    alert(res.errorMsg);
                }
            }
        });
    }

    function onItem(data) {
        $.ajax({
            url: "issue/" + data.id,
            dataType: "json",
            success: function (res) {
                if (res.success) {
                    messageContent(res.data);
                } else {
                    alert(res.errorMsg);
                }
            }
        })
    }

    function issueList(list) {
        var messages_list = $(".messages-list");
        $(".messages-item").remove();
        if (list.length > 0) {
            $.each(list, function (i, item) {
                var message_Item = getMessage_Item(item);
                messages_list.append(message_Item);
                if (i == 0) {
                    onItem(item);
                }
            });
        }
    }


    function getMessage_Item(data) {
        var $item = $("<li class='messages-item'></li>");
        var $item_from = $("<span class='messages-item-from' style='white-space:nowrap;text-overflow:ellipsis;overflow:hidden;' title='" + data.title + "'>" + data.title + "</span>");
        var $item_preview = $("<span class='messages-item-preview'></span>");
        $item.append($item_from);
        $item.append($item_preview);
        $item.click(function () {
            onItem(data);
        });
        return $item;
    }

    function choose(id, choose) {
//        alert(id+"------"+choose);
        $.ajax({
            url: "/tea/choose/" + choose,
            data: {
                id: id
            },
            dataType: "json",
            success: function (res) {
                if (res.success) {
//                    messageContent(res.data);
                    onItem({id: res.data.issue.id});
                } else {
                    alert(res.errorMsg);
                }
            }
        });
    }

    function messageContent(data) {
        $(".message-time").html(data.issue.createTime);
        $(".message-from").html(data.issue.title);
        $(".message-to").html("课题人数: "+data.issue.taskSize + " 人 ");
        $(".message-status").html("题目状态:"+sourceMaps().issuePhaseMap[data.issue.status]);
//        $("")
        var columns = [{
            checkbox: true
        }, {
            field: 'declare.id',
            title: '编号'
        }, {
            field: 'declare.createAuthor',
            title: '小组长'
        }, {
            field: 'declare.status',
            title: '小组状态',
            formatter: function (value, row, index) {
                return value == "ING" ? "审核中" : (value == "YES" ? "审核通过" : "拒绝");
            }
        }, {
            title: '小组成员',
            formatter: function (value, row, index) {
                var str = "";
                if (row.infoList.length > 0) {
                    $.each(row.infoList, function (i, item) {
                        str += item.identityName + "、";
                    });
                }
                return str;
            }
        }, {
            title: "操作",
            formatter: function (value, row, index) {
                var str = "";
                if (row.declare.status == "ING") {
                    str += "<a class='btn btn-primary btn-sm' href='javascript:choose(" + row.declare.id + ",`YES`)'>同意</a>";
                    str += "<a class='btn btn-default btn-sm' href='javascript:choose(" + row.declare.id + ",`NO`)'>拒绝</a>";
                }
                return str;
            }
        }
        ];
        var params = {topicId: data.issue.id};
        $(".message-content .col-sm-12").html("<table id='table' class='table table-container'></table>");
        customSearch({id: "#table", url: "/tea/declareInfo", dataType: "json", params: params, columns: columns});

    }

    $(function () {
        var data = {};
        issueAll(data);
//        issueList(list);
    });


</script>
</body>
</html>
