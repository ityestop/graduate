package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.InfoTypeAble;

/**
 * Created by chuchuang on 16/10/23.
 */
public interface InfoStrategy {

    InfoTypeAble getType();

    String getKey();

    String getValue();

    String toJson();

}
