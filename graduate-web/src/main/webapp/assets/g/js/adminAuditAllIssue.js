$(function () {

    var IssueAndIdentity = {};
    var sourceMaps = {
        issuePhaseMap: {},
        issueKindMap: {},
        issueSourceMap: {},
    };

    $.ajax({
        url: "/issue/gainIssuePhase",
        dataType: "json",
        cache:true,
        type: "get",
        success: function (result) {
            if (result.success) {
                sourceMaps.issuePhaseMap = result.data;
            } else {
                alert(result.errorMsg);
            }
        }
    });

    $(function () {
        $.ajax({
            type:'POST',
            url:'./admin/toexpertIssue/auditIssue',
            dataType:'json',
            data:{
                role:'tea'
            },
            success:function (result) {
                if(result.success){
                    IssueAndIdentity = result.data;
                    issueList();
                }
            }
        });
    });

    // $("#tea_AuditIssue").click(function () {
    //     $('#auditIssuseTop').html('');
    //     $.ajax({
    //         type:'POST',
    //         url:'./admin/toexpertIssue/auditIssue',
    //         dataType:'json',
    //         data:{
    //             role:'tea'
    //         },
    //         success:function (result) {
    //             if(result.success){
    //                 IssueAndIdentity = result.data;
    //                 issueList();
    //             }
    //         }
    //     });
    // });
    //
    // $("#stu_AuditIssue").click(function () {
    //     $('#auditIssuseTop').html('');
    //     $.ajax({
    //         type:'POST',
    //         url:'./admin/toexpertIssue/auditIssue',
    //         dataType:'json',
    //         data:{
    //             role:'stu'
    //         },
    //         success:function (result) {
    //             if(result.success){
    //                 IssueAndIdentity = result.data;
    //                 issueList();
    //             }
    //         }
    //     });
    // });
    function issueList() {
        $.each(IssueAndIdentity,function (key,value) {
            var tr_h = $(getTr());
            var IssueAndIdentity = value;
            var td_h_title_s = $(getTd());
            var td_h_title = $(getsm());
            var td_h_title_id = $(getId());
            td_h_title.attr('wit')
            td_h_title.attr('title', IssueAndIdentity.issue.title);
            td_h_title.attr('style', 'overflow:hidden;white-space:nowrap;text-overflow:ellipsis;');
            td_h_title_id.attr("href","./admin/issue?id="+IssueAndIdentity.issue.id);
            td_h_title_id.append(IssueAndIdentity.issue.title);
            td_h_title.append(td_h_title_id);
            td_h_title_s.append(td_h_title);
            tr_h.append('');
            tr_h.append(td_h_title_s);

            var td_h_source_sm = $(getTd());
            var td_h_source = $(getsm());
            switch (IssueAndIdentity.issue.source) {
                case 'school':
                    td_h_source.append("校内题目");
                    break;
                case 'myself':
                    td_h_source.append("自拟题目");
                    break;
            }
            td_h_source_sm.append(td_h_source);
            tr_h.append(td_h_source_sm);

            var td_h_tea_sm = $(getTd());
            var td_h_tea = $(getsm());
            td_h_tea.append(IssueAndIdentity.issue.schoolTeaName);
            td_h_tea_sm.append(td_h_tea);
            tr_h.append(td_h_tea_sm);

            var td_h_status_sm = $(getTd());
            var td_h_status = $(getsm());
            var $label = $("<label></label>");
            $label.addClass("label");
            switch (IssueAndIdentity.issue.status) {
                case 'saveIng':
                    $label.addClass('label-info');
                    break;
                case 'auditIng':
                    $label.addClass('label-warning');
                    break;
                case 'auditErrorIng':
                    $label.addClass('label-danger');
                    break;
                case 'giveUp':
                    $label.addClass('label-inverse');
                    break;
                case 'finish':
                    $label.addClass('label-success');
                    break;
                default:
                    $label.addClass('label-default');
            }
            td_h_status.append($label.append(sourceMaps.issuePhaseMap[IssueAndIdentity.issue.status]));
            td_h_status_sm.append(td_h_status);
            tr_h.append(td_h_status_sm);

            var  td_h_size_sm = $(getTd());
            var  td_h_size = $(getsm());
            td_h_size.append(IssueAndIdentity.issue.taskSize);
            td_h_size_sm.append(td_h_size);
            tr_h.append(td_h_size_sm);

            /**
             * if auditname not null
             *      add  name + resetbtn
             * else
             *      add  disbtn
             * @type {any}
             */
            if(IssueAndIdentity.identity == null) {
                var td_h_update_sm =$(getTd());
                var th_h_u_btn = $(getDistrBtn(IssueAndIdentity.issue.id));
                td_h_update_sm.append(th_h_u_btn);
                tr_h.append(td_h_update_sm);
            }
            else {
                var td_h_update_sm =$(getTd());
                var th_h_u_btn = $(getResetBtn(IssueAndIdentity.identity.id, IssueAndIdentity.issue.id));
                td_h_update_sm.append(IssueAndIdentity.identity.name);
                td_h_update_sm.append(th_h_u_btn);

                tr_h.append(td_h_update_sm);
            }
            $('#auditIssuseTop').append(tr_h);
            tr_h.append('');

        })
    }

    function getTr() {
        return "<tr></tr>";
    }

    function getTd() {
        return "<td></td>";
    }
    function getsm() {
        return "<small></small>";
    }

    function getId() {
        return "<a href='#'></a>";
    }

    function getDistrBtn(id) {
        return "<button data-target='#teaList' data-toggle='modal' class='btn btn-primary btn-sm' onclick='getIssueID(" + id + ")' >分配</button>";
    }

    function getResetBtn(userId, issueId) {
        return "<button class='btn btn-danger btn-sm' style='float: right; margin-right: 25%' onclick='reset(" + userId + ", " + issueId + ")'>重置</button>";
    }

});

var curID = '';

function getIssueID(id) {
    curID = id;
}

function distr(userId, userName) {
    var url = "./admin/edit_aduit_new?userId=" + userId + " &issueId=" + curID + "&userName=" + userName
    url = encodeURI(url);
    window.location.href=url;
}

function reset(userId, issueId) {
    var url = "./admin/delete_audit_new?userId=" + userId + "&issueId=" + issueId;
    url = encodeURI(url);
    window.location.href=url;

}


