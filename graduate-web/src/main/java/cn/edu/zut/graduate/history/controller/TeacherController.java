//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.DaoFit;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.constant.TopStatus;
//import cn.edu.zut.soft.graduate.core.model.*;
//import cn.edu.zut.soft.graduate.core.model.tools.ReportTeaSelect;
//import cn.edu.zut.soft.graduate.core.model.tools.StuGroupTopicLink;
//import cn.edu.zut.soft.graduate.core.model.tools.StudentRW;
//import cn.edu.zut.soft.graduate.core.utils.*;
//import org.apache.ibatis.session.RowBounds;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//import tag.BaseController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.File;
//import java.io.OutputStream;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by sky on 15/11/7.
// */
//
//@Controller
//@RequestMapping("/teacher/")
//public class TeacherController extends BaseController implements ConfigDo {
//	@Resource
//	private DaoFit daoFit;
//	@Resource
//	private ServiceFit serviceFit;
//	private Role r = Role.TEA;
//
//	@RequestMapping("home")
//	public String homePage(Model model) {
//		model.addAttribute("menuSelected1", ConfigDo.INDEX);
//		return r.getName() + "/home";
//	}
//	@RequestMapping("Description")
//	public String  Description(Model model){
//		return r.getName() + "/Descriptionhome";
//	}
//	@RequestMapping("toNewTopic")
//	public String toNewTopic(Model model) {
//		ResultDo resultDo = serviceFit.getTopicService().findAll(Technophobe.class, new RowBounds());
//		if (resultDo.isSuccess()) {
//			// model.addAttribute("topic", new Topic());
//			model.addAttribute("technophobeList", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEANEWTOPIC);
//		return r.getName() + "/toNewTopic";
//	}
//
//	@RequestMapping("submitNewTopic")
//	public String addNewTopic(HttpSession session, Topic topic, @RequestParam("content") String[] ct,
//							  @RequestParam(value = "jsly", required = false) String[] tec,
//							  @RequestParam(value = "fz", required = false) String fz, @RequestParam("ctn") String[] ctn,
//							  @RequestParam("topstat") String topstat, Model model) {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		topic.setTopTeacherId(teacher.getTeaId());
//		if (topstat == null || topstat.equals("")) {
//			topic.setTopStatus(TopStatus.SAVE.getIndex());
//		} else {
//			topic.setTopStatus(Integer.parseInt(topstat));
//		}
//		if ("".equals(topic.getTopType())) {
//			topic.setTopType(fz);
//		}
//		if (ctn != null && ct != null) {
//			if (tec != null) {
//				List<Technophobe> technophobes = new ArrayList<Technophobe>();
//				for (String aTec : tec) {
//					technophobes.add(new Technophobe(aTec));
//				}
//				topic.setTopTechnophobeList(technophobes);
//			}
//			List<ChildTopic> childTopics = new ArrayList<ChildTopic>();
//			for (int i = 0, l = ctn.length; i < l; i++) {
//				childTopics.add(new ChildTopic(ctn[i], ct[i]));
//			}
//			topic.setChildTopicList(childTopics);
//			ResultDo resultDo = serviceFit.getTopicService().saveOrUpdateTopic(topic);
//			if (resultDo.isSuccess()) {
//				if (((Topic) resultDo.getResult()).getTopStatus() >= 3) {
//					return "redirect:./topicDetails.do?id=" + ((Topic) resultDo.getResult()).getTopId();
//				} else if (((Topic) resultDo.getResult()).getTopStatus() < 3) {
//					return "redirect:./topicDetailsTea.do?id=" + ((Topic) resultDo.getResult()).getTopId();
//				}
//
//			}
//		} else {
//			model.addAttribute("mag", "子标题未空！");
//		}
//		return "redirect:./toNewTopic.do";
//	}
//
//	/**
//	 * 教师+管理跳转详细信息页面
//	 *
//	 * @param model
//	 * @param id
//	 * @return
//	 */
//	@RequestMapping("topicDetails")
//	public String topicDetails(Model model, @RequestParam("id") String id) {
//		ResultDo resultDo = serviceFit.getTopicService().loadTopic(id);
//		if (resultDo.isSuccess()) {
//			model.addAttribute("topic", resultDo.getResult());
//			resultDo = serviceFit.getTopicService().findSGTLink(id, new RowBounds());
//			if (resultDo.isSuccess()) {
//				model.addAttribute("SGTLink", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return r.getName() + "/topicDetails";
//	}
//
//	/**
//	 * 教师跳转详细信息
//	 *
//	 * @param
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("topicDetailsTea")
//	public String topicDetailsTea(Model model, @RequestParam("id") String id) {
//		ResultDo resultDo = serviceFit.getTopicService().loadTopic(id);
//		if (resultDo.isSuccess()) {
//			model.addAttribute("topic", resultDo.getResult());
//			resultDo = serviceFit.getTopicService().findSGTLink(id, new RowBounds());
//			if (resultDo.isSuccess()) {
//				model.addAttribute("SGTLink", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return r.getName() + "/topicDetailsTea";
//	}
//
//	@RequestMapping("teaTopicList")
//	public String teaTopicList(HttpSession session, Model model) {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		ResultDo<List<Topic>> resultDo = serviceFit.getTopicService().findByTeaIdTopic(teacher.getTeaId());
//		if (resultDo.isSuccess()) {
//			model.addAttribute("teacherAndTopic", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		// model.addAttribute("editStatus", );
//		model.addAttribute("menuSelected1", ConfigDo.TEATOPICLIST);
//		return r.getName() + "/teaTopicList";
//	}
//
//	@RequestMapping("toToPicUpdate")
//	public String toToPicUpdate(Model model, @RequestParam("id") String id) {
//		ResultDo resultDo = serviceFit.getTopicService().loadTopic(id);
//		if (resultDo.isSuccess()) {
//			model.addAttribute("topic", resultDo.getResult());
//			resultDo = serviceFit.getTopicService().findAll(Technophobe.class, new RowBounds());
//			model.addAttribute("technophobeList", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return r.getName() + "/toNewTopic";
//	}
//
//	/**
//	 * 提交课题
//	 *
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping("submitTopic")
//	public String submitTopic(@RequestParam("id") String id,
//			@RequestParam(value = "statues", required = false) String statues) throws Exception {
//		// System.out.println(id +"测试数据"+statues);
//		if (statues.equals("") || statues == null) {
//			serviceFit.getTopicService().AdminAudit(id, null, TopStatus.AUDIT);
//		} else if (statues.equals("invalid")) {
//			serviceFit.getTopicService().AdminAudit(id, null, TopStatus.INVALID);
//			return "redirect:/teacher/topicDetailsTea.do?id=" + id;
//		}
//		// System.out.println(resultDo.getMessage());
//		return "redirect:/teacher/teaTopicList.do";
//		//
//	}
//
//	/**
//	 * 教师信息
//	 *
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping("teaInfo")
//	public String topicInfo(Model model, HttpSession session) throws Exception {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getEmpService().load(teacher.getTeaId(), r);
//		if (resultDo.isSuccess()) {
//			model.addAttribute("teacher", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEAINFO);
//		return r.getName() + "/teaInfo";
//		//
//	}
//
//	@RequestMapping("upTeacher")
//	public String upTeacher(Teacher teacher, HttpSession session, Model model) {
//		if (teacher != null) {
//			serviceFit.getEmpService().insertAndUpdate(teacher, r);
//			session.setAttribute("user", serviceFit.getEmpService().load(teacher.getTeaId(), r).getResult());
//		}
//		return "redirect:./teaInfo.do";
//	}
//
//	@RequestMapping("toUpPwd")
//	public String toUpPwd() {
//		return r.getName() + "/upTeaPwd";
//	}
//
//	@RequestMapping("upTeaPwd")
//	public String upTeaPwd(HttpSession session, @RequestParam("o") String o, @RequestParam("n") String n,
//			@RequestParam("re") String re, Model model) {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		if (n.equals(re)) {
//			if (teacher != null) {
//				ResultDo resultDo = serviceFit.getEmpService().verify(teacher.getTeaName(), MDUtil.MD5Tools(o), r);
//				if (resultDo.isSuccess()) {
//					teacher = (Teacher) resultDo.getResult();
//					teacher.setTeaPassword(MDUtil.MD5Tools(n));
//					serviceFit.getEmpService().insertAndUpdate(teacher, r);
//					model.addAttribute("mag", "1");
//
//				} else {
//					model.addAttribute("mag", "密码错误，请重新输入");
//				}
//			} else {
//				model.addAttribute("mag", "没有登录");
//			}
//		} else {
//			model.addAttribute("mag", "密码与确认密码不一致");
//		}
//		return r.getName() + "/upTeaPwd";
//	}
//
//	/**
//	 * 删除课题
//	 *
//	 * @return
//	 * @throws Exception
//	 */
//	@RequestMapping("deleteTopic")
//	public String deleteTopic(@RequestParam("id") String id, HttpSession session) throws Exception {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		if (teacher != null) {
//			serviceFit.getTopicService().deleteTopic(id, teacher);
//		}
//		return "redirect:/teacher/teaTopicList.do";
//	}
//
//	/**
//	 * 专家审批题目-查询所有信息
//	 */
//	@RequestMapping("toteacAndTopicChoose")
//	public String teacAndTopicChoose(Map<String, Object> model, HttpSession session,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getTopicService().findByExpert(teacher.getTeaId(),
//				new RowBounds((l - 1) * s, s));
//		int count = ((List) serviceFit.getTopicService().findByExpert(teacher.getTeaId(), new RowBounds()).getResult())
//				.size();
//		initPage(model, l, s, count);
//		if (resultDo.isSuccess()) {
//			model.put("teacherAndTopic", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		model.put("menuSelected1", ConfigDo.TEACHERANDTOPIC);
//		return r.getName() + "/teaChooseTopic";
//
//	}
//
//	@RequestMapping("lookTeaStudent")
//	public String lookTeaStudent(Map<String, Object> model, HttpSession session) {
//		ResultDo resultDo;
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		resultDo = serviceFit.getEmpService().load(teacher.getTeaId(), Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "lookTeaStudent");
//			map.put("teaId", teacher.getTeaId());
//			map.put("b", false);
//			resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.ADMIN);
//			if (resultDo.isSuccess()) {
//				model.put("list", resultDo.getResult());
//				List<StudentRW> list = (List<StudentRW>) resultDo.getResult();
//				for (StudentRW lis : list) {
//					resultDo = serviceFit.getTopicService().loadTopic(lis.getTopId());
//					model.put("topic", resultDo.getResult());
//				}
//
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("menuSelected1", ConfigDo.TEATOPICLIST);
//		model.put("menuSelected2", ConfigDo.LOOKSTUDENT);
//		return r.getName() + "/lookTeaStudent";
//	}
//
//	@RequestMapping("childTopic")
//	public @ResponseBody Map<String, Object> childTopic(@RequestParam("topId") String topId) {
//		HashMap<String, Object> map = new HashMap<String, Object>();
//		ResultDo resultDo = serviceFit.getTopicService().loadTopic(topId);
//		if (resultDo.isSuccess()) {
//			map.put("topic", resultDo.getResult());
//			map.put("status", true);
//		} else {
//			map.put("mag", resultDo.getMessage());
//		}
//
//		return map;
//
//	}
//
//	/**
//	 * 专家审批--同意课题
//	 */
//	@RequestMapping("submitAdTopic")
//	public ModelAndView refuseAdTopic(RedirectAttributes attr, @RequestParam("id") String topId,
//			@RequestParam("status") int st, @RequestParam(value = "details", required = false) String content) {
//		String mag = "";
//		ResultDo resultDo;
//		TopStatus status = TopStatus.getName(st);
//		resultDo = serviceFit.getTopicService().AdminAudit(topId, content, status);
//		if (resultDo.isSuccess()) {
//			Topic topic = (Topic) resultDo.getResult();
//			attr.addAttribute("teacherId", topic.getTopTeacherId());
//			attr.addAttribute("topStatus", topic.getTopStatus());
//		} else {
//			mag = mag + resultDo.getMessage();
//		}
//		attr.addAttribute("mag", mag);
//		return new ModelAndView("redirect:./toteacAndTopicChoose.do");
//	}
//
//	/**
//	 * 审核学生提交的申报
//	 *
//	 * @param attr
//	 * @return
//	 */
//	@RequestMapping("upAdSgTL")
//	public String upAdSgTL(RedirectAttributes attr, @RequestParam(value = "res") boolean res,
//			@RequestParam(value = "sgTl") String sgTl) {
//		ResultDo resultDo = serviceFit.getStuGroupService().updateTopStuLink(sgTl, res);
//		attr.addAttribute("id", ((StuGroupTopicLink) resultDo.getResult()).getChooTopicId());
//		if (!resultDo.isSuccess()) {
//			attr.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./topicDetailsTea.do";
//	}
//
//	@RequestMapping("assignStudent")
//	public String assignStudent(Model model, @RequestParam("stuId") String stuId,
//			@RequestParam("childId") String childId) {
//		ResultDo resultDo = new ResultDo();
//		// System.out.println("stuId--"+stuId+"childId---"+childId);
//		resultDo = serviceFit.getTopicService().assignChildTopic(stuId, childId);
//		if (resultDo.isSuccess()) {
//			return "redirect:./lookTeaStudent.do";
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return r.getName() + "/lookTeaStudent";
//	}
//
//	@RequestMapping("teaGroup")
//	public String teaGroupcontent(Model model, @RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findTeacherBytgId(tgId);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("list", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据格式不正确！");
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		model.addAttribute("menuSelected2", ConfigDo.TEACHERGROUPALL);
//		return r.getName() + "/teaGroup";
//	}
//
//	@RequestMapping("teaGroupStudent")
//	public String teaGroupStudent(Model model, @RequestParam("tgId") String tgId, @RequestParam("number") int number) {
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findStudentBytgId(tgId, 0);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("stulist", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据格式不正确！");
//		}
//		model.addAttribute("number", number);
//		if (number == 1) {
//			model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		} else {
//			model.addAttribute("menuSelected1", ConfigDo.DOCKING);
//		}
//
//		model.addAttribute("menuSelected2", ConfigDo.TEACHERASSIGSTUDENT);
//		if (number == 4) {
//			return r.getName() + "/todonGradeOther";
//		} else {
//			return r.getName() + "/teaGroupStu";
//		}
//
//	}
//
//	@RequestMapping("OpenGrade")
//	public String OpenGrade(Model model, @RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findStudentBytgId(tgId, 1);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("stulistFalse", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//			resultDo = serviceFit.getTeacherGroupService().findStudentBytgId(tgId, 2);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("stulistTrue", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据格式不正确！");
//		}
//
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		model.addAttribute("menuSelected2", ConfigDo.OPENGRADE);
//		return r.getName() + "/OpenGrade";
//	}
//
//	/**
//	 * 下载学生信息
//	 *
//	 * @param tgId
//	 * @param response
//	 */
//	@RequestMapping("download")
//	public void dowmLoad(@RequestParam("tgId") String tgId, HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition", "attachment;fileName=student.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getUploadService().HandleExcelBytgId(tgId);
//			if (resultDo.isSuccess()) {
//				// System.out.println("teach"+resultDo.getResult());
//				excel = (Excel) resultDo.getResult();
//				try {
//					File file = new File("小组学生信息表.xls");
//					OutputStream fout = response.getOutputStream();
//					ExcelUtil.excelFactory(excel).write(fout);
//					fout.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//
//	@RequestMapping("updateOpenGrade")
//	public String updateOpenGrade(Model model, @RequestParam(value = "stuIds", required = false) String[] stuIds,
//			@RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (stuIds.length > 0 && stuIds != null) {
//			resultDo = serviceFit.getEmpService().updateStudentOpenTopics(stuIds, true);
//			if (resultDo.isSuccess()) {
//				if (tgId != null && tgId != "") {
//					return "redirect:./OpenGrade.do?tgId=" + tgId;
//				}
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			return "redirect:./OpenGrade.do?tgId=" + tgId;
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		model.addAttribute("menuSelected2", ConfigDo.OPENGRADE);
//		return "redirect:./OpenGrade.do?tgId=" + tgId;
//	}
//
//	@RequestMapping("updateOpenGradeOne")
//	public String updateOpenGradeOne(Model model, @RequestParam(value = "stuId") String stuId,
//			@RequestParam("tgId") String tgId, @RequestParam("status") boolean status) {
//		ResultDo resultDo = new ResultDo();
//		if (stuId != null && stuId != "" && status == true) {
//			resultDo = serviceFit.getEmpService().updateStudentOpenTopic(stuId, status);
//			if (resultDo.isSuccess()) {
//				if (tgId != null && tgId != "") {
//					return "redirect:./OpenGrade.do?tgId=" + tgId;
//				}
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else if (stuId != null && stuId != "" && status == false) {
//			resultDo = serviceFit.getEmpService().updateStudentOpenTopic(stuId, status);
//			if (resultDo.isSuccess()) {
//				if (tgId != null && tgId != "") {
//					return "redirect:./OpenGrade.do?tgId=" + tgId;
//				}
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		model.addAttribute("menuSelected2", ConfigDo.OPENGRADE);
//		return "redirect:./OpenGrade.do?tgId=" + tgId;
//	}
//
//	@RequestMapping("TeaByStudentGrade")
//	public String TeaByOwnStudent(HttpSession session, Map<String, Object> model, @RequestParam("name") String name) {
//		ResultDo resultDo;
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		String interimCount = DataLS.functionMap.get("InterimScampling").getContent();
//		Integer countInteger = Integer.parseInt(interimCount);
//		int interimForCount = codeValue[countInteger - 1];
//		resultDo = serviceFit.getEmpService().load(teacher.getTeaId(), Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "lookTeaStudent");
//			map.put("teaId", teacher.getTeaId());
//			map.put("b", false);
//			resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.ADMIN);
//			if (resultDo.isSuccess()) {
//				model.put("list", resultDo.getResult());
//				List<StudentRW> list = (List<StudentRW>) resultDo.getResult();
//				for (StudentRW lis : list) {
//					resultDo = serviceFit.getTopicService().loadTopic(lis.getTopId());
//					if (lis.getVa() == 0) {// 当与操作值
//						lis.setVa(-1);
//					} else {
//						if ((lis.getVa() & interimForCount) != 0) {
//							lis.setVa(0);
//						} else {
//							lis.setVa(-1);
//						}
//					}
//				}
//				model.put("topic", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("GradeName", name);
//		model.put("menuSelected1", ConfigDo.GRADEALL_STRING);
//		model.put("menuSelected2", ConfigDo.TEACHERBYSTUDEBTGRADE);
//		if (name.equals("MyStudent")) {
//			return r.getName() + "/MyStudent";
//		}else if(name.equals("documentGrade")){
//			return r.getName()+"/documentGrade";
//		}else if(name.equals("finalReplyGrade")){
//			return r.getName()+"/finalReplyGrade";
//		}else {
//			return r.getName() + "/TeaByStudentGrade";
//		}
//	}
//
//	/**
//	 * 教师重置分配子标题任务
//	 *
//	 * @param model
//	 * @param topId
//	 * @return
//	 */
//	@RequestMapping("restChildTopic")
//	public String restChildTopic(Model model, @RequestParam("topId") String topId) {
//		ResultDo resultDo;
//		resultDo = serviceFit.getTopicService().restChildTopic(topId);
//		if (resultDo.isSuccess()) {
//			model.addAttribute("topic", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./lookTeaStudent.do";
//	}
//
//	/**
//	 * 教师检索
//	 *
//	 * @param session
//	 * @param model
//	 * @return
//	 * @RequestParam(value = "pageNum", required = false, defaultValue = "1")
//	 *                     int l,
//	 * @RequestParam(value = "pageSize", required = false, defaultValue = "20")
//	 *                     int s
//	 */
//	@RequestMapping("teaWeekReportList")
//	public ModelAndView _ListReport(HttpSession session, ReportTeaSelect reportTeaSelect, Map<String, Object> model) {
//		ResultDo resultDo;
//		int totalCount = 0;
//		int weekCount = 0, monthCount = 0;
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		if (reportTeaSelect == null) {
//			reportTeaSelect.setTeaId(teacher.getTeaId());
//			resultDo = serviceFit.getReportSerivce().findListTea(reportTeaSelect, new RowBounds());
//			totalCount = ((List) serviceFit.getReportSerivce().findListTea(reportTeaSelect, new RowBounds())
//					.getResult()).size();
//			if (resultDo.isSuccess()) {
//				model.put("ReportList", resultDo.getResult());
//			} else {
//				model.put("mag", "查询错误！");
//			}
//		} else {
//			if (reportTeaSelect.getTeaId() == null) {
//				reportTeaSelect.setTeaId(teacher.getTeaId());
//			}
//			resultDo = serviceFit.getReportSerivce().findListTea(reportTeaSelect, new RowBounds());
//			totalCount = ((List) serviceFit.getReportSerivce().findListTea(reportTeaSelect, new RowBounds())
//					.getResult()).size();
//			if (resultDo.isSuccess()) {
//				model.put("ReportList", resultDo.getResult());
//			} else {
//				model.put("mag", "查询错误2！");
//			}
//		}
//		try {
//			weekCount = DataUtil
//					.week(new SimpleDateFormat("yyyy-MM-dd").parse(DataLS.functionMap.get("startTime").getContent()));
//			monthCount = DataUtil.yue();
//			int count = weekCount - 3;
//			int[] a = new int[count];
//			if (weekCount > 0 && monthCount > 0) {
//				for (int i = 0; i < count; i++) {
//					a[i] = i + 4;
//					if (a[i] == weekCount) {
//						break;
//					}
//				}
//			}
//			model.put("number", a);
//			model.put("weekCount", weekCount);
//			model.put("monthCount", monthCount);
//		} catch (ParseException e) {
//		}
//		resultDo = serviceFit.getEmpService().selectStudentByTeaId(teacher.getTeaId());
//		if (resultDo.isSuccess()) {
//			model.put("student", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		// initPage(model, l, s, totalCount);
//
//		model.put("menuSelected1", ConfigDo.ReportGroup);
//		model.put("menuSelected2", ConfigDo.WEEK_REPORT);
//		return new ModelAndView(r.getName() + "/weekReport", model);
//	}
//
//	@RequestMapping("showReport")
//	public ModelAndView _showReport(@RequestParam("id") String id, @RequestParam("status") int status, Model model) {
//		ResultDo<?> resultDo = serviceFit.getReportSerivce().find(id);
//		if (resultDo != null && resultDo.isSuccess()) {
//			model.addAttribute("info", resultDo.getResult());
//		}
//		model.addAttribute("status", status);
//		return new ModelAndView(r.getName() + "/weekReportResult");
//	}
//
//	/**
//	 * 教师强制解散小组(学生指导教师不变)
//	 *
//	 * @param sgId
//	 * @return
//	 */
//	@RequestMapping("DissolutionGroup")
//	public String DissolutionGroup(@RequestParam("sgId") String sgId, Model model,
//			@RequestParam("topId") String topId) {
//		ResultDo resultDo;
//		if (sgId != null && topId != null) {
//			resultDo = serviceFit.getStuGroupService().CompulsoryDissolutionGroup(sgId, topId);
//			if (resultDo.isSuccess()) {
//				return "redirect:teaTopicList.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "传递参数为空");
//		}
//
//		return "redirect:./topicDetailsTea.do?id=" + topId;
//	}
//
//	/**
//	 * 通过校外题目
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("passOutsideTopic")
//	public String passOutsideTopic(Model model, @RequestParam("topId") String topId,
//			@RequestParam("sgId") String sgId) {
//		ResultDo resultDo = serviceFit.getTopicService().passOutsideTopic(topId, sgId);
//		if (resultDo.isSuccess()) {
//			return "redirect:./teaTopicList.do";
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./topicDetailsTea.do?id=" + topId;
//	}
//
//	/**
//	 * 退回题目
//	 *
//	 * @param model
//	 * @param topId
//	 * @return
//	 */
//	@RequestMapping("BackToapicWrite")
//	public String backTopicWrite(Model model, @RequestParam("topId") String topId) {
//		ResultDo resultDo = serviceFit.getTopicService().backTopicWrite(topId);
//		if (resultDo.isSuccess()) {
//			return "redirect:./topicDetailsTea.do?id=" + topId;
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./topicDetailsTea.do?id=" + topId;
//	}
//
//	/**
//	 * 退回周月报状态
//	 *
//	 * @param model
//	 * @param weekId
//	 * @return
//	 */
//	@RequestMapping("BackStatusForReport")
//	public ModelAndView backStatusForReport(Map<String, Object> model, @RequestParam("weekId") String weekId) {
//		ResultDo resultDo;
//		Report report = new Report();
//		if (weekId != null) {
//			report.setId(weekId);
//			report.setStatus(1);
//			resultDo = serviceFit.getReportSerivce().updateReport(report);
//			if (resultDo.isSuccess()) {
//				return new ModelAndView("redirect:./teaWeekReportList.do");
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		} else {
//			model.put("mag", "数据错误！");
//		}
//		return new ModelAndView("redirect:./teaWeekReportList.do", model);
//	}
//
//	@RequestMapping("ReportGrade")
//	public ModelAndView ReportGrade(Map<String, Object> model, Report report) {
//		ResultDo resultDo;
//		if (report != null) {
//			report.setStatus(3);
//			resultDo = serviceFit.getReportSerivce().updateReport(report);
//			if (resultDo.isSuccess()) {
//				return new ModelAndView("redirect:./teaWeekReportList.do");
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		} else {
//			model.put("mag", "数据错误！");
//		}
//		return new ModelAndView("redirect:./showReport.do?id=" + report.getId(), model);
//	}
//
//	@RequestMapping("InterimGrade")
//	public ModelAndView InterimGrade(Map<String, Object> model, @RequestParam("stuId") String stuId,
//			@RequestParam("grade") int grade) {
//		ResultDo resultDo;
//		if (stuId != null) {
//			resultDo = serviceFit.getEmpService().updateStudentInterim(stuId, grade);
//			if (resultDo.isSuccess()) {
//				return new ModelAndView("redirect:./toInterimGrade.do");
//			}
//		} else {
//			model.put("mag", "数据错误！");
//		}
//
//		return new ModelAndView("redirect:./toInterimGrade.do", model);
//	}
//
//	@RequestMapping("toInterimGrade")
//	public ModelAndView interimGrade(HttpSession session, Map<String, Object> model) {
//		ResultDo resultDo;
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		String interimCount = DataLS.functionMap.get("InterimScampling").getContent();
//		Integer countInteger = Integer.parseInt(interimCount);
//		int interimForCount = codeValue[countInteger - 1];
//		resultDo = serviceFit.getEmpService().load(teacher.getTeaId(), Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "lookTeaStudent");
//			map.put("teaId", teacher.getTeaId());
//			map.put("b", false);
//			resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.ADMIN);
//			if (resultDo.isSuccess()) {
//				model.put("list", resultDo.getResult());
//				List<StudentRW> list = (List<StudentRW>) resultDo.getResult();
//				for (StudentRW lis : list) {
//					resultDo = serviceFit.getTopicService().loadTopic(lis.getTopId());
//					if (lis.getVa() == 0) {// 当与操作值
//						lis.setVa(-1);
//					} else {
//						if ((lis.getVa() & interimForCount) != 0) {
//							lis.setVa(0);
//						} else {
//							lis.setVa(-1);
//						}
//					}
//				}
//				model.put("topic", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("menuSelected1", ConfigDo.GRADEALL_STRING);// 父
//		model.put("menuSelected2", ConfigDo.ITERIMGRADE_STRING);// 子
//
//		return new ModelAndView(r.getName() + "/InterimGrade", model);
//	}
//
//	@RequestMapping("toAdminUploadFile")
//	public ModelAndView touploadFile(Map<String, Object> model) {
//		ResultDo resultDo = serviceFit.getFileModelService().findAll();
//		if (resultDo.isSuccess()) {
//			model.put("fileList", resultDo.getResult());
//		}
//		model.put("menuSelected1", ConfigDo.TEACHER_FILE);
//		return new ModelAndView(r.getName() + "/fileList", model);
//	}
//
//	// 该指导教师所带学生未交周月报名单
//
//	@RequestMapping("teaReportManage")
//	public ModelAndView teaReportManage(Map<String, Object> model, HttpSession session) {
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getReportSerivce().searchDetails(r, teacher.getTeaId(), null);
//		List<String[]> req = new ArrayList<>();
//		List<String[]> req2 = new ArrayList<>();
//		req = (List<String[]>) resultDo.getResult();
//
//		DataUtil du = new DataUtil();
//		String[] st;
//		Student stu;
//		for (String[] ss : req) {
//			st = new String[5];
//
//			stu = daoFit.getStudentDao().selectByKey(ss[0]);
//			st[0] = stu.getStuName(); // 姓名
//			if (stu.isStuSex()) { // 性别
//				st[1] = "男";
//			} else {
//				st[1] = "女";
//			}
//			st[2] = stu.getStuClass(); // 班级
//			st[3] = du.Notsubmitted(Long.parseLong(ss[1])); // 未交周
//			st[4] = du.Notsubmitted(Long.parseLong(ss[2])); // 未交月
//
//			req2.add(st);
//
//		}
//
//		if (resultDo.isSuccess()) {
//			// model.put("report", resultDo.getResult());
//			model.put("report", req2);
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		model.put("menuSelected1", ConfigDo.ReportGroup);
//		model.put("menuSelected2", ConfigDo.TEACHER_NO_REPORT);
//		return new ModelAndView(r.getName() + "/unSumbitReport", model);
//	}
//
//	/**
//	 * 答辩名单导出
//	 *
//	 * @param tgId
//	 * @param response
//	 */
//	@RequestMapping("downloadDefense")
//	public void dowmLoad2(@RequestParam("tgId") String tgId, HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition", "attachment;fileName=student.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getUploadService().HandleExcelDefenseList(tgId);
//			if (resultDo.isSuccess()) {
//				// System.out.println("teach"+resultDo.getResult());
//				excel = (Excel) resultDo.getResult();
//				try {
//					File file = new File("小组学生信息表.xls");
//					OutputStream fout = response.getOutputStream();
//					ExcelUtil.excelFactory(excel).write(fout);
//					fout.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//
//	/**
//	 * 答辩名单导出
//	 *
//	 * @param response
//	 */
//	@RequestMapping("downloadTeaStuResult")
//	public void dowmLoad3(HttpSession s,HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition", "attachment;fileName=student.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//		Teacher teacher = (Teacher) s.getAttribute("user");
//		if (teacher != null) {
//			resultDo = serviceFit.getUploadService().HandleExcelTeaResultList(teacher);
//			if (resultDo.isSuccess()) {
//				// System.out.println("teach"+resultDo.getResult());
//				excel = (Excel) resultDo.getResult();
//				try {
//					File file = new File("小组学生信息表.xls");
//					OutputStream fout = response.getOutputStream();
//					ExcelUtil.excelFactory(excel).write(fout);
//					fout.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//
//	@RequestMapping("donGrade")
//	public ModelAndView DonGrade(Map<String, Object> model, @RequestParam("stuId") String stuId,
//			@RequestParam("grade") String grade) {
//		ResultDo resultDo;
//		String[] stuIds = stuId.split(",");
//		String[] grades = grade.split(",");
//		for (int i = 0; i < grades.length; i++) {
//			if (!grades[i].equals("")) {
//				resultDo = serviceFit.getEmpService().updateStudentdonGrade(stuIds[i], Double.parseDouble(grades[i]));
//			}
//
//		}
//		return new ModelAndView("redirect:./todonGrade.do", model);
//	}
//	@RequestMapping("donGradeOne")
//	public ModelAndView DonGradeOne(Map<String, Object> model, @RequestParam("stuId") String stuId,
//			@RequestParam("grade")double grade) {
//		ResultDo resultDo;
//		if(stuId != null && grade != 0){
//			resultDo = serviceFit.getEmpService().updateStudentdonGrade(stuId, grade);
//		}
//		return new ModelAndView("redirect:./todonGrade.do", model);
//	}
//
//	@RequestMapping("todonGrade")
//	public ModelAndView donGrade(HttpSession session, Map<String, Object> model) {
//		ResultDo resultDo;
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		String interimCount = DataLS.functionMap.get("InterimScampling").getContent();
//		Integer countInteger = Integer.parseInt(interimCount);
//		int interimForCount = codeValue[countInteger - 1];
//		resultDo = serviceFit.getEmpService().load(teacher.getTeaId(), Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "lookTeaStudent");
//			map.put("teaId", teacher.getTeaId());
//			map.put("b", false);
//			resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.ADMIN);
//			if (resultDo.isSuccess()) {
//				model.put("list", resultDo.getResult());
//				List<StudentRW> list = (List<StudentRW>) resultDo.getResult();
//				for (StudentRW lis : list) {
//					resultDo = serviceFit.getTopicService().loadTopic(lis.getTopId());
//				}
//				model.put("topic", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("menuSelected1", ConfigDo.GRADEALL_STRING);// 父
//		model.put("menuSelected2", ConfigDo.ITERIMGRADE_STRING);// 子
//
//		return new ModelAndView(r.getName() + "/donGrade", model);
//	}
//
//	@RequestMapping("checkGrade")
//	public String CheckGrade(Model model, @RequestParam("tgId") String tgId,@RequestParam("name")String name) {
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findStudentBytgId(tgId, 0);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("stulist", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据格式不正确！");
//		}
//		model.addAttribute("menuSelected1", "docking");
//		model.addAttribute("menuSelected2", "checkGrade");
//		if(name.equals("finalGrade")){
//			return r.getName()+"/finalGrade";
//		}else if(name.equals("docGrade")){
//			return r.getName()+"/docGrade";
//		}else if(name.equals("finalAllGrade")){
//			return r.getName()+"/finalAllGrade";
//		}
//		return r.getName() + "/checkGrade";
//	}
//
//	@RequestMapping("updateCheckGrade")
//	public String updateCheckGrade(Model model, @RequestParam(value = "stuIds", required = false) String[] stuIds,
//			@RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (stuIds.length > 0 && stuIds != null) {
//			resultDo = serviceFit.getEmpService().updateStudentcheckgrades(stuIds, 1);
//			if (resultDo.isSuccess()) {
//				if (tgId != null && tgId != "") {
//					return "redirect:./checkGrade.do?tgId=" + tgId+"&name=default";
//				}
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			return "redirect:./checkGrade.do?tgId=" + tgId+"&name=default";
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		return "redirect:./checkGrade.do?tgId=" + tgId+"&name=default";
//	}
//
//	@RequestMapping("updateCheckGradeOne")
//	public String updateCheckGradeOne(Model model, @RequestParam(value = "stuId") String stuId,
//			@RequestParam("tgId") String tgId, @RequestParam("status") int status ) {
//		ResultDo resultDo = new ResultDo();
//		if (stuId != null && stuId != "") {
//			resultDo = serviceFit.getEmpService().updateStudentcheckgrade(stuId, status);
//			if (resultDo.isSuccess()) {
//				if (tgId != null && tgId != "") {
//					return "redirect:./checkGrade.do?tgId="+tgId+"&name=finalGrade";
//				}
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		return "redirect:./checkGrade.do?tgId=" + tgId+"&name=default";
//	}
//
//	@RequestMapping("checkGradeView")
//	public String checkGradeView(HttpSession session, Map<String, Object> model) {
//		ResultDo resultDo;
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		String interimCount = DataLS.functionMap.get("InterimScampling").getContent();
//		Integer countInteger = Integer.parseInt(interimCount);
//		int interimForCount = codeValue[countInteger - 1];
//		resultDo = serviceFit.getEmpService().load(teacher.getTeaId(), Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "lookTeaStudent");
//			map.put("teaId", teacher.getTeaId());
//			map.put("b", false);
//			resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.ADMIN);
//			if (resultDo.isSuccess()) {
//				model.put("list", resultDo.getResult());
//				List<StudentRW> list = (List<StudentRW>) resultDo.getResult();
//				for (StudentRW lis : list) {
//					resultDo = serviceFit.getTopicService().loadTopic(lis.getTopId());
//					if (lis.getVa() == 0) {// 当与操作值
//						lis.setVa(-1);
//					} else {
//						if ((lis.getVa() & interimForCount) != 0) {
//							lis.setVa(0);
//						} else {
//							lis.setVa(-1);
//						}
//					}
//				}
//				model.put("topic", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("menuSelected1", ConfigDo.GRADEALL_STRING);
//		model.put("menuSelected2", ConfigDo.TEACHERBYSTUDEBTGRADE);
//		return r.getName() + "/checkGradeView";
//	}
//
//	// 教师推荐自己小组学生参与公开答辩
//	@RequestMapping("TeaRecommend")
//	public String recommend(HttpSession session, Model model) {
//
//		Teacher teacher = (Teacher) session.getAttribute("user");
//		String groupid = teacher.getTeaGroupId();
//		int TeaIdentity = teacher.getTeaIdentity();
//		String TeaId = teacher.getTeaId();
//		model.addAttribute("Identity", TeaIdentity);
//		model.addAttribute("TeaId", TeaId);
//
//		ResultDo resultDo2 = serviceFit.getTeacherGroupService().findStudentBytgId(groupid, 0);
//		if (resultDo2.isSuccess()) {
//			model.addAttribute("TID", teacher.getTeaId());
//			model.addAttribute("TName", teacher.getTeaName());
//			model.addAttribute("whoRecommend", teacher.getTeaName());
//			model.addAttribute("recommend", resultDo2.getResult());
//		} else {
//
//			model.addAttribute("mag", resultDo2.getMessage());
//		}
//
//		model.addAttribute("menuSelected1", ConfigDo.TRECOMMEND);
//		model.addAttribute("menuSelected2", ConfigDo.TEARECMMEND);
//
//		return r.getName() + "/TeaCommend";
//
//	}
//
//	// 修改学生是否参与答辩
//	@RequestMapping("UpadteTeaRecommend")
//	public String updaterecommend(Map<String, Object> model, @RequestParam("stuId") String id,
//			@RequestParam("num") int num, @RequestParam("whoRecommend") String whoRecomment) {
//		if (whoRecomment.equals("1")) {
//			whoRecomment =null;
//		}
//		if (id != null && num > -1) {
//			boolean resultDo = serviceFit.getRecommendService().updateRecommend(id, num, whoRecomment);
//		}
//		return "redirect:./TeaRecommend.do";
//	}
//
//	// 教师推荐对接小组学生参与公开答辩
//	@RequestMapping("TeaRecommendOther")
//	public String Otherrecommend(HttpSession session, Model model, @RequestParam("tgId") String tgId) {
//
//		Teacher teacher = (Teacher) session.getAttribute("user");
//
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findStudentBytgId(tgId, 0);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("TName", teacher.getTeaName());
//				model.addAttribute("Identity", teacher.getTeaIdentity());
//				model.addAttribute("whoRecommend", teacher.getTeaName());
//				model.addAttribute("recommend", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据格式不正确！");
//		}
//
//		model.addAttribute("Tgid", tgId);
//		model.addAttribute("menuSelected1", ConfigDo.TRECOMMEND);
//		model.addAttribute("menuSelected2", ConfigDo.TEARECMMENDOTHER);
//		return r.getName() + "/TeaCommendOther";
//
//	}
//	// 对接小组修改学生是否参与答辩
//	@RequestMapping("UpadteTeaRecommendOther")
//	public String Otherupdaterecommend(Map<String, Object> model, @RequestParam("stuId") String id,
//			@RequestParam("num") int num, @RequestParam("tgId") String tgId,
//			@RequestParam("whoRecommend") String whoRecomment) {
//		if (whoRecomment.equals("1")) {
//			whoRecomment = null;
//		}
//		if (id != null && num > -1) {
//			boolean resultDo = serviceFit.getRecommendService().updateRecommend(id, num, whoRecomment);
//		}
//		return "redirect:./TeaRecommendOther.do?tgId=" + tgId;
//	}
//
//	/*
//	 * 未完成的功能
//	 */
//	// 教师推荐对接小组学生参与公开答辩
//	@RequestMapping("empty1")
//	public String Empty(Model model) {
//		model.addAttribute("menuSelected1", "GradeAll");
//		model.addAttribute("menuSelected2", "empty");
//		return r.getName() + "/empty";
//	}
//
//	@RequestMapping("empty2")
//	public String Empty1(Model model) {
//		model.addAttribute("menuSelected1", "docking");
//		model.addAttribute("menuSelected2", "empty");
//		return r.getName() + "/empty";
//	}
//	 @RequestMapping("finalGrade")
//	 public String finalReplyGrade(Model model,@RequestParam("stuId")String stuId,@RequestParam("finalReplyGrade")double finalGrade,@RequestParam("documentGrade")double docGrade,@RequestParam("tgId")String tgId){
//		ResultDo resultDo=serviceFit.getGradeService().retrofit(stuId,docGrade,finalGrade);
//		if(resultDo.isSuccess()){
//			return "redirect:./checkGrade.do?tgId="+tgId+"&name=finalGrade";
//		}
//		return "redirect:./checkGrade.do?tgId="+tgId+"&name=finalGrade";
//	 }
//	 @RequestMapping("restGrade")
//	 public String restGrade(Model model,@RequestParam("stuId")String stuId,@RequestParam("tgId")String tgId){
//		 ResultDo resultDo=serviceFit.getGradeService().resetRetrofit(stuId);
//		 if(resultDo.isSuccess()){
//			 return "redirect:./checkGrade.do?tgId="+tgId+"&name=finalGrade";
//		 }
//		 return "redirect:./checkGrade.do?tgId="+tgId+"&name=finalGrade";
//	 }
//}