package cn.edu.zut.soft.graduate.topicCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import lombok.Data;

/**
 * @author lxd
 * @date 17/2/24
 */
@Data
public class IssueAndIdentity {
    private Issue issue;
    private Identity identity;
}
