<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>答辩成绩</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table {
            font-size: 12px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">答辩成绩</li>
        </ol>
        <div class="page-header">
            <h4>答辩成绩
                <small>请按照自己需要条件进行检索</small>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12 ">
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    答辩小组
                </label>
                <div class="col-sm-5">
                    <select class="form-control search-select" id="groupId">
                        <option value="">请选择</option>
                    </select>
                </div>

                <label class="col-sm-1 control-label">
                    成绩
                </label>
                <div class="col-sm-5">
                    <select class="form-control search-select" title="状态列表" id="systemGradeStatus">
                        <option value="">请选择</option>
                        <option value="excellenceGrade">优秀</option>
                        <option value="wellGrade">良好</option>
                        <option value="passGrade">合格</option>
                        <option value="noPassGrade">不合格</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    姓名
                </label>
                <div class="col-sm-5">
                    <input name="title" class="form-control" placeholder="学生姓名" id="name">
                </div>

                <div class="col-sm-1" style="text-align: right;">
                    <button id="findStu" class="btn btn-primary">检索</button>
                </div>
                <div class="col-sm-1">
                <a  class="btn btn-primary"  href="/file/downloadFinalGrade">最终成绩数据导出</a>
                </div>
            </div>
        </div>
        <div class="form-group" id="exportTable">
        </div>
        <div class="form-group" id="exportTableTwo">
        </div>

    </div>
</div>


<div class="modal fade" id="saveGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">答辩成绩 <small>给予成绩时,若有成绩请不要填写分数,该操作仅为保存操作,可单个或多个保存!</small></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="StuIdOne">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuName">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClass">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopic">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildTopic">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            指导成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="指导成绩" id="guideGrade">
                        </div>
                        <span class="help-inline col-sm-4" id="guideGradeOne">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            评阅成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="评阅成绩" id="reportGrade">
                        </div>
                        <span class="help-inline col-sm-4" id="reportGradeOne">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="答辩成绩" id="replyGrade">
                        </div>
                        <span class="help-inline col-sm-4" id="replyGradeOne">
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="replyGradeSubmitOne">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置部分答辩成绩 <small>该重置操作仅为未生成最终成绩前的部分成绩重置,该操作仅为更新操作,可单个或多个重置!</small></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="stuIdTwo">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuNameTwo">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClassTwo">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopicTwo">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildTopicTwo">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            指导成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="指导成绩" id="guideGradeTwo">
                        </div>
                        <span class="help-inline col-sm-4" id="guideGradeInfoTwo">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            评阅成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="评阅成绩" id="reportGradeTwo">
                        </div>
                        <span class="help-inline col-sm-4" id="reportGradeInfoTwo">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="答辩成绩" id="replyGradeTwo">
                        </div>
                        <span class="help-inline col-sm-4" id="replyGradeInfoTwo">
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="resteplyGradeOne">重置</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restFinalGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置最终答辩成绩 <small>该操作针对学生符合规则的3个成绩的重置,重新计算学生最终答辩成绩,可单个或多个重置!</small></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="stuIdThree">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuNameThree">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClassThree">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopicThree">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildTopicThree">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            指导成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="指导成绩" id="guideGradeThree">
                        </div>
                        <span class="help-inline col-sm-4" id="guideGradeInfoThree">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            评阅成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="评阅成绩" id="reportGradeThree">
                        </div>
                        <span class="help-inline col-sm-4" id="reportGradeInfoThree">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="答辩成绩" id="replyGradeThree">
                        </div>
                        <span class="help-inline col-sm-4" id="replyGradeInfoThree">
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restFinaleGrade">重置</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>



<div class="modal fade" id="clearGuideGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置指导成绩 </h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="stuIdFour">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuNameFour">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClassFour">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopicFour">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildTopicFour">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                        </label>
                        <span class="help-inline col-sm-4" id="guideGradeInfoFour">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                        </label>
                        <span class="help-inline col-sm-4" id="reportGradeInfoFour">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                        </label>
                        <span class="help-inline col-sm-4" id="replyGradeInfoFour">
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="clearGuideButton">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="clearReplyGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置评阅成绩和答辩成绩 </h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="stuIdFive">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuNameFive">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClassFive">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopicFive">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildFive">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                        </label>
                        <span class="help-inline col-sm-4" id="guideGradeInfoFive">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                        </label>
                        <span class="help-inline col-sm-4" id="reportGradeInfoFive">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                        </label>
                        <span class="help-inline col-sm-4" id="replyGradeInfoFive">
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="clearReplyButton">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">
    var $groupIdTea = '';
    var $name = '';
    var $status = '';
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: "/admin/getAllTeaGroup",
        data: {
            pageSize: 1000
        },
        success: function (result) {
            if (result.success) {
                var data = result.data;
                $.each(data, function (key, value) {
                    $("#groupId").append("<option value=" + value.id + ">" + value.identityName + "</option>");
                })
            }
        }
    });

    $("#findStu").click(function () {
        selectData();
    });

    function selectData() {
        $groupIdTea = $("#groupId").val();
        $name = $("#name").val();
        $status = $("#systemGradeStatus").val();

        $("#exportTableTwo").html('<table id="table" class="table table-container"></table>');
        var columns = [
            {
                field: 'identity.id',
                title: '编号'
            },
            {
                field: 'identity.noId',
                title: '学号'
            }, {
                field: 'identity.name',
                title: '姓名'
            }, {
                title: '班级',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                            str += array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '指导教师',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '课题',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var $strOne = "";
                    var $aone = "<a href='/admin/issue?id=";
                    var $atwo = "'>";
                    var $athree = "</a>";
                    var $result = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'topicName') {
                            $strOne = array[i].value;
                            break;
                        }
                    }
                    for (var j = 0; j < array.length; j++) {
                        if (array[j].type == "TOPIC" && array[j].key == "topicId") {
                            $result = $aone + array[j].value + $atwo + $strOne + $athree;
                            break;
                        } else {
                            $result = $strOne;
                        }
                    }
                    return $result;
                }
            }, {
                title: '子任务',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '指导成绩',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "未操作";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'guideGrade') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '评阅成绩',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "未操作";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'reportGrade') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '答辩成绩',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "未操作";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'replyGrade') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '最终成绩',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "未操作";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'finalReplyGrade') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '操作',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "未开放";
                    var $roleStu = true;
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'finalReplyGrade') {
                            str = "<a  data-toggle='modal' data-target='#restFinalGradeModel'>重置最终成绩</a>" + "<a  data-toggle='modal' data-target='#clearGuideGradeModel'>重置指导成绩</a>" + "<a  data-toggle='modal' data-target='#clearReplyGradeModel'>重置答辩成绩</a>";
                            $roleStu = false;
                            break;
                        }
                    }
                    if ($roleStu) {
                        str = "<a  data-toggle='modal' data-target='#saveGradeModel'>给予成绩</a>" + "<a  data-toggle='modal' data-target='#restGradeModel'>重置部分成绩</a>";
                    }
                    return str;
                }
            }

        ];
        customSearch({
            id: "#table",
            url: "/identity/getGradeStatus",
            dataType: "json",
            params: {groupId: $groupIdTea, name: $name, status: $status},
            columns: columns,
            pageSize: 25
        });
    }

    $("#dateExpert").click(function () {
        $groupIdTea = $("#groupId").val();
        $name = $("#name").val();
        $status = $("#systemGradeStatus").val();
        window.location.href = '/file/downloadGradeStatus?groupId=' + $groupIdTea + '&name=' + $name + '&status=' + $status;
    });

    $(function () {
        selectData();
    });


    $("#saveGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(6).innerHTML);
        var $guideGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(7).innerHTML);
        var $reportGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(8).innerHTML);
        var $replyGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(9).innerHTML);
        $("#stuName").html('学生姓名:' + $stuName);
        $("#stuClass").html('班级:' + $stuClass);
        $("#stuTopic").html('题目:'+ $stuTopic);
        $("#stuChildTopic").html('子标题:'+ $stuChildTopic);
        $("#StuIdOne").attr("value", $stuId);
        $("#guideGradeOne").html('指导成绩:' + $guideGrade);
        $("#reportGradeOne").html('评阅成绩:' + $reportGrade);
        $("#replyGradeOne").html('答辩成绩:' + $replyGrade);
    });


    $("#restGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(6).innerHTML);
        var $guideGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(7).innerHTML);
        var $reportGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(8).innerHTML);
        var $replyGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(9).innerHTML);
        $("#stuNameTwo").html('学生姓名:' + $stuName);
        $("#stuClassTwo").html('班级:' + $stuClass);
        $("#stuTopicTwo").html('题目:'+ $stuTopic);
        $("#stuChildTopicTwo").html('子标题:'+ $stuChildTopic);
        $("#stuIdTwo").attr("value", $stuId);
        $("#guideGradeInfoTwo").html('指导成绩:' + $guideGrade);
        $("#reportGradeInfoTwo").html('评阅成绩:' + $reportGrade);
        $("#replyGradeInfoTwo").html('答辩成绩:' + $replyGrade);
    });

    $("#restFinalGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(6).innerHTML);
        var $guideGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(7).innerHTML);
        var $reportGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(8).innerHTML);
        var $replyGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(9).innerHTML);
        $("#stuNameThree").html('学生姓名:' + $stuName);
        $("#stuClassThree").html('班级:' + $stuClass);
        $("#stuTopicThree").html('题目:'+ $stuTopic);
        $("#stuChildTopicThree").html('子标题:'+ $stuChildTopic);
        $("#stuIdThree").attr("value", $stuId);
        $("#guideGradeInfoThree").html('指导成绩:' + $guideGrade);
        $("#reportGradeInfoThree").html('评阅成绩:' + $reportGrade);
        $("#replyGradeInfoThree").html('答辩成绩:' + $replyGrade);
    });

    $("#clearGuideGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(6).innerHTML);
        var $guideGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(7).innerHTML);
        var $reportGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(8).innerHTML);
        var $replyGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(9).innerHTML);
        $("#stuNameFour").html('学生姓名:' + $stuName);
        $("#stuClassFour").html('班级:' + $stuClass);
        $("#stuTopicFour").html('题目:'+ $stuTopic);
        $("#stuChildTopicFour").html('子标题:'+ $stuChildTopic);
        $("#stuIdFour").attr("value", $stuId);
        $("#guideGradeInfoFour").html('指导成绩:' + $guideGrade);
        $("#reportGradeInfoFour").html('评阅成绩:' + $reportGrade);
        $("#replyGradeInfoFour").html('答辩成绩:' + $replyGrade);
    });


    $("#clearReplyGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(6).innerHTML);
        var $guideGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(7).innerHTML);
        var $reportGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(8).innerHTML);
        var $replyGrade = $.trim($(e.relatedTarget).parent().parent().children("td").get(9).innerHTML);
        $("#stuNameFive").html('学生姓名:' + $stuName);
        $("#stuClassFive").html('班级:' + $stuClass);
        $("#stuTopicFive").html('题目:'+ $stuTopic);
        $("#stuChildFive").html('子标题:'+ $stuChildTopic);
        $("#stuIdFive").attr("value", $stuId);
        $("#guideGradeInfoFive").html('指导成绩:' + $guideGrade);
        $("#reportGradeInfoFive").html('评阅成绩:' + $reportGrade);
        $("#replyGradeInfoFive").html('答辩成绩:' + $replyGrade);
    });



    $("#replyGradeSubmitOne").click(function () {
        var $stuId = $("#StuIdOne").val();
        var $guide = $("#guideGrade").val();
        var $report = $("#reportGrade").val();
        var $reply = $("#replyGrade").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/saveReplyGradeByAdmin',
            data: {
                stuId: $stuId,
                reportGrade: $report,
                replyGrade:$reply,
                guideGrade:$guide
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    if(result.errorMsg == undefined || result.errorMsg == '' || result.errorMsg == null || result.errorMsg == "" ){
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                        window.location.reload();
                    }
                }
            }
        });
    });

    $("#resteplyGradeOne").click(function () {
        var $stuId = $("#stuIdTwo").val();
        var $guide = $("#guideGradeTwo").val();
        var $report = $("#reportGradeTwo").val();
        var $reply = $("#replyGradeTwo").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/restReplyGradeByAdmin',
            data: {
                stuId: $stuId,
                reportGrade: $report,
                replyGrade:$reply,
                guideGrade:$guide
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    if(result.errorMsg == undefined || result.errorMsg == '' || result.errorMsg == null || result.errorMsg == "" ){
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                        window.location.reload();
                    }
                }
            }
        });
    });

    $("#restFinaleGrade").click(function () {
        var $stuId = $("#stuIdThree").val();
        var $guide = $("#guideGradeThree").val();
        var $report = $("#reportGradeThree").val();
        var $reply = $("#replyGradeThree").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/restFinalGrade',
            data: {
                stuId: $stuId,
                reportGrade: $report,
                replyGrade:$reply,
                guideGrade:$guide
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    if(result.errorMsg == undefined || result.errorMsg == '' || result.errorMsg == null || result.errorMsg == "" ){
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                        window.location.reload();
                    }
                }
            }
        });
    });

    $("#clearGuideButton").click(function () {
        var $stuId = $("#stuIdFour").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/clearGuide',
            data: {
                stuId: $stuId
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    if(result.errorMsg == undefined || result.errorMsg == '' || result.errorMsg == null || result.errorMsg == "" ){
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                        window.location.reload();
                    }
                }
            }
        });
    });

    $("#clearReplyGradeModel").click(function () {
        var $stuId = $("#stuIdFive").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/clearReply',
            data: {
                stuId: $stuId
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    if(result.errorMsg == undefined || result.errorMsg == '' || result.errorMsg == null || result.errorMsg == "" ){
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                        window.location.reload();
                    }
                }
            }
        });
    });

</script>

</body>
</html>
