package cn.edu.zut.soft.graduate.massageCenter.bo;

import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.model.Impl.Message;
import cn.edu.zut.soft.graduate.core.query.MassageQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.massageCenter.generator.Builder;
import cn.edu.zut.soft.graduate.massageCenter.pojo.SendMessage;

import java.util.List;

/**
 * Created by chuchuang on 16/11/13.
 */
public interface MessageBO extends BasicBO<Message,MassageQuery> {

    Integer notRead(Integer id);

    Message read(Integer id, String operator);

    /**
     * 发送信息
     * @param loginVO
     * @param ids
     * @param builder
     * @return 拦截发送失败的情况,并返回发送情况
     */
    SendMessage AssemblyAndSend(LoginVO loginVO, List<Integer> ids, Builder builder);

}
