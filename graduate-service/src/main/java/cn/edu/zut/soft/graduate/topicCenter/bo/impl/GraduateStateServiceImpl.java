package cn.edu.zut.soft.graduate.topicCenter.bo.impl;

import cn.edu.zut.soft.graduate.core.constant.*;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.GraduateStateVO;
import cn.edu.zut.soft.graduate.core.vo.IssueVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.TeacherGroupBO;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportCenterService;
import cn.edu.zut.soft.graduate.topicCenter.bo.GraduateStateService;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
@Service
public class GraduateStateServiceImpl implements GraduateStateService {

    @Resource
    private IdentityBO identityBO;

    @Resource
    private ReportCenterService reportCenterService;

    @Resource
    private TeacherGroupBO teacherGroupBO;

    @Resource
    private InfoBO infoBO;

    @Value("#{commonProp['randomDrawingFilePath']}")
    private String randomDrawingFilePath;


    @Override
    public GraduateStateVO findByUser(Integer userId) {
        UserDescVO userDescVO = identityBO.getUserDesc(userId);
        if (userDescVO == null) {
            return null;
        }
        GraduateStateVO graduateStateVO = new GraduateStateVO();
        graduateStateVO.setUser(userDescVO);
        graduateStateVO.setIssue(new IssueVO(userDescVO.getIssue()));
        graduateStateVO.setTask(userDescVO.getTask());

        /*
          指导教师
         */
        Info tmp = filterInfo(userDescVO.getInfoList(), "Tea", IKEY.Guide);
        if (tmp != null) {
            graduateStateVO.setTeacher(identityBO.get(Integer.parseInt(tmp.getValue())));
        }
        /*
          一遍成绩
         */
        tmp = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.OPEN_GRADE + IKEY.ONE_REPLY);
        if (tmp == null) {
            tmp = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.OPEN_GRADE + IKEY.TWO_REPLY);
            if (tmp != null) {
                graduateStateVO.setStartStatus(ScheduleStatus.TWO);
                graduateStateVO.setFinallyOpenStatus(BasicStatus.YES);
            } else {
                graduateStateVO.setStartStatus(ScheduleStatus.ERROR);
                tmp = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.OPEN_GRADE);
                if (tmp != null && "NO".equals(tmp.getValue())) {
                    graduateStateVO.setFinallyOpenStatus(BasicStatus.NO);
                } else {
                    graduateStateVO.setFinallyOpenStatus(BasicStatus.ING);
                }
            }
        } else {
            graduateStateVO.setStartStatus(ScheduleStatus.ONE);
            graduateStateVO.setFinallyOpenStatus(BasicStatus.YES);
        }
        tmp = filterInfo(userDescVO.getInfoList(), "Basic", IKEY.ADDRESS);
        if (tmp != null) {
            graduateStateVO.setStartAddress(Address.valueOf(tmp.getValue()));
        } else {
            graduateStateVO.setStartAddress(Address.zhengzhou);
        }

        /*
         是否抽取并应用
         */
        if (detectionMidRan()) {
            tmp = filterInfo(userDescVO.getInfoList(), "GRADE", "MID");
            if (tmp != null) {
                graduateStateVO.setDidAddress(true);
            } else {
                graduateStateVO.setDidAddress(false);
            }
        }
        /**
         * 中期结果
         */
        tmp = filterInfo(userDescVO.getInfoList(),"GRADE",IKEY.INTERIM_GRADE);
        if (tmp != null){
            graduateStateVO.setFinallyDidStatus(Integer.parseInt(tmp.getValue()));
        }

        /**
         * 系统验收
         */
        Info systemGrade = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.SYSTEM_GRADE);
        Info systemTwoGrade = filterInfo(userDescVO.getInfoList(), "GRADE", GradeTimeType.SYSTEM_GRADE_TWO.getKey());
        Info replyOneGrade = filterInfo(userDescVO.getInfoList(), "GRADE", GradeTimeType.REPLY_GRADE_ONE.getKey());
        Info replyTwoGrade = filterInfo(userDescVO.getInfoList(), "GRADE", GradeTimeType.REPLY_GRADE_TWO.getKey());
        Info stopGrade = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.GRADE_STOP_KEY);
        if (stopGrade == null){
            if (systemGrade == null){
                //未操作
                graduateStateVO.setSystemAcceptanceStatus("教师未操作");
            }else{
                //二次验收
                if (systemTwoGrade != null){
                    graduateStateVO.setSystemAcceptanceStatus("二次系统验收");
                }else if (replyOneGrade != null){
                    //进入一辩
                    graduateStateVO.setSystemAcceptanceStatus("进入一辩");
                }else if (replyTwoGrade != null){
                    //进入二辩
                    graduateStateVO.setSystemAcceptanceStatus("进入二辩");
                }
            }
        }else{
            //终止答辩
            graduateStateVO.setSystemAcceptanceStatus("已终止答辩");
        }



        /**
         * 教师小组
         */


        graduateStateVO.setTeacherGroup(teacherGroupBO.getByTeacher(graduateStateVO.getTeacher().getId()));

        /**
         * 周月报统计
         */
        graduateStateVO.setReportCount(reportCenterService.countByUser(userId));

        /**
         * 最后报告
         */
        Info guideGrade = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.GUIDE_GRADE);
        Info reportGrade = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.REPORT_GRADE);
        Info replyGrade = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.REPLY_GRADE);
        Info finalGrade = filterInfo(userDescVO.getInfoList(), "GRADE", IKEY.FINAL_REPLY_GRADE);
        if (guideGrade != null){
            if (StringUtils.isNotBlank(guideGrade.getValue())){
                graduateStateVO.setGuideGrade(guideGrade.getValue());
            }
        }
        if (reportGrade != null){
            if (StringUtils.isNotBlank(reportGrade.getValue())){
                graduateStateVO.setReportGrade(reportGrade.getValue());
            }
        }
        if (replyGrade != null){
            if (StringUtils.isNotBlank(replyGrade.getValue())){
                graduateStateVO.setReplyGrade(replyGrade.getValue());
            }
        }
        if (finalGrade != null){
            if (StringUtils.isNotBlank(finalGrade.getValue())){
                graduateStateVO.setFinalGrade(finalGrade.getValue());
            }
        }
        return graduateStateVO;
    }

    @Override
    public Boolean detectionMidRan() {
        InfoQuery infoQuery = new InfoQuery();
        infoQuery.setRole(Role.stu);
        infoQuery.setInfoType(InfoType.GRADE);
        infoQuery.setKey("MID");
        int count = infoBO.countByQuery(infoQuery);
        return count != 0;
    }


    private Info filterInfo(List<Info> infoList, String type, String key) {
        for (Info info : infoList) {
            if (info.getType().equals(type) && info.getKey().equals(key)) {
                return info;
            }
        }
        return null;
    }
}
