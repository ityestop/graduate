<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/11/24
  Time: 下午7:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>分配学生</title>
    <script src="IAssets/js/selectCheckbox.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">分配学生</li>
        </ol>
        <div class="page-header">
            <h3>分配学生 	<font color="red"><small>分配学生为分配未拥有课题和指导教师的学生</small></font></h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <form name="addZJ" action="./${sessionScope.pathCode}/upFpxs.do" method="post">
                <input type="hidden" name="id" value="${teaId}">
                <table class="table  table-hover">
                    <thead>
                    <tr>
                        <th>
                            <label>
                               <input type="checkbox" id="selectAll"/><small>全选</small>
                            </label>
                        </th>
                        <th>
                            <small>学号</small>
                        </th>
                        <th>
                            <small>姓名</small>
                        </th>
                        <th>
                            <small>性别</small>
                        </th>
                        <th>
                            <small>班级</small>
                        </th>
                        <th>
                            <small>联系方式</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="stu">
                        <tr>
                            <td>
                                <label>
                                	
                                    <input type="checkbox" name="stuId" value="${stu.stuId}"/>
                                </label>
                            </td>
                            <td>
                                <small>${stu.stuStudentId}</small>
                            </td>
                               <td>
                                <small>${stu.stuName}</small>
                            </td>
                              <c:choose>
                            <c:when test="${stu.stuSex eq false }">
                                <td>
                                    <small>男</small>
                                </td>
                            </c:when>
                            <c:when test="${stu.stuSex eq true }">
                                <td>
                                    <small>女</small>
                                </td>
                            </c:when>
                        </c:choose>
                            <td>
                                <small>${stu.stuClass}</small>
                            </td>
                            <td>
                                <small>${stu.stuTell}</small>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
              <div class="from-group">
                     <label class="col-sm-3 control-label" ></label>
                       <div class="col-sm-4">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-blue btn-block" onclick="submitForm()">
                              	 <i class="clip-checkmark-2  "></i>   分配
                            </button>
                        </div>
                        <div class="col-sm-6">
									<button type="button" class="btn btn-blue btn-block"
										onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i>   返回</button>
								</div>
						</div>
                    </div>
                    
            </form>
        </div>
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
</body>
</html>
