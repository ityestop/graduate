package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Message extends BaseModelImpl {
    private Integer addressor;//发件人
    private Integer recipients;//收件人
    private Integer read;//是否已读
    private String title;
    private String content;//文件内容
    private String accessory;//附件
    private String type;//消息类型

    private String addressorName;//发件人姓名
    private String recipientsName;//收件人姓名

}
