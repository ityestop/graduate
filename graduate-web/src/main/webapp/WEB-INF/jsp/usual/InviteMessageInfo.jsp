<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>邀请信息操作</title>
    <script src="/assets/g/js/source.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/tea"> 首页 </a></li>
            <li class="active"></li>
        </ol>
        <div class="page-header">
            <h4>邀请信息</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->


<div class="row">
    <div class="panel-body">
        <input type="hidden" value="${group.id}" id="groupId_invite">
        <table class="table" id="sample-table-1">
            <tbody>
            <tr>
                <td width="10%" class="center"><small>邀请标题:</small></td>
                <td><small><span id="messageTitle">${group.identityName} 邀请你加入小组</span></small></td>
            </tr>
            </tbody>
        </table>
        <div id="issueAuditIng">
            <label class="col-sm-6 control-label"></label>
            <div class="col-sm-2">
                <button type="button" class="btn btn-blue btn-block" id="submitIuessPass" >
                    <span><i class="clip-checkmark-2  "></i>   同意</span>
                </button>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-blue btn-block"
                        id="remarkAndRefuse">
                    <span><i class=" clip-cancel-circle  "></i>   拒绝</span>
                </button>
            </div>
        </div>
        <div class="col-sm-2" style="float: right">
            <button type="button" class="btn btn-blue btn-block"
                    onclick="window.location.href='javascript:history.go(-1)'">
                <span><i class=" clip-arrow-right-2  "></i> 返回</span>
            </button>
        </div>



    </div>
</div>

<script type="application/javascript">
    var groupID = $("#groupId_invite").val();
    $("#submitIuessPass").click(function () {

        $.ajax({
            type:'post',
            url:'./group/join/YES',
            dataType: "json",
            data:{
                groupId:groupID,
            },
            success:function (result) {
                if(result.success){
                    window.location.href="./stu/stuGroup";
//                    window.location.replace("./stu/stuGroup");
                }else{
                    alert(result.errorMsg);
                }
            }
        })

    });

    $("#remarkAndRefuse").click(function () {
        $.ajax({
            type:'post',
            url:'./group/join/NO',
            dataType: "json",
            data:{
                groupId:groupID,
            },
            success:function (result) {
                if(result.success){
//                    window.location.replace("./stu/inbox");
                    alert("你确定拒绝该邀请？");
                    window.location.href="./stu/inbox";
                }else{
                    alert(result.errorMsg);
                }
            }
        })
    });
</script>

</body>
</html>
