
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>

<title>教师答辩小组</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">小组学生</li>
			</ol>
			<div class="page-header">
				<c:if test="${number eq 1 }">
					<h3>小组学生</h3>
				</c:if>
				<c:if test="${number eq 2 }">
					<h3>对接小组学生</h3>
				</c:if>
				<c:if test="${number eq 3 }">
					<h3>中期检查</h3>
				</c:if>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<div>
					<label class="col-sm-10"></label>
					<div class="col-sm-2">
						<c:if test="${number eq 1 }">
							<a class="btn btn-blue btn-block"
								href="./${sessionScope.pathCode}/download.do?tgId=${sessionScope.user.teaGroupId}">
								学生信息导出</a>
						</c:if>
						<c:if test="${number eq 2 }">
							<a class="btn btn-blue btn-block"
								href="./${sessionScope.pathCode}/downloadDefense.do?tgId=${sessionScope.doucking}">
								学生信息导出</a>
						</c:if>
					</div>
				</div>
				<table class="table  table-hover"
					style="TABLE-LAYOUT: fixed; WORD-BREAK: break-all">
					<thead>
						<tr>
							<th width="14%"><small>学号</small></th>
							<th><small>姓名</small></th>
							<th><small>班级</small></th>
							<c:if test="${number ne 3 }">
								<th><small>联系方式</small></th>
							</c:if>
							<th width="14%"><small>答辩题目</small></th>
							<th width="8%"><small>校外导师</small></th>
							<th><small>指导教师</small></th>
							<th width="8%"><small>教师职称</small></th>
							<th width="7%"><small> ${number eq 3 ? "中期进度" : "指导成绩" }
							</small></th>
							<th><small>题目来源</small></th>
						</tr>
					</thead>
					<c:forEach items="${stulist}" var="t">
						<tr>

							<td><small>${t.stuStudentId}</small></td>
							<td><small>${t.stuName }</small></td>
							<td><small>${t.stuClass}</small></td>
							<c:if test="${number ne 3 }">
								<td><small>${t.stuTell }</small></td>
							</c:if>

							<c:choose>
								<c:when test="${t.topTitle ne null }">
									<c:if
										test="${t.childTitle eq null or t.childTitle eq '' or t.childTitle eq 'null' }">
										<td
											style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
											title="${t.topTitle}"><small><a
												href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
													${t.topTitle}</a></small></td>
									</c:if>
									<c:if
										test="${t.childTitle ne null and t.childTitle ne '' and t.childTitle ne 'null' }">
										<td
											style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
											title="${t.topTitle}-${t.childTitle}"><small><a
												href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
													${t.topTitle}</a></small></td>
									</c:if>
								</c:when>
								<c:when test="${t.topTitle eq null }">
									<td
										style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><small><a
											href="javaScript:void(0);"> 无</a></small></td>
								</c:when>
							</c:choose>
							<td><small>${t.topOutTeacher eq null ? "无" :t.topOutTeacher }</small></td>
							<td><small>${t.teaName}</small></td>
							<c:choose>
								<c:when test="${t.teaQualifications eq 0}">
									<td><small>无</small></td>
								</c:when>
								<c:when test="${t.teaQualifications eq 1}">
									<td><small>助教</small></td>
								</c:when>
								<c:when test="${t.teaQualifications eq 2}">
									<td><small>讲师</small></td>
								</c:when>
								<c:when test="${t.teaQualifications eq 3}">
									<td><small>副教授</small></td>
								</c:when>
								<c:when test="${t.teaQualifications eq 4}">
									<td><small>教授</small></td>
								</c:when>

							</c:choose>
							</td>
							<td><small> <c:if test="${number eq 3 }">
										<c:if test="${t.interimGrade eq 90}">
                            		进度较好
                            		</c:if>
										<c:if test="${t.interimGrade eq 80}">
                            		进度正常
                            		</c:if>
										<c:if test="${t.interimGrade eq 60}">
                            		进度滞后
                            		</c:if>
										<c:if test="${t.interimGrade eq -1}">
                            		终止答辩
                            		</c:if>
										<c:if test="${t.interimGrade eq 0}">
                            		${number eq 3 ? "未审批" : "教师未审批 " }
                            		</c:if>
									</c:if> <c:if test="${number eq 1 || number eq 2 }">	
									<fmt:formatNumber value="${t.donGrade}" pattern="0" />
								</c:if>
							</small></td>
							<c:choose>
								<c:when test="${t.topSource eq 3}">
									<td><small>自拟题目</small></td>
								</c:when>
								<c:when test="${t.topSource lt 3}">
									<td><small>校内题目</small></td>
								</c:when>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
			</div>

		</div>
		<!-- end: TABLE WITH IMAGES PANEL -->
	</div>
	</div>
	<script>
		$('#assignIndetity').on('show.bs.modal', function(e) {
			var $p = $(e.relatedTarget).parent().parent().parent();
			alert($p.html());
			var $teaId = $p.find(".teaId");
			var $teaName = $p.find(".teaName");
			var $tgId = $p.find(".tgId");
			var $modal = $(this);
			$modal.find("input[name='teaName']").val($teaName.val());
			$modal.find("input[name='teaId']").val($teaId.val());
			$modal.find("input[name='tgId']").val($teaId.val());
			alert($tgId.val());
		});
	</script>

	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			FormElements.init();
		});
	</script>

</body>
</html>
