package cn.edu.zut.soft.graduate.core.annotation;

import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.basic.core.model.dao.Page;

/**
 * @author cc
 * @Date 2017/2/19
 * @value 1.0
 */
public interface QueryParameterHandler {
    void handler(Criteria criteria,Page page) throws IllegalAccessException;
}
