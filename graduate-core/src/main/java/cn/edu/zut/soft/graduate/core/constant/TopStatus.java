package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by cc on 15/10/31.
 */
public enum TopStatus implements Status {
    SUCCESS("已报满",0),AUDIT("审核中",1),ING("选报中",2),INVALID("未通过",3),SAVE("未提交",4);

    private String name;
    private int index;

    TopStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public String toString() {
        return "name:" + name +" index:" + index;
    }

    public static TopStatus getName(int index){
        for (TopStatus s : TopStatus.values()){
            if (s.getIndex() == index){
                return s;
            }
        }
        return null;
    }

    public static TopStatus getIndex(String name){
        for (TopStatus s : TopStatus.values()){
            if (s.getName().equals(name)){
                return s;
            }
        }
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }



}
