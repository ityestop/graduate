package cn.edu.zut.soft.graduate.massageCenter.bo;

import cn.edu.zut.soft.graduate.core.constant.LoginMode;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.massageCenter.generator.Impl.InviteGroupBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;

/**
 * Created by cc on 2016/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/bean-all.xml")
public class MessageBOImplTest {

    @Autowired
    private MessageBO messageBO;

    @Autowired
    private InviteGroupBuilder inviteGroupBuilder;

    @Test
    public void notRead() throws Exception {

    }

    @Test
    public void read() throws Exception {

    }

    @Test
    public void save() throws Exception {

    }

    @Test
    public void get() throws Exception {

    }

    @Test
    public void findByQuery() throws Exception {

    }

    @Test
    public void AssemblyAndSend() throws Exception {
        LoginVO loginVO = new LoginVO();
        loginVO.setId(62);
        loginVO.setExpert(false);
        loginVO.setName("楚闯");
        loginVO.setRole(Role.stu.name());
        loginVO.setLoginMode(LoginMode.PHONE);
        messageBO.AssemblyAndSend(loginVO, Collections.singletonList(94),inviteGroupBuilder);
    }

}