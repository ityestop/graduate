package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.GradeTimeType;
import cn.edu.zut.soft.graduate.core.constant.GradeType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicGradeStrategy;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Grade;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.gradesCenter.bo.GradeBO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>文件名称：GradeController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/5 下午11:41</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RequestMapping("/grade")
@Controller
public class GradeController extends BaseController {

    @Resource
    private GradeBO gradeBO;

    @ResponseBody
    @RequestMapping("/passOpenGrade")
    public CommonResult passOpenGrade(@RequestParam("grade") String grade, @RequestParam("id") Integer id, HttpServletRequest request, @RequestParam("time") Integer time) {
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        if (StringUtils.isEmpty(grade) || id == null || time == null) {
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(gradeBO.save(new BasicGradeStrategy(GradeType.Open, IKEY.OPEN_GRADE, grade), id, loginVO.getName(),time));
    }

    @ResponseBody
    @RequestMapping("/restOpenGrade")
    public CommonResult restOpenGrade(HttpServletRequest request, @RequestParam("grade") String grade, @RequestParam("id") Integer id,@RequestParam("time") Integer time){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        if (StringUtils.isEmpty(grade) || id == null){
            return CommonResult.errorReturn("System error");
        }
        Grade result = null;
        if (time == IKEY.END){
            result = gradeBO.restGradeRecorde(new BasicInfoStrategy(GradeType.Open, IKEY.GRADE_STOP_KEY, grade), id, loginVO.getName(), time);
        }else{
            result = gradeBO.restGradeRecorde(new BasicInfoStrategy(GradeType.Open, IKEY.OPEN_GRADE, grade), id, loginVO.getName(), time);
        }
        if (result == null){
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(result);
    }

    @ResponseBody
    @RequestMapping("/restSecondGrade")
    public CommonResult restSecondGrade(HttpServletRequest request, @RequestParam("grade") String grade, @RequestParam("id") Integer id, @RequestParam("times") Integer times){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        if (StringUtils.isEmpty(grade) || id == null || times ==null){
            return CommonResult.errorReturn("System error");
        }
        Grade result = null;
        if (times == IKEY.END){
            result = gradeBO.restGradeRecorde(new BasicInfoStrategy(GradeType.Open, IKEY.GRADE_STOP_KEY, grade), id, loginVO.getName(), times);
        }else{
            result = gradeBO.restGradeRecorde(new BasicInfoStrategy(GradeType.Open, IKEY.OPEN_GRADE, grade), id, loginVO.getName(), times);
        } if (result == null){
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(result);
    }

    @ResponseBody
    @RequestMapping("/findAllGrade")
    public CommonResult findAllGrade(HttpSession session){
        LoginVO loginVO = (LoginVO)session.getAttribute(IKEY.USER);
        if (Role.stu.name().equals(loginVO.getRole())){
            return CommonResult.successReturn(gradeBO.findAllGrade(loginVO));
        }
        return CommonResult.errorReturn("role is error");
    }

    @ResponseBody
    @RequestMapping("/passInterimGrade")
    public CommonResult passInterimGrade(HttpServletRequest request, @RequestParam("grade") String grade, @RequestParam("id") Integer id){
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        if (StringUtils.isEmpty(grade) || id == null) {
            return CommonResult.errorReturn("System error");
        }
        if (grade.equals("-1")){
            return gradeBO.saveInterimGrade(new BasicGradeStrategy(GradeType.Interim, IKEY.GRADE_STOP_KEY, grade), id, loginVO.getName());
        }else {
            return gradeBO.saveInterimGrade(new BasicGradeStrategy(GradeType.Interim, IKEY.INTERIM_GRADE, grade), id, loginVO.getName());
        }
    }

    @ResponseBody
    @RequestMapping("/restInterimGrade")
    public CommonResult restInterimGrade(HttpServletRequest request, @RequestParam("grade") String grade, @RequestParam("id") Integer id){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        if (StringUtils.isEmpty(grade) || id == null){
            return CommonResult.errorReturn("System error");
        }
        if (grade.equals("-1")){
             return gradeBO.restInterimGrade(new BasicGradeStrategy(GradeType.Interim, IKEY.GRADE_STOP_KEY, grade), id, loginVO.getName());
        }else {
            return gradeBO.restInterimGrade(new BasicGradeStrategy(GradeType.Interim, IKEY.INTERIM_GRADE, grade), id, loginVO.getName());
        }
    }

    @ResponseBody
    @RequestMapping("/passSystemGrade")
    public CommonResult restSystemGrade(HttpServletRequest request, @RequestParam("stuId") Integer stuId, @RequestParam("code") Integer code ){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        if (stuId == null ||  code == null){
            return CommonResult.errorReturn("restSystemGrade System param error !");
        }
        CommonResult<Boolean> result = null;
        switch (code){
            case 1001:
                result = gradeBO.saveSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "NO"),stuId , GradeTimeType.SYSTEM_GRADE_ONE, loginVO.getName());
                break;
            case 1002:
                result = gradeBO.saveSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "YES"),stuId , GradeTimeType.SYSTEM_GRADE_ONE, loginVO.getName());
                break;
            case 1003:
                result = gradeBO.saveSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.GRADE_STOP_KEY, IKEY.SYSTEM_GRADE),stuId , GradeTimeType.GRADE_STOP, loginVO.getName());
                break;
            case 2001:
                result = gradeBO.restSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "NO"),stuId , GradeTimeType.SYSTEM_GRADE_ONE, loginVO.getName());
                break;
            case 2002:
                result = gradeBO.restSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "YES"),stuId , GradeTimeType.SYSTEM_GRADE_ONE, loginVO.getName());
                break;
            case 2003:
                result = gradeBO.restSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "YES"),stuId , GradeTimeType.SYSTEM_GRADE_TWO, loginVO.getName());
                break;
            case 2004:
                result = gradeBO.restSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.GRADE_STOP_KEY, IKEY.SYSTEM_GRADE),stuId , GradeTimeType.GRADE_STOP, loginVO.getName());
                break;
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/saveGuideGrade")
    public CommonResult saveGuideGrde(HttpServletRequest request,@RequestParam("stuId") Integer stuId, @RequestParam("grade") Integer grade){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null || grade == null){
            return CommonResult.errorReturn("System param error!");
        }
        CommonResult<Boolean> resultOne = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.GUIDE_GRADE, String.valueOf(grade)), stuId, loginVO.getName());
        CommonResult<Boolean> result = null;
        if(resultOne.isSuccess()){
            result = gradeBO.checkFinalReplyGrade(stuId, loginVO.getName());
        }else{
            result = resultOne;
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/restGuideGrade")
    public CommonResult restGuideGrade(HttpServletRequest request,@RequestParam("stuId") Integer stuId, @RequestParam("grade") Integer grade){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null || grade == null){
            return CommonResult.errorReturn("System param error!");
        }
        CommonResult<Boolean> resultOne = gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.GUIDE_GRADE, String.valueOf(grade)), stuId, loginVO.getName());
        CommonResult<Boolean> result = null;
        if(resultOne.isSuccess()){
            result = gradeBO.checkFinalReplyGrade(stuId, loginVO.getName());
        }else{
            result = resultOne;
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/saveReplyGrade")
    public CommonResult saveReplyGrade(HttpServletRequest request,@RequestParam("stuId") Integer stuId, @RequestParam("reportGrade") Integer report, @RequestParam("replyGrade") Integer reply){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null || reply == null || report == null){
            return CommonResult.errorReturn("System param error!");
        }
        CommonResult<Boolean> resultOne = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, String.valueOf(reply)), stuId, loginVO.getName());
        CommonResult<Boolean> resultTwo = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPORT_GRADE, String.valueOf(report)), stuId, loginVO.getName());
        CommonResult<Boolean> result = gradeBO.checkFinalReplyGrade(stuId, loginVO.getName());
        return result;
    }

    @ResponseBody
    @RequestMapping("/restReplyGrade")
    public CommonResult restReplyGrade(HttpServletRequest request,@RequestParam("stuId") Integer stuId, @RequestParam("reportGrade") Integer report, @RequestParam("replyGrade") Integer reply){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null || reply == null || report == null){
            return CommonResult.errorReturn("System param error!");
        }
        CommonResult<Boolean> resultOne = gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, String.valueOf(reply)), stuId, loginVO.getName());
        CommonResult<Boolean> resultTwo = gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPORT_GRADE, String.valueOf(report)), stuId, loginVO.getName());
        CommonResult<Boolean> result = gradeBO.checkFinalReplyGrade(stuId, loginVO.getName());
        return result;
    }

    @ResponseBody
    @RequestMapping("/restFinalGrade")
    public CommonResult restFinalGrade(HttpServletRequest request, @RequestParam("guideGrade") Integer guide, @RequestParam("reportGrade") Integer report, @RequestParam("replyGrade") Integer reply , @RequestParam("stuId") Integer stuId){
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录!");
        }
        if (guide != null){
            gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.GUIDE_GRADE, String.valueOf(guide)), stuId, loginVO.getName());
        }
        if (report != null){
            gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPORT_GRADE, String.valueOf(report)), stuId, loginVO.getName());
        }
        if (reply != null){
            gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, String.valueOf(reply)), stuId, loginVO.getName());
        }
        CommonResult<Boolean> result = gradeBO.checkFinalReplyGrade(stuId, loginVO.getName());
        return result;
    }


    @ResponseBody
    @RequestMapping("/saveReplyGradeByAdmin")
    public CommonResult saveReplyGradeByAdmin(HttpServletRequest request,@RequestParam("stuId") Integer stuId, @RequestParam("reportGrade") Integer report, @RequestParam("replyGrade") Integer reply, @RequestParam("guideGrade")Integer guide){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null){
            return CommonResult.errorReturn("System param error!");
        }
        if (guide != null){
            CommonResult<Boolean> resultOne = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.GUIDE_GRADE, String.valueOf(guide)), stuId, loginVO.getName());
        }
        if (report != null){
            CommonResult<Boolean> resultTwo = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPORT_GRADE, String.valueOf(report)), stuId, loginVO.getName());
        }
        if (reply != null){
            CommonResult<Boolean> resultTwo = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, String.valueOf(reply)), stuId, loginVO.getName());
        }
        CommonResult<Boolean> result = gradeBO.checkFinalRepolyGradeAdminRole(stuId, loginVO.getName());
        return result;
    }

    @ResponseBody
    @RequestMapping("/restReplyGradeByAdmin")
    public CommonResult restReplyGradeByAdmin(HttpServletRequest request,@RequestParam("stuId") Integer stuId, @RequestParam("reportGrade") Integer report, @RequestParam("replyGrade") Integer reply, @RequestParam("guideGrade")Integer guide){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null){
            return CommonResult.errorReturn("System param error!");
        }
        if (guide != null){
            gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.GUIDE_GRADE, String.valueOf(guide)), stuId, loginVO.getName());
        }
        if (report != null){
            gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPORT_GRADE, String.valueOf(report)), stuId, loginVO.getName());
        }
        if (reply != null){
            gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, String.valueOf(reply)), stuId, loginVO.getName());
        }
        CommonResult<Boolean> result = gradeBO.checkFinalRepolyGradeAdminRole(stuId, loginVO.getName());
        return result;
    }


    @ResponseBody
    @RequestMapping("/clearGuide")
    public CommonResult clearGuide(HttpServletRequest request, @RequestParam("stuId") Integer stuId){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null){
            return CommonResult.errorReturn("System param error!");
        }
        CommonResult<Boolean> result = gradeBO.clearReplyGrade(stuId, IKEY.GUIDE_GRADE, loginVO.getName());
        return result;
    }

    @ResponseBody
    @RequestMapping("/clearReply")
    public CommonResult clearReply(HttpServletRequest request, @RequestParam("stuId") Integer stuId){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登录");
        }
        if (stuId == null){
            return CommonResult.errorReturn("System param error!");
        }
        CommonResult<Boolean> result = gradeBO.clearReplyGrade(stuId, IKEY.REPLY_GRADE, loginVO.getName());
        return result;
    }

}

