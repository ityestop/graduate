/**
 * Created by chuchuang on 16/10/28.
 */
$(function () {

    var $id = $("#id");
    var $noid = $("#noid");
    var $name = $("#name");
    var $sex = $("#sex");
    var $clazz = $("#clazz");
    var $phone = $("#phone");
    var $email = $("#email");
    var $sexSpan;
    var $phoneSpan;
    var $emailSpan;
    var $addressSpan;
    var $address = $("#address");

    var $professionalSpan;
    $.ajax({
        url: "/identity/myself",
        dataType: "json",
        type: "get",
        success: function (result) {
            if (result.success) {
                var data = result.data;
                $id.val(data.identity.id);
                $noid.val(data.identity.noId);
                $name.val(data.identity.name);
                $sex.val(data.identity.sex);
                $.each(data.infoList, function (key, value) {
                    if (value.key == 'address') {
                        $address.val(value.value);
                    }
                });
                $sexSpan = $($sex.next("span")).click(function () {
                    if ($sex.attr('disabled')) {
                        $sex.removeAttr('disabled');
                    } else {
                        $.ajax({
                            url: "/identity/myself",
                            data: {
                                sex: $sex.val()
                            },
                            dataType: "json",
                            type: "post",
                            success: function (res) {
                                if (res.success) {
                                    alert("修改成功");
                                    $sex.attr('disabled', 'disabled');
                                } else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
                $phone.val(data.identity.phone);
                $phoneSpan = $($phone.next("span")).click(function () {
                    if ($phone.attr('disabled')) {
                        $phone.removeAttr('disabled');
                    } else {
                        $.ajax({
                            url: "/identity/myself",
                            data: {
                                phone: $phone.val()
                            },
                            dataType: "json",
                            type: "post",
                            success: function (res) {
                                if (res.success) {
                                    alert("修改成功");
                                    $phone.attr('disabled', 'disabled');
                                } else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
                $email.val(data.identity.email);
                $emailSpan = $($email.next("span")).click(function () {
                    if ($email.attr('disabled')) {
                        $email.removeAttr('disabled');
                    } else {
                        $.ajax({
                            url: "/identity/myself",
                            data: {
                                email: $email.val()
                            },
                            dataType: "json",
                            type: "post",
                            success: function (res) {
                                if (res.success) {
                                    alert("修改成功");
                                    $email.attr('disabled', 'disabled');
                                } else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
                $.each(data.infoList, function (i, node) {
                    if (node.key == 'clazz') {
                        $clazz.val(node.value);
                        $clazz.attr("value", node.value);
                    }
                });
                $addressSpan = $($address.next("span")).click(function () {
                    if ($address.attr('disabled')) {
                        $address.removeAttr('disabled');
                    } else {
                        var $addressValue = $("#address").val();
                        $.ajax({
                            url: "/identity/saveAddress",
                            data: {
                                address: $addressValue,
                                id: data.identity.id
                            },
                            dataType: "json",
                            type: "post",
                            success: function (res) {
                                if (res.success) {
                                    alert("修改成功");
                                    $address.attr('disabled', 'disabled');
                                } else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
            } else {
                alert(result.errorMsg);
            }
        }
    });


});