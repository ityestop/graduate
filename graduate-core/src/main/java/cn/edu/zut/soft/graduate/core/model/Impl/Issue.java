package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.IssueKind;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Issue extends BaseModelImpl {
    private String title;//项目名称
//    private String owner;//归属人

    private Integer identityId;
    private Role identityRole;
    private String identityName;

    private Integer schoolTeaId;
    private String schoolTeaName;

    private IssueSource source;//课题来源：公司项目,校内项目，自拟题目
    private IssueKind kind;//课题性质：社会服务，科研项目
    private String type;//课题类型
    private IssuePhase status;//课题状态
    private String stateSet;//课题流转情况
    private String content;//课题信息：结构化数据。{teacher:{id:主键,name:教师姓名},outTeacher:{name:校外负责人,phone:联系方式},background:项目背景,technology:[技术1,技术2],task:[{nid:标题编号,title:任务标题,conent:任务内容},{nid:标题编号,title:任务标题,conent:任务内容}]}
    private String contentHash;//课题内容Hash值防止修改
    private Integer taskSize;//任务数

    private String technology;//技术领域

    @JSONField(serialize = false)
    public String getContent(String content){
        return content;
    }

    public void addStatus(IssuePhase status){
        if (StringUtils.isBlank(stateSet)){
            stateSet = status.name();
        }else {
            stateSet = stateSet + ","+status.name();
        }
        this.status = status;
    }
}
