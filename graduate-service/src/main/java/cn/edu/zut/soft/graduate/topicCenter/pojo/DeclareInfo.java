package cn.edu.zut.soft.graduate.topicCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import lombok.Data;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/27
 */
@Data
public class DeclareInfo {
    private Issue issue;
    private Declare declare;
    private List<Info> infoList;
}
