package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.model.Impl.CompanyProjectRequest;
import cn.edu.zut.soft.graduate.core.query.CompanyProjectRequestQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.ReplyCompanyProject;

import java.util.List;

/**
 * @author cc
 * @Date 2016/12/1
 * @value 1.0
 */
public interface CompanyProjectRequestBO extends BasicBO<CompanyProjectRequest,CompanyProjectRequestQuery> {

    /**
     * 发出申请,提出校内课题申请
     *
     * 修改小组状态,不可以解散小组,
     *
     * @param loginVO
     * @param content
     * @return
     */
    CompanyProjectRequest sendRequest(LoginVO loginVO,String content);

    boolean isSendCompany(Integer groupId);

    /**
     * 同意申请并分配教师
     * @param loginVO
     * @param replyCompanyProject
     * @return
     */
    CompanyProjectRequest successRequest(LoginVO loginVO, ReplyCompanyProject replyCompanyProject);

    /**
     * 是否具备申请校外课题资格
     * @param loginVO
     * @return null 代表不存在,异常代表数据错误。 对象代表已存在数据
     */
    CompanyProjectRequest isQualified(LoginVO loginVO);

    /**
     * 检索所有的申请记录
     * @param groupId
     * @return
     */
    List<CompanyProjectRequest> groupRequest(Integer groupId);

    /**
     * 同步申请的小组人数
     * @return
     */
    List<CompanyProjectRequest> syncGroupNum(CompanyProjectRequestQuery crq);

    /**
     * 功能描述:根据学生学生ID 小组ID是否具有创建题目的权限
     * @param id
     * @param groupId
     * @return
     */
    boolean isCreateCompanyIssue(Integer id, Integer groupId);

}
