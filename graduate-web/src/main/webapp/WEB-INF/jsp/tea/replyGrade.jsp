<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>系统验收成绩</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table{
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">答辩成绩</li>
        </ol>
        <div class="page-header">
            <h4>答辩成绩
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <input type="hidden" id="groupLeader" value="${sessionScope.GroupgroupLeader}"/>
            <input type="hidden" id="groupSEC" value="${sessionScope.GroupgroupSEC}"/>
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>

<div class="modal fade" id="replyGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">答辩成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="replyStuId">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuName">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClass">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopic">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildTopic">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            报告成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="报告成绩" id="reportGrade">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="答辩成绩" id="replyGrade">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="replyGradeSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restReplyGradeModel" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置答辩成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restReplyStuId">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="restStuName">
                        </label>
                        <label class="col-sm-2 control-label" id="restStuClass">
                        </label>
                        <label class="col-sm-4 control-label" id="restStuTopic">
                        </label>
                        <label class="col-sm-4 control-label" id="restStuChildTopic">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            报告成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="学生指导成绩" id="restReportGrade">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="学生指导成绩" id="restReplyGrade">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restReplyGradeSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">

    var $goupLeader = $("#groupLeader").val();
    var $goupSEC = $("#groupSEC").val();

    var $role = false;
    if (($goupLeader != null && $goupLeader != "" && $goupLeader != undefined) || ($goupSEC != null && $goupSEC != "" && $goupSEC != undefined)) {
        $role = true;
    }

    var columns = [
        {
            field: 'identity.id',
            title: '编号'
        }, {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'infoList',
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var topicName = "";
                var topicId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC') {
                        if (array[i].key == 'topicName') {
                            topicName = array[i].value;
                        }else if (array[i].key == 'topicId'){
                            topicId = array[i].value;
                        }
                    }
                }
                return "<a href='/tea/issueDetail?id="+topicId+"'>"+topicName+"</a>";
            },
            sortStable: true
        },{
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        },{
            title: '指导成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "未操作";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'guideGrade') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        },{
            title: '评阅成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "未操作";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'reportGrade') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        },{
            title: '答辩成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "未操作";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'replyGrade') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        },{
            title: '最终成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "未操作";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'finalReplyGrade') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                var $roleStu = true;
                if ($role) {
                    for (var i = 0; i < array.length; i++){
                        if(array[i].type == 'GRADE' && array[i].key == 'finalReplyGrade') {
                            str = "已结束";
                            $roleStu = false;
                            break;
                        }
                    }
                    if($roleStu){
                        for (var i = 0; i < array.length; i++) {
                            if((array[i].type == 'GRADE' && array[i].key == 'replyGrade') || (array[i].type == 'GRADE' && array[i].key == 'reportGrade')){
                                str = "<a  data-toggle='modal' data-target='#restReplyGradeModel'>重置</a>";
                                break;
                            }else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                                if(array[i].value == 'openGrade'){
                                    str = "<a href='javaScript:void(0);'>不可操作</a>";
                                    break;
                                }else if(array[i].value == 'systemGrade'){
                                    str = "<a href='javaScript:void(0);'>不可操作</a>";
                                    break;
                                }
                            }else {
                                str = "<a  data-toggle='modal' data-target='#replyGradeModel'>答辩成绩</a>"
                            }

                        }
                    }
                } else {
                    str = "无操作";
                }
                return str;
            }
        }
    ];
    customSearch({
        id: "#table",
        url: "/identity/teaGroupStu",
        dataType: "json",
        params:{groupId:${sessionScope.Groupgroup},reverse:true },
        columns: columns,
        pageSize: 40
    });

    $("#replyGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(6).innerHTML);
        $("#stuName").html('学生姓名:' + $stuName);
        $("#stuClass").html('班级:' + $stuClass);
        $("#stuTopic").html('题目:'+ $stuTopic);
        $("#stuChildTopic").html('子标题:'+ $stuChildTopic);
        $("#replyStuId").attr("value", $stuId);
    });

    $("#restReplyGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restReplyStuId").attr("value", $stuId);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(4).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        $("#restStuName").html('学生姓名:' + $stuName);
        $("#restStuClass").html('班级:' + $stuClass);
        $("#restStuTopic").html('题目:'+ $stuTopic);
        $("#restStuChildTopic").html('子标题:'+ $stuChildTopic);
    });


    $("#replyGradeSubmit").click(function () {
        var $stuId = $("#replyStuId").val();
        var $report = $("#reportGrade").val();
        var $reply = $("#replyGrade").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/saveReplyGrade',
            data: {
                stuId: $stuId,
                reportGrade: $report,
                replyGrade:$reply
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                    window.location.reload();
                }
            }
        });
    });

    $("#restReplyGradeSubmit").click(function () {
        var $stuId = $("#restReplyStuId").val();
        var $report = $("#restReportGrade").val();
        var $reply = $("#restReplyGrade").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/restReplyGrade',
            data: {
                stuId: $stuId,
                reportGrade: $report,
                replyGrade:$reply
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                    window.location.reload();
                }
            }
        });
    });

</script>
</body>
</html>
