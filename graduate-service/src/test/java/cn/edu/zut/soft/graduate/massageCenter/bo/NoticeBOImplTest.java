package cn.edu.zut.soft.graduate.massageCenter.bo;

import cn.edu.zut.soft.basic.core.model.dao.Page;
import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.model.Impl.Notice;
import cn.edu.zut.soft.graduate.core.query.NoticeQuery;
import cn.edu.zut.soft.graduate.core.vo.Owner;
import org.junit.Test;
import test.BaseTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * Created by chuchuang on 16/10/29.
 */
public class NoticeBOImplTest extends BaseTest {

    @Resource
    private NoticeBO noticeBO;

    @Test
    public void selectByRole() throws Exception {
        NoticeQuery noticeQuery = new NoticeQuery();
//        page.setOrderByClause();
        noticeQuery.setRole(Role.stu.name());
        System.out.println(noticeBO.findByQuery(noticeQuery));
    }

    @Test
    public void save() throws Exception {
        List<Role> roleList = Arrays.asList(Role.stu);
        Owner owner = new Owner(1,"楚闯",Role.tea.name());
        Notice notice = new Notice();
        notice.setContent("测试公告");
        notice.setTag("");
        notice.setTitle("这只是测试");
        noticeBO.save(notice,owner,roleList,"楚闯");
    }

    @Test
    public void update() throws Exception {

    }

}