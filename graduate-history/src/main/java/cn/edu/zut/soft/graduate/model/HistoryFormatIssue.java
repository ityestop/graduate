package cn.edu.zut.soft.graduate.model;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>文件名称：HistoryTopic.java</p>
 * <p>文件描述：提供历史库转换issue模型</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/29 下午11:38</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HistoryFormatIssue {


    private Integer id;

    private String owner;//拥有者  现不提供 teacher k-v  k有可能为空 操作者提供

    private String title;//消息标题

    private String source; // 课题来源：公司项目，校内课题，自拟题目

    private String kind; //课题性质：社会服务，科研项目

    private String type;//课题类型

    private String status;//课题状态

    private String background;//项目背景

    private List<String> technology = new ArrayList<>();//技术背景

    private String childNames;

    private List<HChildTopic> task = new ArrayList<>();//任务列表

    private String outTeacher;//校外教师

    private Integer task_size;//任务数

    private String cerateTime;

    public HistoryFormatIssue() {
    }

    public HistoryFormatIssue(String owner, String title, String source, String kind, String type, String status, String background, List<String> technology, List<HChildTopic> task, String outTeacher, Integer task_size, String cerateTime) {
        this.owner = owner;
        this.title = title;
        this.source = source;
        this.kind = kind;
        this.type = type;
        this.status = status;
        this.background = background;
        this.technology = technology;
        this.task = task;
        this.outTeacher = outTeacher;
        this.task_size = task_size;
        this.cerateTime = cerateTime;
    }

    public String getChildNames() {
        return childNames;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getSource() {
        return source;
    }

    public String getKind() {
        return kind;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getBackground() {
        return background;
    }

    public List<String> getTechnology() {
        return technology;
    }

    public List<HChildTopic> getTask() {
        return task;
    }

    public String getOutTeacher() {
        return outTeacher;
    }

    public Integer getTask_size() {
        return task_size;
    }

    public String getCerateTime() {
        return cerateTime;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public void setTechnology(List<String> technology) {
        this.technology = technology;
    }

    public void setTask(List<HChildTopic> task) {
        this.task = task;
    }

    public void setOutTeacher(String outTeacher) {
        this.outTeacher = outTeacher;
    }

    public void setTask_size(Integer task_size) {
        this.task_size = task_size;
    }

    public void setCerateTime(String cerateTime) {
        this.cerateTime = cerateTime;
    }

    public void setChildNames(String childNames) {
        this.childNames = childNames;
    }

}
