package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.Role;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Info extends BaseModelImpl{

    private Integer identityId;
    private Role identityRole;
    private String identityName;

    private String owner;//主管
    private String type;//类型
    private String key;//数据的键
    private String value;//数据的值
    private Integer parentId;//该信息的父信息ID，如果一条数据设置了父信息ID，则表示该数据是用户账号（信息）的明细数据。例如一个账号有激活日期，那么激活日期就是该账号的明细数据，此处使用自关联实现

    @JSONField(serialize = false)
    public String getOwner(){
        return owner;
    }

}
