<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: SongCi
  Date: 2017/3/31
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>毕业设计状态</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <script type="application/javascript" src="assets/g/js/config.js"></script>
    <script type="application/javascript">
        function centerGraduate(identityId) {
            var data;
            $.ajax({
                type: 'post',
                dataType: 'json',
                params: {},
                url: '/identity/selectDrawing',
                success: function (result) {
                    data = result.data;
                    var n = data.length;
                    if (n == 0) {
                        $("#centerGraduateRed").append("等待抽取");
                        return;
                    }
                    for (var i = 0; i < n; i++) {
                        for (var key in data[i]) {
                            if (key == "identity") {
                                if (data[i][key].id == identityId) {
                                    showMassage(1);
                                    return;
                                }
                            }
                        }
                    }
                    showMassage(0);
                    return;
                }

            });
        }
        function showMassage(n) {
            var showM = 0;
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: "/stu/stuDetectionMidRan",
                success: function (result) {
                    if (result.success) {
                        if (result.data) {
                            showM = 0;
                        } else {
                            showM = 1;
                        }
                    } else {
                        alert(result.errorMsg);
                    }
                    if (showM == 0) {
                        if (n == 1) {
                            $("#centerGraduateRed").empty();
                            $("#centerGraduateRed").append("等待确认");
                        } else {
                            $("#centerGraduateRed").append("等待抽取");
                        }
                    } else {
                        $("#centerGraduateRed").empty();
                        if (n == 1) {
                            $("#centerGraduateRed").append("返校答辩");
                        } else {
                            $("#centerGraduateRed").append("远程答辩");
                        }
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("网络请求异常！");
                }
            });
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./tea"> 首页 </a>
            </li>
            <li class="active">学生毕业设计状态</li>
            </li>
        </ol>
        <div class="page-header">
            <h3>学生毕业设计状态
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="panel-body">
        <div class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">基本信息</font></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-2">
                        <label class=" h5"> 学生姓名：<span id="name"></span> </label>
                    </div>
                    <div class="col-sm-2">
                        <label class=" h5"> 指导教师：<span id="teacherName"></span> </label>
                    </div>
                    <div class="col-sm-5">
                        <label class=" h5"> 课题名称：<span id="topicName"></span></label>
                    </div>
                    <div class="col-sm-3">
                        <label class=" h5"> 子任务：<span id="taskName"></span> </label>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">答辩教师</font></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-2">
                        <label class=" h5"> 指导教师组：<span id="teacherGroup"></span> </label>
                    </div>
                    <div class="col-sm-2">
                        <label class=" h5"> 答辩教师组：<span id="otherTeacherGroup"></span> </label>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">开题答辩</font></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-4"><%-- 此处数据不齐，无法进行正常逻辑判断 --%>
                        <label class=" h5"> 答辩进度： <span id="startStatus"></span></label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 答辩地点： <span id="startAddress"></span>
                        </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 最终结果：<span id="finallyOpenStatus"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">中期答辩</font></h3>
                </div>
                <div class="panel-body">
                    <%--抽中返校答辩 未抽中远程答辩--%>
                    <div class="col-sm-4">
                        <label class=" h5"> 答辩地点：<font id="centerGraduateRed" color="red">
                            <span id="didAddress"></span></font></label>
                    </div>
                    <div class="col-sm-8">
                        <label class=" h5"> 最终结果：<span id="finallyDidStatus"></span> </label>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">课题验收</font></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-4">
                        <label class=" h5"> 验收进度： <span id="systemAcceptanceStatus"></span> </label>
                    </div>
                    <%--<div class="col-sm-8">--%>
                        <%--<label class=" h5"> 最终结果： <span id="finallySystemAcceptanceStatus"></span> </label>--%>
                    <%--</div>--%>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">周月报情况</font></h3>
                </div>
                <div class="panel-body">

                    <div class="col-sm-4">
                        <label class=" h5"> 报告提交次数：<span id="subCount"></span> </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 报告补交次数：<span id="twoCount"></span> </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 报告缺失： <span id="errCount" style="color: red"></span></label>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">答辩形式</font></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-4">
                        <label class=" h5"> 公开答辩：<span id="openSource"></span> </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 答辩进度： <span id="finallyStatus"></span> </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 最终结果：<span id="overStatus"></span></label>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="#4169e1">答辩成绩</font></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-4">
                        <label class=" h5"> 指导教师成绩：<span id="guideGrade"></span> </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 报告成绩： <span id="reportGrade"></span> </label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 答辩成绩：<span id="replyGrade"></span></label>
                    </div>
                    <div class="col-sm-4">
                        <label class=" h5"> 最终成绩：<span id="finalGrade"></span></label>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script
        src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="assets/g/js/stu/stuInfo.js"></script>

<script type="application/javascript">
    $(function () {
        var scheduleStatus = {
            ONE: "一次",
            TWO: "二次",
            ERROR: "终止"
        };
        var address = {
            zhengzhou: "郑州",
            hangzhou: "杭州",
            shanghai: "上海",
            beijing: "北京"
        };
        var basicStatus = {
            ING: "进行中",
            YES: "通过",
            NO: "终止"
        };
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: "/identity/stateFindByUser?userId=" +${sessionScope.user.id},
            success: function (result) {
                if (result.success) {
                    var data = result.data;
                    var userResult = result.data.user.infoList;
                    $("#name").html(data.user.identity.name);
                    $("#teacherName").html(data.teacher.name);
                    $("#topicName").html("<a href='/stu/issueInfo?id=" + data.issue.issue.id + "'>" + data.issue.issue.title + "</a>");
                    $("#taskName").html(data.task.title);
                    $("#teacherGroup").html(data.teacherGroup.identityName);
                    $("#otherTeacherGroup").html(data.teacherGroup.parallelism.identityName);
                    $("#startStatus").html(scheduleStatus[data.startStatus] + "答辩");
                    $("#startAddress").html(address[data.startAddress]);
                    $("#finallyOpenStatus").html(data.finallyOpenStatus ? basicStatus[data.finallyOpenStatus] : "暂无");
                    $("#didAddress").html(data.didAddress == null ? "未开启抽取" : (data.didAddress ? "返校答辩" : "远程答辩"));
                    $("#finallyDidStatus").html(data.finallyDidStatus ? data.finallyDidStatus : "暂无");
                    $("#systemAcceptanceStatus").html(data.systemAcceptanceStatus);
//                    $("#finallySystemAcceptanceStatus").html(data.finallySystemAcceptanceStatus ? basicStatus[data.finallySystemAcceptanceStatus] : "暂无");
                    $("#subCount").html(data.reportCount.subCount);
                    $("#twoCount").html(data.reportCount.twoCount);
                    $("#errCount").html(data.reportCount.errCount);
                    $("#openSource").html(data.openSource ? data.openSource : "暂无");
                    $("#finallyStatus").html(data.finallyStatus ? scheduleStatus[data.finallyStatus] : "暂无");
                    $("#overStatus").html(data.overStatus ? basicStatus[data.overStatus] : "暂无");
//                    $("#guideGrade").html(data.guideGrade ? data.guideGrade : "暂无");
                    $("#guideGrade").html("暂无");
//                    $("#reportGrade").html(data.reportGrade ? data.reportGrade  : "暂无");
                    $("#reportGrade").html("暂无");
//                    $("#replyGrade").html(data.replyGrade ? data.replyGrade  : "暂无");
                    $("#replyGrade").html("暂无");
//                    $("#finalGrade").html(data.finalGrade ? data.finalGrade  : "暂无");
                    $("#finalGrade").html("暂无");
                } else {
                    alert(result.errorMsg);
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("网络请求异常！");
            }
        });
    });
</script>

</body>
</html>
