package cn.edu.zut.soft.graduate.model;

/**
 * <p>文件名称：HTeacher.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/23 下午3:53</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HTeacher {

    private Integer id;

    private String teacher;

    private String info;

    private String result;

    private String years;

    public HTeacher() {
    }

    public HTeacher(Integer id, String teacher, String info, String result, String years) {
        this.id = id;
        this.teacher = teacher;
        this.info = info;
        this.result = result;
        this.years = years;
    }

    public Integer getId() {
        return id;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getInfo() {
        return info;
    }

    public String getResult() {
        return result;
    }

    public String getYears() {
        return years;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setYears(String years) {
        this.years = years;
    }
}


