/**
 * Created by chuchuang on 16/10/29.
 */
document.write("<script src='/assets/g/js/source.js'></script>");
$(function () {

    var $noticeAll = $("#noticeAll");
    var $show = $(".blog-categories");

    function getBlogPosts() {
        return "<div class='blog-posts'> " +
            "<div class='row'> " +
            "<div class='col-md-12 content'> " +
            "<h4> <a href=''></a> </h4> " +
            "<p> </p> " +
            "</div> " +
            "</div> " +
            "<div class='row'> " +
            "<div class='col-md-12'> " +
            "<div class='post-meta'> " +
            "<span> <i class='fa fa-calendar'></i> </span> " +
            "<span> <i class='fa fa-user'></i> By <label></label> </span> " +
            "<a class='btn btn-xs btn-main-color pull-right' href=''> 编辑</a>" +
            "</div>" +
            "</div>" +
            "</div>" +
            "</div>";
    }

    function searchSelf(data) {
        $.ajax({
            url: '/notice',
            dataType: 'json',
            type: 'get',
            data:data,
            success: function (result) {
                $noticeAll.html("");
                if (result.success) {
                    $.each(result.data, function (i, node) {
                        var $initSearch = $(getBlogPosts());
                        $($initSearch.find(".content a")).attr("href", "/admin/notice?id="+node.id).html(node.title);
                        // $(initSearch.find(".content p")).html(node.content);
                        $($initSearch.find(".post-meta span")).first().append(timeStampYYYYMMDDHHMM(node.createTime));
                        $($initSearch.find(".post-meta span label")).append(JSON.parse(node.owner).name);
                        $($initSearch.find(".post-meta a")).attr("href", "/admin/notice/new?id="+node.id);
                        $noticeAll.append($initSearch);
                    });
                } else {
                    alert(result.errorMsg);
                }
            }
        });

    }

    //默认执行
    searchSelf();

    $.ajax({
        url: '/notice',
        dataType: 'json',
        type: 'get',
        data:{
            orderByClause:'read_num desc'
        },
        success: function (result) {
            $show.html("");
            if (result.success) {
                $.each(result.data, function (i, node) {
                    var $a = $("<a href=''></a>");
                    $($a.attr("href", "/admin/notice?id="+node.id)).html(node.title+'('+node.readNum+')');
                    $show.append($("<li></li>").append($a));
                });
            } else {
                alert(result.errorMsg);
            }
        }
    });

    $(".searchSelf").click(function () {
        searchSelf({title:$("#searchSelf").val()});
    });

});