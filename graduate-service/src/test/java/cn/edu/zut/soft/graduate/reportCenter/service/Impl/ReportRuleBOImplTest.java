package cn.edu.zut.soft.graduate.reportCenter.service.Impl;

import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportRule;
import cn.edu.zut.soft.graduate.reportCenter.query.Impl.SimpleReportDataQuery;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportRuleBO;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

import java.util.Date;

/**
 * Created by cc on 2017/2/11.
 */
public class ReportRuleBOImplTest extends BaseTest {

    @Autowired
    private ReportRuleBO reportRuleBO;

    @Override
    public void text() throws Exception {
        super.text();
        ReportRule reportRule = new ReportRule();
        reportRule.setStartTime(new Date());
        reportRule.setEndTime(new Date());
        reportRule.setNum(1);
        reportRule.setRepeat(2);
        reportRule.setType(1);
        System.out.println("save: ======>" + reportRuleBO.save(reportRule,"chuchuang"));
        ReportRule two = reportRuleBO.get(reportRule.getId());
        System.out.println("get: =====>" + two);
        Thread.sleep(1000);
        System.out.println("update: =====>" + reportRuleBO.update(two,"chuchuang"));
        System.out.println("list: =====>" + reportRuleBO.countByQuery(new SimpleReportDataQuery()));
        System.out.println("delete: =====>" + reportRuleBO.delete(two.getId(),"chuchuang"));
    }

}