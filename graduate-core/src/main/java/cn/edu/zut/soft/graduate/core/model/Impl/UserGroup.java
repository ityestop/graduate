package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>文件名称：Usergroup.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/26 下午11:37</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class UserGroup extends  BaseModelImpl {

    private Integer userId;

    private Integer groupId;

    private String userName;

    private String owner;
}
