package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.basic.core.util.MDUtil;
import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.constant.IssueKind;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.*;
import cn.edu.zut.soft.graduate.topicCenter.dao.IssueDAO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/bean-all.xml")
public class IssueBOImplTest {

    @Resource
    private IssueBO issueBO;

    @Resource
    private IssueDAO<Issue> issueDAO;

    @Test
    public void save() throws Exception {
        IssueContent issueContent = new IssueContent(){
            {
                setBackground("这只是一个测试");
                setSchoolTeacher(new SchoolTeacher(){
                    {
                        setId(10001);
                        setName("测试校内教师");
                    }
                });
                setOutTeacher(new OutTeacher(){
                    {
                        setName("测试校外教师");
                        setPhone("10000000");
                    }
                });
                List<Task> taskList = new ArrayList<>();
                taskList.add(new Task(){
                    {
                        setNid(MDUtil.MD5(System.currentTimeMillis()+Math.random() * 1000 + ""));
                        setTitle("小标题1");
                        setContent("小标题1内容");
                    }
                });
                taskList.add(new Task(){
                    {
                        setNid(MDUtil.MD5(System.currentTimeMillis()+Math.random() * 1000 + ""));
                        setTitle("小标题2");
                        setContent("小标题2内容");
                    }
                });
                setTaskList(taskList);
            }
        };
        Issue issue = new Issue(){
            {
                setStatus(IssuePhase.saveIng);
                setTitle("测试课题");
                setKind(IssueKind.Social);
                setSource(IssueSource.school);
                setType("测试类型");
                List<Technology> technologyList = new ArrayList<>();
                technologyList.add(new Technology(){
                    {
                        setId(MDUtil.MD5(System.currentTimeMillis()+Math.random() * 1000 + ""));
                        setTitle("测试技术领域1");
                    }
                });
                technologyList.add(new Technology(){
                    {
                        setId(MDUtil.MD5(System.currentTimeMillis()+Math.random() * 1000 + ""));
                        setTitle("测试技术领域2");
                    }
                });
                setTechnology(JSONArray.toJSONString(technologyList));
            }
        };
        issue.setTaskSize(issueContent.getTaskList().size());
        IssueVO issueVO = new IssueVO();
        issueVO.setIssue(issue);
        issueVO.setIssueContent(issueContent);
//        issueBO.save(issueVO,"test");
    }

    @Test
    public void update() throws Exception {

    }

    @Test
    public void changeIssuePhase() throws Exception {
        issueBO.changeIssuePhase(new LoginVO(),1,IssuePhase.Handle.Reedit);
    }


    @Test
    public void get() throws Exception {
        Issue issue = issueBO.get(1);
        IssueContent issueContent =JSONObject.parseObject(issue.getContent(),IssueContent.class);
        System.out.println(issueContent);
    }


    @Test
    public void findByQuery() throws Exception {
//        List<Issue> issueList = issueDAO.selectByExample(new Example().cleanPage());
//        Owner owner;
//        for (Issue issue : issueList) {
//            owner = JSONObject.parseObject(issue.getOwner(),Owner.class);
//            issue.setIdentityId(owner.getId());
//            issue.setIdentityName(owner.getName());
//            issue.setIdentityRole(Role.valueOf(owner.getRole()));
//            issueDAO.updateByPrimaryKeySelective(issue);
//        }
    }

    @Test
    public void delete() throws Exception {
        List<Issue> list = issueBO.noExpertIssues(null);
        System.out.println(list.size());
        System.out.println("sssss");
    }

    @Test
    public void test(){
        List<Integer> ls = new ArrayList<>();
        ls.add(1);
        CommonResult<List<Issue>> list = issueBO.queryByIds(ls);
        System.out.println(list.isSuccess());
    }

    @Test
    public void syncSchoolTea(){
        List<Issue> issueList  = issueDAO.selectByExampleWithBLOBs(new Example().cleanPage());
        Issue iss;
        IssueContent issueContent;
        for (Issue issue : issueList){
             issueContent = JSONObject.parseObject(issue.getContent(),IssueContent.class);
            iss = new Issue();
            iss.setId(issue.getId());
            iss.setSchoolTeaId(issueContent.getSchoolTeacher().getId());
            iss.setSchoolTeaName(issueContent.getSchoolTeacher().getName());
            issueDAO.updateByPrimaryKeySelective(iss);
        }
        System.out.println("ok");
    }

    @Test
    public void queryTeaGroupAll(){
        IssueQuery issueQuery = new IssueQuery();
        issueQuery.clear();
        Print.println(issueBO.queryTeaGroupAll(302,issueQuery),Issue.class);
    }
}