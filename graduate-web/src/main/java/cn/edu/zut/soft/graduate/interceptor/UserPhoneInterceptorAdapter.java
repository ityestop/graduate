package cn.edu.zut.soft.graduate.interceptor;

import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.userCenter.bo.StatisticsService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * <p>文件名称：StudentInfoHanler.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/11/20 下午10:03</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Component
public class UserPhoneInterceptorAdapter extends HandlerInterceptorAdapter {

    public UserPhoneInterceptorAdapter() {
        super();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        String url = "";
        if (loginVO == null) {
            url = "/login";
        } else {
            StatisticsService.loginMap.put(loginVO.getId(), loginVO);
            if (loginVO.getPhone() == null){
                url = "/"+loginVO.getRole()+"/myself";
            }else {
                if (Pattern.compile("^/" + loginVO.getRole() + ".*").matcher(request.getRequestURI()).matches()) {
                    return super.preHandle(request, response, handler);
                } else {
                    url = "/home";
                }
            }
        }
        response.sendRedirect(request.getContextPath() +url);
        return false;

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        super.afterConcurrentHandlingStarted(request, response, handler);
    }
}
