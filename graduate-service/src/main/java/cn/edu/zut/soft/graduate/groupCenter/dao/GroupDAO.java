package cn.edu.zut.soft.graduate.groupCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import org.springframework.stereotype.Component;

/**
 * Created by chuchuang on 16/10/21.
 */
@Component
public interface GroupDAO<T extends Group> extends BasicDAO<Integer,T> {

    int restartParallelismId(Integer id,String updateAuthor);

}
