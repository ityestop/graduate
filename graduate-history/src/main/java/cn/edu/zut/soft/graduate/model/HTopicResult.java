package cn.edu.zut.soft.graduate.model;

/**
 * <p>文件名称：HtopicResult.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/30 上午2:10</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HTopicResult {

    private String topTopicType;

    private String topSourece;

    private String topOutTeacher;

    private String topCreateTime;

    private String topType;

    private String topChild;

    private String toptech;

    public String getTopTopicType() {
        return topTopicType;
    }

    public String getTopSourece() {
        return topSourece;
    }

    public String getTopOutTeacher() {
        return topOutTeacher;
    }

    public String getTopCreateTime() {
        return topCreateTime;
    }

    public String getTopType() {
        return topType;
    }

    public String getTopChild() {
        return topChild;
    }

    public String getToptech() {
        return toptech;
    }

    public void setTopTopicType(String topTopicType) {
        this.topTopicType = topTopicType;
    }

    public void setTopSourece(String topSourece) {
        this.topSourece = topSourece;
    }

    public void setTopOutTeacher(String topOutTeacher) {
        this.topOutTeacher = topOutTeacher;
    }

    public void setTopCreateTime(String topCreateTime) {
        this.topCreateTime = topCreateTime;
    }

    public void setTopType(String topType) {
        this.topType = topType;
    }

    public void setTopChild(String topChild) {
        this.topChild = topChild;
    }

    public void setToptech(String toptech) {
        this.toptech = toptech;
    }
}
