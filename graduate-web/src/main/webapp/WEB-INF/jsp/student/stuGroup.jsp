<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>小组信息管理</title>
</head>

<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a>
				</li>
				<li class="active">小组信息</li>
			</ol>
			<div class="page-header">
				<c:if test="${headMan eq null or headMan eq '' }">
					<h4>
						<strong>未参加任何小组</strong>
				</c:if>
				<c:if test="${headMan ne null }">
					<h4>
						<strong>${headMan}的小组信息</strong>
				</c:if>
				<small><font color="red">当拥有审核中或选报成功状态的课题时,不能进行邀请新成员和解散小组操作！</font>
				</small>
				</h4>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<!-- start: TABLE WITH IMAGES PANEL -->
			<div class="panel-body">
				<c:if test="${student.sgId == null || student.sgId eq '' }">
					<p class="text-center">
						未加入小组<a href="#" data-toggle="modal" data-target="#createStuGroup">创建小组</a>
					</p>
				</c:if>
				<c:if test="${student.sgId != null || !student.sgId eq ''}">
					<c:if
						test="${stuList != null and stuList.size() le 3 and stuList.size() > 0  and student.stuId eq stuGroup.sgStudentId and sgTlListsize le 0}">
						<a class="btn btn-primary"
							href="./${sessionScope.pathCode}/inviteMembers.do"><i
							class=" clip-plus-circle "></i> 邀请新成员</a>
					</c:if>
					<c:if
						test="${stuList != null and stuList.size() gt 3 and stuList.size() gt 0  and student.stuId eq stuGroup.sgStudentId and sgTlListsize le 0}">
						<a class="btn btn-primary" href="javaScript:void(0);"
							data-toggle="modal" data-target="#groupCount"><i
							class=" clip-plus-circle "></i> 邀请新成员</a>
					</c:if>
					<c:if
						test="${stuList != null and stuList.size() le 3 and stuList.size() gt 0  and student.stuId eq stuGroup.sgStudentId and sgTlListsize gt 0}">
						<a class="btn btn-primary" href="javaScript:void(0);"
							data-toggle="modal" data-target="#delectGroup"><i
							class=" clip-plus-circle "></i> 邀请新成员</a>
					</c:if>
					<c:if
						test="${stuList != null and stuList.size() > 0  and student.stuId eq stuGroup.sgStudentId and sgTlListsize < 1}">
						<a class="btn btn-primary"
							href="./${sessionScope.pathCode}/deleteStuGroup.do"><i
							class="icon-delete"></i> 解散小组</a>
					</c:if>
					<c:if
						test="${stuList != null and stuList.size() > 0  and student.stuId eq stuGroup.sgStudentId and sgTlListsize > 0 }">
						<a class="btn btn-primary" href="javaScript:void(0);"
							data-toggle="modal" data-target="#delectGroup"><i
							class="icon-delete"></i> 解散小组</a>
					</c:if>
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th width="20%"><small>学号</small></th>
								<th><small>姓名</small></th>
								<th><small>性别</small></th>
								<th><small>专业</small></th>
								<th><small>班级</small></th>
								<th><small>任务分工</small></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${stuList}" var="stu">
								<tr>
									<td><small>${stu.stuStudentId}</small></td>
									<td><small>${stu.stuName}</small></td>
									<td><small>${stu.stuSex ? "女" : "男"}</small></td>
									<td><small>${stu.profession.proName}</small></td>
									<td><small>${stu.stuClass}</small></td>
									<td><small>${stu.childTopic.name}</small></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<c:if test="${sgTlListlength gt 0}">
						<div class="page-header">
							<h4>
								<strong>选报详情</strong>
							</h4>
						</div>
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th><small>课题名称</small></th>
									<th><small>课题类型</small></th>
									<th><small>教师</small></th>
									<th><small>申请时间</small></th>
									<%--<th><small>小组</small></th>--%>
									<th><small>状态</small></th>
									<th><small>操作</small></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${sgTlList}" var="topLink">
									<tr>
										<td><small><a
												href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topLink.chooTopicId}">${topLink.choTop.topTitle}</a>
										</small></td>
										<c:choose>
											<c:when test="${topLink.choTop.topSource eq 3 }">
												<td><small> 自拟题目</small></td>
											</c:when>
											<c:when test="${topLink.choTop.topSource ne 3 }">
												<td><small>校内题目</small></td>
											</c:when>
										</c:choose>
										<td><small>${topLink.choTop.teacher.teaName}</small></td>
										<td><small><fm:formatDate
													value="${topLink.chooCreateTime}" type="both"
													dateStyle="short" timeStyle="medium"></fm:formatDate>
										</small></td>
										<c:choose>
											<c:when
												test="${topLink.chooStatus eq 0 and topLink.choTop.topStatus eq 1 }">
												<td><small class="label label-warning"
													style="font-size: 75% !important;">审核中 </small></td>
												<td>无操作</td>
											</c:when>

											<c:when
												test="${topLink.chooStatus eq 1 and topLink.choTop.topStatus eq 0}">
												<td><small class="label label-success"
													style="font-size: 75% !important;">选报成功 </small></td>
											</c:when>
											<c:when
												test="${topLink.chooStatus eq 1 and topLink.choTop.topStatus eq 1}">
												<td><small class="label label-warning"
													style="font-size: 75% !important;">审核中 </small></td>
											</c:when>
											<c:when test="${topLink.chooStatus eq 2 }">
												<c:choose>
													<c:when
														test="${topLink.choTop.topSource eq 3 and topLink.choTop.topStatus eq 4 }">
														<td><small class="label label-inverse "
															style="font-size: 75% !important;">未提交</small></td>
														<td><a
															href="./${sessionScope.pathCode}/submitStuTop.do?chooId=${topLink.chooId}&topId=${topLink.chooTopicId}">提交</a>
															<a
															href="./${sessionScope.pathCode}/topicUpdate.do?topId=${topLink.chooTopicId}">
																编辑</a></td>
													</c:when>
													<c:when
														test="${topLink.choTop.topSource eq 3 and topLink.choTop.topStatus eq 1 }">
														<td><small class="label label-inverse "
															style="font-size: 75% !important;">退回编辑</small></td>
														<td><a
															href="./${sessionScope.pathCode}/submitStuTop.do?chooId=${topLink.chooId}&topId=${topLink.chooTopicId}">提交</a>
															<a
															href="./${sessionScope.pathCode}/topicUpdate.do?topId=${topLink.chooTopicId}">
																编辑</a></td>
													</c:when>
													<c:when
														test="${topLink.choTop.topSource eq 3 and topLink.choTop.topStatus eq 3 }">
														<td><small class="label label-inverse "
															style="font-size: 75% !important;">未通过</small></td>
														<td><a
															href="./${sessionScope.pathCode}/submitStuTop.do?chooId=${topLink.chooId}&topId=${topLink.chooTopicId}">提交</a>
															<a
															href="./${sessionScope.pathCode}/topicUpdate.do?topId=${topLink.chooTopicId}">
																编辑</a></td>
													</c:when>
													<c:otherwise>
														<td><small class="label label-inverse "
															style="font-size: 75% !important;">失效</small>
														</td>
														<td></td>
													</c:otherwise>
												</c:choose>
											</c:when>
										</c:choose>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>
				</c:if>
			</div>
			<!-- end: TABLE WITH IMAGES PANEL -->
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="createStuGroup" tabindex="-1" role="dialog"
		aria-labelledby="CSGModalLabe">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="CSGModalLabe">创建小组</h4>
				</div>
				<div class="modal-body">是否创建小组?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<a class="btn btn-primary" href="./student/createStuGroup.do">确认</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="delectGroup" tabindex="-1" role="dialog"
		aria-labelledby="CSGModalLab">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="CSGModalLab">权限问题</h4>
				</div>
				<div class="modal-body">
					您已经存在申报记录，不能在邀请新的小组成员或解散小组，若有问题可以线下联系指导教师！</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="groupCount" tabindex="-1" role="dialog"
		aria-labelledby="CSGModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="CSGModalLabel">人数问题</h4>
				</div>
				<div class="modal-body">您的小组超越人数限制！不能邀请,若需要重新建组请先解散小组，重新邀请！</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
