<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>软件学院毕业设计管理平台</title>
<style type="text/css">
body {
	font-size: 14;
}
</style>

</head>

<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">首页</li>
			</ol>
			<div class="page-header">
				<h2>
					软件学院毕业设计管理平台 <small>毕业设计开发小组 <small>若有问题可以@开发人员<font color="red">@楚闯 tel:18638637700    @杨晓天 tel:18638512501</font></small></small>
				</h2>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER -->
	<div class="row">
		<div class="col-md-12">
			<!-- start: GENERAL PANEL -->
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<h4>
							<strong>软件学院 2016届课题申报学生端操作注意事项</strong>
						</h4>
						<p>1、首次登录，请完善个人信息，注意联系方式是必填，请认真填写，不要影响到毕业设计题目选报。</p>
						<p>
							<img style="height: 200px ;width: 900px;" src="./img/3601.png"></img>
						</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/001.png"></img>
						</p>
						<p>2、在'毕业设计选报'中，进行毕业设计题目查询</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/002.png"></img>
						</p>
						<p>3、找到合适的题目之后，查看教师的联系方式</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/003.png"></img>
						</p>
						<p>4、给老师打电话，确认选报，教师同意再选报，以免耽误时间，因为申报一个题目后，如果老师不确认或者拒绝，将不能进行下一次选报。
							选报流程为：</p>
						<p>1）创建毕设小组</p>
						<p>
							<img style="height: 200px ;width: 900px;" src="./img/004.png"></img>
						</p>
						<p>
							<img style="height: 150px ;width: 400px;" src="./img/005.png"></img>
						</p>
						<p>点击创建小组后，自己就会成为组长，要负担起'邀请小组成员''题目申报'的工作，如果非本人担任组长，则请等待别人的邀请。</p>
						<p>
							<img style="height: 200px ;width: 900px;" src="./img/006.png"></img>
						</p>
						<p>此时创建者即组长。</p>
						
						<p>2）组长根据要选报的题目的人数要求，进入自己的“小组信息””，点击“邀请新成员”按钮，邀请同学进入小组，邀请同学需要争得同学的同意，及时通知对方进入系统确认邀请。</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/007.png"></img>
						</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/008.png"></img>
						</p>
						<p>201207082114刘举杰与201207092417柳恒博需要登录到毕业设计管理系统中，就可以看到组长的邀请：</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/009.png"></img>
						</p>
						<p>如果同意邀请，则点击“同意”链接，否则点击“不同意”链接。</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/010.png"></img>
						</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/011.png"></img>
						</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/012.png"></img>
						</p>
						<p>3）、课题选报
							此项任务必须由组长完成，首先进入“毕业设计选报”，进行题目查询，可以通过条件筛选“选报中”的题目，或者某个具体的指导教师的题目：</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/013.png"></img>
						</p>
						<p>点击“查看详情”或者直接点击题目，则可以进入到题目详情页面，通过本页可以查看题目的详细信息，以及进行申报操作：</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/014.png"></img>
						</p>
						<p>如果是柳恒博（非组长）进行题目选报申请：</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/015.png"></img>
						</p>
						<p>虽然当前已经邀请了3人，但是小组成员人数只计算同意邀请的人员个数，顾当前小组的成员只有2人，所以在选报的时候只能选择，要求完成人数为2人的题目，而如果选报3人题目，则会给出如下提示：</p>
						<p>
							<img style="height: 300px ;width: 900px;" src="./img/016.png"></img>
						</p>
						<p>当组长正确的提交了毕设题目的申请后，将会看到如下提示界面，此时，请及时联系指导教师，进行选报确认。如果选报不成功，请联系指导教师及时退回你的申请，一个小组在同一时刻只能申请一个题目，为了不耽误毕设题目选报，请一定要及时同教师进行沟通确认。</p>
						<p>
							<img style="height: 400px ;width: 900px;" src="./img/017.png"></img>
						</p>
					</div>
				</div>
			</div>
			<!-- end: GENERAL PANEL -->
		</div>
	</div>

</body>
</html>
