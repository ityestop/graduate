package cn.edu.zut.soft.graduate.config.bo.Impl;

import cn.edu.zut.soft.graduate.config.bo.ConfigBO;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Config;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

/**
 * Created by cc on 2016/11/24.
 */
public class ConfigBOImplTest extends BaseTest {

    @Autowired
    private ConfigBO configBO;

    @Test
    public void findByKey() throws Exception {
        configBO.findByKey(IKEY.ISSUESTART);
    }

    @Test
    public void updateByKey() throws Exception {
        Config config = new Config();
        config.setConfigKey(IKEY.ISSUESTART);
        config.setConfigValue(Boolean.TRUE.toString());
//        configBO.updateByKey(config,"test");
    }

}