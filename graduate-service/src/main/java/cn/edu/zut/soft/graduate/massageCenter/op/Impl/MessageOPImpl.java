package cn.edu.zut.soft.graduate.massageCenter.op.Impl;

import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.massageCenter.bo.MessageBO;
import cn.edu.zut.soft.graduate.massageCenter.generator.Builder;
import cn.edu.zut.soft.graduate.massageCenter.op.MessageOP;
import cn.edu.zut.soft.graduate.massageCenter.pojo.SendMessage;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author chuchuang
 * @date 16/11/17
 */
@Component
public class MessageOPImpl implements MessageOP {

    protected org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private MessageBO messageBO;

    @Override
    @Transactional
    public void sendMessage(SendMessage sendMessage, Builder builder) {
        for (Identity identity : sendMessage.getIdentityList()) {
            try {
                messageBO.save(builder.assemblyMessage(sendMessage.getIdentity(), identity), sendMessage.getIdentity().getName());
            } catch (Exception e) {
                logger.error(e.getMessage(),e);
                sendMessage.getErrorList().put(identity.getId(), e.getMessage());
            }
        }
    }
}
