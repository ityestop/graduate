package cn.edu.zut.soft.graduate.reportCenter.pojo;

import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cc
 * @Date 2017/2/21
 * @value 1.0
 */
@Data
public class ReportSearchVO {

    public class ReportSearchDataVO{

        public ReportSearchDataVO(ReportRule reportRule, List<ReportDataEntity> reportDataEntityList) {
            this.reportRule = reportRule;
            this.reportDataEntityList = reportDataEntityList;
        }

        private ReportRule reportRule;

        private List<ReportDataEntity> reportDataEntityList;

        public ReportRule getReportRule() {
            return reportRule;
        }

        public void setReportRule(ReportRule reportRule) {
            this.reportRule = reportRule;
        }

        public List<ReportDataEntity> getReportDataEntityList() {
            return reportDataEntityList;
        }

        public void setReportDataEntityList(List<ReportDataEntity> reportDataEntityList) {
            this.reportDataEntityList = reportDataEntityList;
        }
    }

    private Map<ReportRule,List<ReportDataEntity>> ruleListMap = new HashMap<>();

    public class ReportDataEntity {

        private ReportData reportData;

        private UserDescVO userDescVO;

        public ReportDataEntity(ReportData reportData, UserDescVO userDescVO) {
            this.reportData = reportData;
            this.userDescVO = userDescVO;
        }

        public ReportData getReportData() {
            return reportData;
        }

        public void setReportData(ReportData reportData) {
            this.reportData = reportData;
        }

        public UserDescVO getUserDescVO() {
            return userDescVO;
        }

        public void setUserDescVO(UserDescVO userDescVO) {
            this.userDescVO = userDescVO;
        }
    }

    public void addReportDataEntity(ReportRule reportRule,ReportData reportData,UserDescVO userDescVO){
        List<ReportDataEntity> reportDataEntityList = ruleListMap.get(reportRule);
        if (reportDataEntityList == null){
            reportDataEntityList = new ArrayList<>();
        }
        reportDataEntityList.add(new ReportDataEntity(reportData, userDescVO));
    }

    public List<ReportSearchDataVO> toReportSearchDataVO(){
        List<ReportSearchDataVO> reportSearchDataVOList = new ArrayList<>(this.ruleListMap.size());
        for (Map.Entry<ReportRule,List<ReportDataEntity>> me : this.ruleListMap.entrySet()){
            reportSearchDataVOList.add(new ReportSearchDataVO(me.getKey(),me.getValue()));
        }
        return reportSearchDataVOList;
    }
}
