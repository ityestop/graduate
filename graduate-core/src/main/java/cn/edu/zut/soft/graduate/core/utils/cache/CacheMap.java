package cn.edu.zut.soft.graduate.core.utils.cache;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chuchuang
 * @date 16/11/28
 */
public class CacheMap<K, V> extends AbstractMap<K, V> {

    private static final long DEFAULT_TIMEOUT = 60000;//1分钟
//    private static CacheMap defaultInstance;


    public static <K, V> CacheMap<K,V> getDefault() {
        return new CacheMap<>(DEFAULT_TIMEOUT);
    }

    public static <K,V> CacheMap<K,V> getInstance(long time){
//        if (defaultInstance == null) {
//            defaultInstance = new CacheMap<>(time);
//        }
        return new CacheMap<>(time);
    }


    class CacheEntry implements Entry<K, V> {
        long time;
        V value;
        K key;

        CacheEntry(K key, V value) {
            super();
            this.value = value;
            this.key = key;
            this.time = System.currentTimeMillis();
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }
    }

    private final Map<K, CacheEntry> map = new ConcurrentHashMap<>();

    private CacheMap(long timeout) {
        Thread thread = new Thread(new CacheRunnable(map, timeout));
        thread.setName(getClass().getSimpleName() + " Thread");
        thread.start();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet = new HashSet<Entry<K, V>>();
        Set<Entry<K, CacheEntry>> wrapEntrySet = map.entrySet();
        for (Entry<K, CacheEntry> entry : wrapEntrySet) {
            entrySet.add(entry.getValue());
        }
        return entrySet;
    }

    @Override
    public V get(Object key) {
        CacheEntry entry = map.get(key);
        return entry == null ? null : entry.value;
    }

    @Override
    public V put(K key, V value) {
        CacheEntry entry = new CacheEntry(key, value);
        synchronized (map) {
            map.put(key, entry);
        }
        return value;
    }

}
