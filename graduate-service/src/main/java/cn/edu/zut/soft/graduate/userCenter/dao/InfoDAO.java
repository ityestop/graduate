package cn.edu.zut.soft.graduate.userCenter.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import lombok.Data;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by chuchuang on 16/10/21.
 */
@Repository
public interface InfoDAO<T extends Info> extends BasicDAO<Integer,T> {
    List<T> selectTwoOpenGreade();
}
