package cn.edu.zut.soft.graduate.massageCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.model.Impl.Notice;
import cn.edu.zut.soft.graduate.core.query.NoticeQuery;
import cn.edu.zut.soft.graduate.core.vo.Owner;

import java.util.List;

/**
 * Created by chuchuang on 16/10/29.
 */
public interface NoticeBO extends BasicBO<Notice,NoticeQuery> {


    CommonResult<Notice> save(Notice notice, Owner owner,List<Role> roleList,String operator);

    CommonResult<Notice> update(Notice notice, Owner owner,List<Role> roleList,String operator);

}
