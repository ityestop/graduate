package cn.edu.zut.soft.graduate.core.constant;

/**
 * @author chuchuang
 * @date 16/12/15
 */
public enum Address {
    zhengzhou("郑州"),shanghai("上海"),beijing("北京"),hangzhou("杭州");

    private String name;

    Address(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
