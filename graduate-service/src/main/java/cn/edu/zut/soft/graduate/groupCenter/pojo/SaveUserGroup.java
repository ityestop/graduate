package cn.edu.zut.soft.graduate.groupCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import lombok.Data;

/**
 * Created by chuchuang on 16/10/24.
 */
@Data
public class SaveUserGroup {
    private Integer id;
    private Identity identity;
    private Group group;
    private LoginVO user;
}
