package cn.edu.zut.soft.graduate.config.bo.Impl;

import cn.edu.zut.soft.graduate.config.bo.ConfigBO;
import cn.edu.zut.soft.graduate.config.dao.ConfigDAO;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Config;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cc
 * @Date 2016/11/24
 * @value 1.0
 */
@Service
public class ConfigBOImpl implements ConfigBO {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ConfigDAO configDAO;

    @Autowired
    private IssueBO issueBO;

    @Override
    public String findByKey(String key) {
        return configDAO.findByKey(key);
    }

    @Override
    public Config getConfigBykey(String key) {
        return configDAO.getConfigBykey(key);
    }


    @Transactional
    @Override
    public Config updateByKey(LoginVO loginVO,Config config) {
        Assert.notNull(config);
        Assert.notNull(config.getConfigKey());
        config.setUpdateAuthor(loginVO.getName());
        if (StringUtils.isBlank(findByKey(config.getConfigKey()))){
            configDAO.save(config);
        }else {
            configDAO.update(config);
        }

        if (IKEY.ISSUESTART.equals(config.getConfigKey()) && Boolean.parseBoolean(config.getConfigValue())){
            IssueQuery issueQuery = new IssueQuery();
            issueQuery.setLimit(Integer.MAX_VALUE);
            issueQuery.setIdentityRole(Role.tea);
            issueQuery.setIssuePhase(IssuePhase.auditSuccessIng);
            issueQuery.setIssueSource(IssueSource.school);
            List<Issue> issueList = issueBO.findByQuery(issueQuery).getData();
            if (CollectionUtils.isEmpty(issueList)){
                return config;
            }
            List<Integer> ids = new ArrayList<>(issueList.size());
            CollectionUtils.collect(issueList, new Transformer() {
                @Override
                public Object transform(Object o) {
                    return ((Issue)o).getId();
                }
            },ids);
            int error = issueBO.batchChangeIssuePhase(loginVO, IssuePhase.Handle.OpenChoose,ids);
            if (error > 0){
                logger.error("批量数据操作错误,请管理员检查数据,loginVO ={},ids = {}",loginVO,ids);
            }
        }
        return config;

    }

    @Override
    public List<Config> findAll() {
        return configDAO.findAll();
    }

}
