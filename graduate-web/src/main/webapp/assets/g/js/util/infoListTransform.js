/**
 * Created by cc on 2017/1/6.
 */
function infoResult(array,type,key) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].type == type && array[i].key == key) {
            return array[i].value;
        }
    }
    return null;
}