<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<html>
<head>

<title>验收成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<script src="IAssets/js/selectCheckbox2.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">验收成绩</li>
			</ol>
			<div class="page-header">
				<h3>
					验收成绩<small><font color="red">仅组长和秘书有权限给学生成绩</font> </small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="panel-body">
			
						<form action="./${sessionScope.pathCode}/updateCheckGrade.do"
							method="post">
							<input type="hidden" name="tgId"
								value="${sessionScope.doucking}">
							<table class="table  table-hover"
								style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
								<thead>
									<tr>
										<th width="3%"><input type="checkbox" id="selectAll" />
										</th>
										<th width="5%"><small>编号</small>
										</th>
										<th width="13%"><small>学号</small>
										</th>
										<th><small>姓名</small>
										</th>
										<th width="13%"><small>答辩题目</small>
										</th>
										<th width="8%"><small>校外导师</small>
										</th>
										<th><small>指导教师</small>
										</th>
										<th><small>验收成绩</small>
										</th>
										<th width="15%"><small>操作</small>
										</th>
									</tr>
								</thead>
								<c:forEach items="${stulist}" var="t" varStatus="i">
									<tr>
										<c:choose>
											<c:when  test="${sessionScope.user.teaIdentity > 0 && (t.checkGrade eq 1  || t.checkGrade eq 3 ||  t.checkGrade eq  -1)}">
											<td><label></label></td>
											</c:when>
											<c:when test="${sessionScope.user.teaIdentity > 0 && (t.checkGrade eq 2 || t.checkGrade eq 0 )}">
												<td><label> <input type="checkbox"
														name="stuIds" value="${t.stuId}" /> </label>
												</td>
											</c:when>
											<c:when test="${sessionScope.user.teaIdentity == 0  }">
												<td><label></label></td>
											</c:when>
										</c:choose>
								
										<td><small>${i.index + 1 }</small></td>
										<td><small>${t.stuStudentId}</small></td>
										<td><small>${t.stuName }</small></td>
										<c:choose>
											<c:when test="${t.topTitle ne null }">
												<c:if
													test="${t.childTitle eq null or t.childTitle eq '' or t.childTitle eq 'null' }">
													<td
														style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
														title="${t.topTitle}"><small><a
															href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
																${t.topTitle}</a> </small>
													</td>
												</c:if>
												<c:if
													test="${t.childTitle ne null and t.childTitle ne '' and t.childTitle ne 'null' }">
													<td
														style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
														title="${t.topTitle}-${t.childTitle}"><small><a
															href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
																${t.topTitle}</a> </small></td>
												</c:if>

											</c:when>
											<c:when test="${t.topTitle eq null }">
												<td
													style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><small><a
														href="javaScript:void(0);"> 无</a> </small>
												</td>
											</c:when>
										</c:choose>
										<td><small>${t.topOutTeacher eq null ? "无"
												:t.topOutTeacher }</small>
										</td>
										<td><small>${t.teaName}</small>
										</td>
										<c:choose>
										<c:when test="${t.checkGrade == 0}" >
											<td><small>未操作</small></td>
										</c:when>
										<c:when test="${t.checkGrade == 1}">
										<td><small>已通过</small></td></c:when>
										<c:when test="${t.checkGrade == 2}">
										<td><small>二次验收</small></td></c:when>
										<c:when test="${t.checkGrade == 3}">
										<td><small>进入二辩</small></td></c:when>
										<c:when test="${t.checkGrade == -1}">
										<td><small>终止答辩</small></td></c:when>
										</c:choose>
										<td>
										<c:choose>
											<c:when test="${sessionScope.user.teaIdentity > 0}">
												<c:if test="${t.checkGrade == 0 }">
												<small><a href="./${sessionScope.pathCode}/updateCheckGradeOne.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}&status= 1">通过</a>
												</small>
											</c:if>
											<c:if test="${t.checkGrade == 2 }">
												<small><a href="./${sessionScope.pathCode}/updateCheckGradeOne.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}&status= 3">进入二辩</a>
												</small>
											</c:if>
											<c:if test="${t.checkGrade == 1 || t.checkGrade == -1 || t.checkGrade eq 3}"></c:if>
											<c:if test="${t.checkGrade == 0}">
														<small><a href="./${sessionScope.pathCode}/updateCheckGradeOne.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}&status= 2">二次验收</a>
														</small>
													</c:if>
											<c:if test="${t.checkGrade != 1  and t.checkGrade != -1 and t.checkGrade != 3}">
												<small><a href="./${sessionScope.pathCode}/updateCheckGradeOne.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}&status= -1">终止答辩</a>
														</small>
											</c:if>
												</c:when>
											<c:when test="${sessionScope.user.teaIdentity == 0}">
												<small>无权限操作</small>
											</c:when>
										</c:choose>
										</td>
									</tr>
								</c:forEach>
							</table>
							<div class="col-sm-2" style="float: right;">
								<c:choose>
									<c:when test="${sessionScope.user.teaIdentity > 0}">
										<button type="submit" class="btn btn-blue btn-block" >
											通过</button>
									</c:when>
									<c:when test="${sessionScope.user.teaIdentity == 1}">
									</c:when>
								</c:choose>
							</div>
						</form>
			
		</div>
	</div>

	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
