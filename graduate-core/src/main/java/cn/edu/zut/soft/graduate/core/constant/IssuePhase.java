package cn.edu.zut.soft.graduate.core.constant;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chuchuang on 16/10/22.
 */
public enum IssuePhase implements NameString {
    //保存
    saveIng("保存中"),
    //审核中
    auditIng("审核中"),
    //审核不通过
    auditErrorIng("审核不通过"),
    //审核通过
    auditSuccessIng("审核通过"),
    //选报中
    chooseIng("选报中"),
    //选报成功
    finish("选报成功"),
    //失效
    giveUp("失效"),
    //无人选报
    noBody("无人选报"),
    //历史
    history("历史数据");


    private String name;

    IssuePhase(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public enum Handle {
        UpAudit(saveIng, auditIng), SaveIngGiveUp(saveIng, giveUp), AuditError(auditIng, auditErrorIng), AuditSuccess(auditIng, auditSuccessIng),
        Reedit(auditErrorIng, saveIng), AuditGiveUp(auditErrorIng, giveUp), OpenChoose(auditSuccessIng, chooseIng),
        ChooseSuccess(chooseIng, finish), ChooseError(auditIng, noBody), Dissolve(finish, giveUp), FinishPutIn(finish, history),
        NoBodyPutIn(noBody, history), Copy(history, saveIng);

        private final IssuePhase src;
        private final IssuePhase dst;

        Handle(IssuePhase src, IssuePhase dst) {
            this.src = src;
            this.dst = dst;
        }

        private static final Map<IssuePhase, Map<Handle, IssuePhase>> m = new EnumMap<>(IssuePhase.class);

        static {
            for (IssuePhase issuePhase : IssuePhase.values()) {
                m.put(issuePhase, new EnumMap<Handle, IssuePhase>(Handle.class));
            }
            for (Handle handle : Handle.values()) {
                m.get(handle.src).put(handle, handle.dst);
            }
        }

        public IssuePhase getDst(){
            return this.dst;
        }

        public IssuePhase getSrc(){return this.src;}

        //获取操作之后的流转状态
        public static IssuePhase from(IssuePhase src, Handle handle) {
            return m.get(src).get(handle);
        }

    }

    private static Map<String, String> issuePhaseMap = new HashMap<String, String>();

    public static Map<String, String> getMap() {
        for (IssuePhase source : IssuePhase.values()) {
            issuePhaseMap.put(source.name(), source.getName());
        }
        return issuePhaseMap;
    }

}
