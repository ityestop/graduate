package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by chuchuang on 16/10/29.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class NoticeQuery extends Query {
    private String title;

    private String role;
}
