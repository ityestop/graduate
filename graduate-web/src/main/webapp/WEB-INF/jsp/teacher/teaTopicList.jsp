<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<title></title>
<script type="text/javascript" src="IAssets/js/topoclist.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">选题维护</li>
			</ol>
			<div class="page-header">
				<h3>选题维护 <small>可以对未提交的课题进行修改和提交操作</small> </h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
				<div class="panel-body">
					<table id="sample-table-1" class="table table-hover"style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all" >
						<thead>
							<tr>
								<th width="20%"><small>课题名称</small></th>
								<th><small>课题类型</small></th>
								<th><small>课题来源</small></th>
								<th><small>人数</small></th>
								<th width="16%"><small>申报时间</small></th>
								<th><small>课题状态</small></th>
								<th><small>技术领域</small></th>
								<th width="18%"><small>操作</small></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${teacherAndTopic}" var="topic">
								<tr>
									<td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">
									<a
										href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${topic.topId}" title="${topic.topTitle}"><small>${topic.topTitle}</small></a>
									</td>
									<c:choose>
										<c:when test="${topic.topTopicType eq false}">
											<td><small>社会服务</small></td>
										</c:when>
										<c:when test="${topic.topTopicType eq true}">
											<td><small>科研项目</small></td>
										</c:when>
									</c:choose>
										<c:choose>
										<c:when test="${topic.topSource eq 3}">
											<td><small><font color="red">自拟题目</font></small></td>
										</c:when>
										<c:when test="${topic.topSource ne 3}">
											<td><small>校内题目</small></td>
										</c:when>
									</c:choose>
									<td><small>  ${topic.topCount }</small></td>
									<td><small><fmt:formatDate value="${topic.topCreateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></small></td>

									<c:choose>
										<c:when test="${topic.topStatus eq 0 }">
											<td><small class="label label-danger"  style="font-size: 75% !important;">已报满</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 2 }">
											<td><small class="label label-success" style="font-size: 75% !important;">选报中</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 1 }">
											<td ><small class="label label-warning" style="font-size: 75% !important;">审核中</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 3 }">
											<td><small class="label label-inverse " style="font-size: 75% !important;">未通过</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 4 }">
											<td><small class=" 	label label-default" style="font-size: 75% !important;">未提交</small></td>
										</c:when>
									</c:choose>
									<td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">
									<c:forEach items="${topic.topTechnophobeList}" var="t">
                       				 <small>${t.techName}、</small>
                 				   </c:forEach>
									</td>
									<td >
									<c:choose>
									<c:when test="${topic.status.index ge 3 and topic.topSource eq 3}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
									  	<c:if test="${sessionScope.functionMap['teaEdit'].status eq false}">
										<a href="javaScript:void(0);">
										<i class="fa fa-info-circle"></i><small>不可操作</small></a>
										</c:if>
										<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
										<a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">
										<i class="clip-pencil-3"></i><small>编辑</small></a>
										</c:if>
										</c:when>
										<c:when test="${topic.status.index ge 3}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
									  	<c:if test="${sessionScope.functionMap['teaEdit'].status eq false}">
										<a href="javaScript:void(0);">
										<small></small>
										</a>
										</c:if>
										<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
										<a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">
										<i class=" clip-pencil-3 "></i><small>编辑</small>
										</a>
										</c:if>
										<a href="./${sessionScope.pathCode}/submitTopic.do?id=${topic.topId}&statues=">
										<i class="clip-checkmark-2"></i><small>提交</small>
										</a>
										<a href="./${sessionScope.pathCode}/deleteTopic.do?id=${topic.topId}">
										<i class="fa fa-trash-o"></i><small>删除</small>
										</a>									  									  
										</c:when>
										<c:when test="${topic.status.index eq 2}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
									  	<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
										<a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">
										<i class=" clip-pencil-3 "></i><small>编辑</small>
										</a>
										<a href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${topic.topId}">
										<small>选报确认</small>
										</a>
										</c:if>
										<c:if test="${sessionScope.functionMap['teaEdit'].status eq false}">
										<a href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${topic.topId}">
										<small>选报确认</small>
										</a>
										</c:if>
										</c:when>
										<c:when test="${topic.status.index eq 1 and topic.topSource eq 3}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
									  	<c:if test="${sessionScope.functionMap['teaEdit'].status eq false}">
										<a href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${topic.topId}" ><i class=" 	clip-checkmark "></i><small>   审核   </small></a>
										</c:if>
										<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
										<a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">
										<i class=" clip-pencil-3 "></i><small>编辑</small>
										</a>
										<a href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${topic.topId}" ><i class=" 	clip-checkmark "></i><small>   审核   </small></a>
										
										</c:if>
										</c:when>
										<c:when test="${topic.status.index eq 1 and topic.topSource lt 3}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
									  	<c:if test="${sessionScope.functionMap['teaEdit'].status eq false}">
										<a href="javaScript:void(0);">
										<i class="fa fa-info-circle"></i><small>不可操作</small>
										</a>
										</c:if>
										<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
										<a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">
										<i class=" clip-pencil-3 "></i><small>编辑</small>
										</a>
										</c:if>
										</c:when>
										<c:when test="${topic.status.index eq 0}">
									  	<div class="visible-md visible-lg hidden-sm hidden-xs">
									  	<c:if test="${sessionScope.functionMap['teaEdit'].status eq false}">
										<a href="javaScript:void(0);">
										<i class="fa fa-info-circle"></i><small>不可操作</small>
										</a>
										</c:if>
										<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
										<a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">
										<i class=" clip-pencil-3 "></i><small>编辑</small>
										</a>
										</c:if>
										</c:when>
										</c:choose>
									  </div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

		</div>
	</div>

</body>
</html>
