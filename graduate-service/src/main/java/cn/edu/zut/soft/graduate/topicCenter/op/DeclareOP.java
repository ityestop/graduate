package cn.edu.zut.soft.graduate.topicCenter.op;

import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.ConfirmInfo;

/**
 * @author chuchuang
 * @date 16/11/27
 */
public interface DeclareOP {

    Declare GroupDeclare(LoginVO loginVO, Issue issue);

    void TeaConfirm(ConfirmInfo confirmInfo);
}
