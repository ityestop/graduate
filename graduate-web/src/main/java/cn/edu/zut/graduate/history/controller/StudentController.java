//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.constant.TopStatus;
//import cn.edu.zut.soft.graduate.core.model.*;
//import cn.edu.zut.soft.graduate.core.model.tools.Profession;
//import cn.edu.zut.soft.graduate.core.model.tools.SelectTopic;
//import cn.edu.zut.soft.graduate.core.model.tools.StuGroupTopicLink;
//import cn.edu.zut.soft.graduate.core.utils.DataLS;
//import cn.edu.zut.soft.graduate.core.utils.DataUtil;
//import cn.edu.zut.soft.graduate.core.utils.MDUtil;
//import org.apache.ibatis.session.RowBounds;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//import tag.BaseController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpSession;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by cc on 15/11/22.
// */
//@Controller
//@RequestMapping("/student/")
//public class StudentController extends BaseController {
//
//	@Resource
//	private ServiceFit serviceFit;
//
//	private Role r = Role.STU;
//
//	@RequestMapping("home")
//	public String homePage(Model model) {
//		model.addAttribute("menuSelected1", ConfigDo.INDEX);
//		return r.getName() + "/home";
//	}
//	@RequestMapping("Description")
//	public String  Description(Model model){
//		return r.getName() + "/Descriptionhome";
//	}
//	@RequestMapping("topicList")
//	public ModelAndView studentList(Map<String, Object> model, SelectTopic selectTopic,
//			// @RequestParam(value = "jsly", required = false) String[] tec,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		int totalCount = 0;
//		map.put("type", "studentAllTop");
//		// selectTopic.setTopTech(tec);
//		ResultDo resultDo = serviceFit.getTopicService().selectTopicStatus(map, selectTopic,
//				new RowBounds((l - 1) * s, s));
//		totalCount = ((List) serviceFit.getTopicService().selectTopicStatus(map, selectTopic, new RowBounds())
//				.getResult()).size();
//		if (resultDo.isSuccess()) {
//			model.put("topicList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//
//		ResultDo resultDo1 = serviceFit.getTopicService().findAll(Technophobe.class, new RowBounds());
//		if (resultDo1.isSuccess()) {
//			model.put("techno", resultDo1.getResult());
//		} else {
//			model.put("mag", resultDo1.getMessage());
//		}
//		map.put("status", TopStatus.ING);
//		map.put("checked", false);
//		ResultDo resultDo3 = serviceFit.getEmpService().findEmp(map, new RowBounds(), r);
//		// System.out.println("测试---0"+resultDo3.getResult());
//		if (resultDo3.isSuccess()) {
//			model.put("teacherList", resultDo3.getResult());
//		} else {
//			model.put("mag", resultDo3.getMessage());
//		}
//		initPage(model, l, s, totalCount);
//		model.put("menuSelected1", ConfigDo.STUDENT_XUANBAO);
//		model.put("menuSelected2", ConfigDo.STUDENT_ISSUES);
//
//		return new ModelAndView(r.getName() + "/studentList", model);
//	}
//
//	@RequestMapping("selectOneTopic")
//	public ModelAndView selectOneTopic(Map<String, Object> model, HttpSession session,
//			@RequestParam(value = "topId", required = false) String topId) {
//		ResultDo resultDo, resultDo2, resultDo3;
//		String mag = "";
//		Student student = (Student) session.getAttribute("user");
//		if (student.getSgId() != null) {
//			resultDo2 = serviceFit.getStuGroupService().selectBySgId(student.getSgId());
//			if (resultDo2.isSuccess()) {
//				model.put("sgIdCount", ((List<Student>) resultDo2.getResult()).size());
//				resultDo2 = serviceFit.getTopicService().findSgLBySgId(student.getSgId());
//				List<StuGroupTopicLink> links = (List<StuGroupTopicLink>) resultDo2.getResult();
//				int c = 0;
//				for (StuGroupTopicLink s : links) {
//					if (s.getChooStatus() < 2) {
//						c++;
//					}
//				}
//				model.put("sgCount", c);
//			} else {
//				mag = mag + resultDo2.getMessage();
//			}
//		} else {
//			mag = mag + "未分配小组！";
//		}
//		resultDo3 = serviceFit.getStuGroupService().selectStuGroup(student.getStuId(), student.getSgId());
//		if (resultDo3.isSuccess()) {
//			model.put("headMan", true);
//		} else {
//			model.put("headMan", false);
//		}
//		if (topId != null) {
//			resultDo = serviceFit.getTopicService().loadTopic(topId);
//			if (resultDo.isSuccess()) {
//				model.put("topicInfo", resultDo.getResult());
//				resultDo = serviceFit.getTopicService().findSGTLink(topId, new RowBounds());
//				if (resultDo.isSuccess()) {
//					List<StuGroupTopicLink> list = (List<StuGroupTopicLink>) resultDo.getResult();
//					model.put("SGTLink", list);
//					int i = 0;
//					for (StuGroupTopicLink s : list) {
//						if (s.getChooStatus() == 1) {
//							i++;
//						}
//					}
//					model.put("sta", i);
//				} else {
//					mag = mag + resultDo.getMessage();
//				}
//			} else {
//				mag = mag + resultDo.getMessage();
//			}
//		} else {
//			mag = mag + "没有信息";
//		}
//		model.put("mag", mag);
//		return new ModelAndView(r.getName() + "/selectOneTopic", model);
//	}
//
//	@RequestMapping("toStuGroup")
//	public String toStuGroup(Model model, HttpSession session,
//			@RequestParam(value = "mag", required = false) String mag) {
//		model.addAttribute("mag", mag);
//		Student student = (Student) serviceFit.getEmpService()
//				.load(((Student) session.getAttribute("user")).getStuId(), r).getResult();
//		model.addAttribute("student", student);
//		List<Student> list = (List<Student>) serviceFit.getEmpService().findGroupList(student.getSgId(), Role.STU)
//				.getResult();
//		model.addAttribute("stuList", list);
//		if (list != null && list.size() > 0) {
//			StuGroup stuGroup = (StuGroup) serviceFit.getStuGroupService().load(student.getSgId()).getResult();
//			model.addAttribute("stuGroup", stuGroup);
//			if (stuGroup != null) {
//				ResultDo resultDo = serviceFit.getStuGroupService().selectStuGroup(student.getStuStudentId(),
//						student.getSgId());
//				if (resultDo.isSuccess()) {
//					model.addAttribute("headMan", student.getStuName());
//				} else {
//					student = (Student) serviceFit.getEmpService().load(stuGroup.getSgStudentId(), r.STU).getResult();
//					model.addAttribute("headMan", student.getStuName());
//				}
//			}
//			List<StuGroupTopicLink> stuGroupTopicLinks = (List<StuGroupTopicLink>) serviceFit.getTopicService()
//					.findSgLBySgId(student.getSgId()).getResult();
//			int count = (Integer) serviceFit.getStuGroupService().findStatusCount(student.getSgId()).getResult();
//			model.addAttribute("sgTlList", stuGroupTopicLinks);
//			model.addAttribute("sgTlListsize", count);
//			model.addAttribute("sgTlListlength", stuGroupTopicLinks.size());
//		}
//
//		return r.getName() + "/stuGroup";
//	}
//
//	@RequestMapping("createStuGroup")
//	public String createStuGroup(HttpSession session, RedirectAttributes model) {
//		Student student = (Student) session.getAttribute("user");
//		if (student != null) {
//			ResultDo resultDo = serviceFit.getEmpService().load(student.getStuId(), Role.STU);
//			if (resultDo.isSuccess()) {
//				student = (Student) resultDo.getResult();
//				if (student.getSgId() == null || student.getSgId().equals("")) {
//					resultDo = serviceFit.getStuGroupService().createStuGroup(student, r);
//					if (resultDo.isSuccess()) {
//						session.setAttribute("user", resultDo.getResult());
//					}
//				} else {
//					model.addAttribute("mag", "已经存在小组,不可以创建");
//				}
//			} else {
//				model.addAttribute("mag", "出现错误,请重新登录");
//			}
//		} else {
//			model.addAttribute("mag", "出现错误,请重新登录");
//		}
//		model.addAttribute("menuSelected1", ConfigDo.INDEX);
//		return "redirect:./toStuGroup.do";
//	}
//
//	@RequestMapping("deleteStuGroup")
//	public String deleteStuGroup(HttpSession session, RedirectAttributes model) {
//		Student student = (Student) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getEmpService().load(student.getStuId(), Role.STU);
//		if (resultDo.isSuccess()) {
//			student = (Student) resultDo.getResult();
//			StuGroup stuGroup = (StuGroup) serviceFit.getStuGroupService().load(student.getSgId()).getResult();
//			if (stuGroup != null) {
//				if (stuGroup.getSgStudentId().equals(student.getStuId())) {
//					resultDo = serviceFit.getStuGroupService().deleteSG(stuGroup.getSgId());
//					if (resultDo.isSuccess()) {
//						session.setAttribute("user",
//								serviceFit.getEmpService().load(student.getStuId(), Role.STU).getResult());
//					} else {
//						model.addAttribute("mag", resultDo.getMessage());
//					}
//				} else {
//					model.addAttribute("mag", "非本人小组,无法删除");
//				}
//			} else {
//				model.addAttribute("mag", "小组不存在");
//			}
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//
//		return "redirect:./toStuGroup.do";
//	}
//
//	@RequestMapping("joinStuGroup")
//	public String joinStuGroup(HttpSession session, @RequestParam("sgId") String sgID,
//			@RequestParam("mailId") String mailId, @RequestParam("type") boolean b) {
//		Student student = (Student) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getStuGroupService().joinStuGroup(student, b, sgID, mailId);
//		// 重定向到查看信息页面.
//		return "redirect:./allMailBox.do";
//	}
//
//	@RequestMapping("allMailBox")
//	public String allMailBox(Map<String, Object> model, HttpSession session,
//			@RequestParam(value = "d", required = false, defaultValue = "false") Boolean d, // 是否读取过
//			@RequestParam(value = "g", required = false, defaultValue = "false") Boolean g, // 发送或者接收true接false发
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		Student student = (Student) session.getAttribute("user");
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("d", d);
//		map.put("g", g);
//		ResultDo resultDo = serviceFit.getMailBoxService().findById(student.getStuId(), map,
//				new RowBounds((l - 1) * s, s));
//		int count = ((List) serviceFit.getMailBoxService().findById(student.getStuId(), map, new RowBounds())
//				.getResult()).size();
//		initPage(model, l, s, count);
//		if (resultDo.isSuccess()) {
//			model.put("list", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		// System.out.println(g);
//		if (g) {
//			return r.getName() + "/mailBox";
//		} else {
//			return r.getName() + "/SmailBox";
//		}
//	}
//
//	/**
//	 * 学生基本信息
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("stuInfo")
//	public String stuInfo(Model model, HttpSession session) {
//		Student student = (Student) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getTopicService().findAll(Profession.class, new RowBounds());
//		if (resultDo.isSuccess()) {
//			model.addAttribute("profession", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		model.addAttribute("student", student);
//		model.addAttribute("menuSelected1", ConfigDo.STUDENT_INFO);
//		return r.getName() + "/stuInfo";
//	}
//
//	@RequestMapping("upStuInfo")
//	public String upStuInfo(Model model, HttpSession session, Student student) {
//		student.setStuId(((Student) session.getAttribute("user")).getStuId());
//		ResultDo resultDo = serviceFit.getEmpService().insertAndUpdate(student, r);
//		if (resultDo.isSuccess()) {
//			student = (Student) resultDo.getResult();
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		ResultDo resultDo1 = serviceFit.getTopicService().findAll(Profession.class, new RowBounds());
//		if (resultDo1.isSuccess()) {
//			model.addAttribute("profession", resultDo1.getResult());
//		} else {
//			model.addAttribute("mag", resultDo1.getMessage());
//		}
//		model.addAttribute("student", student);
//		session.setAttribute("user", student);
//		model.addAttribute("menuSelected1", ConfigDo.STUDENT_INFO);
//		return r.getName() + "/stuInfo";
//	}
//
//	/**
//	 * 自拟题目
//	 */
//	@RequestMapping("toMyTitle")
//	public String toMytitle(Model model, @RequestParam(value = "stuId", required = false) String stutId,
//			@RequestParam(value = "sgId", required = false) String sgId) {
//		ResultDo resultDo = serviceFit.getStuGroupService().selectStuGroup(stutId, sgId);
//		if (resultDo.isSuccess()) {
//			model.addAttribute("headMan", true);
//		} else {
//			model.addAttribute("headMan", false);
//		}
//		if (sgId != null) {
//			ResultDo resultDo2 = serviceFit.getStuGroupService().selectBySgId(sgId);
//			if (resultDo2.isSuccess()) {
//				int count = (Integer) serviceFit.getStuGroupService().findStatusCount(sgId).getResult();
//				model.addAttribute("topcount", count);
//				model.addAttribute("Count", ((List<Student>) resultDo2.getResult()).size());
//			} else {
//				model.addAttribute("mag", resultDo2.getMessage());
//			}
//		}
//		ResultDo resultDo3 = serviceFit.getTopicService().findAll(Technophobe.class, new RowBounds());
//		if (resultDo3.isSuccess()) {
//			model.addAttribute("technophobeList", resultDo3.getResult());
//		} else {
//			model.addAttribute("mag", resultDo3.getResult());
//		}
//		model.addAttribute("menuSelected1", ConfigDo.STUDENT_XUANBAO);
//		model.addAttribute("menuSelected2", ConfigDo.STUDENT_MYTITLE);
//		return r.getName() + "/myTitle";
//	}
//
//	@RequestMapping("inviteMembers")
//	public String inviteMembers(Map<String, Object> model, HttpSession session, Student student,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		ResultDo resultDo;
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "findStuNoGroup");
//		// System.out.println(student.getStuName());
//		map.put("id", student.getStuName());
//		student = (Student) session.getAttribute("user");
//		map.put("teaId", student.getStuTeacherId());
//		resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds((l - 1) * s, s), r);
//		int count = ((List) serviceFit.getEmpService().findEmp(map, new RowBounds(), r).getResult()).size();
//		initPage(model, l, s, count);
//		if (resultDo.isSuccess()) {
//			model.put("stuList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		// model.put("student",student);
//		// model.put("menuSelected1", ConfigDo.STUDENT_INFO);
//		return r.getName() + "/inviteMembers";
//	}
//
//	@RequestMapping("submitInviteMembers")
//	public String submitInviteMembers(HttpSession session, @RequestParam("ids") String[] ids) {
//		Student student = (Student) session.getAttribute("user");
//		serviceFit.getMailBoxService().createMail(student, ids);
//		return "redirect:./toStuGroup.do";
//	}
//
//	@RequestMapping("submitNewTopic")
//	public String addNewTopic(HttpSession session, Topic topic, @RequestParam("content") String[] ct,
//			@RequestParam(value = "jsly", required = false) String[] tec,
//			@RequestParam(value = "fz", required = false) String fz, @RequestParam("ctn") String[] ctn, Model model) {
//		// System.out.println("测试---1" + topic);
//
//		Student student = (Student) session.getAttribute("user");
//		StuGroupTopicLink stuGroupTopicLink = null;
//		topic.setTopStatus(TopStatus.SAVE.getIndex());
//		if (student.getStuTeacherId() != null || student.getStuTeacherId() != "") {
//			topic.setTopTeacherId(student.getStuTeacherId());
//		} else {
//			topic.setTopTeacherId("mytitleTeacher");
//		}
//		if (topic.getTopSource() == 0) {
//			topic.setTopSource(3);
//		}
//		if ("".equals(topic.getTopType())) {
//			topic.setTopType(fz);
//		}
//		if (ctn != null && ct != null) {
//			if (tec != null) {
//				List<Technophobe> technophobes = new ArrayList<Technophobe>();
//				for (String aTec : tec) {
//					technophobes.add(new Technophobe(aTec));
//				}
//				topic.setTopTechnophobeList(technophobes);
//			}
//			List<ChildTopic> childTopics = new ArrayList<ChildTopic>();
//			for (int i = 0, l = ctn.length; i < l; i++) {
//				childTopics.add(new ChildTopic(ctn[i], ct[i]));
//			}
//			topic.setChildTopicList(childTopics);
//			ResultDo resultDo = serviceFit.getTopicService().saveOrUpdateTopic(topic);
//			Topic topic2 = (Topic) resultDo.getResult();
//			if (resultDo.isSuccess()) {
//				if (resultDo.getMessage().equals("true")) {
//					resultDo = serviceFit.getStuGroupService().inserttopStuLink(student.getSgId(), topic2.getTopId());
//					if (resultDo.isSuccess()) {
//						return "redirect:./toStuGroup.do";
//					} else {
//						model.addAttribute("mag", resultDo.getMessage());
//					}
//				} else {
//					return "redirect:./toStuGroup.do";
//				}
//
//			} else {
//				model.addAttribute("mag", "添加失败！");
//			}
//		}
//		return "redirect:./toMyTitle.do?sgId=" + student.getSgId() + "&" + "stuStudentId=" + student.getStuStudentId();
//	}
//
//	/**
//	 * 跳转编辑题目
//	 *
//	 * @param model
//	 * @param topId
//	 * @param session
//	 * @return
//	 */
//	@RequestMapping("topicUpdate")
//	public String uodateTopic(Model model, @RequestParam("topId") String topId, HttpSession session) {
//		ResultDo resultDo = new ResultDo();
//		Student student = (Student) session.getAttribute("user");
//		resultDo = serviceFit.getStuGroupService().selectStuGroup(student.getStuId(), student.getSgId());
//		if (resultDo.isSuccess()) {
//			model.addAttribute("headMan", true);
//		} else {
//			model.addAttribute("headMan", false);
//		}
//		int count = (Integer) serviceFit.getStuGroupService().findStatusCount(student.getSgId()).getResult();
//		model.addAttribute("topcount", count);
//		if (topId != null) {
//			resultDo = serviceFit.getTopicService().loadTopic(topId);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("topic", resultDo.getResult());
//				resultDo = serviceFit.getTopicService().findAll(Technophobe.class, new RowBounds());
//				model.addAttribute("technophobeList", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//
//		return r.getName() + "/myTitle";
//	}
//
//	@RequestMapping("submitStuTop")
//	public String submitStuGroupLink(Model model, @RequestParam("chooId") String chooId,
//			@RequestParam("topId") String topId) {
//		ResultDo resultDo = new ResultDo();
//		if (chooId != null && topId != null) {
//			resultDo = serviceFit.getStuGroupService().updateStuTopLink(chooId, topId);
//		} else {
//			model.addAttribute("mag", "数据错误！");
//		}
//
//		return "redirect:./toStuGroup.do";
//	}
//
//	/*
//	 * 学生课题情况
//	 */
//	@RequestMapping("tostuTopList")
//	public String tostuTopicList(Model model, @RequestParam(value = "sgId", required = false) String sgId) {
//		// ResultDo resultDo=serviceFit.get
//		model.addAttribute("menuSelected1", ConfigDo.STUDENT_TOPIC);
//		return r.getName() + "/stuTopList";
//	}
//
//	/**
//	 * 提交选报
//	 *
//	 * @return
//	 */
//	@RequestMapping("submitSGTL")
//	public String submitSGTL(RedirectAttributes attr, HttpSession session,
//			@RequestParam(value = "topId") String topId) {
//		Student student = (Student) session.getAttribute("user");
//		ResultDo resultDo = serviceFit.getStuGroupService().insertTopStuLink(student.getStuId(), topId);
//		if (!resultDo.isSuccess()) {
//			attr.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./toStuGroup.do";
//	}
//
//	/**
//	 * 修改密码
//	 */
//	@RequestMapping("toUpPwd")
//	public String toUpPwd() {
//		return r.getName() + "/updatePwd";
//	}
//
//	@RequestMapping("upTeaPwd")
//	public String upTeaPwd(HttpSession session, @RequestParam("o") String o, @RequestParam("n") String n,
//			@RequestParam("re") String re, Model model) {
//		ResultDo resultDo;
//		Student student = (Student) session.getAttribute("user");
//		if (n.equals(re)) {
//			if (student != null) {
////				System.out.println("测试修改密码--2" + student.getStuStudentId() + "密码" + MDUtil.MD5Tools(o));
//				resultDo = serviceFit.getEmpService().verify(student.getStuStudentId(), MDUtil.MD5Tools(o), Role.STU);
//				if (resultDo.isSuccess()) {
//					student = (Student) resultDo.getResult();
//					student.setStuPassword(MDUtil.MD5Tools(n));
//					serviceFit.getEmpService().insertAndUpdate(student, r);
//					model.addAttribute("mag", 1);
//				}
//			} else {
//				model.addAttribute("mag", "没有登录");
//			}
//		} else {
//			model.addAttribute("mag", "密码与确认密码不一致");
//		}
//		return r.getName() + "/updatePwd";
//	}
//
//	@RequestMapping("GuidanceTeacher")
//	public ModelAndView GuidanceTeacher(Map<String, Object> model, @RequestParam("stuTeacherId") String stuTeacherId) {
//		ResultDo resultDo;
//		HashMap<String, Object> map = new HashMap<String, Object>();
//		if (stuTeacherId != null || stuTeacherId != "") {
//			SelectTopic selectTopic = new SelectTopic();
//			selectTopic.setTeacherId(stuTeacherId);
//			map.put("type", "studentAllTop");
//			resultDo = serviceFit.getTopicService().selectTopicStatus(map, selectTopic, new RowBounds());
//			if (resultDo.isSuccess()) {
//				model.put("topicList", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		} else {
//			model.put("mag", "数据为空!");
//		}
//		model.put("menuSelected1", ConfigDo.STUDENT_XUANBAO);
//		model.put("menuSelected2", ConfigDo.STUDENT_ISSUES);
//		return new ModelAndView(r.getName() + "/GuidanceTeacherTopic", model);
//	}
//	/**
//	 * 跳转添加周月报页面
//	 * @param session
//	 * @param model
//	 * @param type
//	 * @return
//	 */
//	@RequestMapping("toWeekReport")
//	public ModelAndView _newReport(HttpSession session,Model model,@RequestParam("type")int type,@RequestParam(value="num",required=false)int num) {
//		Student student=(Student)session.getAttribute("user");
//		ResultDo resultDo;
//		String childName = null;
//		int weekCount,month;
//		if(student != null){
//			try {
//				//weekCount 是显示当前的周
//				weekCount = DataUtil.week(new SimpleDateFormat("yyyy-MM-dd").parse(DataLS.functionMap.get("startTime").getContent()));
//				month=DataUtil.yue();
//				model.addAttribute("weekCount",weekCount);
//				model.addAttribute("month", month);
//			} catch (ParseException e) {
//			}
//			if(type == 1){
//				resultDo=serviceFit.getReportSerivce().get(student.getStuId(),type,0);
//				if(resultDo.isSuccess()){
//					model.addAttribute("report", resultDo.getResult());
//				}
//			}else if(type ==2){
//				resultDo=serviceFit.getReportSerivce().get(student.getStuId(),type,0);
//				if(resultDo.isSuccess()){
//					model.addAttribute("report", resultDo.getResult());
//				}
//			}else if(type == 3){
//				resultDo=serviceFit.getReportSerivce().get(student.getStuId(), type, num);
//				if(resultDo.isSuccess()){
//					model.addAttribute("report", resultDo.getResult());
//					model.addAttribute("weekCount",num);
//				}
//			}else if(type == 4){
//				resultDo=serviceFit.getReportSerivce().get(student.getStuId(), type, num);
//				if(resultDo.isSuccess()){
//					model.addAttribute("report", resultDo.getResult());
//					model.addAttribute("month",num);
//				}
//			}
//			resultDo=serviceFit.getTopicService().loadTopic(student.getTopId());
//			if(resultDo.isSuccess()){
//				Topic topic=(Topic)resultDo.getResult();
//				for(ChildTopic child:topic.getChildTopicList()){
//					if(child.getId().equals(student.getChildId())){
//						childName=child.getName();
//						break;
//					}
//				}
//				model.addAttribute("childName", childName);
//				model.addAttribute("topic", resultDo.getResult());
//			}
//		}
//		return new ModelAndView(r.getName()+"/addWeekReport");
//	}
//	/**
//	 * 编辑周月报
//	 * @param id
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("editReport")
//	public ModelAndView _toEditReport(HttpSession session,@RequestParam("id") String id, Model model) {
//		Student student=(Student)session.getAttribute("user");
//		ResultDo resultDo;
//		String childName = null;
//		resultDo= serviceFit.getReportSerivce().find(id);
//		Report report=(Report)resultDo.getResult();
//		if (resultDo != null && resultDo.isSuccess()) {
//			model.addAttribute("report", resultDo.getResult());
//			if(report.getNumber() != 0){
//				if(report.getType() == 1){
//					model.addAttribute("weekCount",report.getNumber());
//				}else if(report.getType() == 2){
//					model.addAttribute("month", report.getNumber());
//				}
//			}
//		}else{
//			model.addAttribute("mag", "数据错误");
//		}
//		resultDo=serviceFit.getTopicService().loadTopic(student.getTopId());
//		if(resultDo.isSuccess()){
//			Topic topic=(Topic)resultDo.getResult();
//			for(ChildTopic child:topic.getChildTopicList()){
//				if(child.getId().equals(student.getChildId())){
//					childName=child.getName();
//					break;
//				}
//			}
//			model.addAttribute("childName", childName);
//			model.addAttribute("topic", resultDo.getResult());
//		}
//		return new ModelAndView(r.getName()+"/addWeekReport");
//	}
//	/**
//	 * 更新周月报内容
//	 * @param session
//	 * @param report
//	 * @param model
//	 * @return
//	 */
//
//	@RequestMapping("saveWeekRepport")
//	public ModelAndView _saveReport(HttpSession session,Report report, Model model) {
//		Student student = (Student) session.getAttribute("user");
//		ResultDo resultDo;
//		if (report.getId() == null || "".equals(report.getId())) {
//			resultDo = serviceFit.getReportSerivce().get(student.getStuId(), report.getType(),0);
//		} else {
//			resultDo = serviceFit.getReportSerivce().updateReport(report);
//		}
//		if (resultDo.isSuccess()) {
//			return new ModelAndView("redirect:./weekReportList.do");
//		} else {
//			model.addAttribute("report", report);
//		}
//		return new ModelAndView("redirect:./weekReportList.do");
//	}
//	/**
//	 * 周月报列表
//	 * @param session
//	 * @param model
//	 * @return
//	 */
//    @RequestMapping("weekReportList")
//	public ModelAndView _ListReport(HttpSession session, Model model) {
//    	Boolean weekReportYesOrNo;
//    	Boolean mothBoolean;
//    	ResultDo resultDo;
//		Student student = (Student) session.getAttribute("user");
//		int weekCount;
//		try {
//			weekCount = DataUtil.week(new SimpleDateFormat("yyyy-MM-dd").parse(DataLS.functionMap.get("startTime").getContent()));
//			model.addAttribute("weekCount",weekCount);
//		} catch (ParseException e) {
//		}
//		if(student != null){
//			weekReportYesOrNo=serviceFit.getReportSerivce().isWeek(student.getStuId());
//			mothBoolean=serviceFit.getReportSerivce().isYue(student.getStuId());
//		    resultDo=serviceFit.getReportSerivce().findListStu(student.getStuId());
//		    if(resultDo.isSuccess()){
////		    	System.out.println(resultDo.getResult());
//		    	model.addAttribute("weekList",resultDo.getResult());
//		    	model.addAttribute("weekBoolean", weekReportYesOrNo);
//		    	model.addAttribute("monthBoolean", mothBoolean);
//		    }
//		}
//		model.addAttribute("menuSelected1", ConfigDo.WEEK_REPORT);
//		return new ModelAndView(r.getName() + "/weekReport");
//	}
//    /**
//     * 周月报详情
//     * @param id
//     * @param model
//     * @return
//     */
//    @RequestMapping("showReport")
//	public ModelAndView _showReport(@RequestParam("id") String id, Model model) {
//		ResultDo<?> resultDo = serviceFit.getReportSerivce().find(id);
//		if (resultDo != null && resultDo.isSuccess()) {
//			model.addAttribute("info", resultDo.getResult());
//		}
//		return new ModelAndView(r.getName()+"/weekReportResult");
//	}
//
//    @RequestMapping("submitReport")
//    public ModelAndView  submitReportbyId(Model model,@RequestParam("id")String id){
//    	ResultDo resultDo=new ResultDo();
//    	if(id != null){
//    		resultDo=serviceFit.getReportSerivce().find(id);
//    		Report report=(Report)resultDo.getResult();
//    		if(report != null){
//    			report.setStatus(2);
//    			resultDo=serviceFit.getReportSerivce().updateReport(report);
//    			if(resultDo.isSuccess()){
//    				return new ModelAndView("redirect:./weekReportList.do");
//    			}else{
//    				model.addAttribute("mag", "数据错误");
//    			}
//    		}
//    	}
//    	return new ModelAndView("redirect:./weekReportList.do");
//    }
//
//
//    @RequestMapping("toAdminUploadFile")
//	public ModelAndView touploadFile(Map<String, Object> model) {
//		ResultDo resultDo = serviceFit.getFileModelService().findAll();
//		if (resultDo.isSuccess()) {
//			model.put("fileList", resultDo.getResult());
//		}
//		model.put("menuSelected1", ConfigDo.STUDENT_FILE);
//		return new ModelAndView(r.getName()+"/fileList",model);
//	}
//    @RequestMapping("StudentAchievement")
//    public ModelAndView StuAchievement(HttpSession session,Map<String, Object> model){
//    	String[] s=new String[6];
//    	Student stu=(Student) session.getAttribute("user");
//    	if(stu.isOpenTopic()){
//    		s[0]="通过";
//    	}else{
//    		s[0]="未通过";
//    	}
//    	s[1]=stu.getInterimGrade()+"";
//
//    	model.put("Achievement",s);
//    	model.put("menuSelected1", ConfigDo.STUDENT_ACHIEVEMENT);
//
//		return new ModelAndView(r.getName()+"/StuAchievement",model);
//
//    }
//
//
//}
