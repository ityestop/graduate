package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import org.junit.Test;
import test.BaseTest;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author chuchuang
 * @date 16/11/19
 */
public class AuditIssueLinkBOImplTest extends BaseTest {

    @Resource
    private AuditIssueLinkBO auditIssueLinkBO;

    @Test
    public void save() throws Exception {

        AuditIssueLink auditIssueLink;
        List<AuditIssueLink> auditIssueLinkList = new ArrayList<>(10);
        CommonResult<AuditIssueLink> result;
        for (int i = 0; i < 10; i++){
            auditIssueLink = new AuditIssueLink();
            auditIssueLink.setIdentityId(i);
            auditIssueLink.setIssueId(i);
            result = auditIssueLinkBO.save(auditIssueLink,"test");
            auditIssueLinkList.add(result.getData());
        }
        Print.println(auditIssueLinkList, AuditIssueLink.class);
    }

}