package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.InviteStatus;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Invite;
import cn.edu.zut.soft.graduate.core.query.InviteQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.InviteBO;
import cn.edu.zut.soft.graduate.groupCenter.pojo.InviteInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/29
 */
@Controller
@RequestMapping("invite")
public class InviteController extends BaseController {

    @Autowired
    private InviteBO inviteBO;

    private static final List<InviteStatus> inviteStatusList = Arrays.asList(InviteStatus.values());

    //发送邀请
    @RequestMapping("self/initiative")
    @ResponseBody
    public CommonResult initiative(HttpSession session, InviteQuery inviteQuery){
        LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
        //设定发送者
        inviteQuery.setActivesId(loginVO.getId());
        inviteQuery.setInviteStatusList(inviteStatusList);
        return inviteBO.findByQuery(inviteQuery);
    }

    //接受邀请
    @RequestMapping("self/invite")
    @ResponseBody
    public CommonResult invite(HttpSession session, InviteQuery inviteQuery){
        LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
        //设定接受者
        inviteQuery.setPassivesId(loginVO.getId());
        inviteQuery.setInviteStatusList(inviteStatusList);
        return inviteBO.findByQuery(inviteQuery);
    }

    @ResponseBody
    @RequestMapping("join/{inviteStatus}")
    public CommonResult Join(HttpSession session, Integer id,@PathVariable("inviteStatus") InviteStatus inviteStatus){
        LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
        InviteInfo inviteInfo = new InviteInfo().addLoginVO(loginVO).addInviteWithId(id);
        Assert.isTrue(Arrays.asList(InviteStatus.YES,InviteStatus.NO).contains(inviteStatus),"状态错误");
        Invite invite = inviteBO.inviteOp(inviteInfo,inviteStatus);
        if (InviteStatus.YES == inviteStatus){
            session.setAttribute(IKEY.GROUP_GROUP,invite.getGroupId());
        }
        return CommonResult.successReturn("ok");
    }

}
