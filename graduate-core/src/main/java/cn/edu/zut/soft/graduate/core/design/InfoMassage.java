package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.InfoTypeAble;

/**
 * Created by chuchuang on 16/10/23.
 */
public class InfoMassage implements InfoStrategy {

    private InfoStrategy infoStrategy;

    public InfoMassage(InfoStrategy infoStrategy) {
        this.infoStrategy = infoStrategy;
    }

    public static InfoMassage getInstantiate(InfoStrategy infoStrategy){
        return new InfoMassage(infoStrategy);
    }

    @Override
    public InfoTypeAble getType() {
        return infoStrategy.getType();
    }

    @Override
    public String getKey() {
        return infoStrategy.getKey();
    }

    @Override
    public String getValue() {
        return infoStrategy.getValue();
    }

    @Override
    public String toJson() {
        return infoStrategy.toJson();
    }
}
