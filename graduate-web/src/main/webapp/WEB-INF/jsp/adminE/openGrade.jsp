<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>小组学生</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table, .table {
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">开题成绩</li>
        </ol>
        <div class="page-header">
            <h4>开题成绩
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="form-horizontal">
        <div class="from-group">
            <label class="col-sm-1 control-label">
                开题状态:
            </label>
            <div class="col-sm-5">
                <select id="openGradeStatus" class="form-control">
                    <option value="1">校内学生</option>
                    <option value="2">校外学生</option>
                    <option value="3">二次开题</option>
                    <option value="4">未操作学生</option>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>

<div class="modal fade" id="passOpenGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">开题成绩</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="passOpenId"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩时间 </label>
                        <div class="col-sm-5">
                            <select id="open_time" class="form-control">
                                <option value="1">一辨</option>
                                <option value="2">二辨</option>
                                <option value="-1">终止答辩</option>
                            </select>
                        </div>
                        <span class="help-inline col-sm-5">
                            <i class="fa fa-info-circle"></i>
                            <font color="red">请选择学生答辩时间<1辨/2辨></font>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            成绩 </label>

                        <div class="col-sm-5">
                            <select id="open_grade" class="form-control">
                                <option value="YES">通过</option>
                                <option value="NO">拒绝</option>
                            </select>
                        </div>
                        <span class="help-inline col-sm-3">
                            <i class="fa fa-info-circle"></i>
                            <font color="red">请选择学生成绩</font>
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="open_grader_submit">提交</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restOpenGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">开题成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restOpen_id"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            答辩时间 </label>
                        <div class="col-sm-5">
                            <select id="rest_open_time" class="form-control">
                                <option value="1">一辨</option>
                                <option value="2">二辨</option>
                                <option value="-1">终止答辩</option>
                            </select>
                        </div>
                        <span class="help-inline col-sm-5">
                            <i class="fa fa-info-circle"></i>
                            <font color="red">请选择学生答辩时间<1辨/2辨></font>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            成绩 </label>

                        <div class="col-sm-5">
                            <select id="rest_grade" class="form-control">
                                <option value="YES">通过</option>
                                <option value="NO">拒绝</option>
                            </select>
                        </div>
                        <span class="help-inline col-sm-3"> <i
                                class="fa fa-info-circle"></i><font color="red">请选择学生成绩</font> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="rest_grader_submit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">

    var $url = '/admin/outIssueOpneGrade'
    var $table = $("#table");
    selectData();
    $("#openGradeStatus").change(function () {
       var $status = $("#openGradeStatus").val();
        if($status == '1'){
            $url = '/admin/outIssueOpneGrade';
            $table.bootstrapTable('refresh', {url: $url});
        }else if($status == '2'){
            $url = '/admin/outIssueOpneGradeSchool';
            $table.bootstrapTable('refresh', {url: $url});
        }else if($status == '3'){
            $url = '/admin/outIssueTwoOpenGrade';
            $table.bootstrapTable('refresh', {url: $url});
        }else if($status == '4'){
            $url = '/admin/openGradeNull';
            $table.bootstrapTable('refresh', {url: $url});
        }
    });
function selectData() {
    var columns = [
        {
            field: 'identity.id',
            title: '编号'
        },
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'infoList',
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var topicName = "";
                var topicId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC') {
                        if (array[i].key == 'topicName') {
                            topicName = array[i].value;
                        } else if (array[i].key == 'topicId') {
                            topicId = array[i].value;
                        }
                    }
                }
                return "<a href='/tea/issueDetail?id=" + topicId + "'>" + topicName + "</a>";
            },
            sortStable: true
        }, {
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '城市',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'address') {
                        str = array[i].value;
                        break;
                    }
                }
                return sourceMaps().addressMap[str];
            }
        }, {
            title: '成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "<label class='label label-warning'>未操作</label>";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'openGrade') {
                        if (array[i].value == 'YES') {
                            str = "<label class='label label-success'>通过</label>";
                            break;
                        }else if(array[i].value == 'NO'){
                            str = "<label class='label label-danger'>未通过</label>";
                            break;
                        }
                    }else if(array[i].type = 'GRADE' && array[i].key == 'stop'){
                        str = "<label class='label label-danger'>终止答辩</label>";
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'openGrade') {
                        str = "<a  data-toggle='modal' data-target='#restOpenGrade'>重置</a>";
                        break;
                    } else if (array[i].type = 'GRADE' && array[i].key == 'stop') {
                        str = "<a  data-toggle='modal' data-target='#restOpenGrade'>重置</a>";
                        break;
                    } else {
                        str = "<a  data-toggle='modal' data-target='#passOpenGrade'>录入</a>";
                    }
                }
                return str;
            }
        }
    ];
    customSearch({
        id: "#table",
        url: $url,
        dataType: "json",
        params: {},
        columns: columns,
        pageSize: 100
    });
}

    $("#passOpenGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#passOpenId").attr("value", $stuId);
    });

    $("#restOpenGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restOpen_id").attr("value", $stuId);
    });

    $("#open_grader_submit").click(function () {
        var $stuId = $("#passOpenId").val();
        var $openGrade = $("#open_grade").val();
        var $time = $("#open_time").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passOpenGrade',
            data: {
                grade: $openGrade,
                id: $stuId,
                time: $time
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }
            }
        });
    });

    $("#rest_grader_submit").click(function () {
        var $stuId = $("#restOpen_id").val();
        var $openGrade = $("#rest_grade").val();
        var $time = $("#rest_open_time").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/restOpenGrade',
            data: {
                grade: $openGrade,
                id: $stuId,
                time: $time
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }
            }
        });
    });


</script>
</body>
</html>
