package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by cc on 15/12/3.
 */
public enum MailStatus {
    ING("邀请中",0),SUC("选报成功",1),INVALID("未通过",2),NO("已经解散小组",3);

    private String name;
    private int index;

    MailStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public String toString() {
        return "name:" + name +" index:" + index;
    }

    public static MailStatus getName(int index){
        for (MailStatus s : MailStatus.values()){
            if (s.getIndex() == index){
                return s;
            }
        }
        return null;
    }

    public static MailStatus getIndex(String name){
        for (MailStatus s : MailStatus.values()){
            if (s.getName().equals(name)){
                return s;
            }
        }
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


}
