<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生基本信息管理</title>
    <!-- Latest compiled and minified CSS -->

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">学生基本信息</li>
        </ol>
        <div class="page-header">
            <h3>学生基本信息管理</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="/admin/list/stu" method="get">
                <div class="form-group">
                    <input class="form-control" style="width: 300px" title="学生姓名" placeholder="学生姓名" name="name"
                           value="${param.name}"/>
                </div>
                <div class="form-group" style="text-align: right;">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
                <div class="form-group">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                        <span><i class=" clip-plus-circle"></i> 添加学生</span>
                    </a>
                </div>
                <div class="form-group">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#AllModal">
                        <span><i class=" clip-plus-circle"></i> 导入学生</span>
                    </a>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" id="createOneGroup">
                        <span>自动建组</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="studentTable" class="table table-container" >
            </table>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
<!-- end: PAGE HEADER 頭部結束-->


<%--<div class="row">--%>
    <%--<div class="col-md-12">--%>
        <%--<div class="panel-body">--%>
            <%--<table id="teacherTable" class="table table-container">--%>
                <%--<thead>--%>
                <%--<tr>--%>
                    <%--<th>--%>
                        <%--学号--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--姓名--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--性别--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--班级--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--联系方式--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--操作--%>
                    <%--</th>--%>
                <%--</tr>--%>
                <%--</thead>--%>
                <%--<tbody id="tbody">--%>
                <%--</tbody>--%>
            <%--</table>--%>
            <%--<div id="page-nav"></div>--%>
        <%--</div>--%>

    <%--</div>--%>
    <%--<!-- end: TABLE WITH IMAGES PANEL -->--%>
<%--</div>--%>

<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="addModalLabel">添加新成员</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i>
                        <span>错误</span>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">学生学号</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="noid" title="学生学号" placeholder="学生学号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">学生姓名</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="addName" title="学生姓名" placeholder="学生姓名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">学生手机</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="addPhone" title="学生手机" placeholder="学生手机"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addNewStu()">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="AllModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="AllModalLabel">导入学生</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" enctype="multipart/form-data" id="uploadForm">

                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-remove-sign"></i>
                        <span>错误</span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">导入列表</label>
                        <div class="col-sm-9">
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="upAllStu()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="/assets/g/js/adminAllStudent.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/admin/stuAll-table-data.js"></script>
</body>
</html>
