package cn.edu.soft.fastdfs.client;

import cn.edu.soft.fastdfs.data.Result;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


public interface StorageClient {
	
	public Result<String> upload(File file, String fileName, byte storePathIndex) throws IOException;

	/**
	 * 使用字节流直接输出
	 * @param fileName
	 * @param storePathIndex
	 * @return
	 * @throws IOException
     */
	public Result<String> upload(InputStream inputStream, long filePath, String fileName, byte storePathIndex) throws IOException;
	public Result<Boolean> delete(String group, String fileName) throws IOException;
	public Result<Boolean> setMeta(String group, String fileName, Map<String, String> meta) throws IOException;
	public Result<Map<String,String>> getMeta(String group, String fileName) throws IOException;
	public void close() throws IOException;
	
}
