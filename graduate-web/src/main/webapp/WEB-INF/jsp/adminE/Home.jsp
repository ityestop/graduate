<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>软件学院毕业设计管理平台</title>
</head>

<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a>
            </li>
        </ol>
        <div class="page-header">
            <h4>
                软件学院毕业设计管理平台
                <small>毕业设计开发小组</small>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<div class="row">
    <div class="col-md-12">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5><label class="col-sm-1"></label>hi   管理员!</h5>
                        <p>
                            <label class="col-sm-1"></label> 亲 , 您发布通知可以使用公告管理的哟,可以针对教师和学生不同身份进行消息通知。<br/>
                            <label class="col-sm-1"></label> 攻城狮-告知:因今年流程与去年流程有微调变动,我们会真针对这些变动进行系统功能调整,若有 BUG 或您对用户操作不满意对地方可告知我们,我们会以最快的速度解决它,若有好的建议也可告知我们哦。谢谢!<br/>
                            <label class="col-sm-1"></label>攻城狮联系方式:<font color="red">@楚闯 tel:18638637700    @杨晓天 tel:18638512501</font>
                        </p>
                        <div class="center">
                        <img src="./img/home.png" alt="welcome!"/>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-- end: GENERAL PANEL -->
    </div>
</div>
</body>
</html>
