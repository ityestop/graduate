package cn.edu.zut.soft.graduate.groupCenter.bo;

import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.constant.InviteStatus;
import cn.edu.zut.soft.graduate.core.model.Impl.Invite;
import cn.edu.zut.soft.graduate.core.query.InviteQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.groupCenter.pojo.InviteInfo;

/**
 * @author chuchuang
 * @date 16/11/29
 */
public interface InviteBO  extends BasicBO<Invite,InviteQuery> {
    /**
     * 操作 邀请记录状态
     * @param inviteInfo
     * @param inviteStatus
     * @return
     */
    Invite inviteOp(InviteInfo inviteInfo, InviteStatus inviteStatus);

    /**
     * 删除根据小组
     * @param loginVO
     * @param id
     */
    void deleteByGroupId(LoginVO loginVO, Integer id);

}
