n<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/12/10
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">学生列表</li>
        </ol>
        <div class="page-header">
            <h3>${teacher.teaName}${teacher.jobTitle.name}的学生列表</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <div class="row"></div>
            <div class="panel-body">
                <table class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                    <thead>
                    <tr>

                        <th width="5%">
                            <small>编号</small>
                        </th>
                        <th width="15%">
                            <small>学号</small>
                        </th>
                        <th>
                            <small>学生姓名</small>
                        </th>
                        <th>
                            <small>班级</small>
                        </th>
                        <th>
                            <small>小组</small>
                        </th>
                        <th>
                            <small>电话</small>
                        </th>
                        <th width="20%">
                            <small>课题</small>
                        </th>
                        <th>
                            <small>任务</small>
                        </th>
                        <th width="15%">
                            <small>课题来源</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="s" varStatus="i">
                        <tr>
                            <td><small>${i.index + 1}</small></td>
                            <td><small>${s.stuStudentId}</small></td>
                            <td><small>${s.stuName}</small></td>
                            <td><small>${s.stuClass}</small></td>
                            <td><small>${s.sgSName}的小组</small></td>
                            <td><small>${s.stuTell}</small></td>
                            <td><small>${s.topicTitle}</small></td>
                            <td><small>${s.childTopicTitle}</small></td>
                            <td><small>${s.topSource == 3 ? "自拟题目" : (s.topicTitle == null or s.topicTitle == '') ? "无题目" : "校内题目" }</small></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-blue btn-block"
                            onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i> 返回
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
