<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>校内课题列表</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">校内课题列表</li>
        </ol>
        <div class="page-header">
            <h3>校内课题列表
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-horizontal" action="/admin/issue/list/tea" method="get">
                <div class="form-group">
                    <label class="col-sm-1 control-label">
                        教师列表
                    </label>
                    <div class="col-sm-3">
                        <select class="form-control search-select" title="教师列表" name="identityId">
                            <option value="">请选择</option>
                        </select>
                    </div>

                    <label class="col-sm-1 control-label">
                        状态列表
                    </label>
                    <div class="col-sm-3">
                        <select class="form-control search-select" title="状态列表" name="issuePhase">
                            <option value="">请选择</option>
                        </select>
                    </div>
                    <label class="col-sm-1  control-label">
                        课题人数
                    </label>
                    <div class="col-sm-3">
                        <select class="form-control search-select" title="课题人数" name="taskSize">
                            <option value="">请选择</option>
                            <option value="1" ${param.taskSize == 1 ? "selected":""}>1</option>
                            <option value="2" ${param.taskSize == 2 ? "selected":""}>2</option>
                            <option value="3" ${param.taskSize == 3 ? "selected":""}>3</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label">
                        课题名称
                    </label>
                    <div class="col-sm-6">
                        <input name="title" class="form-control" value="${param.title}" placeholder="课题名称">
                    </div>
                    <div class="col-sm-1">
                        <label class="col-sm-1 control-label"></label>
                    </div>
                    <div class="col-sm-1" style="text-align: right;">
                        <button type="submit" class="btn btn-primary">检索</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="teacherIssueTable" class="table table-container">
                <%--<thead>--%>
                <%--<tr>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                            <%--序号--%>
                        <%--</small>--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                        <%--课题名称--%>
                        <%--</small>--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                        <%--校内教师--%>
                        <%--</small>--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                        <%--课题状态--%>
                        <%--</small>--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                        <%--课题来源--%>
                        <%--</small>--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                        <%--课题任务数--%>
                        <%--</small>--%>
                    <%--</th>--%>
                    <%--<th>--%>
                        <%--<small>--%>
                        <%--创建时间--%>
                        <%--</small>--%>
                    <%--</th>--%>
                <%--</tr>--%>
                <%--</thead>--%>
                <%--<tbody id="tbody">--%>
                <%--</tbody>--%>
            </table>
            <div id="page-nav" style="float: right"></div>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>


<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>

<script type="application/javascript">
    $(function () {
        var $teaList = '${teaList}';
        var $statusList = sourceMaps().issuePhaseMap;
        if ($teaList != null && $teaList != '') {
            $.each(JSON.parse($teaList), function (i, node) {
                $("select[name='identityId']").append("<option value='" + node.id + "' "+('${param.identityId}' == node.id ? "selected" : "")+">"+node.name+"</option>");
            });
        }
        if ($statusList != null && $statusList != ''){
            $.each($statusList, function (i, node) {
                $("select[name='issuePhase']").append("<option value='" + i + "' "+('${param.issuePhase}' == i ? "selected" : "")+">"+node+"</option>");
            });
        }
    });
</script>


<script type="application/javascript">
    function getParams() {
        return {
            identityId:'${param.identityId}',
            issuePhase:'${param.issuePhase}',
            title:'${param.title}',
            taskSize:'${param.taskSize}'
        }
    }
    function selectData() {
        var columns = [
            {
                field: 'issue.id',
                title: '编号'
            }, {
                field: 'issue.title',
                title: '课题名称',
                formatter: function (value, row, index) {
                    return "<a href='/admin/issue?id="+row.issue.id+"'>"+value+"</a>"
                }
            }, {
                field: 'witness.name',
                title: '审核专家'
            }, {
                field: 'issue.schoolTeaName',
                title: '指导教师'
            }, {
                field: 'issue.status',
                title: '课题状态',
                formatter: function (value, row, index) {
                    return sourceMaps().issuePhaseMap[value];
                }
            }, {
                field: 'issue.source',
                title: '课题来源',
                formatter: function (value, row, index) {
                    return sourceMaps().issueSourceMap[value];
                }
            }, {
                field: 'issue.taskSize',
                title: '课题任务数'
            }, {
                field: 'issue.createTime',
                title: '创建时间',
                formatter: function (value, row, index) {
                    return timeStamp2StringYYYYMMDD(value);
                }
            }
        ];
        customSearch({
            id: "#teacherIssueTable",
            url: "issue/list/tea",
            dataType: "json",
            params: getParams(),
            columns: columns,
            pageSize: 25
        });
    }
    $(function () {
        selectData();
    })
</script>

<%--<script type="application/javascript">--%>
    <%--$(function () {--%>
        <%--var $table = $("#tbody");--%>
        <%--var pageSize = '${pageSize}';--%>

        <%--function getTr(num) {--%>
            <%--var $tr = $("<tr></tr>");--%>
            <%--for (var i = 0; i < num; i++) {--%>
                <%--$tr.append("<td><small></small></td>");--%>
            <%--}--%>
            <%--return $tr;--%>
        <%--}--%>

        <%--function createTable(data) {--%>
            <%--$table.html("");--%>
            <%--$.each(data, function (i, node) {--%>
                <%--var $tr = getTr(8);--%>
                <%--var td = $tr.find("small");--%>
                <%--$(td.get(0)).html(i+1);--%>
                <%--$(td.get(1)).html("<a href='/admin/issue?id="+node.id+"'>"+node.title+"</a>");--%>
                <%--$(td.get(2)).html(node.identityName);--%>
                <%--$(td.get(3)).html(${statusList}[node.status]);--%>
                <%--$(td.get(4)).html(${kindList}[node.kind]);--%>
                <%--$(td.get(5)).html(node.taskSize);--%>
                <%--$(td.get(6)).html(timeStamp2StringYYYYMMDD(node.createTime));--%>
                <%--$table.append($tr);--%>
            <%--})--%>

        <%--}--%>
        <%--function createPage(count) {--%>
            <%--var opt = {--%>
                <%--$container: $("#page-nav"),--%>
                <%--className: "pagination pagination-sm",--%>
                <%--pageCount: Math.ceil(count/pageSize),--%>
                <%--beforeFun: function (targetNum) {--%>
                    <%--var data = {--%>
                        <%--identityId:'${param.identityId}',--%>
                        <%--issuePhase:'${param.issuePhase}',--%>
                        <%--title:'${param.title}',--%>
                        <%--taskSize:'${param.taskSize}',--%>
                        <%--pageSize: pageSize,--%>
                        <%--currentPage: targetNum - 1--%>
                    <%--};--%>
                    <%--$.ajax({--%>
                        <%--url: "/issue/list/tea",--%>
                        <%--data: data,--%>
                        <%--dataType: "json",--%>
                        <%--type: "get",--%>
                        <%--success: function (result) {--%>
                            <%--if (result.success) {--%>
                                <%--createTable(result.data);--%>
                            <%--} else {--%>
                                <%--alert(result.errorMsg);--%>
                            <%--}--%>
                        <%--}--%>
                    <%--});--%>
                <%--}--%>
            <%--};--%>
            <%--createPageNav(opt);--%>
        <%--}--%>
        <%--createPage('${requestScope.count}');--%>
        <%--createTable(JSON.parse('${requestScope.list}'));--%>
    <%--});--%>
<%--</script>--%>
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
</body>
</html>
