<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/11/24
  Time: 下午7:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加新专家</title>
    <script src="IAssets/js/selectCheckbox.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">添加专家组新成员</li>
        </ol>
        <div class="page-header">
            <h3>添加专家组新成员</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <form action="./${sessionScope.pathCode}/auditExpert.do" method="post" id="condition"></form>
            <form name="addZJ" action="./${sessionScope.pathCode}/submitAuditExpert.do" method="post">
                <table class="table  table-hover">
                    <thead>
                    <tr>
                        <th>
                            <label>
                               <input type="checkbox" id="selectAll"/><small>全选</small>
                            </label>
                        </th>
                        <th>
                            <small>工号</small>
                        </th>
                        <th>
                            <small>姓名</small>
                        </th>
                        <th>
                            <small>性别</small>
                        </th>
                        <th>
                            <small>教师职称</small>
                        </th>
                        <th>
                            <small>联系方式</small>
                        </th>
                        <th>
                            <small>电子邮箱</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${teacherList}" var="teacher">
                        <tr>
                            <td>
                                <label>
                                    <input type="checkbox" name="teaId" value="${teacher.teaId}"/>
                                </label>
                            </td>
                            <td>
                                <small>${teacher.teaTeacherId}</small>
                            </td>
                            <td>
                                <small>${teacher.teaName}</small>
                            </td>
                            <td>
                                <small>${teacher.teaSex ? "女" : "男"}</small>
                            </td>
                            <td>
                                <small>${teacher.jobTitle.name}</small>
                            </td>
                            <td>
                                <small>${teacher.teaTell }</small>
                            </td>
                            <td>
                                <small>${teacher.teaEmail }</small>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                 <div class="form-group">
<label class="col-sm-8 "> </label>
<div class="col-sm-4" style="text-align: right; height: 40px">
                <page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"
                                  curPage="${pageNum}" formId="condition"/>
            </div>


</div>
              <div class="from-group">
                     <label class="col-sm-3 control-label" ></label>
                       <div class="col-sm-4">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-blue btn-block" onclick="submitForm()">
                              	 <i class="clip-checkmark-2  "></i>    添加
                            </button>
                        </div>
                        <div class="col-sm-6">
									<button type="button" class="btn btn-blue btn-block"
										onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i>   返回</button>
								</div>
						</div>
                    </div>
                    
            </form>
        </div>
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
</body>
</html>
