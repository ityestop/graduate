package cn.edu.zut.soft.graduate.topicCenter.pojo;

import cn.edu.zut.soft.graduate.core.vo.IssueVO;
import cn.edu.zut.soft.graduate.core.vo.WitnessData;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 课题携带专家属性
 * @author cc
 * @Date 2016/12/21
 * @value 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IssueTakeWitness extends IssueVO {
    private WitnessData witness;
}
