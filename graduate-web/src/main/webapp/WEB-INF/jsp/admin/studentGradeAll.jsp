<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<html>
<head>
<title>学生答辩成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">学生答辩成绩</li>
			</ol>
			<div class="page-header">
				<h3>学生答辩成绩</h3>
				<div style="text-align: right;"><a href="./${sessionScope.pathCode }/downStudentGradeAll.do"><i class=" clip-download-2"></i> 学生成绩导出</a></div>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>

	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<sf:form id="condition" class="form-horizontal"
					action="./${sessionScope.pathCode}/toTermGrade.do" method="post"
					commandName="studentGradeTerm">
					<!--  panel-scroll" style="height:215px" -->
					<div class="form-group">
						<label class="col-sm-1 control-label"> 学生姓名 </label>
						<div class="col-sm-2">
							<sf:input path="stuName" class="form-control" />
						</div>
						<label class="col-sm-1 control-label"> 学生状态 </label>

						<div class="col-sm-3">
							<sf:select class="form-control search-select" path="grade"
								id="grade">
								<sf:option value="0">全部</sf:option>
								<sf:option value="90">成绩优秀</sf:option>
								<sf:option value="1">成绩进度未完成</sf:option>
								<sf:option value="3">二次答辩</sf:option>
								<sf:option value="-1">终止答辩</sf:option>
							</sf:select>
						</div>

						<label class="col-sm-1 control-label"> 指导教师 </label>
						<div class="col-sm-3">
							<sf:select class="form-control search-select" path="teacherId"
								id="teaName">
								<sf:option value="null" label="全部" />
								<sf:options items="${teaList}" itemValue="teaId"
									itemLabel="teaName" />
							</sf:select>
						</div>

						<div class="col-sm-1">
							<button type="submit" class="btn btn-primary">检索</button>
						</div>
					</div>
				</sf:form>
				<table class=" table table-hover">
					<thead>
						<tr>
							<th><small>学号</small></th>
							<th><small>姓名</small></th>
							<th><small>性别</small></th>
							<th><small>班级</small></th>
							<th><small>指导教师</small></th>
							<th><small>指导成绩</small></th>
							<th><small>评阅成绩</small></th>
							<th><small>答辩成绩</small></th>
							<th><small>最终答辩成绩</small></th>
						</tr>
					</thead>
					<c:forEach items="${student}" var="student">
						<tbody>
							<tr>
								<td><small class="stuId">${student.stuStudentId}</small></td>
								<td><small>${student.stuName}</small></td>
								<c:choose>
									<c:when test="${student.stuSex eq false }">
										<td><small>男</small></td>
									</c:when>
									<c:when test="${student.stuSex eq true }">
										<td><small>女</small></td>
									</c:when>
								</c:choose>
								<td><small>${student.stuClass }</small></td>
								<td><small>${student.teaName}</small></td>
								<td><small>${student.donGrade}</small></td>
								<td><small>${student.documentGrade}</small></td>
								<td><small>${student.replyGrade}</small></td>
								<td><small>${student.finalReplyGrade}</small></td>
							</tr>
						</tbody>
					</c:forEach>
				</table>
				<div class="form-group">
					<label class="col-sm-4 "> </label>
					<div class="col-sm-8" style="text-align: right; height: 40px">
						<page:createPager pageSize="${pageSize}" totalPage="${totalPage}"
							totalCount="${totalCount}" curPage="${pageNum}"
							formId="condition" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
</body>
</html>
