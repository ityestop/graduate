package cn.edu.zut.soft.graduate.model;

/**
 * <p>文件名称：HTopic.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/23 下午3:53</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HTopic {

    private Integer id;

    private String teacher;

    private String title;

    private Integer size;

    private String status;

    private String context;

    private String result;

    private String years;

    public HTopic() {
    }

    public HTopic(Integer id, String teacher, String title, Integer size, String status, String context, String result, String years) {
        this.id = id;
        this.teacher = teacher;
        this.title = title;
        this.size = size;
        this.status = status;
        this.context = context;
        this.result = result;
        this.years = years;
    }

    public Integer getId() {
        return id;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getTitle() {
        return title;
    }

    public Integer getSize() {
        return size;
    }

    public String getStatus() {
        return status;
    }

    public String getContext() {
        return context;
    }

    public String getResult() {
        return result;
    }

    public String getYears() {
        return years;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setYears(String years) {
        this.years = years;
    }
}
