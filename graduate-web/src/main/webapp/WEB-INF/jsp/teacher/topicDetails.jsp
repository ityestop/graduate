<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>题目详情</title>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">题目详情</li>
			</ol>
			<div class="page-header">
				<h3>题目详情</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">

			<div class="panel-body">
				<div class="form-group">
					<table class="table">
						<tbody>
							<tr>
								<td width="10%"><small>题目标题:</small></td>
								<td><small>${topic.topTitle}</small></td>
							</tr>
							<tr>
								<td width="10%"><small>申报教师：</small></td>
								<td><small>${topic.teacher.teaName}</small></td>
							</tr>
							<c:choose>
								<c:when test="${topic.topStatus eq 3 }">
									<tr class="danger">
										<td><small>管理员批语</small></td>
										<td><small><font color="red">${topic.topRemark}</font>
										</small></td>
									</tr>
								</c:when>
							</c:choose>
							<tr>
								<td><small>课题来源:</small></td>
								<td><c:if test="${topic.topTopicType eq false }">
										<small>社会服务</small>
									</c:if> <c:if test="${topic.topTopicType eq true }">
										<small>科研项目</small>
									</c:if></td>
							</tr>
							<tr>
								<td><small>课题类型:</small></td>
								<td><c:if test="${topic.topType eq '设计' }">
										<small>设计</small>
									</c:if> <c:if test="${topic.topType eq '研究' }">
										<small>研究</small>
									</c:if> <c:if test="${topic.topType != '研究' && topic.topType != '设计'}">
										<small>${topic.topType}</small>
									</c:if></td>
							</tr>
							<tr>
								<td><small>详细来源:</small></td>
								<td><c:if test="${topic.topSource eq 3 }">
										<small><font color="red">自拟题目</font> </small>
									</c:if> <c:if test="${topic.topSource lt  3}">
										<small><font color="red">校内题目</font></small>
									</c:if> </td>
							</tr>
							<tr>
								<td><small>技术领域:</small></td>
								<td><c:forEach items="${topic.topTechnophobeList}" var="t">
										<small> ${t.techName}、</small>
									</c:forEach></td>
							</tr>
							<tr>
								<td><small>项目背景:</small></td>
								<td><small><pre>${topic.topContent}</pre>
								</small></td>
							</tr>
							<tr>
								<td colspan="2"><c:forEach items="${topic.childTopicList}"
										var="c">
										<small><strong>${c.name}</strong>
										</small>
										<small> <pre>${c.content}</pre>
										</small>
									</c:forEach></td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: center;"><c:if
										test="${topic.status.index == 1}">
										<form action="./${sessionScope.pathCode}/submitAdTopic.do"
											method="post">
											<input type="hidden" value="${topic.topId }" name="id">
											<input type="hidden" value="2" name="status"> <label
												class="col-sm-3 control-label"></label>

											<div class="col-sm-2">
												<button type="submit" class="btn btn-blue btn-block"
													id="submit">
													<span><i class="clip-checkmark-2  "></i> 通过</span>
												</button>
											</div>
											<div class="col-sm-2">
												<button type="button" class="btn btn-blue btn-block"
												data-toggle="modal" data-target="#createStuGroup">
													<span><i class=" clip-cancel-circle  "></i> 不通过</span>
												</button>
											</div>
											<div class="col-sm-2">
												<button type="button" class="btn btn-blue btn-block"
													onclick="window.location.href='./${sessionScope.pathCode}/toteacAndTopicChoose.do'">
													<span><i class=" clip-arrow-right-2  "></i> 返回</span>
												</button>
											</div>
										</form>
									</c:if></td>

							</tr>
						</tbody>
					</table>
				</div>

				<c:if test="${SGTLink != null && SGTLink.size() > 0}">
					<table width="100%" class="table   table-hover">
						<thead>
							<tr>
								<th><small>小组编号</small></th>
								<th><small>小组成员</small></th>
								<th><small>操作</small></th>
							</tr>
						</thead>
						<c:forEach items="${SGTLink}" var="li">
							<tbody>
								<tr>
									<td><small>${li.choStuGroup.head.stuName}的小组</small></td>
									<td>
										<ul>
											<c:forEach items="${li.choStuGroup.studentList}" var="lis">
												<li><small>${lis.stuName} ----
														${lis.stuStudentId} ---- ${lis.stuClass}</small></li>
											</c:forEach>
										</ul></td>
									<td><small>操作</small></td>
								</tr>
							</tbody>
						</c:forEach>
					</table>
				</c:if>
				<div class="form-group">
					<c:choose>
						<c:when test="${topic.topStatus eq 2 or topic.topStatus eq 0}">
							<label class="col-sm-10 control-label"></label>
							<div class="col-sm-2">
								<button type="button" class="btn btn-blue btn-block"
									onclick="window.location.href='javascript:history.go(-1);'">
									<i class="clip-arrow-right-2 "></i> 返回
								</button>
							</div>
						</c:when>
						<c:when test="${topic.topStatus eq 3 or topic.topStatus eq 4 }">
							<label class="col-sm-3 control-label"></label>
							<div class="col-sm-2">
								<button type="button" class="btn btn-blue btn-block"
									onclick="window.location.href='./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}'">
									<i class=" clip-pencil-3 "></i> 编辑
								</button>
							</div>
							<div class="col-sm-2">
								<button type="button" class="btn btn-blue btn-block"
									onclick="window.location.href='./${sessionScope.pathCode}/submitTopic.do?id=${topic.topId}&statues='">
									<i class="clip-checkmark-2"></i> 提交
								</button>
							</div>

							<div class="col-sm-2">
								<button type="button" class="btn btn-blue btn-block"
									onclick="window.location.href='./teacher/teaTopicList.do'">
									<i class="clip-arrow-right-2 "></i> 返回
								</button>
							</div>
							<label class="col-sm-3 control-label"></label>
						</c:when>
					</c:choose>
				</div>

			</div>

		</div>
	</div>
<div class="modal fade" id="createStuGroup" tabindex="-1" role="dialog" aria-labelledby="CSGModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLabel">权限</h4>
            </div>
            <div class="modal-body">
          		您没有权限拒绝此课题，请与管理员联系！
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
