<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>周月报未提交统计</title>
 <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>


<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">周月报未交统计</li>
        </ol>
        <div class="page-header">
            <h3>周月报未交统计</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<table>

</table>


<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <%--<!--  panel-scroll" style="height:215px" -->--%>
          
            <div class="panel-body">
                <div class="row"></div>
                <div class="panel-body">
                    <table class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                        <thead>
                        <tr>

                            <th width="10%">
                                <small>学生姓名</small>
                            </th>
                            <th width="10%">
                                <small>性别</small>
                            </th>
                            <th width="15%">
                                <small>班级</small>
                            </th>
                            <th width="45%">
                                <small>未交周次</small>
                            </th>
                            <th width="20%">
                                <small>未交月份</small>
                            </th>
                        
                        </tr>
                        </thead>
                        <tbody>
                     <c:forEach items="${report}" var="r" >
                        <tr>
                            <td><small>${r[0]}</small></td>
                            <td><small><c:if test="${r[1] eq '男'}">
											女
										</c:if>
										<c:if test="${r[1] eq '女'}">
											男
										</c:if></small></td>
                            <td><small>${r[2]}</small></td>
                            <td><small>${r[3]}</small></td>
                            <td><small>${r[4]}</small></td>
                       
                        </tr>
                    </c:forEach>
                        </tbody>
                    </table>
                    <div class="form-group right">
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




</body>
</html>