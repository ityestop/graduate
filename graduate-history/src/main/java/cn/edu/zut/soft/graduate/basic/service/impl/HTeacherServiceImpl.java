package cn.edu.zut.soft.graduate.basic.service.impl;

import cn.edu.zut.soft.graduate.basic.service.HTeacherService;
import cn.edu.zut.soft.graduate.basic.dao.HTeacherDao;
import cn.edu.zut.soft.graduate.model.HTeacher;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>文件名称：HTeacherServiceImpl.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/29 上午12:48</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Repository
public class HTeacherServiceImpl implements HTeacherService {

    @Resource
    private HTeacherDao hTeacherDao;

    @Override
    public Integer save(HTeacher hTeacher) {
        Integer result = null;
        if (hTeacher != null){
            Integer save = hTeacherDao.save(hTeacher);
            if (save > 0){
                result = hTeacher.getId();
            }
        }
        return result;
    }

    @Override
    public Integer update(HTeacher hTeacher) {
        Integer result = null;
        if (hTeacher != null){
            result = hTeacherDao.updateByPrimaryKeySelective(hTeacher);
        }
        return result;
    }

    @Override
    public boolean delete(Integer id) {
        boolean result = false;
        if (id != null){
            Integer delete = hTeacherDao.deleteByHTeacher(id);
            if (delete > 0){
                result = true;
            }
        }
        return result;
    }

    @Override
    public Integer count() {
        return hTeacherDao.count();
    }

    @Override
    public List<HTeacher> findAll() {
        return null;
    }
}
