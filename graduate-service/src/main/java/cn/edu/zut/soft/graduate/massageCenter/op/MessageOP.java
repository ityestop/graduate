package cn.edu.zut.soft.graduate.massageCenter.op;


import cn.edu.zut.soft.graduate.massageCenter.generator.Builder;
import cn.edu.zut.soft.graduate.massageCenter.pojo.SendMessage;

/**
 * @author chuchuang
 * @Date 16/11/16
 */
public interface MessageOP {

    /**
     * 招募信息
     */

    void sendMessage(SendMessage sendMessage,Builder builder);

}
