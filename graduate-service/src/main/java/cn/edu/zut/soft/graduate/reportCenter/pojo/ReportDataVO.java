package cn.edu.zut.soft.graduate.reportCenter.pojo;

/**
 * @author cc
 * @Date 2017/2/20
 * @value 1.0
 */
public class ReportDataVO {

    private ReportDataDTO reportData;

    private ReportRuleDTO reportRule;

    public ReportDataVO(ReportDataDTO reportData) {
        this.reportData = reportData;
    }

    public ReportDataVO(ReportDataDTO reportData, ReportRuleDTO reportRule) {
        this.reportData = reportData;
        this.reportRule = reportRule;
    }

    public ReportDataDTO getReportData() {
        return reportData;
    }

    public void setReportData(ReportDataDTO reportData) {
        this.reportData = reportData;
    }

    public ReportRuleDTO getReportRule() {
        return reportRule;
    }

    public void setReportRule(ReportRuleDTO reportRule) {
        this.reportRule = reportRule;
    }
}
