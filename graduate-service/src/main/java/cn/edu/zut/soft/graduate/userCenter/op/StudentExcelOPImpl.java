package cn.edu.zut.soft.graduate.userCenter.op;

import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.dao.InfoDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by chuchuang on 16/11/14.
 */
@Service
public class StudentExcelOPImpl implements StudentExcelOP {

    protected Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private IdentityBO identityBO;
    @Autowired
    private InfoBO infoBO;

    @Autowired
    private InfoDAO<Info> infoDAO;

    private static final String CLAZZ = "clazz";

    @Override
    @Transactional
    public void Handle(Collection collection) {
        if (!(collection instanceof List)) {
            return;
        }
        List<Map<String, String>> mapList = (List<Map<String, String>>) collection;
        Identity identity;
        Info info;
        for (Map<String, String> map : mapList) {
            identity = new Identity();
            try {
                identity.setRole(Role.stu);
                identity.setNoId(map.get("3"));
                identity.setPwd(identity.getNoId());
                identity.setSex(map.get("5").equals("男") ? 1 : -1);
                identity.setName(map.get("4"));
                identityBO.save(identity, "studentOP");
                info = new Info();
                info.setIdentityId(identity.getId());
                info.setIdentityRole(identity.getRole());
                info.setIdentityName(identity.getName());
                info.setType(InfoType.Basic.name());
                info.setKey(CLAZZ);
                info.setValue(map.get("2"));
                infoDAO.insert(info);
                logger.info("identity is OK :" + identity);
            } catch (Exception e) {
                logger.error("identity is error " + identity, e);
            }
        }
        logger.info("identity ok _______________");

    }
}
