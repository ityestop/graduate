package cn.edu.zut.soft.graduate.basic.dao;


import cn.edu.zut.soft.graduate.model.HTeacher;
import org.springframework.stereotype.Repository;

/**
 * <p>文件名称：HTeacher.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/23 下午4:02</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Repository
public interface HTeacherDao {

    int save(HTeacher hTeacher);

    int saveSelective(HTeacher hTeacher);


    int update(HTeacher hTeacher);

    int updateByPrimaryKeySelective(HTeacher hTeacher);

    int deleteByHTeacher(Integer id);

    int count();
}
