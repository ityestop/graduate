package cn.edu.zut.soft.graduate.core.constant;

import java.util.regex.Pattern;

/**
 * Created by chuchuang on 16/10/22.
 */
public enum LoginMode implements Matching {
    PHONE("phone","^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\\d{8}$"),
    EMAIL("email","^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$"),
    NID("noid","^\\d{4}|\\d{12}$");

    private String rule;

    private String tag;

    LoginMode(String tag, String rule) {
        this.tag = tag;
        this.rule = rule;
    }

    public String getTag() {
        return tag;
    }

    public static LoginMode getLoginMode(String str){
        for (LoginMode loginMode : LoginMode.values()){
            if (loginMode.isMatching(str)){
                return loginMode;
            }
        }
        return null;
    }

    @Override
    public boolean isMatching(String str) {
        return Pattern.compile(this.rule).matcher(str).matches();
    }

    public static void main(String[] args){
        LoginMode loginMode = LoginMode.PHONE;
        loginMode.isMatching("17317140434");
    }
}
