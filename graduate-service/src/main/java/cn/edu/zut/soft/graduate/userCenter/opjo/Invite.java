package cn.edu.zut.soft.graduate.userCenter.opjo;

import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import lombok.Data;

/**
 * @author chuchuang
 * @date 16/11/20
 */
@Data
public class Invite {
    private LoginVO user;
    private Role.Runner runner;//角色类型,admin OR user
    private Integer owner; // 组长
    private IdentityQuery identityQuery;
}
