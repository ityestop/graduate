package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.model.Impl.Notice;
import cn.edu.zut.soft.graduate.core.query.NoticeQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.Owner;
import cn.edu.zut.soft.graduate.massageCenter.bo.NoticeBO;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chuchuang on 16/10/29.
 */
@Controller
@RequestMapping("notice")
public class NoticeController extends BaseController {

    @Resource
    private NoticeBO noticeBO;

    @RequestMapping("self")
    @ResponseBody
    public CommonResult aboutSelfTitle(HttpSession session,NoticeQuery noticeQuery){
        LoginVO loginVO = (LoginVO) session.getAttribute("user");
        if (loginVO == null || loginVO.getId() == null){
            return CommonResult.errorReturn("未登录,请重新登录");
        }
        noticeQuery.setRole(loginVO.getRole());
        return noticeBO.findByQuery(noticeQuery);
    }

    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    @ResponseBody
    public CommonResult get(@PathVariable("id") String id){
        return CommonResult.successReturn(noticeBO.get(Integer.parseInt(id)));
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public CommonResult save(HttpSession session,Notice notice,@RequestParam("roles[]") String[] roles){
        if (notice == null){
            return CommonResult.errorReturn("传输数据错误");
        }
        //转义
        notice.setContent(StringEscapeUtils.unescapeHtml4(notice.getContent()));
        LoginVO loginVO = (LoginVO) session.getAttribute("user");
        if (loginVO == null || loginVO.getId() == null){
            return CommonResult.errorReturn("未登录,请重新登录");
        }
        List<Role> roleList = new ArrayList<>(2);
        for (String r : roles){
            Role role = Role.valueOf(r);
            roleList.add(role);
        }
        return noticeBO.save(notice,new Owner(loginVO.getId(),loginVO.getName(),loginVO.getRole()),roleList,loginVO.getName());
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public CommonResult list(NoticeQuery noticeQuery){
//        LoginVO loginVO = (LoginVO) session.getAttribute("user");
//        if (loginVO == null || loginVO.getId() == null){
//            return CommonResult.errorReturn("未登录,请重新登录");
//        }
//        List<Role> roleList = new ArrayList<>(2);
//        for (String r : roles){
//            Role role = Role.valueOf(r);
//            roleList.add(role);
//        }
        return noticeBO.findByQuery(noticeQuery);
    }

    @RequestMapping(value = "{id}",method = RequestMethod.POST)
    @ResponseBody
    public CommonResult update(HttpServletRequest request, HttpSession session, Notice notice, @RequestParam("roles[]") String roles[], @PathVariable("id") String id){

        if (notice == null){
            return CommonResult.errorReturn("传输数据错误");
        }
        //反解密
        notice.setContent(StringEscapeUtils.unescapeHtml4(notice.getContent()));
        LoginVO loginVO = (LoginVO) session.getAttribute("user");
        if (loginVO == null || loginVO.getId() == null){
            return CommonResult.errorReturn("未登录,请重新登录");
        }
        List<Role> roleList = new ArrayList<>(2);
        for (String r : roles){
            Role role = Role.valueOf(r);
            roleList.add(role);
        }
        return noticeBO.update(notice,new Owner(loginVO.getId(),loginVO.getName(),loginVO.getRole()),roleList,loginVO.getName());
    }

}
