package cn.edu.zut.soft.graduate.topicCenter.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>文件名称：IssueDaos.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/26 下午11:16</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Component
public interface IssueDAO<T extends Issue> extends BasicDAO<Integer,T> {

    int updateByExampleWithBLOBs(@Param("record") T record, @Param("example") Example example);

    List<T> selectByExampleWithBLOBs(Example example);

    int updateByPrimaryKeyWithBLOBs(T record);

}
