package cn.edu.zut.soft.graduate.base.utils.Comparator;

/**
 * @author cc
 * @Date 2017/1/5
 * @value 1.0
 */
public class ComparatorException extends RuntimeException {

    private ComparatorException(String message, Throwable cause) {
        super(message, cause);
    }

    private ComparatorException(String message) {
        super(message);
    }

    public static ComparatorException create(String message){
        return new ComparatorException(message);
    }
    public static ComparatorException create(String message,Throwable cause){
        return new ComparatorException(message,cause);
    }


}
