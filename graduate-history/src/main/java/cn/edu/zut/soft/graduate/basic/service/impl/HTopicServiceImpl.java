//package cn.edu.zut.soft.graduate.service.impl;
//
//
//import cn.edu.zut.soft.graduate.dao.HTopicDao;
//import cn.edu.zut.soft.graduate.model.HTopic;
//import cn.edu.zut.soft.graduate.service.HTopicService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import javax.annotation.Resource;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * <p>文件名称：HTopicServiceImpl.java</p>
// * <p>文件描述：</p>
// * <p>版权所有： 版权所有(C)2011-2099</p>
// * <p>公   司： 口袋购物 </p>
// * <p>内容摘要： </p>
// * <p>其他说明： </p>
// * <p>完成日期：16/10/29 上午12:43</p>
// *
// * @author yangxiaotian@weidian.com
// * @version 1.0
// */
//@Service
//public class HTopicServiceImpl implements HTopicService {
//
//    @Resource
//    private HTopicDao hTopicDao;
//
//    @Override
//    public Integer save(HTopic hTopic) {
//        Integer result = null;
//        if (hTopic != null){
//            Integer row = hTopicDao.saveSelective(hTopic);
//            if (row > 0) {
//                result = hTopic.getId();
//            }
//        }
//        return result;
//    }
//
//    @Override
//    public Integer update(HTopic hTopic) {
//        Integer result = null;
//        if (hTopic != null){
//            result = hTopicDao.updateByPrimaryKeySelective(hTopic);
//        }
//        return result;
//    }
//
//    @Override
//    public boolean delteTopic(Integer id) {
//        boolean result = false;
//        if (id != null){
//            Integer delete = hTopicDao.deleteByHTopic(id);
//            if (delete > 0){
//                result = true;
//            }
//        }
//        return result;
//    }
//
//    @Override
//    public Integer count() {
//        return hTopicDao.count();
//    }
//
//    @Override
//    public List<HTopic> findAll() {
//        return null;
//    }
//
//    @Override
//    public List<HTopic> selectByTeaName(String teaName) {
//        if (StringUtils.isEmpty(teaName)){
//            return null;
//        }
//        return hTopicDao.selectByTeacherName(teaName);
//    }
//
//    @Override
//    public HTopic selectById(Integer id) {
//        if (id == null){
//            return  null;
//        }
//        return hTopicDao.selectById(id);
//    }
//}
