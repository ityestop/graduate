package cn.edu.zut.soft.graduate.groupCenter.op.impl;

import cn.edu.zut.soft.basic.core.model.pojo.Relation;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.groupCenter.op.GroupOP;
import cn.edu.zut.soft.graduate.groupCenter.pojo.SaveUserGroup;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/20
 */
@Component
public class GroupOPImpl implements GroupOP {

    @Autowired
    private GroupBO groupBO;

    @Autowired
    private InfoBO infoBO;

    @Override
    @Transactional
    public Group OwnerCreateGroup(LoginVO loginVO, Identity identity, String operator) {
        //保存小组
        GroupQuery groupQuery = new GroupQuery();
        groupQuery.setIdentityId(identity.getId());
        groupQuery.setRole(identity.getRole());
        List list = groupBO.findByQuery(groupQuery).getData();
        Assert.isTrue(!(list  != null && list.size() > IKEY.ZERO),"组长已经创建小组");
        Group group = new Group();
        group.setIdentityId(identity.getId());
        group.setRole(identity.getRole());
        group.setIdentityName(identity.getName());
        group.setStatus(IssueSource.school);
        group.setStateSets(IssueSource.school.name());
        //初始小组创建只能有一个人
        group.setCount(1);
        group = groupBO.save(group, operator).getData();
        //保存小组长身份
        infoBO.save(new BasicInfoStrategy(InfoType.Group,IKEY.GroupLeader,""+group.getId()),identity.getId(),loginVO.getName());
        //关联小组
        SaveUserGroup saveUserGroup = new SaveUserGroup();
        saveUserGroup.setUser(loginVO);
        saveUserGroup.setGroup(group);
        saveUserGroup.setIdentity(identity);
        Assert.isTrue(groupBO.associationIDGroup(saveUserGroup, Relation.AND), "关联小组失败");
//        groupBO.update(group,loginVO.getName());
        return group;
    }

    @Override
    public Group JoinGroup(LoginVO loginVO, Identity identity, Group group) {
        //关联小组
        SaveUserGroup saveUserGroup = new SaveUserGroup();
        saveUserGroup.setUser(loginVO);
        saveUserGroup.setGroup(group);
        saveUserGroup.setIdentity(identity);
        Assert.isTrue(groupBO.associationIDGroup(saveUserGroup, Relation.AND), "关联小组失败");
        group.setCount(group.getCount() + 1);
        groupBO.update(group, loginVO.getName());
        return group;
    }
}
