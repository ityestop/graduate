package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.userCenter.opjo.Teacher;

import java.util.List;
import java.util.Map;

/**
 * @author chuchuang
 * @date 16/12/5
 */
public interface TeacherBO {
    /**
     * 检索所有教师以及所带的学生情况
     */
    CommonResult<List<Teacher>> queryTeaOfStuName(IdentityQuery identityQuery);

    /**
     * 全部未分组学生数不为零的教师列表，
     */
    CommonResult<List<Identity>> queryNoGroupAndNoStuOfTea(IdentityQuery identityQuery);

    /**
     * 获取全部存在学生的指导教师
     * @param identityQuery 检索条件
     * @return 根据教师的一个对应关系
     */
    CommonResult<Map<Integer,List<Info>>> queryExistStudentOfTea(IdentityQuery identityQuery);


}
