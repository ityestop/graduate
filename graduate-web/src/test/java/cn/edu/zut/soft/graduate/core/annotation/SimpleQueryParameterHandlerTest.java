package cn.edu.zut.soft.graduate.core.annotation;

import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.graduate.reportCenter.query.Impl.SimpleReportDataQuery;
import org.junit.Test;

import java.util.Collections;

/**
 * Created by cc on 2017/2/19.
 */
public class SimpleQueryParameterHandlerTest {
    @Test
    public void handler() throws Exception {
        Criteria criteria = new Criteria();
        SimpleReportDataQuery query = new SimpleReportDataQuery();
        query.setIds(Collections.singletonList(10));
        query.setRuleIds(Collections.singletonList(423));
        query.setIdentityIds(Collections.singletonList(30));
        SimpleQueryParameterHandler.DEFAULT.handler(criteria,query);
    }

}