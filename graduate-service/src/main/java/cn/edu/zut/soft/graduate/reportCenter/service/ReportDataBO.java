package cn.edu.zut.soft.graduate.reportCenter.service;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportData;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportDataBO  extends BasicBO<ReportData,Query> {

}
