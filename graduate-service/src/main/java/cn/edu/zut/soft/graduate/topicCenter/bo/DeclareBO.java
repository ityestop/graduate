package cn.edu.zut.soft.graduate.topicCenter.bo;

/**
 * @author chuchuang
 * @date 16/11/27
 */

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.DeclareQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.ConfirmInfo;
import cn.edu.zut.soft.graduate.topicCenter.pojo.DeclareInfo;

import java.util.List;

/**
 * 小组申报课题管理
 */
public interface DeclareBO extends BasicBO<Declare,DeclareQuery> {

    //小组申报,loginvo,issueid,
    /**
     * 学生申报课题
     * @param loginVO
     * @param issue
     * @return
     */
    Declare GroupDeclare(LoginVO loginVO,Issue issue);

    //效验是否能够申请 loginvo,issueid,
    boolean isAsk(LoginVO loginVO, Issue issue);

    //根据loginvo,选报的课题列表(封装状态)。
    /**
     * 检索选报结果
     * @param declareQuery
     * @return
     */
    List<DeclareInfo> findDeclareInfo(DeclareQuery declareQuery);

    /**
     * 教师处理申报操作
     * @param loginVO
     * @param alternative
     * @return
     */
    ConfirmInfo TeaConfirm(LoginVO loginVO, Integer declareId, Alternative alternative);

    /**
     * 1.拒绝所有申报记录
     * 2.同意当前申报记录
     * @param confirmInfo
     * @return
     */
    ConfirmInfo Confirm(ConfirmInfo confirmInfo);

    List<Declare> deleteByGroupId(Integer id,String operator);

}
