package cn.edu.zut.soft.graduate.base.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
public interface BasicBO<T, Q> {
    CommonResult<T> save(T t, String operator);

    //默认使用主键为integer类型
    T get(Integer id);

    CommonResult<T> update(T t, String operator);

    CommonResult<List<T>> findByQuery(Q q);

    CommonResult<T> delete(Integer id, String operator);

    //    CommonResult<List<T>> selectByExample(Example example);
    public int countByQuery(Q q);
}
