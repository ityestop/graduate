package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.websocket.UserMonitoringHandler;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.TextMessage;

import java.io.IOException;

/**
 * @author cc
 * @Date 2016/11/15
 * @value 1.0
 */
@RequestMapping("backDoor")
@Controller
public class WebSocketController extends BaseController {
    @RequestMapping("restartTomcat")
    @ResponseBody
    public CommonResult restartTomcat() throws IOException {
        CommonResult result = CommonResult.successReturn("系统将于10分钟之后进行重启,请保存数据,谢谢合作");
        userMonitoringHandler.broadcast(new TextMessage(JSON.toJSONBytes(result)));
        return  result;
    }

    @RequestMapping("onlineNumber")
    @ResponseBody
    public CommonResult onlineNumber(){
        return CommonResult.successReturn(UserMonitoringHandler.userSocketSessionMap.size());
    }
}
