<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>小组学生</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table{
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">小组学生</li>
        </ol>
        <div class="page-header">
            <h4>小组学生
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12" ${sessionScope.Groupgroup == null ? "hidden" : ""}>
            <a class="btn btn-primary" href="/file/downloadTeaGroupStu?groupId=${sessionScope.Groupgroup}&reverse=${param.get("reverse") == null ? 'false' : param.get("reverse") }" >教师小组学生下载</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>


<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">

    var columns = [
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'identity.phone',
            title: '手机'
        }, {
            field: 'infoList',
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var topicName = "";
                var topicId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC') {
                        if (array[i].key == 'topicName') {
                            topicName = array[i].value;
                        }else if (array[i].key == 'topicId'){
                            topicId = array[i].value;
                        }
                    }
                }
                return "<a href='/tea/issueDetail?id="+topicId+"'>"+topicName+"</a>";
            },
            sortStable: true
        },{
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        },{
            title: '是/否',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "<font color='red'>未分配<font>";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = "已分配";
                        break;
                    }
                }
                return str;
            }
        },{
            title:'答辩地区',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for(var i = 0; i < array.length;i++){
                    if (array[i].key == 'address' && array[i].value == 'shanghai'){
                        str = '上海';
                        break;
                    }else if(array[i].key == 'address' && array[i].value == 'beijing'){
                        str = '北京';
                        break;
                    }else if(array[i].key == 'address' && array[i].value == 'hangzhou'){
                        str = '杭州';
                        break;
                    }else{
                        str = '郑州';
                    }
                }
                return str;
            }
        }
    ];
    customSearch({
        id: "#table",
        url: "/identity/teaGroupStu",
        dataType: "json",
        params:{groupId:${sessionScope.Groupgroup},reverse:${param.get("reverse")}+'' },
        columns: columns,
        pageSize: 25
    });

</script>
</body>
</html>
