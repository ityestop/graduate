//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.model.Student;
//import cn.edu.zut.soft.graduate.core.model.Teacher;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import java.io.File;
//
//
////import cn.springmvc.entry.Employee;
//@Controller
//@RequestMapping("/admin/")
//public class UploadController implements ConfigDo{
//
//    @Resource
//    private ServiceFit serviceFit;
//    private Role r = Role.ADMIN;
//
//	@RequestMapping("touploadTeacher")
//	public String touploadTeacher( Model model){
//		  	model.addAttribute("menuSelected1", ConfigDo.ADMIN_TEACHER);
//	        model.addAttribute("menuSelected2", ConfigDo.ADMIN_UPLOAD);
//		return r.getName()+"/uploadTeacher";
//	}
//	@RequestMapping("touploadStudent")
//	public String touploadStudent( Model model){
//		  	model.addAttribute("menuSelected1", ConfigDo.ADMIN_STUDENT);
//	        model.addAttribute("menuSelected2", ConfigDo.ADMIN_UPLOAD_STUDENT);
//		return r.getName()+"/uploadStudent";
//	}
//
//	@RequestMapping("uploadTeacher")
//	public String upload(@RequestParam(value="file",required=false) MultipartFile file,HttpServletRequest request, ModelMap model){
////		if (request.getSession().getAttribute("ADM") != null) {
//			System.out.println("开始");
//			String path = request.getSession().getServletContext()
//					.getRealPath("upload");
//			String fileName = file.getOriginalFilename();
//			// String fileName = new Date().getTime()+".jpg";
//			System.out.println(path);
//			File targetFile = new File(path, fileName);
//			if (!targetFile.exists()) {
//				targetFile.mkdirs();
//			}
//
//			// 保存
//			try {
//				file.transferTo(targetFile);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			model.addAttribute("fileUrl", request.getContextPath() + "/upload/"
//					+ fileName);
//
//			readTeacherFile((Teacher) request.getSession().getAttribute("teacher"),
//					targetFile);
//			return "redirect:/admin/touploadTeacher.do";
////		} else {
////			return "redirect:/index.do";
////		}
////
//	}
//	@RequestMapping("uploadStudent")
//	public String  uploadStudent(@RequestParam(value="file",required=false) MultipartFile file,HttpServletRequest request, ModelMap model){
//		System.out.println("开始----学生上传信息");
//		String path =request.getSession().getServletContext().getRealPath("upload");
//		String fileName = file.getOriginalFilename();
//		// String fileName = new Date().getTime()+".jpg";
//		System.out.println(path);
//		File targetFile = new File(path, fileName);
//		if (!targetFile.exists()) {
//			targetFile.mkdirs();
//		}
//
//		// 保存
//		try {
//			file.transferTo(targetFile);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		model.addAttribute("fileUrl", request.getContextPath() + "/upload/"
//				+ fileName);
//
//		readStudentFile((Student) request.getSession().getAttribute("student"),
//				targetFile);
//
//
//		return "redirect:/admin/touploadStudent.do";
//	}
//
//
//		public void readTeacherFile(Teacher e, File file) {
//
//			serviceFit.getUploadService().uploadTeacherFile(e, file);
//		}
//		public void readStudentFile(Student e,File file){
//			serviceFit.getUploadService().uploadStudentFile(e, file);
//		}
//
//}
