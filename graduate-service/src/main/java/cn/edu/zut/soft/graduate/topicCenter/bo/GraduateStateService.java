package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.graduate.core.vo.GraduateStateVO;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
public interface GraduateStateService {

    GraduateStateVO findByUser(Integer userId);

    Boolean detectionMidRan();
}
