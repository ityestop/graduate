package cn.edu.zut.soft.graduate.userCenter.handler;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/20
 */
public interface QueryHandler {


    void createQuery(IdentityQuery identityQuery);

//    List<InfoStrategy> createInfoStrategyList();
}
