package cn.edu.zut.soft.graduate.gradesCenter.bo.Impl;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.core.constant.GradeTimeType;
import cn.edu.zut.soft.graduate.core.constant.GradeType;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicGradeStrategy;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.GradeStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Grade;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.gradesCenter.bo.GradeBO;
import cn.edu.zut.soft.graduate.gradesCenter.dao.GradesDAO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.dao.InfoDAO;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import test.BaseTest;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>文件名称：GradeBOImplTest.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/3 下午3:09</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class GradeBOImplTest extends BaseTest {

    private final static Logger logger = LoggerFactory.getLogger(GradeBOImplTest.class);
    @Resource
    private GradeBO gradeBO;

    @Resource
    private GradesDAO gradesDAO;

    @Resource
    private InfoDAO infoDAO;

    @Resource
    private InfoBO infoBO;

    @Test
    public void testSave() {
        Info grade = gradeBO.save(new BasicGradeStrategy(GradeType.Open, IKEY.OPEN_GRADE, "YES"), 95, "杨晓天", 1);
        System.out.println(grade.getId());
    }

    @Test
    public void testRest() {
        Grade grade = gradeBO.restGradeRecorde(new BasicInfoStrategy(GradeType.Open, IKEY.OPEN_GRADE, "YES"), 96, "yxt");
        System.out.println(grade.getId());
    }

    @Test
    public void testInterimGrade() {
        CommonResult<Grade> result = gradeBO.saveInterimGrade(new BasicGradeStrategy(GradeType.Interim, IKEY.INTERIM_GRADE, "80"), 96, "yxt");
        System.out.println(result);
    }

    @Test
    public void testGreadeDao() {
        Grade grade = new Grade();
        grade.setType("Open");
        grade.setKey(IKEY.OPEN_GRADE);
        grade.setValue("YES");
        grade.setIdentityId(305);
        grade.setIdentityName("范佳楠");
        grade.setIdentityRole(Role.stu);
        grade.setDel(0);
        gradeBO.save(grade, "yangxiaotian");
    }

    @Test
    public void testSaveSystemGrade() {
        Integer id = 96;
        String op = "yxt";
        CommonResult<Boolean> result = gradeBO.saveSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "YES"), id, GradeTimeType.SYSTEM_GRADE_ONE, op);
        System.out.println(result.isSuccess());
        System.out.println(result.getErrorMsg());
    }

    @Test
    public void testRestSystemGrade() {
        Integer id = 96;
        String op = "yxt";
        CommonResult<Boolean> result = gradeBO.restSystemGrade(new BasicGradeStrategy(GradeType.SystemAcceptance, IKEY.SYSTEM_GRADE, "YES"), id, GradeTimeType.SYSTEM_GRADE_TWO, op);
        System.out.println(result.isSuccess());
        System.out.println(result.getErrorMsg());
//        System.out.println(GradeTimeType.GRADE_STOP);
    }

    @Test
    public void testCheckOpenGrade() {
        Example openGradeExample = new Example();
        openGradeExample.createCriteria().andEqualTo("`key`", "openGrade").andEqualTo("del", "0");
        List<Grade> openList = gradesDAO.selectByExample(openGradeExample);
        if (!CollectionUtils.isEmpty(openList)) {
            logger.error("begin-------------");
            for (Grade grade : openList) {
                Example infoExample = new Example();
                infoExample.createCriteria().andEqualTo("`key`", "openGrade").andEqualTo("del", "0").andEqualTo("identity_id", grade.getIdentityId());
                // 根据 grade 中 记录更新 info 表中成绩
                List<Info> openInfoList = infoDAO.selectByExample(infoExample);
                if (!CollectionUtils.isEmpty(openInfoList)) {
                    //若开题成绩大于1条 删除重新插入一条
                    if (openInfoList.size() > 1) {
                        for (Info info : openInfoList) {
                            info.setDel(-1);
                            infoBO.update(info, "yxt");
                            logger.error(" openGrade > 1 update info del -1");
                        }
                        Info newOpenInfo = new Info();
                        newOpenInfo.setType("GRADE");
                        newOpenInfo.setKey(IKEY.OPEN_GRADE);
                        newOpenInfo.setValue(grade.getValue());
                        newOpenInfo.setIdentityId(grade.getIdentityId());
                        newOpenInfo.setIdentityName(grade.getIdentityName());
                        newOpenInfo.setIdentityRole(Role.stu);
                        infoBO.save(newOpenInfo, "yxt");
                        logger.error("openGrade > 1  save new Info recorde" + JSON.toJSONString(newOpenInfo));
                    } else {
                        //判断成绩是否一致  不一致强制更新
                        Info openGrade = openInfoList.get(0);
                        if (!openGrade.getValue().equals(grade.getValue())) {
                            openGrade.setValue(grade.getValue());
                            infoBO.update(openGrade, "yxt");
                            logger.error(" grade - openGrade  and  info - openGrade is not same , info :" + JSONObject.toJSONString(openGrade) + ", grade : " + JSONObject.toJSONString(grade));
                        }
                    }
                } else {
                    //如果没有成绩 强制插入
                    Info newOpenInfo = new Info();
                    newOpenInfo.setType("GRADE");
                    newOpenInfo.setKey(IKEY.OPEN_GRADE);
                    newOpenInfo.setValue(grade.getValue());
                    newOpenInfo.setIdentityId(grade.getIdentityId());
                    newOpenInfo.setIdentityName(grade.getIdentityName());
                    newOpenInfo.setIdentityRole(Role.stu);
                    infoBO.save(newOpenInfo, "yxt");
                    logger.error(" info no have opneGrade , new save openGrade , info:" + JSON.toJSONString(newOpenInfo));

                }
            }
        }
    }


    @Test
    public void testString() {
        String s = "RB700023";
        String s1 = s.substring(6);
        System.out.println(s1);
    }

    @Test
    public void testSaveReplyGrade() {
        Integer id = 96;
        String op = "yxt";
        CommonResult<Boolean> result = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.GUIDE_GRADE, "80"), id, op);
        System.out.println(result.isSuccess());
        System.out.println(result.getErrorMsg());
        CommonResult<Boolean> reportresult = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPORT_GRADE, "80"), id, op);
        System.out.println(reportresult.isSuccess());
        System.out.println(reportresult.getErrorMsg());
        CommonResult<Boolean> replyresult = gradeBO.saveReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, "70"), id, op);
        System.out.println(replyresult.isSuccess());
        System.out.println(replyresult.getErrorMsg());
    }

    @Test
    public void testCheckFinalGrade() {
        Integer id = 96;
        String op = "yxt";
        CommonResult<Boolean> result = gradeBO.checkFinalReplyGrade(id, op);
        System.out.println(result.isSuccess());
        System.out.println(result.getErrorMsg());
    }

    @Test
    public void checkFinalGradeRest() {
        Integer id = 96;
        CommonResult<Boolean> resultOne = gradeBO.restReplyGrade(new BasicGradeStrategy(GradeType.Final, IKEY.REPLY_GRADE, "99"), id, "yxt");
        System.out.println(resultOne.isSuccess());
        CommonResult<Boolean> resultTwo = gradeBO.checkFinalReplyGrade(id, "yxt");
        System.out.println(resultTwo.isSuccess());

    }

    @Test
    public void checkFinalGrade() {
        Example example = new Example();
        example.createCriteria().andEqualTo("`key`", IKEY.FINAL_REPLY_GRADE).andEqualTo("del", "0");
        List<Grade> finalGradeList = gradesDAO.selectByExample(example);
        if (!CollectionUtils.isEmpty(finalGradeList)) {
            int successCount = 0;
            for (int i = 0; i < finalGradeList.size(); i++) {
                CommonResult<Boolean> result = gradeBO.checkFinalReplyGrade(finalGradeList.get(i).getIdentityId(), "yxt");
                System.out.println( i + finalGradeList.get(i).getIdentityName() + ", result: " + result.isSuccess() + ", " + result.getErrorMsg());
                if (result.isSuccess()){
                    successCount++ ;
                }
            }
            System.out.println("成绩变动总人数:"+successCount);
        }
    }

    @Test
    public void testMath() {
        Integer guide = 79;
        Integer report = 85;
        Integer reply = 83;

//        double finalGradeD = (guide * 0.4) + (report * 0.2) + (reply * 0.4);
        double finalGradeD2 = 80.5;
        BigDecimal s = new BigDecimal(finalGradeD2).setScale(0, BigDecimal.ROUND_HALF_UP);
        System.out.println(s);

    }
}
