package cn.edu.zut.soft.graduate.topicCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditIssueLinkDAO<T extends AuditIssueLink> extends BasicDAO<Integer,T> {
}