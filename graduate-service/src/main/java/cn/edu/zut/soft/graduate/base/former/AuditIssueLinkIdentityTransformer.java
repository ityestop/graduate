package cn.edu.zut.soft.graduate.base.former;

import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import org.apache.commons.collections.Transformer;
import org.springframework.stereotype.Component;

/**
 * 根据关联表,查出专家列列表
 * @author cc
 * @Date 2016/12/21
 * @value 1.0
 */
@Component
public class AuditIssueLinkIdentityTransformer implements Transformer {
    @Override
    public Object transform(Object o) {
        return ((AuditIssueLink)o).getIdentityId();
    }
}