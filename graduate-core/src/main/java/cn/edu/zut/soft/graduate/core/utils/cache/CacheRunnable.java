package cn.edu.zut.soft.graduate.core.utils.cache;

import java.util.Map;

/**
 * @author chuchuang
 * @date 16/11/30
 */
class CacheRunnable implements Runnable {

    public final Map<Object,CacheMap.CacheEntry> map;

    private long cacheTimeout;

    CacheRunnable(Map map, long cacheTimeout) {
        this.map = map;
        this.cacheTimeout = cacheTimeout;
    }

    @Override
    public void run() {
        while (true) {
            try {
                long now = System.currentTimeMillis();
                Object[] keys = map.keySet().toArray();
                for (Object key : keys) {
                    CacheMap.CacheEntry entry = map.get(key);
                    if (now - entry.time >= cacheTimeout) {
                        synchronized (map) {
                            map.remove(key);
                        }
                    }
                }
                Thread.sleep(cacheTimeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
