package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Remark extends BaseModelImpl {

    private String owner;//主管
    private String type;//评语类型
    private String author;//作者
    private String content;//评价内容
    private String parentId;//自连接

}
