package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;
import lombok.ToString;

/**
 * Created by chuchuang on 16/10/23.
 */
@Data
@ToString(callSuper = true)
public class Classes {

    private Major major;
    private Team team;

    public Classes createMajor(String id, String title) {
        if (major == null) {
            this.major = new Major();
        }
        this.major.id = id;
        this.major.title = title;
        return this;
    }

    public Classes createTeam(String id, String title) {
        if (team == null) {
            this.team = new Team();
        }
        this.team.id = id;
        this.team.title = title;

        return this;
    }
    @Data
    public class Major {
        private String id;
        private String title;
    }
    @Data
    public class Team {
        private String id;
        private String title;
    }
}
