package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static cn.edu.zut.soft.graduate.userCenter.bo.StatisticsService.loginMap;

/**
 * @author chuchuang
 * @date 16/11/27
 */
@Controller
@RequestMapping("statistics")
public class StatisticsController {


    @RequestMapping("onlineNumber")
    @ResponseBody
    public CommonResult onlineNumber(){
        return CommonResult.successReturn(loginMap.size());
    }



    @RequestMapping("onlineUser")
    @ResponseBody
    public CommonResult onlineUser(){
        List<LoginVO> loginVoList = new ArrayList<>(loginMap.size());
        for (Map.Entry<Integer, LoginVO> ce:loginMap.entrySet()){
            loginVoList.add(ce.getValue());
        }
        return CommonResult.successReturn(loginVoList);
    }

    @RequestMapping("train")
    @ResponseBody
    public CommonResult train(HttpSession session){
        LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
        loginMap.put(loginVO.getId(),loginVO);
        return CommonResult.successReturn("ok");
    }

}
