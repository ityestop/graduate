package cn.edu.zut.soft.graduate.reportCenter.query.Impl;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.annotation.QueryParameter;
import cn.edu.zut.soft.graduate.reportCenter.common.TimePeriod;
import cn.edu.zut.soft.graduate.reportCenter.query.ReportDataQuery;
import cn.edu.zut.soft.graduate.reportCenter.query.SearchReportQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SimpleReportDataQuery extends Query implements ReportDataQuery, SearchReportQuery {


    @QueryParameter(set = true,equal = QueryParameter.Type.Equal, rowName = "id")
    private List<Integer> ids;

    @QueryParameter(set = true,equal = QueryParameter.Type.Equal,rowName = "`report_rule_id`")
    private List<Integer> ruleIds;

    @QueryParameter(set = true,equal = QueryParameter.Type.unEqual,rowName = "`report_rule_id`")
    private List<Integer> unRuleIds;

    @QueryParameter(set = true,equal = QueryParameter.Type.Equal,rowName = "identity_id")
    private List<Integer> identityIds;


    @QueryParameter(set = true,equal = QueryParameter.Type.Equal,rowName = "num")
    private List<Integer> nums;

    @QueryParameter(equal = QueryParameter.Type.Equal, rowName = "del", defaultValue = QueryParameter.DefaultValueType.ZERO)
    private Integer del;

    @QueryParameter(equal = QueryParameter.Type.Equal, rowName = "supplement", defaultValue = QueryParameter.DefaultValueType.NULL)
    private Integer supplement;

    private TimePeriod timePeriod;

    private Alternative reportStatus;

    private Integer teaId;

    private Integer teaGroupId;

}
