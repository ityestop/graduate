<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>题目详情</title>
    <script src="/assets/g/js/source.js"></script>
    <script src="/assets/g/js/issueDetails.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/home.do"> 首页 </a></li>
            <li class="active">题目详情</li>
        </ol>
        <div class="page-header">
            <h4>题目详情</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->


<div class="row">
    <div class="panel-body">
        <table class="table" id="sample-table-1">
            <thead>
            <tr>
                <td width="10%" class="center"><small>课题编号:</small></td>
                <td><small><span id="issueId">${param.id}</span></small></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="center"><small>题目标题:</small></td>
                <td><small><span id="issueTitle"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>校内教师:</small></td>
                <td><small><span id="issueTeacher"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>课题来源:</small></td>
                <td><small><span id="issueSource"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>课题性质:</small></td>
                <td><small><span id="issueKind"></span></small></td>
            </tr>

            <tr>
                <td class="center"><small>课题类型:</small></td>
                <td><small><span id="issueType"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>课题人数:</small></td>
                <td><small><span id="issueTaskSize"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>技术领域:</small></td>
                <td><small><span id="issueTechnology"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>项目背景:</small></td>
                <td>
                    <small><pre id="issueBackground"></pre></small>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <small>项目任务:</small>
                </td>
                <td>
                    <div class="row" id="issueTaskList">
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div>
        <label class="col-sm-8 control-label"></label>
        <div id="chooseBtn" class="col-sm-2" >
            <button type="button" class="btn btn-blue btn-block"
                    id="stu_Choose_btn">
                <span><i class=" clip-checkmark-2  "></i> 申请</span>
            </button>
        </div>

    <div class="col-sm-2" style="float: right">
        <button type="button" class="btn btn-blue btn-block"
                onclick="window.location.href='javascript:history.go(-1)'">
            <span><i class=" clip-arrow-right-2  "></i> 返回</span>
        </button>
    </div>
    </div>
</div>
</body>
</html>
