package cn.edu.zut.soft.graduate.reportCenter.pojo;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportDataDTO {

    /**
     * 编号
     */
    Integer getId();

    /**
     * 获取报告内容
     */
    String getContext();

    /**
     * 获取规则id
     * @return
     */
    Integer getReportRuleId();

    /**
     * 获取报告内容的hash值
     */
    Integer getHashContext();

}
