package cn.edu.zut.soft.graduate.fileUploadCenter.bo;

import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by chuchuang on 16/11/14.
 */
public interface FileUploadBO {
    String upload(InputStream inputStream, String fileName, long fileSize, LoginVO loginVO) throws Exception;

    /**
     * 下载教师小组下学生的基本数据
     */

    void downloadTeaGroupStu(OutputStream outputStream, IdentityQuery identityQuery);

    void downloadGradeStu(OutputStream outputStream, IdentityQuery identityQuery, String key);

    void downloadGuideStudent(OutputStream outputStream, Integer teaId);

    void downloadFinalGrade(OutputStream outputStream);
}
