//package cs;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import org.apache.ibatis.session.RowBounds;
//import org.junit.AfterClass;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//import cn.edu.zut.graduate.entity.ChildTopic;
//import cn.edu.zut.graduate.entity.StuGroup;
//import cn.edu.zut.graduate.entity.Student;
//import cn.edu.zut.graduate.entity.Teacher;
//import cn.edu.zut.graduate.entity.Technophobe;
//import cn.edu.zut.graduate.entity.Topic;
//import cn.edu.zut.graduate.entity.tools.StuGroupTopicLink;
//import cn.edu.zut.graduate.supervisor.DaoFit;
//
//public class TestStuTopic {
//
//	private static DaoFit dao;
//
//	private static Map<String, Object> map = new HashMap<>();
//
//	static Map<String, Student> students = new HashMap<>();
//	static Map<String, Teacher> teachers = new HashMap<>();
//	static Map<String, Topic> topics = new HashMap<>();
//	static Map<String, StuGroup> stuGroups = new HashMap<>();
//	static Map<String, ChildTopic> childTopics = new HashMap<>();
//	static Map<String, StuGroupTopicLink> stuGroupTopicLinks = new HashMap<>();
//	static Map<String, Technophobe> technophobes = new HashMap<>();// 技术领域
//
//	static List<Student> studentsList;
//	static List<Teacher> teachersList;
//	static List<Topic> topicsList;
//	static List<StuGroup> stuGroupsList;
//	static List<ChildTopic> childTopicsList;
//	static List<StuGroupTopicLink> stuGroupTopicLinksList;
//	static List<Technophobe> technophobesList;// 技术领域
//
//	// 删除数据列表
//	static List<Object> romveAllList = new LinkedList<Object>();
//	static List<Object> romveList = new LinkedList<>();
//
//	private static ExecutorService pool = Executors.newFixedThreadPool(10);// 创建一个固定大小为5的线程池
//
//	File file = null;
//	BufferedWriter bf = null;
//
//	@BeforeClass
//	public static void setUpBeforeClass() throws Exception {
//		ApplicationContext context = new ClassPathXmlApplicationContext(
//				new String[] { "classpath:conf/Spring-mvc.xml", "classpath:conf/Spring-sql.xml" });
//		dao = (DaoFit) context.getBean("daoFit");
//	}
//
//	@AfterClass
//	public static void tearDownAfterClass() throws Exception {
//
//	}
//
//	@Test
//	public void test() throws IOException {
//		file = new File("AllStu.txt");
//		if (!file.exists()) {
//			try {
//				file.createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		try {
//			bf = new BufferedWriter(new PrintWriter(file));
//			bf.write("");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//			bf.close();
//		}
//		findAll();
//		guolv();
//		AllStudent();
//		bf.close();
//		file = new File("romveAllList.txt");
//		try {
//			bf = new BufferedWriter(new PrintWriter(file));
//			bf.write("");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//			bf.close();
//		}
//		for (int i = 0, l = romveAllList.size(); i < l; i++) {
//			println(romveAllList.get(i).toString());
//		}
//		bf.close();
//	}
//
//	void println(String str) throws IOException {
//		System.out.println(str);
//		bf.write(str);
//		bf.newLine();
//	}
//
//	private void AllStudent() throws IOException {
//		StuGroup sg;
//		StuGroupTopicLink sGroupTopicLink = null;
//		StuGroupTopicLink sGroupTopicL = null;
//		Topic topic = null;
//		Teacher teacher = null;
//		List<ChildTopic> cTopics = null;
//		for (Student s : studentsList) {
//			cTopics = new LinkedList<>();
//			println("开始检测学生" + s.getStuName() + "的信息");
//			// println(s.toString());
//			// String sn = s.getStuId();
//			if (s.getSgId() != null && !"".equals(s.getSgId().trim())) {
//				sg = stuGroups.get(s.getSgId());
//				if (sg != null) {
//					println("存在小组");
//				} else {
//					println("不存在小组");
//				}
//
//				// println(sg.toString());
//				boolean error = false;
//				for (int i = 0, l = stuGroupTopicLinksList.size(); i < l; i++) {
//					sGroupTopicLink = stuGroupTopicLinksList.get(i);
//					// println(sg.getSgId().equals(sGroupTopicLink.getChooStuGroupId())
//					// + "****" + sGroupTopicLink.getChooStatus());
//					if (sg.getSgId().equals(sGroupTopicLink.getChooStuGroupId())
//							&& sGroupTopicLink.getChooStatus() == 1) {
//						println("存在小组选报成功记录");
//						if (error) {
//							romveAllList.add("学生小组" + sg.getSgId() + "检测出现问题，小组出现多个选报成功" + sGroupTopicLink.getChooId());
//							println("学生小组" + sg.getSgId() + "检测出现问题，小组出现多个选报成功" + sGroupTopicLink.getChooId());
//						}
//
//						sGroupTopicL = sGroupTopicLink;
//						error = true;
//					}
//				}
//				println("检测课题");
//				if (error && sGroupTopicL != null) {
//					// println(sGroupTopicL.getChooTopicId());
//					topic = topics.get(sGroupTopicL.getChooTopicId());
//					// println(topic.getTopTitle());
//					if (topic != null) {
//						println("拥有课题" + topic.getTopTitle());
//					}
//
//				} else {
//					println("不存在课题");
//				}
//				teacher = teachers.get(topic.getTopTeacherId());
//				println("检测指导教师");
//				if (teacher != null) {
//					println("指导教师为" + teacher.getTeaName());
//				} else {
//					println("注意：当前学生不存在指导教师，请注意数据");
//					romveAllList.add("注意：学生" + s.getStuName() + "不存在指导教师，请注意数据");
//				}
//				ChildTopic childTopic = null;
//				if (topic != null) {
//					error = false;
//					println("开始检测小标题");
//					for (int i = 0, l = childTopicsList.size(); i < l; i++) {
//						childTopic = childTopicsList.get(i);
//						if (topic.getTopId().equals(childTopic.getParentId())) {
//							// println(topic.getTopId() + "****" +
//							// childTopic.getParentId());
//							// println("小标题 id为:" +s.getChildId()+ "当前小标题id为:"+
//							// childTopic.getId());
//							if (s.getChildId() != null && s.getChildId().equals(childTopic.getId())) {
//								println("存在任务为" + childTopic.getId() + "***" + childTopic.getName());
//								error = true;
//							}
//							cTopics.add(childTopic);
//						}
//					}
//					println("小标题数目:" + cTopics.size());
//					if (!error && !cTopics.isEmpty()) {
//						println("当前课题存在小标题数" + cTopics.size() + ",学生并没有分配任务,指导教师为:" + teacher.getTeaName());
//						romveAllList.add("注意：学生" + s.getStuName() + "存在课题，但没有被分配任务,指导教师为:" + teacher.getTeaName());
//					}
//
//				}
//			} else if (s.getStuTeacherId() != null && !"".equals(s.getStuTeacherId())) {
//				teacher = teachers.get(s.getStuTeacherId());
//				println("当前学生没有申报课题,被直接分配个指导教师" + teacher.getTeaName());
//				romveAllList.add("注意：学生" + s.getStuName() + "没有申报课题,被直接分配个指导教师" + teacher.getTeaName());
//			} else {
//				println("当前学生不存在指导教师，属于未分配学生");
//				romveAllList.add("注意：学生" + s.getStuName() + "不存在指导教师，属于未分配学生");
//			}
//
//		}
//		sg = null;
//		sGroupTopicLink = null;
//		topic = null;
//		teacher = null;
//		cTopics = null;
//	}
//
//	private void findAll() {
//		RowBounds rowBounds = new RowBounds();
//		studentsList = dao.getStudentDao().findAll(rowBounds);
//		for (Student student : studentsList) {
//			students.put(student.getStuId(), student);
//		}
//		teachersList = dao.getTeacherDao().findAll(rowBounds);
//		for (Teacher teacher : teachersList) {
//			teachers.put(teacher.getTeaId(), teacher);
//		}
//		topicsList = dao.getTopicDao().findAll(rowBounds);
//		for (Topic t : topicsList) {
//			topics.put(t.getTopId(), t);
//		}
//		stuGroupsList = dao.getStuGroupDao().findAll(rowBounds);
//		for (StuGroup sg : stuGroupsList) {
//			stuGroups.put(sg.getSgId(), sg);
//		}
//		childTopicsList = dao.getChildTopicDao().findAll(rowBounds);
//		for (ChildTopic childTopic : childTopicsList) {
//			childTopics.put(childTopic.getId(), childTopic);
//		}
//		stuGroupTopicLinksList = dao.getStuGroupTopicLinkDao().findAll(rowBounds);
//		for (StuGroupTopicLink sgl : stuGroupTopicLinksList) {
//			stuGroupTopicLinks.put(sgl.getChooId(), sgl);
//		}
//		technophobesList = dao.getTechnosphereDao().findAll(rowBounds);
//		for (Technophobe t : technophobesList) {
//			technophobes.put(t.getTecId(), t);
//		}
//	}
//
//	// 过滤
//	private void guolv() {
//		System.out.println("开始过滤");
//		System.out.println("开始过滤无效课题");
//		System.out.println("过滤无效课题 TopStatus 3 未通过，4 未提交");
//		Topic t;
//		for (int i = topicsList.size() - 1; i > 0; i--) {
//			// topStatus 3 为未通过 4，为未提交
//			t = topicsList.get(i);
//			if (t.getTopStatus() > 2) {
//				// 无效题目有
//				// System.out.println(t.toString());
//				romveList.add(t.getTopId() + "**" + t.getTopTitle());
//				topicsList.remove(t);
//				topics.remove(t.getTopId());
//			}
//		}
//		System.out.println("结束过滤无效课题");
//		romveAllList.addAll(romveList);
//		System.out.println("删除的无效课题数目：" + romveList.size());
//		romveList.clear();
//		System.out.println(topicsList.size());
//	}
//
//}
