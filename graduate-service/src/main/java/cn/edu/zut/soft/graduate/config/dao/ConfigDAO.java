package cn.edu.zut.soft.graduate.config.dao;

import cn.edu.zut.soft.graduate.core.model.Impl.Config;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author cc
 * @Date 2016/11/24
 * @value 1.0
 */
@Component
public interface ConfigDAO {

    String findByKey(String key);

    Config getConfigBykey(String key);

    void save(Config config);

    void update(Config config);

    List<Config> findAll();

}
