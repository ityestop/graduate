package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by chuchuang on 16/10/23.
 */
public enum InfoType implements InfoTypeAble {
    Basic,Audit,Group,TOPIC,Tea,Positive,GRADE;
}
