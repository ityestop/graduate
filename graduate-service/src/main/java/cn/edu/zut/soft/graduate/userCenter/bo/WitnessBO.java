package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.vo.Witness;
import cn.edu.zut.soft.graduate.core.vo.WitnessData;

import java.util.List;

/**
 * Created by chuchuang on 16/10/23.
 */
public interface WitnessBO {
    //专家数据处理的业务逻辑

    CommonResult<WitnessData> getWitnessData(Integer id);

    CommonResult<List<WitnessData>> getAllWitnessData();

    List<Witness> saveWitess(String[] userId,String operator);

}
