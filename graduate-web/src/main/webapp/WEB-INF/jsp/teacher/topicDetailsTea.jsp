<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>题目详情</title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">题目详情</li>
        </ol>
        <div class="page-header">
            <h3>题目详情   <small><font color="red">若自拟题目：题目标题、需求等格式不对 教师可以进行不通过操作打回由学生修改、教师在同意操作前后都可以对课题信息进行微调</font></small></h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">

        <div class="panel-body">
            <div class="form-group">
                <table class="table">
                    <tbody>
                    <tr>
                        <td width="10%">
                            <small>题目标题:</small>
                        </td>
                        <td>
                            <small>${topic.topTitle}</small>
                        </td>
                    </tr>
                    <c:choose>
                        <c:when test="${topic.topStatus eq 3 }">
                            <tr class="danger">
                                <td>
                                    <small>管理员批语</small>
                                </td>
                                <td>
                                    <small><font color="red">${topic.topRemark}</font>
                                    </small>
                                </td>
                            </tr>
                        </c:when>
                    </c:choose>
                    <tr>
                        <td>
                            <small>课题来源:</small>
                        </td>
                        <td><c:if test="${topic.topTopicType eq false }">
                            <small>社会服务</small>
                        </c:if> <c:if test="${topic.topTopicType eq true }">
                            <small>科研项目</small>
                        </c:if></td>
                    </tr>
                    <tr>
                        <td>
                            <small>课题类型:</small>
                        </td>
                        <td><c:if test="${topic.topType eq '设计' }">
                            <small>设计</small>
                        </c:if> <c:if test="${topic.topType eq '研究' }">
                            <small>研究</small>
                        </c:if> <c:if test="${topic.topType != '研究' && topic.topType != '设计'}">
                            <small>${topic.topType}</small>
                        </c:if></td>
                    </tr>
					<tr>
								<td><small>详细来源:</small></td>
								<td><c:if test="${topic.topSource eq 3 }">
										<small><font color="red">自拟题目</font> </small>
									</c:if> <c:if test="${topic.topSource lt  3}">
										<small><font color="red">校内题目</font></small>
									</c:if> </td>
							</tr>
                    <tr>
                        <td>
                            <small>技术领域:</small>
                        </td>
                        <td><c:forEach items="${topic.topTechnophobeList}" var="t">
                            <small> ${t.techName}、</small>
                        </c:forEach></td>
                    </tr>
                    <tr>
                        <td>
                            <small>项目背景:</small>
                        </td>
                        <td>
                            <small>
                                <pre>${topic.topContent}</pre>
                            </small>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><c:forEach items="${topic.childTopicList}"
                                                   var="c">
                            <small><strong>${c.name}</strong>
                            </small>
                            <small>
                                <pre>${c.content}</pre>
                            </small>
                        </c:forEach></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <c:if test="${SGTLink != null && SGTLink.size() > 0}">
                <c:forEach items="${SGTLink}" var="li">
                <table width="100%" class="table   table-hover">
                    <thead>
                    <tr>
                        <th>
                            <small>小组编号</small>
                        </th>
                        <th>
                            <small>小组成员</small>
                        </th>
                        <th>
                            <small>操作</small>
                        </th>
                    </tr>
                    </thead>
                   
                        <tbody>
                        <tr>
                            <td>
                                <small>${li.choStuGroup.head.stuName}的小组</small>
                            </td>
                            <td>
                                <ul>
                                    <c:forEach items="${li.choStuGroup.studentList}" var="lis">
                                        <li>
                                            <small>${lis.stuName}(<font color="red">电话:${lis.stuTell}</font>) ----
                                                    ${lis.stuStudentId} ---- ${lis.stuClass}</small>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                            <td>
                                <small>
                                    <c:choose>
                                           <c:when test="${li.chooStatus == 1}">
                                            同意选报  
                                        </c:when>
                                        <c:when test="${topic.topSource eq 3 and li.chooStatus == 2}">
                                           退回编辑
                                        </c:when>
                                         <c:when test="${topic.topSource ne 3 and li.chooStatus == 2}">
                                            拒绝选报
                                        </c:when>
                                        <c:when test="${topic.topStatus eq 2 and topic.topSource != 3 }">
                                            <a href="./${sessionScope.pathCode}/upAdSgTL.do?res=true&sgTl=${li.chooId}">同意</a>
                                            <a href="./${sessionScope.pathCode}/upAdSgTL.do?res=false&sgTl=${li.chooId}">拒绝</a>
                                        </c:when>
                                         <c:when test="${topic.topStatus eq 1 and topic.topSource eq 3 }">
                                            <a href="./${sessionScope.pathCode}/upAdSgTL.do?res=true&sgTl=${li.chooId}">同意</a>
                                            <a href="./${sessionScope.pathCode}/upAdSgTL.do?res=false&sgTl=${li.chooId}">拒绝</a>
                                             <a href="./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}">编辑</a>
                                        </c:when>
                                    </c:choose>
                                </small>
                            </td>
                        </tr>
                        </tbody>
                </table>
                <c:choose>
                 <c:when test="${topic.topStatus eq 1 and topic.topSource eq 3 and li.chooStatus == 1 }">
                        <label class="col-sm-1 control-label"></label>
						<div class="col-sm-2">
						<c:if test="${sessionScope.functionMap['teaEdit'].status eq true}">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}'">
                                <i class=" clip-pencil-3 "></i> 编辑
                            </button>
                        </c:if>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/passOutsideTopic.do?topId=${topic.topId}&sgId=${li.choStuGroup.sgId}'">
                                <i class=" clip-pencil-3 "></i> 审核通过
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block" onclick="window.location.href='./${sessionScope.pathCode}/BackTopicWrite.do?topId=${topic.topId}'">
                                <i class="clip-checkmark-2"></i> 退回修改
                            </button>
                        </div>
                         <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/DissolutionGroup.do?sgId=${li.choStuGroup.sgId}&topId=${topic.topId}'">
                                <i class="clip-checkmark-2"></i> 解散小组
                            </button>
                        </div>
                        <label class="col-sm-1 control-label"></label>
                    </c:when>
                </c:choose>
                 </c:forEach>
                 
            </c:if>
            <div class="form-group">
                <c:choose>
                    <c:when test="${topic.topStatus eq 2 or topic.topStatus eq 1 or topic.topStatus eq 0 }">
                        <label class="col-sm-10 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                    </c:when>
                    
                    
                        <c:when test="${topic.topStatus eq 3 or topic.topStatus eq 4  and topic.topSource eq 3}">
                        <label class="col-sm-10 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/teaTopicList.do'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        <label class="col-sm-3 control-label"></label>
                    </c:when>
                    
                    <c:when test="${topic.topStatus eq 3 or topic.topStatus eq 4}">
                        <label class="col-sm-3 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/toToPicUpdate.do?id=${topic.topId}'">
                                <i class=" clip-pencil-3 "></i> 编辑
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/submitTopic.do?id=${topic.topId}&statues='">
                                <i class="clip-checkmark-2"></i> 提交
                            </button>
                        </div>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/teaTopicList.do'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        <label class="col-sm-3 control-label"></label>
                    </c:when>
                    
                </c:choose>
            </div>

        </div>

    </div>
</div>
</body>
</html>
