package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.bo.BasicBOImpl;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import cn.edu.zut.soft.graduate.core.query.AuditIssueLinkQuery;
import cn.edu.zut.soft.graduate.topicCenter.dao.AuditIssueLinkDAO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.IssueTakeWitness;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.*;

/**
 * Created by chuchuang on 16/10/23.
 */
@Service
public class AuditIssueLinkBOImpl extends BasicBOImpl<AuditIssueLink,AuditIssueLinkQuery> implements AuditIssueLinkBO {

    @Resource
    private AuditIssueLinkDAO<AuditIssueLink> auditIssueLinkDAO;

    @Override
    protected BasicDAO<Integer, AuditIssueLink> getDAO() {
        return auditIssueLinkDAO;
    }

    @Override
    protected List<Criteria> Query(final AuditIssueLinkQuery auditIssueLinkQuery) {
        Criteria criteria = new Criteria();
        if (auditIssueLinkQuery.getIdentityId() != null){
            criteria.andEqualTo("identity_id",auditIssueLinkQuery.getIdentityId());
        }
        if (auditIssueLinkQuery.getIssueId() != null){
            criteria.andEqualTo("issue_id",auditIssueLinkQuery.getIssueId());
        }
        if (CollectionUtils.isNotEmpty(auditIssueLinkQuery.getIdentityIds())){
            criteria.andIn("identity_id",auditIssueLinkQuery.getIdentityIds());
        }
        if (CollectionUtils.isNotEmpty(auditIssueLinkQuery.getIssueIds())){
            criteria.andIn("issue_id",auditIssueLinkQuery.getIssueIds());
        }
        return Collections.singletonList(criteria);
    }


    @Override
    public List<Integer> noExpertTopic() {
        List<Integer> result =  new ArrayList<>();
        Set<Integer> issuseIdSet = new HashSet<>();
        Example example = new Example();
        example.createCriteria().andEqualTo("del","0");
        List<AuditIssueLink> issueLinkTopic = auditIssueLinkDAO.selectByExample(example);
        if (issueLinkTopic == null){
            return result;
        }
        for (AuditIssueLink a:issueLinkTopic){
            issuseIdSet.add(a.getIssueId());
        }
        Iterator<Integer> it=issuseIdSet.iterator();
        while(it.hasNext()){
            result.add(it.next());
        }
        return result;
    }

    @Override
    public List<Integer> getExpertIssue(Integer identityId) {
        if (identityId == null){
            return null;
        }
        List<Integer> result = new ArrayList<>();
        Example example = new Example();
        example.createCriteria().andEqualTo("del","0").andEqualTo("identity_id",identityId);
        List<AuditIssueLink> auditIssueLinks = auditIssueLinkDAO.selectByExample(example);
        if (auditIssueLinks == null){
            return null;
        }
        for (AuditIssueLink a:auditIssueLinks){
            result.add(a.getIssueId());
        }
        return result;
    }

    @Override
    public AuditIssueLink getByissueAndUser(Integer userId, Integer issueId) {
        if (userId == null && issueId == null){
            return  null;
        }

        Example example = new Example();
        example.createCriteria().andEqualTo("identity_id", userId).andEqualTo("issue_id",issueId).andEqualTo("del",0);
        List<AuditIssueLink> list = auditIssueLinkDAO.selectByExample(example);
        if (CollectionUtils.isEmpty(list)){
            return  null;
        }
        return list.get(0);
    }

    @Override
    public AuditIssueLink getAuditIssueLink(Integer issueId) {
        if(issueId == null) {
            return null;
        }
        Example example = new Example();
        example.createCriteria().andEqualTo("issue_id",issueId).andEqualTo("del",0);
        List<AuditIssueLink> list = auditIssueLinkDAO.selectByExample(example);
        if (CollectionUtils.isEmpty(list)){
            return  null;
        }
        return list.get(0);
    }



}
