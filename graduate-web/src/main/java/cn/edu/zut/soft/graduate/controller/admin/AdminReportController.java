package cn.edu.zut.soft.graduate.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cc
 * @Date 2017/2/19
 * @value 1.0
 */
@Controller
@RequestMapping("admin/report")
public class AdminReportController {

    @RequestMapping({"listRule","/"})
    public String listRule(){
        return "/adminE/report/ruleList";
    }

    @RequestMapping("editRule")
    public String editRule(){
        return "/adminE/report/ruleEdit";
    }

    @RequestMapping("getRule")
    public String getRule(){
        return "/adminE/report/ruleDetails";
    }

    @RequestMapping("listData")
    public String dataList(){
        return "/adminE/report/reportDataList";
    }

}
