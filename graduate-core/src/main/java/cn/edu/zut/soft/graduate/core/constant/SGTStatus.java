package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by cc on 15/10/31.
 */
public enum SGTStatus implements Status {
    ING("导师确认中",0),SUC("选报成功",1),INVALID("未通过",2);

    private String name;
    private int index;

    SGTStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public String toString() {
        return "name:" + name +" index:" + index;
    }

    public static SGTStatus getName(int index){
        for (SGTStatus s : SGTStatus.values()){
            if (s.getIndex() == index){
                return s;
            }
        }
        return null;
    }

    public static SGTStatus getIndex(String name){
        for (SGTStatus s : SGTStatus.values()){
            if (s.getName().equals(name)){
                return s;
            }
        }
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }



}
