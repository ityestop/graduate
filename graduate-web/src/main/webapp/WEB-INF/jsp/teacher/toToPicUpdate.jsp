<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">修改密码</li>
			</ol>
			<div class="page-header">
				<h3>修改密码</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
		<div class="col-md-12">
		<div class="panel panel-default">
		<div class="panel-heading">
		
                <i class="fa fa-reorder"></i> 修改密码
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a> <a class="btn btn-xs btn-link panel-config" href="#panel-config"
                            data-toggle="modal"> <i class="fa fa-wrench"></i> </a> <a
                        class="btn btn-xs btn-link panel-refresh" href="#"> <i
                        class="fa fa-refresh"></i> </a> <a
                        class="btn btn-xs btn-link panel-expand" href="#"> <i
                        class="fa fa-resize-full"></i> </a> <a
                        class="btn btn-xs btn-link panel-close" href="#"> <i
                        class="fa fa-times"></i> </a>
                </div>
            </div>
				<div class="panel-body ">
				</div>
			
			</div>
		</div>
	</div>	

</body>
</html>
