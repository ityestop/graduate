package cn.edu.zut.soft.graduate.controller.tea;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.query.DeclareQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.bo.DeclareBO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.DeclareInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>文件名称：TeacherChooseController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/11/27 下午9:47</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Controller
@RequestMapping("/tea")
public class TeaChooseController extends BaseController {
    @Resource
    private DeclareBO declareBO;

    @RequestMapping("/choose/{alternative}")
    @ResponseBody
    public CommonResult teaChooseIssue(HttpSession session, @PathVariable("alternative")Alternative alternative, String id){
        return CommonResult.successReturn(declareBO.TeaConfirm((LoginVO) session.getAttribute(IKEY.USER),Integer.parseInt(id),alternative));
    }

    @ResponseBody
    @RequestMapping("/declareInfo")
    public CommonResult declareInfo(DeclareQuery declareQuery){
//        List<DeclareInfo> result = ;
        return CommonResult.successReturn(declareBO.findDeclareInfo(declareQuery),declareQuery.getCurrentPage());
    }

}
