package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by chuchuang on 16/10/25.
 */
public interface Matching {
    boolean isMatching(String str);
}
