package cn.edu.zut.soft.graduate.controller.stu;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.core.constant.IssueKind;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.DeclareQuery;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.bo.CompanyProjectRequestBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.DeclareBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.DeclareInfo;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>文件名称：StudentIssusController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/11/27 下午4:20</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Controller
@RequestMapping("/stu")
public class StudentIssusController extends BaseController {
    @Resource
    private IdentityBO identityBO;

    @Resource
    private IssueBO issueBO;

    @Resource
    private DeclareBO declareBO;

    @Resource
    private CompanyProjectRequestBO companyProjectRequestBO;

    /**
     * 功能描述:学生端题目检索
     * @param model
     * @param issueQuery
     * @param role
     * @return
     */
    @RequestMapping("/issue/list/{role}")
    public ModelAndView issueSelect(Map<String, Object> model, IssueQuery issueQuery, @PathVariable("role") Role role) {
        //修改掉角色
        issueQuery.setIdentityRole(role);
        if (issueQuery.getIssuePhase() == null) {
            issueQuery.setIssuePhase(IssuePhase.chooseIng);
        }
        CommonResult<List<Issue>> result = issueBO.findByQuery(issueQuery);
        for (Issue issue : result.getData()) {
            issue.setTechnology(null);
        }
        model.put("list", JSON.toJSON(result.getData()));
        model.put("count", result.getTotalCount());
        model.put("pageSize", issueQuery.getLimit());
        model.put("currentPage", issueQuery.getCurrentPage());
        putViewSource(model);
        if (Role.tea == role) {
            IdentityQuery identityQuery = new IdentityQuery();
            //检索所有的教师
            identityQuery.setLimit(Integer.MAX_VALUE);
            identityQuery.setRole(role);
            model.put("teaList", JSON.toJSON(identityBO.findByQuery(identityQuery).getData()));
        }
        return new ModelAndView("stu/issueList", model);
    }

    /**
     * 功能描述:学生端 题目详情跳转
     * @return
     */
    @RequestMapping("/issueInfo")
    public ModelAndView getIssueInfo() {
        return new ModelAndView("stu/issueDetails");
    }

    /**
     * 功能描述:学生选报权限 <学生详情页>
     * @param request
     * @param issueId
     * @return
     */

    @ResponseBody
    @RequestMapping("/stuChooseRule")
    public CommonResult stuChooseRule(HttpServletRequest request, @RequestParam("issueId") Integer issueId) {
        LoginVO session = (LoginVO) request.getSession().getAttribute("user");
        if (session == null) {
            return CommonResult.errorReturn("请重新登陆!");
        }
        if (issueId == null) {
            CommonResult.errorReturn("System error");
        }
        Issue issue = new Issue();
        issue.setId(issueId);
        boolean result = declareBO.isAsk(session, issue);
        return CommonResult.successReturn(result);
    }

    /**
     * 功能描述:学生端选报题目
     * @param request
     * @param issueId
     * @return
     */
    @ResponseBody
    @RequestMapping("/stuChooseIssue")
    public CommonResult stuChooseIssue(HttpServletRequest request, @RequestParam("issueId") Integer issueId) {
        if (issueId == null) {
            return CommonResult.errorReturn("System error");
        }
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登陆!");
        }
        Issue issue = new Issue();
        issue.setId(issueId);
        Declare result = declareBO.GroupDeclare(loginVO, issue);
        return CommonResult.successReturn(result);
    }

    /**
     * 功能描述:获取学生选报详细信息
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/stuIssueStatus")
    public CommonResult stuIssueStatus(HttpServletRequest request) {
        List<DeclareInfo> result = Collections.emptyList();
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登陆!");
        }
        if (request.getSession().getAttribute(IKEY.GROUP_GROUP) == null) {
            return CommonResult.successReturn(result);
        }
        Integer groupId = Integer.parseInt(request.getSession().getAttribute(IKEY.GROUP_GROUP).toString());
        DeclareQuery declareQuery = new DeclareQuery();
        declareQuery.setGroupId(groupId);
        result = declareBO.findDeclareInfo(declareQuery);
        return CommonResult.successReturn(result);
    }

    private void putViewSource(Map<String, Object> model) {
        Map<String, String> stuStatus = new HashMap<>();
        stuStatus.put("chooseIng", "选报中");
        stuStatus.put("finish", "选报成功");
        model.put("stuStatus", JSON.toJSON(stuStatus));
        model.put("statusList", JSON.toJSON(IssuePhase.getMap()));
        model.put("typeList", JSON.toJSON(IssueSource.getMap()));
        model.put("kindList", JSON.toJSON(IssueKind.getMap()));
    }

    /**
     * 功能描述:学生创建校外题目
     * @return
     */
    @RequestMapping("/toCompanyIssue")
    public ModelAndView toCompanyIssue(){
        return new ModelAndView("/stu/newCompanyIssue");
    }

    @ResponseBody
    @RequestMapping("/createCompanyRole")
    public CommonResult createCompanyRole(HttpServletRequest request){
        LoginVO session = (LoginVO)request.getSession().getAttribute("user");
        if (session == null){
            return CommonResult.errorReturn("请重新登陆!");
        }
        if (request.getSession().getAttribute(IKEY.GROUP_GROUP) == null){
            return CommonResult.successReturn(null);
        }
        Integer groupID = Integer.parseInt(request.getSession().getAttribute(IKEY.GROUP_GROUP).toString());
        boolean result = companyProjectRequestBO.isCreateCompanyIssue(session.getId(), groupID);
        if (result){
            return CommonResult.successReturn(result);
        }
        return CommonResult.errorReturn("你没有权限申请公司项目!");
    }


}
