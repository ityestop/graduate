package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.InfoTypeAble;
import com.alibaba.fastjson.JSON;

/**
 * Created by chuchuang on 16/11/4.
 */
public enum  AdminInfoStrategy implements InfoStrategy {
    admin;

    @Override
    public InfoTypeAble getType() {
        return InfoType.Basic;
    }

    @Override
    public String getKey() {
        return "admin";
    }

    @Override
    public String getValue() {
        return "";
    }

    @Override
    public String toJson() {
        return JSON.toJSONString(this);
    }
}
