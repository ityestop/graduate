package cn.edu.zut.soft.graduate.log;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author chuchuang
 * @date 16/11/30
 */
public interface LogService {
    void behaviour(HttpServletRequest httpRequest, HttpServletResponse response, Map map);

    HttpSession getSession(HttpServletRequest httpRequest);
}
