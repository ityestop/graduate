<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/12/5
  Time: 下午11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教师任务列表</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">教师任务列表</li>
        </ol>
        <div class="page-header">
            <h3>教师任务列表
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="/admin/teaWorking" method="get">
                <div class="form-group">
                    <input class="form-control" style="width: 300px" title="教师姓名" placeholder="教师姓名" name="name"
                           value="${param.name}"/>
                </div>
                <div class="form-group" style="text-align: right;">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
            </form>
        </div>
        <div class="col-sm-12">
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>

<div class="modal fade" id="assignCompanyGruop" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">分配公司项目小组</h4>
            </div>
            <div class="modal-body">
                <table class="table table-container" id="companyGroupInfo">
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="stuMemberBytea" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">指导学生</h4>
            </div>
            <div class="modal-body" id="modelTable">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="assignStuOneNumber" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">分配学生</h4>
            </div>
            <div class="modal-body" id="table_stuMember">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript">
    function params() {
        return {
            name: $("input[name='name']").val()
        }
    }

    var columns = [
//            {
//                checkbox: true
//            },
        {
            field: 'identity.id',
            title: '编号'
        }, {
            field: 'identity.noId',
            title: '工号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            field: 'schoolStuNum',
            title: '校内学生数'
        }, {
            field: 'companyStuNum',
            title: '校外学生数'
        }, {
            field: 'allStuNum',
            title: '学生总数'
        }, {
            title: "操作",
            formatter: function (value, row, index) {
                return "<a class='btn btn-primary btn-sm' data-toggle='modal' data-target='#assignCompanyGruop'>选择小组</a> <a class='btn btn-primary btn-sm' data-toggle='modal' data-target='#stuMemberBytea'>指导学生</a> <a class='btn btn-primary btn-sm' data-toggle='modal' data-target='#assignStuOneNumber'>分配学生</a>";
            }
        }
    ];
    customSearch({
        id: "#table",
        url: "/identity/queryTeaOfStuName",
        dataType: "json",
        params: params(),
        columns: columns
    });
    var $teaId = '';
    $("#assignCompanyGruop").on('show.bs.modal', function (e) {
        $teaId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var columns = [{
            field: 'id',
            title: 'ID'
        }, {
            field: 'identityName',
            title: '组长'
        }, {
            field: 'phone',
            title: '联系方式'
        }, {
            field: 'groupNum',
            title: '小组人数'
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value, row, index) {
                return value == "ING" ? "申请中" : (value == "YES" ? "同意" : (value == "NO" ? "拒绝" : ""));
            }
        }, {
            field: 'createTime',
            title: '创建时间',
            formatter: function (value, row, index) {
                return timeStampYYYYMMDDHHMM(value);
            }
        }, {
            title: '操作',
            align: 'center',
            formatter: function (value, row, index) {
                var str = '';
                if (row.status == 'ING') {
                    str += "<button class='btn btn-primary btn-sm' onclick='assignCompany(" + row.id + ")'>分配</button>";
                }
                return str;
            }
        }
        ];
        customSearch({
            id: "#companyGroupInfo",
            url: "/admin/companyList",
            dataType: "json",
            params: {status:'ING',orderByClause: 'update_time'},
            columns: columns,
            height:400
        });
    });

    function assignCompany(id) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/admin/assignCompanyTea',
            data: {
                teaId: $teaId,
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }
        })
    }

    $("#stuMemberBytea").on('show.bs.modal', function (e) {
        $("#modelTable").html("<table class='table table-container' id='stuMember_tea'> </table>");
        var $teaId_stu = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var columns = [{
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            field: 'identity.phone',
            title: '联系方式'
        }];
        customSearch({
            id: "#stuMember_tea",
            url: "/admin/stuMemberByteaId",
            dataType: "json",
            params: {teaId:$teaId_stu},
            columns: columns,
            height:500
        });
    });

    $("#assignStuOneNumber").on('show.bs.modal', function (e) {
        $("#table_stuMember").html("<table class='table table-container' id='stuOneGroup'> </table>");
        $teaId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var columns = [{
            field: 'id',
            title: '小组ID'
        }, {
            field: 'identityName',
            title: '组长'
        }, {
            field: 'count',
            title: '人数'
        },{
            title: '操作',
            formatter: function (value, row, index) {
                var str = "<button class='btn btn-primary btn-sm' onclick='assignSchool(" + row.id + ")'>分配</button>";
                return str;
            }
        }];
        customSearch({
            id: "#stuOneGroup",
            url: "/group/getOneGroupStu",
            dataType: "json",
            params: {status:'school'},
            columns: columns,
            height:500
        });
    });

    function assignSchool(id) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/admin/assignOneGrop',
            data: {
                teaId: $teaId,
                groupId: id
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }
        })
    }

</script>
</body>
</html>
