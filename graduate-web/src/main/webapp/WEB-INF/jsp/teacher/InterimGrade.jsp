<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/12/10
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>毕业设计管理平台</title>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">中期进度</li>
			</ol>
			<div class="page-header">
				<h3>
					中期进度<small><font color="red">查看学生期中进度可以到--成绩查看--期中进度中查看详情</font></small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- start: TABLE WITH IMAGES PANEL -->
			<div class="panel-body">
				<div class="tabbable">
					<table class="table table-hover"
						style="TABLE-LAYOUT: fixed; WORD-BREAK: break-all">
						<thead>
							<tr>
								<th width="5%"><small>编号</small></th>
								<th width="15%"><small>学号</small></th>
								<th><small>学生姓名</small></th>
								<th><small>性别</small></th>
								<th><small>班级</small></th>
								<th><small>小组</small></th>
								<th width="15%"><small>课题</small></th>
								<th><small>任务</small></th>
								<th><small>中期进度</small></th>
								<th><small>操作</small></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="s" varStatus="i">
								<tr>
									<input type="hidden" class="stuId" value="${s.stuId}">
									<td><small>${i.index + 1}</small></td>
									<td><small>${s.stuStudentId}</small></td>
									<td><small>${s.stuName}</small></td>
									<td><small>${s.stuSex ? "女" : "男"}</small></td>
									<td><small>${s.stuClass}</small></td>
									<td
										style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
										title="${s.sgSName}的小组"><small>${s.sgSName}的小组</small></td>
									<td
										style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
										title="${s.topicTitle}"><small>${s.topicTitle}</small></td>
									<td id="childTop"
										style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
										title="${s.childTopicTitle}"><small>${s.childTopicTitle}</small>
									</td>
									<td><small> <c:if test="${s.interimGrade eq 90}">
                            		进度较好
                            		</c:if> <c:if test="${s.interimGrade eq 80}">
                            		进度正常
                            		</c:if> <c:if test="${s.interimGrade eq 60}">
                            		进度滞后
                            		</c:if> <c:if test="${s.interimGrade eq -1}">
                            		终止答辩
                            		</c:if> <c:if test="${s.interimGrade eq 0}">
                            		教师未审批
                            		</c:if>
									</small></td>
									<td><small> <c:if test="${s.va eq 0}">
												<a href="javaScript:void(0);" contenteditable="false"
													class="choose" data-toggle="modal"
													data-target="#AdminChoucha">抽查状态</a>
											</c:if> <c:if test="${s.va eq -1 and s.interimGrade eq 0}">
												<a href="javaScript:void(0);" class="choose"
													data-toggle="modal" data-target="#IterimGrade">给予成绩</a>
											</c:if> <c:if test="${s.va eq -1 and s.interimGrade ne 0}">
												<a href="javaScript:void(0);" contenteditable="false"
													class="choose" data-toggle="modal"
													data-target="#IterimGrade">重置</a>
											</c:if>
									</small></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="IterimGrade" tabindex="-1" role="dialog"
		aria-labelledby="alterModalLabel">
		<form action="./${sessionScope.pathCode}/InterimGrade.do"
			class="form-horizontal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="alterModalLabel">中期进度</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" name="stuId">
						<div class="form-group">
							<div class="radio">
								<label> <input type="radio" value="90" name="grade"
									class="grey"> 进度较好
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio" value="80" name="grade"
									class="grey"> 进度正常
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio" value="60" name="grade"
									class="grey"> 进度滞后
								</label>
							</div>
							<div class="radio">
								<label> <input type="radio" value="-1" name="grade"
									class="grey"> 终止答辩
								</label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭
						</button>
						<button type="submit" class="btn btn-primary">确认</button>
					</div>
				</div>
			</div>
		</form>
	</div>

<div class="modal fade" id="AdminChoucha" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">管理員抽查</h4>
                </div>
                <div class="modal-body">
                	输入的成绩
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="AdminChoucha" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">管理員抽查</h4>
                </div>
                <div class="modal-body">
                	该学生已经被抽查，成绩由管理员给定
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
</div>
	<div class="modal fade" id="AdminChoucha" tabindex="-1" role="dialog"
		aria-labelledby="alterModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="alterModalLabel">管理員抽查</h4>
				</div>
				<div class="modal-body">该学生已经被抽查，成绩由管理员给定</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
				</div>
			</div>
		</div>
	</div>

<script>
		$('#IterimGrade').on('show.bs.modal', function(e) {
			var $p = $(e.relatedTarget).parent().parent().parent();
			var $stuId = $p.find(".stuId");
			var $modal = $(this);
			$modal.find("input[name='stuId']").val($stuId.val());

		});
	</script>
</body>
</html>
