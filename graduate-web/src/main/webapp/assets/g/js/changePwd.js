/**
 * Created by chuchuang on 16/10/29.
 */
$(function () {
    var $oldpwd = $("#oldpwd");
    var $nowpwd = $("#nowpwd");
    var $againpwd = $("#againpwd");
    var $errorHandler = $(".errorHandler");
    $oldpwd.change(function () {
        $errorHandler.hide();
    });
    $nowpwd.change(function () {
        $errorHandler.hide();
    });
    $againpwd.change(function () {
        $errorHandler.hide();
    });
    $("#savepwd").click(function () {
        if ($oldpwd.val() == $nowpwd.val()) {
            $($errorHandler.find("span")).html("旧密码与新密码相同");
            $errorHandler.show();
            return false;
        }
        if ($nowpwd.val() != $againpwd.val()) {
            $($errorHandler.find("span")).html("新密码与重复密码不一致");
            $errorHandler.show();
            return false;
        }

        $.ajax({
            url:"/identity/myself/changePwd",
            data:{
                old:$oldpwd.val(),
                now:$nowpwd.val()
            },
            dataType:"json",
            type:"post",
            success:function (result) {
                if (result.success){
                    alert("修改成功");
                    $("#pwdModal").modal('hide');
                }else {
                    $($errorHandler.find("span")).html(result.errorMsg);
                    $errorHandler.show();
                }
            }
        });
    });
});