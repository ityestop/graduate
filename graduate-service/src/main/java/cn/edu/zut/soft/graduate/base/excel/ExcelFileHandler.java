package cn.edu.zut.soft.graduate.base.excel;

import java.util.List;
import java.util.Map;

/**
 * Created by chuchuang on 16/9/23.
 */
public interface ExcelFileHandler {

    List<Map<String,String>> handler(List<Map<String, String>> mapList);


}
