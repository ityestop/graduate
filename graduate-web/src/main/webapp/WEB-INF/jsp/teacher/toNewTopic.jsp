<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>教师申报题目</title>
    <%--<script charset="utf-8" src="assets/plugins/kindeditor/kindeditor.js"></script>--%>
    <%--<script charset="utf-8" src="assets/plugins/kindeditor/lang/zh_CN.js"></script>--%>
    <%--<script charset="utf-8" src="IAssets/js/toNewTopic.js"></script>--%>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <script type="application/javascript">

        $(function () {
            updatefg();
            $("#stuCount").change(function () {
                updatefg();
            });

            function updatefg() {
                var num = $("#stuCount").val();
                var st = "";
                
                if (num != $(".rwbt").length) {
                		if(num==1){
                			st += "<div class='rwbt'> <div class='form-group'> " +
                                "<label class='col-sm-2 control-label'>子标题</label> " +
                                "<div class='col-sm-7'> " +
                                "<input name='ctn' type='text' placeholder='请不要重复添加上方题目名称，如果没有子标题，可不填写' id='childName'class='form-control'> " +
                                "</div><span class='help-inline col-sm-3'> <i class='fa fa-info-circle'></i> <font color='red'>上方题目名称与此处子标题一共不超过25字</font> </span> </div> " +
                                "<div class='form-group'> <label class='col-sm-2 control-label'>任务分工</label> " +
                                "<div class='col-sm-7'><textarea name='content' style=\"height: 150px;\" placeholder='任务分工，此项必填' class='form-control'></textarea>" +
                                "</div> </div> </div>";
                		}else{
	                    for (var i = 0; i < num; i++) {
	                        st += "<div class='rwbt'> <div class='form-group'> " +
	                                "<label class='col-sm-2 control-label'>子标题"+(i + 1)+"</label> " +
	                                "<div class='col-sm-7'> " +
	                                "<input name='ctn' type='text' placeholder='此处为任务标题，请不要重复输入上方题目名称' id='childName'class='form-control'> " +
	                                "</div><span class='help-inline col-sm-3'> <i class='fa fa-info-circle'></i> <font color='red'>上方题目名称与此处子标题一共不超过25字</font> </span> </div> " +
	                                "<div class='form-group'> <label class='col-sm-2 control-label'>任务分工</label> " +
	                                "<div class='col-sm-7'><textarea name='content' style=\"height: 150px;\" placeholder='任务分工' class='form-control'></textarea>" +
	                                "</div> </div> </div>";
	                    }
                  }
                    $("#group_rw").html(st); 
                }
            }
        });

        function submitForm(){
            var num = $("input[name='jsly']:checked").size();
//            alert(num);
            if(num > 0){
                $("#topform").submit();
                return true;
            }
            alert("请选择技术领域");
            return false;
        }

    </script>
    
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">题目编辑</li>
        </ol>
        <div class="page-header">
            <h3>题目编辑
                <small>请填写项目基本信息</small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <form id="topform" role="form" class="form-horizontal" action="./${sessionScope.pathCode}/submitNewTopic.do" method="post">
                <div class="row">
                    <input type="hidden" name="topId" id="topId" value="${topic.topId}"/>
					<input type="hidden" name="topstat" value="${topic.topStatus}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="topicName">
                            题目名称 </label>

                        <div class="col-sm-7">
                            <input name="topTitle" type="text" placeholder="课题名称" id="topicName" class="form-control"
                                   maxlength="25" value="${topic.topTitle}">
                        </div>
                     
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="stuCount">
                            学生人数 </label>

                        <div class="col-sm-2">
                            <select name="topCount" id="stuCount" class="form-control search-select">
                                <option value="1"
                                        <c:if test="${topic.topCount == 1}">selected="selected"</c:if> >1
                                </option>
                                <option value="2"
                                        <c:if test="${topic.topCount == 2}">selected="selected"</c:if> >2
                                </option>
                                <option value="3"
                                        <c:if test="${topic.topCount == 3}">selected="selected"</c:if> >3
                                </option>
                            </select>
                        </div>
                         
                        <span class="help-inline col-sm-3"> <i
								class="fa fa-info-circle"></i><font color="red">请选择学生人数</font> </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-select-3">
                            课题来源 </label>

                        <div class="col-sm-2">
                            <select name="topTopicType" id="form-field-select-3" class="form-control search-select">
                                <option value="false" <c:if test="${topic.topTopicType}">selected="selected"</c:if>>社会服务
                                </option>
                                <option value="true"
                                        <c:if test="${topic.topTopicType}">selected="selected"</c:if> >科研项目
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            课题类型 </label>

                        <div class="col-sm-7">
                            <label>
                                <input type="radio" name="topType" value="设计" <c:if test="${topic.topType == '设计' || topic.topType == null}">checked="checked"</c:if>>
                            </label>设计  &nbsp;
                            <label>
                                <input type="radio" name="topType" value="研究" <c:if test="${topic.topType == '研究'}">checked="checked"</c:if>>
                            </label>研究 &nbsp;
                            <label>
                                <input type="radio" name="topType" value="" <c:if test="${topic.topType != null && topic.topType != '设计' && topic.topType != '研究'}">checked="checked"</c:if>>
                            </label>其他&nbsp;
                            <label>
                                <input type="text" name="fz" <c:if test="${topic.topType != null && topic.topType != '设计' && topic.topType != '研究'}">value="${topic.topType}" maxlength="10" </c:if> >
                            </label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jsly">
                            技术领域 </label>

                        <div class="col-sm-7" id="jsly">
                            <c:forEach items="${technophobeList}" var="te">
                                <label>
                                    <input type="checkbox" value="${te.tecId}" name="jsly"
                                           <c:if test="${topic.topTechnophobeList.indexOf(te) > -1}">checked="checked"</c:if> > ${te.techName}
                                </label>
                            </c:forEach>
                        </div>
                        <span class="help-inline col-sm-3"> <i
								class="fa fa-info-circle"></i> <font color="red">若此项为空将不能保存项目信息！</font></span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jsly">
                            项目背景 </label>

                        <div class="col-sm-7">
                            <textarea name='topContent' style="height: 150px;" placeholder="此处为项目背景，包含题目背景说明、设计内容说明，学生详细任务请写入下方任务分工" class="form-control">${topic.topContent}</textarea>
                        
                        </div>
                    </div>
                    <div class="page-header">
                        <h3>任务详情
                            <small>请填写项目子标题</small>
                        </h3>
                    </div>
                    <div id="group_rw">
                        <c:forEach items="${topic.childTopicList}" var="to" varStatus="s">
                        <div class='rwbt'>
                            <div class='form-group'>
                                <label class='col-sm-2 control-label'>子标题${s.index + 1}</label>

                                <div class='col-sm-7'>
                                    <input name='ctn' type='text' placeholder='子标题' id='childName'
                                           class='form-control' value="${to.name}">
                                </div>
                                 
                            </div>
                            <div class='form-group'><label class='col-sm-2 control-label'>任务分工</label>
                                <div class='col-sm-7'>
                                    <textarea name='content' style="height: 150px;" placeholder="任务分工" class="form-control">${to.content}</textarea>
                                </div>
                            </div>
                        </div>
                        </c:forEach>
                    </div>

                    <div class="from-group">
                     <label class="col-sm-3 control-label" ></label>
                       <div class="col-sm-4">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-blue btn-block" onclick="submitForm()">
                              	 <i class="clip-checkmark-2  "></i>    保存
                            </button>
                        </div>
                        <div class="col-sm-6">
									<button type="button" class="btn btn-blue btn-block"
										onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i>   返回</button>
								</div>
						</div>
                    </div>
                </div>
            </form>

        </div>
        <!-- end: GENERAL PANEL -->
    </div>
</div>
</div>
	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
	<script src="assets/js/form-elements.js"></script><!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			FormElements.init();
		});
	</script>
</body>
</html>
