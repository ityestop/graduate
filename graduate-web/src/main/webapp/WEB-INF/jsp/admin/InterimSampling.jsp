<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>软件学院毕业设计管理平台</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>

<body>

	<!-- start: PAGE HEADER -->

	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">中期抽查</li>
			</ol>
			<div class="page-header">
				<h3>
					中期抽查 <small><font color="red">实际指导教师人数：${teacherCount}人(管理员可以随机多次，选择其中1次结果设定为最终抽选结果)</font>
					</small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<!-- start: GENERAL PANEL -->
			<div class="panel">
				<div class="panel-body">
					<div class="form-group">
						<form action="${sessionScope.pathCode}/InterimSampling.do"
							method="post" commandName="studentSta" id="condition">
							<label class="col-sm-1 control-label" for="number"> 抽查人数</label>
							<div class="col-sm-3">
								<input value="${teacher.teaTeacherId }" type="text"
									placeholder="抽查人数" id="number" name="number"
									class="form-control">
							</div>
							<div class="col-sm-1">
								<c:if
									test="${sessionScope.functionMap['InterimScampling'].content eq 0 }">
									<button type="submit" class="btn btn-primary">随机抽查</button>
								</c:if>
								<c:if
									test="${sessionScope.functionMap['InterimScampling'].content ne 0 }">
									<a class="btn btn-primary" href="javaScript:void(0);"
										data-toggle="modal" data-target="#NoChoose"> 随机抽查</a>
								</c:if>
							</div>
						</form>
						<label class="col-sm-1"> </label>
						<form action="${sessionScope.pathCode}/InterimForNumber.do"
							method="post">
							<label class="col-sm-1 control-label" for="count"> 抽查次数</label>

							<div class="col-sm-3">
								<select class="form-control search-select" id="count"
									name="count">
									<option value="-1">全部</option>
									<c:forEach items="${count}" var="c" begin="0">
										<option value="${c}">${c}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-1">
								<c:if
									test="${sessionScope.functionMap['InterimScampling'].content eq 0 }">
									<button type="submit" class="btn btn-primary">查询</button>
								</c:if>
								<c:if
									test="${sessionScope.functionMap['InterimScampling'].content ne 0 }">
									<a class="btn btn-primary" href="javaScript:void(0);"
										data-toggle="modal" data-target="#NoChoose"> 查询</a>
								</c:if>
							</div>
						</form>
					</div>
					<div class="panel-body">
						<c:if
							test="${sessionScope.functionMap['InterimScampling'].content eq 0 and status eq false }">
							<c:forEach items="${student}" var="stu" varStatus="i">
								<div class="page-header">
									<h3>第${i.index + 1}次随机成员</h3>
									<div style="text-align: right;">
										<a
											href="${sessionScope.pathCode}/settingNumber.do?id=2B99208EAE81CAE43FF5D8004A9C8524&action=InterimScampling&content=${i.index+1}">设定为最终结果</a>
									</div>
								</div>
								<table class="table table-hover">
									<thead>
										<tr>
											<th><small>编号</small></th>
											<th><small>学号</small></th>
											<th><small>姓名</small></th>
											<th><small>班级</small></th>
											<th><small>指导教师</small></th>
										</tr>
										<c:forEach items="${stu}" var="s" varStatus="i">
											<tr>
												<td><small>${i.index + 1}</small></td>
												<td><small>${s.stuStudentId }</small></td>
												<td><small>${s.stuName }</small></td>
												<td><small>${s.stuClass}</small></td>
												<td><small>${s.teacher.teaName }</small></td>
											</tr>
										</c:forEach>
									</thead>

								</table>

							</c:forEach>
						</c:if>
						<c:if
							test="${sessionScope.functionMap['InterimScampling'].content eq 0 and status eq true }">
							<div class="page-header">
								<h3>第${count2}次随机成员</h3>
							</div>
							<table class="table table-hover">
								<thead>
									<tr>
										<th><small>编号</small></th>
										<th><small>学号</small></th>
										<th><small>姓名</small></th>
										<th><small>班级</small></th>
										<th><small>指导教师</small></th>
									</tr>
									<c:forEach items="${student}" var="s" varStatus="i">
										<tr>
											<td><small>${i.index + 1}</small></td>
											<td><small>${s.stuStudentId }</small></td>
											<td><small>${s.stuName }</small></td>
											<td><small>${s.stuClass}</small></td>
											<td><small>${s.teacher.teaName }</small></td>
										</tr>
									</c:forEach>
								</thead>

							</table>
						</c:if>
						<c:if
							test="${sessionScope.functionMap['InterimScampling'].content ne 0 }">

							<div style="text-align: left;">
								<h3>最终抽查结果</h3>
							</div>
							<div style="text-align: right;">
								<a class="btn btn-primary"
									href="./${sessionScope.pathCode}/downloadEx.do?id=2B99208EAE81CAE43FF5D8004A9C8524&action=InterimScampling&content=${i.index+1}">
									导出抽查结果</a>
							</div>


							<table class="table table-hover">
								<thead>
									<tr>
										<th><small>编号</small></th>
										<th><small>学号</small></th>
										<th><small>姓名</small></th>
										<th><small>班级</small></th>
										<th><small>指导教师</small></th>
										<th><small>中期进度</small></th>
										<th><small>操作</small></th>
									</tr>
									<c:forEach items="${student}" var="s" varStatus="i">
										<tr>
											<input type="hidden" class="stuId" value="${s.stuId}">
											<td><small>${i.index + 1}</small></td>
											<td><small>${s.stuStudentId }</small></td>
											<td><small>${s.stuName }</small></td>
											<td><small>${s.stuClass}</small></td>
											<td><small>${s.teacher.teaName }</small></td>
											<td><small> <c:if test="${s.interimGrade eq 90}">
			                            		进度较好
			                            		</c:if> <c:if test="${s.interimGrade eq 80}">
			                            		进度正常
			                            		</c:if> <c:if test="${s.interimGrade eq 60}">
			                            		进度滞后
			                            		</c:if> <c:if test="${s.interimGrade eq -1}">
			                            		终止答辩
			                            		</c:if> <c:if test="${s.interimGrade eq 0}">
			                            		未审批
			                            		</c:if>
											</small></td>
											<td><small> <c:if test="${s.interimGrade eq 0}">
														<a href="javaScript:void(0);" class="choose"
															data-toggle="modal" data-target="#IterimGrade">给予成绩</a>
													</c:if> <c:if test="${s.interimGrade ne 0}">
														<a href="javaScript:void(0);" class="choose"
															data-toggle="modal" data-target="#IterimGrade">重置</a>
													</c:if>
											</small></td>

										</tr>
									</c:forEach>
								</thead>
							</table>
						</c:if>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="NoChoose" tabindex="-1" role="dialog"
			aria-labelledby="CSGModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="CSGModalLabel">权限问题</h4>
					</div>
					<div class="modal-body">您好，您已经最终确定中期抽查人员，不能执行该操作！</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="IterimGrade" tabindex="-1" role="dialog"
			aria-labelledby="alterModalLabel">
			<form action="./${sessionScope.pathCode}/InterimGrade.do"
				class="form-horizontal">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="alterModalLabel">中期进度</h4>
						</div>
						<div class="modal-body">
							<input type="hidden" name="stuId">
							<div class="form-group">
								<div class="radio">
									<label> <input type="radio" value="90" name="grade"
										class="grey"> 进度较好
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" value="80" name="grade"
										class="grey"> 进度正常
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" value="60" name="grade"
										class="grey"> 进度较慢
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" value="-1" name="grade"
										class="grey"> 终止答辩
									</label>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">确认</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<script>
			$('#IterimGrade').on('show.bs.modal', function(e) {
				var $p = $(e.relatedTarget).parent().parent().parent();
				var $stuId = $p.find(".stuId");
				var $modal = $(this);
				$modal.find("input[name='stuId']").val($stuId.val());

			});
		</script>


		<script
			src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<!-- 3 -->
		<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
		<!-- 1 -->
		<script src="assets/plugins/select2/select2.min.js"></script>
		<!-- 2 -->
		<script src="assets/js/form-elements.js"></script>
		<!-- 4 -->
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				FormElements.init();

			});
		</script>
</body>
</html>
