package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by cc on 15/11/1.
 */
public enum JobTitle {
    DEFAULT("未分配",0),ASSISTANT("助教",1),LECTURER("讲师",2),ASSISTANT_PROFESSOR("副教授",3),PROFESSOR("教授",4);

    private String name;
    private int index;

    JobTitle(String name, int index) {
        this.name = name;
        this.index = index;
    }

    @Override
    public String toString() {
        return "name:" + name +" index:" + index;
    }

    public static JobTitle getName(int index){
        for (JobTitle s : JobTitle.values()){
            if (s.getIndex() == index){
                return s;
            }
        }
        return null;
    }

    public static JobTitle getIndex(String name){
        for (JobTitle s : JobTitle.values()){
            if (s.getName().equals(name)){
                return s;
            }
        }
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }



}
