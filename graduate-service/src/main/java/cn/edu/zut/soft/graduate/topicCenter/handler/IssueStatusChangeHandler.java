package cn.edu.zut.soft.graduate.topicCenter.handler;

import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */
public interface IssueStatusChangeHandler {
    void efficacyAndChange(LoginVO loginVO, Issue issue, IssuePhase.Handle handle);
}
