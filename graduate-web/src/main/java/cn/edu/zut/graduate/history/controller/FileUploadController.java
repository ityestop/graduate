//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.model.FileModel;
//import cn.edu.zut.soft.graduate.core.utils.DataLS;
//import cn.edu.zut.soft.graduate.core.utils.SaveFileUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.context.ServletContextAware;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Map;
//
//
///**
// * Created by cc on 16/5/10.
// */
//@Controller
//@RequestMapping("admin/file")
//public class FileUploadController implements ServletContextAware, ConfigDo {
//	private ServletContext context;
//
//	@Autowired
//	private ServiceFit serviceFit;
//
//	@Override
//	public void setServletContext(ServletContext servletContext) {
//		this.context = servletContext;
//	}
//
//	@RequestMapping("toAdminUploadFile")
//	public ModelAndView touploadFile(Map<String, Object> model) {
//		ResultDo resultDo = serviceFit.getFileModelService().findAll();
//		if (resultDo.isSuccess()) {
//			model.put("fileList", resultDo.getResult());
//		}
//		return new ModelAndView("admin/fileList");
//	}
//
//	@RequestMapping("uploadFile")
//	public String Upload(HttpServletRequest request,
//			@RequestParam(value = "file") MultipartFile file, Model model)
//			throws IOException {
//		ResultDo resultDo;
//		String filename = file.getOriginalFilename();
//		FileModel fileModel = new FileModel(filename, file);
//		System.out.println(DataLS.functionMap.get("fileaddress").getContent()+"*-*");
//		fileModel.setFileAddrss(DataLS.functionMap.get("fileaddress").getContent());
//		resultDo = serviceFit.getFileModelService().create(fileModel);
//		if (resultDo.isSuccess()) {
//			return "redirect:./toAdminUploadFile.do";
//		} else {
//			model.addAttribute("mag", "上传未成功！");
//		}
//		return "redirect:./toAdminUploadFile.do";
//	}
//
//	@RequestMapping("download")
//	public void download(@RequestParam("id") String id,
//			HttpServletRequest request, HttpServletResponse response)
//			throws Exception {
//		request.setCharacterEncoding("UTF-8");
//		response.setContentType("multipart/form-data");
//		ResultDo<FileModel> resultDo = serviceFit.getFileModelService()
//				.findById(id);
//		FileModel fileModel = resultDo.getResult();
//		if (fileModel == null) {
//			return;
//		}
////		System.out.println(fileModel.getRealName());
//		response.setHeader("Content-Disposition", "attachment; filename=" + fileModel.getSaveName()
//				+ "." + fileModel.getFileType());
//		response.setContentType("application/octet-stream; charset=utf-8");
////		System.out.println(fileModel.getFileAddrss()+"/" + fileModel.getSaveName()
////				+ "." + fileModel.getFileType()+"---1");
//		SaveFileUtil.down(fileModel.getFileAddrss() +"/"+ fileModel.getSaveName()
//				+ "." + fileModel.getFileType(), response.getOutputStream());
//		serviceFit.getFileModelService().updateDowm(fileModel.getId());
//	}
//
//	@RequestMapping("deleteFile")
//	public ModelAndView uploadFileTwo(@RequestParam(value = "id") String fileId) {
//		if (fileId != null) {
//			ResultDo resultDo = serviceFit.getFileModelService().delete(fileId);
//			if (resultDo.isSuccess()) {
//				return new ModelAndView(
//						"redirect:./toAdminUploadFile.do");
//			}
//		}
//		return new ModelAndView("redirect:./toAdminUploadFile.do");
//	}
//}
