    /**
 * Created by chuchuang on 16/10/28.
 */
$(function () {

    var $id = $("#id");
    var $noid = $("#noid");
    var $name = $("#name");
    var $sex = $("#sex");
    var $professional = $("#professional");
    var $phone = $("#phone");
    var $email = $("#email");
    var $sexSpan;
    var $phoneSpan;
    var $emailSpan;
    var $professionalSpan;
    $.ajax({
        url: "/identity/myself",
        dataType: "json",
        type: "get",
        success: function (result) {
            if (result.success) {
                var data = result.data;
                $id.val(data.identity.id);
                $noid.val(data.identity.noId);
                $name.val(data.identity.name);
                $sex.val(data.identity.sex);
                $sexSpan = $($sex.next("span")).click(function () {
                    if ($sex.attr('disabled')) {
                        $sex.removeAttr('disabled');
                    } else {
                        $.ajax({
                            url:"/identity/myself",
                            data:{
                                sex:$sex.val()
                            },
                            dataType:"json",
                            type:"post",
                            success:function (res) {
                                if (res.success){
                                    alert("修改成功");
                                    $sex.attr('disabled','disabled');
                                }else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
                $phone.val(data.identity.phone);
                // $phoneSpan = $($phone.next("span")).click(function () {
                //     if ($phone.attr('disabled')) {
                //         $phone.removeAttr('disabled');
                //     } else {
                //         $.ajax({
                //             url:"/identity/myself",
                //             data:{
                //                 phone:$phone.val()
                //             },
                //             dataType:"json",
                //             type:"post",
                //             success:function (res) {
                //                 if (res.success){
                //                     alert("修改成功");
                //                     $phone.attr('disabled','disabled');
                //                 }else {
                //                     alert(res.errorMsg);
                //                 }
                //             }
                //         });
                //     }
                // });
                $email.val(data.identity.email);
                $emailSpan = $($email.next("span")).click(function () {
                    if ($email.attr('disabled')) {
                        $email.removeAttr('disabled');
                    } else {
                        $.ajax({
                            url:"/identity/myself",
                            data:{
                                email:$email.val()
                            },
                            dataType:"json",
                            type:"post",
                            success:function (res) {
                                if (res.success){
                                    alert("修改成功");
                                    $email.attr('disabled','disabled');
                                }else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
                $.each(data.infoList, function (i, node) {
                    if (node.key == 'professionalLever') {
                        $professional.val(node.value);
                    }
                });
                $professionalSpan = $($professional.next("span")).click(function () {
                    if ($professional.attr('disabled')) {
                        $professional.removeAttr('disabled');
                    } else {
                        $.ajax({
                            url:"identity/info/"+data.identity.id+"/"+$professional.val(),
                            dataType:"json",
                            type:"get",
                            success:function (res) {
                                if (res.success){
                                    alert("修改成功");
                                    $professional.attr('disabled','disabled');
                                }else {
                                    alert(res.errorMsg);
                                }
                            }
                        });
                    }
                });
            } else {
                alert(result.errorMsg);
            }
        }
    });


});