<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 2017/3/25
  Time: 下午5:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>中期抽取人员名单</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table {
            font-size: 12px;
        }
    </style>
</head>
<body>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">中期答辩人员名单</li>
        </ol>
        <div class="page-header">
            <h4>中期答辩人员名单
            </h4>
            <font color="red">如果应用过之后想重新计算的话，需要删除物理文件才可以。</font>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12 ">
        <div class="form-group">
            <div class="col-sm-3">
                <button id="dateExpert" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">
                    中期答辩抽取
                </button>
                <button id="saveMidRan" class="btn btn-primary" disabled="disabled">
                    应用
                </button>
            </div>
        </div>
        <div class="form-group">
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gridSystemModalLabel">中期抽取</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        抽取规则：若抽取人数小于教师总数，对所有的学生进行随机抽取。若抽取的人数大于教师总数，则每个教师抽取1名学生，剩余学生完全随机抽取。
                        <br/>
                        例子：若教师人数为40，输入数目为30，则所有学生中抽取30人，若输入数字为50，则40个教师的指导学生中选择一个，剩余10个学生在其余学生中随机选择。
                    </div>
                    <div class="row">
                        <form>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">
                                    请输入抽取人数
                                </label>
                                <div class="col-sm-8">
                                    <input id="num" class="form-control" placeholder="抽取人数" type="number" value="0">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <button class="btn btn-primary col-md-offset-10" id="subNum">提交</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">

    var columns = [
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            field: 'identity.phone',
            title: '手机号'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var $strOne = "";
                var $aone = "<a href='/admin/issue?id=";
                var $atwo = "'>";
                var $athree = "</a>";
                var $result = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'topicName') {
                        $strOne = array[i].value;
                        break;
                    }
                }
                for (var j = 0; j < array.length; j++) {
                    if (array[j].type == "TOPIC" && array[j].key == "topicId") {
                        $result = $aone + array[j].value + $atwo + $strOne + $athree;
                        break;
                    } else {
                        $result = $strOne;
                    }
                }
                return $result;
            }
        }, {
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }
    ];

    $(function () {
        customSearch({
            id: "#table",
            url: "/identity/selectDrawing",
            dataType: "json",
            params: {},
            columns: columns,
            pageSize: 100
        });

        $("#subNum").click(function (e) {
            var $this = $(this);
            var num = $("#num").val();
            if(num == 0){
                return;
            }
            $this.attr("disabled","disabled");
            $this.html("提交中...");
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: "/identity/randomDrawing",
                data: {
                    num: num
                },
                success: function (result) {
                    if (result.success) {
                        window.location.href = '/admin/randomDrawing';
                    } else {
                        alert(result.errorMsg);
                        $this.html("提交");
                        $this.removeAttr("disabled");
                    }
                }
            });
        });

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: "/admin/detectionMidRan",
            success: function (result) {
                if (result.success) {
                    if (result.data){
                        $("#saveMidRan").removeAttr("disabled");
                    }
                } else {
                    alert(result.errorMsg);
                }
            }
        });

        $("#saveMidRan").click(function () {
            var $this = $(this);
            $this.attr("disabled","disabled");
            $this.html("应用中...");
            $.ajax({
               type:'post',
                dataType:'json',
                url:'/admin/saveMidRan',
                success: function (result) {
                    if (result.success) {
                        alert("应用完成");
                    } else {
                        alert(result.errorMsg);
                        $this.removeAttr("disabled");
                    }
                    $this.html("应用");
                }
            });
        });
    })

</script>
</body>
</html>
