<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<title></title>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="/admin"> 首页 </a></li>
				<li class="active">功能设置</li>
			</ol>
			<div class="page-header">
				<c:if test="${not empty errorMsg}">
					<div class="alert alert-warning">
						${errorMsg}
					</div>
				</c:if>
				<h4>功能设置</h4>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
				<div class="panel-body">
					<table id="sample-table-1" class="table table-hover"style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all" >
						<thead>
							<tr>
								<th><small>编号</small></th>
								<th><small>功能名称</small></th>
								<th><small>状态</small></th>
								<th><small>操作</small></th>
							</tr>
							<c:forEach items="${config}" var="config" varStatus="i">
							<input type="hidden" class="functionId" value="${function.id}">
							<tr>
								<td><small>${i.index + 1 }</small></td>
								<c:if test="${config.configKey eq 'issueStart'}">
									<td><small>选报开关</small></td>
								</c:if>
								<c:if test="${config.configKey eq 'issueEdit'}">
									<td><small>编辑开关</small></td>
								</c:if>

								<td><small>${config.configValue == false ? "未开启":"开启状态"}</small></td>
								<c:if test="${config.configValue eq false and config.configKey eq 'issueStart'}">
									<td><small><a href="./admin/updateConfig?key=${config.configKey }&value=true">开启</a> | 关闭</small></td>
								</c:if>
								<c:if test="${config.configValue eq true and config.configKey eq 'issueStart'}">
									<td><small> 开启 | <a href="./admin/updateConfig?key=${config.configKey }&value=false">关闭</a></small></td>
								</c:if>
								<c:if test="${config.configValue eq false and config.configKey eq 'issueEdit'}">
									<td><small><a href="./admin/updateConfig?key=${config.configKey }&value=true">开启</a> | 关闭</small></td>
								</c:if>
								<c:if test="${config.configValue eq true and config.configKey eq 'issueEdit'}">
									<td><small> 开启 | <a href="./admin/updateConfig?key=${config.configKey }&value=false">关闭</a></small></td>
								</c:if>
							</tr>
							</c:forEach>
						</thead>
					</table>
				</div>
		</div>
	</div>
</body>
</html>
