package cn.edu.zut.soft.graduate.core.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chuchuang on 16/10/22.
 */
public enum IssueSource implements NameString {
    school("校内课题"), myself("自拟题目");

    protected static final Map<Role, IssueSource> map = new HashMap<>();

    static {
        map.put(Role.stu, myself);
        map.put(Role.tea, school);
    }

    public static IssueSource findIssueSource(Role role) {
        return map.get(role);
    }

    private String name;

    IssueSource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private static Map<String, String> issueSourceMap = new HashMap<String, String>();

    public static Map<String, String> getMap() {
        for (IssueSource source : IssueSource.values()) {
            issueSourceMap.put(source.name(), source.getName());
        }
        return issueSourceMap;
    }


}
