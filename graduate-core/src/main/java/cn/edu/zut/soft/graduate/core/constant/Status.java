package cn.edu.zut.soft.graduate.core.constant;

/**
 * Created by cc on 15/11/3.
 */
public interface Status {

    String getName();

    int getIndex();

}
