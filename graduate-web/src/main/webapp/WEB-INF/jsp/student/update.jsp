<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改密码</title>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/homePage.do"> Home </a>
				</li>
				<li class="active">修改密码</li>
			</ol>
			<div class="page-header">
				<h2>修改密码</h2>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
		<div class="col-md-12">
			<!-- start: TABLE WITH IMAGES PANEL -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-reorder"></i> 修改密码
					<div class="panel-tools">
						<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
						</a> <a class="btn btn-xs btn-link panel-config" href="#panel-config"
							data-toggle="modal"> <i class="fa fa-wrench"></i> </a> <a
							class="btn btn-xs btn-link panel-refresh" href="#"> <i
							class="fa fa-refresh"></i> </a> <a
							class="btn btn-xs btn-link panel-expand" href="#"> <i
							class="fa fa-resize-full"></i> </a> <a
							class="btn btn-xs btn-link panel-close" href="#"> <i
							class="fa fa-times"></i> </a>
					</div>
				</div>
				<div class="panel-body">
					<form id="condition" role="form" class="form-horizontal" action="" method="post">
							<div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-7">
												请输入新密码
											</label>
											<div class="col-sm-5">
												<input type="text" placeholder="Text Field" id="form-field-7" class="form-control">
											</div>
											
							</div>
							<div class="form-group">
											<label class="col-sm-2 control-label" for="form-field-7">
												请再次输入密码
											</label>
											<div class="col-sm-5">
												<input type="text" placeholder="Text Field" id="form-field-7" class="form-control">
											</div>
											
							</div>
							<div class="from-group"> 
								<div class="col-sm-4" style="text-align:right;">
								<button type="button" class="btn btn-primary">
											-提交-
										</button>
								</div>
								<div class="col-sm-2" style="text-align:right;">
								<button type="button" class="btn btn-primary">
											-重置-
										</button>
								</div>
							</div>
					</form>
				</div>
			</div>
			<!-- end: TABLE WITH IMAGES PANEL -->
		</div>

	</div>	
	
</body>
</html>
