/**
 * Created by cc on 2016/10/25.
*/
document.write("<script src='/assets/g/js/config.js'></script>");

$(function () {
    var $editRole = false;
    $.ajax({
       type:'post',
        url:'/config/getIssueEditRole',
        dataType:'json',
        success:function (result) {
            if(result.success){
                $editRole = true;
            }
        }
    });

    function timeStamp2String(time) {
        var datetime = new Date();
        datetime.setTime(time);
        var year = datetime.getFullYear();
        var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
        var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
        var hour = datetime.getHours() < 10 ? "0" + datetime.getHours() : datetime.getHours();
        var minute = datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
        var second = datetime.getSeconds() < 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
        return year + "-" + month + "-" + date;
    }

    var dataResult = {};

    var dataList = [];

    function getTr() {
        return "<tr></tr>";
    }

    function getTd() {
        return "<td></td>";
    }

    //change for slx
    function getTbody() {
        return"<tbody></tbody>";
    }


    function getId() {
        return "<a href='#'></a>"
    }

    function getOperation(node) {

        //提交审核
        var $subButton = $("<button class='btn btn-link '>提交</button>").click(function () {
            $.ajax({
                url: "/issue/issueOperate/UpAudit/" + node.id,
                dataType: "json",
                type: "get",
                success: function (result) {
                    if (result.success) {
                        location.reload();
                    } else {
                        alert(result.errorMsg);
                    }
                }
            });
        });
        var $editButton = $("<button class='btn btn-link '>编辑</button>").click(function () {
            //页面跳转
            window.location.href = '/tea/declare?issueId=' + node.id;
        });

        var $againEditButton = $("<button class='btn btn-link '>重新编辑</button>").click(function () {
            //页面跳转,修改为保存状态
            $.ajax({
                url: "/issue/issueOperate/Reedit/" + node.id,
                dataType: "json",
                type: "get",
                success: function (result) {
                    if (result.success) {
                        window.location.href = '/tea/declare?issueId=' + node.id;
                    } else {
                        alert(result.errorMsg);
                    }
                }
            });
        });

        var $giveUp = $("<button class='btn btn-link '>失效</button>").click(function () {
            //设定为失效
            $.ajax({
                url: "/issue/issueOperate/SaveIngGiveUp/" + node.id,
                dataType: "json",
                type: "get",
                success: function (result) {
                    if (result.success) {
                        location.reload();
                    } else {
                        alert(result.errorMsg);
                    }
                }
            });
        });
        var $btnGroup = $("<div class='btn-group'>");
        if($editRole){
            $btnGroup.append($editButton);
            if (node.status == 'saveIng') {
                $btnGroup.append($subButton, $giveUp);
            }
            if (node.status == 'auditErrorIng') {
                $btnGroup.append($giveUp,$againEditButton);
            }
        }else{
            if (node.status == 'saveIng') {
                $btnGroup.append($subButton, $editButton, $giveUp);
            }
            if (node.status == 'auditErrorIng') {
                $btnGroup.append($giveUp,$againEditButton);
            }
        }

        return $btnGroup;
    }

    function refurbishTeaIssue() {
        var $tbody=$(getTbody());
        $.each(dataResult, function (i, node) {
            var $row = $(getTr());
            var r1 = $(getTd());
            r1.append($(getId()).append(node.title).attr("href", "/tea/issueDetail?id=" + node.id))
                .attr('title', node.title)
                .attr('style', 'overflow:hidden;white-space:nowrap;text-overflow:ellipsis;');
            $row.append(r1);
            var r2 = $(getTd());
            if(node.source == 'school'){
                r2.append('校内题目');
            }else if(node.source == 'myself'){
                r2.append('公司项目');
            }
            $row.append(r2);
            // var r3 = $(getTd());
            // r3.append(sourceMaps().issueKindMap[node.kind]);
            // $row.append(r3);
            var r4 = $(getTd());
            r4.append(node.taskSize);
            $row.append(r4);
            var r6 = $(getTd());
            var $label = $("<label></label>");
            $label.addClass("label");
            switch (node.status) {
                case 'saveIng':
                    $label.addClass('label-info');
                    break;
                case 'auditIng':
                    $label.addClass('label-warning');
                    break;
                case 'auditErrorIng':
                    $label.addClass('label-danger');
                    break;
                case 'giveUp':
                    $label.addClass('label-inverse');
                    break;
                case 'finish':
                    $label.addClass('label-success');
                    break;
                default:
                    $label.addClass('label-default');
            }
            r6.append($label.append(sourceMaps().issuePhaseMap[node.status]));
            $row.append(r6);
            var r7 = $(getTd());
            if (node.technology != null && JSON.parse(node.technology).length != 0) {
                $.each(JSON.parse(node.technology), function (i, child) {
                    r7.append(child.title + "、");
                });
            }
            $row.append(r7);

            var r5 = $(getTd());
            r5.append(timeStamp2String(node.createTime));
            $row.append(r5);

            var r8 = $(getTd());
            //获取操作按钮
            r8.append(getOperation(node));
            $row.append(r8);
            $tbody.append($row)
        });
        $("#sample-table-1").append($tbody);
    }

    $.ajax({
        url: "/issue/querySelf",
        dataType: "json",
        type: "get",
        success: function (result) {
            // alert("")
            if (result.success) {
                dataResult = result.data;
                refurbishTeaIssue();
            } else {
                alert(result.errorMsg);
            }

        }
    });

});