<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>小组学生</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table{
            font-size: 12px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">答辩小组学生</li>
        </ol>
        <div class="page-header">
            <h4>答辩小组学生 <small>请按照自己需要条件进行检索,当检索条件为空时导出所有学生,支持条件导出哦</small>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12 ">
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    答辩小组
                </label>
                <div class="col-sm-5">
                    <select class="form-control search-select" id="groupId">
                        <option value="">请选择</option>
                    </select>
                </div>

                <label class="col-sm-1 control-label">
                    答辩地区
                </label>
                <div class="col-sm-5">
                    <select class="form-control search-select" title="状态列表" id="address">
                        <option value="">请选择</option>
                        <option value="zhengzhou">郑州</option>
                        <option value="hangzhou">杭州</option>
                        <option value="beijing">北京</option>
                        <option value="shanghai">上海</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    姓名
                </label>
                <div class="col-sm-5">
                    <input name="title" class="form-control" placeholder="学生姓名" id="name">
                </div>

                <div class="col-sm-1" style="text-align: right;">
                    <button id="findStu" class="btn btn-primary">检索</button>
                </div>

                <div class="col-sm-1">
                    <button id="dateExpert" class="btn btn-primary">开题数据导出</button>
                </div>
            </div>
        </div>
        <div class="form-group" id="exportTable">
        </div>
        <div class="form-group" id="exportTableTwo">
        </div>

    </div>
</div>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">
    var $groupIdTea='';
    var $name='';
    var $address='';
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: "/admin/getAllTeaGroup",
        data: {
            pageSize: 1000
        },
        success: function (result) {
            if (result.success) {
                var data = result.data;
                $.each(data, function (key, value) {
                    $("#groupId").append("<option value=" + value.id + ">" + value.identityName + "</option>");
                })
            }
        }
    });

    $("#findStu").click(function () {
        selectData();
    });

    function selectData() {
        $groupIdTea= $("#groupId").val();
        $name= $("#name").val();
        $address= $("#address").val();

        $("#exportTableTwo").html('<table id="table" class="table table-container"></table>');
        var columns = [
            {
                field: 'identity.noId',
                title: '学号'
            }, {
                field: 'identity.name',
                title: '姓名'
            }, {
                field: 'identity.phone',
                title: '手机号'
            }, {
                title: '班级',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                            str += array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '指导教师',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '课题',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var $strOne = "";
                    var $aone = "<a href='/admin/issue?id=";
                    var $atwo = "'>";
                    var $athree = "</a>";
                    var $result = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'topicName') {
                            $strOne = array[i].value;
                            break;
                        }
                    }
                    for (var j = 0; j < array.length; j++) {
                        if(array[j].type == "TOPIC" && array[j].key == "topicId"){
                            $result = $aone+array[j].value+$atwo+$strOne+$athree;
                            break;
                        }else{
                            $result = $strOne;
                        }
                    }
                    return $result;
                }
            }, {
                title: '子任务',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            },{
                title: '是/否',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "<font color='red'>未分配<font>";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                            str = "已分配";
                            break;
                        }
                    }
                    return str;
                }
            },
//        {
//            title: '子任务'
//        },
            {
                title: '答辩地区',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].key == 'address' && array[i].value == 'shanghai') {
                            str = '上海';
                            break;
                        } else if (array[i].key == 'address' && array[i].value == 'beijing') {
                            str = '北京';
                            break;
                        } else if (array[i].key == 'address' && array[i].value == 'hangzhou') {
                            str = '杭州';
                            break;
                        } else {
                            str = '郑州';
                        }
                    }
                    return str;
                }
            }
        ];
        customSearch({
            id: "#table",
            url: "/identity/teaGroupStu",
            dataType: "json",
            params: {groupId:$groupIdTea, name:$name, address:$address},
            columns: columns,
            pageSize: 25
        });
    }

    $("#dateExpert").click(function () {
        $groupIdTea= $("#groupId").val();
        $name= $("#name").val();
        $address= $("#address").val();
        window.location.href='/file/downloadTeaGroupStu?groupId='+$groupIdTea+'&name='+$name+'&address='+$address;
    });

    $(function () {
        selectData();
    });

</script>
</body>
</html>
