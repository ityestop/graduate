package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class IssueContent {
    private OutTeacher outTeacher;
    private SchoolTeacher schoolTeacher;
    private String background;
//    private List<Technology> technologyList;
    private List<Task> taskList;
    private String remark;

}
