package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/27
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DeclareQuery extends Query {
    private Integer id;
    private Integer topicId;//课题主键
    private Integer groupId;//小组主键
    private DeclareStatus status;//申报状态
    private Integer del;//
    private List<Integer> ids;//

    private List<DeclareStatus> declareStatusList;//状态集
}
