package cn.edu.zut.soft.graduate.core.constant.config;

/**
 * @author cc
 * @Date 2017/1/5
 * @value 1.0
 */
public enum  TopicPhase implements TopicPhaseAble {
    openGrade_one("一次开题",Value.openGrade), openGrade_double("二次开题",Value.openGrade),mid("中期答辩",Value.mid),plea_open("公开答辩",Value.last),plea_one("一次答辩",Value.last),plea_two("二次答辩",Value.last);

    private String title;

    private Value value;

    TopicPhase(String title,Value value) {
        this.title = title;
        this.value = value;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public Value getValue() {
        return value;
    }

    public enum Value implements TopicPhaseAble{
        openGrade("开题成绩"),mid("中期成绩"), last("最终成绩");

        private String title;

        Value(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
