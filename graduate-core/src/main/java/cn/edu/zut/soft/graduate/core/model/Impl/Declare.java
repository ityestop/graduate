package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Declare extends BaseModelImpl {

    private Integer topicId;//课题主键
    private Integer groupId;//小组主键
    private DeclareStatus status;//申报状态

}
