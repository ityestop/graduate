package cn.edu.zut.soft.graduate.core.constant;

/**
 * @author cc
 * @Date 2017/1/6
 * @value 1.0
 */
public interface InfoTypeAble {
    String name();
}
