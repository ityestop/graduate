package cn.edu.zut.soft.graduate.topicCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.CompanyProjectRequest;
import org.springframework.stereotype.Component;

@Component
public interface CompanyProjectRequestDAO<T extends CompanyProjectRequest> extends BasicDAO<Integer,T> {

}