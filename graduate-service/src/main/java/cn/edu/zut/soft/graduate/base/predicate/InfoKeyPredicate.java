package cn.edu.zut.soft.graduate.base.predicate;

import cn.edu.zut.soft.graduate.core.constant.InfoTypeAble;
import cn.edu.zut.soft.graduate.core.constant.config.TopicPhaseAble;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import org.apache.commons.collections.Predicate;

import java.util.Collections;
import java.util.List;

/**
 * @author cc
 * @Date 2017/1/5
 * @value 1.0
 */
public class InfoKeyPredicate implements Predicate {

    private List<TopicPhaseAble> ableList;
    private InfoTypeAble infoTypeAble;

    private InfoKeyPredicate(InfoTypeAble infoTypeAble, TopicPhaseAble able) {
        this.infoTypeAble = infoTypeAble;
        this.ableList = Collections.singletonList(able);
    }
    private InfoKeyPredicate(InfoTypeAble infoTypeAble,List<TopicPhaseAble> ableList) {
        this.infoTypeAble = infoTypeAble;
        this.ableList = ableList;
    }


    public List<TopicPhaseAble> getAbleList() {
        return ableList;
    }

    public void setAbleList(List<TopicPhaseAble> ableList) {
        this.ableList = ableList;
    }

    @Override
    public boolean evaluate(Object o) {
        //先比较type,再比较key
        if (infoTypeAble == null || infoTypeAble.name().equals(((Info)o).getType())) {
            if (ableList == null){
                return true;
            }
            for (TopicPhaseAble phase : ableList) {
                if (phase.name().equals(((Info) o).getKey())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static InfoKeyPredicate create(InfoTypeAble infoTypeAble,TopicPhaseAble able){
        return new InfoKeyPredicate(infoTypeAble,able);
    }
    public static InfoKeyPredicate create(InfoTypeAble infoTypeAble,List<TopicPhaseAble> able){
        return new InfoKeyPredicate(infoTypeAble,able);
    }
}
