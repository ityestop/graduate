package cn.edu.zut.soft.graduate.topicCenter.handler;

import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

import static cn.edu.zut.soft.graduate.core.constant.IssuePhase.Handle.ChooseSuccess;

/**
 * @author chuchuang
 * @date 16/11/29
 */
@Component
public class ChangeToFinish  extends AbstractChangeHandler {
    private List<IssuePhase.Handle> handleList = Collections.singletonList(ChooseSuccess);


    @Override
    protected boolean conform(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        return handleList.contains(handle);
    }

    @Override
    protected void conformChange(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        Assert.notNull(issue);
        //优化性能,去除大对象的修改
        issue.setContent(null);
        //提前记录接下来的变化
        issue.addStatus(handle.getDst());
    }

}
