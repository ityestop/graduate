<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 2017/2/22
  Time: 下午9:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>周月报规则管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/tea"> 首页 </a></li>
            <li class="active">周月报列表</li>
        </ol>
        <div class="page-header">
            <h3>周月报列表</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-horizontal" action="/admin/report/listData" method="get">
                <%--<div class="form-group">--%>
                <%--<a class="btn btn-primary" href="/stu/report/addData">--%>
                <%--<span><i class=" clip-plus-circle"></i> 添加报告</span>--%>
                <%--</a>--%>
                <%--</div>--%>
                <div class="form-group">
                    <label class="col-sm-1 control-label">规则:</label>
                    <div class="col-sm-4">
                        <select class="form-control " type="form-control" title="规则" name="ruleIds">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-10">
                        <button class="btn btn-primary">
                            <span><i class=" clip-plus-circle"></i> 检索报告</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="dataTable" class="table table-container" data-unique-id="id">
            </table>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>

<!-- Modal -->
<div class="modal fade" id="yulan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">预览</h4>
            </div>
            <div class="modal-body" id="yulan-content">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<%--<!-- Modal -->--%>
<%--<div class="modal fade" id="delmodel" tabindex="-1" role="dialog" aria-labelledby="delmodelLabel">--%>
<%--<div class="modal-dialog" role="document">--%>
<%--<div class="modal-content">--%>
<%--<div class="modal-header">--%>
<%--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--%>
<%--aria-hidden="true">&times;</span></button>--%>
<%--<h4 class="modal-title" id="delmodelLabel">确认删除</h4>--%>
<%--</div>--%>
<%--<div class="modal-body" id="delmodel-content">--%>
<%--</div>--%>
<%--<div class="modal-footer">--%>
<%--<button type="button" class="btn btn-primary">确认删除</button>--%>
<%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript">
    $(function () {
        var $table = $("#dataTable");
        var $selectObj = $("select[name='ruleIds']");

        function getParams() {
            return {
                escape: true,
                ruleIds: $selectObj.val()
            }
        }

        var $model = $("#yulan-content");

        var $yulan = $('#yulan');

        function delete_confirm(row) {
            var na = confirm("删除是不可恢复的，你确认要删除吗？");
            if (na) {
                $.ajax({
                    url: "/report/data/delete",
                    type: "POST",
                    data: {
                        id: row.reportData.id
                    },
                    dataType: "json",
                    success: function (result) {
                        if (result.success) {
//                            $table.bootstrapTable('remove', {field: 'reportData.id', "values": [row]});
                            alert("删除成功");
                            window.location.reload()
                        } else {
                            alert(result.errorMsg);
                        }
                    }
                });
            }
        }

        window.operateEvents = {
            'click .preview': function (e, value, row, index) {
                $yulan.modal('show', row);
            },
            'click .delete': function (e, value, row, index) {
                delete_confirm(row);
            }
        };

        var columns = [{
            field: 'reportData.id',
            title: '编号'
        }, {
            field: 'reportRule',
            title: '规则',
            formatter: function (value, row, index) {
                return "第 " + value.num + " " + (value.type == 1 ? "周" : "月") + "报告";
            }
        }, {
            field: 'userDescVO',
            title: '学生',
            formatter: function (value, row, index) {
                return value.identity.name;
            }
        }, {
            field: 'reportData.supplement',
            title: '状态',
            formatter: function (value, row, index) {
                return value == 0 ? "正常提交" : "补交";
            }
        }, {
            field: 'reportData.updateTime',
            title: '最后修改时间',
            formatter: function (value, row, index) {
                return timeStamp2String(value);
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var str = "";
                str += "<button class='preview btn btn-green'>预览</button>";
                str += "<button class='delete btn btn-danger'>删除</button>";
                return str;
            },
            events: operateEvents
        }];


        $.ajax({
            url: "/report/rule/list",
            type: "GET",
            dataType: "json",
            success: function (result) {
                if (result.success) {
                    for (var i in result.data) {
                        var $opt = $("<option value='" + result.data[i].id + "'>" + "第 " + result.data[i].num + " " + (result.data[i].type == 1 ? "周" : "月") + "报告" + "</option>");
                        if (result.data[i].id == ${'' + param.ruleIds}) {
                            $opt.attr("selected", "selected");
                        }
                        $selectObj.append($opt);
                    }
                    customSearch({
                        $table: $table,
                        url: "/report/data/list/admin",
                        dataType: "json",
                        params: getParams(),
                        columns: columns,
                        pageSize: 25,
                        uniqueId: "id"
                    });
                } else {
                    alert(result.errMsg);
                }
            }
        });


        $yulan.on('show.bs.modal', function (e) {
            $model.html(e.relatedTarget.reportData.context);
        });
        $yulan.on('hide.bs.modal', function (e) {
            $model.html("");
        })
    });

</script>

</body>
</html>
