<jsp:useBean id="user" scope="session" type="cn.edu.zut.graduate.entity.Student"/>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<html>
<head>
    <title>修改密码</title>
 <script type="text/javascript">
 function queren(){
 	$("#condition").submit();
 }
 </script>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">修改密码</li>
        </ol>
        <div class="page-header">
            <h3>修改密码
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->

<c:choose>
<c:when test="${mag eq null }">
<div class="alert alert-info">
请输入新密码--
</div>
</c:when>
<c:when test="${mag ne null && mag ne '1'}">
<div class="alert alert-danger">
${mag }
</div>
</c:when>
<c:when test="${mag == '1'}">
<script type="text/javascript">

alert("修改成功！");
window.location.href="./student/home.do";
</script>


</c:when>
</c:choose>

<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <form id="condition" action="./student/upTeaPwd.do" role="form" class="form-horizontal" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="o">
                        原密码 </label>

                    <div class="col-sm-5">
                        <input type="password" id="o" placeholder="原密码" name="o" class="form-control">

                    </div>
                    <div id="tip1" class="col-sm-5"></div>
                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label" for="n">
                        请输入新密码 </label>

                    <div class="col-sm-5">
                        <input type="password" placeholder="请输入新的密码" name="n"
                               id="n" class="form-control">
                    </div>
                    <div id="tip2" class="col-sm-5"></div>
                </div>

                <div class="form-group">

                    <label class="col-sm-2 control-label" for="re">
                        请再次输入密码 </label>

                    <div class="col-sm-5">
                        <input type="password" placeholder="请再次输入新的密码" name="re"
                               id="re" class="form-control">
                    </div>
                    <div id="tip3" class="col-sm-5"></div>
                </div>
                <div class="from-group">
                    <label class="col-sm-2 control-label"></label>

                    <div class="col-sm-2" style="text-align:right;">
                        <button type="submit" class="btn btn-blue btn-block" onclick="queren();"><i class="clip-checkmark-2"></i>  确认</button>
                    </div>
                    <div class="col-sm-2" style="text-align:right;">
                        <button type="reset" class="btn btn-blue btn-block" onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i>  返回
                        </button>
                    </div>
                    <div class="col-sm-3" style="text-align: center" id="tip4"></div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- end: TABLE WITH IMAGES PANEL -->

</body>
</html>
