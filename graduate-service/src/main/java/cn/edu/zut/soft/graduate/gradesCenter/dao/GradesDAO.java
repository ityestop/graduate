package cn.edu.zut.soft.graduate.gradesCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Grade;
import org.springframework.stereotype.Component;

/**
 * Created by chuchuang on 16/10/21.
 */
@Component
public interface GradesDAO<T extends Grade> extends BasicDAO<Integer,T> {

}
