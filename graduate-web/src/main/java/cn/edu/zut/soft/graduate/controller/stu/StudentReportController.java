package cn.edu.zut.soft.graduate.controller.stu;

import cn.edu.zut.soft.graduate.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cc
 * @Date 2017/2/19
 * @value 1.0
 */
@RequestMapping("/stu/report")
@Controller
public class StudentReportController extends BaseController {

    @RequestMapping({"listData",""})
    public String listRule(){
        return "/stu/report/reportDataList";
    }

    @RequestMapping("addData")
    public String addRule(){
        return "/stu/report/reportDataAdd";
    }

    @RequestMapping("editData")
    public String editRule(){
        return "/stu/report/reportDataEdit";
    }

    @RequestMapping("getData")
    public String getRule(){
        return "/stu/report/reportDataDetails";
    }

}
