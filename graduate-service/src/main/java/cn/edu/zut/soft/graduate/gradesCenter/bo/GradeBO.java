package cn.edu.zut.soft.graduate.gradesCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.constant.GradeTimeType;
import cn.edu.zut.soft.graduate.core.design.GradeStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Grade;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.GradeQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;

import java.util.List;
import java.util.Map;

/**
 * <p>文件名称：GradesBO.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/2 下午9:39</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public interface GradeBO extends BasicBO<Grade, GradeQuery> {

    /**
     * 功能描述:保存题目成绩记录
     * @param infoStrategy
     * @param id
     * @param operator
     * @return
     */
    Grade save(GradeStrategy gradeStrategy, Integer id, String operator, Integer time);


    /**
     * 功能描述:重置成绩
     * 说明:当修改成绩时 该条历史数据逻辑删除 重现添加一条新纪录
     * @param infoStrategy
     * @param id
     * @param operator
     * @return
     */
    Grade restGradeRecorde(InfoStrategy infoStrategy, Integer id, String operator);

    /**
     * <p>功能描述:重置2次成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-03-05 22:47:51</p>
     *
     * @param infoStrategy
     * @param id
     * @param operator
     * @return
     */
     Grade restGradeRecorde(InfoStrategy infoStrategy, Integer id, String operator, Integer times);

    /**
     * 功能描述:查询所有的成绩
     * 说明:查询所有的成绩信息,包括开题一辩,开题二辩,中期答辩,最终公开答辩,最终一辩,最终二辩
     * @param loginVO
     * @return
     */
    List<Info> findAllGrade(LoginVO loginVO);

    /**
     * <p>功能描述：保存中期检查成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-04-24 09:46:48</p>
     *
     * @param gradeStrategy
     * @param id
     * @param operator
     * @return
     */
    CommonResult<Grade> saveInterimGrade(GradeStrategy gradeStrategy,  Integer id, String operator);

    /**
     * <p>功能描述：重置中期检查成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-04-24 10:22:42</p>
     *
     * @param gradeStrategy
     * @param id
     * @param operator
     * @return
     */
    CommonResult<Grade> restInterimGrade(GradeStrategy gradeStrategy,  Integer id, String operator);

    /**
     * <p>功能描述：保存系统验收成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-16 12:59:27</p>
     *
     * @param gradeStrategy
     * @param id
     * @param times
     * @param operator
     * @return
     */
    CommonResult<Boolean> saveSystemGrade(GradeStrategy gradeStrategy, Integer id, GradeTimeType times, String operator);

    /**
     * <p>功能描述：重置系统验收成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-16 13:01:33</p>
     *
     * @param gradeStrategy
     * @param id
     * @param times
     * @param operator
     * @return
     */
    CommonResult<Boolean> restSystemGrade(GradeStrategy gradeStrategy, Integer id, GradeTimeType times, String operator);

    /**
     * <p>功能描述：答辩成绩输入   答辩成绩  指导成绩  文档成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-16 13:05:04</p>
     *
     * @param gradeStrategy
     * @param id
     * @param operator
     * @return
     */
    CommonResult<Boolean> saveReplyGrade(GradeStrategy gradeStrategy, Integer id, String operator);

    /**
     * <p>功能描述：重置答辩成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-28 17:31:17</p>
     *
     * @param gradeStrategy
     * @param id
     * @param operator
     * @return
     */
    CommonResult<Boolean> restReplyGrade(GradeStrategy gradeStrategy, Integer id, String operator);
    /**
     * <p>功能描述:校验答辩成绩规则, 并计算最终成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-16 13:07:23</p>
     *
     * @param id
     * @return
     */
    CommonResult<Boolean> checkFinalReplyGrade(Integer id, String operator);

    /**
     * <p>功能描述：管理员端校验成绩规则  不区分分数档</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-09 08:23:23</p>
     *
     * @param id
     * @param operator
     * @return
     */
    CommonResult<Boolean> checkFinalRepolyGradeAdminRole(Integer id, String operator);

    /**
     * <p>功能描述：根据key和ID获取成绩信息</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-07 00:40:00</p>
     *
     * @param key
     * @param id
     * @return
     */
    List<Grade> getGrade(String key, Integer id);

    /**
     * <p>功能描述：清除部分成绩</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-09 08:31:55</p>
     *
     * @param id
     * @param key
     * @param operator
     * @return
     */
    CommonResult<Boolean> clearReplyGrade(Integer id, String key, String operator);


}
