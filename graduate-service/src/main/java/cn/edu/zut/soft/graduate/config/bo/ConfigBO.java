package cn.edu.zut.soft.graduate.config.bo;

import cn.edu.zut.soft.graduate.core.model.Impl.Config;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;

import java.util.List;

/**
 * @author cc
 * @Date 2016/11/24
 * @value 1.0
 */
public interface ConfigBO{

    String findByKey(String key);

    Config getConfigBykey(String key);

    Config updateByKey(LoginVO loginVO,Config config);

    List<Config> findAll();

}
