<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/10/26
  Time: 上午1:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>公告</title>
</head>
<body>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/tea"> 首页 </a></li>
            <li class="active">公告信息</li>
        </ol>
        <div class="page-header">
            <h3>公告信息</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<%--<div class="container">--%>

<div class="row">
    <div class="col-md-8" id="noticeAll">
    </div>
    <div class="col-md-4">
        <aside class="sidebar">
            <div class="input-group">
                <input type="text" id="searchSelf" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-primary searchSelf" type="button">
                    <i class="fa fa-search"></i> 搜索
                </button>
            </span>
            </div>
            <hr/>
            <h4>阅读榜Top20</h4>
            <ul class="nav nav-list blog-categories">
            </ul>
        </aside>
    </div>
</div>
<%--</div>--%>
<script src='/assets/g/js/source.js'></script>
<script type="application/javascript">
    $(function () {

        var $noticeAll = $("#noticeAll");
        var $show = $(".blog-categories");

        function getBlogPosts() {
            return "<div class='blog-posts'> " +
                    "<div class='row'> " +
                    "<div class='col-md-12 content'> " +
                    "<h4> <a href=''></a> </h4> " +
                    "<p> </p> " +
                    "</div> " +
                    "</div> " +
                    "<div class='row'> " +
                    "<div class='col-md-12'> " +
                    "<div class='post-meta'> " +
                    "<span> <i class='fa fa-calendar'></i> </span> " +
                    "<span> <i class='fa fa-user'></i> By <label></label> </span> " +
                    "<a class='btn btn-xs btn-main-color pull-right' href='blog_post.html'> Read more...</a>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
        }

        function searchSelf(data) {
            $.ajax({
                url: '/notice/self',
                dataType: 'json',
                type: 'post',
                data: data,
                success: function (result) {
                    $noticeAll.html("");
                    if (result.success) {
                        $.each(result.data, function (i, node) {
                            var $initSearch = $(getBlogPosts());
                            $($initSearch.find(".content a")).attr("href", "/${sessionScope.user.role}/notice?id=" + node.id).html(node.title);
                            // $(initSearch.find(".content p")).html(node.content);
                            $($initSearch.find(".post-meta span")).first().append(timeStampYYYYMMDDHHMM(node.createTime));
                            $($initSearch.find(".post-meta span label")).append(JSON.parse(node.owner).name);
                            $($initSearch.find(".post-meta a")).attr("href", "/${sessionScope.user.role}/notice?id=" + node.id);
                            $noticeAll.append($initSearch);
                        });
                    } else {
                        alert(result.errorMsg);
                    }
                }
            });

        }

        //默认执行
        searchSelf();

        $.ajax({
            url: '/notice/self',
            dataType: 'json',
            type: 'get',
            data: {
                orderByClause: 'read_num desc'
            },
            success: function (result) {
                $show.html("");
                if (result.success) {
                    $.each(result.data, function (i, node) {
                        var $a = $("<a href=''></a>");
                        $($a.attr("href", "/${sessionScope.user.role}/notice?id=" + node.id)).html(node.title + '(' + node.readNum + ')');
                        $show.append($("<li></li>").append($a));
                    });
                } else {
                    alert(result.errorMsg);
                }
            }
        });

        $(".searchSelf").click(function () {
            searchSelf({title: $("#searchSelf").val()});
        });

    });
</script>
</body>
</html>
