<jsp:useBean id="power" scope="request" type="cn.edu.zut.graduate.entity.Power"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 15/11/7
  Time: 21:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">教师信息修改</li>
        </ol>
        <div class="page-header">
            <h3>教师信息
                <small>请填写教师详细信息</small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <div class="row">
                <form class="form-horizontal" action="./${sessionScope.pathCode}/submitPower.do" method="post">
                    <input type="hidden" value="${power.id}" name="id">

                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">URL</label>

                        <div class="col-sm-8">
                            <input placeholder="url" class="form-control" id="teaTeacherId" readonly="readonly" value="${power.powerUrl}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">权限名称</label>

                        <div class="col-sm-8">
                            <input placeholder="权限名称" class="form-control" id="powerName"
                                   name="powerName" value="${power.powerName}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">权限级别</label>

                        <div class="col-sm-8">
                            <label>
                                <input type="checkbox" value="1" name="com" <c:if
                                    test="${power.common == 1 || power.common == 3 || power.common == 7}">
                                checked="checked"</c:if> />
                                管理员、
                            </label>
                            <label>
                                <input type="checkbox" value="2" name="com" <c:if
                                    test="${power.common == 2 || power.common == 3 || power.common == 7}">
                                checked="checked"</c:if> />
                                教师、
                            </label>
                            <label>
                                <input type="checkbox" value="4" name="com" <c:if
                                    test="${power.common == 4 || power.common == 5 || power.common == 7}">
                                checked="checked"</c:if> />
                                学生
                            </label>
                        </div>
                    </div>
                    <div class="from-group">
                        <div class="col-sm-2 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary btn-block">
                                保存
                            </button>
                        </div>

                    </div>
                </form>

            </div>
        </div>
        <!-- end: GENERAL PANEL -->
    </div>
</div>
</body>
</html>
