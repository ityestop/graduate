<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>小组信息管理</title>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <script src="/assets/g/js/stu/stuGroup.js"></script>
</head>

<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.user.role}"> 首页 </a>
            </li>
            <li class="active">小组信息</li>
        </ol>
        <div class="page-header">
            <h4>小组信息<small><font color="red" id="company_status_cont"></font></small>&nbsp;
            <small>指导教师：${sessionScope.TeaguideName}</small>&nbsp;
            <small>课题名称：<a href="stu/issueInfo?id=${sessionScope.TOPICtopicId}">${sessionScope.TOPICtopicName}</a></small>&nbsp;
            <small>子任务：${sessionScope.TOPICchildTopicName}</small>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <input type="hidden" id="group_ID" value="${sessionScope.Groupgroup}">
            <input type="hidden" id="group_Leader" value="${sessionScope.GroupgroupLeader}">
            <input type="hidden" id="identyId" value="${sessionScope.user.id}">
            <input type="hidden" id="topicId" value="${sessionScope.TOPICtopicId}">
            <div class="form-group" id="group_btnes">
            </div>
            <div class="form-group">
                <table class="table table-striped table-hover" id="group_stu_info">
                </table>
            </div>
        </div>
            <div id="chooseInfo" class="panel-body">
                <div class="page-header">
                    <h4>
                        <strong>选报详情</strong><small><font color="red">公司项目申请填写完毕后请联系指导教师审阅提交,校内题目请自动忽略!</font></small>

                    </h4>
                </div>

                <div class="form-group">
                    <table class="table table-striped table-hover" id="group_issue_info">
                        <thead>
                        <tr>
                            <th><small>课题名称</small></th>
                            <th><small>课题类型</small></th>
                            <th><small>教师</small></th>
                            <th><small>申请时间</small></th>
                            <th><small>状态</small></th>
                            <th><small>操作</small></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
    </div>
</div>

<div class="modal fade" id="companyReason" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="alterModalLabel">申请原因</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">申请:</label>
                        <div class="col-sm-8">
                                    <textarea name="reason" id="reason"
                                              style="width:100%;height:220px;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="company_reason_submit">提交</button>

                <button type="button" class="btn btn-default" data-dismiss="modal" >关闭
                </button>

            </div>

        </div>
    </div>
</div>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript" src="assets/g/js/util/infoListTransform.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">
    var columns = [
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '性别',
            formatter: function (value, row, index) {
                var str = '';
                if (row.identity.sex == -1) {
                    str = '女';
                } else if (row.identity.sex == 1) {
                    str = '男'
                }
                return str;
            }
        }, {
            title: '班级',
            field: 'infoList',
            formatter: function (value, row, index) {
                return infoResult(value,'Basic','clazz');
            }
        }, {
            field: 'identity.phone',
            title: '联系方式'
        }, {
            field: 'infoList',
            title: '任务分工',
            formatter: function (value, row, index) {
                return infoResult(value,'TOPIC','childTopicName');
            }
        }
    ];
    customSearch({
        id: "#group_stu_info",
        url: "/stu/groupInfo",
        dataType: "json",
        isParams: true,
        height: 150,
        pagination:false,
        columns: columns
    });
</script>
</body>
</html>
