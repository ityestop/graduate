package cn.edu.zut.soft.graduate.model;

/**
 * <p>文件名称：HChildTopic.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/29 下午2:44</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HChildTopic {

    public String name;

    public String content;

    public HChildTopic() {
    }

    public HChildTopic(String name, String context) {
        this.name = name;
        this.content = context;
    }

    public String getName() {
        return name;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }
}
