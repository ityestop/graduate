package cn.edu.zut.soft.graduate.controller.util;

import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.design.ProfessionalLever;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/24
 */
public class IdentityUtil {
    //优化掉
    public static List<InfoStrategy> findByRole(Role role) {
        List<InfoStrategy> infoStrategyList = new ArrayList<>();
        if (Role.stu == role) {
            infoStrategyList.addAll(IKEY.StuInfoAll);
        } else {
            //检索职称,无关value
            infoStrategyList.add(ProfessionalLever.DEFAULT);
        }
        return infoStrategyList;
    }
}
