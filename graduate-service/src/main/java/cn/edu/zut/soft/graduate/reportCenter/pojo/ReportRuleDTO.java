package cn.edu.zut.soft.graduate.reportCenter.pojo;

import java.util.Date;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportRuleDTO {

    Integer getId();

    /**
     * 获取当前的报告类型
     * @return 1为周报,2为月报 默认为0 忽略
     */
    Integer getType();

    /**
     * 规则开始的时间
     * @return 起始时间
     */
    Date getStartTime();

    /**
     * 规则结束时间
     * @return 截止时间
     */
    Date getEndTime();

    /**
     * 节次
     * @return 周报的周次,月报的月次
     */
    Integer getNum();

    /**
     * 是否可以重复
     * @return 默认为0,不可以重复
     */
    Integer getRepeat();

    /**
     * 最后的修改时间
     * @return 修改时间
     */
    Date getUpdateTime();

    /**
     * 最后的修改者
     * @return 修改者
     */
    String getUpdateAuthor();
}
