package cn.edu.zut.soft.graduate.groupCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Invite;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import lombok.Data;

/**
 * @author chuchuang
 * @date 16/11/29
 */
@Data
public class InviteInfo {
    private LoginVO loginVO;
    private Invite invite;
    private Group group;
    private Identity passives;

    public InviteInfo addLoginVO(LoginVO loginVO){
        this.loginVO = loginVO;
        return this;
    }

    public InviteInfo addInviteWithId(Integer id){
        this.invite = new Invite();
        invite.setId(id);
        return this;
    }

    public InviteInfo addGroupWithId(Integer id){
        this.group = new Group();
        group.setId(id);
        return this;
    }

    public InviteInfo addPassivesWithId(Integer id){
        this.passives = new Identity();
        passives.setId(id);
        return this;
    }
}
