package cn.edu.zut.soft.graduate.core.constant;

/**
 * <p>文件名称：GradeType.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/2 下午9:54</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public enum GradeType implements InfoTypeAble {
    /**
     * Open 开题
     * Interim 中期
     * WeekReport 周月报
     * SystemAcceptance 系统验收
     * Final 最终成绩
     */
    Open,Interim,WeekReport,SystemAcceptance,Final,stop
}
