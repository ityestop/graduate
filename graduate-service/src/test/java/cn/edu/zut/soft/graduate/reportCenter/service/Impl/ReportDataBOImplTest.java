package cn.edu.zut.soft.graduate.reportCenter.service.Impl;

import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportData;
import cn.edu.zut.soft.graduate.reportCenter.query.Impl.SimpleReportDataQuery;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportDataBO;
import org.junit.Test;
import test.BaseTest;

import javax.annotation.Resource;

/**
 * Created by cc on 2017/2/11.
 */
public class ReportDataBOImplTest extends BaseTest {

    @Resource
    private ReportDataBO reportDataBO;

    @Test
    public void text() throws Exception {
        ReportData reportData = new ReportData();
        reportData.setContext("nihao");
        reportData.setHashContext(reportData.getContext().hashCode());
        reportData.setIdentityId(1);
        reportData.setIdentityName("123");
        reportData.setIdentityRole(Role.stu.name());
        reportData.setSupplement(1);
        System.out.println("save: ======>" + reportDataBO.save(reportData,"chuchuang"));
        ReportData two = reportDataBO.get(reportData.getId());
        System.out.println("get: =====>" + two);
        two.setContext(two.getContext()+"2");
        two.setHashContext(two.getContext().hashCode());
        Thread.sleep(1000);
        System.out.println("update: =====>" + reportDataBO.update(two,"chuchuang"));
        System.out.println("list: =====>" + reportDataBO.countByQuery(new SimpleReportDataQuery()));
        System.out.println("delete: =====>" + reportDataBO.delete(two.getId(),"chuchuang"));
    }

}