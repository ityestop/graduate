

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<style type="text/css">
			body{
				font-size:14;
			}	
		</style>
		
	</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
				</li>
			</ol>
			<div class="page-header">
				<h2>
					软件学院毕业设计管理平台 <small>毕业设计开发小组 </small>
				</h2>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<h4><strong>软件学院 2016届课题申报教师端操作注意事项</strong></h4>
							<h1 style="color:red">
								本系统以一个大标题为单位录入，子标题在大标题内部拆分 
							</h1>
							<h4 style="color:red">
							1、一个题目有多个子标题，也在同一个页面完成申报，当选择了人数后，会自动给出对应个数的子标题设置框
							</h4>
							<h4 style="color:red">
							2、如果题目没有子标题，只有一人，可以不填写子标题，但是需要填写具体任务分工，不要将任务分工写在题目背景中
							</h4>
							<p>一、首次登录系统时必须填写联系方式，若无联系方式将不能执行任何操作！</p>
							<p><img style="height: 300px ;width: 900px;" src="./img/3601.png"></img></p>
							
							<p>二、本次申报课题将题目细分化,每个申报项目标题为总标题。总标题下细分为子标题,子标题内功能和任务将和所选报小组内学生绑定，所以请教师将项目具体细分。</p>
							<p><img style="height: 300px ;width: 900px;" src="./img/src1.png"></img>	</p>
							<p></p>
								<p><img style="height:500px ;width: 900px;" src="./img/src2.jpg"></img></p>
						
							<p>三、本系统申报流程，先填写项目信息后，点击保存是将数据保存并未提交，选题维护中有可以对已保存题目执行编辑和提交操作，提交后将不能修改，若管理员拒绝可以再次进行修改提交。</p>
							<p><img style="height:300px ;width: 900px;" src="./img/src3.png"></img></p>
							<p>未提交：未提交状态的题目只有教师本人可以查看，编辑，删除</p>
								<p>审核中：教师点击提交后，题目会提交给管理员审批，提交后，待审核的题目将不可做任何操作。</p>
								<p>选报中：管理员审核通过的题目，将会展现在学生端，等待学生选报，此时也不允许做任何操作。</p>
								<p>未通过：未通过状态的题目是指，题目审批表有部分不足需要修改，管理员会将修改内容记录在题目详细信息中。</p>
								<p><img style="height:300px ;width: 900px;" src="./img/src4.png"></img></p>
								<p><img style="height:300px ;width: 900px;" src="./img/src5.png"></img></p>
							<p>四、系统暂时提供教师端题目申报和管理的功能，其他功能尚未开放，各功能会陆续上线，请包涵。如若遇到任何问题请及时联系管理员，如有使用系统时出现问题，请多多包涵，我们会及时修订。</p>
						</div>
					</div>
				</div>
			</div>
			<!-- end: GENERAL PANEL -->
		</div>
	</div>


</body>
</html>
