$(function() {
    var dataFrom = {};
    var $error = $(".errorHandler");
    $("input[name='p']").change(function () {
        $error.hide();
    });
    $("input[name='u']").change(function () {
        $error.hide();
    });

    $("button[name='sub']").click(function () {
        $.ajax({
            url:"/login",
            type:"POST",
            dataType:"json",
            data:{
                u:$("input[name='u']").val(),
                p:$("input[name='p']").val()
            },
            success: function(data){
                if(data.success){
                    window.location.href='/'+data.data.role;
                }else{
                    $($error.find("span")).html(data.errorMsg);
                    $error.show();
                }
            },
            error:function (result) {
                $($error.find("span")).html(result);
                $error.show();
            }
        });
    });
});