/**
 * Created by chuchuang on 16/11/23.
 */

document.write("<script src='/assets/g/js/config.js'></script>");

function respwd (id){
    $.ajax({
        url: "/identity/resetPwd/" + id,
        type: "put",
        dataType: "json",
        success: function (result) {
            if (result.success) {
                alert("重置成功,密码为用户手机号");
            } else {
                alert(result.errorMsg);
            }
        }
    });
}

function customSearch(opt) {
    var oTableInit = {};
    //初始化Table
    oTableInit.Init = function () {
        $(opt.id).bootstrapTable({
            url: '/identity/list/tea',  //请求后台的URL（*）
            classes: 'table-no-bordered',
            method: 'get',   //请求方式（*）
            pagination: true,   //是否显示分页（*）
            sidePagination: "server",
            pageNumber: 1,   //初始化加载第一页，默认第一页
            pageSize: 10,   //每页的记录行数（*）
            pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
            search: false,
            queryParams: oTableInit.queryParams,
            dataType: "json",
            columns: [{
                checkbox: true
            }, {
                field: 'identity.noId',
                title: '教师工号'
            }, {
                field: 'identity.name',
                title: '教师姓名'
            }, {
                field: 'identity.sex',
                title: '教师性别',
                formatter: function (value, row, index) {
                    return value == "-1" ? "女" : (value == "1" ? "男" : "未知");
                }
            }, {
                field: 'infoList',
                title: '教师身份',
                formatter: function (value,row,index) {
                    var res = {};
                    $.each(value,function (index,item) {
                        res[item.key] = item.value;
                    });
                    return sourceMaps().professionalLeverMap[res["professionalLever"]];
                }
            }, {
                field: 'identity.phone',
                title: '联系方式'
            },{
                title:"操作",
                formatter:oTableInit.operateFormatter,
                events:operate
            }
            ],
            responseHandler: oTableInit.responseHandler,
            customSearch:customSearch,
            searchOnEnterKey:true,
            // showRefresh:true
        })
    };
    oTableInit.queryParams = function (params) {
        return { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            limit: params.limit, //页面大小
            offset: params.offset, //页码
            name:$("input[name='name']").val()
        };
    };
    oTableInit.responseHandler = function (res) {
        return {
            total: res.totalCount,
            fixedScroll: true,
            rows: res.data
        };
    };
    oTableInit.operateFormatter = function (value, row, index) {
        return ["<a href='javascript:respwd("+row.identity.id+")'>重置密码</a>"].join("");
    };

    var operate=function (event, value, row, index) {

    };

    oTableInit.Init();
}

$(function () {

    customSearch({id:'#teacherTable'});


});
