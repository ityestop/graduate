package cn.edu.zut.soft.graduate.core.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>文件名称：DataUtilsTools.java</p>
 * <p>文件描述：数据模型转换工具</p>
 *
 * @author yangxiaotian
 * @version 1.0
 */
public class DataTypeUtil {

    /**
     * k-v转string
     * @param featureMap
     * @return
     */
//    public static String  getFeature(Map<String, String> featureMap){
//        if (CollectionUtils.isEmpty(featureMap)){
//            return  null;
//        }
//        return JSON.toJSONString(featureMap);
//    }

    /**
     * string 转k-v <String, String>
     * @param features
     * @return
     */

    public static Map<String, String> getFeatureMap(String features){
        Map<String, String> featureMap = new HashMap<>();
        if (StringUtils.isEmpty(features)){
            return null;
        }
        JSONObject json = JSON.parseObject(features);
        for (Map.Entry<String, Object> entry: json.entrySet()){
            featureMap.put(entry.getKey(), entry.getValue().toString());
        }
        return  featureMap;
    }

    /**
     * k-v Map<String, Map<String, String>> 转 String
     * @param featuresMap
     * @return
     */
//    public static String  getFeatureByMap(Map<String, Map<String, String>> featuresMap){
//        if (CollectionUtils.isEmpty(featuresMap)){
//            return  null;
//        }
//        return JSON.toJSONString(featuresMap);
//    }

    /**
     * String 转 k-v Map<String, Map<String, String>>
     * @param features
     * @return
     */
    public static Map<String, Map> getFeatureForMaps(String features){
        Map<String, Map> map = new HashMap<>();
        if (StringUtils.isEmpty(features)){
            return null;
        }
        JSONObject json = JSONObject.parseObject(features);
        for (Map.Entry<String, Object> entry : json.entrySet()){
            map.put(entry.getKey(), (Map) entry.getValue());
        }
        return map;
    }


}
