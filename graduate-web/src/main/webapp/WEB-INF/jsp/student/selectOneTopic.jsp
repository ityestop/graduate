<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>题目详情</title>
    <script type="text/javascript" src="IAssets/js/selectTopic.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">题目详细信息</li>
        </ol>
        <div class="page-header">
            <h3>题目详细信息</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table class="table">
                <tbody>
                <tr>
                    <td width="10%">
                        <small>题目标题:</small>
                    </td>
                    <td>
                        <small>${topicInfo.topTitle}</small>
                    </td>
                </tr>
                <c:choose>
                        <c:when test="${topicInfo.topStatus eq 3 }">
                            <tr class="danger">
                                <td>
                                    <small>管理员批语</small>
                                </td>
                                <td>
                                    <small><font color="red">${topicInfo.topRemark}</font>
                                    </small>
                                </td>
                            </tr>
                        </c:when>
                    </c:choose>
                <c:if test="${topicInfo.topTeacherId != 'mytitleTeacher'}">
                    <tr>
                    <td>
                        <small>指导教师:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.teacher.teaName}(<font color="red"> 联系电话：${topicInfo.teacher.teaTell}</font>)
                        </small>
                    </td>
                </tr>
                 </c:if>
                 <c:if test="${topicInfo.topOutTeacher ne null}">
                    <tr>
                    <td>
                        <small>校外教师:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.topOutTeacher}
                        </small>
                    </td>
                </tr>
                 </c:if>
                <tr>
                    <td>
                        <small>课题类型:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.topType}</small>
                    </td>
                </tr>
                  
                <tr>
                    <td>
                        <small>课题来源:</small>
                    </td>
                    <td>
                        <c:if test="${topicInfo.topTopicType eq false }">
                            <small>社会服务</small>
                        </c:if>
                        <c:if test="${topicInfo.topTopicType eq true }">
                            <small>科研项目</small>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <small>课题人数:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.topCount}</small>
                    </td>
                </tr>
                <tr>
                    <td>
                        <small>技术领域:</small>
                    </td>
                    <td>
                        <c:forEach items="${topicInfo.topTechnophobeList}" var="t">
                            <small> ${t.techName}、</small>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td>项目背景:</td>
                    <td>
                        <small>
                            <pre>${topicInfo.topContent}</pre>
                        </small>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <c:forEach items="${topicInfo.childTopicList}" var="c">
                            <strong>
                                <small> ${c.name}</small>
                            </strong>
                            <small>
                                <pre>${c.content}</pre>
                            </small>
                        </c:forEach>
                    </td>
                </tr>
                </tbody>
            </table>
            <c:if test="${SGTLink != null && SGTLink.size() > 0}">
                <table width="100%" class="table   table-hover">
                    <thead>
                    <tr>
                        <th>
                            <small>小组编号</small>
                        </th>
                        <th>
                            <small>小组成员</small>
                        </th>
                        <th>
                            <small>操作</small>
                        </th>
                    </tr>
                    </thead>
                    <c:forEach items="${SGTLink}" var="li">
                        <tbody>
                        <tr>
                            <td>
                                <small>${li.choStuGroup.head.stuName}的小组</small>
                            </td>
                            <td>
                                <ul>
                                    <c:forEach items="${li.choStuGroup.studentList}" var="lis">
                                        <li>
                                            <small>${lis.stuName} (<font color="red"> 电话：${lis.stuTell}</font>)----
                                                    ${lis.stuStudentId} ---- ${lis.stuClass}</small>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                            <td>
                                <small>
                                    <c:choose>
                                        <c:when test="${li.chooStatus == 1}">
                                            同意选报
                                        </c:when>
                                        <c:when test="${li.chooStatus == 2}">
                                            拒绝选报
                                        </c:when>
                                        <c:otherwise>
                                            审核中
                                        </c:otherwise>
                                    </c:choose>
                                </small>
                            </td>
                        </tr>
                        </tbody>
                    </c:forEach>
                </table>
            </c:if>
        </div>
         <div class="form-group">
                <c:choose> 
                    <c:when test="${topicInfo.topStatus eq 2 and headMan eq true and sgIdCount eq topicInfo.topCount and sta  le 0 and sgCount le 0 }">
                        <label class="col-sm-8 control-label"></label>

                        <div class="col-sm-2">
                            <a class="btn btn-blue btn-block"
                               href="./${sessionScope.pathCode}/submitSGTL.do?topId=${topicInfo.topId}">提交申报</a>
                        </div>
                        <c:if test="${sessionScope.user.stuTeacherId eq null }">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/topicList.do?teacherId=${ topicInfo.topTeacherId}'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                         <c:if test="${sessionScope.user.stuTeacherId ne null}">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                    </c:when>
                     <c:when test="${topicInfo.topStatus eq 2 and headMan eq true and sgIdCount eq topicInfo.topCount and sta ge 0 and sgCount gt 0}">
                        <label class="col-sm-8 control-label"></label>

                        <div class="col-sm-2">
                             <button type="button" class="btn btn-blue btn-block"
                                    data-toggle="modal" data-target="#sta"> 提交申报
                            </button>
                        </div>
                        <c:if test="${sessionScope.user.stuTeacherId eq null}">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/topicList.do?teacherId=${ topicInfo.topTeacherId}'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                         <c:if test="${sessionScope.user.stuTeacherId ne null}">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                    </c:when>
                    <c:when test="${topicInfo.topStatus eq 2 and headMan eq false and sgIdCount eq topicInfo.topCount}">
                        <label class="col-sm-8 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    data-toggle="modal" data-target="#NoheadMan"> 提交申报
                            </button>
                        </div>
                        <c:if test="${sessionScope.user.stuTeacherId eq null }">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/topicList.do?teacherId=${ topicInfo.topTeacherId}'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                         <c:if test="${sessionScope.user.stuTeacherId ne null}">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                    </c:when>
                    <c:when test="${topicInfo.topStatus eq 2 and headMan eq true and sgIdCount ne topicInfo.topCount}">
                        <label class="col-sm-8 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    data-toggle="modal" data-target="#Yesheadman"> 提交申报
                            </button>
                        </div>
                        <c:if test="${sessionScope.user.stuTeacherId eq null }">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/topicList.do?teacherId=${ topicInfo.topTeacherId}'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                         <c:if test="${sessionScope.user.stuTeacherId ne null}">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                    </c:when>
                    <c:when test="${topicInfo.topStatus eq 2}">
                        <label class="col-sm-8 control-label"></label>
						 <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    data-toggle="modal" data-target="#prompt"> 提交申报
                            </button>
                        </div>
                         <c:if test="${sessionScope.user.stuTeacherId eq null  }">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/topicList.do?teacherId=${ topicInfo.topTeacherId}'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                         <c:if test="${sessionScope.user.stuTeacherId ne null}">
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                        </c:if>
                    </c:when>
                    <c:when test="${topicInfo.topStatus eq 0}">
                        <label class="col-sm-10 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                    </c:when>
                     <c:when test="${topicInfo.topStatus eq 4}">
                        <label class="col-sm-10 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                    </c:when>
                    <c:when test="${topicInfo.topStatus eq 3}">
                        <label class="col-sm-10 control-label"></label>

                        <div class="col-sm-2">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                    </c:when>
                </c:choose>
                </from>
            </div>
    </div>
</div>
<div class="modal fade" id="NoheadMan" tabindex="-1" role="dialog" aria-labelledby="CSGModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLabel">权限问题</h4>
            </div>
            <div class="modal-body">
                sorry,您不是组长,没有权限选报！
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="Yesheadman" tabindex="-1" role="dialog" aria-labelledby="CSGModalLabe">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLabe">人数问题</h4>
            </div>
            <div class="modal-body">
                sorry,您小组人数与课题人数不匹配,没有权限申请此题目！
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="sta" tabindex="-1" role="dialog" aria-labelledby="CSGModalLab">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLab">权限问题</h4>
            </div>
            <div class="modal-body">
                sorry,您存审核记录或已通过审核记录.不能提出申请!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="prompt" tabindex="-1" role="dialog" aria-labelledby="CSGModalLa">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLa">提示信息</h4>
            </div>
            <div class="modal-body">
                	您现在还未参加或创建小组，未具有选报权限。<br/>
                	<br/><br/>
                	提示选报流程：创建小组(线下商量xxx为小组长)-> 邀请小组成员  -> 选报校内题目/添加自拟题目(组长拥有权限创建) -> 等待教师审批 -> 通过(选报成功)/失败(可以重新选报)<br/>
                	<br/><br/>
                	提示小组流程：小组在拥有题目为审核中或选报成功状态下，不可进行添加新成员和解散小组操作。
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
