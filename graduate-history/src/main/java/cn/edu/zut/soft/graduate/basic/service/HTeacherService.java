package cn.edu.zut.soft.graduate.basic.service;


import cn.edu.zut.soft.graduate.model.HTeacher;

import java.util.List;

/**
 * <p>文件名称：HTeacherService.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/29 上午12:33</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public interface HTeacherService {

    public Integer save(HTeacher hTeacher);

    public Integer update(HTeacher hTeacher);

    public boolean delete(Integer id);

    public Integer count();

    public List<HTeacher> findAll();
}
