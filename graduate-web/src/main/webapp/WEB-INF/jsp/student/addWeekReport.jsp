<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>学生提交周月报</title>
<%--<script charset="utf-8" src="assets/plugins/kindeditor/kindeditor.js"></script>--%>
<%--<script charset="utf-8" src="assets/plugins/kindeditor/lang/zh_CN.js"></script>--%>
<%--<script charset="utf-8" src="IAssets/js/toNewTopic.js"></script>--%>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">编辑周月报</li>
			</ol>
			<div class="page-header">
				<h3>
					编辑周月报 <small>请填写周月报详细信息 </small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>

	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<form class="form-horizontal"
					action="./${sessionScope.pathCode}/saveWeekRepport.do"
					method="post">
					<div class="row">
							<div class="form-group">
							<input type="hidden" name="id" value="${report.id}"> <input
								type="hidden" name="type" value="${report.type }"> <label
								class="col-sm-2 control-label" for="topicName"> 题目名称 </label>
							<div class="col-sm-7">
								<c:choose>
									<c:when test="${report.type eq 1 and report.title eq null }">
										<input type="text" disabled="disabled" class="form-control"
											value="${topic.topTitle}-${childName}-第${weekCount}周">
										<input name="title" type="hidden"
											value="${topic.topTitle}-${childName}-第${weekCount}周">
									</c:when>
									
									<c:when test="${report.type eq 1 and report.title ne null}">
										<input type="text" disabled="disabled" class="form-control"
											value="${report.title}">
										<input name="title" type="hidden"
											value="${report.title}">
									</c:when>
									
									<c:when test="${report.type eq 2 and report.title eq null }">
										<input type="text" disabled="disabled" class="form-control"
											value="${topic.topTitle}-${childName}-${month}月">
										<input name="title" type="hidden"
											value="${topic.topTitle}-${childName}-${month}月">
									</c:when>
									
									<c:when test="${report.type eq 2 and report.title ne null}">
										<input type="text" disabled="disabled" class="form-control"
											value="${report.title}">
										<input name="title" type="hidden"
											value="${report.title}">
									</c:when>
								</c:choose>

							</div>
						</div>
						<!-- 
						<div class="form-group">
							<label class="col-sm-2 control-label" for="form-field-select-3">
								类型</label>

							<div class="col-sm-7">
								<select name="type" id="form-field-select-3"
									class="form-control search-select">
									<option value="1"
									<c:if test="${report.type eq 1 }">
										 selected="selected"</c:if> >周报
									</option>
									<option value="2" <c:if test="${report.type eq 2 }">
										 selected="selected"</c:if> >月报
									</option>
								</select>
							</div>
						</div>
						 -->
					</div>
					<div class="page-header">
						<h3>工作进度</h3>
					</div>
					<div class="form-group">
						<c:if test="${report.type eq 1 }">
						<label class="col-sm-2 control-label"> 本周进度 </label>
						</c:if>
						<c:if test="${report.type eq 2 }">
						<label class="col-sm-2 control-label"> 本月进度 </label>
						</c:if>
						<div class="col-sm-7">
							<textarea name='nowContext' style="height: 150px;"
								placeholder=" 请详细写出本周/月工作进度" class="form-control"
								value="${report.nowContext }">${report.nowContext}</textarea>

						</div>
					</div>
					<div class='form-group'>
						<c:if test="${report.type eq 1 }">
						<label class="col-sm-2 control-label"> 下周任务计划 </label>
						</c:if>
						<c:if test="${report.type eq 2 }">
						<label class="col-sm-2 control-label"> 下月任务计划 </label>
						</c:if>
						<div class='col-sm-7'>
							<textarea name='nextContext' style="height: 150px;"
								placeholder="请写出下周/月工作计划" class="form-control"
								value="${report.nextContext}">${report.nextContext}</textarea>
						</div>
					</div>

					<div class="from-group">
						<label class="col-sm-3 control-label"></label>
						<div class="col-sm-4">
							<div class="col-sm-6">
								<button type="submit" class="btn btn-blue btn-block">
									<i class="clip-checkmark-2  "></i> 保存
								</button>
							</div>
							<div class="col-sm-6">
								<button type="button" class="btn btn-blue btn-block"
									onclick="javascript:history.go(-1);">
									<i class="clip-arrow-right-2 "></i> 返回
								</button>
							</div>
						</div>
					</div>
			</div>
			</form>

		</div>
		<!-- end: GENERAL PANEL -->
	</div>
	</div>
	</div>

	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			FormElements.init();
		});
	</script>
</body>
</html>
