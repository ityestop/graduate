package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.*;
import cn.edu.zut.soft.graduate.reportCenter.query.Impl.SimpleReportDataQuery;
import cn.edu.zut.soft.graduate.reportCenter.query.Impl.SimpleReportRuleQuery;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportCenterService;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author cc
 * @Date 2017/2/19
 * @value 1.0
 */
@Controller
@RequestMapping("report")
public class ReportController extends BaseController {

    private final static long oneDataTime = 24*3600*1000 - 1000;

    @Autowired
    private ReportCenterService reportCenterService;

    @RequestMapping(value = "/rule", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult addReportRule(ReportRule reportRule, HttpSession session) {
        if (session.getAttribute(IKEY.BASIC_ADMIN) == null) {
            return null;
        }
        reportRule.setEndTime(new Date(reportRule.getEndTime().getTime()+oneDataTime));
        reportCenterService.addReportRule(reportRule, (LoginVO) session.getAttribute(IKEY.USER));
        return CommonResult.successReturn(reportRule);
    }

    @RequestMapping(value = "/rule/delete", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult updateReportRule(String id, HttpSession session) {
        return CommonResult.successReturn(reportCenterService.removeReportRule(Integer.parseInt(id), (LoginVO) session.getAttribute(IKEY.USER)));
    }

    @RequestMapping(value = "/rule", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getReportRule(String id, HttpSession session) {
        return CommonResult.successReturn(reportCenterService.getRule(Integer.parseInt(id)));
    }

    @RequestMapping(value = "/rule/list", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult listReportRule(SimpleReportRuleQuery reportRuleQuery) {
        List queryResult = reportCenterService.listRule(reportRuleQuery);
        return CommonResult.successReturn(queryResult);
    }

    @RequestMapping(value = "/rule/conform", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult conformRule(HttpSession session,boolean supplement) {
        LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
        Date date = null;
        if (!supplement){
            date = new Date();
        }
        List<ReportRule> list = reportCenterService.findRuleByDateOfLoginVO(date, loginVO);
        if ( supplement && list.isEmpty()) {
            return CommonResult.errorReturn("您所有的报告已经提交完毕!");
        }else if ( !supplement && list.isEmpty()){
            return CommonResult.errorReturn("正式提交时间已结束,若有未提交报告请选择补交进行提交!");
        }
        return CommonResult.successReturn(list);
    }

    @RequestMapping(value = "/data", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult addReportData(ReportData reportData, HttpSession session) {
        reportCenterService.addReportData(reportData, (LoginVO) session.getAttribute(IKEY.USER));
        return CommonResult.successReturn(reportData);
    }

    @RequestMapping(value = "/data/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult updateReportData(ReportData reportData, HttpSession session) {
        reportCenterService.updateReportData(reportData, (LoginVO) session.getAttribute(IKEY.USER));
        return CommonResult.successReturn(reportData);
    }

    @RequestMapping(value = "/data", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getReportData(String id, @RequestParam(value = "escape", required = false, defaultValue = "false") boolean escape) {
        ReportDataDTO reportDataDTO = reportCenterService.getReportData(Integer.parseInt(id));
        if (escape) {
            ((ReportData) reportDataDTO).setContext(StringEscapeUtils.unescapeHtml4(reportDataDTO.getContext()));
        }
        ReportRuleDTO reportRuleDTO = reportCenterService.getRule(reportDataDTO.getReportRuleId());
        ReportDataVO reportDataVO = new ReportDataVO(reportDataDTO, reportRuleDTO);
        return CommonResult.successReturn(reportDataVO);
    }

    @RequestMapping(value = "/data/delete",method = RequestMethod.POST)
    @ResponseBody
    public CommonResult deleteReportData(Integer id,HttpSession session){
        if (session.getAttribute(IKEY.BASIC_ADMIN) == null){
            CommonResult.errorReturn("非管理员不可以操作");
        }
        ReportDataDTO reportDataDTO = reportCenterService.removeReportData(id, (LoginVO) session.getAttribute(IKEY.USER));
        if (reportDataDTO == null){
            return CommonResult.errorReturn("当前报告不存在");
        }
        return CommonResult.successReturn(reportDataDTO);
    }


    @RequestMapping("/data/list/{role}")
    @ResponseBody
    public CommonResult listReportData(@RequestParam(value = "escape", required = false, defaultValue = "false") boolean escape, SimpleReportDataQuery reportDataQuery, HttpSession session, @PathVariable("role") String role) {
        if (role.equals("admin") || role.equals(Role.tea.name())) {
            if (role.equals("admin")) {
                if (session.getAttribute(IKEY.BASIC_ADMIN) == null) {
                    return null;
                }
            }else {
                LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
                reportDataQuery.setTeaId(loginVO.getId());
            }
            List<ReportDataSearchVO> list = reportCenterService.statisticsStudentOfQuery(reportDataQuery);
            unEscapeRDContext(escape, list);
            return CommonResult.successReturn(list);
        } else if (role.equals(Role.stu.name())) {
            LoginVO loginVO = (LoginVO) session.getAttribute(IKEY.USER);
            reportDataQuery.setIdentityIds(Collections.singletonList(loginVO.getId()));
            return CommonResult.successReturn(getReportDataVOs(escape, reportDataQuery));
        }
        return null;
    }

    private void unEscapeRDContext(boolean escape, List<ReportDataSearchVO> list) {
        if (!escape) {
            return;
        }
        for (ReportDataSearchVO rdsv : list) {
            rdsv.getReportData().setContext(StringEscapeUtils.unescapeHtml4(rdsv.getReportData().getContext()));
        }
    }


    private List<ReportDataVO> getReportDataVOs(boolean escape, SimpleReportDataQuery reportDataQuery) {
        List<ReportDataDTO> reportDataDTOList = reportCenterService.listReportData(reportDataQuery);
        List<ReportDataVO> reportDataVOList = new ArrayList<>(reportDataDTOList.size());
        for (ReportDataDTO reportDataDTO : reportDataDTOList) {
            if (escape) {
                ((ReportData) reportDataDTO).setContext(StringEscapeUtils.unescapeHtml4(reportDataDTO.getContext()));
            }
            reportDataVOList.add(new ReportDataVO(reportDataDTO, reportCenterService.getRule(reportDataDTO.getReportRuleId())));
        }
        return reportDataVOList;
    }

}
