/**
 * Created by chuchuang on 16/11/24.
 */

function sourceMaps() {
    return {
        issuePhaseMap: {"history":"历史数据","auditSuccessIng":"审核通过","saveIng":"保存中","finish":"选报成功","noBody":"无人选报","auditErrorIng":"审核不通过","chooseIng":"选报中","giveUp":"失效","auditIng":"审核中"},
        issueKindMap: {"Research":"科研项目","Social":"社会服务"},
        issueSourceMap: {"school":"校内课题","myself":"自拟题目"},
        professionalLeverMap: {"PROFESSOR":"教授","ASSISTANT":"助教","LECTURER":"讲师","ASSISTANT_PROFESSOR":"副教授","DEFAULT":"未指定"},
        addressMap:{'beijing':'北京','shanghai':'上海','hangzhou':'杭州','zhengzhou':'郑州'}
    };
}