package cn.edu.zut.soft.graduate.core.model.Impl;


import cn.edu.zut.soft.graduate.core.constant.InviteStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Invite extends BaseModelImpl {

    private String activesName;

    private Integer activesId;

    private String passivesName;

    private Integer passivesId;

    private Integer groupId;

    private InviteStatus status;

}