$(function () {

    var issue = {};
    var sourceMaps = {
        issuePhaseMap: {},
        issueKindMap: {},
        issueSourceMap: {},
    };

    $.ajax({
        url: "/issue/gainIssuePhase",
        dataType: "json",
        cache:true,
        type: "get",
        success: function (result) {
            if (result.success) {
                sourceMaps.issuePhaseMap = result.data;
            } else {
                alert(result.errorMsg);
            }
        }
    });




    var userId_audit = $("#userId_audit").val();
    var userName_audit = $("#userName_audit").val();
    $(function () {
        $.ajax({
            type:'POST',
            url:'./admin/auditIssueList',
            dataType:'json',
            data:{
                role:'tea'
            },
            success:function (result) {
                if(result.success){
                    issue = result.data;
                    issueList();
                }
            }
        });
    });
    
    $("#tea_AuditIssue").click(function () {
        $('#auditIssuseTop').html('');
        $.ajax({
            type:'POST',
            url:'./admin/auditIssueList',
            dataType:'json',
            data:{
                role:'tea'
            },
            success:function (result) {
                if(result.success){
                    issue = result.data;
                    issueList();
                }
            }
        });
    });

    $("#stu_AuditIssue").click(function () {
        $('#auditIssuseTop').html('');
        $.ajax({
            type:'POST',
            url:'./admin/auditIssueList',
            dataType:'json',
            data:{
                role:'stu'
            },
            success:function (result) {
                if(result.success){
                    issue = result.data;
                    issueList();
                }
            }
        });
    });
    function issueList() {
        $.each(issue,function (key,value) {
            var tr_h = $(getTr());
            var issue = value;
            var td_h_title_s = $(getTd());
            var td_h_title = $(getsm());
            var td_h_title_id = $(getId());
            td_h_title.attr('wit')
            td_h_title.attr('title', issue.title);
            td_h_title.attr('style', 'overflow:hidden;white-space:nowrap;text-overflow:ellipsis;');
            td_h_title_id.attr("href","./admin/issue?id="+issue.id);
            td_h_title_id.append(issue.title);
            td_h_title.append(td_h_title_id);
            td_h_title_s.append(td_h_title);
            tr_h.append('');
            tr_h.append(td_h_title_s);

            var td_h_source_sm = $(getTd());
            var td_h_source = $(getsm());
            switch (issue.source) {
                case 'school':
                    td_h_source.append("校内题目");
                    break;
                case 'myself':
                    td_h_source.append("自拟题目");
                    break;
            }
            td_h_source_sm.append(td_h_source);
            tr_h.append(td_h_source_sm);

            var td_h_tea_sm = $(getTd());
            var td_h_tea = $(getsm());
            td_h_tea.append(issue.schoolTeaName);
            td_h_tea_sm.append(td_h_tea);
            tr_h.append(td_h_tea_sm);

            var td_h_status_sm = $(getTd());
            var td_h_status = $(getsm());
            var $label = $("<label></label>");
            $label.addClass("label");
            switch (issue.status) {
                case 'saveIng':
                    $label.addClass('label-info');
                    break;
                case 'auditIng':
                    $label.addClass('label-warning');
                    break;
                case 'auditErrorIng':
                    $label.addClass('label-danger');
                    break;
                case 'giveUp':
                    $label.addClass('label-inverse');
                    break;
                case 'finish':
                    $label.addClass('label-success');
                    break;
                default:
                    $label.addClass('label-default');
            }
            td_h_status.append($label.append(sourceMaps.issuePhaseMap[issue.status]));
            td_h_status_sm.append(td_h_status);
            tr_h.append(td_h_status_sm);

            var  td_h_size_sm = $(getTd());
            var  td_h_size = $(getsm());
            td_h_size.append(issue.taskSize);
            td_h_size_sm.append(td_h_size);
            tr_h.append(td_h_size_sm);

            var td_h_update_sm =$(getTd());
            var td_h_update =$(getsm());
            var td_h_u_id = $(getId());
            td_h_update.append(td_h_u_id);
            td_h_u_id.attr("href","/admin/edit_aduit?userId="+userId_audit+"&issueId="+issue.id+"&userName="+userName_audit);
            td_h_u_id.append("分配-"+userName_audit);
            td_h_update_sm.append(td_h_update);
            tr_h.append(td_h_update_sm);
            $('#auditIssuseTop').append(tr_h);
            tr_h.append('');
        })
    }

    function getTr() {
        return "<tr></tr>";
    }

    function getTd() {
        return "<td></td>";
    }
    function getsm() {
        return "<small></small>";
    }

    function getId() {
        return "<a href='#'></a>"
    }


});