<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>分配答辩小组 </title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <script type="application/javascript">
        $(function () {
            $stuNum = $(".stuNum");
            var stuAllNum = 0;
            for(var i = 0, l = $stuNum.length; i < l; i++ ){
                stuAllNum+=parseInt($($stuNum[i]).html());
//                alert(stuAllNum);
            }
            $(".stuAllNum").html(stuAllNum);
//            alert($($stuNum[0]).html());
        });
    </script>
</head>

<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">分配答辩小组</li>
        </ol>
        <div class="page-header">
            <h4><strong>分配答辩小组 </strong><small>学生总数:</small><small class="stuAllNum"></small>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <div class="col-sm-3">
            <a class="btn btn-primary" href="javaScript:void(0);" data-toggle="modal" data-target="#teaGroup"><i
                    class=" clip-plus-circle "></i> 添加答辩小组</a>
            </div>
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>
                        <small>小组名</small>
                    </th>
                    <th>
                        <small>组员人数</small>
                    </th>
                    <th>
                        <small>学生总数</small>
                    </th>
                    <th>
                        <small>对接答辩小组</small>
                    </th>
                    <th>
                        <small class="center">操作</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${teaGroup}" var="teaGroup">
                    <tr>
                    	<input type="hidden" class="tgID" value="${teaGroup.tgId}">
                        <td>
                            <small class="tgTea">${teaGroup.tgTea }小组</small>
                        </td>
                        <td>
                            <small class="teaNum">${teaGroup.teaNum }</small>
                        </td>
                        <td>
                            <small class="stuNum">${teaGroup.stuNum }</small>
                        </td> 
                        <td>
                            <small>
                            <c:if test="${teaGroup.secretary ne  null}">
                            	 ${teaGroup.secretary}小组
                            </c:if>
                            <c:if test="${teaGroup.secretary eq  null}">
                            	 无
                            </c:if>
                           </small>
                        </td>
                        <td>
                            <small><a href="./${sessionScope.pathCode}/TeacherNoGroup.do?tgId=${teaGroup.tgId}"><i
                                    class="	clip-user-plus"></i> 邀请人员</a>
                                <a href="./${sessionScope.pathCode}/assignIdentity.do?tgId=${teaGroup.tgId}"><i
                                        class="clip-user-5 "></i> 成员身份</a>
                                  <a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#docking"> <i class="clip-expand"></i>  对接答辩小组  </a>
                            </small>
                        
                        </td>

                    </tr>
                </c:forEach>
            </table>

        </div>
    </div>
</div>


<!--创建答辩小组 Model -->
<div class="modal fade" id="teaGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form action="./admin/createTeaGroup.do">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">创建答辩小组--选择小组长</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control search-select" id="teaId"
                            name="teaId">
                        <c:forEach items="${teacherList}" var="teacher">
                            <option value="${teacher.teaId }">${teacher.teaName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>

            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="docking" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
    <form action="./${sessionScope.pathCode}/updateDocking.do"
          class="form-horizontal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">分配对接答辩小组</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="tgId">
                    <div id="dock" class="form-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">确认</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    $('#docking').on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
        var $tgId = $p.find(".tgId");
        var $tgTea=$p.find(".tgTea");
//    	alert($tgId.val() + "________");
        $.ajax({
            url:"./admin/findDockingListName.do",
            type:"post",
            dataType:"json",
            data:{
                tgId: $tgId.val(),
            },
            success:function(data){
//          	alert(data);
                var str = "";
                if (data.status == true){
                    var $tg = data.list;
//                 alert($tg.length);
                    for (var i = 0; i< $tg.length;i++){
                        if ($tg[i].tgId != null || $tg[i].tgId != 'null'){
//                  	alert("123");
                            str += "<div><label> <input type='radio' value='"+$tg[i].tgId+"' name='tgTeagroupId' class='grey' >" + $tg[i].tgTea+"-小组" + "</label>";
 //                       	alert(str);
                        }
                    }

                    if(str == ""){
                        str = "答辩小组已经分配完毕！";
                    }

                }else{
                    str = "查询数据错误";
                }
                $("#dock").html(str);
                $("input[name='tgId']").val($tgId.val());
            }
        });
    });
</script>
<script>
    jQuery(document).ready(function () {
        FormElements.init();

    });

</script>
</body>
</html>
