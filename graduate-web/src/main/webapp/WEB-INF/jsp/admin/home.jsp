<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>软件学院毕业设计管理平台</title>
</head>

<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
        </ol>
        <div class="page-header">
            <h2>
                软件学院毕业设计管理平台
                <small>毕业设计开发小组</small>
            </h2>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER -->
<div class="row">
    <div class="col-md-12">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>软件学院 2016届毕业生选报毕业设计注意事项</h4>
                        <h5>具体操作流程如下：</h5>

                        <p>一、正式选报之前一定要认真查看附件中的所有文档，不要出现不必要的错误，以免给教师增加额外的工作量；</p>

                        <p>二、校内教师题目选报时间：13周周一至周五；</p>

                        <p>三、校外自拟题目申报时间：13周周五至周日，自拟题目学生可提前填写附件2：毕业设计选题审批表；</p>

                        <p>四、选择校内教师题目操作流程：首先查看教师题目审批表，选择指导教师和题目，按题目要求人数组建小组，然后在毕业设计管理系统中进行选报，具体操作查看附件中《学生端操作指南》；</p>

                        <p>五、校外题目毕业设计申报：线下确定毕业设计题目（查看《申请校外毕业设计题目注意事项》），按人数组建小组，13周周五可以进行网上申报，具体操作查看附件中《学生端操作指南》.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: GENERAL PANEL -->
    </div>
</div>
</body>
</html>
