package cn.edu.zut.soft.graduate.userCenter.handler;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.dao.IdentityDAO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/20
 */
@Component
public class NoGroupHandler implements QueryHandler {

    /**
     * 总数请使用query的count来获取总数
     */

    @Autowired
    private IdentityDAO<Identity> identityDAO;

    @Autowired
    private InfoBO infoBO;

    @Autowired
    private IdentityBO identityBO;


    //如若使用ids老进行查询时,需要在此处做出检索ids
    @Override
    public void createQuery(IdentityQuery identityQuery) {
        InfoQuery infoQuery = new InfoQuery();
        infoQuery.setInfoType(InfoType.Group);
        infoQuery.setKey(IKEY.GROUP);
        infoQuery.setRole(identityQuery.getRole());
        //设定最大值检索
        infoQuery.setLimit(Integer.MAX_VALUE);
        infoQuery.setDel(0);
        CommonResult<List<Info>> result = infoBO.findByQuery(infoQuery);
        List<Info> list = result.getData();
        //查询存在小组的用户
        List<Integer> ids = new ArrayList<>(list.size());
        CollectionUtils.collect(list,new Transformer() {
            public Object transform(Object obj) {
                Info info = (Info) obj;
                return info.getIdentityId();
            }
        },ids);
        //当不存在小组用户,默认添加小组为0的小组,// FIXME: 16/11/21 待优化
        if (CollectionUtils.isEmpty(ids)){
            ids = Collections.singletonList(0);
        }
        //使用query添加分页
        //设定非ids参数
        identityQuery.setNotIds(ids);
        identityQuery.setRole(infoQuery.getRole());
        identityQuery.setDel(0);
        List<Identity> identityList  =identityBO.findByQuery(identityQuery).getData();
//        Assert.notEmpty(identityList,"用户不存在");
        //用户不存在直接返回即可
        if (CollectionUtils.isEmpty(identityList)){
            return;
        }
        //获取不存在小组的ids
        ids = new ArrayList<>(identityList.size());
        CollectionUtils.collect(identityList,new Transformer() {
            public Object transform(Object obj) {
                Identity identity = (Identity) obj;
                return identity.getId();
            }
        },ids);
        identityQuery.setIds(ids);
    }

//    @Override
//    public List<InfoStrategy> createInfoStrategyList() {
//        List<InfoStrategy> infoStrategyList = new ArrayList<>();
//        infoStrategyList.add();
//        return infoStrategyList;
//    }
}
