package cn.edu.zut.soft.graduate.core.utils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by cc on 16/6/16.
 */
public class RowNode {
    private List<Node> list=new LinkedList<>();
    public boolean add(Node node){
        return list.add(node);
    }
    public boolean remove(Node node){
        return list.remove(node);
    }

    public Node get(int i){
        return list.get(i);
    }

}
