package cn.edu.zut.soft.graduate.reportCenter.pojo;

import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import lombok.Data;

/**
 * @author cc
 * @Date 2017/2/22
 * @value 1.0
 */
@Data
public class ReportDataSearchVO {

    private ReportRule reportRule;

    private ReportData reportData;

    private UserDescVO userDescVO;

    public ReportDataSearchVO() {
    }

    public ReportDataSearchVO(ReportRule reportRule, ReportData reportData, UserDescVO userDescVO) {
        this.reportRule = reportRule;
        this.reportData = reportData;
        this.userDescVO = userDescVO;
    }
}
