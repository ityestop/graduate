package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by chuchuang on 16/10/24.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GroupQuery extends Query {

    private Integer id;
    private IssueSource status;
    private Role role;
    private Integer del;
    private Integer identityId;

    private List<Integer> ids;
}
