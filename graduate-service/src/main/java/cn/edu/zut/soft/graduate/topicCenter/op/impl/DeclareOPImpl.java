package cn.edu.zut.soft.graduate.topicCenter.op.impl;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.DeclareBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import cn.edu.zut.soft.graduate.topicCenter.op.DeclareOP;
import cn.edu.zut.soft.graduate.topicCenter.pojo.ConfirmInfo;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/27
 */
@Component
public class DeclareOPImpl implements DeclareOP {

    @Autowired
    private DeclareBO declareBO;

    @Autowired
    private GroupBO groupBO;

    @Autowired
    private IssueBO issueBO;

    @Autowired
    private InfoBO infoBO;

    @Override
    @Transactional
    public Declare GroupDeclare(LoginVO loginVO, Issue issue) {
        //效验是否能够操作,错误抛出异常
        declareBO.isAsk(loginVO, issue);
        Integer groupId = null;
        Assert.notEmpty(loginVO.getInfoList());
        for (Info info : loginVO.getInfoList()){
            if (info.getType().equals(InfoType.Group.name()) && info.getKey().equals(IKEY.GROUP)){
                groupId = Integer.parseInt(info.getValue());
            }
        }
        Assert.notNull(groupId,"未加入小组");
        GroupQuery groupQuery = new GroupQuery();
        groupQuery.setId(groupId);
        Assert.isTrue(groupBO.countByQuery(groupQuery) == 1,"小组数据错误");

        Assert.notNull(issue);
        Declare declare = new Declare();
        declare.setTopicId(issue.getId());
        declare.setGroupId(groupId);
        declare.setStatus(DeclareStatus.ING);
        return  declareBO.save(declare,loginVO.getName()).getData();
    }

    @Override
    @Transactional
    public void TeaConfirm(ConfirmInfo confirmInfo) {
        Assert.notNull(confirmInfo);
        Assert.notNull(confirmInfo.getAlternative(),"操作为空");
        if (confirmInfo.getAlternative() == Alternative.NO){
            confirmInfo.getDeclareOk().setStatus(DeclareStatus.NO);
            //修改单独的记录即可
            declareBO.update(confirmInfo.getDeclareOk(),confirmInfo.getLoginVO().getName());
        }else {
            //1.拒绝所有申报记录
            //2.同意当前申报记录
            declareBO.Confirm(confirmInfo);
            //5.修改课题状态
            issueBO.changeIssuePhase(confirmInfo.getLoginVO(),confirmInfo.getIssue().getId(), IssuePhase.Handle.ChooseSuccess);
            List<Info> infoList = infoBO.queryByInfoStrategy(Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.Group,IKEY.GROUP,""+confirmInfo.getGroup().getId())));
            List<Integer> ids = new ArrayList<>(infoList.size());
            CollectionUtils.collect(infoList, new Transformer() {
                @Override
                public Object transform(Object o) {
                    return ((Info)o).getIdentityId();
                }
            },ids);
            Assert.notEmpty(ids,"学生小组不存在成员");
            for (Integer id : ids) {
                //6.绑定指导教师
                infoBO.save(new BasicInfoStrategy(InfoType.Tea,IKEY.Guide,confirmInfo.getLoginVO().getId().toString()), id, confirmInfo.getLoginVO().getName());
                infoBO.save(new BasicInfoStrategy(InfoType.Tea,IKEY.GuideName,confirmInfo.getLoginVO().getName()), id, confirmInfo.getLoginVO().getName());
                //7.绑定课题编号
                infoBO.save(new BasicInfoStrategy(InfoType.TOPIC,IKEY.TopicId,confirmInfo.getIssue().getId().toString()),id,confirmInfo.getLoginVO().getName());
                //8.绑定课题名称
                infoBO.save(new BasicInfoStrategy(InfoType.TOPIC,IKEY.TopicName,confirmInfo.getIssue().getTitle()),id,confirmInfo.getLoginVO().getName());
            }
        }
    }
}
