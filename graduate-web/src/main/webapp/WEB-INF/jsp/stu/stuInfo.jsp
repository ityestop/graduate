<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

    <title>个人信息</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./tea"> 首页 </a>
            </li>
            <li class="active">个人信息</li>
            </li>
        </ol>
        <div class="page-header">
            <h3>个人信息
                <small>首次登录必须完善信息后才可执行其他操作! <font color="red">点击小铅笔进行编辑操作,修改完毕请点击小铅笔进行保存!</font></small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="panel-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    编号 </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" disabled='disabled' id="id" title="编号"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    学生学号 </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" disabled='disabled' id="noid" title="学生学号"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    学生姓名 </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" disabled='disabled' id="name" title="学生姓名"/>
                </div>
                        <span class="help-inline col-sm-2 "> <i
                                class="fa fa-info-circle"></i> 不可修改 </span>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    性别 </label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <select class="form-control" id="sex" title="性别" disabled='disabled'>
                            <option value="0">未知</option>
                            <option value="1">男</option>
                            <option value="-1">女</option>
                        </select>
                        <span class="input-group-addon add-on"><i class="clip-pencil-3"></i></span>
                    </div>
                </div>

            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    班级 </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" disabled='disabled' id="clazz" title="班级"/>
                </div>

            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    联系方式 </label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <input type="text" class="form-control" disabled='disabled' id="phone" title="联系方式"/>
                        <span class="input-group-addon add-on"><i class="clip-pencil-3"></i></span>
                    </div>
                </div>
                <span class="help-inline col-sm-4">
                    <i class="fa fa-info-circle"></i>
                    此项为必填项  此项为空将不能执行任何操作
                </span>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    电子邮箱 </label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <input type="text" class="form-control" disabled='disabled' id="email" title="电子邮箱"/>
                        <span class="input-group-addon add-on"><i class="clip-pencil-3"></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label h5">
                    答辩地区 </label>
                <div class="col-sm-6">
                    <div class="input-group">
                        <select class="form-control" id="address" title="答辩地区" disabled='disabled'>
                            <option value="zhengzhou">郑州</option>
                            <option value="hangzhou">杭州</option>
                            <option value="beijing">北京</option>
                            <option value="shanghai">上海</option>
                        </select>
                        <span class="input-group-addon add-on"><i class="clip-pencil-3"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script
        src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="assets/g/js/stu/stuInfo.js"></script>

</body>
</html>
