<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 2017/2/19
  Time: 下午10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>周月报规则管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">周月报列表</li>
        </ol>
        <div class="page-header">
            <h3>周月报列表</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="/stu/" method="get">
                <div class="form-group">
                    <a class="btn btn-primary" href="/stu/report/addData">
                        <span><i class=" clip-plus-circle"></i> 添加报告</span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="dataTable" class="table table-container">
            </table>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>


<!-- Modal -->
<div class="modal fade" id="yulan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">预览</h4>
            </div>
            <div class="modal-body" id="yulan-content">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript">
    var $table = $("#dataTable");
    function getParams() {
        return {escape:true}
    }

    var $model=$("#yulan-content");

    var $yulan = $('#yulan');

    window.operateEvents = {
        'click .edit': function (e, value, row, index) {
            window.location.href='/stu/report/editData?id='+row.reportData.id;
        },
        'click .preview' : function (e, value, row, index) {
            $yulan.modal('show',row);
        }
    };
    var timetimeStamp1;
    var timetimeStamp2=timeStamp2();
    var columns = [{
        field: 'reportData.id',
        title: '编号'
    }, {
        field: 'reportRule',
        title: '规则',
        formatter: function (value, row, index) {
            return "第 " + value.num +" " + (value.type == 1 ? "周" : "月") +"报告";
        }
    }, {
        field: 'reportData.supplement',
        title: '状态',
        formatter: function (value, row, index) {
            return value == 0 ? "正常提交" : "补交";
        }
    }, {
        field: 'reportData.updateTime',
        title: '最后修改时间',
        formatter: function (value, row, index) {
            timetimeStamp1=value;
            return timeStamp2String(value);
        }
    }, {
        field: 'reportRule',
        title: '最后截止时间',
        formatter: function (value, row, index) {

            return timeStamp2String(value.endTime);
        }
    },{
        title: '操作',
        formatter: function (value, row, index) {
            var str = "";
                str +="<button class='preview btn btn-green'>预览</button>";
            if(timetimeStamp2.valueOf() < row.reportRule.endTime ){
                str += "<button class='edit btn btn-primary'>编辑</button>";
            }
            return str;
        },
        events: operateEvents
    }];
    customSearch({
//        id: "#ruleTable",
        $table: $table,
        url: "report/data/list/stu",
        dataType: "json",
        params: getParams(),
        columns: columns,
        pageSize: 25
    });

    $yulan.on('show.bs.modal',function (e) {
        $model.html(e.relatedTarget.reportData.context);
    });
    $yulan.on('hide.bs.modal',function (e) {
        $model.html("");
    })

</script>

</body>
</html>
