package cn.edu.zut.soft.graduate.userCenter.bo.Impl;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.userCenter.bo.StudentBO;
import cn.edu.zut.soft.graduate.userCenter.bo.TeacherBO;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author chuchuang
 * @date 16/12/5
 */
@Slf4j
public class TeacherBOImplTest extends BaseTest {

    @Autowired
    private TeacherBO teacherBO;

    @Resource
    private StudentBO studentBO;

    @Test
    public void queryTeaOfStuName() throws Exception {

        IdentityQuery identityQuery = new IdentityQuery();
//        identityQuery
        CommonResult commonResult = teacherBO.queryTeaOfStuName(identityQuery);
        Print.println(commonResult,CommonResult.class);

    }

    @Test
    public void queryNoGroupAndNoStuOfTea(){
//        Print.println(teacherBO.queryNoGroupAndNoStuOfTea(),CommonResult.class);
    }

    @Test
    public void queryExistStudentOfTeaTest() throws Exception {
        IdentityQuery query = new IdentityQuery();
        query.clear();
        System.out.println(JSON.toJSONString(teacherBO.queryExistStudentOfTea(query).getData()));
    }

    @Test
    public void name() throws Exception {
        int num = 41;
        List<UserDescVO> userDescVOList = studentBO.randomDrawing(num).getData();
        Assert.assertEquals(userDescVOList.size(),num);
        for (UserDescVO userDescVO : userDescVOList){
            Info info = getInfo(userDescVO.getInfoList(),InfoType.Tea, IKEY.GuideName);
           log.warn("name=>{},teaName=>{}",userDescVO.getIdentity().getName(),info == null ? "null":info.getValue());
        }

    }

    private Info getInfo(List<Info> infoList, InfoType infoType,String key){
        for (Info info : infoList) {
            if (info.getType().equals(infoType.name()) && info.getKey().equals(key)) {
                return info;
            }
        }
        return null;
    }
}