package cn.edu.zut.soft.graduate.base.former;

import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import org.apache.commons.collections.Transformer;

/**
 * @author cc
 * @Date 2016/12/21
 * @value 1.0
 */
public class AuditIssueLinkIssueTransformer implements Transformer {
    @Override
    public Object transform(Object o) {
        return ((AuditIssueLink)o).getIssueId();
    }
}
