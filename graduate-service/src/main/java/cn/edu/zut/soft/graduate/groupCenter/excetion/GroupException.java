package cn.edu.zut.soft.graduate.groupCenter.excetion;

/**
 * @author chuchuang
 * @date 16/11/20
 */
public class GroupException extends RuntimeException {

    public GroupException() {
        super();
    }

    public GroupException(String message) {
        super(message);
    }

    public GroupException(String message, Throwable cause) {
        super(message, cause);
    }

    public GroupException(Throwable cause) {
        super(cause);
    }

    protected GroupException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public static GroupException create(String message){
        return new GroupException(message);
    }

    public static GroupException create(String message,Throwable cause){
        return new GroupException(message,cause);
    }

}
