package cn.edu.zut.soft.graduate.reportCenter.service.Impl;

import cn.edu.zut.soft.graduate.base.cache.CachePool;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.reportCenter.common.TimePeriod;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportData;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportRule;
import cn.edu.zut.soft.graduate.reportCenter.query.Impl.SearchReportQueryImpl;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportCenterService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

import java.util.Date;

/**
 * Created by cc on 2017/2/19.
 */
public class ReportCenterServiceImplTest extends BaseTest {

    @Autowired
    private ReportCenterService reportCenterService;

    @Test
    public void addReportRule() throws Exception {
        ReportRule reportRule = new ReportRule();
        reportRule.setType(TimePeriod.MONTH.getValue());
        reportRule.setNum(4);
        reportRule.setStartTime(new Date());
        reportRule.setEndTime(new Date(System.currentTimeMillis()+1000*3600*24));
        reportRule.setRepeat(1);
        System.out.println(reportCenterService.addReportRule(reportRule,new LoginVO()));
    }

    @Test
    public void addReportRuleAll() throws Exception {

    }

    @Test
    public void getRule() throws Exception {

    }

    @Test
    public void listRule() throws Exception {

    }

    @Test
    public void removeReportRule() throws Exception {

    }

    @Test
    public void addReportData() throws Exception {
        ReportData reportData = new ReportData();
        reportData.setContext("测试");
        reportData.setReportRuleId(244);
        LoginVO loginVO = new LoginVO();
        loginVO.setId(62);
        loginVO.setName("楚闯");
        loginVO.setRole("stu");
//        reportData.set
        reportCenterService.addReportData(reportData,loginVO);
    }

    @Test
    public void findRuleByDateOfLoginVO() throws Exception {
        LoginVO loginVO = new LoginVO();
        loginVO.setId(62);
        loginVO.setName("楚闯");
        loginVO.setRole("stu");
        System.out.println(reportCenterService.findRuleByDateOfLoginVO(new Date(),loginVO));
    }

    @Test
    public void updateReportData() throws Exception {

    }

    @Test
    public void removeReportData() throws Exception {

    }

    @Test
    public void getReportData() throws Exception {

    }

    @Test
    public void listReportData() throws Exception {

    }

    @Test
    public void statisticsStudentOfQuery() throws Exception {
        SearchReportQueryImpl reportQuery = new SearchReportQueryImpl();
        reportQuery.setPageSize(Integer.MAX_VALUE);
        reportCenterService.statisticsStudentOfQuery(reportQuery);
        System.out.println(CachePool.reportRuleCacheMap);
        System.out.println(CachePool.userDescVOCacheMap);
    }

}