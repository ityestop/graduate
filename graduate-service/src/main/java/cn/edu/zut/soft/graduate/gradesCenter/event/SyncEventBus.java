package cn.edu.zut.soft.graduate.gradesCenter.event;

import com.google.common.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>文件名称：SyncEventBus.java</p>
 * <p>文件描述：同步事件总线</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/4/19 下午5:52</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class SyncEventBus {

    private static final Logger logger = LoggerFactory.getLogger(SyncEventBus.class);

    private static final EventBus eventBus = new EventBus();

    /**
     * <p>功能描述：注册事件监听器</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-04-19 17:59:44</p>
     *
     * @param eventListener
     */
    public static void register(EventListener eventListener){
        if (eventListener == null){
            logger.error(" null listener register, skip");
            return;
        }
        eventBus.register(eventListener);
    }

    /**
     * <p>功能描述：发布事件</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-04-19 18:00:17</p>
     *
     * @param event
     */
    public static void post(BaseEvent event){
        if (event == null){
            logger.error(" null event is been posted, skip");
            return;
        }
        eventBus.post(event);
    }

}
