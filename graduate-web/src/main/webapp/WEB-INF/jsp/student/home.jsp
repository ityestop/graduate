<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>软件学院毕业设计管理平台</title>
<style type="text/css">
body {
	font-size: 14px;
}
</style>

</head>

<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">首页</li>
			</ol>
			<div class="page-header">
				<h2>
					软件学院毕业设计管理平台 <small>毕业设计开发小组 <small>若有问题可以@开发人员<font color="red">@楚闯 tel:18638637700    @杨晓天 tel:18638512501</font></small></small>
				</h2>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER -->
	<div class="row">
		<div class="col-md-12">
			<!-- start: GENERAL PANEL -->
			<div class="panel-body">
				<div class="row">
			
				</div>
			</div>
			<!-- end: GENERAL PANEL -->
		</div>
	</div>

</body>
</html>
