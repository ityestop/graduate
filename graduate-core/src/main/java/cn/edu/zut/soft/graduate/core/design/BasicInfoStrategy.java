package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.InfoTypeAble;
import com.alibaba.fastjson.JSON;
import lombok.Data;

/**
 * Created by chuchuang on 16/10/23.
 */
@Data
public class BasicInfoStrategy implements InfoStrategy {

    private InfoTypeAble type;
    private String key;
    private String value;

    public BasicInfoStrategy(){

    }

    public BasicInfoStrategy(InfoTypeAble infoType,String key,String value){
        this.type = infoType;
        this.key = key;
        this.value = value;
    }

    @Override
    public String toJson() {
        return JSON.toJSONString(this);
    }
}
