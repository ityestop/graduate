<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i  class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">发件信箱</li>
        </ol>
        <div class="page-header">
            <a class="btn btn-primary" href="./${sessionScope.pathCode}/allMailBox.do?g=true"><i class="clip-folder-upload "></i>发件信箱</a>&nbsp;&nbsp;&nbsp;<a class="btn btn-primary" href="./${sessionScope.pathCode}/allMailBox.do"><i class="clip-folder-download "></i>收件信箱</a>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <form method="post" action="./student/allMailBox.do" id="condition">
        </form>
        <div class="form-group">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th><small>选项</small></th>
                    <th><small>接收人</small></th>
                    <th><small>内容</small></th>
                    <th><small>发送时间</small></th>
                    <th><small>操作</small></th>

                </tr>
                </thead>
                <tbody>
                <c:forEach items="${list}" var="s">
                    <tr>
                        <td></td>
                        <%--<td>${s.address.stuName}</td>--%>
                        <td>${s.recipients.stuName}</td>
                        <td>${s.mailContent}</td>
                        <td><fmt:formatDate value="${s.mailCreateTime}" type="both"
                                            dateStyle="short" timeStyle="medium"/> </td>
                        <td>${s.mailReplyStatus >0 ? (s.mailReplyStatus > 1 ? (s.mailReplyStatus > 2 ? "小组已经解散":"已拒绝") : "已同意") : "邀请中"}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="form-group">
        <label class="col-sm-8"></label>
         <div class="col-sm-4" style="text-align: right; height: 40px">
            <page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"
                              curPage="${pageNum}" formId="condition"/>
        </div>
        </div>
        <!-- end: TABLE WITH IMAGES PANEL -->
    </div>

</div>
</body>
</html>
