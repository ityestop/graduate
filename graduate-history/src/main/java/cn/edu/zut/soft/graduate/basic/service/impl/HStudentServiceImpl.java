package cn.edu.zut.soft.graduate.basic.service.impl;


import cn.edu.zut.soft.graduate.basic.dao.HStudentDao;
import cn.edu.zut.soft.graduate.model.HStudent;
import cn.edu.zut.soft.graduate.basic.service.HStudentService;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>文件名称：HStudentServiceImpl.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/29 上午12:47</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Repository
public class HStudentServiceImpl implements HStudentService {

    @Resource
    private HStudentDao hStudentDao;

    @Override
    public Integer save(HStudent hStudent) {
        Integer result = null;
        if (hStudent != null){
            Integer hs = hStudentDao.save(hStudent);
            if (hs > 0){
                result = hStudent.getId();
            }
        }
        return result;
    }

    @Override
    public Integer update(HStudent hStudent) {
        Integer result = null;
        if (hStudent != null){
            result = hStudentDao.updateByPrimaryKeySelective(hStudent);
        }
        return result;
    }

    @Override
    public boolean deleteStudent(Integer id) {
        boolean result = false;
        if (id != null){
            Integer i = hStudentDao.deleteByHStudent(id);
            if (i > 0){
                result = true;
            }
        }
        return result;
    }

    @Override
    public Integer count() {
        return hStudentDao.count();
    }

    @Override
    public List<HStudent> findAll() {
        return null;
    }
}
