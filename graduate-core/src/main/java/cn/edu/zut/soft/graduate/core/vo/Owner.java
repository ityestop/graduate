package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class Owner {
    private Integer id;
    private String name;
    private String role;

    public Owner() {
    }


    public Owner(Integer id,String name,String role){
        this.id = id;
        this.name = name;
        this.role = role;
    }
}
