package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.config.bo.ConfigBO;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Config;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * <p>文件名称：ConfigController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/6 上午12:03</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RequestMapping("/config")
@Controller
public class ConfigController  extends BaseController{
    @Resource
    private ConfigBO configBO;

    @ResponseBody
    @RequestMapping("/getIssueEditRole")
    public CommonResult getIssueEditRole(){
        Config result = configBO.getConfigBykey(IKEY.ISSUE_EDIT);
        if(Boolean.parseBoolean(result.getConfigValue())){
            return CommonResult.successReturn(result);
        }
        return CommonResult.errorReturn("无编辑权限");
    }
}
