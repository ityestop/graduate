package cn.edu.zut.soft.graduate.gradesCenter.pojo;

import lombok.Data;

/**
 * @author cc
 * @Date 2017/1/6
 * @value 1.0
 */
@Data
public class GradeRow {
    private String title;
    private Object value;
    private Type type;
    enum Type {
        text,list,map;
    }
}
