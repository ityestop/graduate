package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author cc
 * @Date 2016/11/24
 * @value 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Config extends BaseModelImpl {
    private String configKey;
    private String configValue;
}
