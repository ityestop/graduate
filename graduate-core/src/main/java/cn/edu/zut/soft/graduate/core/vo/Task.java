package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class Task {
    private String nid;
    private String title;
    private String content;
    private String isAssign;//是否被分配
}
