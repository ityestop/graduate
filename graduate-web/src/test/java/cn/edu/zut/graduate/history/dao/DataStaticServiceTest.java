//package cn.edu.zut.graduate.dao;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.ibatis.session.RowBounds;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.graduate.entity.ChildTopic;
//import cn.edu.zut.graduate.entity.Student;
//import cn.edu.zut.graduate.entity.Technophobe;
//import cn.edu.zut.graduate.entity.Topic;
//import cn.edu.zut.graduate.entity.tools.Profession;
//import cn.edu.zut.graduate.entity.tools.StudentSta;
//import cn.edu.zut.graduate.service.DataStatisticsService;
//import cn.edu.zut.graduate.service.EmpService;
//import cn.edu.zut.graduate.service.FunctionService;
//import cn.edu.zut.graduate.service.StuGroupService;
//import cn.edu.zut.graduate.service.TeacherGroupService;
//import cn.edu.zut.graduate.service.TopicService;
//import cn.edu.zut.graduate.service.UploadService;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//
//public class DataStaticServiceTest {
//
//	private ServiceFit serviceFit;
//	private DataStatisticsService dataStatisticsService;
//	private TopicService topicService;
//	private ProfessionDao<Profession> professionDao;
//	private StuGroupService stuGroupService;
//	private EmpService empService;
//	private TopicDao<Topic> topicDao;
//	private TeacherGroupService teacherGroupService;
//	private StudentDao<Student> studentDao;
//	private UploadService uploadService;
//	private ChildTopicDao<ChildTopic> childTopicDao;
//	private FunctionService functionService;
//
//	@Before
//	public void setUp() throws Exception {
//		ApplicationContext context = new ClassPathXmlApplicationContext(
//				new String[] { "classpath:conf/spring-sql.xml",
//						"classpath:conf/spring-mvc.xml.brak" });
//		dataStatisticsService = (DataStatisticsService) context
//				.getBean("dataStatistics");
//		topicService = (TopicService) context.getBean("topicService");
//		professionDao = (ProfessionDao<Profession>) context
//				.getBean("professionDao");
//		stuGroupService = (StuGroupService) context.getBean("stuGroupService");
//		empService = (EmpService) context.getBean("empService");
//		topicDao = (TopicDao<Topic>) context.getBean("topicDao");
//		teacherGroupService = (TeacherGroupService) context
//				.getBean("teacherGroupService");
//		studentDao = (StudentDao<Student>) context.getBean("studentDao");
//		uploadService = (UploadService) context.getBean("uploadService");
//		childTopicDao = (ChildTopicDao<ChildTopic>) context
//				.getBean("childTopicDao");
//		functionService = (FunctionService) context.getBean("functionService");
//
//	}
//
//	@Test
//	public void test() {
//		List<Map<String, Object>> list = dataStatisticsService
//				.analysisGroupByTeacher();
//		for (Map<String, Object> objectMap : list) {
//			System.out.println(objectMap.get("teaName"));
//			System.out.println(objectMap.get("number"));
//			System.out.println(objectMap.get("student"));
//		}
//		// System.out.println(list);
//	}
//	
//	
//	@Test
//	public void test2() {
//		// Map<String ,Object>map =new HashMap<String,Object>();
//		// SelectTopic selectTopic=new SelectTopic();
//		// map.put("type", "studentAllTop");
//		// selectTopic.setTopStatus("5");
//		// ResultDo resultDo=topicService.selectTopicStatus(map,selectTopic);
//		// ResultDo
//		// resultDo=serviceFit.getTopicService().findAll(Profession.class,new
//		// RowBounds());
//		// System.out.println(resultDo.getResult());
//		// System.out.println(resultDo.getResult());
//		ResultDo resultDo = topicService.findAll(Profession.class,
//				new RowBounds());
//		// System.out.println(professionDao.findAll(new RowBounds()));
//		if (resultDo.isSuccess()) {
//			System.out.println(resultDo.getResult());
//		} else {
//			System.out.println("null");
//		}
//	}
//
//	@Test
//	public void test3() {
//		Topic topic = new Topic();
//		topic.setTopTitle("测试--12");
//		List<Technophobe> technophobes = new ArrayList<Technophobe>();
//		List<ChildTopic> childTopics = new ArrayList<ChildTopic>();
//		topic.setChildTopicList(childTopics);
//		topic.setTopTechnophobeList(technophobes);
//		ResultDo resultDo = topicService.saveOrUpdateTopic(topic);
//		System.out.println(resultDo.getResult());
//	}
//
//	@Test
//	public void test4() {
//		// ResultDo
//		// resultDo=empService.findGroupList("C5F32B0628023A4172AD8705CAAA589E",
//		// Role.STU);
//		// System.out.println(resultDo.getResult());
//		// List
//		// topics=topicDao.TopicLinkStudent("8DE3C48DF49A07EDCBE6D28AE8F9A807");
//		// System.out.println(topics);
//		// ResultDo
//		// resultDo=stuGroupService.stuGroupByteaId("8DE3C48DF49A07EDCBE6D28AE8F9A807");
//		// System.out.println(resultDo.getResult());
//		// ResultDo
//		// resultDo=topicService.assignChildTopic("A3D7BFB94D0971E5F08EAA17F4522A21",
//		// "276EE0D467FA7215C29A74299BB2CF38");
//		// System.out.println(resultDo.isSuccess()+resultDo.getMessage());
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "teaNoGroup");
//		ResultDo resultDo = empService.findEmp(map, new RowBounds(), Role.TEA);
//		System.out.println(resultDo.getResult());
//		// SelectTopic selectTopic=new SelectTopic();
//		// selectTopic.setTeacherId("mytitleTeacher");
//		// ResultDo resultDo=topicService.selectTopicStatus(map, selectTopic,
//		// new RowBounds());
//		// System.out.println(resultDo.getResult());
//
//	}
//
//	@Test
//	public void test5() {
//		// ResultDo
//		// resultDo=teacherGroupService.findStudentBytgId("0510D7DE5B3DD0DB6359C904FFF19BD5");
//		// System.out.println(resultDo.getResult());
//		// ResultDo
//		// resultDo=stuGroupService.updateTopStuLink("22DFC59F967C4594396203D6E859C5D5",true);
//		// System.out.println(resultDo.getResult());
//		// List<Student>
//		// lis=studentDao.findStuByGroupId("068DC4E0F8474A014431FF9C5E8FF25E");
//		// System.out.println(lis);
//		// ResultDo
//		// resultDo=uploadService.HandleExcelBytgId("0510D7DE5B3DD0DB6359C904FFF19BD5");
//		// System.out.println(resultDo.getResult());
//		// List<Student> st =studentDao.findTeaGroupStudentBytgId("",0,new
//		// RowBounds());
//		ResultDo resultDo = teacherGroupService.findStudentBytgId(null, 1);
//		// int count =
//		// ((List)serviceFit.getTeacherGroupService().findStudentBytgId("", 1,
//		// new RowBounds()).getResult()).size();
//		System.out.println(resultDo.getResult());
//
//	}
//
//	@Test
//	public void test6() {
//		List<Student> list = studentDao.findAll(new RowBounds());
//		System.out.println(list.size());
//		for (Student stu : list) {
//			System.out.println(stu);
//			List<Topic> top = topicDao.findByTea(stu.getStuTeacherId(), 0);
//			for (Topic topic : top) {
//				System.out.println("2");
//				List<ChildTopic> child = childTopicDao.findByTopic(topic
//						.getTopId());
//				for (ChildTopic chid : child) {
//					System.out.println("3");
//					if (stu.getStuId().equals(chid.getStudentId())) {
//						System.out.println("4");
//						stu.setChildId(chid.getId());
//						studentDao.updateByKeySelective(stu);
//						System.out.println(stu.getStuId());
//					}
//				}
//			}
//		}
//	}
//	/**
//	 * 测试添加指导成绩
//	 */
//	@Test
//	public void test9() {
//		int res=studentDao.updatedonGrade("A3D7BFB94D0971E5F08EAA17F4522A21", 100);
//		System.out.println(res);
//	}
//
//	@Test
//	public void test8() {
//		ResultDo resultDo = functionService.update(
//				"2B99208EAE81CAE43FF5D8004A9C65EA", true);
//		System.out.println(resultDo.isSuccess());
//	}
//	/**
//	 * 测试添加验收成绩
//	 */
//	@Test
//	public void test10(){
//		List<Student> list=studentDao.findTeaGroupStudentBytgId("74B05B2CABF3E3EE1A16A3E2F2D30B5E", 0);
//		for(Student stu:list){
//			System.out.println(stu);
//		}
//	}
//}
