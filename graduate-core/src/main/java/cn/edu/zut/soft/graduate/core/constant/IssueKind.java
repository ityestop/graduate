package cn.edu.zut.soft.graduate.core.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chuchuang on 16/10/23.
 */
public enum IssueKind implements NameString {
    Social("社会服务"),Research("科研项目");

    private String name;

    IssueKind(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    private static Map<String, String> issueKindMap = new HashMap<String, String>();
    public static Map<String, String> getMap() {
        for (IssueKind source:IssueKind.values()) {
            issueKindMap.put(source.name(), source.getName());
        }
        return issueKindMap;
    }

}
