/**
 * Created by chuchuang on 16/11/29.
 */
function customSearch(opt) {
    var oTableInit = {};
    //初始化Table
    oTableInit.Init = function () {
        var table = (opt.id ? $(opt.id) : opt.$table);
        table.bootstrapTable({
            url: opt.url,  //请求后台的URL（*）
            classes: 'table-no-bordered',
            method: 'get',   //请求方式（*）
            pagination: opt.pagination == null ?  true : opt.pagination ,   //是否显示分页（*）
            sidePagination: "server",
            height: opt.height ? opt.height :600,
            pageNumber: 1,   //初始化加载第一页，默认第一页
            pageSize: opt.pageSize ? opt.pageSize : 10 ,   //每页的记录行数（*）
            pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
            striped:true,
            search: false,
            queryParams: oTableInit.queryParams,
            dataType: opt.dataType,
            columns: opt.columns,
            responseHandler: oTableInit.responseHandler,
            customSearch:customSearch,
            searchOnEnterKey:true,
            uniqueId: opt.uniqueId ? opt.uniqueId : undefined
            // showRefresh:true
        })
    };
    oTableInit.queryParams = function (params) {
        if (opt.isParams){
            return opt.params;
        }else {
            opt.params.limit = params.limit;
            opt.params.offset = params.offset;
            return opt.params;
            // return Object.assign({ //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            //     limit: params.limit, //页面大小
            //     offset: params.offset //页码
            //
            // }, opt.params);
        }
    };
    oTableInit.responseHandler = function (res) {
        if (opt.isResponseHandler){
            res = opt.responseHandler(res);
        }
        return {
            total: res.totalCount,
            fixedScroll: true,
            rows: res.data
        };
    };
    oTableInit.operateFormatter = function (value, row, index) {
        return [""].join("");
    };

    var operate=function (event, value, row, index) {

    };

    oTableInit.Init();
}