package cn.edu.zut.soft.graduate.base.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.aspectj.weaver.ast.Call;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * @author chuchuang
 * @date 16/12/11
 */
public class ExcelWriter {
    public static void write(OutputStream outputStream,String createSheetName,Map<String,Object> heard, List<Map<String, Object>> mapList)throws Exception {
        //第一步创建workbook
        Workbook wb = new HSSFWorkbook();
        //第二步创建sheet
        Sheet sheet = wb.createSheet(createSheetName);

/*      //第三步创建行row:添加表头0行
        writeHeard(sheet,heard);
*/
        //第三步添加表头
        newWriteGradeStatusHeard(sheet);

        //第四步设定数据
        writeData(sheet,mapList);

        wb.write(outputStream);
    }

    public static void gradeStatusWrite(OutputStream outputStream,String createSheetName,Map<String,Object> heard, List<Map<String, Object>> mapList)throws Exception {
        //第一步创建workbook
        Workbook wb = new HSSFWorkbook();
        //第二步创建sheet
        Sheet statusSheet = wb.createSheet(createSheetName);

        //第三步添加表头
        newWriteHeard(statusSheet);

        //第四步设定数据
        writeData(statusSheet,mapList);

        wb.write(outputStream);
    }

    public static void guideTeacherStudentInfo(OutputStream outputStream,String createSheetName,Map<String,Object> heard, List<Map<String, Object>> mapList)throws Exception{
        //第一步创建workbook
        Workbook wb = new HSSFWorkbook();
        //第二步创建sheet
        Sheet statusSheet = wb.createSheet(createSheetName);

        //第三步添加表头
        newGuideTeacherStudentInfoHead(statusSheet);

        //第四步设定数据
        writeData(statusSheet,mapList);

        wb.write(outputStream);
    }

    private static void writeData(Sheet sheet, List<Map<String, Object>> mapList) {
        Row row = null;
        Cell cell = null;
        for (int i = 0,l=mapList.size() ; i < l; i++ ){
            //row = sheet.createRow(i+1);
            row = sheet.createRow(i+4);
            for (Map.Entry<String, Object> me: mapList.get(i).entrySet()){
                cell = row.createCell(Integer.parseInt(me.getKey()));
                cell.setCellValue(me.getValue() == null ? "":me.getValue().toString());
            }
        }
    }

    private static void writeHeard(Sheet sheet,Map<String,Object> heard){
        Row row = sheet.createRow(0);
        Cell cell = null;
        for (Map.Entry<String,Object> me : heard.entrySet()){
            cell = row.createCell(Integer.parseInt(me.getKey()));
            cell.setCellValue(me.getValue().toString());
        }
    }


    private static void newWriteHeard(Sheet sheet) {

        Row row = null;
        Row row1 = null;
        Cell cell = null;
        CellRangeAddress cra = null;

        //2016届毕业设计（论文）教学执行计划
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("2016届毕业设计（论文）教学执行计划");
        cra = new CellRangeAddress(0, 0, 0, 13);
        sheet.addMergedRegion(cra);

        //一大串
        row = sheet.createRow(1);
        cell = row.createCell(0);
        cell.setCellValue("部门盖章：          院长审核签字：                   填表人：                     年   月    日\t\t\t\t\t\t\t\t\t\t\t\t\n");
        cra = new CellRangeAddress(1, 1, 0, 13);
        sheet.addMergedRegion(cra);

        String[] head = new String[]{"学院名称", "学生班级", "学号", "学生姓名", "指导教师名称", "指导教师职称", "毕业设计(论文)课题名称"};

        //head
        row = sheet.createRow(2);
        row1 = sheet.createRow(3);

        for (int i = 0; i < head.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(head[i]);
            cra = new CellRangeAddress(2, 3, i, i);
            sheet.addMergedRegion(cra);
        }

        head = new String[]{"课题来源", "课题类型"};
        for (int i = 0; i < head.length; i++) {
            cell = row.createCell(7 + i * 3);
            cell.setCellValue(head[i]);
            cra = new CellRangeAddress(2, 2, 7, 7 + i * 3 + 2);
            sheet.addMergedRegion(cra);
        }

        head = new String[]{"社会服务", "科研项目", "其他来源", "设计", "论文", "其他类型"};
        for (int i = 0; i < head.length; i++) {
            cell = row1.createCell(7 + i);
            cell.setCellValue(head[i]);
        }

        cell = row.createCell(13);
        cell.setCellValue("备注");
        cra = new CellRangeAddress(2, 3, 13, 13);
        sheet.addMergedRegion(cra);

    }

    private static void newWriteGradeStatusHeard(Sheet sheet) {

        Row row = null;
        Cell cell = null;
        CellRangeAddress cra = null;

        //2016届毕业设计（论文）教学执行计划
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("2016届毕业设计（论文）中期成绩");
        cra = new CellRangeAddress(0, 0, 0, 13);
        sheet.addMergedRegion(cra);

        //一大串
        row = sheet.createRow(1);
        cell = row.createCell(0);
        cell.setCellValue("部门盖章：          院长审核签字：                   填表人：                     年   月    日\t\t\t\t\t\t\t\t\t\t\t\t\n");
        cra = new CellRangeAddress(1, 1, 0, 13);
        sheet.addMergedRegion(cra);


        String[] head = new String[]{"学院名称", "学生班级", "学号", "学生姓名", "指导教师名称", "指导教师职称", "毕业设计(论文)课题名称", "子题目", "成绩"};
        row = sheet.createRow(2);
        for (int i = 0; i < head.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(head[i]);
            cra = new CellRangeAddress(2, 3, i, i);
            sheet.addMergedRegion(cra);
        }
    }

    private static void newGuideTeacherStudentInfoHead(Sheet sheet){

        Row row = null;
        Row row1 = null;
        Cell cell = null;
        CellRangeAddress cra = null;

        //2016届毕业设计（论文）教学执行计划
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("2017届毕业设计（论文）答辩成绩汇总表");
        cra = new CellRangeAddress(0, 0, 0, 17);
        sheet.addMergedRegion(cra);

        //一大串
        row = sheet.createRow(1);
        cell = row.createCell(0);
        cell.setCellValue("部门盖章：          院长审核签字：                   填表人：                     年   月    日\t\t\t\t\t\t\t\t\t\t\t\t\n");
        cra = new CellRangeAddress(1, 1, 0, 17);
        sheet.addMergedRegion(cra);

        //标题头
        String[] head = new String[]{"序号","学院班级", "学号", "学生姓名", "指导教师姓名","毕业设计(论文)课题名称"};
        row = sheet.createRow(2);
        row1 = sheet.createRow(3);

        for (int i = 0; i < head.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(head[i]);
            cra = new CellRangeAddress(2, 3, i, i);
            sheet.addMergedRegion(cra);
        }

        head = new String[]{"题目来源", "论文类型"};
        for (int i = 0; i < head.length; i++) {
            cell = row.createCell(6 + i * 3);
            cell.setCellValue(head[i]);
            cra = new CellRangeAddress(2, 2, 6 + i * 3, 6 + i * 3 + 2);
            sheet.addMergedRegion(cra);
        }

        head = new String[]{"社会服务", "科研项目", "其他来源", "设计", "论文", "其他类型"};
        for (int i = 0; i < head.length; i++) {
            cell = row1.createCell(6 + i);
            cell.setCellValue(head[i]);
        }

        head = new String[]{"指导成绩", "评阅成绩", "答辩成绩", "总评成绩", "备注"};
        for (int i = 0; i < head.length; i++) {
            cell = row.createCell(12 + i);
            cell.setCellValue(head[i]);
            cra = new CellRangeAddress(2, 3, i, i);
            sheet.addMergedRegion(cra);
        }

    }
}
