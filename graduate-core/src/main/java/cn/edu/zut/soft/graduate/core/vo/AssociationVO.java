package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/24.
 */
@Data
public class AssociationVO {
    private Object left;
    private Object right;

    public <T> AssociationVO addLeft(T left) {
        this.left = left;
        return this;
    }

    public <T> AssociationVO addRight(T right) {
        this.right = right;
        return this;
    }

    public <T> AssociationVO addAll(T left, T right) {
        this.left = left;
        this.right = right;
        return this;
    }

}
