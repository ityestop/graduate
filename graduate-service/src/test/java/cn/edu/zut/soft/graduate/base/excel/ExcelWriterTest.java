package cn.edu.zut.soft.graduate.base.excel;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author chuchuang
 * @date 16/12/11
 */
public class ExcelWriterTest {
    @Test
    public void write() throws Exception {

//        ExcelWriter excelWriter = new ExcelWriter();
        Map<String,Object> heard = new HashMap<>();
        heard.put("0","学号");
        heard.put("1","姓名");
        heard.put("2","班级");
        heard.put("3","手机号");
        heard.put("4","答辩题目");
        heard.put("5","校外导师");
        heard.put("6","指导教师");
        heard.put("7","职称");
        heard.put("8","地区");
        heard.put("9","题目来源");


        List<Map<String,Object>> mapList = Arrays.asList(heard,heard,heard,heard);
        File file = new File("123123.xls");
        OutputStream outputStream = new FileOutputStream(file);
        ExcelWriter.write(outputStream,"测试",heard,mapList);

    }

}