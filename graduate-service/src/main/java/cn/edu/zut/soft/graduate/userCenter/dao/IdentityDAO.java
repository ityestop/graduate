package cn.edu.zut.soft.graduate.userCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import org.springframework.stereotype.Repository;

/**
 * @author cc
 * @Date 2016/10/19
 * @value 1.0
 */
@Repository
public interface IdentityDAO<T extends Identity> extends BasicDAO<Integer,T> {


}
