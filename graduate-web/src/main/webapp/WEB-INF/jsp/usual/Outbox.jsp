<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/11/13
  Time: 下午3:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>收件箱</title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/home"> 首页 </a></li>
            <li class="active">发件箱</li>
        </ol>
        <div class="page-header">
            <h3>发件箱</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table class="table table-container">
                <thead>
                <tr>
                    <th>
                        序号
                    </th>
                    <%--<th>--%>
                        <%--类型--%>
                    <%--</th>--%>
                    <th>
                        标题
                    </th>
                    <th>
                        创建时间
                    </th>
                    <th>
                        操作
                    </th>
                </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
            <div id="page-nav" style="float: right"></div>
        </div>
    </div>
</div>

<!-- Model -->
<div class="modal fade" id="choosestuGroup" tabindex="-1" role="dialog" aria-labelledby="CSGModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLabel"></h4>
            </div>
            <div class="modal-body">
                <label id="inviteInfo_model"></label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script src="/assets/g/js/source.js"></script>
<script src='/assets/g/js/paging.js'></script>
<script type="application/javascript">
    $(function () {
        var $table = $("#tbody");
        var pageSize = '${pageSize}';

        function getTr(num) {
            var $tr = $("<tr></tr>");
            for (var i = 0; i < num; i++) {
                $tr.append("<td></td>");
            }
            return $tr;
        }

        function createTable(data) {
            $table.html("");
            $.each(data, function (i, node) {
                var $tr = getTr(5);
                var td = $tr.find("td");
                $(td.get(0)).html(i + 1);
//                $(td.get(1)).html(node.type);
                $(td.get(1)).html(node.title);
                $(td.get(2)).html(timeStampYYYYMMDDHHMM(node.createTime));
                $(td.get(3)).append('<a href=\"javaScript:void(0);\" class=\"inviteInfo\" >查看</a>');

//                $(td.get(4)).html(i + 1);
                $table.append($tr);
            })

        }

        function createPage(count) {
            var opt = {
                $container: $("#page-nav"),
                className: "pagination pagination-sm",
                pageCount: Math.ceil(count / pageSize),
                beforeFun: function (targetNum) {
                    var data = {
                        identityId: '${param.identityId}',
                        issuePhase: '${param.issuePhase}',
                        title: '${param.title}',
                        taskSize: '${param.taskSize}',
                        pageSize: pageSize,
                        currentPage: targetNum - 1
                    };
                    $.ajax({
                        url: "/massage/query",
                        data: data,
                        dataType: "json",
                        type: "get",
                        success: function (result) {
                            if (result.success) {
                                createTable(result.data);
                            } else {
                                alert(result.errorMsg);
                            }
                        }
                    });
                }
            };
            createPageNav(opt);
        }

        createPage('${requestScope.count}');
        createTable(JSON.parse('${requestScope.list}'));
    });

    $(".inviteInfo").live("click",function () {
        var win_msg = $.trim($(this).parent().parent().children("td").get(4).innerHTML);
        $.ajax({
            url: "/massage/query",
            type:'post',
            dataType: "json",
            data:{
                id:win_msg
            },
            success:function (result) {
                if(result.success){
                    $("#CSGModalLabel").html(result.data.title);
                    $("#inviteInfo_model").html("" + result.data.content +"");
                    $('#choosestuGroup').modal('show')
                }
            }
        });

    })
</script>
</body>
</html>
