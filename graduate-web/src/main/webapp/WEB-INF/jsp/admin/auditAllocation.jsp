<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/11/24
  Time: 下午1:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>审核权限</title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">专家组审核管理</li>
        </ol>
        <div class="page-header">
            <h3>专家组审核管理</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-1 col-sm-offset-10">
        <a class="btn btn-primary" href="./${sessionScope.pathCode}/auditExpert.do">
            <span><i class=" clip-plus-circle "></i> 添加审核专家</span>
        </a>
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>
                        <small>工号</small>
                    </th>
                    <th>
                        <small>姓名</small>
                    </th>
                    <th>
                        <small>职称</small>
                    </th>
                    <th>
                        <small>手机</small>
                    </th>
                    <th>
                        <small>通过个数</small>
                    </th>
                    <th>
                        <small>分配数目</small>
                    </th>
                    <th>
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${teacherList}" var="teacher">
                    <tbody>
                    <tr>
                        <td>${teacher.teaTeacherId}</td>
                        <td>${teacher.teaName}</td>
                        <td>${teacher.jobTitle.name}</td>
                        <td>${teacher.teaTell}</td>
                        <td>${teacher.sucNum}(${teacher.sucChildNum})</td>
                        <td>${teacher.allNum}(${teacher.allChildNum})</td>
                        <td>
                            <a href="./${sessionScope.pathCode}/auditTask.do?id=${teacher.teaId}">分配任务</a>
                            <a href="./${sessionScope.pathCode}/updateTask.do?id=${teacher.teaId}">修改任务</a>
                        </td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
        </div>
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
</body>
</html>
