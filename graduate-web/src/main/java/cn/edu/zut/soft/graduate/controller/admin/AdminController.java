package cn.edu.zut.soft.graduate.controller.admin;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.config.bo.ConfigBO;
import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.controller.util.IdentityUtil;
import cn.edu.zut.soft.graduate.core.constant.IssueKind;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.ProfessionalLever;
import cn.edu.zut.soft.graduate.core.model.Impl.Config;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>文件名称：AdminController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/26 上午12:03</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RequestMapping("/admin")
@Controller
public class AdminController extends BaseController {

    @Resource
    private IdentityBO identityBO;

    @Resource
    private IssueBO issueBO;

    @Resource
    private ConfigBO configBO;

    @RequestMapping()
    public String home(HttpServletRequest request, Model model) {
        return "/adminE/Home";
    }

    @RequestMapping("notice/list")
    public String noticeList() {
        return "/adminE/noticeList";
    }

    @RequestMapping("notice/new")
    public String noticeNew() {
        return "/adminE/newNotice";
    }

    @RequestMapping("notice")
    public String notice() {
        return "/usual/notice";
    }

    @RequestMapping("list/{role}")
    public ModelAndView allTea(Map<String, Object> model, IdentityQuery identityQuery, @PathVariable("role") Role role) {
        if (identityQuery == null) {
            identityQuery = new IdentityQuery();
        }
        Assert.notNull(role);
        identityQuery.setRole(role);
        if (Role.tea == role){
            model.put("professionalLeverMap", JSON.toJSON(ProfessionalLever.getMap()));
        }
        CommonResult result = CommonResult.successReturn(identityBO.userPersonal(identityQuery, IdentityUtil.findByRole(role)),identityQuery.getCount().intValue());
        if (result.isSuccess()) {
            model.put("list", JSON.toJSON(result.getData()));
            model.put("count", result.getTotalCount());
            model.put("pageSize", identityQuery.getLimit());
            model.put("currentPage", identityQuery.getCurrentPage());
        }
        return new ModelAndView("adminE/"+role.name()+"AllList", model);
    }

    @RequestMapping("issue/list/{role}")
    public ModelAndView issueSelect(Map<String, Object> model, IssueQuery issueQuery, @PathVariable("role") Role role){
        //修改掉角色
        issueQuery.setIdentityRole(role);
        issueQuery.setIssuePhaseList(IKEY.statusSet);
        CommonResult<List<Issue>> result =  issueBO.findByQuery(issueQuery);
        for (Issue issue : result.getData()){
            issue.setTechnology(null);
        }
        model.put("list", JSON.toJSON(result.getData()));
        model.put("count", result.getTotalCount());
        model.put("pageSize", issueQuery.getLimit());
        model.put("currentPage", issueQuery.getCurrentPage());

        putViewSource(model);

        if (Role.tea == role) {
            IdentityQuery identityQuery = new IdentityQuery();
            //检索所有的教师
            identityQuery.setLimit(Integer.MAX_VALUE);
            identityQuery.setRole(role);
            model.put("teaList",JSON.toJSON(identityBO.findByQuery(identityQuery).getData()));
        }
        return new ModelAndView("adminE/"+role.name()+"IssueList",model);
    }


    @RequestMapping("issue")
    public ModelAndView IssueData(Map<String,Object> model){
        putViewSource(model);
        return new ModelAndView("adminE/IssueDetails",model);
    }

    private void putViewSource(Map<String, Object> model) {
        model.put("statusList", JSON.toJSON(IssuePhase.getMap()));
        model.put("typeList", JSON.toJSON(IssueSource.getMap()));
        model.put("kindList", JSON.toJSON(IssueKind.getMap()));
    }


    /**
     * 功能描述;管理员端功能设置
     * @param model
     * @return
     */
    @RequestMapping("/config")
    public ModelAndView configList(Map<String, Object> model){
        List<Config> result = configBO.findAll();
        model.put("config", result);
        return new ModelAndView("/adminE/config", model);
    }

    /**
     * 功能描述:更新管理员端功能设置
     * @param model
     * @param request
     * @param key
     * @param value
     * @return
     */
    @RequestMapping("/updateConfig")
    public ModelAndView updateConfig(Map<String, Object> model,HttpServletRequest request, @RequestParam("key")String key, @RequestParam(value = "value", required = false)String value){
        LoginVO session = (LoginVO)request.getSession().getAttribute("user");
        if (session == null){
            model.put("errorMsg","System error");
        }
        if (key == null && value == null){
            model.put("errorMsg","Param error");
        }
        Config config = new Config();
        config.setConfigKey(key);
        config.setConfigValue(value);
        Config result = configBO.updateByKey(session, config);
        if (result == null){
            model.put("errorMsg","System error");
        }
        return new ModelAndView("redirect:/admin/config", model);
    }

    @RequestMapping("/toreplayGroupStu")
    public ModelAndView toreplayGroupStu(){
        return new ModelAndView("/adminE/replayGroupStu");
    }


}
