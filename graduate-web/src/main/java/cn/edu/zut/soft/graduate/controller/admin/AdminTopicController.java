package cn.edu.zut.soft.graduate.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>文件名称：AdminTopicController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/26 上午12:38</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RequestMapping("/admin")
@Controller
public class AdminTopicController {

    @RequestMapping("/conditionTopic")
    public String conditionFindTopic(HttpServletRequest request, Model model){

        return null;
    }

    @RequestMapping("/TopicInfo")
    public String TopicInfoMessage(HttpServletRequest request, Model model){

        return null;
    }

    @RequestMapping("/updateStatusTopic")
    public String UpdateTopicByStatus(HttpServletRequest request, Model model){

        return null;
    }

    @RequestMapping("/notExpertTopic")
    public String NotExpertTopic(HttpServletRequest request, Model model){
        return null;
    }

    @RequestMapping("/expertTeaTopic")
    public String ExpertTeacherTopic(HttpServletRequest request, Model model){
        return null;
    }



}
