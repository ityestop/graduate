<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>小组学生</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table,.table {
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">开题成绩</li>
        </ol>
        <div class="page-header">
            <h4>校外题目开题成绩
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <input type="hidden" id="groupLeader" value="${sessionScope.GroupgroupLeader}"/>
            <input type="hidden" id="groupSEC" value="${sessionScope.GroupgroupSEC}"/>
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>


<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">

    var $goupLeader = $("#groupLeader").val();
    var $goupSEC = $("#groupSEC").val();

    var $role = false;
    if (($goupLeader != null && $goupLeader != "" && $goupLeader != undefined) || ($goupSEC != null && $goupSEC != "" && $goupSEC != undefined)) {
        $role = true;
    }
    var columns = [
        {
            field: 'identity.id',
            title: '编号'
        },
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'infoList',
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var topicName = "";
                var topicId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC') {
                        if (array[i].key == 'topicName') {
                            topicName = array[i].value;
                        } else if (array[i].key == 'topicId') {
                            topicId = array[i].value;
                        }
                    }
                }
                return "<a href='/tea/issueDetail?id=" + topicId + "'>" + topicName + "</a>";
            },
            sortStable: true
        }, {
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '城市',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'address') {
                        str = array[i].value;
                        break;
                    }
                }
                return sourceMaps().addressMap[str];
            }
        }, {
            title: '成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "<label class='label label-danger'>未通过</label>";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'openGrade') {
                        if (array[i].value == 'YES') {
                            str = "<label class='label label-success'>通过</label>";
                            break;
                        }
                    }
                }
                return str;
            }
        }
    ];
    customSearch({
        id: "#table",
        url: "/admin/outIssueOpneGradeSchool",
        dataType: "json",
        params: {},
        columns: columns,
        pageSize: 100
    });


</script>
</body>
</html>
