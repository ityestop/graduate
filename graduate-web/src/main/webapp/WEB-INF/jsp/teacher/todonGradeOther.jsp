
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>

<title>教师答辩小组</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">指导成绩</li>
			</ol>
			<div class="page-header">
				<c:if test="${number eq 4 }">
					<h3>指导成绩</h3>
				</c:if>
			
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<table class="table  table-hover"
					style="TABLE-LAYOUT: fixed; WORD-BREAK: break-all">
					<thead>
						<tr>
							<th width="14%"><small>学号</small></th>
							<th width="7%"><small>姓名</small></th>
							<th width="5%"><small>性别</small></th>
							<th width="13%"><small >班级</small></th>
							<th width="25%"><small>答辩题目</small></th>
							<th width="8%"><small>校外导师</small></th>
							<th><small>指导教师</small></th>
							<th width="7%"><small> ${number eq 4 ? "指导成绩" : "指导成绩" }
							</small></th>
							<th><small>题目来源</small></th>
						</tr>
					</thead>
					<c:forEach items="${stulist}" var="t">
						<tr>

							<td><small>${t.stuStudentId}</small></td>
							<td><small>${t.stuName }</small></td>
							<td><small>${t.stuSex ? "女" : "男"}</small></td>
							<td><small>${t.stuClass}</small></td>
							<c:choose>
								<c:when test="${t.topTitle ne null }">
									<c:if
										test="${t.childTitle eq null or t.childTitle eq '' or t.childTitle eq 'null' }">
										<td
											style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
											title="${t.topTitle}"><small><a
												href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
													${t.topTitle}</a></small></td>
									</c:if>
									<c:if
										test="${t.childTitle ne null and t.childTitle ne '' and t.childTitle ne 'null' }">
										<td
											style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"
											title="${t.topTitle}-${t.childTitle}"><small><a
												href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
													${t.topTitle}</a></small></td>
									</c:if>
								</c:when>
								<c:when test="${t.topTitle eq null }">
									<td
										style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><small><a
											href="javaScript:void(0);"> 无</a></small></td>
								</c:when>
							</c:choose>
							<td><small>${t.topOutTeacher eq null ? "无" :t.topOutTeacher }</small></td>
							<td><small>${t.teaName}</small></td>
							<td><small> 
							<fmt:formatNumber value="${t.donGrade}" pattern="0" />
								
							</small></td>
							<c:choose>
								<c:when test="${t.topSource eq 3}">
									<td><small>自拟题目</small></td>
								</c:when>
								<c:when test="${t.topSource lt 3}">
									<td><small>校内题目</small></td>
								</c:when>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
			</div>

		</div>
		<!-- end: TABLE WITH IMAGES PANEL -->
	</div>
	
</body>
</html>