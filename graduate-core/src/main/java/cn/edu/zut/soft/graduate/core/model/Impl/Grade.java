package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Grade extends Info {
//    private String owner;//主管
//    private String type;//成绩类型
//    private String key;//数据的键
//    private String value;//值
//
//    private Integer identityId;
//    private Role identityRole;
//    private String identityName;
//
//    @JSONField(serialize = false)
//    public String getOwner(){
//        return owner;
//    }
}
