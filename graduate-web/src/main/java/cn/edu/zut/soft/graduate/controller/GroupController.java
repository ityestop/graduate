package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.CompanyProjectRequest;
import cn.edu.zut.soft.graduate.core.model.Impl.CreateGroup;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.view.TeacherGroupVO;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.groupCenter.bo.TeacherGroupBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.CompanyProjectRequestBO;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.bo.StudentBO;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */
@Controller
@RequestMapping("group")
public class GroupController extends BaseController {

    @Resource
    private GroupBO groupBO;

    @Resource
    private TeacherGroupBO teacherGroupBO;

    @Resource
    private IdentityBO identityBO;

    @Resource
    private InfoBO infoBO;

    @Resource
    private CompanyProjectRequestBO companyProjectRequestBO;

    @Resource
    private StudentBO studentBO;


    /**
     * 功能描述:创建小组
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/createGroup")
    public CommonResult createGroup(HttpServletRequest request, @RequestParam("id") Integer id, @RequestParam("role") Role groupRole) {
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        CreateGroup createGroup = new CreateGroup();
        if (loginVO == null || id == null || groupRole == null) {
            return CommonResult.errorReturn("System error!");
        }
        if (request.getSession().getAttribute(IKEY.BASIC_ADMIN) == null) {
            createGroup.setRunner(Role.Runner.user);
        } else {
            createGroup.setRunner(Role.Runner.admin);
        }
        createGroup.setOwner(id);
        createGroup.setUser(loginVO);
        createGroup.setRole(groupRole);
        Group result = groupBO.userCreateGroup(createGroup);
        if (result == null) {
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(result);
    }

    /**
     * 学生加入小组
     *
     * @param httpSession
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("join/{alternative}")
    public CommonResult JoinGroup(HttpSession httpSession, @Param("groupId") Integer groupId, @PathVariable("alternative") Alternative alternative) {
        LoginVO loginVO = (LoginVO) httpSession.getAttribute(IKEY.USER);
        Assert.notNull(loginVO);
        //效验只能是学生才能加入小组
        Assert.isTrue(Role.stu == Role.valueOf(loginVO.getRole()), "角色不正确");
        Group group = groupBO.joinGroup(loginVO, groupId, alternative);
        if (Alternative.YES == alternative) {
            httpSession.setAttribute(IKEY.GROUP_GROUP, groupId);
        }
        return CommonResult.successReturn(group);
    }

    /**
     * 功能描述:管理员分配教师答辩小组
     *
     * @param request
     * @param group
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/assignGroupTea")
    public CommonResult assignGroupTea(HttpServletRequest request, @RequestParam("groupId") Integer group, @RequestParam("id") Integer id) {
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登陆");
        }
        if (group == null || id == null) {
            return CommonResult.errorReturn("System error");
        }
        Group groupInfo = groupBO.assignGroupTea(loginVO, group, id);
        if (groupInfo == null) {
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(groupInfo);
    }


    /**
     * 功能描述:根据小组ID 检索小组成员
     *
     * @param request
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("/groupInfo")
    public CommonResult groupInfo(HttpServletRequest request, @RequestParam("groupId") Integer groupId,@RequestParam(value = "reverse",defaultValue = "false") boolean reverse) {
        if (groupId == null) {
            return null;
        }
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登陆!");
        }
        List<UserDescVO> result = identityBO.findReverseGroupMember(loginVO, groupId,reverse);
        return CommonResult.successReturn(result);
    }

    /**
     * 功能描述:申请自拟题目权限
     *
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/companyRole")
    public CommonResult companyRole(HttpServletRequest request) {
        LoginVO session = (LoginVO) request.getSession().getAttribute("user");
        if (session == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        CompanyProjectRequest result = companyProjectRequestBO.isQualified(session);
        return CommonResult.successReturn(result);
    }

    /**
     * 功能描述:自拟题目申请
     *
     * @param request
     * @param reason
     * @return
     */
    @ResponseBody
    @RequestMapping("/companyAsk")
    public CommonResult companyAsk(HttpServletRequest request, @RequestParam("reason") String reason) {
        if (StringUtils.isEmpty(reason)) {
            return CommonResult.errorReturn("申请理由不可为空!");
        }
        LoginVO loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (loginVO == null) {
            return CommonResult.errorReturn("请重新登录!");
        }
        CompanyProjectRequest result = companyProjectRequestBO.sendRequest(loginVO, reason);
        return CommonResult.successReturn(result);
    }

    /**
     * 功能描述:根据小组ID获取小组成员
     *
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("/groupMember")
    public CommonResult getGroupMember(@RequestParam("groupId") Integer groupId) {
        if (groupId == null) {
            return CommonResult.successReturn("System error");
        }
        InfoQuery infoQuery = new InfoQuery();
        infoQuery.setKey(IKEY.GROUP);
        infoQuery.setValue(groupId.toString());
        infoQuery.setDel(0);
        CommonResult<List<Info>> result = infoBO.findByQuery(infoQuery);
        return CommonResult.successReturn(result.getData());
    }

    /**
     * 功能描述:查询所有未分配小组成员
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/getOneGroupStu")
    public CommonResult getOneGroupStu(GroupQuery groupQuery) {
        groupQuery.setRole(Role.stu);
        return groupBO.findNoIssueStuGroup(groupQuery);
    }

    /**
     * 功能描述: 根据小组ID获取Group表中详情
     *
     * @param groupId
     * @return
     */
    @ResponseBody
    @RequestMapping("/groupMessage")
    public CommonResult getGroupMessage(@RequestParam(value = "groupID", required = false) Integer groupId) {
        if (groupId == null) {
            return CommonResult.errorReturn("System error");
        }
        GroupQuery groupQuery = new GroupQuery();
        groupQuery.setId(groupId);
        groupQuery.setDel(0);
        List<Group> result = groupBO.findByQuery(groupQuery).getData();
        if (result.size() > 0) {
            return CommonResult.successReturn(result.get(0));
        }
        return CommonResult.errorReturn("System error");
    }


    /**
     * 功能描述:把某个小组中的成员踢出小组
     * @param request
     * @param groupId
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/kickOutGroup")
    public CommonResult kickOutGroup(HttpServletRequest request,@RequestParam("groupId") Integer groupId, @RequestParam("id") Integer id){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登陆!");
        }
        if (groupId == null || id == null){
            return CommonResult.errorReturn("System error");
        }
        Group result = groupBO.unAssignGroupTea(loginVO, groupId, id);
        if (result == null){
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(result);
    }
    @ResponseBody
    @RequestMapping("/findAllGroup")
    public CommonResult findAllGroup(HttpServletRequest request){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登陆!");
        }
        List<Group> result = (List<Group>) groupBO.findAllGroup();
        if (result == null){
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(result);
    }

    /**
     * 教师小组
     * @param id
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping("/teacherGroup")
    public CommonResult teacherGroup(Integer id,HttpServletRequest request){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登陆!");
        }
        TeacherGroupVO teacherGroupVO = teacherGroupBO.get(id);
        if (teacherGroupVO == null){
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(request);
    }

}
