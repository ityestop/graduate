package cn.edu.zut.soft.graduate.base.former;

import org.apache.commons.collections.Transformer;
import org.springframework.stereotype.Component;

/**
 * @author cc
 * @Date 2016/12/13
 * @value 1.0
 */
@Component
public class StringToIntegerTransformer implements Transformer {
    @Override
    public Object transform(Object o) {
        return Integer.parseInt(String.valueOf(o));
    }
}
