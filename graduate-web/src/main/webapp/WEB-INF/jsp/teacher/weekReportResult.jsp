<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>周月报详情</title>
<script type="text/javascript" src="IAssets/js/selectTopic.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">周月报详情</li>
			</ol>
			<div class="page-header">
				<h3>周月报详情</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
		<form action="./${sessionScope.pathCode}/ReportGrade.do" method="post">	
			<div class="panel-body">
				
				<table class="table">
					<tbody>
						<tr>
							<td width="10%"><small>周月报标题:</small></td>
							<td><small>${info.title}</small></td>
						</tr>
						<tr>
							<td width="10%"><small>学生:</small></td>
							<td><small><font color="red">${info.student.stuName}</font></small></td>
						</tr>
						<tr>
							<td><small>类型:</small></td>
							<td><c:if test="${info.type eq 1 }">
									<small>周报</small>
								</c:if> <c:if test="${info.type eq 2 }">
									<small>月报</small>
								</c:if></td>
						</tr>
						<tr>
							<td><small>周次:</small></td>
							<td><small> ${info.number}</small></td>
						</tr>

						<tr>
							<td><small>本周进度:</small></td>
							<td><small>${info.nowContext}</small></td>
						</tr>
						<tr>
							<td><small>下周计划:</small></td>
							<td><small> ${info.nextContext}</small></td>
						</tr>
						<c:choose>
						<c:when test="${info.status eq 2 }">
						<tr>
							<input type="hidden" name="id" value="${info.id}">
							<td>
									<small>成绩:</small>
							</td>
							<td>
									<label class="radio-inline">
										<input type="radio" value="90" name="value" class="grey"
										<c:if test="${info.value eq 90 }">
											checked="checked"
										</c:if>
										>
										A
									</label>
									<label class="radio-inline">
										<input type="radio" value="80" name="value" class="grey" 
										<c:if test="${info.value eq 80 }">
											checked="checked"
										</c:if>
										>
										B
									</label>
									<label class="radio-inline">
										<input type="radio" value="70" name="value" class="grey"
										<c:if test="${info.value eq 70 }">
											checked="checked"
										</c:if>
										>
										C
									</label>
									<label class="radio-inline">
										<input type="radio" value="60" name="value" class="grey"
										<c:if test="${info.value eq 60 }">
											checked="checked"
										</c:if>
										>
										D
									</label>
									<label class="radio-inline">
										<input type="radio" value="0" name="value" class="grey"
										<c:if test="${info.value eq 0 }">
											checked="checked"
										</c:if>
										>
										0
									</label>
									<small><font color="red">分数比例：A:90 B:80 C:70 D:60 0:0</font></small>
							</td>
						</tr>
						<tr>
							<td>
							<small>教师评语：</small>
							</td>
							<td>
							<textarea placeholder="教师指导评语" name="common" class="form-control"></textarea>
							</td>
						</tr>
						</c:when>
						<c:when test="${info.status eq 3 }">
						<tr>
							<td><small>成绩：</small></td>
							<c:if test="${status eq 0 }">
							<td><small>
								<c:if test="${info.value eq 90 }">A</c:if>
								<c:if test="${info.value eq 80 }">B</c:if>
								<c:if test="${info.value eq 70 }">C</c:if>
								<c:if test="${info.value eq 60 }">D</c:if>
								<c:if test="${info.value eq 0 }">0</c:if>
							</small></td>
							</c:if>
							<c:if test="${status eq 1 }">
							<input type="hidden" name="id" value="${info.id}">
							<td>
									<label class="radio-inline">
										<input type="radio" value="90" name="value" class="grey"
										<c:if test="${info.value eq 90 }">
											checked="checked"
										</c:if>
										>
										A
									</label>
									<label class="radio-inline">
										<input type="radio" value="80" name="value" class="grey" 
										<c:if test="${info.value eq 80 }">
											checked="checked"
										</c:if>
										>
										B
									</label>
									<label class="radio-inline">
										<input type="radio" value="70" name="value" class="grey"
										<c:if test="${info.value eq 70 }">
											checked="checked"
										</c:if>
										>
										C
									</label>
									<label class="radio-inline">
										<input type="radio" value="60" name="value" class="grey"
										<c:if test="${info.value eq 60 }">
											checked="checked"
										</c:if>
										>
										D
									</label>
									<label class="radio-inline">
										<input type="radio" value="0" name="value" class="grey"
										<c:if test="${info.value eq 0 }">
											checked="checked"
										</c:if>
										>
										0
									</label>
									<small><font color="red">分数比例：A:90 B:80 C:70 D:60 0:0</font></small>
							</td>
							</c:if>
						</tr>
						<tr>
							<td><small>教师评语：</small></td>
							<c:if test="${status eq 0 }">
							<td><small>
								<c:if test="${info.common ne null}">${info.common }</c:if>
								<c:if test="${info.common eq null }">无</c:if>
							</small></td>
							</c:if>
							<c:if test="${status eq 1 }">
							<td><small>
								<c:if test="${info.common ne null}">
								<textarea placeholder="教师指导评语" name="common" class="form-control">${info.common }</textarea>
								</c:if>
								<c:if test="${info.common eq null }">
								<textarea placeholder="教师指导评语" name="common" class="form-control"></textarea>
								</c:if>
							</small></td>
							</c:if>
						</tr>
						</c:when>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div class="form-group">
			<div class="col-sm-7"></div>
			<c:choose>
				<c:when test="${info.status eq 2 }">
					<div class="col-sm-2">
					<button type="submit" class="btn btn-blue btn-block">提交成绩</button>
					</div>
				</c:when>	
				<c:when test="${info.status eq 3 and status eq 1}">
				<div class="col-sm-2">
					<button type="submit" class="btn btn-blue btn-block">提交成绩</button>
					</div>
				</c:when>
			</c:choose>	
			
				<div class="col-sm-2">
					<button type="button" class="btn btn-blue btn-block"
						onclick="window.location.href='javascript:history.go(-1);'">
						<i class="clip-arrow-right-2 "></i> 返回
					</button>
				</div>

			</div>
			</form>
		</div>
	</div>
	</div>
</body>
</html>
