package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by chuchuang on 16/10/23.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AuditIssueLinkQuery extends Query {
    private Integer identityId;

    private Integer issueId;

    private List<Integer> issueIds;

    private List<Integer> identityIds;
}
