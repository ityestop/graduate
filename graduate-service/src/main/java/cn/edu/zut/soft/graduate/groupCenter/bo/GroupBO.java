package cn.edu.zut.soft.graduate.groupCenter.bo;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.model.pojo.Relation;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.model.Impl.CreateGroup;
import cn.edu.zut.soft.graduate.core.model.Impl.Grade;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.groupCenter.pojo.SaveUserGroup;

import java.util.Collection;
import java.util.List;

/**
 * Created by chuchuang on 16/10/24.
 */
public interface GroupBO extends BasicBO<Group,GroupQuery> {

    /**
     * 关联 用户与小组之间的关系
     * @return
     */
    boolean associationIDGroup(SaveUserGroup saveUserGroup, Relation relation);

    /**
     * 加入小组
     * @param loginVO
     * @param groupId
     * @return
     */
    Group joinGroup(LoginVO loginVO, Integer groupId, Alternative alternative);

    /**
     * 用户分配小组
     * @param loginVO
     * @param groupId
     * @param ids
     * @return
     */
    Group assignGroupTea(LoginVO loginVO,Integer groupId,Integer ids);

    /**
     * 用户去除小组
     * @param loginVO
     * @param groupId
     * @param id
     * @return
     */
    Group unAssignGroupTea(LoginVO loginVO, Integer groupId, Integer id);

    /**
     * 批量关联 用户与小组之间的关系
     * @param groupId 小组编号
     * @param ids 用户集合
     * @param loginVO 操作者
     * @return
     */
    CommonResult<List<SaveUserGroup>> batchAssociationIDGroup(LoginVO loginVO,Integer groupId, Collection<Integer> ids);

    /**
     * 用户主动创建小组,
     * @return
     */
    Group userCreateGroup(CreateGroup createGroup);

    /**
     * 逻辑删除小组数据
     * @param loginVO
     * @param id
     * @return
     */
    Group delete(LoginVO loginVO,Integer id);

    /**
     * 检索素有的不存在课题的学生小组
     * @return
     */
    CommonResult<List<Group>> findNoIssueStuGroup(GroupQuery groupQuery);

    /**
     * 查看全部小组
     * @return
     */
    CommonResult<List<Group>> findAllGroup();

    /**
     * <p>功能描述：分配对接答辩小组  仅限管理员操作</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-03 00:01:18</p>
     *
     * @param loginVO
     * @param groupId
     * @param disGroupId
     * @return
     */
    CommonResult<Group> distributionReplyTeaGroup(LoginVO loginVO, Integer groupId, Integer disGroupId);


}
