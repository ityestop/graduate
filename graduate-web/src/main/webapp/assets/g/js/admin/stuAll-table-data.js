/**
 * Created by zgx on 2016/11/29.
 */

document.write("<script src='/assets/g/js/config.js'></script>");

$(function () {

    customSearch({id:'#studentTable'});

});

function customSearch(opt) {
    var oTableInit = {};
    //初始化Table
    oTableInit.Init = function () {
        $(opt.id).bootstrapTable({
            url: '/identity/list/stu',  //请求后台的URL（*）
            classes: 'table-no-bordered',//表格的样式
            method: 'get',   //请求方式（*）
            pagination: true,   //是否显示分页（*）
            sidePagination: "server",//分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,   //初始化加载第一页，默认第一页
            pageSize: 10,   //每页的记录行数（*）
            pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
            search: false,//是否显示表格搜索，此搜索是客户端搜索，不会进服务端
            queryParams: oTableInit.queryParams,//传递参数（*）
            dataType: "json",//服务器返回的数据类型
            /**
             * colums 是表的标题和结构
             */
            columns: [{
                checkbox: true
            }, {
                field: 'identity.noId',
                title: '学生学号'
            }, {
                field: 'identity.name',
                title: '姓名'
            }, {
                field: 'identity.sex',
                title: '性别',
                formatter: function (value, row, index) {
                    return value == "-1" ? "女" : (value == "1" ? "男" : "未知");
                }
            }, {
                title: '班级',
                formatter: function (value,row,index) {
                    var array = row.infoList;
                    var str = "";
                    for(var i = 0; i < array.length;i++){
                        if (array[i].type == 'Basic' && array[i].key == 'clazz'){
                            str += array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                field: 'identity.phone',
                title: '联系方式'
            },{
                title:"操作",
                formatter:oTableInit.operateFormatter,
                events:operate
            }
            ],
            responseHandler: oTableInit.responseHandler, //加载服务器数据之前的处理程序，可以用来格式化数据。 参数：res为从服务器请求到的数据。
            customSearch:customSearch,//自定义搜索框
            searchOnEnterKey:true,//设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
            // showRefresh:true
        })
    };
    oTableInit.queryParams = function (params) {
        return { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            limit: params.limit, //页面大小
            offset: params.offset, //页码
            name:$("input[name='name']").val()
        };
    };
    oTableInit.responseHandler = function (res) {
        return {
            total: res.totalCount,
            fixedScroll: true,
            rows: res.data
        };
    };
    oTableInit.operateFormatter = function (value, row, index) {
        return ["<a href='javascript:respwd("+row.identity.id+")'>重置密码</a>"].join("");
    };

    var operate=function (event, value, row, index) {

    };

    oTableInit.Init();
}
function respwd (id){
    $.ajax({
        url: "/identity/resetPwd/" + id,
        type: "put",
        dataType: "json",
        success: function (result) {
            if (result.success) {
                alert("重置成功,密码为用户手机号");
            } else {
                alert(result.errorMsg);
            }
        }
    });
}
/**
    $(function () {
        var $table = $("#tbody");
        var $name = $("input[name='name']");
        var pageSize = '${pageSize}';

        function getTr(num) {
            var $tr = $("<tr></tr>");
            for (var i = 0; i < num; i++) {
                $tr.append("<td></td>");
            }
            return $tr;
        }

        function createTable(data) {
            $table.html("");
            $.each(data, function (i, node) {
                var $tr = getTr(6);
                var td = $tr.find("td");
                $(td.get(0)).html(node.identity.noId);
                $(td.get(1)).html(node.identity.name);
                $(td.get(2)).html(node.identity.sex == 0 ? "未知" : (node.identity.sex > 0 ? "男" : "女"));

//                var var infoMap = {};
                $.each(node.infoList, function (j, no) {
                    infoMap[no.key] = no.value;
                });professionalLever = infoMap["professionalLever"];
//                if (professionalLever != null) {
//                    $(td.get(3)).html(JSON.parse(professionalLever).value);
//                }
                $(td.get(4)).html(node.identity.phone);
                var $reset = $("<a class='btn btn-link'>重置密码</a>");
                $reset.click(function () {
                    $.ajax({
                        url: "/identity/resetPwd/" + node.identity.id,
                        type: "put",
                        dataType: "json",
                        success: function (result) {
                            if (result.success) {
                                alert("重置成功,密码为学生学号");
                            } else {
                                alert(result.errorMsg);
                            }
                        }
                    });
                });
                $(td.get(5)).html($reset);
                $table.append($tr);
            })

        }

        function createPage(count) {
            var opt = {
                $container: $("#page-nav"),
                className: "pagination pagination-sm",
                pageCount: Math.ceil(count / pageSize),
                beforeFun: function (targetNum) {
                    var data = {
                        name: '${param.name}'.trim(),
                        pageSize: pageSize,
                        currentPage: targetNum - 1
                    };
                    $.ajax({
                        url: "/identity/list/stu",
                        data: data,
                        dataType: "json",
                        type: "post",
                        success: function (result) {
                            if (result.success) {
                                createTable(result.data);
                            } else {
                                alert(result.errorMsg);
                            }
                        }
                    });
                }
            };
            createPageNav(opt);
        }

        createPage('${requestScope.count}');
        createTable(JSON.parse('${requestScope.list}'));
    });
    function upAllStu() {
        $.ajax({
            url: '/file/stu',
            type: 'POST',
            cache: false,
            data: new FormData($('#uploadForm')[0]),
            processData: false,
            contentType: false
        }).done(function(res) {
        }).fail(function(res) {});
    }
 **/