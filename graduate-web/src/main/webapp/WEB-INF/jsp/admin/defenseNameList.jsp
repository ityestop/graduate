<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>答辩名单</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>

<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">数据导出</li>
            <li class="active">答辩名单</li>
        </ol>
        <div class="page-header">
            <h4><strong>答辩名单 </strong>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>
                        <small>小组名</small>
                    </th>
                    <th>
                        <small>组员人数</small>
                    </th>
                    <th>
                        <small>学生总数</small>
                    </th>
                    <th>
                        <small>小组秘书</small>
                    </th>
                    <th>
                        <small class="center">操作</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${teaGroup}" var="teaGroup">
                    <tr>
                        <td>
                            <small>${teaGroup.tgTea }小组</small>
                        </td>
                        <td>
                            <small>${teaGroup.teaNum }</small>
                        </td>
                        <td>
                            <small>${teaGroup.stuNum }</small>
                        </td>
                         <td>
                            <small>${teaGroup.secretary }</small>
                        </td>
                        <td>
                        	<a href="./${sessionScope.pathCode}/downloadDefense.do?tgId=${teaGroup.tgId}&name=${teaGroup.tgTea}小组"><small><i class="clip-download-2"></i>  导出数据  </small></a>
                     	 </td>

                    </tr>
                </c:forEach>
            </table>

        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="teaGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form action="./admin/createTeaGroup.do">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">创建答辩小组--选择小组长</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control search-select" id="teaId"
                            name="teaId">
                        <c:forEach items="${teacherList}" var="teacher">
                            <option value="${teacher.teaId }">${teacher.teaName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>

            </div>
        </div>
    </form>
</div>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();

    });

</script>
</body>
</html>
