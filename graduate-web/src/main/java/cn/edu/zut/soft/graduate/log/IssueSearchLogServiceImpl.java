package cn.edu.zut.soft.graduate.log;

import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author chuchuang
 * @date 16/11/30
 */
@Component
public class IssueSearchLogServiceImpl implements LogService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private static final String logStr = "id:{},name:{},role:{},select:{}";

    @Override
    public void behaviour(HttpServletRequest httpRequest, HttpServletResponse response, Map map) {
        LoginVO loginVO = (LoginVO) getSession(httpRequest).getAttribute(IKEY.USER);
        IssueQuery issueQuery = (IssueQuery) map.get(IssueQuery.class);
        logger.info(logStr,new Object[]{loginVO.getId(),loginVO.getName(),loginVO.getRole(),issueQuery});
    }

    @Override
    public HttpSession getSession(HttpServletRequest httpRequest) {
        return httpRequest.getSession();
    }
}
