//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.model.Teacher;
//import cn.edu.zut.soft.graduate.core.utils.DataLS;
//import cn.edu.zut.soft.graduate.core.utils.MDUtil;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpSession;
//import java.util.Date;
//
//
///**
// * Created by cc on 15/11/5.
// */
//
//@Controller
//@RequestMapping("/")
//public class LoginController implements ConfigDo {
//
//    //redirect:转发
//
//    @Resource
//    private ServiceFit serviceFit;
//
//    private ResultDo resultDo;
//
//    @RequestMapping("index")
//    public String index(){
//        return "./index";
//    }
//    @RequestMapping("login")
//    public ModelAndView login(@RequestParam("p") String p,@RequestParam("u") String u,HttpSession session){
//        Role role = null;
//        Date date=new Date();
//        if (AdminUser.equals(u)){
//            role = Role.ADMIN;
//        }else if (u.length() == 12){
//            role = Role.STU;
//        }else {
//            role = Role.TEA;
//        }
//        resultDo = serviceFit.getEmpService().verify(u.trim(), MDUtil.MD5Tools(p.trim()),role);
//        if (resultDo.isSuccess()){
//            session.setAttribute("user",resultDo.getResult());
//            session.setAttribute("role",role);
//            session.setAttribute("pathCode",role.getName());
//            if (role == Role.TEA || role == Role.STU || role == Role.ADMIN){
//                session.setAttribute("functionMap", DataLS.functionMap);
//                if(role == Role.TEA){
//                	Teacher teacher=(Teacher)resultDo.getResult();
//                	resultDo=serviceFit.getTeacherGroupService().teaGroupTgIdForTeacher(teacher.getTeaId());
//                	if(resultDo.isSuccess()){
//                		session.setAttribute("doucking", resultDo.getResult());
//                	}
//                }
//            }
//
//            return new ModelAndView("redirect:./"+role.getName() + "/home.do");
//        } else {
//            return new ModelAndView("index","mag",resultDo.getMessage());
//        }
//    }
//
//    @RequestMapping("logout")
//    public String logout(HttpSession session){
//        session.invalidate();
//        return "./index";
//    }
//
//}
