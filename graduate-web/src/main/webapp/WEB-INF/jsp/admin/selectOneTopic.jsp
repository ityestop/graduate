<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>题目详情</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <script type="text/javascript" src="IAssets/js/selectTopic.js"></script>
  
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">题目审核</li>
        </ol>
        <div class="page-header">
            <h3>题目审核</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table class="table">
                <tbody>
                <tr>
                    <td width="10%">
                        <small>题目标题:</small>
                    </td>
                    <td>
                        <small>${topicInfo.topTitle}</small>
                    </td>
                </tr>
               <tr>
                    <td>
                        <small>校内教师:</small>
                    </td>
                    <td>
                        <small>${topicInfo.teacher.teaName}</small>
                        <c:if test="${topicInfo.teacher.teaId eq '' or topicInfo.teacher.teaId == null}">
                        <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                            分配教师
                        </button>
                        </c:if>
                    </td>
                </tr>
                  <c:if test="${topicInfo.topOutTeacher ne null}">
                    <tr>
                    <td>
                        <small>校外教师:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.topOutTeacher}
                        </small>
                    </td>
                </tr>
                 </c:if>
                <c:choose>
                    <c:when test="${topicInfo.topStatus eq 3 }">
                        <tr class="danger">
                            <td>
                                <small>管理员批语</small>
                            </td>
                            <td>
                                <small><font color="red">${topicInfo.topRemark}</font></small>
                            </td>
                        </tr>
                    </c:when>
                </c:choose>
                <tr>
                    <td>
                        <small>课题类型:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.topType}</small>
                    </td>
                </tr>
                <tr>
                    <td>
                        <small>课题来源:</small>
                    </td>
                    <td>
                        <c:if test="${topicInfo.topTopicType eq false }">
                            <small>社会服务</small>
                        </c:if>
                        <c:if test="${topicInfo.topTopicType eq true }">
                            <small>科研项目</small>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <small>课题人数:</small>
                    </td>
                    <td>
                        <small> ${topicInfo.topCount}</small>
                    </td>
                </tr>
                <tr>
                    <td>
                        <small>技术领域:</small>
                    </td>
                    <td>
                        <c:forEach items="${topicInfo.topTechnophobeList}" var="t">
                            <small> ${t.techName}、</small>
                        </c:forEach>
                    </td>
                </tr>
				<c:if test="${SGTLink != null && SGTLink.size() > 0}">
               
                <c:forEach items="${SGTLink}" var="li">
                        
                        <tr>
                            <td>
                                <small>${li.choStuGroup.head.stuName}的小组</small>
                            </td>
                            <td>
                                <ul>
                                    <c:forEach items="${li.choStuGroup.studentList}" var="lis">
                                        <li>
                                            <small>${lis.stuName} (<font color="red"> 电话：${lis.stuTell}</font>)----
                                                    ${lis.stuStudentId} ---- ${lis.stuClass}</small>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                        </tr>
				</c:forEach>
				</c:if>
                <tr>
                    <td>项目背景:</td>
                    <td>
                        <small>
                            <pre>${topicInfo.topContent}</pre>
                        </small>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <c:forEach items="${topicInfo.childTopicList}" var="c">
                            <strong>
                                <small> ${c.name}</small>
                            </strong>
                            <small>
                                <pre>${c.content}</pre>
                            </small>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <c:if test="${topicInfo.status.index == 1}">
                            <form action="./admin/submitAdTopic.do" method="post">
                                <input type="hidden" value="${topicInfo.topId }" name="id">
                                <input type="hidden" value="2" name="status">
                                <label class="col-sm-3 control-label"></label>

                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-blue btn-block" id="submit">
                                        <span><i class="clip-checkmark-2  "></i>   通过</span>
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-blue btn-block"
                                            data-toggle="modal" data-target="#JoinStuGroupModal">
                                        <span><i class=" clip-cancel-circle  "></i>    不通过</span>
                                    </button>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-blue btn-block"
                                            onclick="window.location.href='javascript:history.go(-1);'">
                                        <span><i class=" clip-arrow-right-2  "></i> 返回</span>
                                    </button>
                                </div>
                            </form>
                        </c:if>
                        <c:if test="${topicInfo.status.index != 1}">

                        </c:if>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="form-group">
                <label class="col-sm-10 control-label"></label>

                <div class="col-sm-2">
                    <c:choose>
                        <c:when test="${topicInfo.topStatus eq 2}">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </c:when>
                        <c:when test="${topicInfo.topStatus eq 3}">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='./${sessionScope.pathCode}/selectTopic.do?topStatus=null&teacherId=${ topicInfo.topTeacherId}'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </c:when>
                        <c:when test="${topicInfo.topStatus eq 0}">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="window.location.href='javascript:history.go(-1);'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </c:when>
                    </c:choose>
                </div>
                </from>
            </div>

        </div>
    </div>

    <div class="modal fade" id="JoinStuGroupModal" tabindex="-1" role="dialog"
         aria-labelledby="alterModalLabel">
        <div class="modal-dialog" role="document">
            <form action="./admin/submitAdTopic.do" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="alterModalLabel">管理员批语</h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-horizontal">
                            <input type="hidden" value="${topicInfo.topId }" id="depId" name="id">
                            <input type="hidden" value="3" name="status">

                            <div class="form-group">
                                <label class="col-sm-2 col-sm-offset-1 control-label">批语:</label>

                                <div class="col-sm-8">
                                    <textarea id="editor_id" name="details"
                                              style="width:100%;height:220px;"></textarea>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                        </button>
                        <button type="submit" class="btn btn-primary" id="submit1">提交</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form action="./admin/submitNewTea.do">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">分配教师</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="topId" value="${topicInfo.topId}">
                   分配教师: 
                   <select class="form-control search-select" id="teaId"
								name="teaId">
								<c:forEach items="${teaList}" var="teacher">
								<option value="${teacher.teaId }">${teacher.teaName}</option>
								</c:forEach>
							</select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>

            </div>
        </div>
    </form>
</div>
<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			FormElements.init();
		});
	</script>
</body>
</html>
