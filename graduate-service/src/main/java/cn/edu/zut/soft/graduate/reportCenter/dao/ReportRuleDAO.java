package cn.edu.zut.soft.graduate.reportCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportRule;
import org.springframework.stereotype.Component;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
@Component
public interface ReportRuleDAO<T extends ReportRule> extends BasicDAO<Integer,ReportRule> {

}
