<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/12/10
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>毕业设计管理平台</title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">学生列表</li>
        </ol>
        <div class="page-header">
            <h3>选报学生列表</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <div class="row"></div>
            <div class="panel-body">
                <table class="table table-hover"
                       style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                    <thead>
                    <tr>
                        <th width="5%"><small>编号</small></th>
                        <th width="15%">
                            <small>学号</small>
                        </th>
                        <th>
                            <small>学生姓名</small>
                        </th>
                        <th width="5%">
                            <small>性别</small>
                        </th>
                        <th>
                            <small>班级</small>
                        </th>
                        <th>
                            <small>联系方式</small>
                        </th>
                        <th >
                            <small>小组</small>
                        </th>
                        <th width="15%">
                            <small>课题</small>
                        </th>
                        <th>
                            <small>任务</small>
                        </th>
                        <th>
                            <small>操作</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="s" varStatus="i">
                        <tr>
                            <input type="hidden" class="stuId" value="${s.stuId}">
                            <input type="hidden" class="topId" value="${s.topId}">
                            <td><small>${i.index + 1}</small></td>
                            <td>
                                <small>${s.stuStudentId}</small>
                            </td>
                            <td>
                                <small>${s.stuName}</small>
                            </td>
                            <td>
                                <small>${s.stuSex ? "女" : "男"}</small>
                            </td>
                            <td>
                                <small>${s.stuClass}</small>
                            </td>

                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.stuTell}">
                                <small>${s.stuTell}</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.sgSName}的小组">
                                <small>${s.sgSName}的小组</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.topicTitle}">
                                <small>${s.topicTitle}</small>
                            </td>
                            <td id="childTop" style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.childTopicTitle}">
                                <small>${s.childTopicTitle}</small>
                            </td>
                            <c:choose>
                                <c:when test="${s.childTopicTitle eq null }">
                                    <td>
                                        <small>
                                        <c:if test="${sessionScope.functionMap['assigntask'].status eq true }">
                                        <a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#assignChildTopic">分配任务</a>
                                        </c:if>
                                        <c:if test="${sessionScope.functionMap['assigntask'].status eq false }">  
                                        <a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#functionMessage">分配任务</a>
                                        </small>
                                        </c:if>  
                                    </td>
                                </c:when>
                                <c:when test="${s.childTopicTitle ne null }">
                                    <td>
                                        <small>
                                        <c:if test="${sessionScope.functionMap['assigntask'].status eq true }">
                                        <a href="./${sessionScope.pathCode}/restChildTopic.do?topId=${s.topId}">重置任务</a>
                                        </c:if>
                                        
                                        <c:if test="${sessionScope.functionMap['assigntask'].status eq false }">
                                        <a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#functionMessage">重置任务</a>
                                        </c:if>
                                        </small>
                                    </td>
                                </c:when>
                            </c:choose>

                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="assignChildTopic" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
    <form action="./${sessionScope.pathCode}/assignStudent.do"
          class="form-horizontal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">任务分配</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="stuId">
                    <div class="form-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">确认</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal fade" id="functionMessage" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">分配子标题功能</h4>
                </div>
                <div class="modal-body">
                	此功能未被开启，若有问题请联系管理员。
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
</div>


<script>
    $('#assignChildTopic').on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
        var $stuId = $p.find(".stuId");
        var $topId = $p.find(".topId");
        var $modal = $(this);
  //      alert($stuId.val() + "________" + $topId.val());
        $.ajax({
            url:"./teacher/childTopic.do",
            type:"post",
            dataType:"json",
            data:{
                topId: $topId.val()
            },
            success:function(data){
                var str = "";
                if (data.status == true){
                    var $top = data.topic;
                    var $cit = $top.childTopicList;
//                    alert($cit.length);
                    for (var i = 0; i< $cit.length;i++){
                        if ($cit[i].studentId == null || $cit[i].studentId == 'null'){
                            str += "<div><label> <input type='radio' value='"+$cit[i].id+"' name='childId' class='grey' >" + $cit[i].name + "</label>";
                            str += "<div class='col-sm-12'> <textarea id='editor_id' style='width:100%;height:150px;' disabled='disabled'>"+ $cit[i].content+"</textarea></div> </div>"
                        }
                    }
                    $modal.find(".modal-title").html($top.topTitle + "的任务分配");

                    if(str == ""){
                        str = "课题已经分配完.";
                    }

                }else{
                    str = "查询数据错误";
                }
                $modal.find(".modal-body .form-group").html(str);
                $modal.find("input[name='stuId']").val($stuId.val());

            }
        });
    });
</script>
</body>
</html>
