$(function () {

    var history = {};
    $(function () {
       $.ajax({
           type:'POST',
           url:'/issue/history',
           dataType:'json',
           success:function (result) {
               if(result.success){
                   history = result.data;
                   historyList();
               }else {
                   alert(result.errorMsg);
               }
           }
       })
    });

    function historyList() {
        $.each(history,function (key,value) {
            var tr_h = $(getTr());
            var hval = value;
            var td_h_title = $(getTd());
            td_h_title.append(hval.title);
            td_h_title.attr('title', hval.title);
            td_h_title.attr('style', 'overflow:hidden;white-space:nowrap;text-overflow:ellipsis;');
            tr_h.append(td_h_title);
            var td_h_size = $(getTd());
            td_h_size.append(hval.task_size);
            tr_h.append(td_h_size);
            var td_h_status = $(getTd());
            td_h_status.append("历史数据");
            tr_h.append(td_h_status);
            var  td_h_child = $(getTd());
            td_h_child.append(hval.childNames);
            tr_h.append(td_h_child);
            var td_h_update =$(getTd());
            var td_h_u_id = $(getId());
            td_h_update.append(td_h_u_id);
            td_h_u_id.attr("href","/issue/historyToSave?id="+hval.id);
            td_h_u_id.append("copy入库");
            tr_h.append(td_h_update);
            $('#table').append(tr_h);
            tr_h.append('');
        })
    }

    function getTr() {
        return "<tr></tr>";
    }

    function getTd() {
        return "<td></td>";
    }

    function getId() {
        return "<a href='#'></a>"
    }


});