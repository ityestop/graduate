package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.InfoType;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by chuchuang on 16/10/23.
 */
public class WitnessStrategy implements InfoStrategy {
    @Override
    public InfoType getType() {
        return InfoType.Audit;
    }

    @Override
    public String getKey() {
        return "witness";
    }

    @Override
    public String getValue() {
        return "";
    }

    @Override
    public String toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("infoType",getType());
        jsonObject.put("key",getKey());
        jsonObject.put("value",getValue());
        return jsonObject.toJSONString();
    }
}
