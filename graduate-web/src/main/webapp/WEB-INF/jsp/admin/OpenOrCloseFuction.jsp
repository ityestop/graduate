<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
<title></title>
<script type="text/javascript" src="IAssets/js/topoclist.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">功能设置</li>
			</ol>
			<div class="page-header">
				<h3>功能设置</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
				<div class="panel-body">
					<table id="sample-table-1" class="table table-hover"style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all" >
						<thead>
							<tr>
								<th><small>编号</small></th>
								<th><small>功能名称</small></th>
								<th><small>状态</small></th>
								<th><small>操作</small></th>
							</tr>
							<c:forEach items="${functionList}" var="function" varStatus="i">
							<input type="hidden" class="functionId" value="${function.id}">
							<tr>	
							<td><small>${i.index + 1 }</small></td>						
							<td><small>${function.title }</small></td>
							<td><small>${function.status == false ? "未开启":"开启状态"}</small></td>
							<td>
							<c:choose>
							<c:when test="${function.action eq 'teaEdit' }">
							<small>
							<c:if test="${function.status eq true }">
							开启 | <a href="./${sessionScope.pathCode}/updateEditStatus.do?id=${function.id}&status=false">关闭</a>
							</c:if>
							<c:if test="${function.status eq false }">
							未开启 | <a href="./${sessionScope.pathCode}/updateEditStatus.do?id=${function.id}&status=true">开启</a>
							</c:if>
							</small>
							</c:when>
							<c:when test="${function.action eq 'reportSubmit' }">
							<small>
							<c:if test="${function.status eq true }">
							开启 | <a href="./${sessionScope.pathCode}/updateEditStatus.do?id=${function.id}&status=false">关闭</a>
							</c:if>
							<c:if test="${function.status eq false }">
							未开启 | <a href="./${sessionScope.pathCode}/updateEditStatus.do?id=${function.id}&status=true">开启</a>
							</c:if>
							</small>
							</c:when>
							<c:when test="${function.action eq 'startTime' }">
							<small><a href="javaScript:void(0);" data-toggle="modal" data-target="#updateWeekTime">更改开始时间</a></small>
							</c:when>
							<c:when test="${function.action eq 'continued' }">
							<small><a href="javaScript:void(0);" data-toggle="model" data-target="#updateWeekCount">更改所需周次</a></small>
							</c:when>
							<c:when test="${function.action eq 'assigntask'}">
							<small>
							<c:if test="${function.status eq true }">
							开启 | <a href="./${sessionScope.pathCode}/updateEditStatus.do?id=${function.id}&status=false">关闭</a>
							</c:if>
							<c:if test="${function.status eq false }">
							未开启 | <a href="./${sessionScope.pathCode}/updateEditStatus.do?id=${function.id}&status=true">开启</a>
							</c:if>
							</small>
							</c:when>
							</c:choose>
							</td>
							</tr>
							</c:forEach>
						</thead>
					</table>
				</div>
		</div>
	</div>
</body>
</html>
