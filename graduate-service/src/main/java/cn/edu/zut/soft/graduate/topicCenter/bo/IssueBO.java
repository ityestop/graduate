package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.IssueVO;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.IssueTakeWitness;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
public interface IssueBO extends BasicBO<Issue,IssueQuery> {

    /**
     * 保存课题信息
     * @param issueVO
     * @param loginVO
     * @return
     */
    CommonResult<IssueVO> save(IssueVO issueVO,LoginVO loginVO);

    CommonResult<IssueVO> update(IssueVO issueVO,String operator);

    /**
     * 检索携带审核专家
     * 检索一致
     */
    CommonResult<List<IssueTakeWitness>> findByQueryTakeWitness(IssueQuery issueQuery);

    /**
     * 课题状态改变
     */
    Issue changeIssuePhase(LoginVO loginVO, Integer id, IssuePhase.Handle handle);

    Integer batchChangeIssuePhase(LoginVO loginVO,IssuePhase.Handle handle,List<Integer> ids);

    CommonResult<List<Issue>> queryByIds(List<Integer> ids);

    /**
     * 功能描述:根据分配关联表去检索未被分配<审核中>题目
     * @return
     */
    List<Issue> noExpertIssues(String role);

    /**
     * 功能描述:根据教师ID查询该教师下已分配的题目
     * 功能说明:教师段
     * @param teaId
     * @return
     */
    List<Issue> getExpertIssueByTeaID(Integer teaId, String role);

    /**
     * 功能描述:拒绝审核中题目并且添加评语
     * @param handle
     * @param id
     * @param operator
     * @param remark
     * @return
     */
    IssueVO refuseAndRemark(LoginVO loginVO,IssuePhase.Handle handle, String id, String operator, String remark);

//    List<Issue> findQuery(IssueQuery issueQuery,List<IssuePhase> issuePhaseList);

    /**
     * 教师小组的课题列表
     */
    CommonResult<List<Issue>> queryTeaGroupAll(Integer groupId,IssueQuery issueQuery);

    /**
     * 带blob检索
     * @param issueQuery
     * @return
     */
    CommonResult<List<Issue>> findByQueryWith(IssueQuery issueQuery);

    /**
     * 功能描述: 分配子标题
     * @param id
     * @param childId
     * @return
     */
    Issue assignChildTopic(Integer id, String childId, Integer stuId, LoginVO loginVO);

    /**
     * 功能描述:重置子标题
     * @param id
     * @param childId
     * @param stuId
     * @param loginVO
     * @return
     */
    Issue restChildTopic(Integer id, String childId, Integer stuId, LoginVO loginVO);

    IssueVO getCacheIssueVO(Integer id);

}
