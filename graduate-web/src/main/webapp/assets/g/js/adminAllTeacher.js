/**
 * Created by chuchuang on 16/11/5.
 */

// document.write("<script src='/assets/g/js/paging.js'></script>");



function addNewTea() {
    var $error = $(".errorHandler");
    var $noid = $("input[name='noid']");
    var $addName = $("input[name='addName']");
    var $addPhone = $("input[name='addPhone']");
    alert($addName.val());
    if ($addPhone.val() == null || $addPhone.val().trim() == '') {
        alert("手机号不能为空");
        return false;
    }
    $noid.change(function () {
        $error.hide();
    });
    $addName.change(function () {
        $error.hide();
    });
    $addPhone.change(function () {
        $error.hide();
    });
    $.ajax({
        url: "/identity/save/tea",
        type: "post",
        data: {
            noId: $noid.val(),
            name: $addName.val(),
            phone: $addPhone.val()
        },
        dataType: "json",
        success: function (result) {
            if (result.success) {
                $("addModal").modal('hide');
                history.go(0);
            } else {
                $($error.find("span")).html(result.errorMsg);
                $error.show();
            }
        }
    });

}