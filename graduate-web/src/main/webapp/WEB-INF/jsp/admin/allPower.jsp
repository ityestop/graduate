<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 15/11/7
  Time: 18:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">权限管理</li>
        </ol>
        <div class="page-header">
            <h3>权限信息</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>权限名称</th>
                    <th>权限地址</th>
                    <th>权限值</th>
                    <th>权限备注</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${powerList}" var="p">
                    <tr>
                        <td>${p.powerName}</td>
                        <td>${p.powerUrl}</td>
                        <td>${p.common}</td>
                        <td>${p.powerDesc}</td>
                        <td>
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a href="./${sessionScope.pathCode}/powerUpDetail.do?id=${p.id}">
                                    <i class="fa fa-info-circle"></i>查看详情
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
