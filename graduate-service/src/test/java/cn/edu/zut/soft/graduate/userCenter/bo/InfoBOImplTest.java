package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.AdminInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.design.WitnessStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.Classes;
import cn.edu.zut.soft.graduate.core.vo.Owner;
import cn.edu.zut.soft.graduate.core.vo.TeacherVO;
import cn.edu.zut.soft.graduate.core.vo.Witness;
import cn.edu.zut.soft.graduate.userCenter.dao.InfoDAO;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by chuchuang on 16/10/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/bean-all.xml")
public class InfoBOImplTest {

    @Resource
    private InfoBO infoBO;

    @Resource
    private InfoDAO<Info> infoDAO;

    @Test
    public void queryByUser() throws Exception {
        System.out.println(infoBO.queryByUser(1));
    }

    @Test
    public void save() throws Exception {
//        Classes classes = new Classes();
//        classes.createMajor("测试专业编号","测试专业名称");
//        classes.createTeam("测试班级编号","测试班级名称");
//        InfoStrategy infoStrategy = new BasicInfoStrategy(InfoType.Basic,"classes",JSONObject.toJSONString(classes));
//        System.out.println(infoBO.save(infoStrategy,1,"test"));

        InfoStrategy infoStrategy = AdminInfoStrategy.admin;
        System.out.println(infoBO.save(infoStrategy,35,"楚闯"));

    }

    @Test
    public void get() throws Exception {
        Info info = infoBO.get(1);
//        Classes classes = JSONObject.parseObject(info.getValue(),Classes.class);
//        System.out.println(classes);
    }

    @Test
    public void findByQuery() throws Exception {
        InfoQuery infoQuery  = new InfoQuery();
        infoQuery.setLimit(Integer.MAX_VALUE);
        List<Info> infoList = infoDAO.selectByExample(new Example().cleanPage());
        for (Info info : infoList){
            try {
                JSONObject json = JSONObject.parseObject((String) info.getValue());
                info.setValue((String) json.get("key"));
                System.out.println(infoBO.update(info,"楚闯"));
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    @Test
    public void delete() throws Exception {
        CommonResult<List<Witness>> result = infoBO.getAllWitness(new WitnessStrategy());
        for (Witness w:result.getData()){
            System.out.println(w.getName());
        }
    }

    @Test
    public void awardWitness() throws Exception {
        System.out.println(infoBO.getAllWitness(new WitnessStrategy()));
    }

    @Test
    public void testnotWitenssTeaMessage(){
        String[] param = {""};
        CommonResult<List<TeacherVO>> result = infoBO.notWitenssTeaMessage();
        for (TeacherVO t:result.getData()){
            System.out.println(t.getName());
            System.out.println(t.getJobTitle());
        }

    }

    @Test
    public void syncTeaIdToTeaName(){
        System.out.println(infoBO.syncTeaIdToTeaName());
    }


    @Test
    public void changeClass(){
        Example example = new Example();
        example.createCriteria().andEqualTo("`key`", IKEY.CLAZZ).andEqualTo("del","0");
        List<Info> classList = infoDAO.selectByExample(example);
        for (Info info : classList){
            if (info.getValue().equals("卓越131")){
                info.setValue("RB软工13级卓越班");
                infoBO.update(info,"yxt");
                System.out.println(info.getIdentityName() + " success");
            }
        }
    }
}