package cn.edu.zut.soft.graduate.reportCenter.query.Impl;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.annotation.QueryParameter;
import cn.edu.zut.soft.graduate.reportCenter.query.ReportRuleQuery;

import java.util.Collection;

/**
 * @author cc
 * @Date 2017/2/19
 * @value 1.0
 */
public class SimpleReportRuleQuery extends Query implements ReportRuleQuery {

    @QueryParameter(set = true,equal = QueryParameter.Type.Equal, rowName = "id")
    private Collection<Integer> ids;

    @QueryParameter(equal = QueryParameter.Type.Equal,rowName = "`type`")
    private Integer type;

    @QueryParameter(equal = QueryParameter.Type.Equal, rowName = "num")
    private Integer num;

    @Override
    public Collection<Integer> getIds() {
        return ids;
    }

    public void setIds(Collection<Integer> ids) {
        this.ids = ids;
    }

    @Override
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
