package cn.edu.zut.soft.graduate.topicCenter.bo.impl;

import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.model.Impl.CompanyProjectRequest;
import cn.edu.zut.soft.graduate.core.query.CompanyProjectRequestQuery;
import cn.edu.zut.soft.graduate.topicCenter.bo.CompanyProjectRequestBO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

/**
 * Created by cc on 2016/12/6.
 */
public class CompanyProjectRequestBOImplTest extends BaseTest {

    @Autowired
    private CompanyProjectRequestBO companyProjectRequestBO;

    @Test
    public void successRequest() throws Exception {

    }

    @Test
    public void isQualified() throws Exception {

    }

    @Test
    public void groupRequest() throws Exception {

    }

    @Test
    public void syncGroupNum() throws Exception {
        CompanyProjectRequestQuery crq = new CompanyProjectRequestQuery();
//        int count = companyProjectRequestBO.countByQuery(crq);
        crq.clear();
        Print.println(companyProjectRequestBO.syncGroupNum(crq), CompanyProjectRequest.class);
    }

}