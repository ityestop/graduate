<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<html>
<head>
    <title>题目审核管理</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">子题目分配统计</li>
        </ol>
        <div class="page-header">
            <h3>子题目分配统计</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <%--<!--  panel-scroll" style="height:215px" -->--%>
            <sf:form id="condition" class="form-horizontal" action="./${sessionScope.pathCode}/teacherFpRw.do"
                     method="post" commandName="teacherTJ">
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    教师姓名 </label>

                <div class="col-sm-3">
                    <sf:select class="form-control search-select" path="teaName" id="teaName">
                        <sf:option value="" label="全部"/>
                        <sf:options items="${teacherList}" itemValue="teaName" itemLabel="teaName"/>
                    </sf:select>
                </div>
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
                    <%--</div>--%>
                    <%--</div>--%>
                </sf:form>
            </div>
            <div class="panel-body">
                <div class="row"></div>
                <div class="panel-body">
                    <table class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                        <thead>
                        <tr>

                            <th width="5%">
                                <small>编号</small>
                            </th>
                            <th width="10%">
                                <small>教师工号</small>
                            </th>
                            <th>
                                <small>教师姓名</small>
                            </th>
                            <th>
                                <small>教师职称</small>
                            </th>
                            <th>
                                <small>未分配任务数</small>
                            </th>
                            <th>
                                <small>联系电话</small>
                            </th>
                            <th width="15%">
                                <small>操作</small>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${list}" var="t" varStatus="i">
                            <tr>
                                <td>${i.index +1 }</td>
                                <td>${t.teaTeacherId}</td>
                                <td>${t.teaName}</td>
                                <td>${t.jobTitle.name}</td>
                                <td>${t.stuAll}</td>
                                <td>${t.teaTell}</td>
                                <td>
                                    <small>
                                        <a href="./${sessionScope.pathCode}/lookTeaStudent.do?teaId=${t.teaId}&b=true">查看详情</a><br/>
                                    </small>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="form-group right">
                        <div class="row" style="text-align: right; height: 40px">
                            <page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"
                                              curPage="${pageNum}" formId="condition"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script
        src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });

</script>


</body>
</html>

