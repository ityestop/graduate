package cn.edu.zut.soft.graduate.groupCenter.bo.Impl;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.LoginMode;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.model.Impl.CreateGroup;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author chuchuang
 * @date 16/11/20
 */
public class GroupBOImplTest extends BaseTest {

    @Resource
    private GroupBO groupBO;

    @Resource
    private IdentityBO identityBO;

    @Test
    public void associationIDGroup() throws Exception {

    }

    @Test
    public void batchAssociationIDGroup() throws Exception {

    }

    @Test
    public void userCreateGroup() throws Exception {

        LoginVO loginVO = new LoginVO();
        loginVO.setId(92);
        loginVO.setExpert(false);
        loginVO.setName("杨晓天");
        loginVO.setRole(Role.stu.name());
        loginVO.setLoginMode(LoginMode.PHONE);
        CreateGroup createGroup = new CreateGroup();
        createGroup.setUser(loginVO);
        createGroup.setRole(Role.stu);
        createGroup.setRunner(Role.Runner.user);
        Print.println(groupBO.userCreateGroup(createGroup),Group.class);

    }

    @Test
    public void get() throws Exception {

    }

    @Test
    public void update() throws Exception {

    }

    @Test
    public void findByQuery() throws Exception {

    }

    @Test
    public void delete() throws Exception {

        Print.println(groupBO.delete(30,"test"), CommonResult.class);

    }
    @Test
    public void findNoIssueStuGroupTest() throws Exception {
        GroupQuery groupQuery = new GroupQuery();
        groupQuery.clear();
        groupQuery.setStatus(IssueSource.school);
        Print.println(groupBO.findNoIssueStuGroup(groupQuery).getData(),Group.class);

    }

}