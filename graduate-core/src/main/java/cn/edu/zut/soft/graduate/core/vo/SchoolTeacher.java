package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class SchoolTeacher {
    private Integer id;
    private String name;

    public SchoolTeacher(){

    }

    public SchoolTeacher(Integer id,String name){
        this.id= id;
        this.name = name;
    }
}
