<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>编辑专家任务</title>
    <script src="/assets/g/js/admin/editAuditIssue.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./admin"> 首页 </a></li>
            <li class="active">专家管理</li>
            <li class="active">编辑专家任务</li>
        </ol>
        <div class="page-header">
            <h4>编辑专家任务</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="panel-body">
        <div class="form-group">
            <input type="hidden" value="${userId}" id="userId_audit" />
            <table class="table table-hover"
                   style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                    <th width="35%" class="center">
                        <small>题目名称</small>
                    </th>
                    <th>
                        <small>题目来源</small>
                    </th>
                    <th>
                        <small>指导教师</small>
                    </th>
                    <th>
                        <small>题目状态</small>
                    </th>
                    <th>
                        <small>题目人数</small>
                    </th>
                    <th width="15%">
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <tbody id="auditIssuseTop">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
