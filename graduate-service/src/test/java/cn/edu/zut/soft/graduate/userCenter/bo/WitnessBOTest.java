package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.vo.WitnessData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * <p>文件名称：WitnessBOTest.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/11/7 下午6:01</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/bean-all.xml")
public class WitnessBOTest {
    @Autowired
    private WitnessBO witnessBO;
    @Test
    public void getWitnessData() throws Exception {

    }

    @Test
    public void getAllWitnessData() throws Exception {
        CommonResult<List<WitnessData>> result = witnessBO.getAllWitnessData();
        for (WitnessData w:result.getData()){
            System.out.println(w.getInfo().getIdentityName());
        }

    }

}