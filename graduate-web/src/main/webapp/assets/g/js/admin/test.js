document.write("<script src='/assets/g/js/config.js'></script>");
$(function () {

    customSearch({id:'#teacherIssueTable'});
});

function customSearch(opt) {
    var oTableInit = {};
    //初始化Table
    oTableInit.Init = function () {
        $(opt.id).bootstrapTable({
            url: '/identity/issue/list/tea',  //请求后台的URL（*）
            classes: 'table-no-bordered',
            method: 'get',   //请求方式（*）
            pagination: true,   //是否显示分页（*）
            sidePagination: "server",
            pageNumber: 1,   //初始化加载第一页，默认第一页
            pageSize: 10,   //每页的记录行数（*）
            pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
            search: false,
            queryParams: oTableInit.queryParams,
            dataType: "json",
            columns: [ {
                field: 'title',
                title: '课题名称'
            }, {
                field: 'identityName',
                title: '校内导师'
            }, {
                field: 'status',
                title: '课题状态',

            }, {
                field: 'kind',
                title: '课题来源',
            }, {
                field: 'taskSize',
                title: '课题任务数'
            },
                {
                    field: 'createTime',
                    title: '创建时间'
                }
            ],
            responseHandler: oTableInit.responseHandler,
            customSearch:customSearch,
            searchOnEnterKey:true,
            // showRefresh:true
        })
    };
    oTableInit.queryParams = function (params) {
        return { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            limit: params.limit, //页面大小
            offset: params.offset, //页码
            name:$("input[name='name']").val()
        };
    };
    oTableInit.responseHandler = function (res) {
        return {
            total: res.totalCount,
            fixedScroll: true,
            rows: res.data
        };
    };
    oTableInit.Init();
}