<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 2017/2/19
  Time: 下午4:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>周月报规则管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li></i> <a href="/admin/report/"> 周月报规则管理 </a></li>
            <li class="active"> 添加周月报 </li>
        </ol>
        <div class="page-header">
            <h3>添加周月报规则</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="panel-body">
        <form class="form-horizontal" id="form">
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    类型
                </label>
                <div class="col-sm-6">
                    <select class="form-control" name="type" title="类型">
                        <option value="1">周报</option>
                        <option value="2">月报</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    周次/月份
                </label>
                <div class="col-sm-6">
                    <input class="form-control" type="number" title="次数" name="num" value="1" min="1" max="18"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    重复
                </label>
                <div class="col-sm-6">
                    <select class="form-control" name="repeat" title="类型">
                        <option value="1">否</option>
                        <option value="0">是</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    开始时间
                </label>
                <div class="col-sm-6">
                    <input class="form-control" type="date" title="开始时间" name="startTime" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    结束时间
                </label>
                <div class="col-sm-6">
                    <input class="form-control" type="date" title="结束时间" name="endTime" value=""/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">提交</button>
                    <button type="reset" class="btn btn-default">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body">
                是否提交
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitForm()">提交</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    var $from = $("#form");
    function submitForm(){
        var url = "/report/rule";
        $.ajax({
            type: 'POST',
            url: url,
            data: $from.serialize(),
            dataType:'json',
            success:function (result) {
                if (result.success == true){
                    var data = result.data;
                    window.location.href = '/admin/report/listRule';
                }else {
                    alert(result.errorMsg);
                }
            }
        })

    }
</script>

</body>
</html>
