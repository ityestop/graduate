package cn.edu.zut.soft.graduate.reportCenter.service;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.model.Impl.ReportCount;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.*;
import cn.edu.zut.soft.graduate.reportCenter.query.SearchReportQuery;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportCenterService {

    /**
     * 添加一个新的报告规则
     * @param reportRuleDTO 报告规则
     * @param loginVO 调用用户
     * @return 是否添加成功
     */
    @Transactional
    boolean addReportRule(ReportRuleDTO reportRuleDTO, LoginVO loginVO);

    /**
     * 添加新的报告规则的集合
     * @param reportRuleDTOs 报告规则的集合
     * @param loginVO 调用用户
     * @return 是否添加成功
     */
    @Transactional
    boolean addReportRuleAll(Collection<ReportRuleDTO> reportRuleDTOs,LoginVO loginVO);

    /**
     * 获取报告规则
     * @param ruleId 规则id
     * @return 规则的详细属性
     */
    ReportRuleDTO getRule(Integer ruleId);

    /**
     * 批量获取报告规则
     * @param query 报告规则的查询条件
     * @return 批量的报告规则
     */
    List listRule(Query query);


//    /**
//     * 修改报告规则。如果存在报告之后的更新会产生数据的不一致,所以不建议使用当前方法
//     * @param reportRuleDTO 修改的规则
//     * @param loginVO 调用者
//     * @return 是否修改成功
//     */
//    @Deprecated
//    @Transactional
//    boolean updateReportRule(ReportRuleDTO reportRuleDTO,LoginVO loginVO);

    /**
     * 移除某个规则
     * @param ruleId 规则id
     * @param loginVO 调用用户
     * @return 移除的规则对象
     */
    @Transactional
    ReportRule removeReportRule(Integer ruleId, LoginVO loginVO);

    /**
     * 对某规则添加报告
     * @param reportDataDTO getRuleId() 规则
     * @param reportDataDTO 报告实际内容
     * @param loginVO 调用用户
     * @return 是否添加成功,编辑成功reportData为添加后的对象;如果时间小于开启时间,则返回false,如果大于结束时间,则保存改为补交状态
     */
    @Transactional
    boolean addReportData(ReportDataDTO reportDataDTO,LoginVO loginVO);

//    /**
//     * 根据时间获取符合时间的规则
//     * @param date 时间参数
//     * @return 根据时间获取的规则
//     */
//    List<ReportRule> findRuleByDate(Date date);

    /**
     * 根据时间获取符合时间的规则
     * @param date 时间参数
     * @param loginVO 时间参数
     * @return 根据时间获取的规则
     */
    List<ReportRule> findRuleByDateOfLoginVO(Date date,LoginVO loginVO);

    /**
     * 编辑报告内容
     * @param reportDataDTO 编辑后的报告内容
     * @param loginVO 调用用户
     * @return 是否编辑成功 编辑成功reportData为修改后的结果
     */
    @Transactional
    boolean updateReportData(ReportDataDTO reportDataDTO, LoginVO loginVO);

    /**
     * 移除报告
     * @param dataId 报告内容
     * @param loginVO 调用用户
     * @return 移除报告的对象
     */
    @Transactional
    ReportDataDTO removeReportData(Integer dataId,LoginVO loginVO);

    /**
     * 获取报告内容
     * @param dataId 报告编号
     * @return 报告详细内容
     */
    ReportDataDTO getReportData(Integer dataId);

    /**
     * 获取报告的集合
     * @param query 报告的检索参数
     * @return 报告集合
     */
    List<ReportDataDTO> listReportData(Query query);

    /**
     * 统计学生的报告状态
     * @param searchReportQuery 统计条件
     * @return 符合条件的学生报告
     */
    List<ReportDataSearchVO> statisticsStudentOfQuery(SearchReportQuery searchReportQuery);

    /**
     * 根据用户查询用户的报告数
     * @param id
     * @return
     */
    ReportCount countByUser(Integer id);


}
