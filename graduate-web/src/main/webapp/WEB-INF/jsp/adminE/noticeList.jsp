<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/10/26
  Time: 上午1:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>公告</title>
    <script src="/assets/g/js/adminNotice.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
                <li><i class="clip-home-3"></i> <a
                        href="/admin"> 首页 </a></li>
                <li class="active">公告信息</li>
            </ol>
            <div class="page-header">
                <h3>公告信息</h3>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
    </div>
    <!-- end: PAGE HEADER 頭部結束-->
    <%--<div class="container">--%>

    <div class="row">
        <div class="col-sm-12">
            <div class="col-md-8" id="noticeAll">
            </div>
            <div class="col-md-4">
                <aside class="sidebar">
                    <div class="input-group">
                        <input type="text" id="searchSelf" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-primary searchSelf" type="button">
                    <i class="fa fa-search"></i> 搜索
                </button>
            </span>
                    </div>
                    <hr/>
                    <h4>阅读榜Top20</h4>
                    <ul class="nav nav-list blog-categories">
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</div>

</body>
</html>
