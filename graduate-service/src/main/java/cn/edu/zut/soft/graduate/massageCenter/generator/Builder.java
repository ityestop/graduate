package cn.edu.zut.soft.graduate.massageCenter.generator;

import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Message;

/**
 * @author chuchuang
 * @date 16/11/17
 */
public interface Builder {
    Message assemblyMessage(Identity addressor, Identity recipients);
}
