<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>软件学院毕业设计管理平台</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>

<body>
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">指导推荐</li>
			</ol>
			<div class="page-header">
				<h3>指导推荐</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<table class="table table-hover"
					style="TABLE-LAYOUT: fixed; WORD-BREAK: break-all">
					<thead>
						<tr>

							<th width="7%"><small>姓名</small></th>
								<th width="13%"><small>学号</small></th>
								<th width="5%"><small>性别</small></th>
								<th width="13%"><small>班级</small></th>
								<th width="30%"><small>课题名称</small></th>
								<th width="10%"><small>推荐教师</small></th>
								<th width="10%"><small>推荐状态</small></th>
								<th width="7%"><small>操作</small></th>


						</tr>
					</thead>
					<tbody>

						<c:forEach items="${recommend}" var="s">
							<c:if test="${s.recommend eq'1' ||s.recommend eq '2'}">
								<tr>
									<td><small>${s.stuName}</small></td>
									<td><small>${s.stuStudentId}</small></td>
									<td><small> <c:if test="${s.stuSex eq true}">
								女
								</c:if> <c:if test="${s.stuSex eq false}">
								男
								</c:if>
									</small></td>
									<td><small>${s.stuClass}</small></td>
									<td><small>${s.topTitle}</small></td>
									<td><small>${s.whoRecommend}</small></td>
									<td><small>${s.recommend eq '2' ? "<small class='label label-success' style='font-size: 75% !important;'>确认答辩</small>"
                            : s.recommend eq '1'  ? "<small class='label label-warning' style='font-size: 75% !important;'>已被推荐</small>"
                            : "<small class='label label-inverse' style='font-size: 75% !important;'>未被推荐</small>" }
									</small></td>
									<c:if test="${Identity ne 0 && TName eq s.whoRecommend}">
										<td><small> <c:if test="${s.recommend eq '0' }">
													<a
														href="./${sessionScope.pathCode}/UpadteTeaRecommendOther.do?stuId=${s.stuId}&num=1&tgId=${Tgid}">确认</a>|取消 
									</c:if> <c:if test="${s.recommend eq '1' }">
							确认|<a
														href="./${sessionScope.pathCode}/UpadteTeaRecommendOther.do?stuId=${s.stuId}&num=0&tgId=${Tgid}&whoRecommend=1">取消</a>
												</c:if>
										</small></td>
									</c:if>
								</tr>

							</c:if>


						</c:forEach>

					</tbody>
				</table>
				<div class="form-group right"></div>
			</div>
		</div>
	</div>
	<c:if test="${Identity ne 0 }">
		<div class="row">
			<div class="col-md-12">
				<div class="panel-body">
					<table class="table table-hover"
						style="TABLE-LAYOUT: fixed; WORD-BREAK: break-all">
						<thead>
							<tr>

								<th width="7%"><small>姓名</small></th>
								<th width="13%"><small>学号</small></th>
								<th width="5%"><small>性别</small></th>
								<th width="13%"><small>班级</small></th>
								<th width="30%"><small>课题名称</small></th>
								<th width="10%"><small>推荐教师</small></th>
								<th width="10%"><small>推荐状态</small></th>
								<th width="7%"><small>操作</small></th>


							</tr>
						</thead>
						<tbody>

							<c:forEach items="${recommend}" var="s">
								<c:if test="${s.recommend eq '0'}">
									<tr>
										<td><small>${s.stuName}</small></td>
										<td><small>${s.stuStudentId}</small></td>
										<td><small> <c:if test="${s.stuSex eq true}">
								女
								</c:if> <c:if test="${s.stuSex eq false}">
								男
								</c:if>
										</small></td>
										<td><small>${s.stuClass}</small></td>
										<td><small>${s.topTitle}</small></td>
                                     <td><small>${"无"}</small></td>
										<td><small>${s.recommend eq '2' ? "<small class='label label-success' style='font-size: 75% !important;'>确认答辩</small>"
                            : s.recommend eq '1'  ? "<small class='label label-warning' style='font-size: 75% !important;'>已被推荐</small>"
                            : "<small class='label label-inverse' style='font-size: 75% !important;'>未被推荐</small>" }
										</small></td>
										<td><small><c:if test="${Identity ne 0}">
													<c:if test="${s.recommend eq '0' }">
														<a
															href="./${sessionScope.pathCode}/UpadteTeaRecommendOther.do?stuId=${s.stuId}&num=1&tgId=${Tgid }&whoRecommend=${whoRecommend}">确认</a>|取消 
									</c:if>
													<c:if test="${s.recommend eq '1' }">
							确认|<a
															href="./${sessionScope.pathCode}/UpadteTeaRecommendOther.do?stuId=${s.stuId}&num=0&tgId=${Tgid }&whoRecommend=1">取消</a>
													</c:if>
												</c:if> </small></td>

									

									</tr>
								</c:if>
							</c:forEach>

						</tbody>
					</table>
					<div class="form-group right"></div>
				</div>
			</div>
		</div>
	</c:if>
</body>
</html>
