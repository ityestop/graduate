package cn.edu.zut.soft.graduate.core.vo;

import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class IssueVO {
    private Issue issue;
    private IssueContent issueContent;

    public IssueVO(){

    }

    public IssueVO(Issue issue) {
        this.issue = issue;
        this.convert();
    }

    public IssueVO(Issue issue, IssueContent issueContent) {
        this.issue = issue;
        this.issueContent = issueContent;
    }

    public void convert(){
        assert issue != null;
        issueContent = JSONObject.parseObject(issue.getContent(),IssueContent.class);
        issue.setContent(null);
    }
}
