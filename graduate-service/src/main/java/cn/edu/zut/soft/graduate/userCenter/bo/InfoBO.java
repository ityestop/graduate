package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.TeacherVO;
import cn.edu.zut.soft.graduate.core.vo.Witness;

import java.util.List;
import java.util.Set;

/**
 * Created by chuchuang on 16/10/22.
 */
public interface InfoBO extends BasicBO<Info,InfoQuery> {

    /**
     * 根据用户检索出所有的info
     * @param id
     * @return
     */
    List<Info> queryByUser(Integer id);

    boolean getExpertById(Integer userId);

    List<Info> queryByUserInfoStrategy(Integer id, List<InfoStrategy> infoStrategy);

    List<Info> queryByInfoStrategy(List<InfoStrategy> infoStrategyList);


    /**
     * 保存某个基本信息
     * @param infoStrategy
     * @param id
     * @param operator
     * @return
     */
    Info save(InfoStrategy infoStrategy, Integer id,String operator);

    /**
     * 批量对某些用户打标
     * @param infoStrategy 标签
     * @param ids 用户集合
     * @param operator 打标者
     * @return
     */
    CommonResult saveBatch(InfoStrategy infoStrategy, Set<Integer> ids, String operator);

    int delete(InfoStrategy infoStrategy,String opterator);

    List<Info> delete(InfoStrategy infoStrategy, Integer id,String opterator);

    /**
     * 获取所有的专家
     * @return
     */
    CommonResult<List<Witness>> getAllWitness(InfoStrategy infoStrategy);

    /**
     * 获取不是专家教师详细信息
     * @return
     */
    CommonResult<List<TeacherVO>> notWitenssTeaMessage();

    List<Info> selectExampleWithBLOB (InfoQuery infoQuery);

    int syncTeaIdToTeaName();

    /**
     * <p>功能描述：根据key和ID获取信息</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-06-07 00:31:25</p>
     *
     * @param key
     * @param id
     * @return
     */
    List<Info> getInfo(String key, Integer id);
}
