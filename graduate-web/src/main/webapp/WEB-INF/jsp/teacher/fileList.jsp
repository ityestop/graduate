<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>软件学院毕业设计管理平台</title>
<link rel="stylesheet"
	href="assets/plugins/dropzone/downloads/css/dropzone.css">
<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>

	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 主页 </a>
				</li>
				<li class="active">文件模板</li>

			</ol>
			<br>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER -->
	<!-- start: PAGE CONTENT -->

	<%--<form action="manage/uploadDevice.do" method="post" enctype="multipart/form-data">  
					<input type="file" name="file" /> <input type="submit" value="Submit" />
				</form>  
				--%>
	<div class="col-sm-12">
		<!-- start: DROPZONE PANEL -->
		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th ><small>编号</small>
						</th>
						<th ><small>文档名称</small>
						</th>
						<th><small>类型</small>
						</th>
						<th><small>下载次数</small>
						</th>
						<th><small>上传时间</small>
						</th>
						<th><small>操作</small>
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${fileList}" var="file" varStatus="i">
						<tr>
							<td><small>${i.index + 1}</small></td>
							<td><small>${file.realName}</small></td>
							<td><small>${file.fileType}</small></td>
							<td><small>${file.view}</small></td>
							<td><small><jsp:useBean id="occurDate"
										class="java.util.Date" /> <jsp:setProperty name="occurDate"
										property="time" value="${file.createTime}" /> <fmt:formatDate
										value="${occurDate}" type="both" pattern="yyyy-MM-dd HH:mm:ss" />
							</td>
							<td><small><a href="./admin/file/download.do?id=${file.id}"><i class="clip-download"></i> 下载 </a></small></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<!-- end: PAGE CONTENT-->
	</div>

	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="assets/plugins/dropzone/downloads/dropzone.min.js"></script>
	<script src="assets/js/form-dropzone.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
		jQuery(document).ready(function() {
			//	Index.init();
			Dropzone.init();
		});
	</script>

</body>
</html>
