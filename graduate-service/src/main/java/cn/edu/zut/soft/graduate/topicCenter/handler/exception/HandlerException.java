package cn.edu.zut.soft.graduate.topicCenter.handler.exception;

/**
 * @author chuchuang
 * @date 16/11/27
 */
public class HandlerException extends RuntimeException {

    public HandlerException() {
        super();
    }

    public HandlerException(String message) {
        super(message);
    }

    public HandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public HandlerException(Throwable cause) {
        super(cause);
    }

    protected HandlerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static HandlerException create(String message){
        return new HandlerException(message);
    }

}
