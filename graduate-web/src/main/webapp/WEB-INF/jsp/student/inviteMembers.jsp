
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/11/28
  Time: 下午5:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>邀请队友</title>
</head>
<body>

<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">邀请团队成员</li>
        </ol>
        <div class="page-header">
            <h3>邀请团队成员</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
            <sf:form action="./${sessionScope.pathCode}/inviteMembers.do" method="post" commandName="student"
                     id="condition">
                <div class="form-group">
                    <label class="col-sm-1 control-label">
                        学生姓名 </label>

                    <div class="col-sm-3">
                            <%--<sf:select class="form-control search-select" path="teaId" >--%>
                            <%--<sf:option value="" label="全部"/>--%>
                            <%--<sf:options items="${teaList}" itemValue="teaId" itemLabel="teaName"/>--%>
                            <%--</sf:select>--%>
                        <sf:input path="stuName" cssClass="form-control"></sf:input>
                    </div>

                    <div class="col-sm-1 ">
                        <button type="submit" class="btn btn-primary"> 检索</button>
                    </div>
                </div>
            </sf:form>
        </div>
    </div>
   
    <div class="panel-body">
        <div class="row"></div>
        <div class="panel-body">
            <form action="./${sessionScope.pathCode}/submitInviteMembers.do" method="post">
            <table class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                    <th>
                        <small>
                            选项
                        </small>
                    </th>
                    <th width="20%">
                        <small>学号</small>
                    </th>
                    <th>
                        <small>姓名</small>
                    </th>
                    <th>
                        <small>性别</small>
                    </th>
                    <th>
                        <small>专业</small>
                    </th>
                    <th>
                        <small>班级</small>
                    </th>
                </tr>
                </thead>
                <tbody>
           
                <c:forEach items="${stuList}" var="stu">
                    <tr>
                        <th>
                            <small>
                                <label>
                                    <input type="checkbox" name="ids" value="${stu.stuId}">
                                </label>
                            </small>
                        </th>
                        <td>
                            <small>${stu.stuStudentId}</small>
                        </td>
                        <td>
                            <small>${stu.stuName}</small>
                        </td>
                        <td>
                            <small>${stu.stuSex ? "女" : "男"}</small>
                        </td>
                        <td>
                            <small>${stu.profession.proName}</small>
                        </td>
                        <td>
                            <small>${stu.stuClass}</small>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
             <div class="form-group">
        <label class="col-sm-4 "> </label>

        <div class="col-sm-8" style="text-align: right; height: 40px">
            <page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"
                              curPage="${pageNum}" formId="condition"/>
        </div>
    </div>
    			<div class="col-sm-2" style="text-align: left;">
                <button type="submit" class="btn btn-blue btn-block" >提交</button>
				</div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
