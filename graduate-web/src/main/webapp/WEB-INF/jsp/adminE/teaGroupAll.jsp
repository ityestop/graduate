<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教师答辩小组管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">教师答辩小组管理</li>
        </ol>
        <div class="page-header">
            <h4>教师答辩小组管理</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <div class="form-group">
                <div style="float: right">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                        <span><i class=" clip-plus-circle"></i> 创建小组</span>
                    </a>
                </div>
            </div>
            <div class="form-group">
                <table id="teacherTable" class="table table-container">
                </table>
            </div>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>

<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="addModalLabel">创建教师小组</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <%--<div class="row">--%>
                    <div class="form-group">
                        <div class="errorHandler alert alert-danger no-display">
                            <i class="fa fa-remove-sign"></i>
                            <span>错误</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">
                            请选择组长
                        </label>
                        <div class="col-sm-7">
                            <select class="form-control search-select" title="教师列表" id="teaId">
                                <option value="">请选择</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="agreeGroupLeader">确定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>


<!-- 分配小组成员Modal -->
<div class="modal fade" id="assignTeaGroup" tabindex="-1" role="dialog" aria-labelledby="addModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">分配答辩教师小组</h4>
            </div>
            <div class="modal-body" id="noGroupTable">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- 选择对接小组  -->
<div class="modal fade" id="parallelismTeaGroup" tabindex="-1" role="dialog" aria-labelledby="addModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">选择对接小组</h4>
            </div>
            <div class="modal-body" id="parallelismIdNoGroup">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">

    $("#agreeGroupLeader").click(function () {
        var $teaGroupLeaderId = $("#teaId").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/group/createGroup',
            data: {
                id: $teaGroupLeaderId,
                role: 'tea'
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }
        });
    });

    var columns = [
        {
            field: 'id',
            title: '编号'
        }, {
            field: 'identityName',
            title: '组长'
        }, {
            field: 'count',
            title: '小组人数'
        }, {
            field: 'parallelism',
            title: '对接小组',
            formatter: function (value, row, index) {
                if (value) {
                    return "<a class='btn btn-primary btn-sm' href='/admin/toGroupInfo?groupId=" + value.id + "'>对接小组详情</a>";
                } else {
                    return "<a class='btn btn-sm' data-toggle='modal' data-target='#parallelismTeaGroup'>选择对接小组</a>";
                }
            }
        }, {
            title: "操作",
            formatter: function (value, row, index) {
                return "<a class='btn btn-primary btn-sm' data-toggle='modal' data-target='#assignTeaGroup'>选择教师</a>  <a class='btn btn-primary btn-sm' href='/admin/toGroupInfo?groupId=" + row.id + "'>小组详情</a>";
            }
        }
    ];
    customSearch({
        id: "#teacherTable",
        url: "/admin/getAllTeaGroup",
        dataType: "json",
        params: {},
        columns: columns
    });

    $("#addModal").on('show.bs.modal', function (e) {
        $.ajax({
            url: "/identity/noGroupAndNoStuOfTea",
            dataType: "json",
            data: {
                pageSize: 1000
            },
            success: function (res) {
                if (res.success) {
                    var arrays = res.data;
                    if (arrays != null && arrays.length > 0) {
                        var tea = $("#teaId");
                        tea.html("<option value=''>请选择</option>");
                        $.each(arrays, function (i, item) {
                            tea.append("<option value='" + item.id + "'>" + item.name + "</option>");
                        })
                    }
                } else {
                    alert(res.errorMsg);
                }
            }
        });
    });

    var $groupId = '';
    $("#assignTeaGroup").on('show.bs.modal', function (e) {
        $("#noGroupTable").html("<table class='table table-container' id='noGroupTea'> </table>");
        $groupId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $groupName = $.trim($(e.relatedTarget).parent().parent().children("td").get(1).innerHTML);
        var columns = [
            {
                field: 'id',
                title: '编号'
            }, {
                field: 'name',
                title: '教师姓名'
            }, {
                title: '性别',
                formatter: function (value, row, index) {
                    var str = '';
                    if (row.sex == -1) {
                        str = '女';
                    } else if (row.sex == 1) {
                        str = '男'
                    }
                    return str;
                }

            }, {
                field: 'phone',
                title: '联系方式'
            }, {
                title: "操作",
                formatter: function (value, row, index) {
                    return "<button class='btn btn-primary btn-sm' onclick='assignGroupTea(" + row.id + ")' >分配" + $groupName + "小组</button> ";
                }
            }
        ];
        customSearch({
            id: "#noGroupTea",
            url: "/identity/noGroupAndNoStuOfTea",
            dataType: "json",
            params: {},
            columns: columns
        });

    });

    $("#parallelismTeaGroup").on('show.bs.modal', function (e) {
        $("#parallelismIdNoGroup").html("<table class='table table-container' id='parallelismIdNoGroupTable'></table>");
        var columns = [
            {
                field: 'id',
                title: '编号'
            }, {
                field: 'identityName',
                title: '组长'
            }, {
                field: 'count',
                title: '小组人数'
            }, {
                title: "操作",
                formatter: function (value, row, index) {
                    groupId = row.id;
                    return "<button class='btn btn-green'>确认对接</button>";
                }
            }
        ];
        customSearch({
            id: "#parallelismIdNoGroup",
            url: "/admin/noParallelism",
            dataType: "json",
            params: {},
            columns: columns
        });

    });
    function assignGroupTea(id) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/group/assignGroupTea',
            data: {
                groupId: $groupId,
                id: id
            },
            success: function (result) {
                if (result) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }
        });
    }

</script>
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
</body>
</html>