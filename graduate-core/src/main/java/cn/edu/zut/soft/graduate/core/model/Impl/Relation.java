package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Relation extends BaseModelImpl {

    private Integer userOne;//用户1
    private Integer userTwo;//用户2
    private String type;//关系类型

}
