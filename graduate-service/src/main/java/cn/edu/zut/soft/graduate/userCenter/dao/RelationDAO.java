package cn.edu.zut.soft.graduate.userCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Relation;

/**
 * Created by chuchuang on 16/10/21.
 */
public interface RelationDAO<T extends Relation> extends BasicDAO<Integer,T> {
}
