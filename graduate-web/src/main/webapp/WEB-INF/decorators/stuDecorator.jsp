<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>

<html>
<head>
    <title>毕业设计管理系统|<sitemesh:write property='title'/>
    </title>
    <!-- start: META -->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1"/><![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <base href="<%=basePath%>">
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet"
          href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/style.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/main-responsive.css">
    <link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
    <link rel="stylesheet"
          href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/css/theme_light.css" type="text/css"
          id="skin_color">
    <link rel="stylesheet" href="assets/css/print.css" type="text/css"
          media="print"/>
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet"
          href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet"
          href="assets/plugins/datepicker/css/datepicker.css">
    <link rel="stylesheet"
          href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet"
          href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet"
          href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
    <link rel="stylesheet"
          href="assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
    <link rel="stylesheet"
          href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="favicon.ico"/>


    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-1.7.2.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->

    <script type="text/javascript" src="assets/js/jquery-1.7.2.min.js"></script>
    <!--<![endif]-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script
            src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
    <script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
    <script src="assets/plugins/less/less-1.5.0.min.js"></script>
    <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script
            src="assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
    <script src="assets/js/main.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="assets/plugins/flot/jquery.flot.js"></script>
    <script src="assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="assets/plugins/flot/jquery.flot.resize.min.js"></script>
    <script src="assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
    <script
            src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script
            src="assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <script src="assets/js/index.js"></script>
    <script src="assets/g/js//menu/menu.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
        });
    </script>
    <sitemesh:write property='head'/>

</head>
<body>
<!-- start: HEADER -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse"
                    class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <!-- start: LOGO -->
            <a class="navbar-brand" href="/home"> 软件学院毕业设计管理平台 </a>
            <!-- end: LOGO -->
        </div>
        <div class="navbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">
                <%--<li class="dropdown current-user">--%>
                <%--<a  class="dropdown-toggle" href="./${sessionScope.pathCode}/Description.do">--%>
                <%--<i class="clip-youtube "></i> <span class="username">系统使用说明</span>--%>
                <%--</a>--%>
                <%--</li>--%>
                <c:if test="${sessionScope.Basicadmin != null}">
                    <li class="dropdown current-user">
                        <a class="dropdown-toggle" href="/admin">
                            <i class="glyphicon glyphicon-wrench "></i> <span>管理员</span>
                        </a>
                    </li>
                </c:if>
                <!-- start: USER DROPDOWN -->
                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true"
                       href="javascript:void(0)">
                        <i class="clip-user-3 "></i>
                        <span class="username">${sessionScope.user.name}</span>
                        <i class="clip-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/${sessionScope.user.role}/myself">
                                <i class="clip-user-2"></i> &nbsp;个人信息
                            </a>
                        </li>
                        <li>
                            <a href="#pwdModal" data-toggle="modal">
                                <i class="clip-user-2"></i> &nbsp;修改密码
                            </a>
                        </li>
                        <li>
                            <a href="/logout">
                                <i class="clip-calendar"></i>
                                &nbsp;退出
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- end: USER DROPDOWN -->
            </ul>
            <!-- end: TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- end: TOP NAVIGATION CONTAINER -->
</div>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation navbar-collapse collapse">
            <!-- start: MAIN MENU TOGGLER BUTTON -->
            <div class="navigation-toggler">
                <i class="clip-chevron-left"></i> <i class="clip-chevron-right"></i>
            </div>
            <!-- end: MAIN MENU TOGGLER BUTTON -->
            <!-- start: MAIN NAVIGATION MENU -->
            <ul class="main-navigation-menu">
                <li id="index"><a href="/home"><i
                        class="clip-home-3"></i>
                    <span class="title">首页</span><span class="selected"></span> </a></li>
                <%--<li id="notice">--%>
                <%--<a href="javascript:void(0)">--%>
                <%--<i class="clip-bubbles-3"></i>--%>
                <%--<span class="title">信箱</span>--%>
                <%--<i class="icon-arrow"></i>--%>
                <%--<span class="selected"></span>--%>
                <%--</a>--%>
                <%--<ul class="sub-menu">--%>
                <%--<li>--%>
                <%--<a href="/stu/inbox">--%>
                <%--<span class="title">收件箱</span>--%>
                <%--</a>--%>
                <%--</li>--%>
                <%--<li>--%>
                <%--<a href="/stu/outbox">--%>
                <%--<span class="title">发件箱</span>--%>
                <%--</a>--%>
                <%--</li>--%>
                <%--</ul>--%>
                <%--</li>--%>

                <li>
                    <a href="javascript:void(0)">
                        <i class="clip-bubbles-3"></i>
                        <span class="title">邀请详情</span>
                        <i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/stu/inviteReceiver">
                                <span class="title">收到的邀请</span>
                            </a>
                        </li>
                        <li>
                            <a href="/stu/inviteSponsor">
                                <span class="title">发送的邀请</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/stu/issue/list/tea?issuePhase=chooseIng"><i class="clip-popout"></i>
                        <span class="title"> 校内题目</span></i>
                    </a>
                </li>

                <li id="student_group">
                    <a href="./${sessionScope.user.role}/stuGroup">
                        <%--<a href="javaScript:void(0);">--%>
                        <i class="	clip-users-2 "></i>
                        <span class="title">选报信息</span>
                        <span class="selected"></span>
                    </a>

                </li>
                <li id="report"><a href="javascript:void(0)"><i
                        class=" clip-stack-empty"></i> <span class="title">周月报管理</span><i
                        class="icon-arrow"></i> <span class="selected"></span> </a>
                    <ul class="sub-menu">
                        <li id="reportRuleList"><a
                                href="/stu/report/listData"> <span
                                class="title">报告规则管理</span>
                        </a></li>
                    </ul>
                </li>
                <li id="graduateState">
                    <a href="/stu/graduateState"><i class="glyphicon glyphicon-list-alt"></i>
                        <span class="title"> 毕设状态</span></i>
                    </a>
                </li>
                <li id="studentInfo">
                    <a href="/${sessionScope.user.role}/myself">
                        <i class=" clip-settings"></i>
                        <span class="title">个人信息</span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>
    <!-- start: PAGE -->
    <div class="main-content">
        <div class="container">
            <sitemesh:write property='body'/>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="pwdModal" tabindex="-1" role="dialog" aria-labelledby="pwdModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="pwdModalLabel">密码修改</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="errorHandler alert alert-danger no-display">
                                <i class="fa fa-remove-sign"></i>
                                <span></span>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">旧密码</label>
                                <div class="col-sm-10">
                                    <input type="password" id="oldpwd" title="旧密码" class="form-control"
                                           placeholder="旧密码">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">新密码</label>
                                <div class="col-sm-10">
                                    <input type="password" id="nowpwd" title="新密码" class="form-control"
                                           placeholder="新密码">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">重复密码</label>
                                <div class="col-sm-10">
                                    <input type="password" id="againpwd" title="重复密码" class="form-control"
                                           placeholder="重复密码">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="savepwd">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner"> 2015 &copy; 毕业设计管理平台 by 中原工学院软件学院.</div>
    <div class="footer-items">
			<span class="go-top"><i class="clip-chevron-up"></i>
			</span>
    </div>
</div>
<!-- end: FOOTER -->
<div id="event-management" class="modal fade" tabindex="-1"
     data-width="760" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                <h4 class="modal-title">Event Management</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal"
                        class="btn btn-light-grey">Close
                </button>
                <button type="button"
                        class="btn btn-danger remove-event no-display">
                    <i class='fa fa-trash-o'></i> Delete Event
                </button>
                <button type='submit' class='btn btn-success save-event'>
                    <i class='fa fa-check'></i> Save
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Model -->
<div class="modal fade" id="stop" tabindex="-1" role="dialog" aria-labelledby="CSGModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="CSGModalLabel">终止答辩</h4>
            </div>
            <div class="modal-body">
                您已经被指导教师终止答辩，如有问题请自行联系指导教师！
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="/assets/g/js/changePwd.js"></script>
<script type="application/javascript">
    $(function () {
        function onWeb() {
            $.ajax({
                url: "/statistics/train",
                dataType: "json",
                type: "get",
                data: {
                    timestamp: new Date().getTime()
                }
            })
        }

        window.setInterval(onWeb, 60000);
    });
</script>
</body>
</html>
