package cn.edu.zut.soft.graduate.controller.tea;

import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>文件名称：TeacherGroupControllerp.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/12/13 下午8:25</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Controller
@RequestMapping("/tea")
public class TeacherGroupControllerp extends BaseController {

    /**
     * 功能描述:跳转教师端我的教师小组
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/toGroupInfo")
    public ModelAndView toGroupInfo(HttpServletRequest request, Map<String, Object> model){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return new ModelAndView("redirect:/login");
        }
        return new ModelAndView("/tea/groupInfo");
    }

    /**
     * 功能描述:教师端小组学生跳转
     * @return
     */
    @RequestMapping("/toStudentByGroup")
    public ModelAndView toStudentByGroup(){
        return new ModelAndView("/tea/groupStuInfo");
    }

    @RequestMapping("/toSystemGrade")
    public ModelAndView toSystemGrade(){
        return  new ModelAndView("/tea/myStudentSystemGrade");
    }


}
