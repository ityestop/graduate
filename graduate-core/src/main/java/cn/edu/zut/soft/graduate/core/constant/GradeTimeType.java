package cn.edu.zut.soft.graduate.core.constant;

/**
 * <p>文件名称：TimeType.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/5/16 下午4:11</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public enum GradeTimeType {
    //开题成绩 一辩标识/二辩标识
    OPEN_GRADE_ONE("1", "openGrade_one"),
    OPEN_GRADE_TWO("2", "openGrade_two"),
    //系统验收 一辩标识/二辩标识
    SYSTEM_GRADE_ONE("3", "systemGrade_one"),
    SYSTEM_GRADE_TWO("4", "systemGrade_two"),
    //答辩成绩  一辩标识/二辩标识
    REPLY_GRADE_ONE("5", "replyGrade_one"),
    REPLY_GRADE_TWO("6", "replyGrade_two"),
    GRADE_STOP("7", "stop");

    private String type;
    private String key;

    private GradeTimeType(String type, String key) {
        this.type = type;
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public String getKey() {
        return key;
    }
}
