<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>学生成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>


	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">学生成绩</li>
			</ol>
			<div class="page-header">
				<h3>学生成绩</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>

	<table>

	</table>


	<div class="row">
		<div class="col-md-12">
					<div class="panel-body">
						<table class="table table-hover"
							style="TABLE-LAYOUT: fixed; WORD-BREAK: break-all">
							<thead>
								<tr>

									<th width="50%"><small>考核类型</small></th>
									<th width="50%"><small>成绩</small></th>


								</tr>
							</thead>
							<tbody>

								<tr>
									<td><small>开题考核</small></td>
									<td><small>${Achievement[0]}</small></td>
								</tr>
								<tr>
									<td><small>中期考核</small></td>
									<td><small>
									 <c:if test="${Achievement[1] eq 90}">
			                            		A
			                            		</c:if> <c:if test="${Achievement[1] eq 80}">
			                            		B
			                            		</c:if> <c:if test="${Achievement[1] eq 60}">
			                            		C
			                            		</c:if> <c:if test="${Achievement[1] eq -1}">
			                            		终止答辩
			                            		</c:if> <c:if test="${Achievement[1] eq 0}">
			                            		未审批
			                            		</c:if> 
									</small></td>
								</tr>
								<tr>
									<td><small>系统验收</small></td>
									<td><small>
									 <c:if test="${sessionScope.user.checkGrade eq 0}">
			                            		教师未审核
			                        </c:if> 
			                        <c:if test="${sessionScope.user.checkGrade eq 1}">
			                            		通过-进入一辨
			                        </c:if> 
			                        <c:if test="${sessionScope.user.checkGrade eq 2 or sessionScope.user.checkGrade eq 3}">
			                            		进入二辩
			                        </c:if> 
			                        
									</small></td>
								</tr>
								<tr>
									<td><small>指导教师成绩</small></td>
									<td><small>${sessionScope.user.donGrade}</small></td>
								</tr>
								<tr>
									<td><small>评阅成绩</small></td>
									<td><small>${sessionScope.user.documentGrade}</small></td>
								</tr>
								<tr>
									<td><small>答辩成绩</small></td>
									<td><small>${sessionScope.user.replyGrade}</small></td>
								</tr>
								<tr>
									<td><small>最终成绩</small></td>
									<td><small><font color="red">${sessionScope.user.finalReplyGrade}</font></small></td>
								</tr>
							</tbody>
						</table>
						<div class="form-group right"></div>
					</div>
				</div>
			</div>




</body>
</html>