package cn.edu.zut.soft.graduate.core.vo;

import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import lombok.Data;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class InfoVO {

    private Info info;

    private List<Info> childList;

    public InfoVO() {
    }

    public InfoVO(Info info) {
        this.info = info;
    }
}
