package cn.edu.zut.soft.graduate.declareCenter.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.topicCenter.dao.DeclareDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.Arrays;

/**
 * Created by chuchuang on 16/10/22.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/bean-all.xml")
public class DeclareDAOTest {

    @Resource
    private DeclareDAO<Declare> declareDAO;

    public void countByExampleTest(){

    }

    public void deleteByExampleTest(){

    }

    @Test
    public void deleteByPrimaryKeyTest(){
        System.out.println(declareDAO.deleteByExample(new Example(){
            {}
        }));
    }

    public void insertTest(){

    }

    @Test
    public void insertSelectiveTest(){
        Declare declare = new Declare(){

        };
        declareDAO.insert(declare);
        System.out.println(declare);
    }

    @Test
    public void selectByExampleTest(){
        System.out.println(declareDAO.selectByExample(new Example(){
            {
                createCriteria().andIn("id", Arrays.asList(1,5,3,4));
            }
        }));
    }

    public void selectByPrimaryKeyTest(){

    }

    public void updateByExampleSelectiveTest(){
    }

    public void updateByExampleTest(){

    }

    public void updateByPrimaryKeySelectiveTest(){

    }

    public void updateByPrimaryKeyTest(){

    }

}