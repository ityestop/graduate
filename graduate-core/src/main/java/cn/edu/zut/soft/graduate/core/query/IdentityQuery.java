package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.Address;
import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IdentityQuery extends Query {
    private String noId;
    private String name;
    private Integer sex;
    private String phone;
    private String email;
    private Role role;

    private Integer del;
    private List<Integer> ids;
    private List<Integer> notIds;
    private Integer groupId;
    /**
     * 地区
     */
    private Address address;

    private boolean reverse;
}
