package cn.edu.zut.soft.graduate.topicCenter.handler;

import cn.edu.zut.soft.graduate.config.bo.ConfigBO;
import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.DeclareQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.AuditIssueLinkBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.DeclareBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import cn.edu.zut.soft.graduate.topicCenter.handler.exception.HandlerException;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cn.edu.zut.soft.graduate.core.constant.IssuePhase.Handle.AuditError;
import static cn.edu.zut.soft.graduate.core.constant.IssuePhase.Handle.AuditSuccess;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */

/**
 * 提交课题时检测
 */
@Component
public class ChangeToAuditOK extends AbstractChangeHandler {

    @Autowired
    private AuditIssueLinkBO auditIssueLinkBO;

    @Autowired
    private ConfigBO configBO;

    @Autowired
    private IssueBO issueBO;

    @Autowired
    private IdentityBO identityBO;

    @Autowired
    private GroupBO groupBO;

    @Autowired
    private DeclareBO declareBO;

    @Autowired
    private InfoBO infoBO;

    private List<IssuePhase.Handle> handleList = Arrays.asList(AuditError, AuditSuccess);

    @Override
    protected boolean conform(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        return handleList.contains(handle);
    }

    @Override
    protected void conformChange(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
//        Assert.isTrue(loginVO.getId().equals(issue.getIdentityId()),"非本人操作");
        Assert.notNull(auditIssueLinkBO.getByissueAndUser(loginVO.getId(), issue.getId()), "当前课题不具备审核权限");
        changeStatus(loginVO, issue, handle);
        //优化性能,去除大对象的修改
        issue.setContent(null);
    }

    private void changeStatus(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        //提前记录接下来的变化
        issue.addStatus(handle.getDst());
        if (IssuePhase.Handle.AuditSuccess == handle) {
            //学校项目 && 未开启选报情况
            if (issue.getSource() == IssueSource.school) {

                //如果开启状态,校内题目多进行一次流转
                if (Boolean.parseBoolean(configBO.findByKey(IKEY.ISSUESTART))) {
                    IssuePhase issuePhase = IssuePhase.Handle.from(handle.getDst(), IssuePhase.Handle.OpenChoose);
                    issue.addStatus(issuePhase);
                }
            } else if (issue.getSource() == IssueSource.myself) {
                //获取创建者信息
                UserDescVO userDescVO = identityBO.userPersonal(issue.getIdentityId());
                Integer groupId = null;
                for (Info info : userDescVO.getInfoList()) {
                    if (InfoType.Group.name().equals(info.getType()) && IKEY.GROUP.equals(info.getKey())) {
                        groupId = Integer.parseInt(info.getValue());
                        break;
                    }
                }
                Assert.notNull(groupId, "学生小组不存在,数据错误,请检查数据,identity = " + userDescVO.getIdentity());
                Group group = groupBO.get(groupId);
                Assert.notNull(group, "小组不存在");
                Assert.isTrue(group.getDel() == IKEY.ZERO, "学生小组已经被删除");
                DeclareQuery declareQuery = new DeclareQuery();
                declareQuery.setGroupId(groupId);
                declareQuery.setDeclareStatusList(Arrays.asList(DeclareStatus.ING, DeclareStatus.YES));
                List<Declare> declareList = declareBO.findByQuery(declareQuery).getData();
                //效验数据是否为唯一的一条
                Assert.isTrue(CollectionUtils.isNotEmpty(declareList) && declareList.size() == 1, "数据错误,请检查数据");
                Declare declare = declareList.get(0);
                declare.setStatus(DeclareStatus.YES);
                //修改申请状态
                declareBO.update(declare, loginVO.getName());

                //学生放入附属信息
                List<Info> infoList = infoBO.queryByInfoStrategy(Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.Group, IKEY.GROUP, group.getId().toString())));
                for (Info info : infoList) {
                    infoBO.save(new BasicInfoStrategy(InfoType.TOPIC, IKEY.TopicId, issue.getId().toString()), info.getIdentityId(), loginVO.getName());
                    infoBO.save(new BasicInfoStrategy(InfoType.TOPIC, IKEY.TopicName, issue.getTitle()), info.getIdentityId(), loginVO.getName());
                }

                //修改校内题目的状态
                //变为课题审核通过
                //课题变为开始选报
                issue.addStatus(IssuePhase.Handle.from(issue.getStatus(), IssuePhase.Handle.OpenChoose));
                //选报成功
                issue.addStatus(IssuePhase.Handle.from(issue.getStatus(), IssuePhase.Handle.ChooseSuccess));
            } else {
                throw HandlerException.create("未处理的方法");
            }
        }else {
           if (issue.getSource() == IssueSource.school) {
               UserDescVO userDescVO = identityBO.userPersonal(issue.getIdentityId());
               Integer groupId = null;
               for (Info info : userDescVO.getInfoList()) {
                   if (InfoType.Group.name().equals(info.getType()) && IKEY.GROUP.equals(info.getKey())) {
                       groupId = Integer.parseInt(info.getValue());
                       break;
                   }
               }
               //修改申请状态
               declareBO.deleteByGroupId(groupId, loginVO.getName());
           }
        }
    }

}
