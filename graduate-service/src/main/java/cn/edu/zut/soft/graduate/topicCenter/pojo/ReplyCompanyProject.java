package cn.edu.zut.soft.graduate.topicCenter.pojo;

import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import lombok.Data;

/**
 * @author cc
 * @Date 2016/12/1
 * @value 1.0
 */
@Data
public class ReplyCompanyProject {
    private Integer requestId;//请求的id
    private Integer teacherId;//教师id
    private DeclareStatus declareStatus;//同意或拒绝或忽视
}
