package cn.edu.zut.soft.graduate.topicCenter.bo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * <p>文件名称：AuditIssueLinkBOTest.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/11/13 下午1:45</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/bean-all.xml")
public class AuditIssueLinkBOTest {

    @Resource
    private AuditIssueLinkBO auditIssueLinkBO;
    @Test
    public void noExpertTopic() throws Exception {
        List<Integer> list = auditIssueLinkBO.noExpertTopic();
        System.out.println(list.size());
    }

    @Test
    public void getExpertIssue() throws Exception {

    }

}