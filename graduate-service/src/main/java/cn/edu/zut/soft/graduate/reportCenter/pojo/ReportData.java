package cn.edu.zut.soft.graduate.reportCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.BaseModelImpl;

public class ReportData extends BaseModelImpl implements ReportDataDTO {

    private Integer reportRuleId;

    private String context;

    private Integer hashContext;

    private Integer supplement;

    private Integer identityId;

    private String identityName;

    private String identityRole;

    public Integer getReportRuleId() {
        return reportRuleId;
    }

    public void setReportRuleId(Integer reportRuleId) {
        this.reportRuleId = reportRuleId;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Integer getHashContext() {
        return hashContext;
    }

    public void setHashContext(Integer hashContext) {
        this.hashContext = hashContext;
    }

    public Integer getSupplement() {
        return supplement;
    }

    public void setSupplement(Integer supplement) {
        this.supplement = supplement;
    }

    public Integer getIdentityId() {
        return identityId;
    }

    public void setIdentityId(Integer identityId) {
        this.identityId = identityId;
    }

    public String getIdentityName() {
        return identityName;
    }

    public void setIdentityName(String identityName) {
        this.identityName = identityName;
    }

    public String getIdentityRole() {
        return identityRole;
    }

    public void setIdentityRole(String identityRole) {
        this.identityRole = identityRole;
    }
}