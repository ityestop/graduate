package cn.edu.zut.soft.graduate.basic.bo;

import cn.edu.zut.soft.graduate.model.HistoryFormatIssue;

import java.util.List;

/**
 * <p>文件名称：HistoryFormatIssue.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/30 上午12:35</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public interface FormatIssueBO {

    public List<HistoryFormatIssue>  formartIssueByTea(String TeaName);

    public  HistoryFormatIssue  formartIssueById(Integer id);

}
