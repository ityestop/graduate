<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<html>
<head>
    <title>学生基本信息管理</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">学生课题选报情况</li>
        </ol>
        <div class="page-header">
            <h3>学生课题选报情况</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<sf:form action="./${sessionScope.pathCode}/allStuSta.do" method="post" commandName="studentSta" id="condition">
    <div class="form-group">
        <label class="col-sm-1 control-label">
            学生姓名 </label>

        <div class="col-sm-3">
            <sf:input path="stuName" cssClass="form-control"/>
        </div>

        <label class="col-sm-1 control-label">
            选报状态 </label>

        <div class="col-sm-4">
            <sf:select class="form-control search-select" path="choStatus" id="status">
                <sf:option value="-2">全部</sf:option>
                <sf:option value="-3">未选报</sf:option>
                <sf:option value="-1">审核中</sf:option>
                <sf:option value="0">选报成功</sf:option>
            </sf:select>
        </div>

        <div class="col-sm-1 ">
            <button type="submit" class="btn btn-primary"> 检索</button>
        </div>
        <div class="col-sm-1 "  >
            <a class="btn btn-primary" href="./${sessionScope.pathCode}/downloadTopic.do"> 入库题目导出</a>
        </div>
    </div>
</sf:form>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">

            <table class=" table table-hover">
                <thead>
                <tr>
                    <th width="20%">
                        <small>学号</small>
                    </th>
                    <th>
                        <small>姓名</small>
                    </th>
                    <th>
                        <small>专业/班级</small>
                    </th>
                    <th>
                        <small>联系电话</small>
                    </th>
                    <th>
                        <small>小组组长</small>
                    </th>
                    <th>
                        <small>选报状态</small>
                    </th>
                    <th>
                        <small>选报题目</small>
                    </th>
                    <th>
                        <small>指导教师</small>
                    </th>
                    <th>
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${studentList}" var="student">
                    <tbody>
                    <tr>
                        <td>
                            <small>${student.stuStudentId}</small>
                        </td>
                        <td>
                            <small>${student.stuName}</small>
                        </td>
                        <td class="center">
                            <small>${student.stuProfession }/${student.stuClass }</small>
                        </td>
                        <td>
                            <small>${student.stuTell }</small>
                        </td>
                        <td>
                            <small>${student.sgSName }</small>
                        </td>
                        <td>
                            <small>${student.choStatus == 1 ? "<small class='label label-success' style='font-size: 75% !important;'>选报成功</small>"
                            : student.choStatus == 0 ? "<small class='label label-warning' style='font-size: 75% !important;'>选报中</small>"
                            : "<small class='label label-inverse' style='font-size: 75% !important;'>未选报</small>" }
                            </small>
                        </td>
                        <td>
                            <small>${student.topTitle }</small>
                        </td>
                        <td>
                            <%--<small>${student.stuTeacherId}</small>--%>
                        </td>
                        <td><small></small></td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="form-group right">
                <div class="row" style="text-align: right; height: 40px">
                    <page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"
                                      curPage="${pageNum}" formId="condition"/>
                </div>
            </div>
        </div>
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
</div>
<script
        src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
</body>
</html>
