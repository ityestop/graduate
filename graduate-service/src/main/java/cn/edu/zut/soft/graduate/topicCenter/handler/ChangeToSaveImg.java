package cn.edu.zut.soft.graduate.topicCenter.handler;

import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;

import static cn.edu.zut.soft.graduate.core.constant.IssuePhase.Handle.Copy;
import static cn.edu.zut.soft.graduate.core.constant.IssuePhase.Handle.Reedit;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */

/**
 * 重新编辑 历史库入线上库情况监控
 */
@Component
public class ChangeToSaveImg extends ChangeToAuditOK {

    private List<IssuePhase.Handle> handleList = Arrays.asList(Reedit,Copy);


    @Override
    protected boolean conform(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        return handleList.contains(handle);
    }

    @Override
    protected void conformChange(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        Assert.isTrue(loginVO.getId().equals(issue.getIdentityId())|| loginVO.getId().equals(issue.getSchoolTeaId()),"非本人,无法操作");
        //判断是否失效,失效时同时逻辑删除
        issue.addStatus(handle.getDst());
        //优化性能,去除大对象的修改
        issue.setContent(null);
    }
}
