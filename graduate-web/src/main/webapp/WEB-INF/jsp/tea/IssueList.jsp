<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>课题列表</title>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        table{
            font-size: 10px;
        }
    </style>
    <script src="assets/g/js/source.js"></script>
    <script src="assets/g/js/teaIssueList.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/home.do"> 首页 </a></li>
            <li class="active">选题维护</li>
        </ol>
        <div class="page-header">
            <h3>选题维护
                <small>可以对未提交的课题进行修改和提交操作</small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="table" class="table table-container"></table>
            <table id="sample-table-1" class="table table-hover" style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                    <th width="20%">
                        <small>课题名称</small>
                    </th>
                    <th>
                        <small>课题类型</small>
                    </th>
                    <th>
                        <small>人数</small>
                    </th>

                    <th>
                        <small>课题状态</small>
                    </th>
                    <th>
                        <small>技术领域</small>
                    </th>
                    <th width="16%">
                        <small>创建时间</small>
                    </th>
                    <th width="20%">
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <%--<tbody id="table">--%>
                <%--</tbody>--%>
            </table>
        </div>
    </div>
</div>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script src='/assets/g/js/config.js'></script>
<script src='/assets/g/js/source.js'></script>
<%--<script type="application/javascript">--%>
    <%--var $editRole = false;--%>
    <%--$.ajax({--%>
        <%--type:'post',--%>
        <%--url:'/config/getIssueEditRole',--%>
        <%--dataType:'json',--%>
        <%--success:function (result) {--%>
            <%--if(result.success){--%>
                <%--$editRole = true;--%>
            <%--}--%>
        <%--}--%>
    <%--});--%>
    <%--alert($editRole);--%>
    <%--function params() {--%>
        <%--return {};--%>
    <%--}--%>
    <%--var columns;--%>
    <%--columns = [{--%>
        <%--field: 'id',--%>
        <%--title: '编号'--%>
    <%--}, {--%>
        <%--field: 'title',--%>
        <%--title: '课题名称'--%>
    <%--}, {--%>
        <%--field: 'source',--%>
        <%--title: '课题类型',--%>
        <%--formatter: function (value, row, index) {--%>
            <%--return sourceMaps().issueSourceMap[value];--%>
        <%--}--%>
    <%--}, {--%>
        <%--field: 'taskSize',--%>
        <%--title: '人数'--%>
    <%--}, {--%>
        <%--field: 'status',--%>
        <%--title: '课题状态',--%>
        <%--formatter: function (value, row, index) {--%>
            <%--var str = "<label class='label ";--%>
            <%--switch (value) {--%>
                <%--case 'saveIng':--%>
                    <%--str+='label-info';--%>
                    <%--break;--%>
                <%--case 'auditIng':--%>
                    <%--str+='label-warning';--%>
                    <%--break;--%>
                <%--case 'auditErrorIng':--%>
                    <%--str+='label-danger';--%>
                    <%--break;--%>
                <%--case 'giveUp':--%>
                    <%--str+='label-inverse';--%>
                    <%--break;--%>
                <%--case 'finish':--%>
                    <%--str+='label-success';--%>
                    <%--break;--%>
                <%--default:--%>
                    <%--str+='label-default';--%>
            <%--}--%>
            <%--str+="'>";--%>
            <%--str+=sourceMaps().issuePhaseMap[value];--%>
            <%--str+="</label>";--%>
            <%--return str;--%>
        <%--}--%>
    <%--}, {--%>
        <%--field: 'technology',--%>
        <%--title: '技术领域',--%>
        <%--formatter: function (value, row, index) {--%>
            <%--var array = JSON.parse(value);--%>
            <%--var str = "";--%>
            <%--for (var iii in array) {--%>
                <%--str += array[iii].title + "、";--%>
            <%--}--%>
            <%--return str;--%>
        <%--}--%>
    <%--}, {--%>
        <%--field: 'createTime',--%>
        <%--title: '创建时间',--%>
        <%--formatter: function (value, row, index) {--%>
            <%--return timeStamp2StringYYYYMMDD(value);--%>
        <%--}--%>
    <%--}, {--%>
        <%--title: '操作',--%>
        <%--formatter: function (value, row, index) {--%>
            <%--var str="<button class='btn btn-link '>提交</button>";--%>
            <%--str+="<button class='btn btn-link '>编辑</button>";--%>
            <%--str+="<button class='btn btn-link '>重新编辑</button>";--%>
            <%--str+="<button class='btn btn-link '>失效</button>";--%>
            <%--return str;--%>
        <%--}--%>
    <%--}];--%>
    <%--customSearch({id: "#table", url: "/issue/querySelf", dataType: "json", params: params(), columns: columns});--%>
<%--</script>--%>
</body>
</html>
