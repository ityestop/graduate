package cn.edu.zut.soft.graduate.base.excel;


import java.io.Serializable;
import java.util.*;

/**
 * Created by chuchuang on 16/9/24.
 */
public class ExcelFileSimpleHandler implements ExcelFileHandler,Serializable {

    private static final long serialVersionUID = 6766694520264790004L;
    private Set<Integer> invalidRows = new HashSet<>();//无效的行
    private Set<Integer> invalidColumns = new HashSet<>();//无效的列

    private Map<String,String> relationColumns = new HashMap<>();//列关系

    private List<Map<String, String>> mapList = new ArrayList<>();//处理内容

    public List<Map<String,String>> handler(){
        List<Map<String,String>> list = new ArrayList<>();
        for (int i = 0,l=mapList.size(); i<l;i++){
            //无效列忽略
            if (invalidRows.contains(i)){
                continue;
            }
            Map<String,String> map = mapList.get(i);
            if (!relationColumns.isEmpty()) {
                Map<String,String> newMap = new HashMap<>();
                for (Map.Entry<String, String> m : map.entrySet()) {
                    //列表关系存在
                    if (relationColumns.containsKey(m.getKey())) {
                        //关系列表替换key
                        newMap.put(relationColumns.get(m.getKey()), m.getValue());
                    }
                }
                list.add(newMap);
            }else {
                list.add(map);
            }
        }
        return list;
    }

    @Override
    public List<Map<String, String>> handler(List<Map<String, String>> mapList) {
        this.mapList = mapList;
        return handler();
    }

    public Set<Integer> getInvalidRows() {
        return invalidRows;
    }

    public void setInvalidRows(Set<Integer> invalidRows) {
        this.invalidRows = invalidRows;
    }

    public Set<Integer> getInvalidColumns() {
        return invalidColumns;
    }

    public void setInvalidColumns(Set<Integer> invalidColumns) {
        this.invalidColumns = invalidColumns;
    }

    public Map<String, String> getRelationColumns() {
        return relationColumns;
    }

    public void setRelationColumns(Map<String, String> relationColumns) {
        this.relationColumns = relationColumns;
    }

    public List<Map<String, String>> getMapList() {
        return mapList;
    }

    public void setMapList(List<Map<String, String>> mapList) {
        this.mapList = mapList;
    }
}
