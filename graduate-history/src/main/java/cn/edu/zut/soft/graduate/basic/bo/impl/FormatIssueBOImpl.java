package cn.edu.zut.soft.graduate.basic.bo.impl;

import cn.edu.zut.soft.graduate.basic.bo.FormatIssueBO;
import cn.edu.zut.soft.graduate.basic.dao.HTopicDao;
import cn.edu.zut.soft.graduate.model.HChildTopic;
import cn.edu.zut.soft.graduate.model.HTopic;
import cn.edu.zut.soft.graduate.model.HTopicResult;
import cn.edu.zut.soft.graduate.model.HistoryFormatIssue;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>文件名称：HistoryFormatIssue.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/30 上午12:37</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Service
public class FormatIssueBOImpl implements FormatIssueBO {

    @Resource
    private HTopicDao hTopicService;

    public void sethTopicService(HTopicDao hTopicService) {
        this.hTopicService = hTopicService;
    }

    @Override
    public List<HistoryFormatIssue> formartIssueByTea(String TeaName) {
        List<HistoryFormatIssue> result = new ArrayList<>();
        if(StringUtils.isEmpty(TeaName)){
            return null;
        }
        List<HTopic> hTopics = hTopicService.selectByTeacherName(TeaName);
        if (hTopics != null && hTopics.size() > 0){
            for (HTopic h:hTopics){
                HTopicResult hTopicResult = JSONObject.parseObject(h.getResult(),HTopicResult.class);
                HistoryFormatIssue hfi = new HistoryFormatIssue();
                hfi.setId(h.getId());
                hfi.setTitle(h.getTitle());
                hfi.setStatus("history");
                hfi.setTask_size(h.getSize());
                hfi.setChildNames(getChildName(hTopicResult.getTopChild()));
                result.add(hfi);
            }
        }
        return result;
    }

    @Override
    public HistoryFormatIssue formartIssueById(Integer id) {
        if (id == null){
            return null;
        }
        HistoryFormatIssue result = new HistoryFormatIssue();
        HTopic hTopic = hTopicService.selectById(id);
        if (hTopic != null){
            HTopicResult hTopicResult = JSONObject.parseObject(hTopic.getResult(),HTopicResult.class);
            //消息标题
            result.setTitle(hTopic.getTitle());
            //内容
            result.setBackground(hTopic.getContext());
            //人数
            result.setTask_size(hTopic.getSize());
            //状态
            result.setStatus("history");
            //子标题
            result.setTask(JSON.parseArray(hTopicResult.getTopChild(),HChildTopic.class));
            //source
            if (hTopicResult.getTopSourece().equals("公司项目") || hTopicResult.getTopSourece().equals("自拟项目")){
                result.setSource("myself");
            }else{
                result.setSource("school");
            }
            //kind
            if (hTopicResult.getTopType().equals("社会服务")){
                result.setKind("Social");
            }else{
                result.setKind("Research");
            }
            //类型:设计
            result.setType(hTopicResult.getTopType());
            //技术领域
            result.setTechnology(JSONObject.parseArray(hTopicResult.getToptech(), String.class));
        }
        return result;
    }

    /**
     * 返回拼接 子标题
     * @param result
     * @return
     */
    private String getChildName (String result){
        String resultString = "";
        if (StringUtils.isEmpty(result)){
            return  null;
        }
        List<HChildTopic> hChildTopic = JSON.parseArray(result,HChildTopic.class);
        for(HChildTopic h:hChildTopic){
            if (h.getName() != null || h.getName() != ""){
                resultString += h.getName()+"、";
            }else {
                continue;
            }
        }
        return resultString;
    }
}
