
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<html>
<head>

    <title>教师答辩小组</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">教师答辩小组</li>
        </ol>
        <div class="page-header">
            <h3>教师答辩小组</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
    
            <table class="table  table-hover">
                <thead>
                <tr>
                    <th>
                        <small>教师工号</small>
                    </th>
                    <th>
                        <small>姓名</small>
                    </th>
                    <th>
                        <small>性别</small>
                    </th>
                    <th>
                        <small>教师职称</small>
                    </th>
                    <th>
                        <small>联系方式</small>
                    </th>
                    <th>
                        <small>学生人数</small>
                    </th>
                    <th>
                    	<small>小组身份</small>
                    </th>
                    <th >
                        <small >操作</small>
                    </th>
                </tr>
                </thead>
                <c:forEach items="${teacher}" var="t">
                <tr>
                <input type="hidden" class="teaId" value="${t.teaId }">
                <input type="hidden" class="teaName" value="${t.teaName }">
                <input type="hidden" class="tgId" value="${t.teaGroupId }">
                <td><small>${t.teaTeacherId}</small></td>
                <td><small>${t.teaName }</small></td>
                <td><small>${t.teaSex ? "女" : "男"}</small></td>
                 <c:choose>
                            <c:when test="${t.teaQualifications eq 0}">
                                <td>
                                    <small>无</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 1}">
                                <td>
                                    <small>助教</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 2}">
                                <td>
                                    <small>讲师</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 3}">
                                <td>
                                    <small>副教授</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaQualifications eq 4}">
                                <td>
                                    <small>教授</small>
                                </td>
                            </c:when>

                        </c:choose>
                <td><small>${t.teaTell }</small></td>
                   <c:choose>
                            <c:when test="${t.stuAll eq 0}">
                                <td>
                                    <small>无</small>
                                </td>
                            </c:when>
                            <c:when test="${t.stuAll > 0}">
                                <td>
                                    <small>${ t.stuAll}</small>
                                </td>
                            </c:when>
                        </c:choose>
                <c:choose>
                            <c:when test="${t.teaIdentity eq 0}">
                                <td>
                                    <small>成员</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaIdentity eq 1}">
                                <td>
                                    <small>秘书</small>
                                </td>
                            </c:when>
                            <c:when test="${t.teaIdentity eq 2}">
                                <td>
                                    <small>组长</small>
                                </td>
                            </c:when>
                        </c:choose>
                  <c:choose>
                  <c:when test="${t.teaIdentity eq 2 }">
                   <td><small><a href="javaScript:void(0);">无权限设置操作</a>
                </small></td>
                  
                  </c:when>
                  <c:when test="${t.teaIdentity ne 2 }">
                   <td><small><a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#assignIndetity">分配角色</a>
                			<a href="./${sessionScope.pathCode}/deleteGroupTeacher.do?teaId=${t.teaId}&teaGroupId=${t.teaGroupId}">删除</a>
                </small></td>
                  
                  </c:when>
                  </c:choose>
                </tr>
                </c:forEach>
            </table>
              
        </div>
        
    </div>
</div>
<div class="form-group">
                <label class="col-sm-10 control-label"></label>

                <div class="col-sm-2">
                  <button type="button" class="btn btn-blue btn-block"
                                    onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i> 返回
                            </button>
                </div>
           </div>  
</div>
<div class="modal fade" id="assignIndetity" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
    <form action="./${sessionScope.pathCode}/submitIndentity.do"
          class="form-horizontal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">角色分配</h4>
                </div>
                <div class="modal-body">
                <input type="hidden" name="teaId">
              
                <div class="form-group">
							<label class="col-sm-2 control-label" for="teaName">
								教师姓名 </label>
							<div class="col-sm-5">
								<input  type="text"  name="teaName" class="form-control"
									 readonly="readonly">
							</div>
							<span class="help-inline col-sm-2"> <i
								class="fa fa-info-circle"></i> 不可修改 </span>
						</div>
				<div class="form-group">
							<label class="col-sm-2 control-label" >
								身份 </label>
							<div class="col-sm-5">
								<select class="form-control search-select"  name="teaIndentity">
									<option value="0">组员</option>
									<option value="1">秘书</option>
								</select>
							</div>

						</div>
				         <input type="hidden" name="tgId">         		
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary">确认</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $('#assignIndetity').on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
       	var $teaId=$p.find(".teaId");
       	var $teaName=$p.find(".teaName");
       	var $tgId=$p.find(".tgId");
       	var $modal = $(this);
        $modal.find("input[name='teaName']").val($teaName.val());
        $modal.find("input[name='teaId']").val($teaId.val());
        $modal.find("input[name='tgId']").val($tgId.val());
     
    });
</script>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script><!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script><!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script><!-- 2 -->
<script src="assets/js/form-elements.js"></script><!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
