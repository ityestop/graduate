package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;

import java.io.InputStream;
import java.util.List;

/**
 * Created by chuchuang on 16/11/14.
 */
public interface StudentBO {

    /**
     * 上传学生文件并解析
     * @param inputStream
     * @param fileName
     * @param fileSize
     * @param loginVO
     * @return
     */
    String uploadStudent(InputStream inputStream,String fileName,long fileSize, LoginVO loginVO);



    /**
     * 将对没有小组的用户进行新建小组
     * @return
     */
    List<UserDescVO> noGroupInstantiate(LoginVO loginVO);

    /**
     * 功能描述:针对小组单个教师分配
     */
    CommonResult assignStuGrouop(LoginVO loginVO, Integer teaId, Integer groupId);

    /**
     * 检索教师小组下对应的学生列表
     */
    CommonResult queryStuAllByTeaGroupAndAddress(LoginVO loginVO, IdentityQuery identityQuery);

    /**
     * 检索所有的学生信息,根据教师小组以及地区
     * @param identityQuery
     * @return
     */
    List<UserDescVO> queryStuAllByTeaGroupAndAddress(IdentityQuery identityQuery);

    /**
     * <p>功能描述：检索所有的学生, 根据状态</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-21 02:35:23</p>
     *
     * @param key
     * @return
     */
    List<UserDescVO> queryStuAllBySystemGrade(IdentityQuery query, String key);
    /**
     * 同步用户开题地址
     */
    void syncAddress(LoginVO loginVO);

    /**
     * 随机抽取规则一，雨露均沾
     * @param num 抽取的数目
     * @return 抽取之后的结果
     */
    CommonResult<List<UserDescVO>> randomDrawing(int num);
}
