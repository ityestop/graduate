package cn.edu.zut.soft.graduate.base.excel.pojo;

/**
 * Created by chuchuang on 16/9/23.
 */
public enum FileType {
    WORD("doc"),WOEDS("docx"),EXCEL("xls"),EXCELS("xlsx"),PPT("ppt"),PPTS("pptx"),JPEG("jpeg"),PNG("png");
    private String name;

    FileType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FileType getName(String name){
        for (FileType fileType : FileType.values()){
            if (fileType.getName().equalsIgnoreCase(name)){
                return fileType;
            }
        }
        return null;
    }
}
