
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<html>
<head>

<title>开题成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<script src="IAssets/js/selectCheckbox2.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">开题成绩</li>
			</ol>
			<div class="page-header">
				<h3>
					开题成绩 <small> </small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="panel-body">
			<div class="tabbable">
				<ul id="myTab4"
					class="nav nav-tabs tab-padding tab-space-3 tab-blue">
					<li class="active"><a data-toggle="tab"
						href="#panel_tab_example1">开题成绩-未通过</a>
					</li>
					<li><a data-toggle="tab" href="#panel_tab_example2">开题成绩-已通过
					</a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="panel_tab_example1" class="tab-pane active">
						<form action="./${sessionScope.pathCode}/updateOpenGradeMany.do"
							method="post">
							<table class="table  table-hover"
								style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
								<thead>
									<tr>
										<th width="3%"><input type="checkbox" id="selectAll" />
										</th>
										<th width="5%"><small>编号</small>
										</th>
										<th width="13%"><small>学号</small>
										</th>
										<th><small>姓名</small>
										</th>
										<th><small>班级</small>
										</th>
										<th width="15%"><small>答辩题目</small>
										</th>
										<th width="8%"><small>校外导师</small>
										</th>
										<th><small>指导教师</small>
										</th>
										<th width="8%"><small>题目来源</small>
										</th>
										<th><small>开题成绩</small>
										</th>
										<th><small>操作</small>
										</th>
									</tr>
								</thead>
								<c:forEach items="${stulistFalse}" var="t" varStatus="i">
									<tr>
										<td><label> <input type="checkbox" name="stuIds"
												value="${t.stuId}" /> </label>
										</td>
										<td><small>${i.index + 1 }</small></td>
										<td><small>${t.stuStudentId}</small></td>
										<td><small>${t.stuName }</small></td>
										<td><small>${t.stuClass}</small></td>
										<c:choose>
											<c:when test="${t.topTitle ne null }">
												<td
													style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
													title="${t.topTitle}(${t.childTitle eq null ? '未分配' : t.childTitle})"><small><a
														href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${t.topId}">
															${t.topTitle}</a> </small></td>
											</c:when>
											<c:when test="${t.topTitle eq null }">
												<td
													style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><small><a
														href="javaScript:void(0);"> 无</a> </small></td>
											</c:when>
										</c:choose>
										<td><small>${t.topOutTeacher eq null ? "无"
												:t.topOutTeacher }</small></td>
										<td><small>${t.teaName}</small></td>
										<c:choose>
											<c:when test="${t.topSource eq 3}">
												<td><small>自拟题目</small>
												</td>
											</c:when>
											<c:when test="${t.topSource lt 3}">
												<td><small>校内题目</small>
												</td>
											</c:when>
										</c:choose>
										<c:choose>
											<c:when test="${t.openTopic eq false}">
												<td><small>未通过</small>
												</td>
											</c:when>
											<c:when test="${t.openTopic eq true}">
												<td><small>已通过</small>
												</td>
											</c:when>
										</c:choose>
										<td><small><a href="./${sessionScope.pathCode}/updateOpenGradeone.do?stuId=${t.stuId}">通过</a>
										</small>
										</td>
									</tr>
								</c:forEach>
							</table>
							<label class="col-sm-10 control-label"></label>
							<div class="col-sm-2">
								<button type="submit" class="btn btn-blue btn-block">
									通过</button>
							</div>
						</form>
					</div>
					<div id="panel_tab_example2" class="tab-pane">
						<table class="table  table-hover"
							style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
							<thead>
								<tr>
									<th width="5%"><small>编号</small>
									</th>
									<th width="13%"><small>学号</small>
									</th>
									<th><small>姓名</small>
									</th>
									<th><small>班级</small>
									</th>
									<th><small>联系方式</small>
									</th>
									<th width="15%"><small>答辩题目</small>
									</th>
									<th width="8%"><small>校外导师</small>
									</th>
									<th><small>指导教师</small>
									</th>
									<th><small>题目来源</small>
									</th>
									<th><small>开题成绩</small>
									</th>
								</tr>
							</thead>
							<c:forEach items="${stulistTrue}" var="t" varStatus="j">
								<tr>
									<td><small>${j.index + 1 }</small>
									</td>
									<td><small>${t.stuStudentId}</small></td>
									<td><small>${t.stuName }</small></td>
									<td><small>${t.stuClass}</small></td>
									<td><small>${t.stuTell }</small></td>
									<c:choose>
										<c:when test="${t.topTitle ne null }">
											<td
												style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
												title="${t.topTitle}(${t.childTitle eq null ? '未分配' : t.childTitle})"><small><a
													href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
														${t.topTitle}</a> </small></td>
										</c:when>
										<c:when test="${t.topTitle eq null }">
											<td
												style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><small><a
													href="javaScript:void(0);"> 无</a> </small></td>
										</c:when>
									</c:choose>
									<td><small>${t.topOutTeacher eq null ? "无"
											:t.topOutTeacher }</small></td>
									<td><small>${t.teaName}</small></td>
									<c:choose>
										<c:when test="${t.topSource eq 3}">
											<td><small>自拟题目</small>
											</td>
										</c:when>
										<c:when test="${t.topSource lt 3}">
											<td><small>校内题目</small>
											</td>
										</c:when>
									</c:choose>
									<c:choose>
										<c:when test="${t.openTopic eq false}">
											<td><small>未通过</small>
											</td>
										</c:when>
										<c:when test="${t.openTopic eq true}">
											<td><small>已通过</small>
											</td>
										</c:when>
									</c:choose>
								</tr>
							</c:forEach>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- end: TABLE WITH IMAGES PANEL -->
	<script>
		$('#assignIndetity').on('show.bs.modal', function(e) {
			var $p = $(e.relatedTarget).parent().parent().parent();
			alert($p.html());
			var $teaId = $p.find(".teaId");
			var $teaName = $p.find(".teaName");
			var $tgId = $p.find(".tgId");
			var $modal = $(this);
			$modal.find("input[name='teaName']").val($teaName.val());
			$modal.find("input[name='teaId']").val($teaId.val());
			$modal.find("input[name='tgId']").val($teaId.val());
			alert($tgId.val());
		});
	</script>

	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
