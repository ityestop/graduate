package cn.edu.zut.soft.graduate.base.excel.Excetion;

/**
 * @author cc
 * @Date 16/9/6
 * @value 1.0
 */
public class HandleException extends RuntimeException {
    private static final long serialVersionUID = -4277121251610234397L;

    private HandleException() {
        super();
    }

    public HandleException(String message) {
        super(message);
    }

    public HandleException(String message, Throwable cause) {
        super(message, cause);
    }

    public HandleException(Throwable cause) {
        super(cause);
    }

    public static HandleException create(String massage){
        return new HandleException(massage);
    }

    public static HandleException create(String massage,Throwable cause){
        return new HandleException(massage,cause);
    }
}
