<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/11/9
  Time: 下午8:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>校内课题列表</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">校内课题列表</li>
        </ol>
        <div class="page-header">
            <h3>校内课题列表
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="/admin/issue/list/stu" method="get">
                <div class="form-group">
                    <label class="control-label">
                        学生姓名
                    </label>
                    <input name="identityName" class="form-control" value="${param.identityName}" placeholder="学生名称">
                </div>
                <div class="form-group">
                    <label class="control-label">
                    状态列表
                    </label>
                    <select class="form-control" title="状态列表" name="issuePhase">
                        <option></option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">
                        课题名称
                    </label>
                    <input name="title" class="form-control" value="${param.title}" placeholder="课题名称">
                </div>
                <div class="form-group">
                    <label class="control-label">
                        课题人数
                    </label>
                    <select class="form-control" title="课题人数" name="taskSize">
                        <option></option>
                        <option value="1" ${param.taskSize == 1 ? "selected":""}>1</option>
                        <option value="2" ${param.taskSize == 2 ? "selected":""}>2</option>
                        <option value="3" ${param.taskSize == 3 ? "selected":""}>3</option>
                    </select>
                </div>
                <div class="form-group" style="text-align: right;">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="issueTable" class="table table-container">
                <thead>
                <tr>
                    <th>
                        课题名称
                    </th>
                    <th>
                        归属学生
                    </th>
                    <th>
                        课题状态
                    </th>
                    <th>
                        课题来源
                    </th>
                    <th>
                        课题任务数
                    </th>
                    <th>
                        创建时间
                    </th>
                    <th>
                        操作
                    </th>
                </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
            <div id="page-nav"></div>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
<script type="application/javascript">
    $(function () {
        var $teaList = '${teaList}';
        var $statusList = '${statusList}';
        if ($teaList != null && $teaList != '') {
            $.each(JSON.parse($teaList), function (i, node) {
                $("select[name='identityId']").append("<option value='" + node.id + "' "+('${param.identityId}' == node.id ? "selected" : "")+">"+node.name+"</option>");
            });
        }
        if ($statusList != null && $statusList != ''){
            $.each(JSON.parse($statusList), function (i, node) {
                $("select[name='issuePhase']").append("<option value='" + i + "' "+('${param.issuePhase}' == i ? "selected" : "")+">"+node+"</option>");
            });
        }
    });
</script>
<script src="/assets/g/js/source.js"></script>
<script src='/assets/g/js/paging.js'></script>
<script type="application/javascript">
    $(function () {
        var $table = $("#tbody");
        var pageSize = '${pageSize}';

        function getTr(num) {
            var $tr = $("<tr></tr>");
            for (var i = 0; i < num; i++) {
                $tr.append("<td></td>");
            }
            return $tr;
        }

        function createTable(data) {
            $table.html("");
            $.each(data, function (i, node) {
                var $tr = getTr(7);
                var td = $tr.find("td");
                $(td.get(0)).html(node.title);
                $(td.get(1)).html(node.identityName);
                $(td.get(2)).html(${statusList}[node.status]);
                $(td.get(3)).html(${kindList}[node.kind]);
                $(td.get(4)).html(node.taskSize);
                $(td.get(5)).html(timeStamp2StringYYYYMMDD(node.createTime));
                $table.append($tr);
            })

        }
        function createPage(count) {
            var opt = {
                $container: $("#page-nav"),
                className: "pagination pagination-sm",
                pageCount: Math.ceil(count/pageSize),
                beforeFun: function (targetNum) {
                    var data = {
                        identityId:'${param.identityId}',
                        issuePhase:'${param.issuePhase}',
                        title:'${param.title}',
                        taskSize:'${param.taskSize}',
                        pageSize: pageSize,
                        currentPage: targetNum - 1
                    };
                    $.ajax({
                        url: "/identity/list/stu",
                        data: data,
                        dataType: "json",
                        type: "post",
                        success: function (result) {
                            if (result.success) {
                                createTable(result.data);
                            } else {
                                alert(result.errorMsg);
                            }
                        }
                    });
                }
            };
            createPageNav(opt);
        }
        createPage('${requestScope.count}');
        createTable(JSON.parse('${requestScope.list}'));
    });
</script>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
</body>
</html>
