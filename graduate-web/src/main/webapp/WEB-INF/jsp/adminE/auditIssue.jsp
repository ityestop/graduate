<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>专家分配任务</title>
    <script src="/assets/g/js/adminAuditIssue.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./admin"> 首页 </a></li>
            <li class="active">专家管理</li>
            <li class="active">专家分配任务</li>
        </ol>
        <div class="page-header">
            <h4>分配任务</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="panel-body">
        <div class="form-group">
            <input type="hidden" value="${userId_audit}" id="userId_audit" />
            <input type="hidden" value="${userName_audit}" id="userName_audit"/>
            <div class="col-sm-2">
                <button class="btn btn-blue btn-block" id="tea_AuditIssue">校内题目</button>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-blue btn-block" id="stu_AuditIssue">公司题目</button>
            </div>

        </div>
        <div class="form-group">
            <table class="table table-hover"
                   style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                    <th width="35%" class="center">
                        <small>题目名称</small>
                    </th>
                    <th>
                        <small>题目来源</small>
                    </th>
                    <th>
                        <small>指导教师</small>
                    </th>
                    <th>
                        <small>题目状态</small>
                    </th>
                    <th>
                        <small>题目人数</small>
                    </th>
                    <th width="15%">
                        <small>操作</small>
                    </th>
                </tr>
                </thead>
                <tbody id="auditIssuseTop">
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
