<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<html>
<head>
<title>最终成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<script src="IAssets/js/selectCheckbox2.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">最终成绩</li>
			</ol>
			<div class="page-header">
				<h3>
					最终成绩
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="panel-body">
					<div class="tab-pane active">
							<table class="table  table-hover"
								style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
								<thead>
									<tr>
										<th width="5%"><small>编号</small>
										</th>
										<th width="13%"><small>学号</small>
										</th>
										<th><small>姓名</small>
										</th>
										<th width="13%"><small>答辩题目</small>
										</th>
										<th width="8%"><small>校外导师</small>
										</th>
										<th><small>指导教师</small>
										</th>
										<th><small>指导成绩</small>
										</th>
										<th><small>评阅成绩</small>
										</th>
										<th><small>答辩成绩</small>
										</th>
										<th><small>最终成绩</small>
										</th>
									</tr>
								</thead>
								<c:forEach items="${stulist}" var="t" varStatus="i">
									<tr>
										<td><small>${i.index + 1 }</small></td>
										<td><small>${t.stuStudentId}</small></td>
										<td><small>${t.stuName }</small></td>
										<c:choose>
											<c:when test="${t.topTitle ne null }">
												<c:if
													test="${t.childTitle eq null or t.childTitle eq '' or t.childTitle eq 'null' }">
													<td
														style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
														title="${t.topTitle}"><small><a
															href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
																${t.topTitle}</a> </small>
													</td>
												</c:if>
												<c:if
													test="${t.childTitle ne null and t.childTitle ne '' and t.childTitle ne 'null' }">
													<td
														style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
														title="${t.topTitle}-${t.childTitle}"><small><a
															href="./${sessionScope.pathCode}/topicDetailsTea.do?id=${t.topId}">
																${t.topTitle}</a> </small></td>
												</c:if>

											</c:when>
											<c:when test="${t.topTitle eq null }">
												<td
													style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><small><a
														href="javaScript:void(0);"> 无</a> </small>
												</td>
											</c:when>
										</c:choose>
										<td><small>${t.topOutTeacher eq null ? "无"
												:t.topOutTeacher }</small>
										</td>
										<td><small>${t.teaName}</small>
										</td>
										<td><small>${t.donGrade}</small>
										</td>
										<td><small>${t.documentGrade}</small>
										</td>
										<td><small>${t.replyGrade}</small>
										</td>
										<td><small>${t.finalReplyGrade}</small>
										</td>
									</tr>
								</c:forEach>
							</table>
					</div>
		</div>
	</div>

	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
