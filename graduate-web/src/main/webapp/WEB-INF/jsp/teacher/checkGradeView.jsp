<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<html>
<head>

<title>验收成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<script src="IAssets/js/selectCheckbox2.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">验收成绩</li>
			</ol>
			<div class="page-header">
				<h3>验收成绩</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="panel-body">
			<div class="tabbable">
				<div class="tab-content">
					<div class="tab-pane active">
						<table class="table  table-hover"
							style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
							<thead>
								<tr>
									<th width="5%"><small>编号</small>
									</th>
									<th width="15%"><small>学号</small></th>
									<th><small>学生姓名</small></th>
									<th><small>性别</small></th>
									<th><small>班级</small></th>
									<th><small>小组</small></th>
									<th width="15%"><small>课题</small></th>
									<th><small>任务</small></th>
									<th><small>验收成绩</small></th>
								</tr>
							</thead>
							<c:forEach items="${list}" var="s" varStatus="i">
								<tr>
									<td><small>${i.index + 1}</small></td>
									<td><small>${s.stuStudentId}</small>
									</td>
									<td><small>${s.stuName}</small>
									</td>
									<td><small>${s.stuSex ? "女" : "男"}</small>
									</td>
									<td><small>${s.stuClass}</small>
									</td>
									<td
										style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
										title="${s.sgSName}的小组"><small>${s.sgSName}的小组</small>
									</td>
									<td
										style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
										title="${s.topicTitle}"><small>${s.topicTitle}</small>
									</td>
									<td id="childTop"
										style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
										title="${s.childTopicTitle}"><small>${s.childTopicTitle}</small>
									</td>
									<c:choose>
										<c:when test="${s.checkGrade == 0}" >
											<td><small>未审核</small></td>
										</c:when>
										<c:when test="${s.checkGrade == 1}">
										<td><small>已通过-一辩</small></td></c:when>
										<c:when test="${s.checkGrade == 2}">
										<td><small>二次答辩</small></td></c:when>
										<c:when test="${s.checkGrade == 3}">
										<td><small>二次答辩通过</small></td></c:when>
										<c:when test="${s.checkGrade == -1}">
										<td><small>终止答辩</small></td></c:when>
										</c:choose>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
