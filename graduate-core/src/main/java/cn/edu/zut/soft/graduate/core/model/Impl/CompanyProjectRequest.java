package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyProjectRequest extends BaseModelImpl {

    private Integer groupId;

    private Integer identityId;

    private String identityName;

    private String phone;

    private String content;

    private Integer groupNum;

    private DeclareStatus status;

}