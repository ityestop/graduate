package cn.edu.zut.soft.graduate.core.constant.config;

/**
 * @author cc
 * @Date 2017/1/5
 * @value 1.0
 */
public interface TopicPhaseAble {
    String getTitle();

    String name();

    int ordinal();
}
