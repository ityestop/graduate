<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 2017/2/19
  Time: 下午10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>报告</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/plugins/wangEditor/dist/css/wangEditor.min.css">
    <script type="text/javascript" src="/assets/plugins/wangEditor/dist/plupload/plupload.full.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/wangEditor/dist/js/wangEditor.min.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/stu"> 首页 </a></li>
            <li><a href="/stu/report"> 报告列表 </a></li>
            <li class="active">添加新报告</li>
        </ol>
        <div class="page-header">
            <h3>添加新报告</h3>
            <font color="red">补交数据只有一次机会,提交之后无法修改,请慎重</font>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="panel-body">
        <form class="form-horizontal" id="form">
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    报告规则
                </label>
                <div class="col-sm-6">
                    <select class="form-control" name="reportRuleId" title="报告规则">
                    </select>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-primary" onclick="refreshRule('false')">刷新</a>
                    <a class="btn btn-warning" id="bjbuton" onclick="refreshRule('true')">补交</a>
                </div>
            </div>
            <div class="form-group">
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    内容
                </label>
                <div class="col-sm-6">
                    <textarea class="form-control" rows="20" name="context" id="context"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-2">
                    <button type="button" class="sub btn btn-primary" data-toggle="modal" data-target="#myModal"
                            disabled="disabled">提交
                    </button>
                    <button type="reset" class="btn btn-default">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body">
                是否提交
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary sub" disabled="disabled" onclick="submitForm()">提交</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    var $from = $("#form");
    var $reportRule = $("select[name='reportRuleId']");
    var $sub = $(".sub");
    function refreshRule(value) {
        $sub.attr('disabled', 'disabled');
        $reportRule.html("");
        $.ajax({
            type: 'GET',
            url: "/report/rule/conform?supplement="+value,
            dataType: 'json',
            success: function (result) {
                if (result.success == true) {
                    var data = result.data;
                    data.forEach(function (item) {
                        $reportRule.append("<option value='" + item.id + "'>第 " + item.num + " " + (item.type == 1 ? "周" : "月" ) + "报告" + "</option>");
                    });
                    $sub.removeAttr('disabled');
                } else {
                    alert(result.errorMsg);
                }
            }
        });
    }

    function submitForm() {
        var url = "/report/data";
        $.ajax({
            type: 'POST',
            url: url,
            data: $from.serialize(),
            dataType: 'json',
            success: function (result) {
                if (result.success == true) {
                    window.location.href = '/stu/report/listData';
                } else {
                    alert(result.errorMsg);
                }
            }
        })

    }
    refreshRule('false');
    var editor = new wangEditor('context');
    editor.create();

</script>

</body>
</html>

