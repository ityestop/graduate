<%--
  Created by IntelliJ IDEA.
  User: yangxiaotian
  Date: 17/5/3
  Time: 下午7:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>中期检查</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table,.table {
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">中期检查</li>
        </ol>
        <div class="page-header">
            <h4>中期检查<font color="red"><small>中期抽查代表为此学生为中期进度抽查人选,该学生成绩由管理员给予!</small></font>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <input type="hidden" id="groupLeader" value="${sessionScope.GroupgroupLeader}"/>
            <input type="hidden" id="groupSEC" value="${sessionScope.GroupgroupSEC}"/>
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>


<div class="modal fade" id="passInterimGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">中期进度成绩选择</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="passIterimUserId"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passInterimGrade" value="90"/>
                        </label> 进度超前
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passInterimGrade" value="70"/>
                        </label> 进度正常
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passInterimGrade" value="50"/>
                        </label> 进度滞后
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passInterimGrade" value="-1"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="interimGrade_grader_submit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restInterimGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置中期进度成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restIterimUserId"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="restInterimGrade" value="90"/>
                        </label> 进度超前
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="restInterimGrade" value="70"/>
                        </label> 进度正常
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="restInterimGrade" value="50"/>
                        </label> 进度滞后
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="restInterimGrade" value="-1"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="interim_grader_rest">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">

    var $goupLeader = $("#groupLeader").val();
    var $goupSEC = $("#groupSEC").val();

    function params() {
        return {
            value: ${user.id},
            infoType: 'Tea',
            key: 'guide'
        }
    }

    var $role = true;
    var columns = [
        {
            field: 'identity.id',
            title: '编号'
        },
        {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'infoList',
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var topicName = "";
                var topicId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC') {
                        if (array[i].key == 'topicName') {
                            topicName = array[i].value;
                        } else if (array[i].key == 'topicId') {
                            topicId = array[i].value;
                        }
                    }
                }
                return "<a href='/tea/issueDetail?id=" + topicId + "'>" + topicName + "</a>";
            },
            sortStable: true
        }, {
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "<label class='label label-warning'>未操作</label>";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'interimGrade') {
                        if (array[i].value == '90') {
                            str = "<label class='label label-success'>进度超前</label>";
                            break;
                        }else if(array[i].value == '70'){
                            str = "<label class='label label-info'>进度正常</label>";
                            break;
                        }else if(array[i].value == '50'){
                            str = "<label class='label label-primary'>进度滞后</label>";
                            break;
                        }
                    }else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                        if(array[i].value == 'openGrade'){
                            str = "<label class='label label-danger'>开题-毕设终止</label>";
                            break;
                        }else if(array[i].value == 'interimGrade'){
                            str = "<label class='label label-danger'>中期-毕设终止</label>";
                            break;
                        }
                    }
                }
                return str;
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                if ($role) {
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'interimGrade') {
                            str = "<a  data-toggle='modal' data-target='#restInterimGrade'>重置</a>";
                            break;
                        } else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                            if(array[i].value == 'openGrade'){
                                str = "<a href='javaScript:void(0);'>不可操作</a>";
                                break;
                            }else if(array[i].value == 'interimGrade'){
                                str = "<a  data-toggle='modal' data-target='#restInterimGrade'>重置</a>";
                                break;
                            }
                        }else if(array[i].type == 'GRADE' && array[i].key == 'MID'){
                            str = "<a  href='javaScript:void(0);'>中期抽查</a>"
                            break;
                        } else{
                            str = "<a  data-toggle='modal' data-target='#passInterimGrade'>进度选择</a>"
                        }
                    }
                } else {
                    str = "无操作";
                }
                return str;
            }
        }
    ];
    customSearch({
        id: "#table",
        url: "/identity/info/select",
        dataType: "json",
        params: params(),
        columns: columns,
        pageSize: 30
    });


    $("#passInterimGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#passIterimUserId").attr("value", $stuId);
    });

    $("#restInterimGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restIterimUserId").attr("value", $stuId);
    });

    $("#interimGrade_grader_submit").click(function () {
        var $stuId = $("#passIterimUserId").val();
        var $interimGrade = $("input[name='passInterimGrade']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passInterimGrade',
            data: {
                grade: $interimGrade,
                id: $stuId
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    });

    $("#interim_grader_rest").click(function () {
        var $stuId = $("#restIterimUserId").val();
        var $interimGrade = $("input[name='restInterimGrade']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/restInterimGrade',
            data: {
                grade: $interimGrade,
                id: $stuId
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    });
</script>
</body>
</html>
