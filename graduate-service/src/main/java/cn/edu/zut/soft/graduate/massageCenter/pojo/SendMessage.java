package cn.edu.zut.soft.graduate.massageCenter.pojo;

import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Message;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chuchuang
 * @date 16/11/17
 */
//@Data
public class SendMessage {
    Identity identity;
    List<Identity> identityList;
    Message message;
    Map<Integer,String> errorList;

    public SendMessage(){
        identityList = new ArrayList<>();
        errorList = new HashMap<>();
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public List<Identity> getIdentityList() {
        return identityList;
    }

    public void setIdentityList(List<Identity> identityList) {
        this.identityList = identityList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Map<Integer, String> getErrorList() {
        return errorList;
    }

    public void setErrorList(Map<Integer, String> errorList) {
        this.errorList = errorList;
    }
}
