package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Report extends BaseModelImpl {

    private String title;//报告标题
    private String type;//报告类型
    private String status;//状态
    private String statusSets;//
    private String content;//报告内容

}
