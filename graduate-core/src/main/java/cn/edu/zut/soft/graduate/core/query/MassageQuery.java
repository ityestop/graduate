package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Created by chuchuang on 16/11/13.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MassageQuery extends Query {

    private Integer id;
    private Integer addressor;//发件人
    private Integer recipients;//收件人
    private String title;//标题
    private Integer read;//是否已读
    private String type;//消息类型

    private Integer del;//

    private String addressorName;//发件人姓名
    private String recipientsName;//收件人姓名

}
