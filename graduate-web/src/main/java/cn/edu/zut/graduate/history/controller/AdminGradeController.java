//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.model.Function;
//import cn.edu.zut.soft.graduate.core.model.Student;
//import cn.edu.zut.soft.graduate.core.model.Teacher;
//import cn.edu.zut.soft.graduate.core.model.tools.StudentGradeTerm;
//import cn.edu.zut.soft.graduate.core.utils.DataLS;
//import cn.edu.zut.soft.graduate.core.utils.Excel;
//import cn.edu.zut.soft.graduate.core.utils.ExcelUtil;
//import org.apache.ibatis.session.RowBounds;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.ModelAndView;
//import tag.BaseController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.File;
//import java.io.OutputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author yxt
// */
//@Controller
//@RequestMapping("/admin")
//public class AdminGradeController extends BaseController {
//
//	private Role r = Role.ADMIN;
//
//	@Resource
//	private ServiceFit serviceFit;
//
//	@RequestMapping("toInterimScampling")
//	public ModelAndView toInterimScampling(Map<String, Object> model,
//			@RequestParam("DefiniteValue") String value) {
//		ResultDo resultDo;
//		int number = Integer.parseInt(value);
//		int max = 0;
//		resultDo = serviceFit.getEmpService().extractOfTea();
//		if (resultDo.isSuccess()) {
//			model.put("teacherCount",
//					((List<Teacher>) resultDo.getResult()).size());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		if (number == 0) {
//			resultDo = serviceFit.getEmpService().extractMaxNum();
//			max = (Integer) resultDo.getResult();
//			int[] count = new int[max];
//			List<List<Student>> student = new ArrayList<List<Student>>();
//			for (int i = 0; i < max; i++) {
//				ResultDo<?> resultDo1 = serviceFit.getEmpService()
//						.extractValue(i + 1);
//				if (resultDo1.isSuccess()) {
//					student.add((List<Student>) resultDo1.getResult());
//				}
//			}
//			for (int i = 0; i < count.length; i++) {
//				count[i] = i + 1;
//			}
//			model.put("count", count);
//			model.put("student", student);
//		} else {
//			resultDo = serviceFit.getEmpService().extractValue(number);
//			if (resultDo.isSuccess()) {
//				model.put("student", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("status", false);// 区别次数检索表格显示
//		model.put("menuSelected1", ConfigDo.GRADE);
//		model.put("menuSelected2", ConfigDo.INTERIMSCAMPLING);
//		return new ModelAndView(r.getName() + "/InterimSampling", model);
//	}
//
//	@RequestMapping("InterimSampling")
//	public ModelAndView InterimSampling(Map<String, Object> model,
//			@RequestParam("number") int number) {
//		ResultDo resultDo;
//		int max = 0;
//		resultDo = serviceFit.getEmpService().extractOfTea();
//		if (resultDo.isSuccess()) {
//			model.put("teacherCount",
//					((List<Teacher>) resultDo.getResult()).size());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//
//		if (number != 0) {
//			resultDo = serviceFit.getEmpService().extractByTeaOneAndNum(number);
//			if (resultDo.isSuccess()) {
//				resultDo = serviceFit.getEmpService().extractMaxNum();
//				max = (Integer) resultDo.getResult();
//				int[] count = new int[max];
//				List<List<Student>> student = new ArrayList<List<Student>>();
//				for (int i = 0; i < max; i++) {
//					resultDo = serviceFit.getEmpService().extractValue(i + 1);
//					student.add((List<Student>) resultDo.getResult());
//				}
//				for (int i = 0; i < max; i++) {
//					count[i] = i + 1;
//				}
//				model.put("count", count);
//				model.put("student", student);
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("status", false);// 区别次数检索表格显示
//		model.put("menuSelected1", ConfigDo.GRADE);
//		model.put("menuSelected2", ConfigDo.INTERIMSCAMPLING);
//		return new ModelAndView(r.getName() + "/InterimSampling", model);
//	}
//
//	@RequestMapping("InterimForNumber")
//	public ModelAndView InterimForNumber(Map<String, Object> model,
//			@RequestParam("count") int count) {
//		ResultDo resultDo;
//		int max = 0;
//		resultDo = serviceFit.getEmpService().extractOfTea();
//		if (resultDo.isSuccess()) {
//			model.put("teacherCount",
//					((List<Teacher>) resultDo.getResult()).size());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//
//		if (count > 0) {
//			resultDo = serviceFit.getEmpService().extractValue(count);
//			if (resultDo.isSuccess()) {
//				model.put("student", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		} else {
//			return new ModelAndView(
//					"redirect:./toInterimScampling.do?DefiniteValue="
//							+ DataLS.functionMap.get("InterimScampling")
//									.getContent());
//		}
//		resultDo = serviceFit.getEmpService().extractMaxNum();
//		max = (Integer) resultDo.getResult();
//		int[] count1 = new int[max];
//		for (int i = 0; i < max; i++) {
//			count1[i] = i + 1;
//		}
//		model.put("count", count1);
//		model.put("count2", count);
//		model.put("status", true);// 区别次数检索表格显示
//		model.put("menuSelected1", ConfigDo.GRADE);
//		model.put("menuSelected2", ConfigDo.INTERIMSCAMPLING);
//		return new ModelAndView(r.getName() + "/InterimSampling", model);
//	}
//
//	@RequestMapping("settingNumber")
//	public ModelAndView settingNumber(HttpSession session,
//			Map<String, Object> model, Function function) {
//		ResultDo resultDo;
//		if (function != null) {
//			resultDo = serviceFit.getFunctionService().saveOrUpdate(function);
//			if (resultDo.isSuccess()) {
//				Function function2 = (Function) resultDo.getResult();
//				session.removeAttribute("functionMap");
//				session.setAttribute("functionMap", DataLS.functionMap);
//				return new ModelAndView(
//						"redirect:./toInterimScampling.do?DefiniteValue="
//								+ function2.getContent());
//			}
//		}
//		return new ModelAndView(
//				"redirect:./toInterimScampling.do?DefiniteValue="
//						+ DataLS.functionMap.get("InterimScampling")
//								.getContent());
//	}
//
//	@RequestMapping("InterimGrade")
//	public ModelAndView InterimGrade(Map<String, Object> model,
//			@RequestParam("stuId") String stuId,
//			@RequestParam("grade") int grade) {
//		ResultDo resultDo;
//		if (stuId != null) {
//			resultDo = serviceFit.getEmpService().updateStudentInterim(stuId,
//					grade);
//			if (resultDo.isSuccess()) {
//				return new ModelAndView(
//						"redirect:./toInterimScampling.do?DefiniteValue="
//								+ DataLS.functionMap.get("InterimScampling")
//										.getContent());
//			}
//		} else {
//			model.put("mag", "数据错误！");
//		}
//
//		return new ModelAndView(
//				"redirect:./toInterimScampling.do?DefiniteValue="
//						+ DataLS.functionMap.get("InterimScampling")
//								.getContent(), model);
//	}
//
//	@RequestMapping("downloadEx")
//	public void downLoadEx(HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition",
//				"attachment;fileName=theResult.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//		resultDo = serviceFit.getUploadService().DownLoadEx();
//		if (resultDo.isSuccess()) {
//			// System.out.println("teach"+resultDo.getResult());
//			excel = (Excel) resultDo.getResult();
//			try {
//				OutputStream fout = response.getOutputStream();
//				ExcelUtil.excelFactory(excel).write(fout);
//				fout.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//	/**
//	 * 管理员端答辩成绩条件抽查
//	 * @param model
//	 * @param l
//	 * @param s
//	 * @param studentGradeTerm
//	 * @return
//	 */
//	@RequestMapping("toTermGrade")
//	public String toTermGrade(
//			Map<String, Object> model,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s,
//			StudentGradeTerm studentGradeTerm) {
//		ResultDo resultDo = null;
//
//		int totalCount = 0;
//		resultDo = serviceFit.getGradeService().selectGradeTerm(studentGradeTerm,
//				new RowBounds((l - 1) * s, s));
//		totalCount = ((List) serviceFit.getGradeService()
//				.selectGradeTerm(studentGradeTerm, new RowBounds()).getResult()).size();
//		if (resultDo.isSuccess()) {
//			model.put("student", resultDo.getResult());
//		}
//		ResultDo  resultDo2 = serviceFit.getEmpService().findAll(null, Role.TEA, new RowBounds());
//		if (resultDo2.isSuccess()) {
//			model.put("teaList", resultDo2.getResult());
//		}
//		model.put("menuSelected1", "grade");
//		model.put("menuSelected2", "toTermGrade");
//		initPage(model, l, s, totalCount);
//		return r.getName() + "/studentGradeAll";
//	}
//	@RequestMapping("downStudentGradeAll")
//	public void dowmLoad(HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition", "attachment;fileName=studentGrade.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//			resultDo = serviceFit.getUploadService().DownStudentGradeAll();
//			if (resultDo.isSuccess()) {
//				// System.out.println("teach"+resultDo.getResult());
//				excel = (Excel) resultDo.getResult();
//				try {
//					File file = new File("小组学生信息表.xls");
//					OutputStream fout = response.getOutputStream();
//					ExcelUtil.excelFactory(excel).write(fout);
//					fout.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
