<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/11/13
  Time: 下午3:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>收件箱</title>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/stu"> 首页 </a></li>
            <li class="active"></li>
        </ol>
        <div class="page-header">
            <h4>发送的邀请</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id='table' class='table table-container'></table>
        </div>
    </div>
</div>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript">
    $(function () {
        var columns = [{
            checkbox: true
        }, {
            field: 'id',
            title: '编号'
        }, {
            field: 'activesName',
            title: '邀请人'
        }, {
            field: 'passivesName',
            title: '受邀人'
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value, row, index) {
                return value == "ING" ? "邀请中" : (value == "YES" ? "同意邀请" : (value == "NO" ? "拒绝邀请" : ""));
            }
        }, {
            field: 'createTime',
            title: '创建时间',
            formatter: function (value, row, index) {
                return timeStampYYYYMMDDHHMM(value);
            }
        }];

        customSearch({id: "#table", url: "invite/self/initiative", dataType: "json", params: {}, columns: columns});
    });
</script>
</body>
</html>
