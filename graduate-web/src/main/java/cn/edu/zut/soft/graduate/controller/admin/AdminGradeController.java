package cn.edu.zut.soft.graduate.controller.admin;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>文件名称：AdminGradeController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/5 下午10:30</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Controller
@RequestMapping("/admin")
public class AdminGradeController extends BaseController {

    @Resource
    private IdentityBO identityBO;

    @RequestMapping("/toOpenGrade")
    public ModelAndView toOpenGrade(){return new ModelAndView("/adminE/openGrade");}

    @RequestMapping("/toOpenGradeSchool")
    public ModelAndView toOpenGradeSchool(){return new ModelAndView("/adminE/openGradeSchool");}

    @RequestMapping("/toTwoOpenGrade")
    public ModelAndView toTwoOpenGrade(){return new ModelAndView("/adminE/twoOpenGrade");}

    @RequestMapping("/toInterimGrade")
    public ModelAndView toInterimGrade(){return new ModelAndView("/adminE/grade/interimGrade");}

    @RequestMapping("/toSystemGrade")
    public ModelAndView toSystemGrade(){
        return new ModelAndView("/adminE/grade/systemGrade");
    }

    @RequestMapping("/toReplyGrade")
    public ModelAndView toReplyGrade(){
        return new ModelAndView("/adminE/grade/replyGrade");
    }


    @ResponseBody
    @RequestMapping("/outIssueOpneGrade")
    public CommonResult getOutIssueOpenGrade(IdentityQuery identityQuery){
        return identityBO.queryByOutSchool(identityQuery);
    }

    @ResponseBody
    @RequestMapping("/outIssueTwoOpenGrade")
    public CommonResult getIssueTwoOpenGrade(IdentityQuery identityQuery){
        return identityBO.queryByTwoOpenGrade(identityQuery);
    }

    @ResponseBody
    @RequestMapping("/outIssueOpneGradeSchool")
    public CommonResult getOutIssueOpenGradeSchool(IdentityQuery identityQuery){
        return identityBO.queryBySchool(identityQuery);
    }



}
