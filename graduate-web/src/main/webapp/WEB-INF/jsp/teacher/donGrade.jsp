<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/12/10
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<title>毕业设计管理平台</title>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a>
				</li>
				<li class="active">指导成绩</li>
			</ol>
			<div class="page-header">
				<h3>
					指导成绩  <small><font color="red">Ps：答辩成绩与指导成绩是同一阶段成绩，例:答辩成绩80、指导成绩应在80或80以上 --指导成绩与答辩成绩上下浮动3分，望给予成绩时慎重</font></small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
		<div class="row">
			<div class="col-md-12">
				<!-- start: TABLE WITH IMAGES PANEL -->
				<div class="panel-body">
					<div class="tabbable">
						<table class="table table-hover"
							style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
							<thead>
								<tr>
									<th width="5%"><small>编号</small></th>
									<th width="15%"><small>学号</small>
									</th>
									<th><small>学生姓名</small>
									</th>
									<th><small>性别</small>
									</th>
									<th><small>班级</small>
									</th>
									<th width="15%"><small>课题</small>
									</th>
									<th><small>任务</small>
									</th>
									<th class="center"><small>指导成绩</small>
									</th>
									<th><small>操作</small>
									</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list}" var="s" varStatus="i">
									<tr>
										<input type="hidden" class="stuId" value="${s.stuId}">
										<input type="hidden" class="stuName" value="${s.stuName}">
										<input type="hidden" class="donGrade" value="${s.donGrade}">
										<input type="hidden" class="replyGrade" value="${s.replyGrade}" >
										<input type="hidden" class="finalReplyGrade" value="${s.finalReplyGrade}" >
										<input type="hidden" class="documentGrade" 	value="${s.documentGrade}">
										<td><small>${i.index + 1}</small></td>
										<td><small>${s.stuStudentId}</small></td>
										<td><small>${s.stuName}</small></td>
										<td><small>${s.stuSex ? "女" : "男"}</small></td>
										<td><small>${s.stuClass}</small></td>
										<td
											style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
											title="${s.topicTitle}"><small>${s.topicTitle}</small></td>
										<td id="childTop"
											style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
											title="${s.childTopicTitle}"><small>${s.childTopicTitle}</small>
										</td>
										<td><small>
										<fmt:formatNumber value="${s.donGrade}" pattern="0" />
										</small>
										</td>
										<td>
										<small>
										<c:if test="${s.finalReplyGrade eq 0}">
										<a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#finalGrade">给予成绩</a>
										</c:if>
										<c:if test="${s.finalReplyGrade > 0}">
										<a href="javaScript:void(0)">答辩结束</a>
										</c:if>
										</small>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
<div class="modal fade" id="finalGrade" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
    <form action="./${sessionScope.pathCode}/donGradeOne.do"
          class="form-horizontal" id="Fgrade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">编辑指导成绩</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="stuId">
                    <input type="hidden" id="replyGradeF">
                     <div class="modal-body">
                    <div class="form-group">
                   
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary" id="send">确认</button>
                </div>
            </div>
        </div>
    </form>
</div> 
<script>
    $("#finalGrade").on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
        var $stuId = $p.find(".stuId");
        var $stuName = $p.find(".stuName");
        var $donGrade = $p.find(".donGrade");
        var $replyGrade = $p.find(".replyGrade");
        var $documentGrade = $p.find(".documentGrade");
        var $finalReplyGrade = $p.find(".finalReplyGrade");
        var $modal = $(this);
        var str = "<font color='red'>	 注意：答辩成绩与指导成绩是同一阶段成绩，例:答辩成绩80、指导成绩应在80或80以上 <br/>指导成绩与答辩成绩上下浮动3分，望给予成绩时慎重！<br/>若出现界面缓存问题导致提交按钮无响应请刷新,请谅解！ </font>"+"<br/>"+"学生姓名："+$stuName.val()+"<br/>"+"评阅成绩："+$documentGrade.val()+"<br/>"+"答辩成绩："+$replyGrade.val()+"<br/>"+"原指导成绩："+$donGrade.val()+"<br/> <br/>"+"指导成绩：<input type='text' name='grade' id='finalGradeF'>";
        $modal.find("input[name='stuId']").val($stuId.val());
        $modal.find("input[id='replyGradeF']").val($replyGrade.val());
        $modal.find(".modal-body .form-group").html(str);
    });
</script>
<script>
	$(function(){
			  $("#send").click(function(){
				 	var don=document.getElementById("finalGradeF").value;
					var fGra=document.getElementById("replyGradeF").value;
					var don1=parseInt((don-0)/10);
					var fGra1=parseInt((fGra-0)/10);
					var bbb=Math.abs((don-0)-(fGra));
					  if(fGra != 0){
					  if(don1 != fGra1){
						  alert("指导成绩要与答辩成绩在同一阶段，例:指导成绩80 答辩成绩应该在80以上！");
					  	$("#Fgrade").submit( function () {
					  	  return false;
					  	} );
					  }
					  if(bbb >=4){
						  alert("指导成绩应与答辩成绩上下浮动不要超过3分！");
						  $("#Fgrade").submit( function () {
							  	  return false;
							  	} );
					  }
					  if(bbb <4){
						  $("#Fgrade").submit();
					  }
					  }else{
						  $("#Fgrade").submit();
					  }
	         });
		
	});
</script>
<script>
	 /**function checkgrade(){
		var grades=document.getElementsByName("grade");
		var replyGrades=document.getElementsByName("replyGrade");
		var finalReplyGrades=document.getElementsByName("finalReplyGrade");
		var res=false;
		for (var i = 0, j = grades.length; i < j; i++){
			
			if(finalReplyGrades[i].value > 0){
				continue;
			}		
			if(replyGrades[i].value > 0){
			if(grades[i].value>100 || grades[i].value < 0){
				alert("你输入的成绩有不符合要求的数据");
				return res;
			}else if (finalReplyGrades[i].value != 0 && grades[i].value != null && grades[i].value != "" && grades[i].value != undefined){
				alert(grades[i].value);
				alert(grades[i].value != null);
				alert("该学生有最终成绩，不可以修改");
				return res;
			}else if (parseInt( grades[i].value/10) == parseInt( replyGrades[i].value/10)) {
				alert("---2");
				if (Math.abs(replyGrades[i].value - grades[i].value)<4){
					res=true;
					return res;
				}else{
					alert("指导成绩应与答辩成绩上下浮动不要超过3分！");
					return res;
				}
			}else{
					alert("指导成绩和答辩成绩不在同一个阶段内");
					return res;
			}
			}else if(replyGrades[i].value == 0){
				res=true;
				return res;
			}
		}
	}**/
</script>
</body>
</html>
