package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.fileUploadCenter.bo.FileUploadBO;
import cn.edu.zut.soft.graduate.fileUploadCenter.bo.FileUploadBOImpl;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.userCenter.bo.StudentBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by chuchuang on 16/10/29.
 */
@Controller
@RequestMapping("file")
public class FileUploadController extends BaseController {

    @Resource
    private FileUploadBO fileUploadBO;

    @Autowired
    private StudentBO studentBO;

    @Resource
    private GroupBO groupBO;
    @Value("#{commonProp.thesis_proposal_stuTable_sheet}")
    private String filename;

    private final static String GradeStatausStudent = "小组系统验收成绩";

    /**
     * 数据文件上传
     * @param session
     * @param file
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String save(HttpSession session, MultipartFile file) {
        LoginVO loginVO = (LoginVO) session.getAttribute("user");
        String result;
        try {
            result = fileUploadBO.upload(file.getInputStream(),file.getOriginalFilename(),file.getSize(), loginVO);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            result= "保存失败";
        }
        return result;
    }

    /**
     * 学生数据文件上传并解析
     * @param session
     * @param file
     * @return
     */
    @RequestMapping("stu")
    @ResponseBody
    public String stuAll(HttpSession session, MultipartFile file){
        LoginVO loginVO = (LoginVO) session.getAttribute("user");
        String result;
        try {
            result = studentBO.uploadStudent(file.getInputStream(),file.getOriginalFilename(),file.getSize(), loginVO);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            result= "保存失败";
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        return null;
    }

    @RequestMapping("downloadTeaGroupStu")
    public void downloadTeaGroupStu(HttpServletResponse response, IdentityQuery identityQuery) throws IOException {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + filename+".xls");
        OutputStream outputStream = response.getOutputStream();
        //下载全部
        identityQuery.clear();
        fileUploadBO.downloadTeaGroupStu(response.getOutputStream(),identityQuery);
        outputStream.flush();
        outputStream.close();
    }
    @RequestMapping("/downloadGradeStatus")
    public void downloadTeaGroupStu(HttpServletResponse response, IdentityQuery identityQuery, String status)throws IOException{
//        String filename = getGroupFileName(String.valueOf(identityQuery.getGroupId()));
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + identityQuery.getGroupId() +".xls");
        OutputStream outputStream = response.getOutputStream();
        //下载全部
        identityQuery.clear();
        fileUploadBO.downloadGradeStu(response.getOutputStream(),identityQuery, status);
        outputStream.flush();
        outputStream.close();
    }


    @RequestMapping("/downloadGuideStu")
    public void downloadGuideStu(HttpServletResponse response, @RequestParam("teaId") Integer teaId)throws IOException{
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + teaId +".xls");
        OutputStream outputStream = response.getOutputStream();
        fileUploadBO.downloadGuideStudent(outputStream, teaId);
        outputStream.flush();
        outputStream.close();
    }


    @RequestMapping("/downloadFinalGrade")
    public void downloadFinalGrade(HttpServletResponse response)throws IOException{
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=finalGrade.xls");
        OutputStream outputStream = response.getOutputStream();
        fileUploadBO.downloadFinalGrade(outputStream);
        outputStream.flush();
        outputStream.close();
    }


    private String getGroupFileName(String groupId){
        String result = null;
        if (StringUtils.isEmpty(groupId)){
            return GradeStatausStudent;
        }
        Integer groupIds = Integer.parseInt(groupId);
        Group group = groupBO.get(groupIds);
        result = group.getIdentityName() + GradeStatausStudent;
        return result;
    }
}
