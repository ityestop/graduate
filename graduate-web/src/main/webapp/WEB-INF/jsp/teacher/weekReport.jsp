<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>软件学院毕业设计管理平台</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
</head>

<body>

	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a>
				</li>
				<li class="active">周月报管理</li>
			</ol>
			<div class="page-header">
				<h3>
					周月报管理 <small><font color="red">当前周-第${weekCount}周</font> </small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<!-- start: GENERAL PANEL -->
			<div class="panel">
				<div class="panel-body">
					<sf:form class="form-horizontal" id="condition"
						action="./${sessionScope.pathCode}/teaWeekReportList.do"
						method="post" commandName="reportTeaSelect">
						<div class="form-gorup">
							<input type="hidden" name="teaId"
								value="${sessionScope.user.teaId}"> <label
								class="col-sm-1 control-label">学生姓名 </label>
							<div class="col-sm-3">
								<sf:select class="form-control search-select" path="stuName">
									<sf:option value="null" label="全部" />
									<sf:options items="${student}" itemValue="stuName"
										itemLabel="stuName" />
								</sf:select>
							</div>

							<label class="col-sm-1 control-label">类型 </label>
							<div class="col-sm-3">
								<sf:select class="form-control search-select" path="typ">
									<sf:option value="-1">全部</sf:option>
									<sf:option value="1">周报</sf:option>
									<sf:option value="2">月报</sf:option>
								</sf:select>
							</div>

							<label class="col-sm-1 control-label">状态 </label>
							<div class="col-sm-3">
								<sf:select class="form-control search-select" path="status">
									<sf:option value="-1">全部</sf:option>
									<sf:option value="2">未审核</sf:option>
									<sf:option value="3">已审核</sf:option>
								</sf:select>
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="from-group">
							<label class="col-sm-1 control-label"> 题目名称 </label>
							<div class="col-sm-3">
								<input type="text" placeholder="题目名称(模糊查询)" name="topic"
									class="form-control">
							</div>
							<lable class="col-sm-1 control-label">周/月次</lable>
							<div class="col-sm-3">
								<sf:select class="form-control search-select" path="num">
									<sf:option value="-1" label="全部" />
									<sf:option value="3" label="3" />
									<sf:options items="${number}" />
								</sf:select>
							</div>
							
							<div class="col-sm-1">
								<button type="submit" class="btn btn-primary">检索</button>
							</div>
						</div>
					</sf:form>
				</div>
				<div class="panel-body">
					<table class="table 
                 table-hover">
						<thead>
							<tr>
								<th><small>编号</small></th>
								<th><small>学号</small></th>
								<th><small>姓名</small></th>
								<th><small>周/月报名称</small></th>
								<th><small>周/月次</small></th>
								<th><small>成绩</small></th>
								<th><small>类型</small></th>
								<th><small>状态</small></th>
								<th><small>操作</small></th>
							</tr>
						</thead>
						<c:forEach items="${ReportList}" var="week" varStatus="i">
							<tbody>
								<tr>
									<td><small> ${i.index+1}</small>
									</td>
									<td><small>${week.student.stuStudentId}</small>
									</td>
									<td><small>${week.student.stuName}</small>
									</td>
									<td><small><a
											href="./${sessionScope.pathCode}/showReport.do?id=${week.id}&status=0">${week.title}</a>
									</small>
									</td>
									<td><small>${week.number }</small>
									</td>
									<td><small> <c:if test="${week.value eq 90 }">A</c:if>
											<c:if test="${week.value eq 80 }">B</c:if> <c:if
												test="${week.value eq 70 }">C</c:if> <c:if
												test="${week.value eq 60 }">D</c:if> <c:if
												test="${week.value eq 0 }">0</c:if> </small></td>

									<td><c:if test="${week.type eq 1 }">
											<small>周报</small>
										</c:if> <c:if test="${week.type eq 2 }">
											<small>月报</small>
										</c:if>
									</td>
									<td><c:if test="${week.status eq 2 }">
											<small class="label label-inverse"
												style="font-size: 75% !important;">未审核</small>
										</c:if> <c:if test="${week.status eq 3  }">
											<small class="label label-success"
												style="font-size: 75% !important;">已审核</small>
										</c:if>
									</td>
											<td>
												<c:if test="${sessionScope.functionMap['reportSubmit'].status eq true }">
													<c:if test="${week.status eq 2 }">
													<small>
											 			<a href="./${sessionScope.pathCode}/showReport.do?id=${week.id}&status=0">给予成绩</a>
											 			<a	href="./${sessionScope.pathCode}/BackStatusForReport.do?weekId=${week.id}">退回状态</a>
											 		</small>
													</c:if>
												</c:if>
												
												<c:if test="${sessionScope.functionMap['reportSubmit'].status eq false }">
														<c:if test="${week.status eq 2}">
															<small>
											 					<a href="./${sessionScope.pathCode}/showReport.do?id=${week.id}&status=0">给予成绩</a>
											 				</small>
														<c:choose>
																<c:when	test="${week.type eq 1 and week.number eq weekCount or week.type eq 2 and week.number eq monthCount }">
															<small>
																<a	href="./${sessionScope.pathCode}/BackStatusForReport.do?weekId=${week.id}">退回状态</a>
															</small>
																</c:when>
																<c:otherwise>
															<small>
																<a href="javaScript:void(0);" data-toggle="modal"
															data-target="#TimeNo">退回状态</a>
															</small>
																</c:otherwise>
														</c:choose> 
														</c:if> 
												</c:if>
												<c:if test="${week.status eq 3 }">
													<small>
														<a href="./${sessionScope.pathCode}/showReport.do?id=${week.id}&status=1">重新审核</a>
													</small>
												</c:if>
										</td>
								</tr>
							</tbody>
						</c:forEach>
					</table>
					<!-- 	<div class="form-group">
						<label class="col-sm-4 "> </label>
						<div class="col-sm-8" style="text-align: right; height: 40px">
							<page:createPager pageSize="${pageSize}" totalPage="${totalPage}"
								totalCount="${totalCount}" curPage="${pageNum}"
								formId="condition" />
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="TimeNo" tabindex="-1" role="dialog"
		aria-labelledby="CSGModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="CSGModalLabel">	权限问题</h4>
				</div>
				<div class="modal-body">
					您好，在管理员开启补交周月报功能的时间段内可以自由退回状态！<br />
					若管理员未开启补交周月报功能时间段内，教师只有退回当前周或当前月的周报/月报功能！<br/>
					例：某学生第4周提交周报只有再第4周这一周内有权限退回状态,到第5周变没有权限退回状态/月报同理！
					<br /> 当前时间：${monthCount}月--第${weekCount}周
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();

    });
</script>
</body>
</html>
