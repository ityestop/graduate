package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class CompanyProjectRequestQuery extends Query {

    private Integer groupId;

    private Integer identityId;

    private String identityName;

    private String phone;

    private String content;

    private DeclareStatus status;

    private List<Integer> ids;

    private List<DeclareStatus> statusList;

    private Integer del;

}