package cn.edu.zut.soft.graduate.controller.util;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.util.StringUtils;
import cn.edu.zut.soft.graduate.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author cc
 * @Date 2017/1/12
 * @value 1.0
 */
@RequestMapping("/utils")
@Controller
public class UtilController extends BaseController {

    @RequestMapping("createNid")
    public CommonResult nid(@RequestParam(required = false,defaultValue = "4") String length){
        return CommonResult.successReturn(StringUtils.createNid(Integer.parseInt(length)));
    }

}
