//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.model.Power;
//import org.apache.ibatis.session.RowBounds;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import javax.annotation.Resource;
//
///**
// * Created by sky on 15/11/7.
// */
//@Controller
//@RequestMapping("/admin/")
//public class PowerController implements ConfigDo{
//    private Role r = Role.ADMIN;
//
//
//    @Resource
//    private ServiceFit serviceFit;
//
//    @RequestMapping("allPower")
//    public String allPower(Model model,@RequestParam(value = "leaf", required = false, defaultValue = "0") int l,@RequestParam(value = "size", required = false, defaultValue = "20") int s) {
//        ResultDo resultDo = serviceFit.getPowerService().findAllPowers(new RowBounds(l * s,s));
//        if (resultDo.isSuccess()){
//            model.addAttribute("powerList",resultDo.getResult());
//        }else {
//            model.addAttribute("mag",resultDo.getMessage());
//        }
//        model.addAttribute("menuSelected1", ConfigDo.POWER);
//    	model.addAttribute("menuSelected2", ConfigDo.ADMIN_POWER);
//        return r.getName() + "/allPower";
//    }
//
//    @RequestMapping("powerUpDetail")
//    public String powerUpDetail(Model model,@RequestParam("id") String id){
//        ResultDo resultDo;
//        resultDo = serviceFit.getPowerService().load(id);
//        if (resultDo.isSuccess()){
//            model.addAttribute("power",resultDo.getResult());
//        }else {
//            model.addAttribute("mag",resultDo.getMessage());
//        }
//        return r.getName() + "/powerUpDetail";
//    }
//
//    @RequestMapping("submitPower")
//    public String submitPower(Power power, Model model, @RequestParam(value = "com",defaultValue ="1" ) int[] com){
////        System.out.println(com.length);
//        int n = 0;
//        for (int i : com) {
//            n += i;
//        }
//        power.setCommon(n);
//        serviceFit.getPowerService().saveOrUpdateRight(power);
//        return "redirect:./allPower.do";
//    }
//
//
//}
