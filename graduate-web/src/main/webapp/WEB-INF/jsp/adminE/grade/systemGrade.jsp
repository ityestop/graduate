<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>系统验收成绩</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table{
            font-size: 12px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">系统验收成绩</li>
        </ol>
        <div class="page-header">
            <h4>验收成绩 <small>请按照自己需要条件进行检索,当检索条件为空时导出所有学生,支持条件导出哦</small>
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12 ">
        <div class="form-group form-horizontal">
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    答辩小组
                </label>
                <div class="col-sm-5">
                    <select class="form-control search-select" id="groupId">
                        <option value="">请选择</option>
                    </select>
                </div>

                <label class="col-sm-1 control-label">
                    验收状态
                </label>
                <div class="col-sm-5">
                    <select class="form-control search-select" title="状态列表" id="systemGradeStatus">
                        <option value="">请选择</option>
                        <option value="systemGrade_two">二次验收</option>
                        <option value="replyGrade_one">进入一辩</option>
                        <option value="replyGrade_two">进入二辩</option>
                        <option value="stop">终止答辩</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label">
                    姓名
                </label>
                <div class="col-sm-5">
                    <input name="title" class="form-control" placeholder="学生姓名" id="name">
                </div>

                <div class="col-sm-1" style="text-align: right;">
                    <button id="findStu" class="btn btn-primary">检索</button>
                </div>

                <div class="col-sm-1">
                    <button id="dateExpert" class="btn btn-primary">系统验收数据导出</button>
                </div>
            </div>
        </div>
        <div class="form-group" id="exportTable">
        </div>
        <div class="form-group" id="exportTableTwo">
        </div>

    </div>
</div>


<div class="modal fade" id="passSystemGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">系统验收成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="passStuId"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemOne" value="1001"/>
                        </label> 二次系统验收
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemOne" value="1002"/>
                        </label> 进入一辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemOne" value="1003"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="passSystemGradeSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restSystemGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">重置中期进度成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restStuId"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2001"/>
                        </label> 二次系统验收
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2002"/>
                        </label> 进入一辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2003"/>
                        </label> 进入二辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2004"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restSystemGradeSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="systemGradeTwo" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">系统验收成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restStuIdTwo"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemThree" value="2003"/>
                        </label> 进入二辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemThree" value="2004"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restSystemSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>


<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">
    var $groupIdTea='';
    var $name='';
    var $status='';
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: "/admin/getAllTeaGroup",
        data: {
            pageSize: 1000
        },
        success: function (result) {
            if (result.success) {
                var data = result.data;
                $.each(data, function (key, value) {
                    $("#groupId").append("<option value=" + value.id + ">" + value.identityName + "</option>");
                })
            }
        }
    });

    $("#findStu").click(function () {
        selectData();
    });

    function selectData() {
        $groupIdTea= $("#groupId").val();
        $name= $("#name").val();
        $status= $("#systemGradeStatus").val();

        $("#exportTableTwo").html('<table id="table" class="table table-container"></table>');
        var columns = [
            {
                field: 'identity.id',
                title: '编号'
            },
            {
                field: 'identity.noId',
                title: '学号'
            }, {
                field: 'identity.name',
                title: '姓名'
            }, {
                field: 'identity.phone',
                title: '手机号'
            }, {
                title: '班级',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                            str += array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '指导教师',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            }, {
                title: '课题',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var $strOne = "";
                    var $aone = "<a href='/admin/issue?id=";
                    var $atwo = "'>";
                    var $athree = "</a>";
                    var $result = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'topicName') {
                            $strOne = array[i].value;
                            break;
                        }
                    }
                    for (var j = 0; j < array.length; j++) {
                        if(array[j].type == "TOPIC" && array[j].key == "topicId"){
                            $result = $aone+array[j].value+$atwo+$strOne+$athree;
                            break;
                        }else{
                            $result = $strOne;
                        }
                    }
                    return $result;
                }
            }, {
                title: '子任务',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                            str = array[i].value;
                            break;
                        }
                    }
                    return str;
                }
            },{
                title: '成绩',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "<label class='label label-warning'>未操作</label>";
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'systemGrade_two') {
                            str = "<label class='label label-danger'>二次系统验收</label>";
                            break;
                        }else if(array[i].type == 'GRADE' && array[i].key == 'replyGrade_one'){
                            str = "<label class='label label-danger'>进入一辩</label>";
                            break;
                        }else if(array[i].type == 'GRADE' && array[i].key == 'replyGrade_two'){
                            str = "<label class='label label-danger'>进入二辩</label>";
                            break;
                        }else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                            if(array[i].value == 'openGrade'){
                                str = "<label class='label label-danger'>开题-毕设终止</label>";
                                break;
                            }else if(array[i].value == 'interimGrade'){
                                str = "<label class='label label-danger'>中期-毕设终止</label>";
                                break;
                            }else if(array[i].value == 'systemGrade'){
                                str = "<label class='label label-danger'>系统验收-毕设终止</label>";
                                break;
                            }
                        }
                    }
                    return str;
                }
            },{
                title: '操作',
                formatter: function (value, row, index) {
                    var array = row.infoList;
                    var str = "";
                    if (true) {
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'GRADE' && array[i].key == 'systemGrade_two') {
                                str = "<a  data-toggle='modal' data-target='#systemGradeTwo'>二次系统验收</a>";
                                break;
                            } else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                                if(array[i].value == 'openGrade'){
                                    str = "<a href='javaScript:void(0);'>不可操作</a>";
                                    break;
                                }else if(array[i].value == 'systemGrade'){
                                    str = "<a  data-toggle='modal' data-target='#restSystemGrade'>重置</a>";
                                    break;
                                }
                            }else if((array[i].type == 'GRADE' && array[i].key == 'replyGrade_two') || (array[i].type == 'GRADE' && array[i].key == 'replyGrade_one')){
                                str = "<a  data-toggle='modal' data-target='#restSystemGrade'>重置</a>";
                                break;
                            }else{
                                str = "<a  data-toggle='modal' data-target='#passSystemGrade'>验收成绩</a>"
                            }
                        }
                    } else {
                        str = "无操作";
                    }
                    return str;
                }
            }

        ];
        customSearch({
            id: "#table",
            url: "/identity/getGradeStatus",
            dataType: "json",
            params: {groupId:$groupIdTea, name:$name,status:$status},
            columns: columns,
            pageSize: 25
        });
    }

    $("#dateExpert").click(function () {
        $groupIdTea= $("#groupId").val();
        $name= $("#name").val();
        $status= $("#systemGradeStatus").val();
        window.location.href='/file/downloadGradeStatus?groupId='+$groupIdTea+'&name='+$name+'&status='+$status;
    });

    $(function () {
        selectData();
    });


    $("#passSystemGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#passStuId").attr("value", $stuId);
    });

    $("#restSystemGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restStuId").attr("value", $stuId);
    });

    $("#systemGradeTwo").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restStuIdTwo").attr("value", $stuId);
    });


    $("#passSystemGradeSubmit").click(function () {
        var $stuId = $("#passStuId").val();
        var $code = $("input[name='passSystemOne']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passSystemGrade',
            data: {
                stuId: $stuId,
                code: $code
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else {
                    alert(result.errorMsg);
                }
            }
        });
    });

    $("#restSystemGradeSubmit").click(function () {
        var $stuId = $("#restStuId").val();
        var $code = $("input[name='passSystemTwo']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passSystemGrade',
            data: {
                stuId: $stuId,
                code: $code
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    });

    $("#restSystemSubmit").click(function () {
        var $stuId = $("#restStuIdTwo").val();
        var $code = $("input[name='passSystemThree']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passSystemGrade',
            data: {
                stuId: $stuId,
                code: $code
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    });
</script>

</body>
</html>
