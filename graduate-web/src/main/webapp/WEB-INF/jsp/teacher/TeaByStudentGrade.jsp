<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/12/10
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>毕业设计管理平台</title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <c:if test="${GradeName eq 'open'}">
            <li class="active">开题成绩</li>
			</c:if>
			<c:if test="${GradeName eq 'iterim'}">
            <li class="active">期中进度</li>
			</c:if>
        </ol>
        <div class="page-header">
        <c:if test="${GradeName eq 'open'}">
            <h3>开题成绩</h3>
			</c:if>
			<c:if test="${GradeName eq 'iterim'}">
			 <h3>期中进度</h3>
			</c:if>
           
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
        	<div class="tabbable">
                <c:if test="${GradeName eq 'open' }">
                <table class="table table-hover"
                       style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                    <thead>
                    <tr>
                        <th width="5%"><small>编号</small></th>
                        <th width="15%">
                            <small>学号</small>
                        </th>
                        <th>
                            <small>学生姓名</small>
                        </th>
                        <th>
                            <small>性别</small>
                        </th>
                        <th>
                            <small>班级</small>
                        </th>
                        <th >
                            <small>小组</small>
                        </th>
                        <th width="15%">
                            <small>课题</small>
                        </th>
                        <th>
                            <small>任务</small>
                        </th>
                        <th>
                            <small>开题成绩</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="s" varStatus="i">
                        <tr>
                            <input type="hidden" class="stuId" value="${s.stuId}">
                            <input type="hidden" class="topId" value="${s.topId}">
                            <td><small>${i.index + 1}</small></td>
                            <td>
                                <small>${s.stuStudentId}</small>
                            </td>
                            <td>
                                <small>${s.stuName}</small>
                            </td>
                            <td>
                                <small>${s.stuSex ? "女" : "男"}</small>
                            </td>
                            <td>
                                <small>${s.stuClass}</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.sgSName}的小组">
                                <small>${s.sgSName}的小组</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.topicTitle}">
                                <small>${s.topicTitle}</small>
                            </td>
                            <td id="childTop" style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.childTopicTitle}">
                                <small>${s.childTopicTitle}</small>
                            </td>
                            <td><small>${s.openTopic eq false ? "未通过":"通过" }</small></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                </c:if>
                <c:if test="${GradeName eq 'iterim'}">
                <table class="table table-hover"
                       style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                    <thead>
                    <tr>
                        <th width="5%"><small>编号</small></th>
                        <th width="15%">
                            <small>学号</small>
                        </th>
                        <th>
                            <small>学生姓名</small>
                        </th>
                        <th>
                            <small>性别</small>
                        </th>
                        <th>
                            <small>班级</small>
                        </th>
                        <th >
                            <small>小组</small>
                        </th>
                        <th width="15%">
                            <small>课题</small>
                        </th>
                        <th>
                            <small>任务</small>
                        </th>
                        <th>
                            <small>期中进度</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="s" varStatus="i">
                        <tr>
                            <input type="hidden" class="stuId" value="${s.stuId}">
                            <input type="hidden" class="topId" value="${s.topId}">
                            <td><small>${i.index + 1}</small></td>
                            <td>
                                <small>${s.stuStudentId}</small>
                            </td>
                            <td>
                                <small>${s.stuName}</small>
                            </td>
                            <td>
                                <small>${s.stuSex ? "女" : "男"}</small>
                            </td>
                            <td>
                                <small>${s.stuClass}</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.sgSName}的小组">
                                <small>${s.sgSName}的小组</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.topicTitle}">
                                <small>${s.topicTitle}</small>
                            </td>
                            <td id="childTop" style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.childTopicTitle}">
                                <small>${s.childTopicTitle}</small>
                            </td>
                            <td>
                            	<small>
                            		<c:if test="${s.interimGrade eq 90}">
                            		进度较好
                            		</c:if>
                            		<c:if test="${s.interimGrade eq 80}">
                            		进度一般
                            		</c:if>
                            		<c:if test="${s.interimGrade eq 60}">
                            		进度较慢
                            		</c:if>
                            		<c:if test="${s.interimGrade eq -1}">
                            		终止答辩
                            		</c:if>
                            		<c:if test="${s.interimGrade eq 0}">
                            		教师未审批
                            		</c:if>
                           		 </small>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                </c:if>
                  </div>
               </div>
            </div>
    </div>
</div>
<script>
    $('#assignChildTopic').on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
        var $stuId = $p.find(".stuId");
        var $topId = $p.find(".topId");
        var $modal = $(this);
//        alert($stuId.val() + "________" + $topId.val());
        $.ajax({
            url:"./teacher/childTopic.do",
            type:"post",
            data:{
                topId: $topId.val()
            },
            success:function(data){
                var str = "";
                if (data.status == true){
                    var $top = data.topic;
                    var $cit = $top.childTopicList;
//                    alert($cit.length);
                    for (var i = 0; i< $cit.length;i++){
                        if ($cit[i].studentId == null || $cit[i].studentId == 'null'){
                            str += "<div><label> <input type='radio' value='"+$cit[i].id+"' name='childId' class='grey' >" + $cit[i].name + "</label>";
                            str += "<div class='col-sm-12'> <textarea id='editor_id' style='width:100%;height:150px;' disabled='disabled'>"+ $cit[i].content+"</textarea></div> </div>"
                        }
                    }
                    $modal.find(".modal-title").html($top.topTitle + "的任务分配");

                    if(str == ""){
                        str = "课题已经分配完.";
                    }

                }else{
                    str = "查询数据错误";
                }
                $modal.find(".modal-body .form-group").html(str);
                $modal.find("input[name='stuId']").val($stuId.val());

            }
        });
    });
</script>
</body>
</html>
