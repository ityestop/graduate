package cn.edu.zut.soft.graduate.core.model;

import java.util.Date;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
public interface BaseModel {

    //主键
    Integer getId();

    void setId(Integer id);

    //是否删除 默认为0 删除为 -1
    Integer getDel();

    void setDel(Integer del);

    //创建者
    String getCreateAuthor();

    void setCreateAuthor(String createAuthor);

    //修改者
    String getUpdateAuthor();

    void setUpdateAuthor(String updateAuthor);

    //创建时间
    Date getCreateTime();

    void setCreateTime(Date date);

    //最后修改时间
    Date getUpdateTime();

    void setUpdateTime(Date updateTime);
}
