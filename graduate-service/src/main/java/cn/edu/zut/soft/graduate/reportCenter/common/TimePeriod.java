package cn.edu.zut.soft.graduate.reportCenter.common;

/**
 * @author cc
 * @Date 2017/2/13
 * @value 1.0
 */
public enum TimePeriod {
    /**
     * 周
     */
    WEEK(1),
    /**
     * 月
     */
    MONTH(2);

    private Integer value;

    TimePeriod(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

}
