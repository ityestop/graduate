package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
@Data
public class ReportCount {

    /**
     * 提交次数
     */
    private int subCount;

    /**
     * 补交次数
     */
    private int twoCount;

    /**
     * 缺失次数
     */
    private int errCount;

}
