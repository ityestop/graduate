<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>成绩信息</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">成绩管理</li>
        </ol>
        <div class="page-header">
            <h4>成绩信息列表</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <%--<input type="hidden" value="${sessionScope.Groupgroup}" id="groupInfo_ID"/>--%>
            <table id="results" class="table table-container">
            </table>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>


<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/util/infoListTransform.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">
    var columns = [
        {
            filed:'type',
            title: '类型'
        },{
            filed:'key',
            title: '阶段'
        },{
            filed:'value',
            title: '结果'
        }
    ];
    customSearch({
        id: "#results",
        url: "/grade/findAllGrade",
        dataType: "json",
        params: {},
        columns: columns
    });



</script>
</body>
</html>