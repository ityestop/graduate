<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>专家分配任务</title>
    <script src="/assets/g/js/adminAuditAllIssue.js"></script>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./admin"> 首页 </a></li>
            <li class="active">题目管理</li>
        </ol>
        <div class="page-header">
            <h4>题目管理</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="panel-body">
        <div class="form-group">
<%--
            <div class="col-sm-2">
                <button class="btn btn-blue btn-block" id="tea_AuditIssue">校内题目</button>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-blue btn-block" id="stu_AuditIssue">公司题目</button>
            </div>
--%>

        </div>
        <div class="form-group">
            <table class="table table-hover"
                   style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                <thead>
                <tr>
                    <th width="35%" class="center">
                        <small>题目名称</small>
                    </th>
                    <th>
                        <small>题目来源</small>
                    </th>
                    <th>
                        <small>指导教师</small>
                    </th>
                    <th>
                        <small>题目状态</small>
                    </th>
                    <th>
                        <small>题目人数</small>
                    </th>

                    <th>
                        <small>分配专家</small>
                    </th>

                </tr>
                </thead>
                <tbody id="auditIssuseTop">
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="teaList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">专家列表</h4>
            </div>
            <div class="modal-body">
                <table class="table  table-hover">
                    <thead>
                    <tr>
                        <th><small>姓名</small></th>
                        <th><small>分配题目数</small></th>
                        <th><small>分配子标题数</small></th>
                        <th><small>分配</small></th>
                    </tr>
                    </thead>
                    <tbody>

                    <c:forEach items="${witness}" var="wl" >
                        <tr>
                            <td><small>${wl.name}</small></td>
                            <td><small>${wl.allNum}</small></td>
                            <td><small>${wl.smallAllNum}</small></td>
                            <td><button class='btn btn-primary btn-sm' onclick="distr('${wl.id}', '${wl.name}')">分配</button>

                                <%--<a href="./admin/toAuditIssueList?userId=${wl.id}&userName=${wl.name}">分配 </a>--%>
                                <%-- /admin/edit_aduit?userId=1&issueId=239&userName=åå¤å --%>
                                <%--<a href="./admin/issue_userId?userId=${wl.id}"> 删除 </a>--%>
                            </td>

                        </tr>


                        <%--"/admin/delete_audit?userId="+userId_audit+"&issueId="+issue.id  删除连接--%>
                        <%--"/admin/edit_aduit?userId="+userId_audit+"&issueId="+issue.id+"&userName="+userName_audit  分配连接--%>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


</body>
</html>
