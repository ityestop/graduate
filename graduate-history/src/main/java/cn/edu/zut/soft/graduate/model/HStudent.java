package cn.edu.zut.soft.graduate.model;

/**
 * <p>文件名称：HStudent.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/23 下午3:53</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public class HStudent {

    private Integer id;

    private String student;

    private String info;

    private String result;

    private String years;

    public HStudent() {
    }

    public HStudent(Integer id, String student, String info, String result, String years) {
        this.id = id;
        this.student = student;
        this.info = info;
        this.result = result;
        this.years = years;
    }

    public Integer getId() {
        return id;
    }

    public String getStudent() {
        return student;
    }

    public String getInfo() {
        return info;
    }

    public String getResult() {
        return result;
    }

    public String getYears() {
        return years;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setYears(String years) {
        this.years = years;
    }
}
