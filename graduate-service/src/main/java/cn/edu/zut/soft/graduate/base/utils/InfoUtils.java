package cn.edu.zut.soft.graduate.base.utils;

import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;

import java.util.List;

/**
 * @author cc
 * @Date 2017/1/5
 * @value 1.0
 */
public class InfoUtils {
    public static String getInfoValue(List<Info> list,Info i){
        for (Info info : list){
            if (InfoType.TOPIC.name().equals(info.getType()) && IKEY.TopicId.equals(info.getKey())){
                return info.getValue();
            }
        }
        return null;
    }
}
