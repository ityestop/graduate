package cn.edu.zut.soft.graduate.topicCenter.op;

import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.handler.IssueStatusChangeHandler;

import java.util.List;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */
public interface IssueOP {

    List<IssueStatusChangeHandler> getStatusChangeHandlerList();

    Issue changeIssuePhase(LoginVO loginVO, Integer id, IssuePhase.Handle handle);



}
