package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/24.
 */
@Data
public class GroupVO {
    private Integer id;
    private String name;
}
