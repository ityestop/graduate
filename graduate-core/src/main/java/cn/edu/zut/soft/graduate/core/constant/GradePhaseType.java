package cn.edu.zut.soft.graduate.core.constant;

/**
 * <p>文件名称：GradePhaseType.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/4/19 下午6:04</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public enum GradePhaseType {

    OPEN_GRADE("openGrade", "开题成绩"),
    INTERIM_GRADE("interimGrade", "中期成绩"),
    SYSTEM_CHECK_GRADE("systemCheckGrade", "系统验收成绩"),
    GYIDANCE_GRADE("guidanceGrade" , "指导成绩"),
    REPORT_GRADE("reportGrade", "报告成绩"),
    REPLY_GRADE("replyGrade","答辩成绩"),
    FINAL_REPLY_GREADE("finalReplyGrade", "最终答辩成绩");

    private String gradeKey;
    private String gradeName;

    GradePhaseType(String gradeKey, String gradeName) {
        this.gradeKey = gradeKey;
        this.gradeName = gradeName;
    }

    public String getGradeKey() {
        return gradeKey;
    }

    public void setGradeKey(String gradeKey) {
        this.gradeKey = gradeKey;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}
