<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/12/10
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>毕业设计管理平台</title>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a>
            </li>
            <li class="active">最终成绩</li>
        </ol>
        <div class="page-header">
            <h3>最终成绩</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- start: TABLE WITH IMAGES PANEL -->
        <div class="panel-body">
                <div>
                    <label class="col-sm-10"></label>
                    <div class="col-sm-2">
                        <a class="btn btn-blue btn-block" href="./teacher/downloadTeaStuResult.do">
                            学生最终成绩导出</a>
                    </div>
                </div>
                <table class="table table-hover"
                       style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                    <thead>
                    <tr>
                        <th width="5%"><small>编号</small></th>
                        <th width="12%">
                            <small>学号</small>
                        </th>
                        <th width="8%">
                            <small>学生姓名</small>
                        </th>
                        <th width="5%">
                            <small>性别</small>
                        </th>
                        <th width="12%">
                            <small>班级</small>
                        </th>
                        <th width="15%">
                            <small>课题</small>
                        </th>
                        <th>
                            <small>任务</small>
                        </th>
                         <th>
                            <small>指导成绩</small>
                        </th>
                         <th>
                            <small>评阅成绩</small>
                        </th>
                         <th>
                            <small>答辩成绩</small>
                        </th>
                        <th>
                            <small>最终成绩</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${list}" var="s" varStatus="i">
                        <tr>
                            <td><small>${i.index + 1}</small></td>
                            <td>
                                <small>${s.stuStudentId}</small>
                            </td>
                            <td>
                                <small>${s.stuName}</small>
                            </td>
                            <td>
                                <small>${s.stuSex ? "女" : "男"}</small>
                            </td>
                            <td>
                                <small>${s.stuClass}</small>
                            </td>
                            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.topicTitle}">
                                <small>${s.topicTitle}</small>
                            </td>
                            <td id="childTop" style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                            title="${s.childTopicTitle}">
                                <small>${s.childTopicTitle}</small>
                            </td>
                             <td>
                                <small>
                                <fmt:formatNumber value="${s.donGrade}" pattern="0" />
                                </small>
                            </td>
                             <td>
                                <small>
                                <fmt:formatNumber value="${s.documentGrade}" pattern="0" />
                                </small>
                            </td>
                             <td>
                                <small>
                                <fmt:formatNumber value="${s.replyGrade}" pattern="0" />
                                </small>
                            </td>
                             <td>
                                <small>
                                <fmt:formatNumber value="${s.finalReplyGrade}" pattern="0" />
                                </small>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
               </div>
            </div>
    </div>
</div>
</body>
</html>
