package cn.edu.zut.soft.graduate.topicCenter.handler;

import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

import static cn.edu.zut.soft.graduate.core.constant.IssuePhase.Handle.UpAudit;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */

/**
 * 提交课题时检测
 */
@Component
public class ChangeToAudit extends ChangeToAuditOK {

    private List<IssuePhase.Handle> handleList = Collections.singletonList(UpAudit);

    @Override
    protected boolean conform(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        return handleList.contains(handle);
    }

    @Override
    protected void conformChange(LoginVO loginVO, Issue issue, IssuePhase.Handle handle) {
        if (issue.getSource() == IssueSource.school) {
            Assert.isTrue(loginVO.getId().equals(issue.getIdentityId()), "非本人操作");
        }else {
            Assert.isTrue(loginVO.getId().equals(issue.getSchoolTeaId()),"非课题指导教师无法提交");
        }
        //判断是否失效,失效时同时逻辑删除
        issue.addStatus(handle.getDst());
        //优化性能,去除大对象的修改
        issue.setContent(null);
    }
}
