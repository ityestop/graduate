package cn.edu.zut.soft.graduate.core.vo;

import cn.edu.zut.soft.graduate.core.constant.Address;
import cn.edu.zut.soft.graduate.core.constant.BasicStatus;
import cn.edu.zut.soft.graduate.core.constant.ScheduleStatus;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.ReportCount;
import cn.edu.zut.soft.graduate.core.model.view.TeacherGroupVO;
import lombok.Data;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
@Data
public class GraduateStateVO {

    /**
     * 基础信息
     */
    private UserDescVO user;

    private Identity teacher;

    private IssueVO issue;

    private Task task;

    /**
     * 开题阶段
     */
    private ScheduleStatus startStatus;

    /**
     * 开题地点
     */
    private Address startAddress;

    /**
     * 基本状态
     */
    private BasicStatus finallyOpenStatus;

    /**
     * 中期答辩地址 false 远程答辩
     */
    private Boolean didAddress;

    /**
     * 中期结果
     */
    private Integer finallyDidStatus;

    /**
     * 教师组
     */
    private TeacherGroupVO teacherGroup;

    /**
     * 系统验收进度
     */
    private String systemAcceptanceStatus;

    /**
     * 最终系统验收结果
     */
    private BasicStatus finallySystemAcceptanceStatus;

    //报告统计
    private ReportCount reportCount;

    /**
     * 开源 默认不开源
     */
    private Boolean openSource;

    /**
     * 最终答辩进度
     */
    private ScheduleStatus finallyStatus;

    /**
     * 最终结果
     */
    private BasicStatus overStatus;

    private String guideGrade;

    private String reportGrade;

    private String replyGrade;

    private String finalGrade;

}
