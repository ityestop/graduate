package cn.edu.zut.soft.graduate.groupCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.UserGroup;

public interface UserGroupDAO extends BasicDAO<Integer,UserGroup> {
}