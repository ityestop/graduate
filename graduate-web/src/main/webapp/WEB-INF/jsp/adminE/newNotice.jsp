<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 16/10/29
  Time: 22:50
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>课题列表</title>
    <link rel="stylesheet" type="text/css" href="/assets/plugins/wangEditor/dist/css/wangEditor.min.css">
    <script type="text/javascript" src="/assets/plugins/wangEditor/dist/plupload/plupload.full.min.js"></script>
    <script type="text/javascript" src="/assets/plugins/wangEditor/dist/js/wangEditor.min.js"></script>
    <script src="/assets/g/js/newNotice.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">发布公告</li>
        </ol>
        <div class="page-header">
            <h3>发布新公告
                <small>发布新的公告</small>
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group" id="issueIdDiv" style="display: none;">
                    <label class="col-sm-2 control-label">
                        公告编号
                    </label>
                    <div class="col-sm-8" id="id">
                        ${param.id}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        公告名称 </label>
                    <div class="col-sm-8">
                        <input name="title" type="text" placeholder="公告名称" class="form-control"
                               maxlength="25" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        公告对象 </label>
                    <div class="col-sm-8">
                        <label class="checkbox-inline">
                            <input name="roles" type="checkbox" value="tea" title="教师" checked="checked">教师
                        </label>
                        <label class="checkbox-inline">
                            <input name="roles" type="checkbox" value="stu" title="学生" checked="checked">学生
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        公告内容 </label>
                    <div class="col-sm-8">
                        <div id="content" style="height: 500px">
                            <p>请输入内容...</p>
                        </div>
                    </div>
                </div>
                <div class="from-group">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <submit class="btn btn-blue btn-block" id="saveNotice">
                                <i class="clip-checkmark-2"></i> 保存
                            </submit>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="javascript:history.go(-1);"><i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>