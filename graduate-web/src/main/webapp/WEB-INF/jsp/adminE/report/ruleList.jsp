<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 2017/2/19
  Time: 下午4:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>周月报规则管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">周月报规则管理</li>
        </ol>
        <div class="page-header">
            <h3>周月报规则</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="/admin/report/listRule" method="get">
                <%--<div class="form-group">--%>
                    <%--<input class="form-control" style="width: 300px" title="教师姓名" placeholder="教师姓名" name="name"--%>
                           <%--value="${param.name}"/>--%>
                <%--</div>--%>
                <%--<div class="form-group" style="text-align: right;">--%>
                    <%--<button type="submit" class="btn btn-primary">检索</button>--%>
                <%--</div>--%>
                    <div class="form-group">
                        <a class="btn btn-primary" href="/admin/report/editRule">
                            <span><i class=" clip-plus-circle"></i> 添加报告规则</span>
                        </a>
                    </div>
            </form>
        </div>
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="ruleTable" class="table table-container">
            </table>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript">

    var $table = $("#ruleTable");

    function getParams() {
        <%--return {--%>
            <%--identityId: '${param.identityId}'--%>
        <%--}--%>
        return {}
    }

    window.operateEvents = {
        'click .del' : function (e,value,row,index) {
            var url = "/report/rule/delete";
            $.ajax({
                type: 'POST',
                url: url,
                data:{
                    id:row.id
                },
                dataType:'json',
                success:function (result) {
                    if (result.success == true){
                        $table.bootstrapTable('remove',{field:'id',values:[row.id]});
                        alert("删除规则成功,id="+row.id);
                    }else {
                        alert(result.errorMsg);
                    }
                }
            });
        }
    };

    var columns = [{
        field: 'id',
        title: '编号'
    },{
        title: '名称',
        formatter: function (value, row, index) {
            return "第 " + row.num + " " + (row.type == 1 ? "周" : "月") + "报告";
        }
    },{
        field: 'startTime',
        title: '开始时间',
        formatter: function (value, row, index) {
            return timeStamp2StringYYYYMMDD(value);
        }
    },{
        field: 'endTime',
        title: '结束时间',
        formatter: function (value, row, index) {
            return timeStamp2StringYYYYMMDD(value);
        }
    },{
        field: 'createAuthor',
        title: '创建者'
    },{
        title: '操作',
        formatter: function (value, row, index) {
            var str = "<button class='del btn btn-danger'>删除</button>";
            return str;
        },
        events:operateEvents
    }];

    customSearch({
//        id: "#ruleTable",
        $table:$table,
        url: "/report/rule/list",
        dataType: "json",
        params: getParams(),
        columns: columns,
        pageSize: 25
    });



</script>
</body>
</html>
