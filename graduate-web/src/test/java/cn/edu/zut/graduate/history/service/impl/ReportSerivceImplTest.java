//package cn.edu.zut.graduate.service.impl;
//
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.graduate.entity.ChildTopic;
//import cn.edu.zut.graduate.entity.Report;
//import cn.edu.zut.graduate.entity.Student;
//import cn.edu.zut.graduate.entity.Topic;
//import cn.edu.zut.graduate.entity.tools.ReportTeaSelect;
//import cn.edu.zut.graduate.service.EmpService;
//import cn.edu.zut.graduate.supervisor.DaoFit;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.graduate.util.DataLS;
//
//import org.apache.ibatis.session.RowBounds;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.Assert.*;
//
///**
// * Created by cc on 16/3/31.
// */
//public class ReportSerivceImplTest {
//
//
//    private static DaoFit dao;
//    private static ServiceFit service;
//    private EmpService empService;
//
//    @Before
//    public void setUp() throws Exception {
//        ApplicationContext context = new ClassPathXmlApplicationContext(
//                new String[]{"classpath:conf/Spring-mvc.xml", "classpath:conf/Spring-sql.xml"});
//        dao = (DaoFit) context.getBean("daoFit");
//        service = (ServiceFit) context.getBean("serviceFit");
//        empService=(EmpService)context.getBean("empService");
//        
//    }
//    @Test
//    public void testFindListTea() throws Exception {
//        ReportTeaSelect reportTeaSelect = new ReportTeaSelect();
//        reportTeaSelect.setTeaId("8DE3C48DF49A07EDCBE6D28AE8F9A807");
////        reportTeaSelect.setStuName("吴山岗");
////        reportTeaSelect.setNum(3);
////        reportTeaSelect.setTopic("");
////        reportTeaSelect.setTyp(2);
//		List<Report> list=(List<Report>)service.getReportSerivce().findListTea(reportTeaSelect,new RowBounds()).getResult();
//		for(Report lis:list){
//			System.err.println(lis.toString());
//
//		}
//		System.out.println(list.size());
//    }
//    @Test
//    public void testDownlod(){
////    	int value= Integer.valueOf(DataLS.functionMap.get("InterimScampling").getContent());
////		List list= (List) empService.extractValue(value).getResult();
////		System.out.println(list);
//    	List list1=dao.getStudentDao().downloadTopic();
//    	System.out.println(list1);
//    }
//
//    @Test
//    public void testSelectByteaId(){
//    	ResultDo resultDo=service.getEmpService().selectStudentByTeaId("8A9D8E2BF37E5C2B1DF3E91CB7D8E644");
//    	List<Student> list=(List<Student>)resultDo.getResult();
//    	for(Student ls:list){
//    		System.out.println(ls);
//    	}
//    	System.out.println(list.size());
//    }
//
//    @Test
//    public void shujudingzheng(){
//    	Student student;
//    	Topic topci;
//    	ChildTopic childTopic;
//     	ReportTeaSelect reportTeaSelect = new ReportTeaSelect();
//    	reportTeaSelect.setNum(2);
//    	List<Report> list=dao.getReportDao().findByStudent("BB14346B8DBC727FE4E0BED77CA16809");
////    	List<Report> list=(List<Report>)service.getReportSerivce().findListTea(reportTeaSelect,new RowBounds()).getResult();
//    	System.out.println(list);
//    	for(Report lis:list){
//    		student=dao.getStudentDao().selectByKey(lis.getStuId());
//    		topci=dao.getTopicDao().selectByKey(student.getTopId());
//    		childTopic=dao.getChildTopicDao().selectByKey(student.getChildId());
//    		if(lis.getType() == 1){
//    			if(childTopic == null){
//    				continue;
//    			}else{
//    			if(childTopic.getName() == null || childTopic.getName() == "" ||childTopic.getName() == "null"  ){
//    				lis.setTitle(topci.getTopTitle()+"-"+"第"+lis.getNumber()+"周");
//    				dao.getReportDao().updateByKeySelective(lis);
//    				System.out.println(student.getStuName()+"已修改");
//    			}else{
//    				lis.setTitle(topci.getTopTitle()+"-"+childTopic.getName()+"-"+"第"+lis.getNumber()+"周");
//    				dao.getReportDao().updateByKeySelective(lis);
//    				System.out.println(student.getStuName()+"已修改");
//    			}
//    			}
//    		}else if(lis.getType() == 2){
//    			if(childTopic == null){
//    				continue;
//    			}else{
//    			if(childTopic.getName() == null || childTopic.getName() == "" ||childTopic.getName() == "null"  ){
//    				lis.setTitle(topci.getTopTitle()+"-"+"第"+lis.getNumber()+"月");
//    				dao.getReportDao().updateByKeySelective(lis);
//    				System.out.println(student.getStuName()+"已修改");
//    			}else{
//    				lis.setTitle(topci.getTopTitle()+"-"+childTopic.getName()+"-"+"第"+lis.getNumber()+"月");
//    				dao.getReportDao().updateByKeySelective(lis);
//    				System.out.println(student.getStuName()+"已修改");
//    			}
//    			}
//    		}
//
//    	}
//    	System.out.println(list.size());
//    }
//
//    @Test
//    public void testChoucha(){
//    	int number = 35;
////    	ResultDo resultDo=service.getEmpService().extractByTeaOneAndNum(number);
//    	ResultDo resultDo=service.getEmpService().extractValue(4);
//    	List<Student> list=(List<Student>)resultDo.getResult();
//    	for(Student lis:list){
//    		System.out.println(lis.getStuStudentId()+"---"+lis.getStuName());
//    	}
//    }
//
//    @Test
//    public void test1(){
//        ResultDo<List<String[]>> resultDo = service.getReportSerivce().searchDetails(Role.TEA,"8DE3C48DF49A07EDCBE6D28AE8F9A807",new RowBounds());
//        List<String[]> list = resultDo.getResult();
//        for(String[] s : list){
//            System.out.println(s[0]+"**"+s[1]+"__"+s[2]);
//        }
//
//    }
//    @Test
//    public void test2(){
//    	ResultDo res=service.getEmpService().updateStudentcheckgrade("8EC4DD0A18FAB8E01955202A80F07BED", true);
//    	System.out.println(res.isSuccess());
//    }
//}