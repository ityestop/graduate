package cn.edu.zut.soft.graduate.controller;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.websocket.UserMonitoringHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author cc
 * @Date 2016/11/15
 * @value 1.0
 */
public class BaseController {

    @Autowired
    protected UserMonitoringHandler userMonitoringHandler;

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler
    @ResponseBody
    public CommonResult exp(HttpServletRequest request, HttpServletResponse response, Exception ex) {
//        request.setAttribute("ex", ex);
        if (ex instanceof IllegalAccessException){
            logger.warn(ex.getMessage());
        }else {
            logger.error(ex.getMessage(), ex);
        }
        return CommonResult.errorReturn(ex.getMessage());
    }
}
