package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.InviteStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/29
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InviteQuery extends Query {

    private String activesName;

    private Integer activesId;

    private String passivesName;

    private Integer passivesId;

    private Integer groupId;

    private InviteStatus status;

    private Integer del;

    private List<Integer> ids;

    private List<InviteStatus> inviteStatusList;
}
