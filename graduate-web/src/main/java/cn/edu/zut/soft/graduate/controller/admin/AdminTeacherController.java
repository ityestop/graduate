package cn.edu.zut.soft.graduate.controller.admin;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.view.TeacherGroupVO;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.groupCenter.bo.GroupBO;
import cn.edu.zut.soft.graduate.groupCenter.bo.TeacherGroupBO;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>文件名称：AdminTeacherController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/26 上午1:23</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RequestMapping("/admin")
@Controller
public class AdminTeacherController {

    @Resource
    private InfoBO infoBO;

    @Resource
    private IdentityBO identityBO;

    @Resource
    private TeacherGroupBO teacherGroupBO;

    /**
     * 功能描述:跳转教师任务列表
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/teaWorking")
    public String TeacherWorking(HttpServletRequest request, Model model){
        return "adminE/teaWorking";
    }

    /**
     * 功能描述:根据教师ID查询指导学生
     * @param infoQuery
     * @param teaId
     * @return
     */
    @ResponseBody
    @RequestMapping("/stuMemberByteaId")
    public CommonResult getStuMemberByteaId(InfoQuery infoQuery, @RequestParam("teaId") Integer teaId){
        if (teaId == null){
            return CommonResult.errorReturn("System error");
        }
        infoQuery.setInfoType(InfoType.Tea);
        infoQuery.setKey(IKEY.Guide);
        infoQuery.setValue(teaId.toString());
        infoQuery.setDel(0);
        List<Integer> stuIds = new ArrayList<>();
        CommonResult<List<Info>> result = infoBO.findByQuery(infoQuery);
        for (Info info:result.getData()){
            stuIds.add(info.getIdentityId());
        }
        IdentityQuery identityQuery = new IdentityQuery();
        identityQuery.setIds(stuIds);
        identityQuery.setDel(0);
        List<UserDescVO> userDescVOList = identityBO.userPersonal(identityQuery,IKEY.StuInfoAll);
        return CommonResult.successReturn(userDescVOList,result.getTotalCount());
    }

    /**
     * 功能描述:跳转分配答辩小组
     * @return
     */
    @RequestMapping("/teaGroup")
    public String teaGroup(){
        return "/adminE/teaGroupAll";
    }

    /**
     * 功能描述:获取所有教师小组
     * @return
     */
    @ResponseBody
    @RequestMapping("/getAllTeaGroup")
    public CommonResult getAllTeaGroup(GroupQuery groupQuery){
        groupQuery.setRole(Role.tea);
        groupQuery.setDel(0);
        groupQuery.setPageSize(100);
        List<TeacherGroupVO> result = teacherGroupBO.findByQuery(groupQuery);
        return CommonResult.successReturn(result);
    }

    @ResponseBody
    @RequestMapping("/noParallelism")
    public CommonResult noParallelism(GroupQuery groupQuery){
        List<TeacherGroupVO> result = teacherGroupBO.noParallelism();
        return CommonResult.successReturn(result);
    }

    /**
     * 对接接口
     * @param groupId
     * @param parallelismId
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/parallelism")
    public CommonResult parallelism(Integer groupId, Integer parallelismId, HttpSession session){
        UserDescVO userDescVO = (UserDescVO) session.getAttribute(IKEY.USER);
        boolean flag = teacherGroupBO.parallelism(groupId, parallelismId, userDescVO.getIdentity().getName());
        return CommonResult.successReturn(flag);
    }

    /**
     * 重置对接接口
     * @param groupId
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping("/restartParallelism")
    public CommonResult restartParallelism(Integer groupId, HttpSession session){
        UserDescVO userDescVO = (UserDescVO) session.getAttribute(IKEY.USER);
        boolean flag = teacherGroupBO.restartParallelism(groupId, userDescVO.getIdentity().getName());
        return CommonResult.successReturn(flag);
    }



    /**
     * 功能描述:答辩小组详情
     * @return
     */
    @RequestMapping("/toGroupInfo")
    public ModelAndView getNoGroupTea(Map<String,Object> model, @RequestParam("groupId") Integer groupId){
        if (groupId == null){
            return new ModelAndView("redirect:/gouop/teaGroup");
        }
        model.put("groupId", groupId);
        return new ModelAndView("/adminE/teaGroupInfo", model);
    }

    @ResponseBody
    @RequestMapping("/assignGroupSEC")
    public CommonResult assignGroupSEC(HttpServletRequest request,@RequestParam("groupId")Integer groupId, @RequestParam("id") Integer id){
        LoginVO loginVO = (LoginVO)request.getSession().getAttribute("user");
        if (loginVO == null){
            return CommonResult.errorReturn("请重新登陆!");
        }
        if(id == null || groupId == null){
            return CommonResult.errorReturn("System error");
        }
        Info infoResult = infoBO.save(new BasicInfoStrategy(InfoType.Group, IKEY.GroupSEC, groupId.toString()),id, loginVO.getName());
        if (infoResult == null){
            return CommonResult.errorReturn("System error");
        }
        return CommonResult.successReturn(infoResult);
    }



}
