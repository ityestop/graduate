package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper = true)
public class Group extends BaseModelImpl {
    private Integer count;//小组人数
    private IssueSource status;//小组状态
    private String stateSets;//小组流转情况
    private Role role;//小组角色
    private Integer identityId;//主管
    private String identityName;

    /**
     * 对接小组编号
     */
    private Integer parallelismId;

    private List<UserDescVO> userDescVOList;
}
