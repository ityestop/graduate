package cn.edu.zut.soft.graduate.reportCenter.query;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.basic.core.model.dao.Page;
import cn.edu.zut.soft.graduate.reportCenter.common.TimePeriod;

import java.util.List;

/**
 * @author cc
 * @Date 2017/2/13
 * @value 1.0
 */
public interface SearchReportQuery extends Page {
    /**
     * 获取时间周期
     * @return 周或者月或者null
     */
    TimePeriod getTimePeriod();

    /**
     * 获取次
     * @return 1-12或1-18
     */
    List<Integer> getNums();

    /**
     * 报告状态
     * @return yes为交,no为未交,null为检索全部
     */
    Alternative getReportStatus();

    /**
     * 教师的id
     * @return 大于0则检索教师,否则检索所有教师的
     */
    Integer getTeaId();

    /**
     * 教师组的id
     * @return 大于0则检索教师组下面的所有教师,否则检索所有教师
     */
    Integer getTeaGroupId();

    List<Integer> getRuleIds();


}
