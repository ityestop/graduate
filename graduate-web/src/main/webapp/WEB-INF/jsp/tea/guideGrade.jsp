<%--suppress JSDuplicatedDeclaration --%>
<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>指导成绩</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table {
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">指导成绩</li>
        </ol>
        <div class="page-header">
            <h3>指导成绩
            </h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <label class="col-sm-10"></label>
            <div class="col-sm-2">
                <a class="btn btn-primary" href='/file/downloadGuideStu?teaId=${sessionScope.user.id}'><i class="	glyphicon glyphicon-download-alt"></i>  导出指导成绩</a>
            </div>
        </div>
        <div class="col-sm-12">
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="guideGradeModel" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">指导成绩</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="guideStuId">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="stuName">
                        </label>
                        <label class="col-sm-2 control-label" id="stuClass">
                        </label>
                        <label class="col-sm-4 control-label" id="stuTopic">
                        </label>
                        <label class="col-sm-4 control-label" id="stuChildTopic">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            指导成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="学生指导成绩" id="guideGrade">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="guideGrade_submit">提交
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="restGuideGradeMolde" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">指导成绩</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restGuideStuId">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" id="restStuName">
                        </label>
                        <label class="col-sm-2 control-label" id="restStuClass">
                        </label>
                        <label class="col-sm-4 control-label" id="restStuTopic">
                        </label>
                        <label class="col-sm-4 control-label" id="restStuChildTopic">
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            指导成绩:
                        </label>
                        <div class="col-sm-3">
                            <input class="form-control" placeholder="学生指导成绩" id="restGuideGrade">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restGuideGrade_submit">提交
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>


<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->


<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">
    var $id = "";
    function distributeChTopic(cId, cIdentityId) {
        $id = cId;
        $identityId = cIdentityId;
        $("#topId").attr("value", $id);
        $("#stuId").attr("value", $identityId);
        distributeChTopi();
    }
    function distributeChTopi() {
        if ($id != null) {
            $.getJSON("/issue/" + $id, function (result) {
                if (result.success) {
                    succ(result, $identityId);
                } else {
                    err(result);
                }
            });
        }
    }
    function params() {
        return {
            value: ${user.id},
            infoType: 'Tea',
            key: 'guide'
        }
    }

    var columns = [
                {
                    field: 'identity.id',
                    title: '编号'
                },
                {
                    field: 'identity.noId',
                    title: '学号'
                }, {
                    field: 'identity.name',
                    title: '姓名'
                }, {
                    title: '班级',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                                str += array[i].value;
                                break;
                            }
                        }
                        return str;
                    }
                }, {
                    title: '课题',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'TOPIC' && array[i].key == 'topicName') {
                                str += array[i].value;
                                break;
                            }
                        }
                        return str;
                    },
                    sortStable: true
                }, {
                    title: '子任务',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                                str += array[i].value;
                                break;
                            }
                        }
                        return str;
                    }
                }, {
                    title: '指导成绩',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "未操作";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'GRADE' && array[i].key == 'guideGrade') {
                                str = array[i].value;
                                break;
                            }
                        }
                        return str;
                    }
                }, {
                    title: '评阅成绩',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "未操作";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'GRADE' && array[i].key == 'reportGrade') {
                                str = array[i].value;
                                break;
                            }
                        }
                        return str;
                    }
                }, {
                    title: '答辩成绩',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "未操作";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'GRADE' && array[i].key == 'replyGrade') {
                                str = array[i].value;
                                break;
                            }
                        }
                        return str;
                    }
                }, {
                    title: '最终成绩',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "未操作";
                        for (var i = 0; i < array.length; i++) {
                            if (array[i].type == 'GRADE' && array[i].key == 'finalReplyGrade') {
                                str = array[i].value;
                                break;
                            }
                        }
                        return str;
                    }
                }, {
                    title: '操作',
                    formatter: function (value, row, index) {
                        var array = row.infoList;
                        var str = "";
                        var $roleStu = true;
                        for (var i = 0; i < array.length; i++) {
                            if(array[i].type == 'GRADE' && array[i].key == 'finalReplyGrade'){
                                str = '已结束';
                                $roleStu = false;
                                break;
                            }
                        }
                        if($roleStu){
                            for (var i = 0; i < array.length; i++) {
                                if (array[i].type == 'GRADE' && array[i].key == 'guideGrade') {
                                    str = "<a  data-toggle='modal' data-target='#restGuideGradeMolde'>重置</a>";
                                    break;
                                } else {
                                    str = "<a  data-toggle='modal' data-target='#guideGradeModel'>指导成绩</a>"

                                }
                            }
                        }
                        return str;
                    }
                }

            ]
            ;
    customSearch({
        id: "#table",
        url: "/identity/info/select",
        dataType: "json",
        params: params(),
        columns: columns,
        pageSize: 30
    });

    $("#guideGradeModel").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(4).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        $("#guideStuId").attr("value", $stuId);
        $("#stuName").html('学生姓名:' + $stuName);
        $("#stuClass").html('班级:' + $stuClass);
        $("#stuTopic").html('题目:'+ $stuTopic);
        $("#stuChildTopic").html('子标题:'+ $stuChildTopic);
    });
    $("#guideGrade_submit").click(function () {
        var $stuId = $("#guideStuId").val();
        var $guideGrade = $("#guideGrade").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/saveGuideGrade',
            data: {
                stuId: $stuId,
                grade: $guideGrade
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                    window.location.reload();
                }
            }
        });


    });

    $("#restGuideGradeMolde").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        var $stuName = $.trim($(e.relatedTarget).parent().parent().children("td").get(2).innerHTML);
        var $stuClass = $.trim($(e.relatedTarget).parent().parent().children("td").get(3).innerHTML);
        var $stuTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(4).innerHTML);
        var $stuChildTopic = $.trim($(e.relatedTarget).parent().parent().children("td").get(5).innerHTML);
        $("#restGuideStuId").attr("value", $stuId);
        $("#restStuName").html('学生姓名:' + $stuName);
        $("#restStuClass").html('班级:' + $stuClass);
        $("#restStuTopic").html('题目:'+ $stuTopic);
        $("#restStuChildTopic").html('子标题:'+ $stuChildTopic);
    });
    $("#restGuideGrade_submit").click(function () {
        var $reststuId = $("#restGuideStuId").val();
        var $restguideGrade = $("#restGuideGrade").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/restGuideGrade',
            data: {
                stuId: $reststuId,
                grade: $restguideGrade
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    if(result.errorMsg == undefined || result.errorMsg == '' || result.errorMsg == null || result.errorMsg == "" ){
                        window.location.reload();
                    }else{
                        alert(result.errorMsg);
                        window.location.reload();
                    }
                }
            }
        });
    });

</script>
</body>
</html>
