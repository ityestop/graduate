<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教师基本信息管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">答辩小组详细信息</li>
        </ol>
        <div class="page-header">
            <h3>答辩小组详细信息</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <input type="hidden" value="${groupId}" id="groupInfo_ID"/>
        <div class="panel-body" id="table_groupInfo">
        </div>
    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>

<!-- Modal -->
<div class="modal fade" id="groupRole" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">答辩小组身份设置</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" onclick="">确定</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript" src="assets/g/js/config.js"></script>
<script type="application/javascript">
    var $groupID = $("#groupInfo_ID").val();
    var table_groupInf = '<table id="teaGroupInfo" class="table table-container"> </table>';
    $("#table_groupInfo").html(table_groupInf);
    var columns = [
        {
            field: 'identity.noId',
            title: '工号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '性别',
            formatter: function (value, row, index) {
                var str = '';
                if (row.identity.sex == -1) {
                    str = '女';
                } else if (row.identity.sex == 1) {
                    str = '男'
                }
                return str;
            }
        }, {
            field: 'identity.phone',
            title: '联系方式'
        }, {
            title: '小组身份',
            formatter: function (value, row, index) {
                var str = '';
                $.each(row.infoList, function (key, value) {
                    if (value.key == 'groupLeader') {
                        str = '组长';
                        return false;
                    } else if (value.key == 'groupSEC') {
                        str = '秘书';
                        return false;
                    } else {
                        str = '普通成员';
                    }
                });
                return str;
            }

        }, {
            title: "操作",
            formatter: function (value, row, index) {
                var str = '';
                var $devare = "  <button class='btn btn-primary btn-sm' onclick='kickOutMember(" + row.identity.id + ")'>踢出小组</button> ";
                $.each(row.infoList, function (key, value) {
                    if (value.key == 'groupLeader') {
                        str = '无';
                        return false;
                    } else if (value.key == 'groupSEC') {
                        str = $devare;
                        return false;
                    } else {
                        str = "<a class='btn btn-primary btn-sm' onclick='assignSEC(" + row.identity.id + ")'>设为秘书</a>";
                        str += $devare;
                    }
                });

                return str;
            }
        }
    ];
    customSearch({
        id: "#teaGroupInfo",
        url: "/group/groupInfo",
        dataType: "json",
        params: {groupId: $groupID},
        columns: columns
    });


    function assignSEC(id) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/admin/assignGroupSEC',
            data: {
                groupId: $groupID,
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }

        });
    }

    function kickOutMember(id) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/group/kickOutGroup',
            data: {
                groupId: $groupID,
                id: id
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                } else {
                    alert(result.errorMsg);
                }
            }

        });
    }

</script>
</body>
</html>