package cn.edu.zut.soft.graduate.reportCenter.service.Impl;

import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.base.bo.BasicBOImpl;
import cn.edu.zut.soft.graduate.base.cache.CachePool;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.annotation.SimpleQueryParameterHandler;
import cn.edu.zut.soft.graduate.reportCenter.dao.ReportRuleDAO;
import cn.edu.zut.soft.graduate.reportCenter.pojo.ReportRule;
import cn.edu.zut.soft.graduate.reportCenter.service.ReportRuleBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
@Service
public class ReportRuleBOImpl extends BasicBOImpl<ReportRule,Query> implements ReportRuleBO {

    @Resource
    private ReportRuleDAO<ReportRule> reportRuleDAO;

    @Override
    protected BasicDAO<Integer, ReportRule> getDAO() {
        return reportRuleDAO;
    }

    @Override
    public ReportRule get(Integer id) {
        ReportRule reportRule = CachePool.reportRuleCacheMap.get(id);
        if (reportRule == null) {
            reportRule = super.get(id);
            CachePool.reportRuleCacheMap.put(id, reportRule);
        }
        return reportRule;
    }

    @Override
    protected List<Criteria> Query(Query reportQuery) {
        Criteria criteria = new Criteria();
        try {
            SimpleQueryParameterHandler.DEFAULT.handler(criteria,reportQuery);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("query handler is error",e);
        }
        return Collections.singletonList(criteria);
    }

    @Override
    public List<ReportRule> findRuleByDate(Date date) {
        Example example = new Example().cleanPage();
        example.cleanPage().createCriteria()
                .andLessThan("start_time",date)
                .andGreaterThan("end_time",date);
        return getDAO().selectByExample(example);
    }
}
