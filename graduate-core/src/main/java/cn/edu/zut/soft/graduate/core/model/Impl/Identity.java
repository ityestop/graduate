package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.Role;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author cc
 * @Date 2016/10/20
 * @value 1.0
 */
@Data
@ToString(callSuper=true, includeFieldNames=true)
@EqualsAndHashCode(callSuper = true)
public class Identity extends BaseModelImpl {

    private String noId;//编码唯一（登录方式）
    private String name;//名称
    private Integer sex;//性别
    private transient String pwd;//密码
    private String phone;//手机（登录方式）
    private String email;//邮箱（登陆方式）
    private Role role;//用户类型

    @JSONField(serialize = false)
    public String getPwd(){
        return pwd;
    }

}
