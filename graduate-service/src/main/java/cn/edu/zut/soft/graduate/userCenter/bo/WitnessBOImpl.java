package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.design.WitnessStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.AuditIssueLinkQuery;
import cn.edu.zut.soft.graduate.core.vo.Witness;
import cn.edu.zut.soft.graduate.core.vo.WitnessData;
import cn.edu.zut.soft.graduate.topicCenter.bo.AuditIssueLinkBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by chuchuang on 16/10/23.
 */
@Repository
public class WitnessBOImpl implements WitnessBO {

    @Resource
    private IdentityBO identityBO;

    @Resource
    private InfoBO infoBO;

    @Resource
    private IssueBO issueBO;

    @Resource
    private AuditIssueLinkBO auditIssueLinkBO;

    @Override
    public CommonResult<WitnessData> getWitnessData(final Integer id) {
        CommonResult<List<AuditIssueLink>> linkResult = auditIssueLinkBO.findByQuery(new AuditIssueLinkQuery() {
            {
                setIdentityId(id);
                setPageSize(Integer.MAX_VALUE);
            }
        });
        if (!linkResult.isSuccess()) {
            return CommonResult.errorReturn(linkResult.getErrorMsg());
        }
        Identity identity = identityBO.get(id);
//        Assert.notNull(identity,"id is error");
        WitnessData witnessData = new WitnessData();
        witnessData.setId(id);
        witnessData.setName(identity.getName());
        witnessData.setPhone(identity.getPhone());
        //设定总数
        witnessData.setAllNum(linkResult.getTotalCount());

        Set<Integer> noOkSet = new HashSet<>();
        List<Integer> ids = new ArrayList<>();
        int noOkNum = 0;
        for (AuditIssueLink ail : linkResult.getData()) {
            ids.add(ail.getIssueId());
            if (ail.getDel() == -1) {
                noOkNum += ail.getDel();
                noOkSet.add(ail.getIssueId());
            }
        }
        //设定通过数
        witnessData.setNoOkNum(noOkNum);

        if (ids.isEmpty()) {
            return CommonResult.successReturn(witnessData);
        }
        CommonResult<List<Issue>> issueListResult = issueBO.queryByIds(ids);
        if (!issueListResult.isSuccess()) {
            return CommonResult.errorReturn(issueListResult.getErrorMsg());
        }
        //设定总课题数
//        witnessData.setSmallAllNum(issueListResult.getTotalCount());

        //拒绝的子标题数
        int smallNoOkNum = 0;
        //所有子标题数
        int smallAllNum = 0;
        for (Issue issue : issueListResult.getData()) {
            if (noOkSet.contains(issue.getId())) {
                smallNoOkNum += issue.getTaskSize();
            }
            smallAllNum += issue.getTaskSize();
        }
        //必须先设定小标题总数
        witnessData.setSmallAllNum(smallAllNum);
        witnessData.setSmallNoOkNum(smallNoOkNum);

        return CommonResult.successReturn(witnessData);
    }

    @Override
    public CommonResult<List<WitnessData>> getAllWitnessData() {
        CommonResult<List<Witness>> witnessListResult = infoBO.getAllWitness(new WitnessStrategy());
        if (!witnessListResult.isSuccess()) {
            return CommonResult.errorReturn(witnessListResult.getErrorMsg());
        }
        List<WitnessData> witnessDataList = new ArrayList<>();
        CommonResult<WitnessData> dataCommonResult;
        if (CollectionUtils.isEmpty(witnessListResult.getData())) {
            return CommonResult.successReturn(witnessDataList);
        }
        for (Witness witness : witnessListResult.getData()) {
            dataCommonResult = this.getWitnessData(witness.getId());
            if (dataCommonResult.isSuccess()) {
                witnessDataList.add(dataCommonResult.getData());
            }
        }
        return CommonResult.successReturn(witnessDataList);
    }

    @Override
    @Transactional
    public List<Witness> saveWitess(String[] userId, String operator) {
        List<Witness> result = new ArrayList<>();
        for (String s : userId) {
            Integer user = Integer.parseInt(s);
            Witness witness = identityBO.awardWitness(user, new WitnessStrategy(), operator);
            result.add(witness);
        }
        return result;
    }
}
