package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.TopicPhase;
import cn.edu.zut.soft.graduate.core.design.WitnessStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.core.vo.Witness;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chuchuang on 16/10/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/bean-all.xml")
public class IdentityBOImplTest {

    @Resource
    private IdentityBO identityBO;

    @Test
    public void query() throws Exception {

    }

    @Test
    public void identification() throws Exception {
        System.out.println(identityBO.identification("201370034429", "123456"));
    }

    @Test
    public void userPersonal() throws Exception {
        System.out.println(identityBO.userPersonal(1));
    }

    @Test
    public void alterPwd() throws Exception {

    }

    @Test
    public void save() throws Exception {
        Identity identity = new Identity() {
            {
                setPwd("123456");
                setName("楚闯");
                setEmail("754716394@qq.com");
                setNoId("201370034429");
                setPhone("18638637700");
                setRole(Role.stu);
            }
        };
        System.out.println(identityBO.save(identity, "test"));
    }

    @Test
    public void queryByGrade(){
        IdentityQuery identityQuery = new IdentityQuery();
//        identityQuery.clear();
        identityQuery.setRole(Role.stu);
        Print.println(identityBO.queryOrByGrade(TopicPhase.openGrade_one,identityQuery,false).getData(), UserDescVO.class);
    }

    @Test
    public void queryByOutSchool(){
        IdentityQuery identityQuery = new IdentityQuery();
        identityQuery.clear();
        Print.println(identityBO.queryByOutSchool(identityQuery).getData(), UserDescVO.class);
    }

    @Test
    public void awardWitness() throws Exception {
        System.out.println(identityBO.awardWitness(1,new WitnessStrategy(),"test"));
    }
    @Test
    public void notWitnessTea(){
        List<Witness> list = new ArrayList<>();
        CommonResult<List<Integer>> result = identityBO.notWitnessTea(list);
        System.out.println(result.isSuccess());
        for (Integer i:result.getData()){
            System.out.println(i);
        }
    }
}