package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import cn.edu.zut.soft.graduate.core.query.AuditIssueLinkQuery;
import cn.edu.zut.soft.graduate.topicCenter.pojo.IssueTakeWitness;

import java.util.List;

/**
 * Created by chuchuang on 16/10/23.
 */
public interface AuditIssueLinkBO extends BasicBO<AuditIssueLink,AuditIssueLinkQuery> {
    /**
     * 功能描述:获取已经被分配的校内题目ID列表
     * @return
     */
    List<Integer> noExpertTopic();

    /**
     * 功能描述:根据教师ID获取被分配的题目列表
     * @param teaId
     * @return
     */
    List<Integer> getExpertIssue(Integer teaId);

    /**
     *功能描述:根据用户ID和题目ID获取实体
     * @param userId
     * @param issueId
     * @return
     */
    AuditIssueLink getByissueAndUser(Integer userId,Integer issueId);


    /**
     * 功能描述:根据题目ID获取实体
     * @param issueId
     * @return
     */
    AuditIssueLink getAuditIssueLink(Integer issueId);


}
