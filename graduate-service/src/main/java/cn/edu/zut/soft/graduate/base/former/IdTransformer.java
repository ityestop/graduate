package cn.edu.zut.soft.graduate.base.former;

import cn.edu.zut.soft.graduate.core.model.BaseModel;
import org.apache.commons.collections.Transformer;
import org.springframework.stereotype.Component;

/**
 * @author chuchuang
 * @date 16/12/15
 */
@Component
public class IdTransformer implements Transformer {
    @Override
    public Object transform(Object o) {
        return ((BaseModel)o).getId();
    }
}
