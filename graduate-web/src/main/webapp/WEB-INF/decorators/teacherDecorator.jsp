<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/10
  Time: 16:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>

<html>
<head>
    <title>毕业设计管理系统|<sitemesh:write property='title'/>
    </title>
    <!-- start: META -->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1"/><![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <base href="<%=basePath%>">
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet"
          href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/style.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/main-responsive.css">
    <link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
    <link rel="stylesheet"
          href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="assets/css/theme_light.css" type="text/css"
          id="skin_color">
    <link rel="stylesheet" href="assets/css/print.css" type="text/css"
          media="print"/>
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet"
          href="assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="favicon.ico"/>


    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-1.7.2.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->

    <script type="text/javascript" src="assets/js/jquery-1.7.2.min.js"></script>
    <!--<![endif]-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>-->
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script
            src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
    <script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
    <script src="assets/plugins/less/less-1.5.0.min.js"></script>
    <script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
    <script
            src="assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
    <script src="assets/js/main.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="assets/plugins/flot/jquery.flot.js"></script>
    <script src="assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="assets/plugins/flot/jquery.flot.resize.min.js"></script>
    <script src="assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
    <script
            src="assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script
            src="assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script src="assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <script src="assets/js/index.js"></script>
    <script src="assets/g/js//menu/menu.js"></script>

    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
        });
    </script>
    <sitemesh:write property='head'/>
</head>
<body>
<!-- start: HEADER -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse"
                    class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <!-- start: LOGO -->
            <a class="navbar-brand"
               href="/tea"> 软件学院毕业设计管理平台 </a>
            <!-- end: LOGO -->
        </div>
        <div class="navbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">
                <%--<li class="dropdown current-user">--%>
                <%--<a class="dropdown-toggle" href="/Description">--%>
                <%--<i class="clip-youtube "></i> <span class="username">系统使用说明</span>--%>
                <%--</a>--%>
                <%--</li>--%>
                <c:if test="${sessionScope.Basicadmin != null}">
                    <li class="dropdown current-user">
                        <a class="dropdown-toggle" href="/admin">
                            <i class="glyphicon glyphicon-wrench "></i> <span>管理员</span>
                        </a>
                    </li>
                </c:if>
                <!-- start: USER DROPDOWN -->
                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true"
                       href="javascript:void(0)">
                        <i class="clip-user-3 "></i>
                        <span class="username">${sessionScope.user.name}</span>
                        <i class="clip-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/${sessionScope.user.role}/myself">
                                <i class="clip-user-2"></i> &nbsp;个人信息
                            </a>
                        </li>
                        <li>
                            <a href="#pwdModal" data-toggle="modal">
                                <i class="clip-user-2"></i> &nbsp;修改密码
                            </a>
                        </li>
                        <li>
                            <a href="/logout">
                                <i class="clip-calendar"></i>
                                &nbsp;退出
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation navbar-collapse collapse">
            <!-- start: MAIN MENU TOGGLER BUTTON -->
            <div class="navigation-toggler">
                <i class="clip-chevron-left"></i> <i class="clip-chevron-right"></i>
            </div>
            <!-- end: MAIN MENU TOGGLER BUTTON -->
            <!-- start: MAIN NAVIGATION MENU -->
            <ul class="main-navigation-menu">
                <li id="index"><a
                        href="/tea"><i
                        class="clip-home-3"></i> <span class="title">首页</span><span
                        class="selected"></span> </a>
                </li>
                <li id="teaNewTopic"><a
                        href="/tea/declare?op=new"><i
                        class="clip-pencil"></i> <span class="title">题目申报</span><span
                        class="selected"></span> </a>
                </li>
                <li id="teaTopicList"><a href="javascript:void(0)"><i
                        class="clip-picassa 	 "></i> <span class="title">选题维护</span><i
                        class="icon-arrow"></i> <span class="selected"></span> </a>
                    <ul class="sub-menu">
                        <li id="adminTeacherInfo"><a
                                href="/tea/issueList"> <span
                                class="title"> 题目管理 </span> </a>
                        </li>
                        <li id="lookTeaStudent"><a
                                href="/tea/issueDeclare"> <span
                                class="title">选报管理</span> </a>
                        </li>
                        <li id="teacherbystudentgrade">
                            <a href="/tea/history">
                                <span class="title">历史题目</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <c:if test="${sessionScope.user.expert eq true }">
                    <li><a
                            href="/tea/expert"><i
                            class="clip-stackoverflow "></i> <span class="title">专家审批</span><span
                            class="selected"></span> </a>
                    </li>
                </c:if>
                <li id="GradeAll"><a href="javascript:void(0)"><i
                        class="clip-clock-3  	 "></i> <span class="title">我的毕设信息</span><i
                        class="icon-arrow"></i> <span class="selected"></span> </a>
                    <ul class="sub-menu">
                        <li id="empty"><a href="/tea/guide"> <span class="title">我的学生</span> </a></li>

                        <%--<li id="teacherbystudentgrade"><a--%>
                        <%--href="./${sessionScope.pathCode}/TeaByStudentGrade.do?name=open">--%>
                        <%--<span class="title"> 开题成绩 </span> </a>--%>
                        <%--</li>--%>
                        <%--<li id="iterimgrade"><a--%>
                        <%--href="./${sessionScope.pathCode}/toInterimGrade.do"><span--%>
                        <%--class="title">中期进度</span> </a>--%>
                        <%--</li>--%>
                        <%--<!-- <li id="empty"><a--%>
                        <%--href="./${sessionScope.pathCode}/empty1.do"> <span--%>
                        <%--class="title">平时成绩 </span> </a>--%>
                        <%--</li> -->--%>
                        <%--<li id="dongrade"><a--%>
                        <%--href="./${sessionScope.pathCode}/todonGrade.do"> <span--%>
                        <%--class="title">指导成绩 </span> </a>--%>
                        <%--</li>--%>
                        <%--<li id="checkGradeview"><a--%>
                        <%--href="./${sessionScope.pathCode}/checkGradeView.do?tgId=${sessionScope.pathCode}"><span--%>
                        <%--class="title">验收成绩</span> </a></li>--%>
                        <%--<!-- <li id="empty"><a--%>
                        <%--href="./${sessionScope.pathCode}/TeaByStudentGrade.do?name=documentGrade"> <span--%>
                        <%--class="title">评阅成绩 </span> </a>--%>
                        <%--</li> -->--%>

                        <%--<li id="sempty"><a--%>
                        <%--href="./${sessionScope.pathCode}/TeaByStudentGrade.do?name=finalReplyGrade"> <span--%>
                        <%--class="title">答辩成绩 </span> </a>--%>
                        <%--</li>--%>

                    </ul>
                </li>

                <li id="teacherassigngroup"><a href="javascript:void(0)"><i
                        class=" clip-leaf  	 "></i> <span class="title">答辩小组信息</span><i
                        class="icon-arrow"></i> <span class="selected"></span> </a>
                    <ul class="sub-menu">
                        <li id="teachergroupall"><a
                                href="/tea/toGroupInfo">
                            <span class="title"> 小组信息 </span> </a>
                        </li>
                        <li id="teacherassignstudent"><a
                                href="/tea/toStudentByGroup"><span
                                class="title">小组学生</span> </a>
                        </li>
                        <li id="teacherRegroupall"><a
                                href="/tea/toGroupInfo?reverse=true">
                            <span class="title"> 对接答辩小组信息 </span> </a>
                        </li>
                        <li id="teacherRegAssignstudent"><a
                                href="/tea/toStudentByGroup?reverse=true"><span
                                class="title">对接小组学生信息</span> </a>
                        </li>
                    </ul>
                </li>

                <li id="marks">
                    <a href="javascript:void(0)"><i class="glyphicon glyphicon-signal"></i>
                        <span class="title">成绩管理</span><i class="icon-arrow"></i> <span class="selected"></span> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="/tea/topOpenGrade"><span class="title">开题成绩</span> </a>
                        </li>
                        <li>
                            <a href="/tea/interimGrade"><span class="title">中期检查</span> </a>
                        </li>
                        <li>
                            <a href="/tea/systemGrade"><span class="title">系统验收</span> </a>
                        </li>
                        <li>
                            <a href="/tea/guideGrade"><span class="title">指导成绩</span> </a>
                        </li>
                        <li>
                            <a href="/tea/replyGrade"><span class="title"></span>答辩成绩</a>
                        </li>
                    </ul>
                </li>


                <%--<li id="docking"><a href="javascript:void(0)"><i--%>
                <%--class=" 	clip-stack-empty"></i> <span class="title">我的对接小组</span><i--%>
                <%--class="icon-arrow"></i> <span class="selected"></span> </a>--%>
                <%--<ul class="sub-menu">--%>
                <%--<li id="teacherassignstudent"><a--%>
                <%--href="./${sessionScope.pathCode}/teaGroupStudent.do?tgId=${sessionScope.doucking}&number=2"><span--%>
                <%--class="title">对接小组学生</span> </a>--%>
                <%--</li>--%>
                <%--<li id="empty"><a--%>
                <%--href="./${sessionScope.pathCode}/teaGroupStudent.do?tgId=${sessionScope.doucking}&number=3"> <span--%>
                <%--class="title">中期检查 </span> </a>--%>
                <%--</li>--%>
                <%--<!--  <li id="empty"><a--%>
                <%--href="./${sessionScope.pathCode}/empty2.do"> <span--%>
                <%--class="title">平时成绩 </span> </a>--%>
                <%--</li>-->--%>
                <%--<li id="teacherassignstudent"><a--%>
                <%--href="./${sessionScope.pathCode}/teaGroupStudent.do?tgId=${sessionScope.doucking}&number=4"> <span--%>
                <%--class="title">指导成绩</span> </a>--%>
                <%--</li>--%>
                <%--<li id="checkGrade"><a--%>
                <%--href="./${sessionScope.pathCode}/checkGrade.do?tgId=${sessionScope.doucking}&name=default"><span--%>
                <%--class="title">验收成绩</span> </a>--%>
                <%--</li>--%>
                <%--<!-- <li id="empty"><a--%>
                <%--href="./${sessionScope.pathCode}/checkGrade.do?tgId=${sessionScope.doucking}&name=docGrade"> <span--%>
                <%--class="title">评阅成绩 </span> </a>--%>
                <%--</li> -->--%>
                <%--<li id="empty"><a--%>
                <%--href="./${sessionScope.pathCode}/checkGrade.do?tgId=${sessionScope.doucking}&name=finalGrade"> <span--%>
                <%--class="title">答辩成绩 </span> </a>--%>
                <%--</li>--%>
                <%--</ul>--%>
                <%--</li>--%>


                <%--<li id="trecommend"><a href="javascript:void(0)"><i--%>
                <%--class=" glyphicon glyphicon-thumbs-up"></i> <span class="title">公开答辩推荐</span><i--%>
                <%--class="icon-arrow"></i> <span class="selected"></span> </a>--%>
                <%--<ul class="sub-menu">--%>
                <%--<li id="TeaRecommend"><a--%>
                <%--href="./${sessionScope.pathCode}/TeaRecommend.do"><span--%>
                <%--class="title">组内推荐</span> </a>--%>
                <%--</li>--%>

                <%--<li id="TeaRecommendother"><a--%>
                <%--href="./${sessionScope.pathCode}/TeaRecommendOther.do?tgId=${sessionScope.doucking}"><i></i>--%>
                <%--<span class="title">组外推荐</span><span class="title"></span> </a>--%>
                <%--</li>--%>
                <%--</ul>--%>
                <%--</li>--%>
                <li id="reportgroup"><a href="javascript:void(0)"><i
                        class=" 	clip-stack-empty"></i> <span class="title">周月报管理</span><i
                        class="icon-arrow"></i> <span class="selected"></span> </a>
                    <ul class="sub-menu">
                        <%--<li id="week_report"><a--%>
                        <%--href="./${sessionScope.pathCode}/teaWeekReportList.do"><i></i>--%>
                        <%--<span class="title">周月报审核</span><span class="title"></span> </a>--%>
                        <%--</li>--%>

                        <li id="teacher_no_report"><a
                                href="/tea/report"><i></i>
                            <span class="title">周月报统计</span><span class="title"></span> </a>
                        </li>
                    </ul>
                </li>


                <%--<li id="teacher_file"><a--%>
                <%--href="./${sessionScope.pathCode}/toAdminUploadFile.do"><i--%>
                <%--class=" 	clip-download"></i> <span class="title">文件模板</span><span--%>
                <%--class="selected"></span> </a>--%>
                <%--</li>--%>
                <li id="teaInfo">
                    <a href="/${sessionScope.user.role}/myself">
                        <i class=" clip-settings"></i>
                        <span class="title">个人信息</span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>
    <!-- start: PAGE -->
    <div class="main-content">
        <div class="container">
            <sitemesh:write property='body'/>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="pwdModal" tabindex="-1" role="dialog" aria-labelledby="pwdModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="pwdModalLabel">密码修改</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="errorHandler alert alert-danger no-display">
                                <i class="fa fa-remove-sign"></i>
                                <span></span>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">旧密码</label>
                                <div class="col-sm-10">
                                    <input type="password" id="oldpwd" title="旧密码" class="form-control"
                                           placeholder="旧密码">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">新密码</label>
                                <div class="col-sm-10">
                                    <input type="password" id="nowpwd" title="新密码" class="form-control"
                                           placeholder="新密码">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">重复密码</label>
                                <div class="col-sm-10">
                                    <input type="password" id="againpwd" title="重复密码" class="form-control"
                                           placeholder="重复密码">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="savepwd">保存</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">2015 &copy; 毕业设计管理平台 by 中原工学院软件学院.</div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i> </span>
    </div>
</div>
<!-- end: FOOTER -->
<script type="application/javascript" src="/assets/g/js/changePwd.js"></script>
<script type="application/javascript">
    $(function () {
        function onWeb() {
            $.ajax({
                url: "/statistics/train",
                dataType: "json",
                type: "get",
                data: {
                    timestamp: new Date().getTime()
                }
            })
        }

        window.setInterval(onWeb, 50000);
    });
</script>
</body>
</html>
