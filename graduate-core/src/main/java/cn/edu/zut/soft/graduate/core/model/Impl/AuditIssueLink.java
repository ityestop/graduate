package cn.edu.zut.soft.graduate.core.model.Impl;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper=true, includeFieldNames=true)
@EqualsAndHashCode(callSuper = true)
public class AuditIssueLink extends BaseModelImpl {

    private Integer identityId;

    private Integer issueId;
}