package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.IssueKind;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IssueQuery extends Query {

    private Integer identityId;

    private String identityName;

    private Role identityRole;

    private Integer schoolTeaId;
    private String schoolTeaName;

    private IssuePhase issuePhase;

    private IssueSource issueSource;

    private IssueKind issueKind;

    private Integer taskSize;//任务数

    private String title;//题目检索

    private List<Integer> ids;//

    private Integer del;

    private List<IssuePhase> issuePhaseList;

    private List<Integer> identityIds;

    private List<Integer> schoolTeaIds;
}
