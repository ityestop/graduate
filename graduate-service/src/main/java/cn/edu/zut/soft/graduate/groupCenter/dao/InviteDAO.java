package cn.edu.zut.soft.graduate.groupCenter.dao;

import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Invite;
import org.springframework.stereotype.Component;

/**
 * @author chuchuang
 * @date 16/11/29
 */
@Component
public interface InviteDAO<T extends Invite> extends BasicDAO<Integer,T> {

}
