package cn.edu.zut.soft.graduate.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by cc on 2017/2/19.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QueryParameter {

    public enum Type{NULL,Equal,unEqual,Like,Regex,Between}

    /**
     * 匹配类型
     */
    public Type equal();

    public String[] rowName();

    public enum BetweenType{NULL,ROW,VALUE}

    public BetweenType Between() default BetweenType.NULL;

    /**
     * 是否是集合 默认不是集合
     */
    public boolean set() default false;

    public enum DefaultValueType{NULL,ZERO,NULL_SET,EMPTY}
    /**
     * 默认值
     */
    public DefaultValueType defaultValue() default DefaultValueType.NULL;

}
