package cn.edu.zut.soft.graduate.topicCenter.pojo;

import cn.edu.zut.soft.basic.core.constant.Alternative;
import cn.edu.zut.soft.graduate.core.model.Impl.Declare;
import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import lombok.Data;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/11/28
 */
//@Data
public class ConfirmInfo {

    private Declare declareOk;//同意

    private List<Declare> refuseList;//拒绝

    private Issue issue;//课题

    private Group group;//小组

    private LoginVO loginVO;//教师

    private Alternative alternative;//操作

    public Declare getDeclareOk() {
        return declareOk;
    }

    public void setDeclareOk(Declare declareOk) {
        this.declareOk = declareOk;
    }

    public List<Declare> getRefuseList() {
        return refuseList;
    }

    public void setRefuseList(List<Declare> refuseList) {
        this.refuseList = refuseList;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public LoginVO getLoginVO() {
        return loginVO;
    }

    public void setLoginVO(LoginVO loginVO) {
        this.loginVO = loginVO;
    }

    public Alternative getAlternative() {
        return alternative;
    }

    public void setAlternative(Alternative alternative) {
        this.alternative = alternative;
    }
}
