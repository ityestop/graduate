package cn.edu.zut.soft.graduate.userCenter.opjo;

import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import lombok.Data;

import java.util.List;

/**
 * @author chuchuang
 * @date 16/12/5
 */
@Data
public class Teacher {
    private Identity identity;
    private List<Info> infoList;

    private int schoolStuNum;//校内人数
    private int companyStuNum;//公司人数
    private int allStuNum;//总人数

    private List<Issue> schoolIssueList;//校内课题
    private List<Issue> companyIssueList;//公司项目

    public void addAllStuNum(){
        this.allStuNum++;
    }
    public void addCompanyStuNum(){
        this.companyStuNum++;
    }
    public void addSchoolStuNum(){
        this.schoolStuNum++;
    }

    public void AllSubtractCompanyNum(){
        this.schoolStuNum = this.allStuNum-this.companyStuNum;
    }

}
