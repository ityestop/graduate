package cn.edu.zut.soft.graduate.base.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author cc
 * @Date 2016/10/19
 * @value 1.0
 */
public interface BasicDAO<Key,T> {
    int countByExample(Example example);

    int deleteByExample(Example example);

    int deleteByPrimaryKey(@Param("id") Key key,@Param("operator") String operator);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByExample(Example example);

    T selectByPrimaryKey(Key key);

    int updateByExampleSelective(@Param("record") T record, @Param("example") Example example);

    int updateByExample(@Param("record") T record, @Param("example") Example example);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

}
