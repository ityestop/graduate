package cn.edu.soft.fastdfs.client;

import cn.edu.soft.fastdfs.data.GroupInfo;
import cn.edu.soft.fastdfs.data.UploadStorage;
import cn.edu.soft.fastdfs.data.Result;
import cn.edu.soft.fastdfs.data.StorageInfo;

import java.io.IOException;
import java.util.List;


public interface TrackerClient {

	public Result<UploadStorage> getUploadStorage() throws IOException;
	public Result<String> getUpdateStorageAddr(String group, String fileName) throws IOException;
	public Result<String> getDownloadStorageAddr(String group, String fileName) throws IOException;
	public Result<List<GroupInfo>> getGroupInfos() throws IOException;
	public Result<List<StorageInfo>> getStorageInfos(String group) throws IOException;
	public void close() throws IOException;
	
}
