package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.GradeType;
import com.alibaba.fastjson.JSON;
import lombok.Data;

/**
 * <p>文件名称：BasicGradeStrategy.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/1/3 下午2:16</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@Data
public class BasicGradeStrategy implements GradeStrategy {

    private GradeType type;
    private String key;
    private String value;

    public BasicGradeStrategy(){

    }
    public BasicGradeStrategy(GradeType gradeType, String key, String value){
        this.type = gradeType;
        this.key = key;
        this.value = value;
    }
    @Override
    public String toJson() {
       return JSON.toJSONString(this);
    }

}
