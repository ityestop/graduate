package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.util.Print;
import cn.edu.zut.soft.graduate.core.constant.LoginMode;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.userCenter.handler.NoGroupHandler;
import cn.edu.zut.soft.graduate.userCenter.opjo.Invite;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import test.BaseTest;

/**
 * @author chuchuang
 * @date 16/11/21
 */
public class StudentBOImplTest extends BaseTest {

    @Autowired
    private StudentBO studentBO;

    @Autowired
    private NoGroupHandler studentNoGroupHandler;
    @Autowired
    private IdentityBO identityBO;

    @Test
    public void findCanInvitationUser() throws Exception {
        LoginVO loginVO = new LoginVO();
        loginVO.setId(62);
        loginVO.setExpert(false);
        loginVO.setName("楚闯");
        loginVO.setRole(Role.stu.name());
        loginVO.setLoginMode(LoginMode.PHONE);

        Invite invite = new Invite();
        invite.setUser(loginVO);
        IdentityQuery identityQuery = new IdentityQuery();
        invite.setIdentityQuery(identityQuery);
        invite.setRunner(Role.Runner.user);
        System.out.println(identityBO.findCanInvitationUser(invite, studentNoGroupHandler));

    }

    @Test
    public void noCroupCreate() throws Exception{
        LoginVO loginVO = new LoginVO();
        loginVO.setId(62);
        loginVO.setExpert(false);
        loginVO.setName("楚闯");
        loginVO.setRole(Role.stu.name());
        loginVO.setLoginMode(LoginMode.PHONE);
        Print.println(studentBO.noGroupInstantiate(loginVO), UserDescVO.class);
    }

    @Test
    public void syncAddress() {
        LoginVO loginVO = new LoginVO();
        loginVO.setId(62);
        loginVO.setExpert(false);
        loginVO.setName("楚闯");
        loginVO.setRole(Role.stu.name());
        loginVO.setLoginMode(LoginMode.PHONE);
        studentBO.syncAddress(loginVO);
    }

}