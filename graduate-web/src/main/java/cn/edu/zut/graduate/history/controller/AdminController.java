//package cn.edu.zut.graduate.controller;
//
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.DaoFit;
//import cn.edu.zut.graduate.supervisor.ResultDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.constant.TopStatus;
//import cn.edu.zut.soft.graduate.core.model.Student;
//import cn.edu.zut.soft.graduate.core.model.Teacher;
//import cn.edu.zut.soft.graduate.core.model.Topic;
//import cn.edu.zut.soft.graduate.core.model.tools.*;
//import cn.edu.zut.soft.graduate.core.utils.DataUtil;
//import cn.edu.zut.soft.graduate.core.utils.Excel;
//import cn.edu.zut.soft.graduate.core.utils.ExcelUtil;
//import org.apache.ibatis.session.RowBounds;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//import tag.BaseController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.File;
//import java.io.OutputStream;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by cc on 15/11/5.
// */
//
//@Controller
//@RequestMapping("/admin/")
//public class AdminController extends BaseController {
//
//	private Role r = Role.ADMIN;
//
//	@Resource
//	private ServiceFit serviceFit;
//	@Resource
//	private DaoFit daoFit;
//
//	@RequestMapping("home")
//	public ModelAndView home(Model model) {
//		model.addAttribute("menuSelected1", ConfigDo.INDEX);
//		return new ModelAndView("redirect:./file/toAdminUploadFile.do");
//	}
//
//	@RequestMapping("selectTopic")
//	public ModelAndView SelectTopic(Map<String, Object> model, HttpSession session,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s, SelectTopic selectTopic) {
//		ResultDo resultDo;
//		String selectTopicSUM = (String) session.getAttribute("" + selectTopic.hashCode());
//		int totalCount = 0;
//		if (selectTopic == null) {
//			resultDo = serviceFit.getTopicService().findAllTopic(new RowBounds((l - 1) * s, s));
//			totalCount = ((List) serviceFit.getTopicService().findAllTopic(new RowBounds()).getResult()).size();
//		} else {
//			resultDo = serviceFit.getTopicService().findBySelectTopic(selectTopic, new RowBounds((l - 1) * s, s));
//			totalCount = ((List) serviceFit.getTopicService().findBySelectTopic(selectTopic, new RowBounds())
//					.getResult()).size();
//		}
//		if (resultDo.isSuccess()) {
//			model.put("topicList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "findTeaByNoTopicSuc");
//		assert selectTopic != null;
//		map.put("status", selectTopic.getTopSource() == null || selectTopic.getTopSource().equals("null")
//				? TopStatus.SAVE : selectTopic.getTopStatus());
//		map.put("checked", true);
//		resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), r);
//		if (resultDo.isSuccess()) {
//			model.put("teaList", resultDo.getResult());
//		} else {
//			model.put("mag", model.get("mag") + resultDo.getMessage());
//		}
//
//		initPage(model, l, s, totalCount);
//		model.put("menuSelected1", ConfigDo.ADMIN_TOPIC);
//		model.put("menuSelected2", ConfigDo.ADMIN_SELECT);
//		return new ModelAndView(r.getName() + "/selectTopic", model);
//	}
//
//	@RequestMapping("stuTopic")
//	public ModelAndView stuTopictitle(HttpSession session,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s, SelectTopic selectTopic,
//			Map<String, Object> model) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		int totalCount = 0;
//		map.put("type", "stuTopicTitle");
//		ResultDo resultDo = serviceFit.getTopicService().selectTopicStatus(map, selectTopic,
//				new RowBounds((l - 1) * s, s));
//		totalCount = ((List) serviceFit.getTopicService().selectTopicStatus(map, selectTopic, new RowBounds())
//				.getResult()).size();
//		if (resultDo.isSuccess()) {
//			model.put("topicList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		initPage(model, l, s, totalCount);
//		model.put("menuSelected1", ConfigDo.ADMIN_TOPIC);
//		model.put("menuSelected2", ConfigDo.ADMIN_STUDENT_TOPIC);
//		return new ModelAndView(r.getName() + "/stuTopic", model);
//	}
//
//	@RequestMapping("findTeaByStatus")
//	@ResponseBody
//	public Map<String, Object> findTeaByStatus(@RequestParam(value = "s", defaultValue = "-1") int s) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		ResultDo resultDo;
//		TopStatus topStatus = TopStatus.getName(s);
//		Map<String, Object> m = new HashMap<String, Object>();
//		m.put("type", "findTeaByNoTopicSuc");
//		m.put("status", topStatus);
//		m.put("checked", false);
//		resultDo = serviceFit.getEmpService().findEmp(m, new RowBounds(), r);
//		if (resultDo.isSuccess()) {
//			List<Teacher> list = (List<Teacher>) resultDo.getResult();
//			List<Data> ls = new ArrayList<Data>();
//			for (Teacher teacher : list) {
//				ls.add(new Data(teacher.getTeaId(), teacher.getTeaName()));
//			}
//			map.put("term", ls);
//		} else {
//			map.put("mag", resultDo.getMessage());
//		}
//		return map;
//	}
//
//	@RequestMapping("selectOneTopic")
//	public ModelAndView selectOneTopic(Map model, @RequestParam(value = "topId", required = false) String topId) {
//		ResultDo resultDo;
//		String mag = "";
//		if (topId != null) {
//			resultDo = serviceFit.getTopicService().loadTopic(topId);
//			if (resultDo.isSuccess()) {
//				model.put("topicInfo", resultDo.getResult());
//				resultDo = serviceFit.getTopicService().findSGTLink(topId, new RowBounds());
//				if (resultDo.isSuccess()) {
//					model.put("SGTLink", resultDo.getResult());
//				} else {
//					mag = mag + resultDo.getMessage();
//				}
//			} else {
//				mag = mag + resultDo.getMessage();
//			}
//		} else {
//			mag = mag + "没有信息";
//		}
//		model.put("teaList", serviceFit.getEmpService().findAll(null, Role.TEA, new RowBounds()).getResult());
//
//		model.put("mag", mag);
//		return new ModelAndView(r.getName() + "/selectOneTopic", model);
//	}
//
//	@RequestMapping("submitAdTopic")
//	public ModelAndView refuseAdTopic(RedirectAttributes attr, @RequestParam("id") String topId,
//			@RequestParam("status") int st, @RequestParam(value = "details", required = false) String content) {
//		String mag = "";
//		Topic topic = null;
//		ResultDo resultDo;
//		TopStatus status = TopStatus.getName(st);
//		resultDo = serviceFit.getTopicService().AdminAudit(topId, content, status);
//		if (resultDo.isSuccess()) {
//			topic = (Topic) resultDo.getResult();
//			attr.addAttribute("teacherId", topic.getTopTeacherId());
//			attr.addAttribute("topStatus", null);
//			if (topic.getTopSource() == 3) {
//				return new ModelAndView("redirect:./stuTopic.do");
//			}
//			//
//			// if (topic.getTopSource() == 3){
//			// StuGroupTopicLink stuGroupTopicLink =
//			// ((List<StuGroupTopicLink>)serviceFit.getStuGroupService().selectBySgId(topId).getResult()).get(0);
//			// serviceFit.getStuGroupService().updateTopStuLink(stuGroupTopicLink.getChooId(),true);
//			// }
//		} else {
//			mag = mag + resultDo.getMessage();
//		}
//
//		attr.addAttribute("mag", mag);
//		return new ModelAndView("redirect:./selectTopic.do");
//	}
//
//	@RequestMapping("allTeacherList")
//	public ModelAndView allTeacher(Map<String, Object> model, Teacher teacher,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		ResultDo resultDo, resultDo2;
//		// System.out.println("测试------" + teacher);
//		if (teacher.getTeaId() == null) {
//			teacher.setTeaId("");
//		}
//		resultDo = serviceFit.getEmpService().findAll(teacher.getTeaId(), Role.TEA, new RowBounds((l - 1) * s, s));
//		int count = ((List) serviceFit.getEmpService().findAll(teacher.getTeaId(), Role.TEA, new RowBounds())
//				.getResult()).size();
//		initPage(model, l, s, count);
//		if (resultDo.isSuccess()) {
//			model.put("teacherList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		resultDo2 = serviceFit.getEmpService().findAll("", r.TEA, new RowBounds());
//		if (resultDo2.isSuccess()) {
//			model.put("teaList", resultDo2.getResult());
//		} else {
//			model.put("mag", resultDo2.getMessage());
//		}
//
//		model.put("menuSelected1", ConfigDo.ADMIN_TEACHER);
//		model.put("menuSelected2", ConfigDo.ADMIN_TEACHERINFO);
//		return new ModelAndView(r.getName() + "/allTeacherList");
//	}
//
//	/**
//	 * 统计
//	 *
//	 * @param model
//	 * @param teacher
//	 * @param l
//	 * @param s
//	 * @return
//	 */
//	@RequestMapping("tjTeaTopic")
//	public String TJTeaTopic(Map<String, Object> model, Teacher teacher,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		ResultDo resultDo;
//		if (teacher.getTeaName() == null) {
//			teacher.setTeaName("");
//		}
//		resultDo = serviceFit.getEmpService().findAll("", Role.TEA, new RowBounds());
//		if (resultDo.isSuccess()) {
//			model.put("teacherList", resultDo.getResult());
//		}
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "tjTeaTop");
//		map.put("name", teacher.getTeaName());
//		resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds((l - 1) * s, s), r);
//		int count = ((List) serviceFit.getEmpService().findEmp(map, new RowBounds(), r).getResult()).size();
//		initPage(model, l, s, count);
//		if (resultDo.isSuccess()) {
//			model.put("list", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//
//		model.put("menuSelected1", ConfigDo.ADMIN_COUNT);
//		model.put("menuSelected2", ConfigDo.ADMIN_TOPICCOUNT);
//		return r.getName() + "/tjTeaTopic";
//	}
//
//	@RequestMapping("lookTeaStudent")
//	public String lookTeaStudent(Map<String, Object> model, @RequestParam(value = "teaId") String teaId,
//			@RequestParam(value = "b", required = false, defaultValue = "false") boolean b) {
//		ResultDo resultDo;
//		resultDo = serviceFit.getEmpService().load(teaId, Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "lookTeaStudent");
//			map.put("teaId", teaId);
//			map.put("b", b);
//			resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), r);
//			if (resultDo.isSuccess()) {
//				model.put("list", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		}
//		model.put("menuSelected1", ConfigDo.ADMIN_TOPIC);
//		model.put("menuSelected2", ConfigDo.ADMIN_TOPICCOUNT);
//		return r.getName() + "/lookTeaStudent";
//	}
//
//	@RequestMapping("teacherUpDaile")
//	public ModelAndView teacherUpDaile(Map<String, Object> model, @RequestParam("id") String id) {
//		ResultDo resultDo;
//		resultDo = serviceFit.getEmpService().load(id, Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacher", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		return new ModelAndView(r.getName() + "/teacherUpDaile", model);
//	}
//
//	@RequestMapping("addTeacher")
//	public ModelAndView AddTeacher() {
//		return new ModelAndView(r.getName() + "/teacherUpDaile");
//	}
//
//	@RequestMapping("updateTea")
//	public ModelAndView updateTea(Map<String, String> model, Teacher teacher) {
//		if (teacher != null) {
//			serviceFit.getEmpService().insertAndUpdate(teacher, Role.TEA);
//		} else {
//			model.put("mag", "不存在数据");
//		}
//		return new ModelAndView("redirect:./allTeacherList.do");
//	}
//
//	@RequestMapping("allStudentList")
//	public ModelAndView allStudent(Map<String, Object> model, Student student,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		ResultDo resultDo;
//		if (student.getStuName() == null) {
//			student.setStuStudentId("");
//		}
//		resultDo = serviceFit.getEmpService().findAll(student.getStuName(), Role.STU, new RowBounds((l - 1) * s, s));
//		int count = ((List) serviceFit.getEmpService().findAll("", r.STU, new RowBounds()).getResult()).size();
//		if (resultDo.isSuccess()) {
//			model.put("studentList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		initPage(model, l, s, count);
//		model.put("menuSelected1", ConfigDo.ADMIN_STUDENT);
//		model.put("menuSelected2", ConfigDo.ADMIN_STUDENTINFO);
//		return new ModelAndView(r.getName() + "/allStudentList");
//	}
//
//	@RequestMapping("allStuSta")
//	public ModelAndView allStuSta(Map<String, Object> model, StudentSta studentSta,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		ResultDo resultDo;
//		if (studentSta.getStuName() == null) {
//			studentSta.setStuStudentId("");
//		}
//		// System.out.println(studentSta.getChoStatus());
//		resultDo = serviceFit.getEmpService().findStuStatus(studentSta.getStuName(), studentSta.getChoStatus() + 1,
//				new RowBounds((l - 1) * s, s));
//		int count = ((List) serviceFit.getEmpService()
//				.findStuStatus(studentSta.getStuName(), studentSta.getChoStatus() + 1, new RowBounds()).getResult())
//						.size();
//		if (resultDo.isSuccess()) {
//			model.put("studentList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		initPage(model, l, s, count);
//		model.put("menuSelected1", ConfigDo.ADMIN_STUDENT);
//		model.put("menuSelected2", ConfigDo.ADMIN_STUDENTINFO);
//		return new ModelAndView(r.getName() + "/allStuSta");
//	}
//
//	@RequestMapping("studentUpDaile")
//	public ModelAndView studentUpDaile(Map model, @RequestParam("id") String id) {
//		ResultDo resultDo;
//		resultDo = serviceFit.getEmpService().load(id, Role.STU);
//		if (resultDo.isSuccess()) {
//			model.put("student", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		return new ModelAndView(r.getName() + "/studentUpDaile");
//	}
//
//	@RequestMapping("addStudent")
//	public ModelAndView AddStudent() {
//		return new ModelAndView(r.getName() + "/studentUpDaile");
//	}
//
//	@RequestMapping("updateSta")
//	public ModelAndView updateTea(Map<String, String> model, Student student) {
//		// System.out.println("测试学生--1" + student);
//		if (student != null) {
//			serviceFit.getEmpService().insertAndUpdate(student, Role.STU);
//		} else {
//			model.put("mag", "不存在数据");
//		}
//		return new ModelAndView("redirect:./allStudentList.do");
//	}
//
//	/**
//	 * 所有的审核管理
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("auditAllocation")
//	public String auditAllocation(Map<String, Object> model,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		model.put("menuSelected1", ConfigDo.TEACHER_POWER);
//		model.put("menuSelected2", ConfigDo.TEACHER_POWE);
//		AllAudit(model, "AllAudit", true, new RowBounds((l - 1) * s, s));
//		return r.getName() + "/auditAllocation";
//	}
//
//	/**
//	 * 添加新专家
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("auditExpert")
//	public String auditExpert(Map<String, Object> model,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		AllAudit(model, "AllAudit", false, new RowBounds((l - 1) * s, s));
//
//		return r.getName() + "/auditExpert";
//	}
//
//	private void AllAudit(Map<String, Object> model, String s, Boolean b, RowBounds rowBounds) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", s);
//		map.put("val", b);
//		ResultDo resultDo = serviceFit.getEmpService().findEmp(map, rowBounds, Role.TEA);
//		int count = ((List) serviceFit.getEmpService().findEmp(map, rowBounds, Role.TEA).getResult()).size();
//		initPage(model, rowBounds.getOffset(), rowBounds.getLimit(), count);
//		if (resultDo.isSuccess()) {
//			model.put("teacherList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//	}
//
//	/**
//	 * 添加新专家
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("submitAuditExpert")
//	public String submitAuditExpert(Model model, @RequestParam(value = "teaId", defaultValue = "") String[] id) {
//		// System.out.println(Arrays.toString(id));
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "upTeaRole");
//		map.put("val", true);
//		map.put("ids", id);
//		serviceFit.getEmpService().findEmp(map, new RowBounds(), r);
//		return "redirect:./auditAllocation.do";
//	}
//
//	/**
//	 * 分配任务
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("auditTask")
//	public String auditTask(Map<String, Object> model, SelectTea selectTea,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		if (serviceFit.getEmpService().load(selectTea.getId(), Role.TEA) != null) {
//			// System.out.println("测试----auditTask"+teacher);
//			model.put("teaId", selectTea.getId());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("type", "findTeaByNoTopicSuc");
//			map.put("status", TopStatus.AUDIT);
//			map.put("checked", false);
//			ResultDo resultDo1 = serviceFit.getEmpService().findEmp(map, new RowBounds(), r);
//			if (resultDo1.isSuccess()) {
//				model.put("teaList", resultDo1.getResult());
//			} else {
//				model.put("mag", resultDo1.getMessage());
//			}
//			ResultDo resultDo = serviceFit.getTopicService().findByNoExpert(selectTea.getTeaId(),
//					new RowBounds((l - 1) * s, s));
//			int count = ((List) serviceFit.getTopicService().findByNoExpert(selectTea.getTeaId(), new RowBounds())
//					.getResult()).size();
//			initPage(model, l, s, count);
//			if (resultDo.isSuccess()) {
//				model.put("topicList", resultDo.getResult());
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//			// }else {
//			// model.addAttribute("mag","教师选择错误");
//			// }
//		}
//		return r.getName() + "/auditTask";
//
//	}
//
//	/**
//	 * 修改任务
//	 *
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("updateTask")
//	public String updateTask(Map<String, Object> model, @RequestParam(value = "id") String teaId,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//		if (serviceFit.getEmpService().load(teaId, Role.TEA) != null) {
//			model.put("teaId", teaId);
//			ResultDo resultDo = serviceFit.getTopicService().findByExpert(teaId, new RowBounds((l - 1) * s, s));
//			if (resultDo.isSuccess()) {
//				model.put("topicList", resultDo.getResult());
//				int count = ((List) serviceFit.getTopicService().findByExpert(teaId, new RowBounds()).getResult())
//						.size();
//				initPage(model, l, s, count);
//			} else {
//				model.put("mag", resultDo.getMessage());
//			}
//		} else {
//			model.put("mag", "教师选择错误");
//		}
//		return r.getName() + "/updateTask";
//	}
//
//	/**
//	 * 移除分配任务
//	 *
//	 * @param model
//	 * @return 返回查询专家列表
//	 */
//	@RequestMapping("updateAuditTask")
//	public String updateAuditTask(Model model, @RequestParam(value = "topId") String[] ids,
//			@RequestParam("teaId") String teaId) {
//		ResultDo resultDo = serviceFit.getEmpService().load(teaId, Role.TEA);
//		if (resultDo.isSuccess()) {
//			Teacher teacher = (Teacher) resultDo.getResult();
//			resultDo = serviceFit.getTopicService().deleteETL(ids, teacher);
//			if (!resultDo.isSuccess()) {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./auditAllocation.do";
//	}
//
//	/**
//	 * 提交分配任务
//	 *
//	 * @param model
//	 * @return 返回查询专家列表
//	 */
//	@RequestMapping("SubmitAuditTask")
//	public String SubmitAuditTask(Model model, @RequestParam(value = "topId") String[] ids,
//			@RequestParam("teaId") String teaId) {
//		ResultDo resultDo = serviceFit.getEmpService().load(teaId, Role.TEA);
//		if (resultDo.isSuccess()) {
//			Teacher teacher = (Teacher) resultDo.getResult();
//			resultDo = serviceFit.getTopicService().createETL(ids, teacher);
//			if (!resultDo.isSuccess()) {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./auditAllocation.do";
//	}
//
//	/**
//	 * 修改教师.
//	 *
//	 * @param model
//	 * @param topId
//	 * @param teaId
//	 * @return
//	 */
//	@RequestMapping("submitNewTea")
//	public String submitNewTea(Model model, @RequestParam("topId") String topId, @RequestParam("teaId") String teaId) {
//		ResultDo resultDo = serviceFit.getTopicService().loadTopic(topId);
//		Topic topic;
//		if (resultDo.isSuccess()) {
//			topic = (Topic) resultDo.getResult();
//			resultDo = serviceFit.getEmpService().load(teaId, Role.TEA);
//			if (resultDo.isSuccess()) {
//				Teacher teacher = (Teacher) resultDo.getResult();
//				topic.setTopTeacherId(teacher.getTeaId());
//				System.out.println(topic);
//				serviceFit.getTopicService().saveOrUpdateTopic(topic);
//			}
//		}
//		return "redirect:./selectOneTopic.do?topId=" + topId;
//	}
//
//	@RequestMapping("noTeaTopic")
//	public @ResponseBody Map<String, Object> noTeaTopic() {
//		Map<String, Object> map = new HashMap<String, Object>();
//		ResultDo resultDo = serviceFit.getTopicService().findNoTeaTopic();
//		map.put("list", resultDo.getResult());
//		map.put("status", resultDo.isSuccess());
//		return map;
//	}
//
//	@RequestMapping("teacherFpRw")
//	public String teacherFpRw(Map<String, Object> model, TeacherTJ teacherTJ,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s) {
//
//		ResultDo resultDo;
//		if (teacherTJ.getTeaName() == null) {
//			teacherTJ.setTeaName("");
//		}
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "teacherFpRw");
//		map.put("name", teacherTJ.getTeaName());
//		resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds((l - 1) * s, s), r);
//		int count = ((List) serviceFit.getEmpService().findEmp(map, new RowBounds(), r).getResult()).size();
//		initPage(model, l, s, count);
//		if (resultDo.isSuccess()) {
//			model.put("list", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		resultDo = serviceFit.getEmpService().findAll("", Role.TEA, new RowBounds());
//		if (resultDo.isSuccess()) {
//			model.put("teacherList", resultDo.getResult());
//		}
//		model.put("menuSelected1", ConfigDo.ADMIN_COUNT);
//		model.put("menuSelected2", ConfigDo.ADMIN_TOPICCOUNT);
//		return r.getName() + "/tjFpTeaTopic";
//	}
//
//	@RequestMapping("personalTitle")
//	public ModelAndView personalTitle(Map<String, Object> model,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "1000") int s,
//			@RequestParam("teaId") String teaId) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		SelectTopic selectTopic = new SelectTopic();
//		selectTopic.setTeacherId("mytitleTeacher");
//		int totalCount = 0;
//		map.put("type", "TeacherDisPerson");
//		ResultDo resultDo = serviceFit.getTopicService().selectTopicStatus(map, selectTopic,
//				new RowBounds((l - 1) * s, s));
//		totalCount = ((List) serviceFit.getTopicService().selectTopicStatus(map, selectTopic, new RowBounds())
//				.getResult()).size();
//		if (resultDo.isSuccess()) {
//			model.put("topicList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		if (teaId != null) {
//			model.put("teaId", teaId);
//		}
//		// initPage(model,l,s,totalCount);
//		return new ModelAndView(r.getName() + "/disPersonalTitle", model);
//	}
//
//	/**
//	 * 批量分配自拟题目给单个教师
//	 *
//	 * @param model
//	 * @param topId
//	 * @param teaId
//	 * @return
//	 */
//	@RequestMapping("submitDisPersonal")
//	public String submitDisPersonal(Model model, @RequestParam("topId") String[] topId,
//			@RequestParam("teaId") String teaId) {
//		ResultDo resultDo = new ResultDo();
//		if (topId.length > 0 && teaId != null && !"".equals(teaId)) {
//			resultDo = serviceFit.getTopicService().disPersonalTopic(topId, teaId);
//			if (resultDo.isSuccess()) {
//				return "redirect:./tjTeaTopic.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "不符合条件！");
//		}
//		return "redirect:./personalTitle.do?teaId=" + teaId;
//	}
//
//	@RequestMapping("assignTeacherGroup")
//	public ModelAndView assignTeacherGroup(Map<String, Object> model) {
//		ResultDo resultDo = new ResultDo();
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "teaNoGroup");
//		resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.TEA);
//		if (resultDo.isSuccess()) {
//			model.put("teacherList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		resultDo = serviceFit.getTeacherGroupService().allTeaGroup();
//		if (resultDo.isSuccess()) {
//			model.put("teaGroup", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		model.put("menuSelected1", ConfigDo.ADMIN_TEACHER);
//		model.put("menuSelected2", ConfigDo.ADMIN_TEACHERGROUP);
//		return new ModelAndView(r.getName() + "/assignTeaGroup", model);
//	}
//
//	@RequestMapping("TeacherNoGroup")
//	public ModelAndView teacherNoGroup(Map<String, Object> model,
//			@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s,
//			@RequestParam(value = "tgId", required = false) String tgId) {
//		ResultDo resultDo = new ResultDo();
//		int Count = 0;
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("type", "teaNoGroup");
//		resultDo = serviceFit.getEmpService().findEmp(map, new RowBounds((l - 1) * s, s), Role.TEA);
//		Count = ((List) serviceFit.getEmpService().findEmp(map, new RowBounds(), Role.TEA).getResult()).size();
//		if (resultDo.isSuccess()) {
//			model.put("teacherList", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		if (tgId != null) {
//			model.put("tgId", tgId);
//		}
//		initPage(model, l, s, Count);
//		return new ModelAndView(r.getName() + "/assignTeacher", model);
//	}
//
//	@RequestMapping("submitAssignTeacher")
//	public String submitAssignTeacher(Model model, String[] teaId, String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (teaId.length > 0 && tgId != null) {
//			resultDo = serviceFit.getEmpService().updateTeacherGroup(teaId, tgId);
//			if (resultDo.isSuccess()) {
//				return "redirect:./assignTeacherGroup.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//
//		return "redirect:./TeacherNoGroup.do?tgId=" + tgId;
//	}
//
//	@RequestMapping("createTeaGroup")
//	public String createTeaGroup(Model model, @RequestParam("teaId") String teaId) {
//
//		if (teaId != null) {
//			ResultDo resultDo = serviceFit.getTeacherGroupService().createGroup(teaId);
//			if (resultDo.isSuccess()) {
//				return "redirect:./assignTeacherGroup.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//		return "redirect:./assignTeacherGroup.do";
//	}
//
//	@RequestMapping("assignIdentity")
//	public String assignIdentity(Model model, @RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findTeacherBytgId(tgId);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("teacher", resultDo.getResult());
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据错误！");
//		}
//
//		return r.getName() + "/assignGroupTeacher";
//	}
//
//	@RequestMapping("deleteGroupTeacher")
//	public String deleteGroupTeacher(Model model, @RequestParam("teaId") String teaId,
//			@RequestParam("teaGroupId") String teaGroupId) {
//		ResultDo resultDo = new ResultDo();
//		if (teaId != null) {
//			resultDo = serviceFit.getTeacherGroupService().deleteGroupTeacher(teaId);
//			if (resultDo.isSuccess()) {
//				return "redirect:./assignIdentity.do?tgId=" + teaGroupId;
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//		return "redirect:./assignIdentity.do?tgId=" + teaGroupId;
//	}
//
//	@RequestMapping("deleteGroup")
//	public String deleteGroup(Model model, @RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().deleteGroup(tgId);
//			if (resultDo.isSuccess()) {
//				return "redirect:./assignTeacherGroup.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "传入数据为空！");
//		}
//
//		return "redirect:./assignTeacherGroup.do";
//
//	}
//
//	@RequestMapping("submitIndentity")
//	public String submitIndentity(Model model, @RequestParam("teaId") String teaId,
//			@RequestParam("teaIndentity") int teaIndentity, @RequestParam("tgId") String tgId) {
//		ResultDo resultDo = new ResultDo();
//		System.out.println(teaId + "--1" + "---" + teaIndentity + "---2" + "---" + tgId);
//
//		if (teaId != null) {
//			resultDo = serviceFit.getTeacherGroupService().assignIdentity(teaId, teaIndentity);
//			if (resultDo.isSuccess()) {
//				return "redirect:./assignIdentity.do?tgId=" + tgId;
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//
//		return "redirect:./assignIdentity.do?tgId=" + tgId;
//	}
//
//	@RequestMapping("restartPwd")
//	public @ResponseBody Map<String, Object> restartPwd(HttpSession session, @RequestParam("stuId") String stuId) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String user = (String) session.getAttribute("user");
//		String mag = "";
//		boolean flag = false;
//		if ("zzti".equals(user)) {
//			Role role;
//			if (stuId.length() != 12) {
//				role = Role.TEA;
//			} else {
//				role = Role.STU;
//			}
//			ResultDo resultDo = serviceFit.getEmpService().updateRestartPwd(stuId, role);
//			if (resultDo.isSuccess()) {
//				flag = true;
//				mag = "重置密码成功.";
//			} else {
//				mag = resultDo.getMessage();
//			}
//		} else {
//			mag = "非管理员不可以重置密码";
//		}
//		map.put("status", flag);
//		map.put("mag", mag);
//		return map;
//	}
//
//	@RequestMapping("openGradeByStudent")
//	public ModelAndView openGradeByStudent(@RequestParam(value = "pageNum", required = false, defaultValue = "1") int l,
//			@RequestParam(value = "pageSize", required = false, defaultValue = "20") int s, Map<String, Object> model) {
//		ResultDo resultDo = new ResultDo();
//		resultDo = serviceFit.getTeacherGroupService().findStudentBytgId(null, 1);
//		if (resultDo.isSuccess()) {
//			model.put("stulistFalse", resultDo.getResult());
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		ResultDo resultDo1 = serviceFit.getTeacherGroupService().findStudentBytgId(null, 2);
//		if (resultDo1.isSuccess()) {
//			model.put("stulistTrue", resultDo1.getResult());
//		} else {
//			model.put("mag", resultDo1.getMessage());
//		}
//		model.put("menuSelected1", ConfigDo.GRADE);
//		model.put("menuSelected2", ConfigDo.OPENGRADE);
//		return new ModelAndView(r.getName() + "/OpenGrade", model);
//	}
//
//	@RequestMapping("updateOpenGradeMany")
//	public String updateOpenGrade(Model model, @RequestParam(value = "stuIds", required = false) String[] stuIds) {
//		ResultDo resultDo = new ResultDo();
//		if (stuIds.length > 0 && stuIds != null) {
//			resultDo = serviceFit.getEmpService().updateStudentOpenTopics(stuIds, true);
//			if (resultDo.isSuccess()) {
//				return "redirect:./openGradeByStudent.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			return "redirect:./openGradeByStudent.do";
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		model.addAttribute("menuSelected2", ConfigDo.OPENGRADE);
//		return "redirect:./openGradeByStudent.do";
//	}
//
//	@RequestMapping("updateOpenGradeone")
//	public String updateOpenGradeOne(Model model, @RequestParam(value = "stuId") String stuId) {
//		ResultDo resultDo = new ResultDo();
//		if (stuId != null && stuId != "") {
//			resultDo = serviceFit.getEmpService().updateStudentOpenTopic(stuId, true);
//			if (resultDo.isSuccess()) {
//				return "redirect:./openGradeByStudent.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		}
//		model.addAttribute("menuSelected1", ConfigDo.TEACHERASSIGNGROUP);
//		model.addAttribute("menuSelected2", ConfigDo.OPENGRADE);
//		return "redirect:./openGradeByStudent.do";
//	}
//
//	@RequestMapping("OpenOrCloseFunction")
//	public String OpenOrCloseFunction(Model model) {
//		ResultDo resultDo = serviceFit.getFunctionService().findAll(new RowBounds());
//		if (resultDo.isSuccess()) {
//			model.addAttribute("functionList", resultDo.getResult());
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		model.addAttribute("menuSelected1", ConfigDo.POWER);
//		model.addAttribute("menuSelected2", ConfigDo.OPENORCLOSEFUNCTION);
//		return r.getName() + "/OpenOrCloseFuction";
//	}
//
//	@RequestMapping("updateEditStatus")
//	public String updateEditStatus(HttpSession session, Model model, @RequestParam("status") boolean status,
//			@RequestParam("id") String id) {
//		ResultDo resultDo = new ResultDo();
//		if (id != null || id != "") {
//			resultDo = serviceFit.getFunctionService().update(id, status);
//			if (resultDo.isSuccess()) {
//				return "redirect:./OpenOrCloseFunction.do";
//			} else {
//				model.addAttribute("mag", resultDo.getMessage());
//			}
//		} else {
//			model.addAttribute("mag", "数据格式不正确！");
//		}
//		return "redirect:./OpenOrCloseFunction.do";
//	}
//
//	/**
//	 * 教师工作量统计--分配未分配学生
//	 *
//	 * @param id
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("fpxs")
//	public String fpxs(@RequestParam("id") String id, Model model) {
//		ResultDo resultDo = new ResultDo();
//		if (id != null || id != "") {
//			resultDo = serviceFit.getEmpService().findNoTeaStu();
//			model.addAttribute("teaId", id);
//			model.addAttribute("list", resultDo.getResult());
//			return r.getName() + "/allotStudent";
//		} else {
//			model.addAttribute("mag", resultDo.getMessage());
//		}
//		return "redirect:./tjTeaTopic.do";
//	}
//
//	/**
//	 * 教师工作量统计--批量分配未分配学生
//	 *
//	 * @param id
//	 * @param item
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("upFpxs")
//	public String upFpxs(@RequestParam("id") String id, @RequestParam(value = "stuId", defaultValue = "") String[] item,
//			Model model) {
//		ResultDo resultDo = new ResultDo();
//		if (item != null && item.length > 0) {
//			resultDo = serviceFit.getEmpService().upNoTeaStu(id, item);
//			if (resultDo.isSuccess()) {
//				model.addAttribute("mag", "分配成功！");
//				return "redirect:./tjTeaTopic.do";
//			} else {
//				model.addAttribute("mag", "分配失败！");
//			}
//		} else {
//			model.addAttribute("mag", "数据格式错误！");
//		}
//		return "redirect:./fpxs.do?id" + id;
//	}
//
//	@RequestMapping("downloadTopic")
//	public void downLoadTopic(HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition", "attachment;fileName=Topic.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//		resultDo = serviceFit.getUploadService().DownLoadTopic();
//		if (resultDo.isSuccess()) {
//			// System.out.println("teach"+resultDo.getResult());
//			excel = (Excel) resultDo.getResult();
//			try {
//				OutputStream fout = response.getOutputStream();
//				ExcelUtil.excelFactory(excel).write(fout);
//				fout.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//
//	// 管理员端未交周月报统计
//	@RequestMapping("ReportManage")
//	public ModelAndView ReportManage(Map<String, Object> model) {
//		ResultDo resultDo = serviceFit.getReportSerivce().searchDetails(r, null, new RowBounds());
//		List<String[]> req = new ArrayList<>();
//		List<String[]> req2 = new ArrayList<>();
//		req = (List<String[]>) resultDo.getResult();
//
//		DataUtil du = new DataUtil();
//		String[] st;
//		Student stu;
//		for (String[] ss : req) {
//			st = new String[6];
//
//			stu = daoFit.getStudentDao().selectByKey(ss[0]);
//			st[0] = stu.getStuName(); // 姓名
//			if (stu.isStuSex()) { // 性别
//				st[1] = "男";
//			} else {
//				st[1] = "女";
//			}
//			st[2] = stu.getStuClass(); // 班级
//			if (stu.getStuTeacherId() == null) {
//				st[3] = "无指导教师";
//			} else {
//				st[3] = daoFit.getTeacherDao().selectByKey(stu.getStuTeacherId()).getTeaName();
//				;
//			}
//			// st[4] =
//			// daoFit.getTeacherDao().selectByKey("FF820E3DB7015858DE8F65754DFA1046").getTeaName();
//			st[4] = du.Notsubmitted(Long.parseLong(ss[1])); // 未交周
//			st[5] = du.Notsubmitted(Long.parseLong(ss[2])); // 未交月
//			req2.add(st);
//
//		}
//		if (resultDo.isSuccess()) {
//			// model.put("report", resultDo.getResult());
//			model.put("report", req2);
//		} else {
//			model.put("mag", resultDo.getMessage());
//		}
//		model.put("menuSelected1", ConfigDo.ADMIN_COUNT);
//		model.put("menuSelected2", ConfigDo.ADMIN_NO_REPORT);
//		return new ModelAndView(r.getName() + "/testReport", model);
//	}
//
//	@RequestMapping("teaGroupNameList")
//	public String teaGroupList(Model model) {
//		ResultDo resultDo = null;
//		Teacher teacher = null;
//		List<TeacherGroupStu> list = new ArrayList<TeacherGroupStu>();
//		resultDo = serviceFit.getTeacherGroupService().allTeaGroup();
//		if (resultDo.isSuccess()) {
//			list = (List<TeacherGroupStu>) resultDo.getResult();
//			for (TeacherGroupStu ls : list) {
//				teacher = (Teacher) serviceFit.getEmpService().findSecretaryByTgId(ls.getTgId()).getResult();
//				if (teacher == null) {
//					ls.setSecretary("无");
//				} else {
//					ls.setSecretary(teacher.getTeaName());
//				}
//			}
//			model.addAttribute("teaGroup", list);
//		}
//		model.addAttribute("menuSelected1", ConfigDo.DATEOUTPUT_STRING);
//		model.addAttribute("menuSelected2", ConfigDo.DEFENSENAMELIST);
//		return r.getName() + "/defenseNameList";
//	}
//
//	/**
//	 * 答辩名单导出
//	 *
//	 * @param tgId
//	 * @param response
//	 */
//	@RequestMapping("downloadDefense")
//	public void dowmLoad(@RequestParam("tgId") String tgId, @RequestParam("name") String groupName,
//			HttpServletResponse response) {
//		response.setCharacterEncoding("utf-8");
//		response.setContentType("multipart/form-data");
//		response.setHeader("Content-Disposition", "attachment;fileName=student.xls");
//		Excel excel = new Excel();
//		ResultDo resultDo = new ResultDo();
//		if (tgId != null) {
//			resultDo = serviceFit.getUploadService().HandleExcelDefenseList(tgId);
//			if (resultDo.isSuccess()) {
//				// System.out.println("teach"+resultDo.getResult());
//				excel = (Excel) resultDo.getResult();
//				try {
//					File file = new File("小组学生信息表.xls");
//					OutputStream fout = response.getOutputStream();
//					ExcelUtil.excelFactory(excel).write(fout);
//					fout.close();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}
//
//	@RequestMapping("findDockingListName")
//	public @ResponseBody Map<String, Object> findDocking(@RequestParam("tgId") String tgId) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		ResultDo resultDo = null;
//		if (tgId != null) {
//			resultDo = serviceFit.getTeacherGroupService().findDockingListName(tgId);
//			if (resultDo.isSuccess()) {
//				map.put("list", resultDo.getResult());
//				map.put("status", true);
//			}
//		}
//		return map;
//	}
//
//	@RequestMapping("updateDocking")
//	public String updateDocking(@RequestParam("tgId") String tgId, @RequestParam("tgTeagroupId") String tgTeagroupId,
//			Model model) {
//		ResultDo resultDo = serviceFit.getTeacherGroupService().updateDocking(tgId, tgTeagroupId);
//		if (resultDo.isSuccess()) {
//			return "redirect:./assignTeacherGroup.do";
//		}
//		return "redirect:./assignTeacherGroup.do";
//	}
//
//	// 管理员确定最终参与公开答辩的学生
//	@RequestMapping("AdminRecommend")//有问题
//	public ModelAndView recommend(Map<String, Object> model) {
//
//		ResultDo resultDo1 = serviceFit.getReportSerivce().searchDetails(r, null, new RowBounds());
//		List<String[]> req = new ArrayList<>();
//		List<String[]> req2 = new ArrayList<>();
//		req = (List<String[]>) resultDo1.getResult();
//
//		DataUtil du = new DataUtil();
//		String[] st;
//		Student stu;
//		int RE = 0;
//		for (String[] ss : req) {
//
//			st = new String[9];
//			stu = daoFit.getStudentDao().selectByKey(ss[0]);
//			st[0] = stu.getStuName();
//			st[1] = stu.getStuStudentId();
//			if (stu.isStuSex()) {
//				st[2] = "女";
//			} else {
//				st[2] = "男";
//			}
//			st[3] = stu.getStuClass();
//			if (stu.getStuTeacherId() == null) {
//				st[4] = "无指导教师";
//			} else {
//				st[4] =daoFit.getTeacherDao().selectByKey(stu.getStuTeacherId()).getTeaName();
//				;
//			}
//			if (stu.getTopId() != null) {
//				Topic top = daoFit.getTopicDao().selectByKey(stu.getTopId());
//				st[5] = top.getTopTitle();
//			} else {
//				st[5] = "无选报课题";
//			}
//
//			RE = stu.getRecommend();
//			st[6] = RE + "";
//			st[7] = stu.getStuId();
//			if (stu.getWhorecommend() == null) {
//				st[8] = "无";
//			} else {
//				st[8] = stu.getWhorecommend();
//			}
//			//if (RE != 0) {
//				req2.add(st);
//			//}
//		}
//		if (resultDo1.isSuccess()) {
//			model.put("arecommond", req2);
//		} else {
//			model.put("mag", resultDo1.getMessage());
//		}
//
//		model.put("menuSelected1", ConfigDo.ADMIN_STUDENT);
//		// model.put("menuSelected2", ConfigDo.ADMINRECMMEND);
//
//		return new ModelAndView(r.getName() + "/AdminCommend", model);
//	}
//
//	// // 修改学生是否参与答辩s
//	@RequestMapping("UpadteAdminRecommend")
//	public String updaterecommend(Map<String, Object> model, @RequestParam("stuId") String id,
//			@RequestParam("num") int num) {
//		if (id != null && num > -1) {
//			boolean resultDo = serviceFit.getRecommendService().updateRecommend1(id, num);
//
//			/// 感觉此处逻辑不对
//			if (resultDo) {
//				return "redirect:./AdminRecommend.do";
//			} else {
//				return "redirect:./AdminRecommend.do";
//			}
//		}
//		return null;
//	}
//
//}
