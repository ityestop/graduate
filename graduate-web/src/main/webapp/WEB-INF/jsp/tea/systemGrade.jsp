<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>系统验收成绩</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
    <style type="text/css">
        #table{
            font-size: 10px;
        }
    </style>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">系统验收成绩</li>
        </ol>
        <div class="page-header">
            <h4>系统验收成绩
            </h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <input type="hidden" id="groupLeader" value="${sessionScope.GroupgroupLeader}"/>
            <input type="hidden" id="groupSEC" value="${sessionScope.GroupgroupSEC}"/>
            <table id="table" class="table table-container"></table>
        </div>
    </div>
</div>

<div class="modal fade" id="passSystemGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">系统验收成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="passStuId"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemOne" value="1001"/>
                        </label> 二次系统验收
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemOne" value="1002"/>
                        </label> 进入一辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemOne" value="1003"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="passSystemGradeSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="restSystemGrade" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">系统验收成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restStuId"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2001"/>
                        </label> 二次系统验收
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2002"/>
                        </label> 进入一辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2003"/>
                        </label> 进入二辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemTwo" value="2004"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restSystemGradeSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="systemGradeTwo" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">系统验收成绩</h4>
            </div>

            <div class="modal-body">
                <div class="form-horizontal">
                    <input type="hidden" id="restStuIdTwo"/>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemThree" value="2003"/>
                        </label> 进入二辩
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="passSystemThree" value="2004"/>
                        </label> 毕设终止
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="restSystemSubmit">提交</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript">

    var $goupLeader = $("#groupLeader").val();
    var $goupSEC = $("#groupSEC").val();

    var $role = false;
    if (($goupLeader != null && $goupLeader != "" && $goupLeader != undefined) || ($goupSEC != null && $goupSEC != "" && $goupSEC != undefined)) {
        $role = true;
    }

    var columns = [
        {
            field: 'identity.id',
            title: '编号'
        }, {
            field: 'identity.noId',
            title: '学号'
        }, {
            field: 'identity.name',
            title: '姓名'
        }, {
            title: '班级',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Basic' && array[i].key == 'clazz') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            field: 'identity.phone',
            title: '手机'
        }, {
            field: 'infoList',
            title: '指导教师',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'Tea' && array[i].key == 'guideName') {
                        str += array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '课题',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var topicName = "";
                var topicId = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC') {
                        if (array[i].key == 'topicName') {
                            topicName = array[i].value;
                        }else if (array[i].key == 'topicId'){
                            topicId = array[i].value;
                        }
                    }
                }
                return "<a href='/tea/issueDetail?id="+topicId+"'>"+topicName+"</a>";
            },
            sortStable: true
        },{
            title: '子任务',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'TOPIC' && array[i].key == 'childTopicName') {
                        str = array[i].value;
                        break;
                    }
                }
                return str;
            }
        }, {
            title: '成绩',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "<label class='label label-warning'>未操作</label>";
                for (var i = 0; i < array.length; i++) {
                    if (array[i].type == 'GRADE' && array[i].key == 'systemGrade_two') {
                        str = "<label class='label label-danger'>二次系统验收</label>";
                        break;
                    }else if(array[i].type == 'GRADE' && array[i].key == 'replyGrade_one'){
                        str = "<label class='label label-danger'>进入一辩</label>";
                        break;
                    }else if(array[i].type == 'GRADE' && array[i].key == 'replyGrade_two'){
                        str = "<label class='label label-danger'>进入二辩</label>";
                        break;
                    }else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                        if(array[i].value == 'openGrade'){
                            str = "<label class='label label-danger'>开题-毕设终止</label>";
                            break;
                        }else if(array[i].value == 'interimGrade'){
                            str = "<label class='label label-danger'>中期-毕设终止</label>";
                            break;
                        }else if(array[i].value == 'systemGrade'){
                            str = "<label class='label label-danger'>系统验收-毕设终止</label>";
                            break;
                        }
                    }
                }
                return str;
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var array = row.infoList;
                var str = "";
                if ($role) {
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].type == 'GRADE' && array[i].key == 'systemGrade_two') {
                            str = "<a  data-toggle='modal' data-target='#systemGradeTwo'>二次系统验收</a>";
                            break;
                        } else if(array[i].type == 'GRADE' && array[i].key == 'stop'){
                            if(array[i].value == 'openGrade'){
                                str = "<a href='javaScript:void(0);'>不可操作</a>";
                                break;
                            }else if(array[i].value == 'systemGrade'){
                                str = "<a  data-toggle='modal' data-target='#restSystemGrade'>重置</a>";
                                break;
                            }
                        }else if((array[i].type == 'GRADE' && array[i].key == 'replyGrade_two') || (array[i].type == 'GRADE' && array[i].key == 'replyGrade_one')){
                            str = "<a  data-toggle='modal' data-target='#restSystemGrade'>重置</a>";
                            break;
                        }else{
                            str = "<a  data-toggle='modal' data-target='#passSystemGrade'>验收成绩</a>"
                        }
                    }
                } else {
                    str = "无操作";
                }
                return str;
            }
        }
    ];
    customSearch({
        id: "#table",
        url: "/identity/teaGroupStu",
        dataType: "json",
        params:{groupId:${sessionScope.Groupgroup},reverse:true },
        columns: columns,
        pageSize: 25
    });

    $("#passSystemGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#passStuId").attr("value", $stuId);
    });

    $("#restSystemGrade").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restStuId").attr("value", $stuId);
    });

    $("#systemGradeTwo").on('show.bs.modal', function (e) {
        var $stuId = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
        $("#restStuIdTwo").attr("value", $stuId);
    });


    $("#passSystemGradeSubmit").click(function () {
        var $stuId = $("#passStuId").val();
        var $code = $("input[name='passSystemOne']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passSystemGrade',
            data: {
                stuId: $stuId,
                code: $code
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else {
                    alert(result.errorMsg);
                }
            }
        });
    });

    $("#restSystemGradeSubmit").click(function () {
        var $stuId = $("#restStuId").val();
        var $code = $("input[name='passSystemTwo']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passSystemGrade',
            data: {
                stuId: $stuId,
                code: $code
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    });

    $("#restSystemSubmit").click(function () {
        var $stuId = $("#restStuIdTwo").val();
        var $code = $("input[name='passSystemThree']:checked").val();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/grade/passSystemGrade',
            data: {
                stuId: $stuId,
                code: $code
            },
            success: function (result) {
                if (result.success) {
                    window.location.reload();
                }else{
                    alert(result.errorMsg);
                }
            }
        });
    });

</script>
</body>
</html>
