package cn.edu.zut.soft.graduate.core.model.view;

import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
@Data
public class TeacherGroupVO {
    private Integer id;
    private String createAuthor;
    private String updateAuthor;
    private Date createTime;
    private Date updateTime;
    private Integer del;

    private Integer count;//小组人数
    private IssueSource status;//小组状态
    private String stateSets;//小组流转情况
    private Role role;//小组角色
    private Integer identityId;//主管
    private String identityName;

    /**
     * 对接小组
     */
    private TeacherGroupVO parallelism;

    private List<UserDescVO> userDescVOList;
}
