package cn.edu.zut.soft.graduate.core.query;

import cn.edu.zut.soft.basic.core.model.dao.Query;
import cn.edu.zut.soft.graduate.core.constant.GradeType;
import cn.edu.zut.soft.graduate.core.constant.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * <p>文件名称：GradeQuery.java</p>
 * <p>文件描述：</p>
 * <p>完成日期：17/1/2 下午10:17</p>
 *
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GradeQuery extends Query {

    private GradeType gradeType;

    private String key;

    private String value;

    private Role role;

    private Integer identityId;

    private Integer del;

    private List<String> valueList;
}
