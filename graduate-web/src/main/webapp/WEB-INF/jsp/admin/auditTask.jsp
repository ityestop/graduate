<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/11/24
  Time: 下午5:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld" %>
<html>
<head>
    <title>分配任务</title>
    <link rel="stylesheet" href="assets/plugins/select2/select2.css">
    <script type="text/javascript">
        $(function () {
            var $select = $("#selectAll");
            $select.change(function () {
                if ($select.is(":checked")) {
                    $("input[name = 'topId']").attr("checked", true);
                } else {
                    $("input[name = 'topId']").attr("checked", false);
                }
            });
        });

    </script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
            <li class="active">题目审核</li>
        </ol>
        <div class="page-header">
            <h3>题目审核管理</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="panel-body">
        <div class="form-group">
            <sf:form class="form-horizontal" action="./${sessionScope.pathCode}/auditTask.do"
                     method="post" commandName="selectTea" id="condition">
                <label class="col-sm-1 control-label"> 教师姓名 </label>

                <div class="col-sm-3">
                    <sf:select class="form-control search-select" path="teaId"
                               id="teaId">
                        <sf:option value="null" label="全部"/>
                        <sf:options items="${teaList}" itemValue="teaId"
                                    itemLabel="teaName"/>
                    </sf:select>
                </div>

                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
            </sf:form>
        </div>
        </div>
        
        <div class="panel-body">
        <div class="form-group">
          
            <form action="./${sessionScope.pathCode}/SubmitAuditTask.do"
                  method="post">
                <input type="hidden" name="teaId" value="${teaId}">
                <table class="table table-hover"
                       style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
                    <thead>
                    <tr>
                        <th><label> <input type="checkbox" id="selectAll">
                            <small>全选</small>
                        </label></th>
                        <th width="20%" class="center">
                            <small>题目名称</small>
                        </th>
                        <th>
                            <small>题目来源</small>
                        </th>
                        <th>
                            <small>题目类型</small>
                        </th>
                        <th>
                            <small>题目状态</small>
                        </th>
                        <th>
                            <small>校外教师</small>
                        </th>
                        <th>
                            <small>校内教师</small>
                        </th>
                        <th>
                            <small>题目人数</small>
                        </th>
                        <th width="15%">
                            <small>操作</small>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${topicList}" var="topic">
                        <tr>
                            <td><label> <input type="checkbox" name="topId"
                                               value="${topic.topId}"> </label></td>
                            <td
                                    style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
                                    title="${topic.topTitle}"><a
                                    href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topic.topId}"
                                    title="${topic.topTitle}"></small>${topic.topTitle}</a></td>
                            <c:choose>
                                <c:when test="${ topic.topTopicType eq false}">
                                    <td>
                                        <small>社会服务</small>
                                    </td>
                                </c:when>
                                <c:when test="${ topic.topTopicType eq true}">
                                    <td>
                                        <small>科研项目</small>
                                    </td>
                                </c:when>

                            </c:choose>

                            <td>
                                <small>${topic.topType}</small>
                            </td>
                            <td>
                                <small class="badge badge-warning">${topic.status.name}</small>
                            </td>

                            <c:choose>
                                <c:when test="${topic.topOutTeacher eq null }">
                                    <td>无</td>
                                </c:when>
                                <c:when test="${topic.topOutTeacher ne null }">
                                    <td>
                                        <small> ${ topic.topOutTeacher}</small>
                                    </td>
                                </c:when>
                            </c:choose>
                            <td>
                                <small>${topic.teacher.teaName }</small>
                            </td>
                            <td>
                                <small>${topic.topCount }</small>
                            </td>
                            <td>
                                <div class="visible-md visible-lg hidden-sm hidden-xs">
                                    <a
                                            href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topic.topId}">
                                        <i class="fa fa-info-circle"></i>
                                        <small>查看详情</small>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                 <div class="form-group">
<label class="col-sm-8 "> </label>
<div class="col-sm-4" style="text-align: right; height: 40px">
                <page:createPager pageSize="${pageSize}" totalPage="${totalPage}" totalCount="${totalCount}"
                                  curPage="${pageNum}" formId="condition"/>
            </div>


</div>
                <div class="from-group">
                    <label class="col-sm-3 control-label"></label>

                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-blue btn-block">
                                <i class="clip-checkmark-2  "></i> 分配
                            </button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-blue btn-block"
                                    onclick="location.href='./${sessionScope.pathCode}/auditAllocation.do'">
                                <i class="clip-arrow-right-2 "></i> 返回
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<!-- 4 -->
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function () {
        FormElements.init();

    });

</script>
</body>
</html>
