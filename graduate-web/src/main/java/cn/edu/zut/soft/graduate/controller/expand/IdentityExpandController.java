package cn.edu.zut.soft.graduate.controller.expand;

import cn.edu.zut.soft.graduate.controller.util.IdentityUtil;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author chuchuang
 * @date 16/11/24
 */
@RequestMapping("/expand/identity")
@Controller
public class IdentityExpandController {

    @Autowired
    private IdentityBO identityBO;

    @RequestMapping("list/{role}")
    @ResponseBody
    public String listByTea(IdentityQuery identityQuery, @PathVariable("role") Role role) {
        identityQuery.setRole(role);
        if (role == null){
            return "角色为空";
        }
        identityQuery.getCount().intValue();
        identityBO.userPersonal(identityQuery, IdentityUtil.findByRole(role));
        JSONObject json = new JSONObject();
        json.put("page",Math.ceil(((double)identityQuery.getCount())/identityQuery.getLimit()));
        json.put("total",identityQuery.getCount());

        return json.toJSONString();
    }

}
