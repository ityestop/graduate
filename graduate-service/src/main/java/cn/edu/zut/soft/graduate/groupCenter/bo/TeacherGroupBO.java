package cn.edu.zut.soft.graduate.groupCenter.bo;

import cn.edu.zut.soft.graduate.core.model.view.TeacherGroupVO;
import cn.edu.zut.soft.graduate.core.query.GroupQuery;

import java.util.List;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
public interface TeacherGroupBO {

    List<TeacherGroupVO> findByQuery(GroupQuery groupQuery);

    List<TeacherGroupVO> noParallelism();

    boolean parallelism(Integer groupId,Integer parallelismId,String operator);

    TeacherGroupVO get(Integer id);

    TeacherGroupVO getByTeacher(Integer id);

    boolean restartParallelism(Integer groupId,String operator);
}
