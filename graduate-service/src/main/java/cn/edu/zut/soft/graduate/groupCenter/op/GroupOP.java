package cn.edu.zut.soft.graduate.groupCenter.op;

import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;

/**
 * @author chuchuang
 * @date 16/11/20
 */
public interface GroupOP {

     Group OwnerCreateGroup(LoginVO loginVO, Identity identity, String operator);

     Group JoinGroup(LoginVO loginVO, Identity identity, Group group);

}
