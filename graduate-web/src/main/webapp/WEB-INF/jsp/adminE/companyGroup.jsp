<%--
  Created by IntelliJ IDEA.
  User: chuchuang
  Date: 16/11/13
  Time: 下午3:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>收件箱</title>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css"/>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">公司题目</li>
        </ol>
        <div class="page-header">
            <h4>公司题目小组审核</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id='table' class='table table-container'>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="assignTea" tabindex="-1" role="dialog" aria-labelledby="alterModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">分配指导教师</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="compangId"/>
                <table class="table table-container">
                    <tr>
                        <td width="10%">
                            <small>组长</small>
                        </td>
                        <td>
                            <small><label class="control-label" id="groupLeaderName">
                            </label></small>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">
                            <small>小组成员</small>
                        </td>
                        <td>
                            <small><label class="control-label" id="groupMember"></label></small>
                        </td>
                    </tr>
                    <tr id="guideTea_tr">
                        <td width="10%">
                            <small>指导教师</small>
                        </td>
                        <td>
                            <small id="guideTeaName"></small>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">
                            <small>申请理由</small>
                        </td>
                        <td>
                            <small><label class="control-label" id="companyContent"></label></small>
                        </td>
                    </tr>
                    <tr id="companyTeaList">
                        <td width="10%">
                            <small>教师列表</small>
                        </td>
                        <td>
                            <select class="form-control search-select" title="教师列表" name="identityId" id="teaId">
                                <option value="">请选择</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="assignTeaBtn">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="closeBtn">关闭
                </button>
            </div>
        </div>
    </div>
</div>

<script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
<!-- 3 -->
<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
<!-- 1 -->
<script src="assets/plugins/select2/select2.min.js"></script>
<!-- 2 -->
<script src="assets/js/form-elements.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript"
        src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/util/dataTable.js"></script>
<script type="application/javascript" src="assets/g/js/source.js"></script>
<script type="application/javascript">
    $(function () {
        var $teaList = '${teaList}';
        if ($teaList != null && $teaList != '') {
            $.each(JSON.parse($teaList), function (i, node) {
                $("select[name='identityId']").append("<option value='" + node.id + "' " + ('${param.identityId}' == node.id ? "selected" : "") + ">" + node.name + "</option>");
            });
        }
    });
</script>
<script type="application/javascript">
    $(function () {
        var columns = [{
            field: 'id',
            title: 'ID'
        }, {
            field: 'groupId',
            title: '小组ID',
            hidden: 'hidden'
        }, {
            field: 'identityName',
            title: '组长'
        },{
            field: 'phone',
            title: '联系方式'
        }, {
            field: 'groupNum',
            title: '小组人数'
        }, {
            field: 'status',
            title: "状态",
            formatter: function (value, row, index) {
                return value == "ING" ? "申请中" : (value == "YES" ? "同意" : (value == "NO" ? "拒绝" : ""));
            }
        }, {
            field: 'createTime',
            title: '创建时间',
            formatter: function (value, row, index) {
                return timeStampYYYYMMDDHHMM(value);
            }
        }, {
            title: '操作',
            formatter: function (value, row, index) {
                var str = "<a class='choose' href='javaScript:void(0)' data-toggle='modal' data-target='#assignTea'>查看详情</a>";
                return str;
            }
        }
        ];

        customSearch({id: "#table", url: "admin/companyList", dataType: "json", params: {orderByClause: 'update_time'}, columns: columns});

        $("#assignTea").on('show.bs.modal', function (e) {
            $("#assignTeaBtn").hide();
            $("#companyTeaList").hide();
            $("#guideTea_tr").hide();
            var $id = $.trim($(e.relatedTarget).parent().parent().children("td").get(0).innerHTML);
            var $groupId = $.trim($(e.relatedTarget).parent().parent().children("td").get(1).innerHTML);
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'admin/companyInfo',
                data: {
                    id: $id,
                    groupId: $groupId
                },
                success: function (result) {
                    if (result.success) {
                        var $data = result.data[0];
                        var $ideintId = $data.identityId;
                        $("#compangId").attr("value", $data.id);
                        $("#groupLeaderName").html($data.identityName);
                        $("#companyContent").html($data.content);
                        if ($data.status == 'ING') {
                            $("#companyTeaList").show();
                            $("#assignTeaBtn").show();
                        }
                        $.ajax({
                            type: 'post',
                            dataType: 'json',
                            url: 'identity/getGuideTea',
                            data: {
                                id: $ideintId,
                            },
                            success: function (result) {
                                if (result.success) {
                                    var $teaData = result.data[0];
                                    $("#guideTeaName").html($teaData.identityName);
                                    $("#guideTea_tr").show();
                                }
                            }
                        });

                    } else {
                        alert(result.errorMsg);
                    }
                }
            });
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'group/groupMember',
                data: {
                    groupId: $groupId
                },
                success: function (result) {
                    if (result.success) {
                        var $dataList = result.data;
                        var str = '';
                        $.each($dataList, function (key, value) {
                            str += value.identityName + '、';
                        });
                        $("#groupMember").html(str);
                    }
                }

            });

        });

        $("#assignTeaBtn").click(function () {
            var $id = $("#compangId").val();
            var $teaId = $("#teaId").val();
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'admin/assignCompanyTea',
                data: {
                    teaId: $teaId,
                    id: $id
                },
                success: function (result) {
                    if (result.success) {
                        window.location.reload();
                    } else {
                        alert(result.errorMsg);
                    }

                }
            });

        });
        $("#closeBtn").click(function () {
            $("#compangId").attr("value", '');
            $("#groupLeaderName").html('');
            $("#companyContent").html('');
            $("#groupMember").html('');
        });
    });
</script>
<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
</body>
</html>
