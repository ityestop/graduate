<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>题目详情</title>
    <script src="/assets/g/js/source.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="/admin"> 首页 </a></li>
            <li class="active">题目详情</li>
        </ol>
        <div class="page-header">
            <h4>题目详情</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->


<div class="row">
    <div class="panel-body">
        <table class="table" id="sample-table-1">
            <tbody>
            <tr>
                <td width="10%" class="center"><small>题目标题:</small></td>
                <td><small><span id="issueTitle"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>题目状态:</small></td>
                <td><small><font color="red"><span id="issueStatus"></span></font></small></td>
            </tr>
            <tr>
                <td class="center"><small>校内教师:</small></td>
                <td><small><span id="issueTeacher"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>课题来源:</small></td>
                <td><small><span id="issueSource"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>课题性质:</small></td>
                <td><small><span id="issueKind"></span></small></td>
            </tr>

            <tr>
                <td class="center"><small>课题类型:</small></td>
                <td><small><span id="issueType"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>课题人数:</small></td>
                <td><small><span id="issueTaskSize"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>技术领域:</small></td>
                <td><small><span id="issueTechnology"></span></small></td>
            </tr>
            <tr>
                <td class="center"><small>评语:</small></td>
                <td><font color="red"><small><span id="remarContent">无</span></small></font></td>
            </tr>
            <tr>
                <td class="center"><small>项目背景:</small></td>
                <td>
                    <small><pre id="issueBackground"></pre></small>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <small>
                    项目任务:
                    </small>
                </td>
                <td id="issueTaskList">
                    <small></small>
                </td>
            </tr>
            </tbody>
        </table>
            <div id="issueAuditIng">
                <label class="col-sm-6 control-label"></label>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-blue btn-block" id="submitIuessPass" >
                        <span><i class="clip-checkmark-2  "></i>   通过</span>
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-blue btn-block"
                            data-toggle="modal" data-target="#issueRemarkTopic">
                        <span><i class=" clip-cancel-circle  "></i>    不通过</span>
                    </button>
                </div>
            </div>
                <div class="col-sm-2" style="float: right">
                    <button type="button" class="btn btn-blue btn-block"
                            onclick="javascript:history.go(-1);location.reload()">
                        <span><i class=" clip-arrow-right-2  "></i> 返回</span>
                    </button>
                </div>



    </div>
</div>

<div class="modal fade" id="issueRemarkTopic" tabindex="-1" role="dialog"
     aria-labelledby="alterModalLabel">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">管理员批语</h4>
                </div>

                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-offset-1 control-label">题目名称:</label>
                            <label class="control-label" id="modelIssue"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-offset-1 control-label">批语:</label>
                            <div class="col-sm-8">
                                    <textarea name="remark" id="remark"
                                              style="width:100%;height:220px;"></textarea>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="remarkAndRefuse">提交</button>

                    <button type="button" class="btn btn-default" data-dismiss="modal" >关闭
                    </button>

                </div>

            </div>
    </div>
</div>



<script type="application/javascript">
    var $id = '${param.id}';

    var $issueSourceMap = JSON.parse('${typeList}');
    var $issueKindMap = JSON.parse('${kindList}');
    var $issueStatusMap = JSON.parse('${statusList}');

    function succ(result) {
        var data = result.data;
        $("#issueTitle").html(data.issue.title);
        $("#modelIssue").html(data.issue.title);
        $("#issueStatus").html($issueStatusMap[data.issue.status]);
        $("#issueTeacher").html(data.issue.schoolTeaName);
        $("#issueType").html(data.issue.type);
        $("#issueSource").html($issueSourceMap[data.issue.source]);
        $("#issueTaskSize").html(data.issue.taskSize);
        $("#issueKind").html($issueKindMap[data.issue.kind]);
        var $technology = $("#issueTechnology");
        $.each(JSON.parse(data.issue.technology), function (i, node) {
            $technology.append(node.title + "、");
        });
        $("#issueBackground").html(data.issueContent.background);
        var $issueTaskList = $("#issueTaskList");
        $.each(data.issueContent.taskList, function (i, node) {
            $issueTaskList.append("<h5>" + node.title + "</h5><pre>" + node.content + "</pre>");
        });
        $("#remarContent").html(data.issueContent.remark);
        var statusTop = data.issue.status;
        if(statusTop == 'auditIng'){
            $("#issueAuditIng").show();
        }else{
            $("#issueAuditIng").hide();
        }

    }

    function err(data) {
        alert(data.errorMsg);
    }

    if ($id != null) {
        $.getJSON("/issue/" + $id, function (result) {
            if (result.success) {
                succ(result);
            } else {
                err(result);
            }
        });
    }

    $("#submitIuessPass").click(function () {

        $.ajax({
            type:'post',
            url:'./issue/issueOperate/AuditSuccess/'+$id,
            dataType: "json",
            success:function (result) {
                if(result.success){
                    window.location.replace("./admin/issue/list/tea");
                }else{
                    alert(result.errorMsg);
                }
            }
        })

    });

    $("#remarkAndRefuse").click(function () {
       var remark = $("#remark").val();
        if(remark == null){
            alert("请填写拒绝通过评语!");
        }
        $.ajax({
            type:'post',
            url:'./issue/issueRemark/AuditError/'+$id,
            dataType: "json",
            data:{
                remark:remark,
            },
            success:function (result) {
                if(result.success){
                    window.location.reload(false);
                }else{
                    alert(result.errorMsg);
                }
            }
        })
    });


</script>

</body>
</html>
