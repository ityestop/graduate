package cn.edu.zut.soft.graduate.core.vo;

import lombok.Data;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class ChangePwdVO {
    private Integer id;
    private String old;
    private String now;
}
