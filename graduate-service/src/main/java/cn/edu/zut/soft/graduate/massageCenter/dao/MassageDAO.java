package cn.edu.zut.soft.graduate.massageCenter.dao;

import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.core.model.Impl.Message;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by chuchuang on 16/10/21.
 */
@Component
public interface MassageDAO<T extends Message> extends BasicDAO<Integer,T> {

    int updateByExampleWithBLOBs(@Param("record") T record, @Param("example") Example example);

    List<T> selectByExampleWithBLOBs(Example example);

    int updateByPrimaryKeyWithBLOBs(T record);

}
