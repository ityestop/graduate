package cn.edu.zut.soft.graduate.core.model.Impl;

import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import lombok.Data;

/**
 * @author chuchuang
 * @date 16/11/20
 */
@Data
public class CreateGroup {
    Role role;//角色
    Role.Runner runner;//角色类型,admin OR user
    LoginVO user;// 登录用户
    Integer owner; // 组长
}
