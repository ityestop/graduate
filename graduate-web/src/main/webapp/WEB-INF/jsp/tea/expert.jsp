<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>专家审核任务</title>
    <script src="/assets/g/js/tea/teaExpert.js"></script>
</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a
                    href="./tea"> 首页 </a></li>
            <li class="active">专家审核任务</li>
        </ol>
        <div class="page-header">
            <h4>专家审核任务</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="panel-body">
        <div class="form-group">
            <input type="hidden" value="${sessionScope.user.id}" id="userId_audit" />
            <table class="table table-hover"
                   style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all" id="table">
                <thead>
                <tr>
                    <th width="35%" class="center">
                        <small>题目名称</small>
                    </th>
                    <th>
                        <small>题目来源</small>
                    </th>
                    <th>
                        <small>指导教师</small>
                    </th>
                    <th>
                        <small>题目状态</small>
                    </th>
                    <th>
                        <small>题目人数</small>
                    </th>
                </tr>
                </thead>
                <%--<tbody id="auditIssuseTop">--%>
                <%--</tbody>--%>
            </table>
        </div>
    </div>
</div>
</body>
</html>
