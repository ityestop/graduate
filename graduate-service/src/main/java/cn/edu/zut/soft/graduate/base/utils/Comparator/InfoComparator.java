package cn.edu.zut.soft.graduate.base.utils.Comparator;

import cn.edu.zut.soft.graduate.core.constant.InfoTypeAble;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;

import java.util.Comparator;
import java.util.List;

/**
 * @author cc
 * @Date 2017/1/5
 * @value 1.0
 */
public class InfoComparator implements Comparator<UserDescVO> {

    private Info info;

    public InfoComparator(Info info) {
        this.info = info;
    }

    public InfoComparator(InfoTypeAble infoType, String key){
        info = new Info();
        info.setType(infoType.name());
        info.setKey(key);
    }

    @Override
    public int compare(UserDescVO o1, UserDescVO o2) {
        Info ui1 = getInfo(o1.getInfoList());
        Info ui2 = getInfo(o2.getInfoList());

        if (ui1 == null){
            return -1;
        }
        if (ui2 == null){
            return 1;
        }
        return ui1.getValue().compareTo(ui2.getValue());
    }

    private boolean match(Info i) {
        return info.getType().equals(i.getType()) && info.getKey().equals(i.getKey());
    }

    private Info getInfo(List<Info> infoList) {
        for (Info i : infoList) {
            if (match(i)) {
                return i;
            }
        }
        return null;
    }
}
