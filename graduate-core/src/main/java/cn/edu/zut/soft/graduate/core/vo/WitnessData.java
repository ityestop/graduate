package cn.edu.zut.soft.graduate.core.vo;

import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by chuchuang on 16/10/23.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WitnessData extends Witness{

    private Info info;
    private String phone;
    private int okNum;
    private int allNum;
    private int smallOkNum;
    private int smallAllNum;

    /**
     * 设定非通过数,  为负数
     * @param noOkNum
     */
    public void setNoOkNum(int noOkNum){
        this.okNum = allNum+noOkNum;
    }
    /**
     * 设定非通过数,  为正数
     * @param smallNoOkNum
     */
    public void setSmallNoOkNum(int smallNoOkNum){
        this.smallOkNum = smallAllNum-smallNoOkNum;
    }
}
