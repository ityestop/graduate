package cn.edu.zut.soft.graduate.core.design;

import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.NameString;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chuchuang on 16/10/23.
 */
public enum ProfessionalLever implements InfoStrategy, NameString {

    DEFAULT("未指定"), ASSISTANT("助教"), LECTURER("讲师"), ASSISTANT_PROFESSOR("副教授"), PROFESSOR("教授");
    private String name;

    ProfessionalLever(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return name;
    }

    @Override
    public String toJson() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("infoType",getKey());
        jsonObject.put("key",this);
        jsonObject.put("value",getValue());
        return jsonObject.toJSONString();
    }

    @Override
    public InfoType getType() {
        return InfoType.Basic;
    }

    @Override
    public String getKey() {
        return "professionalLever";
    }

    private static Map<String, String> map = new HashMap<String, String>();

    public static Map getMap() {
        for (ProfessionalLever professionalLever : ProfessionalLever.values()) {
            map.put(professionalLever.name(), professionalLever.getValue());
        }
        return map;
    }



}
