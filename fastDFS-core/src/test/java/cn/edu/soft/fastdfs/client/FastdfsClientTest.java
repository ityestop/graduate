package cn.edu.soft.fastdfs.client;

import cn.edu.soft.fastdfs.FastdfsClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/spring-fastdfs.xml")
public class FastdfsClientTest {

    @Resource
    private FastdfsClient fastdfsClient;

    @Test
    public void testFastdfsClient() throws Exception {
        URL fileUrl = this.getClass().getResource("/Koala.jpg");
        File file = new File(fileUrl.getPath());
        String fileId = fastdfsClient.upload(file);
        System.out.println("fileId:" + fileId);
        assertNotNull(fileId);
        String url = fastdfsClient.getUrl(fileId);
        assertNotNull(url);
        System.out.println("url:" + url);
        Map<String, String> meta = new HashMap<String, String>();
        meta.put("fileName", file.getName());
        boolean result = fastdfsClient.setMeta(fileId, meta);
        assertTrue(result);
        Map<String, String> meta2 = fastdfsClient.getMeta(fileId);
        assertNotNull(meta2);
        System.out.println(meta2.get("fileName"));
        result = fastdfsClient.delete(fileId);
        assertTrue(result);
        fastdfsClient.close();
    }


}
