package cn.edu.zut.soft.graduate.core.constant;

/**
 * 基本状态
 * @author chuchuang
 * @date 16/11/27
 */
public enum DeclareStatus {
    ING,YES,NO
}
