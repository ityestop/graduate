package cn.edu.zut.soft.graduate.topicCenter.op;

import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import cn.edu.zut.soft.graduate.topicCenter.handler.IssueStatusChangeHandler;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author cc
 * @Date 2016/11/21
 * @value 1.0
 */
@Data
public class IssueOPImpl implements IssueOP {

    @Autowired
    private IssueBO issueBO;

    @Autowired
    private List<IssueStatusChangeHandler> statusChangeHandlerList;

    @Override
    @Transactional
    public Issue changeIssuePhase(LoginVO loginVO, Integer id, IssuePhase.Handle handle) {
        return issueBO.changeIssuePhase(loginVO, id, handle);
    }
}
