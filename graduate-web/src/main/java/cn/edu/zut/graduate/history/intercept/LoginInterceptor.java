//package cn.edu.zut.graduate.intercept;
//
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.soft.graduate.core.model.Student;
//import cn.edu.zut.soft.graduate.core.model.Teacher;
//import org.springframework.stereotype.Repository;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * Created by sky on 15/11/7.
// */
//@Repository
//public class LoginInterceptor implements HandlerInterceptor {
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        Role role = (Role) request.getSession().getAttribute("role");
//        if (role == null){
//            response.sendRedirect(request.getContextPath());
//            return false;
//        }
//        if (role.getIndex() == Role.TEA.getIndex()){
//            Teacher teacher = (Teacher) request.getSession().getAttribute("user");
//            if (teacher.getTeaTell() == null || teacher.getTeaTell().trim().equals("")){
//                request.getRequestDispatcher("./teaInfo.do").forward(request,response);
//                return false;
//        }
//        }
//        else if(role.getIndex() ==Role.STU.getIndex()){
//        	Student student =(Student)request.getSession().getAttribute("user");
//        	if(student.getStuTell() == null || student.getStuTell().equals("")){
//        		request.getRequestDispatcher("./stuInfo.do").forward(request, response);
//        		return false;
//        	}else if(student.getInterimGrade() == -1){
//        		request.getRequestDispatcher("./home.do").forward(request, response);
//        		return false;
//        	}else if(student.getCheckGrade() == -1){
//        		request.getRequestDispatcher("./home.do").forward(request, response);
//        		return false;
//        	}
//        }
////        response.sendRedirect(request.getContextPath());
//        return true;
//    }
//
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//
//    }
//}
