//package cn.edu.zut.graduate.controller;
//
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import cn.edu.zut.soft.graduate.core.constant.Role;
//import cn.edu.zut.graduate.entity.tools.WebResult;
//import cn.edu.zut.graduate.supervisor.ConfigDo;
//import cn.edu.zut.graduate.supervisor.ServiceFit;
//
//import com.github.abel533.echarts.Option;
//
//
///**
// * 数据统计
// * @author yxt
// *
// */
//@Controller
//@RequestMapping("/admin/")
//public class DataStatisticsController implements ConfigDo {
//	private Role r = Role.ADMIN;
//
//	@Resource
//	private ServiceFit serviceFit;
//
//	@RequestMapping("toStaticsByTeacher")
//	public String toStaticsByTeacher(Model model){
//		model.addAttribute("menuSelected1", ConfigDo.ADMIN_COUNT);
//		model.addAttribute("menuSelected2", ConfigDo.ADMIN_COUNT_SHANBAO);
//		return "./echarts/staticsByTeacher";
//	}
//	@RequestMapping("toStaticByTeacher")
//	public @ResponseBody WebResult staticsByTeacher(Model model){
////		model.addAttribute("menuSelected1", ADMIN_COUNT);
////		model.addAttribute("menuSelected2", attributeValue)
//		WebResult result =new WebResult();
//		try{
//		List<Map<String ,Object>> list= serviceFit.getDataStatisticsService().analysisGroupByTeacher();
//		Option option=serviceFit.getDataStatisticsService().createOption(list, "教师申报情况", "申报数量(个)","学生人数", "teaName", "number","student");
//		result.isOK();
//		result.setData(option);
//		result.setTableData(list);
//		}catch(Exception e){
//			result.setException(e);
//		}
//		return result;
//	}
//}
