<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>邀请小组成员</title>
    <!-- Latest compiled and minified CSS -->

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/stu"> 首页 </a></li>
            <li class="active">邀请小组成员</li>
        </ol>
        <div class="page-header">
            <h4>邀请小组成员</h4>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="./stu/noGroupList" method="post">
                <div class="form-group">
                    <input class="form-control" style="width: 300px" title="学生姓名" placeholder="学生姓名" name="name"/>
                </div>
                <div class="form-group" style="text-align: right;">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="teacherTable" class="table table-container">
                <thead>
                <tr>
                    <th>
                        学号
                    </th>
                    <th>
                        姓名
                    </th>
                    <th>
                        性别
                    </th>
                    <%--<th>--%>
                        <%--班级--%>
                    <%--</th>--%>
                    <th>
                        联系方式
                    </th>
                    <th>
                        操作
                    </th>
                </tr>
                </thead>
                <tbody id="tbody">
                <c:forEach items="${stuList}" var="stu">
                    <tr>
                        <td><small>${stu.identity.noId}</small></td>
                        <td><small>${stu.identity.name}</small></td>
                        <c:if test="${stu.identity.sex eq -1}">
                            <td><small>女</small></td>
                        </c:if>
                        <c:if test="${stu.identity.sex eq 1}">
                            <td><small>男</small></td>
                        </c:if>
                        <c:if test="${stu.identity.sex eq 0}">
                            <td><small></small></td>
                        </c:if>
                        <%--<td><small>${stu.infoList.get(0).value}</small></td>--%>
                        <td><small>${stu.identity.phone}</small></td>
                        <td><small>
                            <a href="./stu/invite?userId=${stu.identity.id}">邀请</a>
                        </small></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div id="page-nav"></div>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>
</body>
</html>
