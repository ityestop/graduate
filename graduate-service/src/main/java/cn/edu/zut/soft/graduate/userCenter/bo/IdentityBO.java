package cn.edu.zut.soft.graduate.userCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.base.bo.BasicBO;
import cn.edu.zut.soft.graduate.core.constant.config.TopicPhaseAble;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.query.IdentityQuery;
import cn.edu.zut.soft.graduate.core.query.InfoQuery;
import cn.edu.zut.soft.graduate.core.vo.ChangePwdVO;
import cn.edu.zut.soft.graduate.core.vo.LoginVO;
import cn.edu.zut.soft.graduate.core.vo.UserDescVO;
import cn.edu.zut.soft.graduate.core.vo.Witness;
import cn.edu.zut.soft.graduate.userCenter.handler.QueryHandler;
import cn.edu.zut.soft.graduate.userCenter.opjo.Invite;

import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
public interface IdentityBO extends BasicBO<Identity, IdentityQuery> {


    /**
     * 身份验证,
     *
     * @param identity 手机号,学号,工号,邮箱
     * @param pwd      密码,
     * @return
     */
    LoginVO identification(String identity, String pwd);

    /**
     * 获取详情页信息
     *
     * @param id
     * @return
     */
    UserDescVO userPersonal(Integer id);


    /**
     * 根据检索条件,检索用户的信息
     * @param identityQuery
     * @param infoStrategyList
     * @return
     */
    List<UserDescVO> userPersonal(IdentityQuery identityQuery, List<InfoStrategy> infoStrategyList);

    List<UserDescVO> userPersonal(IdentityQuery identityQuery);

    /**
     * 保存用户的详情信息
     * 不提供方法,不好实现,所有的信息都进行单个保存
     * @param userDescVO
     * @return
     */
//    CommonResult<UserDescVO> updateUserDesc(UserDescVO userDescVO,String operator);

    /**
     * 修改密码
     *
     * @param changePwdVO
     * @param operator
     * @return
     */
    CommonResult<Identity> alterPwd(ChangePwdVO changePwdVO, String operator);

    /**
     * 重置密码
     * @param id
     * @param operator
     * @return
     */
    Identity resetPwd(Integer id, String operator);

    /**
     * 分配专家身份
     */
    Witness awardWitness(Integer id, InfoStrategy infoStrategy, String operator);

    /**
     * 根据专家组集合查询出不是专家身份教师
     *
     * @param noWitnes
     * @return
     */
    CommonResult<List<Integer>> notWitnessTea(List<Witness> noWitnes);

    /**
     * 根据操作者,与小组id,检索小组成员
     * @param loginVO
     * @param groupId
     * @return
     */
    List<UserDescVO> findGroupMember(LoginVO loginVO,Integer groupId);

    /**
     * 反转小组查询
     * @param loginVO
     * @param groupId
     * @return
     */
    List<UserDescVO> findReverseGroupMember(LoginVO loginVO,Integer groupId,boolean reverse);

    /**
     * 查找能够邀请的用户
     * @param invite
     * @param queryHandler
     * @return
     */
    List<UserDescVO> findCanInvitationUser(Invite invite, QueryHandler queryHandler);

    /**
     * 根据infoquery 检索出符合条件的成员
     * @param infoQuery
     * @return
     */
    CommonResult<List<UserDescVO>>  findByInfo(InfoQuery infoQuery);


    /**
     * 校外的学生数据
     * @param identityQuery
     * @return
     */
    CommonResult<List<UserDescVO>>  queryByOutSchool(IdentityQuery identityQuery);

    /**
     * 检索阶段学生数据
     * @return
     */
    CommonResult<List<UserDescVO>> queryOrByGrade(TopicPhaseAble able, IdentityQuery identityQuery, boolean is);


    /**
     * 需要进行二次答辩的学生数据
     * @param identityQuery
     * @return
     */
    CommonResult<List<UserDescVO>> queryByTwoOpenGrade(IdentityQuery identityQuery);

    /**
     * <p>功能描述：获取无开题成绩的学生</p>
     * <p>创建人：yangxiaotian</p>
     * <p>创建日期：2017-05-18 21:36:34</p>
     *
     * @param identityQuery
     * @return
     */
    CommonResult<List<UserDescVO>> getOpenGradeNull(IdentityQuery identityQuery);

    UserDescVO getUserDesc(Integer id);

    /**
     * 开题校内学生数据
     * @param identityQuery
     * @return
     */
    CommonResult<List<UserDescVO>> queryBySchool(IdentityQuery identityQuery);

    /**
     * 检索所有教师
     * @param identityQuery 检索条件
     * @return 教师的集合
     */
    CommonResult<List<Identity>> queryAllTea(IdentityQuery identityQuery);


}
