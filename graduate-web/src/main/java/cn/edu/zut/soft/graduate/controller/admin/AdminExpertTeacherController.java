package cn.edu.zut.soft.graduate.controller.admin;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.graduate.core.constant.*;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.WitnessStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.AuditIssueLink;
import cn.edu.zut.soft.graduate.core.model.Impl.Identity;
import cn.edu.zut.soft.graduate.core.model.Impl.Issue;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.*;
import cn.edu.zut.soft.graduate.log.IssueSearchLogServiceImpl;
import cn.edu.zut.soft.graduate.topicCenter.bo.AuditIssueLinkBO;
import cn.edu.zut.soft.graduate.topicCenter.bo.IssueBO;
import cn.edu.zut.soft.graduate.topicCenter.pojo.IssueAndIdentity;
import cn.edu.zut.soft.graduate.topicCenter.pojo.IssueTakeWitness;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.bo.WitnessBO;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>文件名称：AdminExpertTeacherController.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：16/10/26 上午12:45</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
@RequestMapping("/admin")
@Controller
public class AdminExpertTeacherController {
    @Autowired
    private IdentityBO identityBO;

    @Autowired
    private InfoBO infoBO;

    @Autowired
    private WitnessBO witnessBO;

    @Autowired
    private IssueBO issueBO;

    @Autowired
    private AuditIssueLinkBO auditIssueLinkBO;


    @RequestMapping("/toexpertTea")
    public ModelAndView ExperTeaList(HttpServletRequest request, Map<String,Object> model){
        CommonResult<List<WitnessData>> result = witnessBO.getAllWitnessData();
        if (result.isSuccess()){
            model.put("witness",result.getData());
        }
        return new ModelAndView("/adminE/ExpertTea",model);
    }

    @ResponseBody
    @RequestMapping("/noExTeacherList")
    public CommonResult NotExpertTeacher(){
        CommonResult<List<TeacherVO>> result = infoBO.notWitenssTeaMessage();
        if (result.isSuccess()){
            return CommonResult.successReturn(result.getData());
        }
        return CommonResult.errorReturn("已无普通身份教师");
    }


    @RequestMapping("/updateExpertTea")
    public ModelAndView UpdateExpertTeacher(HttpServletRequest request, @RequestParam("userId") String[] userId){
        LoginVO  loginVO = (LoginVO) request.getSession().getAttribute("user");
        if (userId == null){
            return new ModelAndView("redirect:/admin/toexpertTea");
        }
        System.out.println(userId.length);
        List<Witness> result = witnessBO.saveWitess(userId,loginVO.getName());
        return new ModelAndView("redirect:/admin/toexpertTea");
    }

    @RequestMapping(value = "/toAuditIssueList")
    public ModelAndView toAuditIssueList(Map<String, Object> model,@RequestParam("userId") String userId, @RequestParam("userName") String userName){
        if (StringUtils.isNotBlank(userId) && StringUtils.isNotBlank(userName)){
            model.put("userId_audit", userId);
            model.put("userName_audit", userName);
        }
        return new ModelAndView("/adminE/auditIssue",model);
    }

    @ResponseBody
    @RequestMapping("/auditIssueList")
    public CommonResult expertIssueList(@RequestParam(value = "role",required = false)String role){
        List<Issue> result = issueBO.noExpertIssues(role);
        return CommonResult.successReturn(result);
    }

    @RequestMapping("/edit_aduit")
    public ModelAndView editAduitIssue(HttpServletRequest request,@RequestParam("userId") Integer userId, @RequestParam("issueId") Integer issueId,@RequestParam("userName")String userName,@RequestParam(value = "del",required = false) Integer del, Map<String, Object> model){
        LoginVO seesion = (LoginVO) request.getSession().getAttribute("user");
        if (userId == null && issueId == null){
            return new ModelAndView("redirect:/admin/toAuditIssueList?userId="+userId+"&"+"userName="+userName);
        }
        AuditIssueLink auditIssueLink = new AuditIssueLink();
        auditIssueLink.setIdentityId(userId);
        auditIssueLink.setIssueId(issueId);
        auditIssueLink.setDel(0);
        CommonResult<AuditIssueLink> result = auditIssueLinkBO.save(auditIssueLink, seesion.getName());
        return new ModelAndView("redirect:/admin/toexpertTea",model);
    }

    @RequestMapping("issue_userId")
    public ModelAndView issueUserId(@RequestParam("userId") Integer userId, Map<String, Object> model){
        if (userId != null){
            model.put("userId",userId);
        }
        return new ModelAndView("/adminE/editAuditIssue",model);
    }

    @RequestMapping("delete_audit")
    public ModelAndView deleteAudit(HttpServletRequest request,@RequestParam("userId") Integer userId, @RequestParam("issueId") Integer issueId, Map<String,Object> model){
        LoginVO session = (LoginVO)request.getSession().getAttribute("user");
        if (userId == null && issueId == null){
            return new ModelAndView("redirect:/admin/issue_userId?userId="+userId,model);
        }
        AuditIssueLink auditIssueLink = auditIssueLinkBO.getByissueAndUser(userId, issueId);
        if (auditIssueLink != null){
            CommonResult<AuditIssueLink> result = auditIssueLinkBO.delete(auditIssueLink.getId(), session.getName());
            if (result.isSuccess()){
                return new ModelAndView("redirect:/admin/toexpertTea",model);
            }
        }
        return new ModelAndView("redirect:/admin/issue_userId?userId="+userId,model);
    }




    @RequestMapping("/toexpertIssue")
    public ModelAndView ExperIssueList(HttpServletRequest request, Map<String,Object> model){
        CommonResult<List<WitnessData>> result = witnessBO.getAllWitnessData();
        if (result.isSuccess()){
            model.put("witness",result.getData());
        }
        return new ModelAndView("/adminE/audtiAllIssue",model);
    }

    @RequestMapping("/toexpertIssue/auditIssue")
    @ResponseBody
    public CommonResult findAuditIssue(IssueQuery issueQuery, @PathVariable(value = "role", required = false) Role role) {
        //issueQuery.setIdentityRole(role);    //不起作用
        issueQuery.setIssuePhase(IssuePhase.auditIng);
        IssueAndIdentity iAI = null;
        Identity identity = null;
        AuditIssueLink aIL = null;
        List<IssueAndIdentity> list = new ArrayList<>();
        for (Issue issue : (List<Issue>) issueBO.findByQueryWith(issueQuery).getData()) {
            iAI = new IssueAndIdentity();
            iAI.setIssue(issue);
            aIL = auditIssueLinkBO.getAuditIssueLink(issue.getId());
            if(aIL == null) {
                list.add(iAI);
                continue;
            }
            identity = identityBO.userPersonal(aIL.getIdentityId()).getIdentity();
            iAI.setIdentity(identity != null ? identity : null);
            list.add(iAI);
        }
        return CommonResult.successReturn(list);
    }

    @RequestMapping("/edit_aduit_new")
    public ModelAndView editAduitIssue_new(HttpServletRequest request,@RequestParam("userId") Integer userId, @RequestParam("issueId") Integer issueId,@RequestParam("userName")String userName,@RequestParam(value = "del",required = false) Integer del, Map<String, Object> model){
        LoginVO seesion = (LoginVO) request.getSession().getAttribute("user");
        if (userId == null && issueId == null){
            return new ModelAndView("redirect:/admin/toexpertIssue");
        }
        AuditIssueLink auditIssueLink = new AuditIssueLink();
        auditIssueLink.setIdentityId(userId);
        auditIssueLink.setIssueId(issueId);
        auditIssueLink.setDel(0);
        CommonResult<AuditIssueLink> result = auditIssueLinkBO.save(auditIssueLink, seesion.getName());
        return new ModelAndView("redirect:/admin/toexpertIssue",model);
    }

    @RequestMapping("issue_userId_new")
    public ModelAndView issueUserId_new(@RequestParam("userId") Integer userId, Map<String, Object> model){
        if (userId != null){
            model.put("userId",userId);
        }
        return new ModelAndView("/adminE/audtiAllIssue",model);
    }

    @RequestMapping("delete_audit_new")
    public ModelAndView deleteAudit_new(HttpServletRequest request,@RequestParam("userId") Integer userId, @RequestParam("issueId") Integer issueId, Map<String,Object> model){
        LoginVO session = (LoginVO)request.getSession().getAttribute("user");
        if (userId == null && issueId == null){
            return new ModelAndView("redirect:/admin/toexpertIssue",model);
        }
        AuditIssueLink auditIssueLink = auditIssueLinkBO.getByissueAndUser(userId, issueId);
        if (auditIssueLink != null){
            CommonResult<AuditIssueLink> result = auditIssueLinkBO.delete(auditIssueLink.getId(), session.getName());
            if (result.isSuccess()){
                return new ModelAndView("redirect:/admin/toexpertIssue",model);
            }
        }
        return new ModelAndView("redirect:/admin/toexpertIssue");
    }


}
