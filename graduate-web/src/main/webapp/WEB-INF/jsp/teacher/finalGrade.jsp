<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>

<title>答辩成绩</title>
<link rel="stylesheet" href="assets/plugins/select2/select2.css">
<script src="IAssets/js/selectCheckbox2.js"></script>
</head>
<body>
	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">答辩成绩</li>
			</ol>
			<div class="page-header">
				<h3>
					答辩成绩<small><font color="red">仅组长和秘书有权限给学生成绩</font> </small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<div class="row">
		<div class="panel-body">
							<table class="table  table-hover"
								style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
								<thead>
									<tr>
										<th width="5%"><small>编号</small>
										</th>
										<th><small>学号</small>
										</th>
										<th><small>姓名</small>
										</th>
										<th><small>指导教师</small>
										</th>
										<th><small>验收成绩</small>
										</th>
										<th><small>指导成绩</small>
										</th>
										<th><small>评阅成绩</small>
										</th>
										<th><small>答辩成绩</small>
										</th>
										<th><small>最终成绩</small>
										</th>
										<th width="12%"><small>操作</small>
										</th>
									</tr>
								</thead>
								<c:forEach items="${stulist}" var="t" varStatus="i">
									<tr>
									 	<input type="hidden" class="stuId" value="${t.stuId}">
									 	<input type="hidden" class="don" value="${t.donGrade}">
									 	<input type="hidden" class="stuName" value="${t.stuName}">
										<td><small>${i.index + 1 }</small></td>
										<td><small>${t.stuStudentId}</small></td>
										<td><small>${t.stuName }</small></td>
										<td><small>${t.teaName}</small>
										</td>
										<c:choose>
										<c:when test="${t.checkGrade == 0}" >
											<td><small>未操作</small></td>
										</c:when>
										<c:when test="${t.checkGrade == 1}">
										<td><small>进入一辩</small></td></c:when>
										<c:when test="${t.checkGrade == 2}">
										<td><small>二次验收</small></td></c:when>
										<c:when test="${t.checkGrade == 3}">
										<td><small>进入二辩</small></td></c:when>
										<c:when test="${t.checkGrade == -1}">
										<td><small>终止答辩</small></td></c:when>
										</c:choose>
										<td><small>
										<fmt:formatNumber value="${t.donGrade}" pattern="0" />
										</small>
										</td>
										<td><small>
										<fmt:formatNumber value="${t.documentGrade}" pattern="0" />
										</small>
										</td>
										<td><small>
										<fmt:formatNumber value="${t.replyGrade}" pattern="0" />
										</small>
										</td>
										<td><small>
										<fmt:formatNumber value="${t.finalReplyGrade}" pattern="0" />
										</small>
										</td>
										<c:if test="${sessionScope.user.teaIdentity > 0 }">
										<td>
										<c:choose>
											<c:when test="${t.finalReplyGrade eq 0 and t.documentGrade eq 0 and t.replyGrade eq 0 }">
											<c:if test="${t.checkGrade eq 1}">
											<small><a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#finalGrade">给予成绩</a></small>
											<small><a href="./${sessionScope.pathCode}/updateCheckGradeOne.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}&status= 3">进入二辩</a></small>
											</c:if>
											<c:if test="${t.checkGrade eq 3}">
											<small>
												<a href="javaScript:void(0)" class="choose" data-toggle="modal" data-target="#finalGrade">给予成绩</a>
												<a href="./${sessionScope.pathCode}/updateCheckGradeOne.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}&status= -1">终止答辩</a>
														
											</small>
											</c:if>
											<c:if test="${t.checkGrade eq 2}">
											<small>二次验收</small>
											</c:if>
											<c:if test="${t.checkGrade eq -1 }">
											<small><a class="label label-danger" href="javaScript:void(0);">终止答辩</a></small>
											</c:if>
											</c:when>
											<c:when test="${t.finalReplyGrade > 0}">
											<small><a class="  label label-success  " href="javaScript:void(0);">答辩结束</a></small>
											</c:when>
											<c:when test="${t.finalReplyGrade eq 0 and t.documentGrade > 0 and t.replyGrade > 0}">
											<small><a href="./${sessionScope.pathCode}/restGrade.do?stuId=${t.stuId}&tgId=${sessionScope.doucking}"><i class="clip-rotate "></i> 重置成绩</a></small>
											</c:when>
										</c:choose>
										</td>
										</c:if>
										<c:if test= "${sessionScope.user.teaIdentity  eq 0 }">
										<td><small>无权限操作</small></td>
										</c:if>
									</tr>
								</c:forEach>
							</table>
					</div>
	</div>
<div class="modal fade" id="finalGrade" tabindex="-1"
     role="dialog" aria-labelledby="alterModalLabel">
    <form action="./${sessionScope.pathCode}/finalGrade.do"
          class="form-horizontal" id="Fgrade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="alterModalLabel">答辩成绩</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="stuId">
                    <input type="hidden" name="donGrade" id="donGradeF">
                    <input type="hidden" name="tgId" value="${sessionScope.doucking}">
                    <div class="form-group">
                    	<font color='red'>	 注意：答辩成绩与指导成绩是同一阶段成绩，例:答辩成绩80、指导成绩应在80或80以上 <br/>指导成绩与答辩成绩上下浮动3分，望给予成绩时慎重！<br/>若出现界面缓存问题导致提交按钮无响应请刷新,请谅解！</font><br/>
                    	<div id="stuNANE">ss</div>
                    	评阅成绩：<input type="text" name="documentGrade">
                    	答辩成绩：<input type="text" name="finalReplyGrade" id="finalGradeF">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                    </button>
                    <button type="submit" class="btn btn-primary" id="send">确认</button>
                </div>
            </div>
        </div>
    </form>
</div> 
<script>
    $('#finalGrade').on('show.bs.modal', function (e) {
        var $p = $(e.relatedTarget).parent().parent().parent();
        var $stuId = $p.find(".stuId");
        var $don = $p.find(".don");
        var $stuName = $p.find(".stuName");
        var $modal = $(this);
        var str="学生姓名："+$stuName.val()+"<br/><br/>"+"指导成绩："+$don.val()+"<br/><br/>";
        $modal.find("input[name='stuId']").val($stuId.val());
        $modal.find("input[name='donGrade']").val($don.val());
        $modal.find("#stuNANE").html(str);
    });
</script>
<script>
	$(function(){
			  $("#send").click(function(){
				 	var don=document.getElementById("donGradeF").value;
					var fGra=document.getElementById("finalGradeF").value;
					var don1=parseInt((don-0)/10);
					var fGra1=parseInt((fGra-0)/10);
					var bbb=Math.abs((don-0)-(fGra));
					  if(don1 != 0){
					  if(don1 != fGra1){
						  alert("指导成绩要与答辩成绩在同一阶段，例:指导成绩80 答辩成绩应该在80以上！");
					  	$("#Fgrade").submit( function () {
					  	  return false;
					  	} );
					  }
					  if(bbb >=4){
						  alert("指导成绩应与答辩成绩上下浮动不要超过3分！");
						  $("#Fgrade").submit( function () {
							  	  return false;
							  	} );
					  }
					  if(bbb <4){
						  $("#Fgrade").submit();
					  }
					  }else{
						  $("#Fgrade").submit();
					  }
	         });
		
	});
</script>
	<script
		src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
	<!-- 3 -->
	<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
	<!-- 1 -->
	<script src="assets/plugins/select2/select2.min.js"></script>
	<!-- 2 -->
	<script src="assets/js/form-elements.js"></script>
	<!-- 4 -->
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>

</body>
</html>
