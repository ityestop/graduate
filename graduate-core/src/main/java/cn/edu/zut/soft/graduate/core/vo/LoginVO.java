package cn.edu.zut.soft.graduate.core.vo;

import cn.edu.zut.soft.graduate.core.constant.LoginMode;
import cn.edu.zut.soft.graduate.core.model.Impl.Info;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by chuchuang on 16/10/22.
 */
@Data
public class LoginVO {

    private Integer id;

    private String name;

    private String role;

    private boolean expert;

    private String phone;

    //登录方式
    private LoginMode loginMode;

    private List<Info> infoList;
}
