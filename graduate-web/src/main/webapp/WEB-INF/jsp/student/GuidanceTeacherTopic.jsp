<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/11/22
  Time: 下午10:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="page" uri="/WEB-INF/pager.tld"%>
<html>
<head>
<title>软件学院毕业设计管理平台</title>
</head>
<body>

	<!-- start: PAGE HEADER -->
	<div class="row">
		<div class="col-sm-12">
			<!-- start: PAGE TITLE & BREADCRUMB -->
			<ol class="breadcrumb">
				<li><i class="clip-home-3"></i> <a
					href="./${sessionScope.pathCode}/home.do"> 首页 </a></li>
				<li class="active">校内题目</li>
			</ol>
			<div class="page-header">
				<h3>
					<strong>校内题目</strong> <small><font color="red">已分配指导教师的同学，只可报该指导教师题目</font>
					</small>
				</h3>
			</div>
			<!-- end: PAGE TITLE & BREADCRUMB -->
		</div>
	</div>
	<!-- end: PAGE HEADER 頭部結束-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel-body">
				<div class="row"></div>
				<div class="panel-body">
					<table class="table table-hover"
						style="TABLE-LAYOUT:fixed;WORD-BREAK:break-all">
						<thead>
							<tr>
								<th width="20%" class="center"><small>题目名称</small>
								</th>
								<th><small>题目来源</small>
								</th>
								<th><small>题目类型</small>
								</th>
								<th><small>题目状态</small>
								</th>
								<th><small>校内教师</small>
								</th>
								<th><small>联系方式</small>
								</th>
								<th><small>题目人数</small>
								</th>
								<th width="15%"><small>操作</small>
								</th>
							</tr>
						</thead>
						<c:forEach items="${topicList}" var="topic">
							<tbody>
								<tr>
									<td
										style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"
										title="${topic.topTitle}"><a
										href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topic.topId}"
										title="${topic.topTitle}"></small>${topic.topTitle}</a>
									</td>
									<c:choose>
										<c:when test="${ topic.topTopicType eq false}">
											<td><small>社会服务</small>
											</td>
										</c:when>
										<c:when test="${ topic.topTopicType eq true}">
											<td><small>科研项目</small>
											</td>
										</c:when>

									</c:choose>

									<td><small>${topic.topType}</small>
									</td>
									<c:choose>
										<c:when test="${topic.topStatus eq 0 }">
											<td><small class="label label-danger "
												style="font-size: 75% !important;">已报满</small></td>
										</c:when>
										<c:when test="${topic.topStatus eq 2 }">
											<td><small class="label label-success"
												style="font-size: 75% !important;">选报中</small></td>
										</c:when>
									</c:choose>
									<td><small>${topic.teacher.teaName }</small>
									</td>
									<td><small>${topic.teacher.teaTell}</small>
									</td>
									<td><small>${topic.topCount }</small>
									</td>
									<td>
										<div class="visible-md visible-lg hidden-sm hidden-xs">
											<a
												href="./${sessionScope.pathCode}/selectOneTopic.do?topId=${topic.topId}">
												<i class="fa fa-info-circle"></i> <small>详情</small> </a>
												
										</div>
									</td>
								</tr>
							</tbody>
						</c:forEach>
					</table>

				</div>
			</div>
		</div>
		<script
			src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
		<!-- 3 -->
		<script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
		<!-- 1 -->
		<script src="assets/plugins/select2/select2.min.js"></script>
		<!-- 2 -->
		<script src="assets/js/form-elements.js"></script>
		<!-- 4 -->
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
    jQuery(document).ready(function () {
        FormElements.init();
    });
</script>
</body>
</html>
