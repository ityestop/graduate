package cn.edu.zut.soft.graduate.core.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cc on 16/6/16.
 */
public class NodeUtils {
    public static List<RowNode> model1(int length){
        List<RowNode> list = new ArrayList<>();
        int i = 0;
        Node node;
        node = new Node(length,"2016届毕业设计（论文）答辩成绩汇总表");
        RowNode rowNode;
        rowNode = new RowNode();
        rowNode.add(node);
        list.add(rowNode);//放完第一行
        node = new Node(length,"部门盖章：            院长（主任）审核签字：                   指导教师：                       年  月  日\t\t\t\t\t\t\t\t\t\t\t\t");
        rowNode = new RowNode();
        rowNode.add(node);
        list.add(rowNode);//放完第二行
        node = new Node(length,"序号\t学生班级\t学号\t学生姓名\t指导教师姓名\t指导教师职称\t设计(论文)课题名称\t课题来源\t\t\t课题类型\t\t\t指导成绩\t评阅成绩\t答辩成绩\t总评成绩\t备注\n" +
                "\t\t\t\t\t\t\t社会服务\t科研项目\t其他来源\t设计\t论文\t其他类型\t\t\t\t\t");
        rowNode = new RowNode();
        rowNode.add(node);
        list.add(rowNode);//放完第二行

        return null;
    }
}
