package cn.edu.zut.soft.graduate.gradesCenter.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>文件名称：EventListener.java</p>
 * <p>文件描述：</p>
 * <p>版权所有： 版权所有(C)2011-2099</p>
 * <p>公   司： 口袋购物 </p>
 * <p>内容摘要： </p>
 * <p>其他说明： </p>
 * <p>完成日期：17/4/19 下午5:51</p>
 *
 * @author yangxiaotian@weidian.com
 * @version 1.0
 */
public abstract class EventListener {

    public static final Logger  logger = LoggerFactory.getLogger(EventListener.class);

}
