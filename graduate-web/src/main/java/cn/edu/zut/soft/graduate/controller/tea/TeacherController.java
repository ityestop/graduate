package cn.edu.zut.soft.graduate.controller.tea;

import cn.edu.zut.soft.graduate.controller.BaseController;
import cn.edu.zut.soft.graduate.core.constant.IssueKind;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.IssueSource;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by chuchuang on 16/10/25.
 */
@Controller
@RequestMapping("tea")
public class TeacherController extends BaseController {

    @RequestMapping
    public String index(){
        return "tea/index";
    }

    @RequestMapping("declare")
    public String declare(){
        return "tea/newIssue";
    }

    @RequestMapping("issueList")
    public String issueList(){
        return "tea/IssueList";
    }

    @RequestMapping("issueDetail")
    public ModelAndView issueDetails(Map<String, Object> model){
        putViewSource(model);
        return new ModelAndView("tea/issueDetails",model);
    }

    @RequestMapping("myself")
    public String myself(){
        return "tea/teaInfo";
    }

    @RequestMapping("notice")
    public String noticeDetails(){
        return "usual/notice";
    }

    @RequestMapping("history")
    public String history(){
        return "tea/history";
    }

    @RequestMapping("expert")
    public String expert(){
        return "tea/expert";
    }

    @RequestMapping("issueDeclare")
    public String issueDeclare(){
        return "tea/declare";
    }


    @RequestMapping("guide")
    public String guideStu(){
        return "tea/guide";
    }

    private void putViewSource(Map<String, Object> model) {
        model.put("statusList", JSON.toJSON(IssuePhase.getMap()));
        model.put("typeList", JSON.toJSON(IssueSource.getMap()));
        model.put("kindList", JSON.toJSON(IssueKind.getMap()));
    }


}
