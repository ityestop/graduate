package cn.edu.zut.soft.graduate.reportCenter.query;

import cn.edu.zut.soft.basic.core.model.dao.Page;

import java.util.Collection;

/**
 * @author cc
 * @Date 2017/2/11
 * @value 1.0
 */
public interface ReportRuleQuery extends Page {

    Collection<Integer> getIds();

    Integer getType();

    Integer getNum();
}
