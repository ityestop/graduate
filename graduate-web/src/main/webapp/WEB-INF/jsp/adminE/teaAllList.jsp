<%--
  Created by IntelliJ IDEA.
  User: cc
  Date: 15/10/14
  Time: 21:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教师基本信息管理</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap-table/dist/bootstrap-table.min.css" />

</head>
<body>
<!-- start: PAGE HEADER -->
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><i class="clip-home-3"></i> <a href="/admin"> 首页 </a></li>
            <li class="active">教师基本信息</li>
        </ol>
        <div class="page-header">
            <h3>教师基本信息管理</h3>
        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <form class="form-inline" action="/admin/list/tea" method="get">
                <div class="form-group">
                    <input class="form-control" style="width: 300px" title="教师姓名" placeholder="教师姓名" name="name" value="${param.name}"/>
                </div>
                <div class="form-group" style="text-align: right;">
                    <button type="submit" class="btn btn-primary">检索</button>
                </div>
                <div class="form-group">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                        <span><i class=" clip-plus-circle"></i> 添加教师</span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end: PAGE HEADER 頭部結束-->
<div class="row">
    <div class="col-md-12">
        <div class="panel-body">
            <table id="teacherTable" class="table table-container" >
            </table>
        </div>

    </div>
    <!-- end: TABLE WITH IMAGES PANEL -->
</div>

<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title" id="addModalLabel">添加新成员</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="form-group">
                            <div class="errorHandler alert alert-danger no-display">
                                <i class="fa fa-remove-sign"></i>
                                <span>错误</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">教师工号</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="noid" title="教师工号" placeholder="教师工号"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">教师姓名</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="addName" title="教师姓名" placeholder="教师姓名"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">教师手机</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="addPhone" title="教师手机" placeholder="教师手机"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addNewTea()">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/bootstrap-table.js"></script>
<script type="application/javascript" src="assets/plugins/bootstrap-table/dist/locale/bootstrap-table-zh-CN.min.js"></script>
<script type="application/javascript" src="assets/g/js/admin/teaAll-table-data.js"></script>

<%--<script src="/assets/g/js/adminAllTeacher.js"></script>--%>
<%--<script type="application/javascript">--%>
    <%--$(function () {--%>
        <%--var $table = $("#tbody");--%>
        <%--var $name = $("input[name='name']");--%>
        <%--var pageSize = '${pageSize}';--%>

        <%--var professionalLeverMap = JSON.parse('${professionalLeverMap}');--%>

        <%--function getTr(num) {--%>
            <%--var $tr = $("<tr></tr>");--%>
            <%--for (var i = 0; i < num; i++) {--%>
                <%--$tr.append("<td></td>");--%>
            <%--}--%>
            <%--return $tr;--%>
        <%--}--%>

        <%--function createTable(data) {--%>
            <%--$table.html("");--%>
            <%--$.each(data, function (i, node) {--%>
                <%--var $tr = getTr(6);--%>
                <%--var td = $tr.find("td");--%>
                <%--$(td.get(0)).html(node.identity.noId);--%>
                <%--$(td.get(1)).html(node.identity.name);--%>
                <%--$(td.get(2)).html(node.identity.sex == 0 ? "未知" : (node.identity.sex > 0 ? "男" : "女"));--%>
                <%--var infoMap = {};--%>
                <%--$.each(node.infoList, function (j, no) {--%>
                    <%--infoMap[no.key] = no.value;--%>
                <%--});--%>
                <%--$(td.get(3)).html(professionalLeverMap[infoMap['professionalLever']]);--%>
                <%--$(td.get(4)).html(node.identity.phone);--%>
                <%--var $reset = $("<a class='btn btn-link'>重置密码</a>");--%>
                <%--$reset.click(function () {--%>
                    <%--$.ajax({--%>
                        <%--url: "/identity/resetPwd/" + node.identity.id,--%>
                        <%--type: "put",--%>
                        <%--dataType: "json",--%>
                        <%--success: function (result) {--%>
                            <%--if (result.success) {--%>
                                <%--alert("重置成功,密码为用户手机号");--%>
                            <%--} else {--%>
                                <%--alert(result.errorMsg);--%>
                            <%--}--%>
                        <%--}--%>
                    <%--});--%>
                <%--});--%>
                <%--$(td.get(5)).html($reset);--%>
                <%--$table.append($tr);--%>
            <%--})--%>

        <%--}--%>

        <%--function createPage(count) {--%>
            <%--var opt = {--%>
                <%--$container: $("#page-nav"),--%>
                <%--className: "pagination pagination-sm",--%>
                <%--pageCount: Math.ceil(count / pageSize),--%>
                <%--beforeFun: function (targetNum) {--%>
                    <%--var data = {--%>
                        <%--name: '${param.name}'.trim(),--%>
                        <%--pageSize: pageSize,--%>
                        <%--currentPage: targetNum - 1--%>
                    <%--};--%>
                    <%--$.ajax({--%>
                        <%--url: "/identity/list/tea",--%>
                        <%--data: data,--%>
                        <%--dataType: "json",--%>
                        <%--type: "post",--%>
                        <%--success: function (result) {--%>
                            <%--if (result.success) {--%>
                                <%--createTable(result.data);--%>
                            <%--} else {--%>
                                <%--alert(result.errorMsg);--%>
                            <%--}--%>
                        <%--}--%>
                    <%--});--%>
                <%--}--%>
            <%--};--%>
            <%--createPageNav(opt);--%>
        <%--}--%>

        <%--createPage('${requestScope.count}');--%>
        <%--createTable(JSON.parse('${requestScope.list}'));--%>
    <%--});--%>
<%--</script>--%>


</body>
</html>
