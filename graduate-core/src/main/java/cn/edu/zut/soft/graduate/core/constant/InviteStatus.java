package cn.edu.zut.soft.graduate.core.constant;

/**
 * @author chuchuang
 * @date 16/11/29
 */
public enum InviteStatus {
    ING,YES,NO,INVALID
}
