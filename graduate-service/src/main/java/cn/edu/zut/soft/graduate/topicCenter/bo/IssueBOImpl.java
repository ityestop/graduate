package cn.edu.zut.soft.graduate.topicCenter.bo;

import cn.edu.zut.soft.basic.core.model.bo.CommonResult;
import cn.edu.zut.soft.basic.core.model.dao.Criteria;
import cn.edu.zut.soft.basic.core.model.dao.Example;
import cn.edu.zut.soft.basic.core.util.StringUtils;
import cn.edu.zut.soft.graduate.base.bo.BasicBOImpl;
import cn.edu.zut.soft.graduate.base.cache.CachePool;
import cn.edu.zut.soft.graduate.base.dao.BasicDAO;
import cn.edu.zut.soft.graduate.base.former.AuditIssueLinkIdentityTransformer;
import cn.edu.zut.soft.graduate.base.former.IdTransformer;
import cn.edu.zut.soft.graduate.base.former.InfoIdentityTransformer;
import cn.edu.zut.soft.graduate.core.constant.DeclareStatus;
import cn.edu.zut.soft.graduate.core.constant.InfoType;
import cn.edu.zut.soft.graduate.core.constant.IssuePhase;
import cn.edu.zut.soft.graduate.core.constant.Role;
import cn.edu.zut.soft.graduate.core.constant.config.IKEY;
import cn.edu.zut.soft.graduate.core.design.BasicInfoStrategy;
import cn.edu.zut.soft.graduate.core.design.InfoStrategy;
import cn.edu.zut.soft.graduate.core.model.Impl.*;
import cn.edu.zut.soft.graduate.core.query.AuditIssueLinkQuery;
import cn.edu.zut.soft.graduate.core.query.IssueQuery;
import cn.edu.zut.soft.graduate.core.vo.*;
import cn.edu.zut.soft.graduate.topicCenter.dao.IssueDAO;
import cn.edu.zut.soft.graduate.topicCenter.handler.IssueStatusChangeHandler;
import cn.edu.zut.soft.graduate.topicCenter.op.IssueOP;
import cn.edu.zut.soft.graduate.topicCenter.pojo.IssueTakeWitness;
import cn.edu.zut.soft.graduate.userCenter.bo.IdentityBO;
import cn.edu.zut.soft.graduate.userCenter.bo.InfoBO;
import cn.edu.zut.soft.graduate.userCenter.bo.WitnessBO;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.*;

/**
 * Created by chuchuang on 16/10/22.
 */
@Service
public class IssueBOImpl extends BasicBOImpl<Issue, IssueQuery> implements IssueBO {

    @Resource
    private IssueDAO<Issue> issueDAO;

    @Autowired
    private IssueOP issueOP;

    @Autowired
    private IdTransformer idTransformer;
    @Autowired
    private AuditIssueLinkIdentityTransformer auditIssueLinkIdentityTransformer;

    @Autowired
    private IdentityBO identityBO;

    @Autowired
    private InfoBO infoBO;

    @Autowired
    private WitnessBO witnessBO;

    @Resource
    private AuditIssueLinkBO auditIssueLinkBO;

    @Resource
    private DeclareBO declareBO;

    @Autowired
    private InfoIdentityTransformer infoIdentityTransformer;

    @Override
    protected BasicDAO<Integer, Issue> getDAO() {
        return issueDAO;
    }


    @Override
    protected List<Criteria> Query(IssueQuery issueQuery) {
        Criteria criteria = new Criteria();
        if (CollectionUtils.isNotEmpty(issueQuery.getIds())) {
            criteria.andIn("id", issueQuery.getIds());
        }
        if (issueQuery.getIdentityId() != null) {
            criteria.andEqualTo("identity_id", issueQuery.getIdentityId());
        }
        if (CollectionUtils.isNotEmpty(issueQuery.getIdentityIds())) {
            criteria.andIn("identity_id", issueQuery.getIdentityIds());
        }
        if (issueQuery.getSchoolTeaId() != null) {
            criteria.andEqualTo("school_tea_id", issueQuery.getSchoolTeaId());
        }
        if (CollectionUtils.isNotEmpty(issueQuery.getSchoolTeaIds())) {
            criteria.andIn("school_tea_id", issueQuery.getSchoolTeaIds());
        }
        if (issueQuery.getIssueKind() != null) {
            criteria.andEqualTo("kind", issueQuery.getIssueKind());
        }
        if (issueQuery.getIssueSource() != null) {
            criteria.andEqualTo("source", issueQuery.getIssueSource());
        }

        if (issueQuery.getTaskSize() != null) {
            criteria.andEqualTo("task_size", issueQuery.getTaskSize());
        }
        if (issueQuery.getIssuePhase() != null) {
            criteria.andEqualTo("`status`", issueQuery.getIssuePhase());
        } else {
            criteria.andIn("`status`", issueQuery.getIssuePhaseList());
        }

        if (issueQuery.getIdentityRole() != null) {
            criteria.andEqualTo("identity_role", issueQuery.getIdentityRole());
        }

        if (issueQuery.getDel() != null) {
            criteria.andEqualTo("del", issueQuery.getDel());
        } else {
            criteria.andEqualTo("del", IKEY.ZERO);
        }
        if (org.apache.commons.lang.StringUtils.isNotBlank(issueQuery.getIdentityName())) {
            criteria.andLike("identity_name", issueQuery.getIdentityName() + "%");
        }
        if (org.apache.commons.lang.StringUtils.isNotBlank(issueQuery.getTitle())) {
            criteria.andLike("title", issueQuery.getTitle() + "%");
        }
        if (org.apache.commons.lang.StringUtils.isNotBlank(issueQuery.getSchoolTeaName())) {
            criteria.andLike("school_tea_name", issueQuery.getIdentityName() + "%");
        }

        return Collections.singletonList(criteria);
    }


    @Override
    public CommonResult<IssueVO> save(IssueVO issueVO, LoginVO loginVO) {
//        issueVO.getIssue().setUpdateAuthor(operator);
        for (Task task : issueVO.getIssueContent().getTaskList()) {
            task.setNid(StringUtils.createNid(4));
        }
        issueVO.getIssue().setContent(JSONObject.toJSONString(issueVO.getIssueContent()));
        // FIXME: 16/10/22 可以考虑换成摘要
        issueVO.getIssue().setContentHash("" + issueVO.getIssue().getContent().hashCode());
//        issueVO.getIssue().setStatus(IssuePhase.saveIng);
        issueVO.getIssue().addStatus(IssuePhase.saveIng);
        if (Role.valueOf(loginVO.getRole()) == Role.stu) {
            //当前方法只有在学生自拟题目时才会生效,否则不生效
            List<InfoStrategy> infoStrategyList = Arrays.<InfoStrategy>asList(new BasicInfoStrategy(InfoType.Group, IKEY.GROUP, null), new BasicInfoStrategy(InfoType.Tea, IKEY.Guide, null), new BasicInfoStrategy(InfoType.Tea, IKEY.GuideName, null));
            //查询info表汇总 是否村存在小组ID、指导教师ID、指导教师名字
            List<Info> infoList = infoBO.queryByUserInfoStrategy(loginVO.getId(), infoStrategyList);
            Assert.isTrue(infoList.size() == infoStrategyList.size(), "不存在小组或者指导教师数据不完整");
            //得出指导教师插入
            for (Info info : infoList) {
                if (InfoType.Tea.name().equals(info.getType())) {
                    Assert.isTrue(Arrays.asList(IKEY.Guide, IKEY.GuideName).contains(info.getKey()), "教师数据错误,请联系管理员");
                    //如果存在指导教师 存放指导教师ID/指导教师名字
                    if (IKEY.Guide.equals(info.getKey())) {
                        issueVO.getIssue().setSchoolTeaId(Integer.parseInt(info.getValue()));
                    } else {
                        issueVO.getIssue().setSchoolTeaName(info.getValue());
                    }
                }
            }
            //得出小组ID
            String groupIdDelcare = null;
            for (Info info : infoList) {
                if (info.getKey().equals(IKEY.GROUP)) {
                    groupIdDelcare = info.getValue();
                    break;
                }
            }
            //插入题目 得出题目ID 小组ID
            CommonResult<Issue> issueStu = this.save(issueVO.getIssue(), loginVO.getName());
            if (issueStu.isSuccess()) {
                final Issue issueMessage = this.get(issueVO.getIssue().getId());
                issueVO.setIssue(issueMessage);
                String issueId = issueStu.getData().getId().toString();
                if (issueStu.getData().getId() != null && groupIdDelcare != null) {
                    //插入选报记录
                    Declare declare = new Declare();
                    declare.setGroupId(Integer.parseInt(groupIdDelcare));
                    declare.setTopicId(issueStu.getData().getId());
                    declare.setStatus(DeclareStatus.ING);
                    CommonResult<Declare> delcareResult = declareBO.save(declare, loginVO.getName());
                    if (delcareResult.isSuccess()) {
                        Info infoTopicId = infoBO.save(new BasicInfoStrategy(InfoType.TOPIC, IKEY.TopicId, issueId), loginVO.getId(), loginVO.getName());
                        Info infoTopicName = infoBO.save(new BasicInfoStrategy(InfoType.TOPIC, IKEY.TopicName, issueMessage.getTitle()), loginVO.getId(), loginVO.getName());
                        return CommonResult.successReturn(issueVO);
                    }
                }
            }
        } else {
            issueVO.getIssue().setSchoolTeaId(loginVO.getId());
            issueVO.getIssue().setSchoolTeaName(loginVO.getName());
        }
        CommonResult<Issue> issueResult = this.save(issueVO.getIssue(), loginVO.getName());
        if (!issueResult.isSuccess()) {
            logger.error("save is error");
            return CommonResult.errorReturn(issueResult.getErrorMsg());
        }
        final Issue result = this.get(issueVO.getIssue().getId());
        issueVO.setIssue(result);
        return CommonResult.successReturn(issueVO);
    }

    @Override
    public CommonResult<IssueVO> update(IssueVO issueVO, String operator) {
        //消除状态改变
        issueVO.getIssue().setStatus(null);
        issueVO.getIssue().setStateSet(null);
        issueVO.getIssue().setUpdateAuthor(operator);
        for (Task task : issueVO.getIssueContent().getTaskList()) {
            if (org.apache.commons.lang.StringUtils.isBlank(task.getNid())) {
                return CommonResult.errorReturn("课题子标题编号不存在");
            }
        }
        // FIXME: 16/10/22 可以考虑摘要对比之后进行判断是否修改,可优化
        issueVO.getIssue().setContent(JSONObject.toJSONString(issueVO.getIssueContent()));
        // FIXME: 16/10/22 可以考虑换成摘要
        issueVO.getIssue().setContentHash("" + issueVO.getIssue().getContent().hashCode());
        CommonResult<Issue> issueResult = this.update(issueVO.getIssue(), operator);
        if (!issueResult.isSuccess()) {
            logger.error("update is error");
            return CommonResult.errorReturn(issueResult.getErrorMsg());
        }
        final Issue result = this.get(issueVO.getIssue().getId());
        issueVO.setIssue(result);
        //修改学生数据
        updateStuTopic(issueVO.getIssueContent().getTaskList(), operator, result);

        // FIXME: 16/10/22 风险高并发时数据异常
        return CommonResult.successReturn(issueVO);
    }

    /**
     * 修改学生数据
     * @param taskList 子任务列表
     * @param operator 修改者
     * @param issue 课题数据
     */
    private void updateStuTopic(List<Task> taskList, String operator, Issue issue) {
        //修改学生数据
        List<Info> issueList = infoBO.queryByInfoStrategy(Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.TOPIC, IKEY.TopicId, issue.getId().toString())));
        if (CollectionUtils.isNotEmpty(issueList)) {
            List<Integer> ids = new ArrayList<>();
            CollectionUtils.collect(issueList,infoIdentityTransformer,ids);
            for (Integer id : ids){
                //更新课题信息
                infoBO.save(new BasicInfoStrategy(InfoType.TOPIC,IKEY.TopicName, issue.getTitle()),id,operator);
                //更新子标题信息
                List<Info> childInfo = infoBO.queryByUserInfoStrategy(id, Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.TOPIC, IKEY.ChildTopicId, null)));
                if (!childInfo.isEmpty()){
                    Task myTask = null;
                    for (Task task:taskList){
                        if(task.getNid().equals(childInfo.get(0).getValue())){
                            myTask = task;
                        }
                    }
                    if (myTask == null){
                        myTask = new Task();
                        //清空
                        infoBO.delete(new BasicInfoStrategy(InfoType.TOPIC,IKEY.ChildTopicId,myTask.getNid()),id,operator);
                        infoBO.delete(new BasicInfoStrategy(InfoType.TOPIC,IKEY.ChildTopicName,myTask.getTitle()),id,operator);
                    }
                    infoBO.save(new BasicInfoStrategy(InfoType.TOPIC,IKEY.ChildTopicName,myTask.getTitle()),id,operator);
                }
            }
        }
    }

    @Override
    public CommonResult<List<IssueTakeWitness>> findByQueryTakeWitness(IssueQuery issueQuery) {
        CommonResult<List<Issue>> result = this.findByQuery(issueQuery);
        List<Issue> data = result.getData();
        //如果不存在结果直接返回
        if (CollectionUtils.isEmpty(data)) {
            return CommonResult.successReturn(Collections.<IssueTakeWitness>emptyList());
        }
        List<Integer> ids = new ArrayList<>(data.size());
        CollectionUtils.collect(data, idTransformer, ids);
        AuditIssueLinkQuery auditIssueLinkQuery = new AuditIssueLinkQuery();
        auditIssueLinkQuery.clear();
        auditIssueLinkQuery.setIssueIds(ids);
        List<AuditIssueLink> auditIssueLinkList = auditIssueLinkBO.findByQuery(auditIssueLinkQuery).getData();
        List<IssueTakeWitness> issueTakeWitnessList = new ArrayList<>();
        IssueTakeWitness issueTakeWitness;
        if (CollectionUtils.isEmpty(auditIssueLinkList)) {
            for (Issue issue : data) {
                issueTakeWitness = new IssueTakeWitness();
                issueTakeWitness.setIssue(issue);
                issueTakeWitnessList.add(issueTakeWitness);
            }
        } else {
            Map<Integer, AuditIssueLink> auditIssueLinkMap = new HashMap<>(auditIssueLinkList.size());
            for (AuditIssueLink auditIssueLink : auditIssueLinkList) {
                auditIssueLinkMap.put(auditIssueLink.getIssueId(), auditIssueLink);
            }
            List<WitnessData> witnessDataList = witnessBO.getAllWitnessData().getData();
            boolean witnessNull = true;
            if (CollectionUtils.isEmpty(witnessDataList)) {
                witnessNull = false;
            }
            Map<Integer, WitnessData> identityMap = new HashMap<>(witnessDataList.size());
            for (WitnessData witnessData : witnessDataList) {
                identityMap.put(witnessData.getId(), witnessData);
            }
            for (Issue issue : data) {
                issueTakeWitness = new IssueTakeWitness();
                issueTakeWitness.setIssue(issue);
//                logger.warn("issue={},auditIssueLink={}",issue,auditIssueLinkMap.get(issue.getId()));
                if (witnessNull) {
                    AuditIssueLink auditIssueLink = auditIssueLinkMap.get(issue.getId());
                    WitnessData witnessData = identityMap.get(auditIssueLink.getIdentityId());
                    issueTakeWitness.setWitness(witnessData);
                }
                issueTakeWitnessList.add(issueTakeWitness);
            }
        }
        //设定总数
        return CommonResult.successReturn(issueTakeWitnessList, result.getTotalCount());
    }

    @Override
    @Transactional
    public Issue changeIssuePhase(LoginVO loginVO, Integer id, IssuePhase.Handle handle) {
        Assert.notNull(handle, "操作不存在");
        Issue issue = this.get(id);
        Assert.notNull(issue, "id错误,请重新输入");
        IssuePhase src = issue.getStatus();
        Assert.notNull(src, "课题状态错误, issue=" + issue);
        IssuePhase dst = IssuePhase.Handle.from(src, handle);
        Assert.notNull(dst, "操作不存在," + src + handle);

        //循环处理结果状态,呈替换情况。使用请多次验证
        for (IssueStatusChangeHandler issueStatusChangeHandler : issueOP.getStatusChangeHandlerList()) {
            issueStatusChangeHandler.efficacyAndChange(loginVO, issue, handle);
        }
        //以下操作均改到处理器中处理
//        //判断是否失效,失效时同时逻辑删除
//        issue.setStatus(dst);
//        issue.addStatus(dst);
//        //优化性能,去除大对象的修改
//        issue.setContent(null);

        this.update(issue, loginVO.getName());
        Issue result = getDAO().selectByPrimaryKey(id);
        Assert.notNull(result, "课题不存在");
//        Assert.isTrue(handle.getDst() != issue.getStatus(),"课题状态已经修改过");
//        Assert.isTrue(issue == null || issue.getStatus() != issue.getStatus(),"操作修改失败");
        return result;
    }

    @Override
    public Integer batchChangeIssuePhase(LoginVO loginVO, IssuePhase.Handle handle, List<Integer> ids) {
        Example example = new Example();
        if (CollectionUtils.isNotEmpty(ids)) {
            example.createCriteria().andIn("id", ids).andEqualTo("del", IKEY.ZERO);
        } else {
            example.createCriteria().andEqualTo("`status`", handle.getSrc()).andEqualTo("del", IKEY.ZERO);
        }
        Issue issue = new Issue();
        issue.setStatus(handle.getDst());
        issue.setUpdateAuthor(loginVO.getName());
        return ids.size() - getDAO().updateByExampleSelective(issue, example);
    }

    @Override
    public CommonResult<List<Issue>> queryByIds(final List<Integer> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return CommonResult.successReturn(null);
        }
        return CommonResult.successReturn(getDAO().selectByExample(new Example() {
            {
                createCriteria().andIn("id", ids)
                        .andEqualTo("del", 0);
            }
        }));
    }

    @Override
    public List<Issue> noExpertIssues(String role) {
        List<Issue> result = null;
        List<Integer> ail = auditIssueLinkBO.noExpertTopic();
        Example example = new Example();
        String roles = null;
        if (org.apache.commons.lang.StringUtils.isEmpty(role)) {
            roles = "tea";
        } else {
            roles = role;
        }
        if (CollectionUtils.isEmpty(ail)) {
            example.createCriteria()
                    .andEqualTo("identity_role", roles)
                    .andEqualTo("status", IssuePhase.auditIng)
                    .andEqualTo("del", "0");

            result = issueDAO.selectByExample(example);
            return result;
        }
        example.createCriteria()
                .andEqualTo("identity_role", roles)
                .andEqualTo("status", IssuePhase.auditIng)
                .andEqualTo("del", "0")
                .andNotIn("id", ail);
        result = issueDAO.selectByExample(example);
        return result;
    }

    @Override
    public List<Issue> getExpertIssueByTeaID(Integer teaId, String role) {
        if (teaId == null) {
            return null;
        }
        List<Issue> result = new ArrayList<>();
        List<Integer> ail = auditIssueLinkBO.getExpertIssue(teaId);
        if (CollectionUtils.isEmpty(ail)) {
            return result;
        }
        Example example = new Example();
        switch (role) {
            case "tea":
                example.createCriteria()
                        .andEqualTo("del", "0")
                        .andIn("id", ail);
                break;
            case "admin":
                example.createCriteria()
                        .andEqualTo("status", IssuePhase.auditIng)
                        .andEqualTo("del", "0")
                        .andIn("id", ail);
                break;
            default:
                example.createCriteria()
                        .andEqualTo("status", IssuePhase.auditIng)
                        .andEqualTo("del", "0")
                        .andIn("id", ail);
                break;
        }
        result = issueDAO.selectByExample(example);
        return result;
    }

    @Override
    @Transactional
    public IssueVO refuseAndRemark(LoginVO loginVO, IssuePhase.Handle handle, String id, String operator, String remark) {
        if (handle == null || org.apache.commons.lang.StringUtils.isEmpty(id)) {
            return null;
        }
        Integer issueId = Integer.parseInt(id);
        Issue issue = this.get(issueId);
        if (issue == null) {
            return null;
        }
        IssueContent issueContent = JSON.parseObject(issue.getContent(), IssueContent.class);
        issueContent.setRemark(remark);
        IssueVO issueVO = new IssueVO();
        issueVO.setIssue(issue);
        issueVO.setIssueContent(issueContent);
        CommonResult<IssueVO> update = this.update(issueVO, operator);
        IssueVO result = new IssueVO();
        if (update.isSuccess()) {
            result.setIssue(issueOP.changeIssuePhase(loginVO, issueId, handle));
            result.convert();
        }
        return result;
    }


    public CommonResult<List<Issue>> findByQueryWith(IssueQuery issueQuery) {
        List<Criteria> criteriaList = Query(issueQuery);
        Example example = Example.createOfPage(issueQuery);
        for (Criteria c : criteriaList) {
            example.or(c);
        }
        CommonResult<List<Issue>> commonResult = new CommonResult<>(true, issueDAO.selectByExampleWithBLOBs(example));
        commonResult.setTotalCount(getDAO().countByExample(example.cleanPage()));
        return commonResult;
    }

    @Override
    public Issue assignChildTopic(Integer topId, String childId, Integer stuId, LoginVO loginVO) {
        if (topId == null || org.apache.commons.lang.StringUtils.isEmpty(childId) || stuId == null) {
            return null;
        }
        Issue issue = this.get(topId);
        IssueContent issueContent = null;
        Info info = null;
        if (issue != null) {
            issueContent = JSON.parseObject(issue.getContent(), IssueContent.class);
            if (issueContent != null) {
                for (Task task : issueContent.getTaskList()) {
                    if (task.getNid().equals(childId)) {
                        task.setIsAssign("YES");
                        info = infoBO.save(new BasicInfoStrategy(InfoType.TOPIC, IKEY.ChildTopicId, childId), stuId, loginVO.getName());
                        info = infoBO.save(new BasicInfoStrategy(InfoType.TOPIC, IKEY.ChildTopicName, task.getTitle()), stuId, loginVO.getName());
                    }
                }
                issue.setContent(JSON.toJSONString(issueContent));
                Example example = new Example();
                example.createCriteria().andEqualTo("id", topId);
                int i = getDAO().updateByExampleSelective(issue, example);
                if (i > 0) {
                    return issue;
                }
            }
        }
        return null;
    }

    @Override
    public Issue restChildTopic(Integer topId, String childId, Integer stuId, LoginVO loginVO) {
        if (topId == null || org.apache.commons.lang.StringUtils.isEmpty(childId) || stuId == null) {
            return null;
        }
        Issue issue = this.get(topId);
        IssueContent issueContent = null;
        List<Info> info = null;
        if (issue != null) {
            issueContent = JSON.parseObject(issue.getContent(), IssueContent.class);
            if (issueContent != null) {
                for (Task task : issueContent.getTaskList()) {
                    if (task.getNid().equals(childId)) {
                        task.setIsAssign(null);
                        info = infoBO.delete(new BasicInfoStrategy(InfoType.TOPIC, IKEY.ChildTopicId, childId), stuId, loginVO.getName());
                        info = infoBO.delete(new BasicInfoStrategy(InfoType.TOPIC, IKEY.ChildTopicName, task.getTitle()), stuId, loginVO.getName());
                    }
                }
                issue.setContent(JSON.toJSONString(issueContent));
                Example example = new Example();
                example.createCriteria().andEqualTo("id", topId);
                int i = getDAO().updateByExampleSelective(issue, example);
                if (i > 0) {
                    return issue;
                }
            }
        }
        return null;
    }

    @Override
    public IssueVO getCacheIssueVO(Integer id) {
        IssueVO issueVO = CachePool.issueVOCacheMap.get(id);
        if (issueVO == null){
            Issue issue = issueDAO.selectByPrimaryKey(id);
            if (issue == null){
                throw new IllegalArgumentException("id is error");
            }
            issueVO = new IssueVO();
            issueVO.setIssue(issue);
            issueVO.setIssueContent(JSON.parseObject(issue.getContent(),IssueContent.class));
        }
        CachePool.issueVOCacheMap.put(issueVO.getIssue().getId(),issueVO);
        return issueVO;
    }

    /**
     * 教师小组的课题列表 将会显示所有的课题,检索条件为schoolTeaId
     *
     * @param groupId
     */
    @Override
    public CommonResult<List<Issue>> queryTeaGroupAll(Integer groupId, IssueQuery issueQuery) {
        List<Info> teaInfoList = infoBO.queryByInfoStrategy(Collections.<InfoStrategy>singletonList(new BasicInfoStrategy(InfoType.Group, IKEY.GROUP, groupId == null ? null : groupId.toString())));
        if (CollectionUtils.isEmpty(teaInfoList)) {
            return CommonResult.successReturn(Collections.<Issue>emptyList());
        }
        Set<Integer> ids = new HashSet<>(teaInfoList.size());
        CollectionUtils.collect(teaInfoList, infoIdentityTransformer, ids);
        //指导的教师id集合
        issueQuery.setSchoolTeaIds(new ArrayList<Integer>(ids));
        //坑呀 状态没有设置。。。。
        issueQuery.setIssuePhase(IssuePhase.finish);
        return findByQueryWith(issueQuery);
    }


}
