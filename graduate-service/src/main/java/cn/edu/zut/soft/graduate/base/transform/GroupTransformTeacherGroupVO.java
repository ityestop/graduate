package cn.edu.zut.soft.graduate.base.transform;

import cn.edu.zut.soft.graduate.core.model.Impl.Group;
import cn.edu.zut.soft.graduate.core.model.view.TeacherGroupVO;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.Transformer;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

/**
 * 注释:
 *
 * @author cc
 * @Date 2017/5/9
 * @value 1.0
 */
@Component
public class GroupTransformTeacherGroupVO implements Transformer<Group,TeacherGroupVO> {
    @Override
    public TeacherGroupVO transform(Group group) {
        if (group == null){
            return null;
        }
        TeacherGroupVO teacherGroupVO = new TeacherGroupVO();
        try {
            BeanUtils.copyProperties(teacherGroupVO,group);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return teacherGroupVO;
    }
}
